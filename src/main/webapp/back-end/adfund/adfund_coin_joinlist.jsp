<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="java.util.*"%>
<%@ page import="com.adfund.model.*"%>
<%@ page import="com.adfundRecord.model.*"%>


<jsp:useBean id="adfund_coin_joinlist" scope="request" type="java.util.Set<ADFundRecordVO>" />


<html lang="">
<head>

<title>廣告募資管理列表</title>
<meta charset="UTF-8">
<meta name="description" content="Food Blog Web Template">
<meta name="keywords" content="food, unica, creative, html">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<!-- Favicon -->   
	<link href="img/favicon.ico" rel="shortcut icon"/>

	<!-- Google Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Poppins:400,400i,500,500i,600,600i,700" rel="stylesheet">

	<!-- Stylesheets -->
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/css/bootstrap.min.css"/>
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/css/font-awesome.min.css"/>
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/css/owl.carousel.css"/>
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/css/animate.css"/>
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/css/style.css"/>
	
	<!-- original -->			
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/adfund/css/bootstrap.min.css">
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/adfund/css/bootstrap-theme.min.css">
	
	<script src="<%=request.getContextPath() %>/front-end/adfund/js/jquery-3.3.1.min.js"></script>
	
	<script src="<%=request.getContextPath() %>/front-end/adfund/js/bootstrap.min.js"></script>
	
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/adfund/css/index.css">


<style type="text/css">
body{
    font-family:\5FAE\8EDF\6B63\9ED1\9AD4,\65B0\7D30\660E\9AD4;
}

.kk-adfund-table {
	margin-top: 20px;
	text-align: center;
	font-size: 13px;
}

.kk-adfund-table thead th {
	margin-top: 20px;
	text-align: center;
	background-color: #DCDCDC;
}

.btn-adfund-back {
	background-color: #9999cc;
	border: 1px solid;
	border-color: #DCDCDC;
	padding: 3px;
	color: white;
	font-size: 11px;
}
</style>

</head>
<body>


	<%-- 錯誤表列 --%>
	<c:if test="${not empty errorMsgs}">
		<font style="color: red">請修正以下錯誤:</font>
		<ul>
			<c:forEach var="message" items="${errorMsgs}">
				<li style="color: red">${message}</li>
			</c:forEach>
		</ul>
	</c:if>

    <div class="container-fiuld">
        	<div class="row">
        		<div class="col-xs-12 col-sm-12 ">

        			<h2>廣告參與者明細</h2>
        			
        			<FORM METHOD="get" ACTION="<%=request.getContextPath()%>/adfundRecord/adfundRecord.do">
        			<input type="hidden" name="action" value="returnCoin">
        			
        			<input type="submit" value="退代幣">
        			
        			
                   <table class="table table-hover table-striped table-condensed table-bordered kk-adfund-table">
                  	

                   	<thead class="kk-adfund-thead">
                   		<tr>
                   		<th>廣告募資明細編號</th>
                   		<th>廣告募資編號</th>
                   		<th>會員編號</th>
                   		<th>捐贈代幣額度</th>
                   		<th>參加募資時間點</th>	
                   		<th>退代幣</th>
                        </tr>
                   	</thead>

                   	<tbody>
                       <c:forEach var="adfundRecordVO" items="${adfund_coin_joinlist}">
						<tr>
							<td>${adfundRecordVO.adfundRecordID}</td>
							<td>${adfundRecordVO.adfundID}</td>
							<td>${adfundRecordVO.memberID}</td>
							<td>${adfundRecordVO.adfundCoinUsed}</td>
							<td>${adfundRecordVO.adfundJoinDate}</td>
							<td>
							<input type="hidden" name="adfundID" value="${adfundRecordVO.adfundID }"> 							   							
							</td>
						</tr>						
                   	</tbody> 
                   	</c:forEach>                 	
                   </table>
                   </form>
        		</div>
        		
        	</div>
        </div>
	

<!--====== Javascripts & Jquery ======-->
	<script src="<%=request.getContextPath() %>/front-end/js/jquery-3.2.1.min.js"></script>
	<script src="<%=request.getContextPath() %>/front-end/js/owl.carousel.min.js"></script>
	<script src="<%=request.getContextPath() %>/front-end/js/main.js"></script>
	<script src="https://code.jquery.com/jquery.js"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
</body>
</html>
