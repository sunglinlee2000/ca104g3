<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="java.util.*"%>
<%@ page import="com.adfund.model.*"%>
<%@ page import="com.adfundReport.model.*"%>


<%
    ADFundReportService adfundSvc = new ADFundReportService();
    List<ADFundReportVO> list = adfundSvc.getAllADFundReport();
    pageContext.setAttribute("list",list);
%>

<html lang="">
<head>
<title>廣告募資管理列表</title>
<meta charset="UTF-8">
	<meta name="description" content="Food Blog Web Template">
	<meta name="keywords" content="food, unica, creative, html">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	
	<!-- Favicon -->   
	<link href="img/favicon.ico" rel="shortcut icon"/>

	<!-- Google Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Poppins:400,400i,500,500i,600,600i,700" rel="stylesheet">

	<!-- Stylesheets -->
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/css/bootstrap.min.css"/>
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/css/font-awesome.min.css"/>
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/css/owl.carousel.css"/>
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/css/animate.css"/>
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/css/style.css"/>
	
	
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/adfund/css/bootstrap.min.css">
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/adfund/css/bootstrap-theme.min.css">
	
	<script src="<%=request.getContextPath() %>/front-end/adfund/js/jquery-3.3.1.min.js"></script>
	
	<script src="<%=request.getContextPath() %>/front-end/adfund/js/bootstrap.min.js"></script>


	<!--[if lt IE 9]>
	  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

<style type="text/css">
body{
    font-family:\5FAE\8EDF\6B63\9ED1\9AD4,\65B0\7D30\660E\9AD4;
}

.kk-adfund-table {
	margin-top: 20px;
	text-align: center;
	font-size: 13px;
}

.kk-adfund-table thead th {
	margin-top: 20px;
	text-align: center;
	background-color: #DCDCDC;
}

.btn-adfund-back {
	background-color: #9999cc;
	border: 1px solid;
	border-color: #DCDCDC;
	padding: 3px;
	color: white;
	font-size: 11px;
}
</style>

</head>
<body>
<!-- Header section -->
	<header class="header-section">
		
		<div class="header-bottom">
			<div class="container">
			
	<!-- 返回後台首頁							 -->
				<a href="<%=request.getContextPath()%>/back-end/administrator.jsp">
					<button style="background-color:white;border: none;">
						<img  src="<%=request.getContextPath() %>/front-end/img/logo-1.png">
					</button>
				</a>
	<!-- 返回後台首頁end							 -->			
				
				<div class="nav-switch">
					<i class="fa fa-bars"></i>
				</div>
				
				<ul class="main-menu">
			
					<li>Super Administrator，你好</li>
					<li>
						<i><a href="<%=request.getContextPath()%>/back-end/login.jsp"  onclick="logout()"><img src="<%=request.getContextPath() %>/back-end/img/signout_106525.png"></a></i>						
					</li>
				</ul>
			</div>
		</div>
	</header>
<!-- Header section end -->

	<%-- 錯誤表列 --%>
	<c:if test="${not empty errorMsgs}">
		<font style="color: red">請修正以下錯誤:</font>
		<ul>
			<c:forEach var="message" items="${errorMsgs}">
				<li style="color: red">${message}</li>
			</c:forEach>
		</ul>
	</c:if>





	<div class="container kk-ad-back-con">
		<div class="row">
			<div class="col-xs-12 col-sm-12 ">

				<h2>廣告檢舉明細</h2>
        			
                   <table class="table table-hover table-striped table-condensed table-bordered kk-adfund-table">
                  	

                   	<thead class="kk-adfund-thead">
                   		<tr>
                   		<th>廣告募資檢舉編號</th>
                   		<th>廣告募資編號</th>
                   		<th>會員編號</th>
                   		<th>管理者編號</th>
                   		<th>檢舉事由</th>
                   		<th>檢舉內容</th>                   			
                   		<th>檢舉時間</th>
                   		<th>審核狀態</th>
                   		<th>修改審核狀態</th>	
                        </tr>
                   	</thead>

                   	<tbody>
                       <c:forEach var="adfundReportVO" items="${list}">
						<tr>
							<td>${adfundReportVO.adfundReportID }</td>
							<td>${adfundReportVO.adfundID }</td>
							<td>${adfundReportVO.memberID }</td>
							<td>${adfundReportVO.administratorID }</td>
							<td>${adfundReportVO.adfundReportReason }</td>
							<td>${adfundReportVO.adfundReportContent }</td>
							<td>${adfundReportVO.adfundReportDate }</td>
							<td id="select-status">${adfundReportVO.adfundReportStatus }</td>
							
							<td>							
							<FORM METHOD="post" ACTION="<%=request.getContextPath()%>/adfundReport/adfundReport.do" enctype="multipart/form-data">
							<input type="submit" value="審查狀態" class="btn btn-adfund-back"> 
						    <input type="hidden" name="adfundReportID" value="${adfundReportVO.adfundReportID}">
						    <input type="hidden" name="adfundID" value="${adfundReportVO.adfundID }">
							<input type="hidden" name="requestURL"	value="<%=request.getServletPath()%>"><!--送出本網頁的路徑給Controller--><!-- 目前尚未用到  -->
							<input type="hidden" name="action"	    value="getOne_For_Update"></FORM>
							</td>                            
						</tr>
						
                   	</tbody>
                   	</c:forEach>                   	
                   </table>
                              
        		</div>
        		
        	</div>
        </div>



<!--====== Javascripts & Jquery ======-->
	<script src="<%=request.getContextPath() %>/front-end/js/jquery-3.2.1.min.js"></script>
	<script src="<%=request.getContextPath() %>/front-end/js/owl.carousel.min.js"></script>
	<script src="<%=request.getContextPath() %>/front-end/js/main.js"></script>
	<script src="https://code.jquery.com/jquery.js"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>>
	  

	
	</script>
	
		
</body>
</html>
