<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page import="com.adfund.model.*"%>

<%
  ADFundVO adfundVO = (ADFundVO) request.getAttribute("adfundVO"); //EmpServlet.java (Concroller) 存入req的empVO物件 (包括幫忙取出的empVO, 也包括輸入資料錯誤時的empVO物件)
%>
<jsp:useBean id="boardSvc" scope="page" class="com.board.model.BoardService" />

<html lang="">
<head>

<title>查看及修改廣告募資狀態</title>
<meta charset="UTF-8">
<meta name="description" content="Food Blog Web Template">
<meta name="keywords" content="food, unica, creative, html">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<!-- Favicon -->   
	<link href="img/favicon.ico" rel="shortcut icon"/>

	<!-- Google Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Poppins:400,400i,500,500i,600,600i,700" rel="stylesheet">

	<!-- Stylesheets -->
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/css/bootstrap.min.css"/>
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/css/font-awesome.min.css"/>
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/css/owl.carousel.css"/>
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/css/animate.css"/>
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/css/style.css"/>
	
	<!-- original -->			
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/adfund/css/bootstrap.min.css">
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/adfund/css/bootstrap-theme.min.css">
	
	<script src="<%=request.getContextPath() %>/front-end/adfund/js/jquery-3.3.1.min.js"></script>
	
	<script src="<%=request.getContextPath() %>/front-end/adfund/js/bootstrap.min.js"></script>

    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>



<style type="text/css">
body{
	background-color:white;
	font-family:\5FAE\8EDF\6B63\9ED1\9AD4,\65B0\7D30\660E\9AD4;
}
.checkbox-inline + .checkbox-inline, .radio-inline + .radio-inline{
	margin-left: 0;
}
.checkbox-inline, .radio-inline{
	margin-right: 1em;
}

#preview_progressbarTW_img1, #preview_progressbarTW_img2 {
	border: 1px solid #ddd;
	border-radius: 4px;
	padding: 2px;
	width: 384px;
	margin-top: 5px;
	margin-bottom: 20px;
}
#preview_progressbarTW_img1, #preview_progressbarTW_img2, #preview_progressbarTW_img3, #preview_progressbarTW_img4 {
	border: 1px solid #ddd;
	border-radius: 4px;
	padding: 2px;
	width: 384px;
	margin-top: 5px;
	margin-bottom: 20px;
}

#preview_progressbarTW_img1:hover, #preview_progressbarTW_img2:hover, #preview_progressbarTW_img3:hover, #preview_progressbarTW_img4:hover{
	box-shadow: 0 0 2px 1px rgba(0, 140, 186, 0.5);
}

.kk-ad-input{
	border: 1px solid #ddd;
	padding:10px;
    font-size:20px;
    margin-top:5px;
}

</style>

</head>
<body>

	<%-- 錯誤表列 --%>
	<c:if test="${not empty errorMsgs}">
		<font style="color: red">請修正以下錯誤</font>
		<ul>
			<c:forEach var="message" items="${errorMsgs}">
				<li style="color: red">${message}</li>
			</c:forEach>
		</ul>
	</c:if>

	<div class="container kk-ad-input">
		<div class="row">
			<div class="col-xs-12 col-sm-6 col-sm-offset-3">
				<form METHOD="post" ACTION="<%=request.getContextPath()%>/adfund/adfund.do" name="form1"
					enctype="multipart/form-data">

					<center>
						<div>
							<h3>
								<b>查看及修改廣告募資狀態</b>
							</h3>
						</div>
					</center>
					<br>



					<div class="form-group">
						<label>募資編號</label><br>
						<p><%=adfundVO.getAdfundID()%></p>
					</div>



					<div class="form-group">
						<label>會員編號</label>
						<p><%=adfundVO.getMemberID()%></p>
					</div>




					<div class="form-group">
						<label>藝人編號</label>
						<p>${boardSvc.getOneBoard(adfundVO.boardID).boardSignatureTitle}</p>
					</div>



					<div class="form-group">
						<label>管理者編號</label>
						<p><%=adfundVO.getAdministratorID()%></p>
					</div>


					<div class="form-group">
						<label>募資發起時間</label>
						<p><fmt:formatDate value="${adfundVO.adfundStartDate}" pattern="yyyy-MM-dd HH:mm"/></p>
					</div>


					<div class="form-group">
						<label>募資結束時間</label>
						<p><fmt:formatDate value="${adfundVO.adfundEndDate}" pattern="yyyy-MM-dd HH:mm"/></p>
					</div>


					<div class="form-group">
						<label>標題</label> 
						<p><%=adfundVO.getAdfundTitle()%></p>
					</div>

					<div class="form-group">
						<label>刊登日期與時間</label>
						<p><fmt:formatDate value="${adfundVO.adfundDisplayDate}" pattern="yyyy-MM-dd HH:mm"/></p>
					</div>

					<div class="form-group">
						<label>廣告類型</label>
						<p><%=adfundVO.getAdfundType()%></p>
					</div>

					
					<div class="from-group">
						<label>首頁圖片</label> 
						<center>
						    <img id="preview_progressbarTW_img3"
								src="<%=request.getContextPath()%>/adfundImg/adfund.do?adfundID=${adfundVO.adfundID}&adfundHomePhoto=adfundHomePhoto">
						    
							
						</center>
					</div>


					<div class="from-group">
						<label>藝人專板圖片</label> 
						<center>
						    <img id="preview_progressbarTW_img4"
								src="<%=request.getContextPath()%>/adfundImg/adfund.do?adfundID=${adfundVO.adfundID}&adfundBoardPhoto=adfundBoardPhoto">							
						</center>
					</div>

					<div class="from-group">
						<label>募資說明</label>
                        <p><%=adfundVO.getAdfundText()%></p>
					</div>


					<div class="from-group">
						<label>代幣目標量</label>
						<p><%=adfundVO.getAdfundCoinTarget()%></p>
					</div>



					<div class="form-group">
						<label>代幣現有量</label>
						<p><%=adfundVO.getAdfundCoinNow()%></p>
					</div>


					<div class="form-group">
						<label>募資提交時間</label>
						<p><fmt:formatDate value="${adfundVO.adfundSubmitDate}" pattern="yyyy-MM-dd HH:mm"/></p>
					</div>



					<div class="form-group">
						<label>募資狀態</label>
						<select class="form-control"
								required="required" name="adfundStatus">
								<option value="">-請選擇-</option>
								<option value="AD_PASSED">審核通過</option>
								<option value="AD_REJECTED">審核未通過</option>
						</select>
					</div>


					<br>
					<div>
						<input type="hidden" name="action" value="Update_For_Status"> 
						<input type="hidden" name="adfundID" value="<%=adfundVO.getAdfundID()%>">
						<input type="hidden" name="adfundStatus" value="<%=adfundVO.getAdfundStatus()%>">
						<input type="hidden" name="requestURL" value="<%=request.getParameter("requestURL")%>"> <!--接收原送出修改的來源網頁路徑後,再送給Controller準備轉交之用-->
                        <input type="hidden" name="whichPage"  value="<%=request.getParameter("whichPage")%>">  <!--只用於:istAllEmp.jsp-->
						<center>
							<input type="submit" class="btn btn-info" value="送出審核"> <a
								href="<%=request.getContextPath() %>/back-end/adfund/adfund_back_list.jsp"><input type="button" value="返回募資管理列表"
								class="btn btn-info"></a>
						</center>
				</FORM>
			</div>
			</form>
		</div>
	</div>
	</div>
	<!--====== Javascripts & Jquery ======-->
	<script src="<%=request.getContextPath() %>/front-end/js/jquery-3.2.1.min.js"></script>
	<script src="<%=request.getContextPath() %>/front-end/owl.carousel.min.js"></script>
	<script src="<%=request.getContextPath() %>/front-end/js/main.js"></script>		
    	<script src="https://code.jquery.com/jquery.js"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script type="text/javascript">

		//圖片
				$('#imgInp1').change(function() {
			var file = $('#imgInp1')[0].files[0];
			var reader = new FileReader;
			reader.onload = function(e) {
				$('#preview_progressbarTW_img1').attr('src', e.target.result);
			};
			reader.readAsDataURL(file);
		});


		$('#imgInp2').change(function() {
			var file = $('#imgInp2')[0].files[0];
			var reader = new FileReader;
			reader.onload = function(e) {
				$('#preview_progressbarTW_img2').attr('src', e.target.result);
			};
			reader.readAsDataURL(file);
		});
        //圖片



    </script>
</body>
<% 
   java.sql.Timestamp displayDate = null;
   try {
	     
	   displayDate = adfundVO.getAdfundDisplayDate();
 	  	  
    } catch (Exception e) {
 	   displayDate = new java.sql.Timestamp(System.currentTimeMillis());
    }
%>

<link rel="stylesheet" type="text/css"
	href="<%=request.getContextPath()%>/datetimepicker/jquery.datetimepicker.css" />
<script src="<%=request.getContextPath()%>/datetimepicker/jquery.js"></script>
<script
	src="<%=request.getContextPath()%>/datetimepicker/jquery.datetimepicker.full.js"></script>

<style>
.xdsoft_datetimepicker .xdsoft_datepicker {
	width: 300px; /* width:  300px; */
}

.xdsoft_datetimepicker .xdsoft_timepicker .xdsoft_time_box {
	height: 151px; /* height:  151px; */
}
</style>

<script>

/*DateTimePicker*/
$.datetimepicker.setLocale('zh'); // kr ko ja en
$(function(){
	 $('#displayDate').datetimepicker({
	  format:'Y-m-d H:i',
	  onShow:function(){
	   this.setOptions({
	    maxDate:$('#end_dateTime').val()?$('#end_dateTime').val():false
	   })
	  },
	  timepicker:true,
	  step: 1
	 });	 
});

/*圖片*/



	   $('#imgInp1').change(function() {
			var file = $('#imgInp1')[0].files[0];
			var reader = new FileReader;
			reader.onload = function(e) {
				$('#preview_progressbarTW_img1').attr('src', e.target.result);
			};
			reader.readAsDataURL(file);
		});


		$('#imgInp2').change(function() {
			var file = $('#imgInp2')[0].files[0];
			var reader = new FileReader;
			reader.onload = function(e) {
				$('#preview_progressbarTW_img2').attr('src', e.target.result);
			};
			reader.readAsDataURL(file);
		});

</script>
</html>