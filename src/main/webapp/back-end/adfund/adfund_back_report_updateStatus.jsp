<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page import="com.adfund.model.*"%>
<%@ page import="com.adfundReport.model.*"%>

<%
  ADFundReportVO adfundReportVO = (ADFundReportVO) request.getAttribute("adfundReportVO"); //EmpServlet.java (Concroller) 存入req的empVO物件 (包括幫忙取出的empVO, 也包括輸入資料錯誤時的empVO物件)
%>


<html lang="">
<head>

<title>查看及修改廣告募資狀態</title>
<meta charset="UTF-8">
<meta name="description" content="Food Blog Web Template">
<meta name="keywords" content="food, unica, creative, html">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<!-- Favicon -->   
	<link href="img/favicon.ico" rel="shortcut icon"/>

	<!-- Google Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Poppins:400,400i,500,500i,600,600i,700" rel="stylesheet">

	<!-- Stylesheets -->
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/css/bootstrap.min.css"/>
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/css/font-awesome.min.css"/>
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/css/owl.carousel.css"/>
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/css/animate.css"/>
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/css/style.css"/>
	
	<!-- original -->			
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/adfund/css/bootstrap.min.css">
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/adfund/css/bootstrap-theme.min.css">
	
	<script src="<%=request.getContextPath() %>/front-end/adfund/js/jquery-3.3.1.min.js"></script>
	
	<script src="<%=request.getContextPath() %>/front-end/adfund/js/bootstrap.min.js"></script>

    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<style type="text/css">
body{
	background-color:white;
	font-family:\5FAE\8EDF\6B63\9ED1\9AD4,\65B0\7D30\660E\9AD4;
}
.checkbox-inline + .checkbox-inline, .radio-inline + .radio-inline{
	margin-left: 0;
}
.checkbox-inline, .radio-inline{
	margin-right: 1em;
}

.kk-ad-input{
	border: 1px solid #ddd;
	padding:10px;
    font-size:20px;
    margin-top:5px;
}

</style>
</head>
<body>

	<%-- 錯誤表列 --%>
	<c:if test="${not empty errorMsgs}">
		<font style="color: red">請修正以下錯誤</font>
		<ul>
			<c:forEach var="message" items="${errorMsgs}">
				<li style="color: red">${message}</li>
			</c:forEach>
		</ul>
	</c:if>

	<div class="container kk-ad-input">
		<div class="row">
			<div class="col-xs-12 col-sm-6 col-sm-offset-3">
				<form METHOD="post" ACTION="<%=request.getContextPath()%>/adfundReport/adfundReport.do" name="form1"
					enctype="multipart/form-data">

					<center>
						<div>
							<h3>
								<b>查看及修改廣告募資狀態</b>
							</h3>
						</div>
					</center>
					<br>



					<div class="form-group">
						<label>廣告募資檢舉編號</label><br>
						<p><%=adfundReportVO.getAdfundReportID()%></p>
					</div>
					
					
                    <div class="form-group">
						<label>廣告募資編號</label>
						<p><%=adfundReportVO.getAdfundID()%></p>
					</div>


					<div class="form-group">
						<label>會員編號</label>
						<p><%=adfundReportVO.getMemberID()%></p>
					</div>




					<div class="form-group">
						<label>管理者編號</label>
						<p><%=adfundReportVO.getAdministratorID()%></p>
					</div>



					<div class="form-group">
						<label>檢舉時間</label>
						<p><fmt:formatDate value="${adfundReportVO.adfundReportDate}" pattern="yyyy-MM-dd HH:mm"/></p>
					</div>


					<div class="form-group">
						<label>檢舉事由</label>
						<p><%=adfundReportVO.getAdfundReportReason()%></p>
					</div>



					<div class="form-group">
						<label>檢舉內容</label>
						<p><%=adfundReportVO.getAdfundReportContent()%></p>
					</div>



					<div class="form-group">
						<label>檢舉審核狀態</label>
						<select class="form-control" required="required" name="adfundReportStatus">
								<option value="">-請選擇-</option>
								<option value="REPORT_ACCEPTED">審核通過</option>
								<option value="REPORT_REJECTED">審核未通過</option>
						</select>
					</div>


					<br>
					
						<input type="hidden" name="action" value="update"> 
						<input type="hidden" name="adfundReportID" value="<%=adfundReportVO.getAdfundReportID()%>">
						<input type="hidden" name="adfundID" value="<%=adfundReportVO.getAdfundID()%>">
						<input type="hidden" name="adfundReportStatus" value="<%=adfundReportVO.getAdfundReportStatus()%>">
						<input type="hidden" name="requestURL" value="<%=request.getParameter("requestURL")%>"> <!--接收原送出修改的來源網頁路徑後,再送給Controller準備轉交之用-->
                        <input type="hidden" name="whichPage"  value="<%=request.getParameter("whichPage")%>">  <!--只用於:istAllEmp.jsp-->
						<center>
							<input type="submit" class="btn btn-info" value="送出審核"> <a
								href="<%=request.getContextPath() %>/back-end/adfund/adfund_back_list.jsp"><input type="button" value="返回募資管理列表"
								class="btn btn-info"></a>
						</center>
				</FORM>
			</div>
			
		</div>
	</div>
	
    	<!--====== Javascripts & Jquery ======-->
	<script src="<%=request.getContextPath() %>/front-end/js/jquery-3.2.1.min.js"></script>
	<script src="<%=request.getContextPath() %>/front-end/owl.carousel.min.js"></script>
	<script src="<%=request.getContextPath() %>/front-end/js/main.js"></script>		
    	<script src="https://code.jquery.com/jquery.js"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>

</body>
</html>