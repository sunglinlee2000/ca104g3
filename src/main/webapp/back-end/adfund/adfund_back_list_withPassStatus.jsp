<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page import="java.util.*"%>
<%@ page import="com.adfund.model.*"%>
<%@ page import="com.adfundReport.model.*"%>

<%
    ADFundService adfundSvc = new ADFundService();
    List<ADFundVO> list = adfundSvc.getAllWithPassStatus();
    pageContext.setAttribute("list",list);
%>

<html lang="">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
<title>廣告募資管理列表</title>
<meta charset="UTF-8">
<meta name="description" content="Food Blog Web Template">
<meta name="keywords" content="food, unica, creative, html">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<!-- Favicon -->   
	<link href="img/favicon.ico" rel="shortcut icon"/>

	<!-- Google Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Poppins:400,400i,500,500i,600,600i,700" rel="stylesheet">

	<!-- Stylesheets -->
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/css/bootstrap.min.css"/>
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/css/font-awesome.min.css"/>
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/css/owl.carousel.css"/>
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/css/animate.css"/>
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/css/style.css"/>
	
	<!-- original -->			
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/adfund/css/bootstrap.min.css">
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/adfund/css/bootstrap-theme.min.css">
	
	<script src="<%=request.getContextPath() %>/front-end/adfund/js/jquery-3.3.1.min.js"></script>
	
	<script src="<%=request.getContextPath() %>/front-end/adfund/js/bootstrap.min.js"></script>
	
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/adfund/css/index.css">


<style type="text/css">
body{
    	font-family:\5FAE\8EDF\6B63\9ED1\9AD4,\65B0\7D30\660E\9AD4;
}
.kk-adfund-table {
	margin-top: 20px;
	text-align: center;
	font-size: 13px;
}

.kk-adfund-table thead th {
	margin-top: 20px;
	text-align: center;
	background-color: #DCDCDC;
}

.btn-adfund-back {
	background-color: #9999cc;
	border: 1px solid;
	border-color: #DCDCDC;
	padding: 3px;
	color: white;
	font-size: 11px;
}
</style>

</head>
<body>
<!-- Header section -->
	<header class="header-section">
		
		<div class="header-bottom">
			<div class="container">
			
	<!-- 返回後台首頁							 -->
				<a href="<%=request.getContextPath()%>/back-end/administrator.jsp">
					<button style="background-color:white;border: none;">
						<img  src="<%=request.getContextPath() %>/front-end/img/logo-1.png">
					</button>
				</a>
	<!-- 返回後台首頁end							 -->			
				
				<div class="nav-switch">
					<i class="fa fa-bars"></i>
				</div>
				
				<ul class="main-menu">
			
					<li>Super Administrator，你好</li>
					<li>
						<i><a href="<%=request.getContextPath()%>/back-end/login.jsp"  onclick="logout()"><img src="<%=request.getContextPath() %>/back-end/img/signout_106525.png"></a></i>						
					</li>
				</ul>
			</div>
		</div>
	</header>
<!-- Header section end -->

	<%-- 錯誤表列 --%>
	<c:if test="${not empty errorMsgs}">
		<font style="color: red">請修正以下錯誤:</font>
		<ul>
			<c:forEach var="message" items="${errorMsgs}">
				<li style="color: red">${message}</li>
			</c:forEach>
		</ul>
	</c:if>


	<div class="container-fiuld kk-ad-back-con">
		<div class="row">
		   <div class="col-xs-12 col-sm-2 col-sm-offset-1">
					     
                

				<div class="list-group kk-ad-list-group">
					<a href="<%=request.getContextPath() %>/back-end/adfund/adfund_back_list.jsp" class="list-group-item ">全部</a> 
					<a href="<%=request.getContextPath() %>/back-end/adfund/adfund_back_list_withStatus.jsp" class="list-group-item">募資中</a> 
					<a href="<%=request.getContextPath() %>/back-end/adfund/adfund_back_list_withNotFinishStatus.jsp" class="list-group-item">募資未達標</a>
					<a href="<%=request.getContextPath() %>/back-end/adfund/adfund_back_list_withSubmitStatus.jsp" class="list-group-item">已提交未審查</a>
					<a href="<%=request.getContextPath() %>/back-end/adfund/adfund_back_list_withPassStatus.jsp" class="list-group-item">審核通過</a> 
					<a href="<%=request.getContextPath() %>/back-end/adfund/adfund_back_list_withUnpassStatus.jsp" class="list-group-item">審核未通過</a>
				</div>
			</div>
		
		 
			<div class="col-xs-12 col-sm-8 ">

				<h2>廣告募資管理</h2>

				<table
					class="table table-hover table-striped table-condensed table-bordered kk-adfund-table">


					<%@ include file="page1.file"%>
					<thead class="kk-adfund-thead">
						<tr>
							<th>募資編號</th>
							<th>募資標題</th>
							<th>刊登日期</th>
							<th>募資狀態</th>
							<th>查看詳情及審查</th>
							<th>查看參與者</th>
							<th>查看檢舉</th>
							<th>刪除</th>
						</tr>
					</thead>

					<tbody>
						<c:forEach var="adfundVO" items="${list}" begin="<%=pageIndex%>"
							end="<%=pageIndex+rowsPerPage-1%>">
							<tr ${(adfundVO.adfundID==param.adfundID) ? 'bgcolor=#CCCCFF':''}><!--將修改的那一筆加入對比色而已-->
								<td>${adfundVO.adfundID}</td>
								<td>${adfundVO.adfundTitle}</td>
								<td><fmt:formatDate value="${adfundVO.adfundDisplayDate}" pattern="yyyy-MM-dd HH:mm" /></td>
								<td>${adfundVO.adfundStatus}</td>
								<td>
									<FORM METHOD="post" ACTION="<%=request.getContextPath()%>/adfund/adfund.do">
									     <input type="submit" value="查看詳情及審查" class="btn btn-adfund-back"> 
									     <input type="hidden" name="adfundID"      value="${adfundVO.adfundID}">
									     <input type="hidden" name="requestURL"	value="<%=request.getServletPath()%>"><!--送出本網頁的路徑給Controller--><!-- 目前尚未用到  -->
									     <input type="hidden" name="whichPage"	value="<%=whichPage%>">               <!--送出本網頁的路徑給Controller--><!-- 目前尚未用到  -->
									     <input type="hidden" name="action"	    value="getBackOne_For_Update"></FORM>
								</td>
								<td>
									<FORM METHOD="post" ACTION="<%=request.getContextPath()%>/adfund/adfund.do" style="margin-bottom: 0px;">
									    <input type="submit" value="查看參與者" class="btn btn-adfund-back"> 
									    <input type="hidden" name="adfundID" value="${adfundVO.adfundID}">
									    <input type="hidden" name="requestURL"	value="<%=request.getServletPath()%>"><!--送出本網頁的路徑給Controller-->
			                            <input type="hidden" name="whichPage"	value="<%=whichPage%>">               <!--送出當前是第幾頁給Controller-->
									    <input type="hidden" name="action" value="listEmps_ByDeptno_A"></FORM>
									</td>
								<td>
									<FORM METHOD="post" ACTION="<%=request.getContextPath()%>/adfund/adfund.do" style="margin-bottom: 0px;">
									    <input type="submit" value="查看檢舉" class="btn btn-adfund-back"> 
									    <input type="hidden" name="adfundID" value="${adfundVO.adfundID}">
									    <input type="hidden" name="requestURL"	value="<%=request.getServletPath()%>"><!--送出本網頁的路徑給Controller-->
			                            <input type="hidden" name="whichPage"	value="<%=whichPage%>">               <!--送出當前是第幾頁給Controller-->
									    <input type="hidden" name="action" value="listEmps_ByDeptno_B"></FORM>
									</td>
								<td>
								   <FORM METHOD="post" ACTION="<%=request.getContextPath()%>/adfund/adfund.do">
										<input type="submit" value="刪除" class="btn btn-adfund-back"> 
										<input type="hidden" name="adfundID" value="${adfundVO.adfundID}"> 										
										<input type="hidden" name="requestURL" value="<%=request.getServletPath()%>">
										<input type="hidden" name="whichPage"	value="<%=whichPage%>">               <!--送出當前是第幾頁給Controller-->
										<input type="hidden" name="whichPage"	value="<%=whichPage%>"><!--送出本網頁的路徑給Controller-->
										<input type="hidden" name="action" value="delete">
									</FORM>
									</td>
							</tr>
					</tbody>
					</c:forEach>
				</table>
				<center><%@ include file="page2.file"%></center>
				
					<%if (request.getAttribute("adfund_back_report_joinlist")!=null){%>
				         <jsp:include page="adfund_back_report_joinlist.jsp"></jsp:include>
				     <%} %>
				     
				     <%if (request.getAttribute("adfund_coin_joinlist")!=null){%>
				         <jsp:include page="adfund_coin_joinlist.jsp"></jsp:include>
				     <%} %>
				
			</div>
		</div>
	</div>



	<script src="https://code.jquery.com/jquery.js"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
</body>
</html>
