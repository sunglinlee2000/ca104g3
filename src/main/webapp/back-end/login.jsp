<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="java.util.*"%>
<%@ page import="com.issue.model.*"%>

<%
  response.setHeader("Cache-Control","no-store"); //HTTP 1.1
  response.setHeader("Pragma","no-cache");        //HTTP 1.0
  response.setDateHeader ("Expires", 0);
%>

<%
  IssueVO issueVO = (IssueVO) request.getAttribute("issueVO");
%>

<html lang="">
<head>
  <meta charset="UTF-8">
  <link rel="mask-icon" type="" href="https://static.codepen.io/assets/favicon/logo-pin-f2d2b6d2c61838f7e76325261b7195c27224080bc099486ddd6dccb469b8e8e6.svg" color="#111" />
  <title>管理員登入</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">

  
<style>

body{
    	font-family:\5FAE\8EDF\6B63\9ED1\9AD4,\65B0\7D30\660E\9AD4;
}

*, *:before, *:after {
  margin: 0;
  padding: 0;
  box-sizing: border-box;
}

body {
  color: #999;
  padding: 20px;
  display: flex;
  min-height: 100vh;
  align-items: center;
  font-family: 'Raleway';
  justify-content: center;
  background-color: #fbfbfb;
}

#mainButton {
  color: white;
  border: none;
  outline: none;
  font-size: 24px;
  font-weight: 200;
  overflow: hidden;
  position: relative;
  border-radius: 2px;
  letter-spacing: 2px;
  box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12), 0 1px 2px rgba(0, 0, 0, 0.24);
  text-transform: uppercase;
  background-color: #0d8fcc;
  -webkit-transition: all 0.2s ease-in;
  -moz-transition: all 0.2s ease-in;
  -ms-transition: all 0.2s ease-in;
  -o-transition: all 0.2s ease-in;
  transition: all 0.2s ease-in;
}
#mainButton .btn-text {
  z-index: 2;
  display: block;
  padding: 10px 20px;
  position: relative;
}
#mainButton .btn-text:hover {
  cursor: pointer;
}
#mainButton:after {
  top: -50%;
  z-index: 1;
  content: '';
  width: 150%;
  height: 200%;
  position: absolute;
  left: calc(-150% - 40px);
  background-color: rgba(255, 255, 255, 0.2);
  -webkit-transform: skewX(-40deg);
  -moz-transform: skewX(-40deg);
  -ms-transform: skewX(-40deg);
  -o-transform: skewX(-40deg);
  transform: skewX(-40deg);
  -webkit-transition: all 0.2s ease-out;
  -moz-transition: all 0.2s ease-out;
  -ms-transition: all 0.2s ease-out;
  -o-transition: all 0.2s ease-out;
  transition: all 0.2s ease-out;
}
#mainButton:hover {
  cursor: default;
  box-shadow: 0 10px 20px rgba(0, 0, 0, 0.19), 0 6px 6px rgba(0, 0, 0, 0.23);
}
#mainButton:hover:after {
  -webkit-transform: translateX(100%) skewX(-30deg);
  -moz-transform: translateX(100%) skewX(-30deg);
  -ms-transform: translateX(100%) skewX(-30deg);
  -o-transform: translateX(100%) skewX(-30deg);
  transform: translateX(100%) skewX(-30deg);
}
#mainButton.active {
  box-shadow: 0 19px 38px rgba(0, 0, 0, 0.3), 0 15px 12px rgba(0, 0, 0, 0.22);
}
#mainButton.active .modal {
  -webkit-transform: scale(1, 1);
  -moz-transform: scale(1, 1);
  -ms-transform: scale(1, 1);
  -o-transform: scale(1, 1);
  transform: scale(1, 1);
}
#mainButton .modal {
  top: 0;
  left: 0;
  z-index: 3;
  width: 100%;
  height: 100%;
  padding: 20px;
  display: flex;
  position: fixed;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  background-color: inherit;
  transform-origin: center center;
  background-image: linear-gradient(to top left, #0d8fcc 10%, #7dccff 65%, white 200%);
  -webkit-transform: scale(0.000001, 0.00001);
  -moz-transform: scale(0.000001, 0.00001);
  -ms-transform: scale(0.000001, 0.00001);
  -o-transform: scale(0.000001, 0.00001);
  transform: scale(0.000001, 0.00001);
  -webkit-transition: all 0.2s ease-in;
  -moz-transition: all 0.2s ease-in;
  -ms-transition: all 0.2s ease-in;
  -o-transition: all 0.2s ease-in;
  transition: all 0.2s ease-in;
}

.close-button {
  top: 20px;
  right: 20px;
  position: absolute;
  -webkit-transition: opacity 0.2s ease-in;
  -moz-transition: opacity 0.2s ease-in;
  -ms-transition: opacity 0.2s ease-in;
  -o-transition: opacity 0.2s ease-in;
  transition: opacity 0.2s ease-in;
}
.close-button:hover {
  opacity: 0.5;
  cursor: pointer;
}

.form-title {
  margin-bottom: 15px;
}

.form-button {
  width: 100%;
  padding: 10px;
  color: #0d8fcc;
  margin-top: 10px;
  max-width: 400px;
  text-align: center;
  border: solid 1px white;
  background-color: white;
  -webkit-transition: color 0.2s ease-in, background-color 0.2s ease-in;
  -moz-transition: color 0.2s ease-in, background-color 0.2s ease-in;
  -ms-transition: color 0.2s ease-in, background-color 0.2s ease-in;
  -o-transition: color 0.2s ease-in, background-color 0.2s ease-in;
  transition: color 0.2s ease-in, background-color 0.2s ease-in;
}
.form-button:hover {
  color: white;
  cursor: pointer;
  background-color: transparent;
}

.input-group {
  width: 100%;
  font-size: 16px;
  max-width: 400px;
  padding-top: 20px;
  position: relative;
  margin-bottom: 15px;
}
.input-group input {
  width: 100%;
  color: white;
  border: none;
  outline: none;
  padding: 5px 0;
  line-height: 1;
  font-size: 16px;
  font-family: 'Raleway';
  border-bottom: solid 1px white;
  background-color: transparent;
  -webkit-transition: box-shadow 0.2s ease-in;
  -moz-transition: box-shadow 0.2s ease-in;
  -ms-transition: box-shadow 0.2s ease-in;
  -o-transition: box-shadow 0.2s ease-in;
  transition: box-shadow 0.2s ease-in;
}
.input-group input + label {
  left: 0;
  top: 20px;
  position: absolute;
  pointer-events: none;
  -webkit-transition: all 0.2s ease-in;
  -moz-transition: all 0.2s ease-in;
  -ms-transition: all 0.2s ease-in;
  -o-transition: all 0.2s ease-in;
  transition: all 0.2s ease-in;
}
.input-group input:focus {
  box-shadow: 0 1px 0 0 white;
}
.input-group input:focus + label, .input-group input.active + label {
  font-size: 12px;
  -webkit-transform: translateY(-20px);
  -moz-transform: translateY(-20px);
  -ms-transform: translateY(-20px);
  -o-transform: translateY(-20px);
  transform: translateY(-20px);
}

.codepen-by {
  left: 0;
  bottom: 0;
  width: 100%;
  padding: 10px;
  font-size: 16px;
  position: absolute;
  text-align: center;
  text-transform: none;
  letter-spacing: initial;
}

.button {
  display: inline-block;
  border-radius: 4px;
  background-color: #0080ff;
  border: none;
  color: #FFFFFF;
  text-align: center;
  font-size: 20px;
  padding: 20px;
  width: 200px;
  transition: all 0.5s;
  cursor: pointer;
  margin: 5px;
}

.button span {
  cursor: pointer;
  display: inline-block;
  position: relative;
  transition: 0.5s;
}

.button span:after {
  content: '\00bb';
  position: absolute;
  opacity: 0;
  top: 0;
  right: -20px;
  transition: 0.5s;
}

.button:hover span {
  padding-right: 25px;
}

.button:hover span:after {
  opacity: 1;
  right: 0;
}

input[type=text], select {
    width: 100%;
    padding: 12px 20px;
    margin: 8px 0;
    display: inline-block;
    border: 1px solid #ccc;
    border-radius: 4px;
    box-sizing: border-box;
}

input[type=password], select {
    width: 100%;
    padding: 12px 20px;
    margin: 8px 0;
    display: inline-block;
    border: 1px solid #ccc;
    border-radius: 4px;
    box-sizing: border-box;
}
</style>

  <script>
  window.console = window.console || function(t) {};
  </script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/prefixfree/1.0.7/prefixfree.min.js"></script>

  
  <script>
  if (document.location.search.match(/type=embed/gi)) {
    window.parent.postMessage("resize", "*");
  }
  </script>


</head>

<body translate="no" >

		
<div class="container">
     <div class="row">
        <div class="col-xs-12 col-sm-12">
        	<div class="col-md-12 col-sm-12 col-xs-12" style="max-width:">
				<div class="header-bottom">
					<div class="container" style="height:200px;">
						<center>
							<img src="<%=request.getContextPath() %>/back-end/img/logo-1.png" style="width:600px;height:220px;">
						</center>				
					</div>
				</div>    		
        	</div>
	        <div class="col-md-12 col-sm-12 col-xs-12">
			<link href='https://fonts.googleapis.com/css?family=Raleway:400,500,300' rel='stylesheet' type='text/css'>
				<div id="mainButton">
				  <div class="btn-text" onclick="openForm()"><h1>Administrator-Login</h1></div>
				  <div class="modal">
				    <div class="close-button" onclick="closeForm()">x</div>
				    <div class="form-title"><h1>FANPOOL-Administrator-Login</h1></div>
				    
	
									    
	
	
                                       <form method="post" action="<%=request.getContextPath() %>/administrator/administrator.do" enctype="multipart/form=data">
                                            
                                            <div class="form-group">
                                                <label><h3>帳號&nbsp</h3></span></label> 
												<input type="text" class="form-control" name="administratorAccount" id="admin_ac">
                                            </div>
                                                                                      
                                            <div class="form-group">
                                                <label><h3>密碼&nbsp</h3></label>  
												<input type="password" class="form-control" name="administratorPassword" id="admin_pw">
	                                        </div>
	                                        
	                                        <div>
                                           		<button type="submit" class="button" id="administratorLogin"><h4>Login</h4></button>
												<input type="hidden" name="action" value="check_Administrator" >&nbsp;
												<button type="button" class="button" onclick="get_admin()"><h4>Super-Login</h4></button>
											</div>											
                                        </form>          									
				    <div class="codepen-by"><h3>FANPOOL-Administrator-Login</h3></div>
				  </div> 
				</div>
				<div class="codepen-by"><h3>FANPOOL-Administrator-Login</h3></div>
			</div>
		</div>
	</div>
							<script type="text/javascript">
							function get_admin(){
								var admin_ac = document.getElementById("admin_ac").value="ca104g3";
								var admin_pw = document.getElementById("admin_pw").value="123456";
							}
							
							</script>	
</div>			
    <script src="//static.codepen.io/assets/common/stopExecutionOnTimeout-41c52890748cd7143004e05d3c5f786c66b19939c4500ce446314d1748483e13.js"></script>

  
  

    <script >
      var button = document.getElementById('mainButton');

var openForm = function() {
  button.className = 'active';
};

var checkInput = function(input) {
  if (input.value.length > 0) {
    input.className = 'active';
  } else {
    input.className = '';
  }
};

var closeForm = function() {
  button.className = '';
};

document.addEventListener("keyup", function(e) {
  if (e.keyCode == 27 || e.keyCode == 13) {
    closeForm();
  }
});
      //# sourceURL=pen.js
    </script>



  
  

  <script src="https://static.codepen.io/assets/editor/live/css_reload-2a5c7ad0fe826f66e054c6020c99c1e1c63210256b6ba07eb41d7a4cb0d0adab.js"></script>
</body>

</html>