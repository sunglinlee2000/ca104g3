<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page import="java.util.*"%>
<%@ page import="com.groupReport.model.*"%>
<%@ page import="com.groupList.model.*"%>


<%
    GroupReportService groupReportSvc = new GroupReportService();
	List<GroupReportVO> list = groupReportSvc.getAll_unchecked();
	pageContext.setAttribute("list", list);	
	response.setHeader("Cache-Control", "no-store");
	response.setHeader("Pragma", "no-cache");
	response.setDateHeader("Expires", 0);
%>

<jsp:useBean id="groupListSvc" scope="page" class="com.groupList.model.GroupListService" />

<html lang="">
<head>
<title>揪團檢舉列表</title>
<meta charset="UTF-8">
<meta name="description" content="Food Blog Web Template">
<meta name="keywords" content="food, unica, creative, html">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<!-- Favicon -->   
	<link href="img/favicon.ico" rel="shortcut icon"/>

	<!-- Google Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Poppins:400,400i,500,500i,600,600i,700" rel="stylesheet">

	<!-- Stylesheets -->
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/css/bootstrap.min.css"/>
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/css/font-awesome.min.css"/>
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/css/owl.carousel.css"/>
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/css/animate.css"/>
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/css/style.css"/>
	
	<!-- original -->			
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/adfund/css/bootstrap.min.css">
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/adfund/css/bootstrap-theme.min.css">
	
	<script src="<%=request.getContextPath() %>/front-end/adfund/js/jquery-3.3.1.min.js"></script>
	
	<script src="<%=request.getContextPath() %>/front-end/adfund/js/bootstrap.min.js"></script>
	
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/adfund/css/index.css">
	
	<!--====== Javascripts & Jquery ======-->
	<script src="<%=request.getContextPath() %>/front-end/js/jquery-3.2.1.min.js"></script>
	<script src="<%=request.getContextPath() %>/front-end/js/owl.carousel.min.js"></script>
	<script src="<%=request.getContextPath() %>/front-end/js/main.js"></script>
	<script src="https://code.jquery.com/jquery.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>


<style type="text/css">
body{
    font-family:\5FAE\8EDF\6B63\9ED1\9AD4,\65B0\7D30\660E\9AD4;
}

.kk-adfund-table {
	margin-top: 20px;
	text-align: center;
	font-size: 13px;
}

.kk-adfund-table thead th {
	margin-top: 20px;
	text-align: center;
	background-color: #DCDCDC;
}

.btn-adfund-back {
	background-color: #9999cc;
	border: 1px solid;
	border-color: #DCDCDC;
	padding: 3px;
	color: white;
	font-size: 11px;
}
</style>

</head>
<body>

        <!-- Page Preloder -->
	<div id="preloder">
		<div class="loader"></div>
	</div>

<!-- Header section -->
	<header class="header-section">
		
		<div class="header-bottom">
			<div class="container">
			
	<!-- 返回後台首頁							 -->
				<a href="<%=request.getContextPath()%>/back-end/administrator.jsp">
					<button style="background-color:white;border: none;">
						<img  src="<%=request.getContextPath() %>/front-end/img/logo-1.png">
					</button>
				</a>
	<!-- 返回後台首頁end							 -->			
				
				<div class="nav-switch">
					<i class="fa fa-bars"></i>
				</div>
				
				<ul class="main-menu">
			
					<li>Super Administrator，你好</li>
					<li>
						<i><a href="<%=request.getContextPath()%>/back-end/login.jsp"  onclick="logout()"><img src="<%=request.getContextPath() %>/back-end/img/signout_106525.png"></a></i>						
					</li>
				</ul>
			</div>
		</div>
	</header>
<!-- Header section end -->


	<%-- 錯誤表列 --%>
	<c:if test="${not empty errorMsgs}">
		<font style="color: red">請修正以下錯誤:</font>
		<ul>
			<c:forEach var="message" items="${errorMsgs}">
				<li style="color: red">${message}</li>
			</c:forEach>
		</ul>
	</c:if>


	<div class="container-fiuld kk-ad-back-con">
		<div class="row">
		   <div class="col-xs-12 col-sm-2 col-sm-offset-1">
					     
                

				<div class="list-group kk-ad-list-group">
					<a href="<%=request.getContextPath() %>/back-end/groupList/groupList_back_unchecked.jsp" 
						class="list-group-item">已檢舉未審查</a>
					<a href="<%=request.getContextPath() %>/back-end/groupList/groupList_back_accepted.jsp" 
						class="list-group-item">檢舉通過</a> 
					<a href="<%=request.getContextPath() %>/back-end/groupList/groupList_back_rejected.jsp" 
						class="list-group-item">檢舉駁回</a>
				</div>
			</div>
		
		 
			<div class="col-xs-12 col-sm-8 ">

				<h2>揪團檢舉管理</h2>

				<table class="table table-hover table-striped table-condensed table-bordered kk-adfund-table">


					<%@ include file="page1.file"%>
					<thead class="kk-adfund-thead">
						<tr>
							<th>揪團編號</th>
							<th>專板編號</th>
							<th>揪團名稱</th>
							<th>查看檢舉</th>
							<th>揪團詳情</th>
							<th>檢舉通過</th>
							<th>檢舉駁回</th>
						</tr>
					</thead>

					<tbody>
						<c:forEach var="groupReportVO" items="${list}" begin="<%=pageIndex%>"
							end="<%=pageIndex+rowsPerPage-1%>">
							<tr ${(groupReportVO.groupID==param.groupID) ? 'bgcolor=#CCCCFF':''}><!--將修改的那一筆加入對比色而已-->
								<td>${groupReportVO.groupID}</td>
								<td>${groupListSvc.getOneGroup(groupReportVO.groupID).boardID}</td>
								<td>${groupListSvc.getOneGroup(groupReportVO.groupID).groupTitle}</td>
								<td>
									<div class="btn btn-adfund-back" onclick="showGroupReport('${groupReportVO.groupReportID}');">
										查看檢舉
									</div>
								
								</td>
								<td>
									<FORM METHOD="post" ACTION="<%=request.getContextPath()%>/groupList/groupList.do">
									
									     <input type="submit" value="查看揪團詳情" class="btn btn-adfund-back" > 
									     <input type="hidden" name="groupID" value="${groupReportVO.groupID}">
									     <input type="hidden" name="boardID" value="${groupListSvc.getOneGroup(groupReportVO.groupID).boardID}">
									     <input type="hidden" name="requestURL"	value="<%=request.getServletPath()%>"><!--送出本網頁的路徑給Controller--><!-- 目前尚未用到  -->
									     <input type="hidden" name="whichPage"	value="<%=whichPage%>">               <!--送出本網頁的路徑給Controller--><!-- 目前尚未用到  -->
									     <input type="hidden" name="action"	    value="getOne_For_Display">
									</FORM>
								</td>
								<td>
									<div class="btn btn-adfund-back" onclick="reportAccept('${groupReportVO.groupReportID}', 
										'${groupReportVO.groupID}');">檢舉通過
									</div>
									</td>
								<td>
								   <div class="btn btn-adfund-back" onclick="reportReject('${groupReportVO.groupReportID}', 
								   		'${groupReportVO.groupID}');">檢舉駁回
									</div>
								</td>
							</tr>
					</tbody>
					</c:forEach>
				</table>
				<center><%@ include file="page2.file"%></center>
		
				<table class="table table-hover table-striped table-condensed table-bordered kk-adfund-table table-hide">
					<thead class="kk-adfund-thead">
						<tr>
							<th>揪團編號</th>
							<th>檢舉人</th>
							<th>檢舉事由</th>
							<th>檢舉內容</th>
							<th>檢舉時間</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td id="tr-groupID">揪團編號</td>
							<td id="tr-memberID">檢舉人</td>
							<td id="tr-groupReportReason">檢舉事由</td>
							<td id="tr-groupReportContent">檢舉內容</td>
							<td id="tr-groupReportDate">檢舉時間</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>



</body>
<script type="text/javascript">

	$(document).ready(function(e) {
		$('.table-hide').hide();
	})
	
// 	=============================秀檢舉詳情==========================
	
	function showGroupReport(groupReportIDStr) {
		var groupReportID = groupReportIDStr;
		console.log(groupReportID);
		
		$.ajax({
			type: "POST",
			url: "<%=request.getContextPath()%>/groupReport/groupReport.do",
			data: {
				action: "showGroupReport",
				groupReportID: groupReportID,
			}
		}).done(function(res) {
			var obj = JSON.parse(res);
			var groupID = obj.groupID;
			var memberID = obj.memberID;
			var groupReportReason = obj.groupReportReason;
			if (obj.groupReportContent == null) {
				var groupReportContent = "無";
			}else {
				var groupReportContent = obj.groupReportContent;
			}
			var groupReportDate = obj.groupReportDate;
			
			$('#tr-groupID').html(groupID);
			$('#tr-memberID').html(memberID);
			$('#tr-groupReportReason').html(groupReportReason);
			$('#tr-groupReportContent').html(groupReportContent);
			$('#tr-groupReportDate').html(groupReportDate);
		});
		
		$('.table-hide').show();
	}
	
// 	=========================檢舉通過=======================
	
	function reportAccept(groupReportIDStr, groupIDStr) {
		var groupReportID = groupReportIDStr;
		var groupID = groupIDStr;
		console.log(groupReportID);
		console.log(groupID);
		
		$.ajax({
			type: "POST",
			url: "<%=request.getContextPath()%>/groupReport/groupReport.do",
			data: {
				action: "reportAccept",
				groupReportID: groupReportID,
				groupID: groupID,
				groupReportStatus: "REPORT_ACCEPTED"
			}
		}).done(function(res) {
// 			var obj = JSON.parse(res);
			var href = '<%=request.getContextPath()%>/back-end/groupList/groupList_back_unchecked.jsp';
			window.location.href = href;
		});
		
	}
	
// 	=========================檢舉駁回=======================
	
	function reportReject(groupReportIDStr, groupIDStr) {
		var groupReportID = groupReportIDStr;
		var groupID = groupIDStr;
		console.log(groupReportID);
		console.log(groupID);
		
		$.ajax({
			type: "POST",
			url: "<%=request.getContextPath()%>/groupReport/groupReport.do",
			data: {
				action: "reportReject",
				groupReportID: groupReportID,
				groupID: groupID,
				groupReportStatus: "REPORT_REJECTED"
			}
		}).done(function(res) {
// 			var obj = JSON.parse(res);
			var href = '<%=request.getContextPath()%>/back-end/groupList/groupList_back_unchecked.jsp';
			window.location.href = href;
		});

	}
	
</script>
</html>
