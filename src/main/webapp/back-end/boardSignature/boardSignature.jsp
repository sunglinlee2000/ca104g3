<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="java.util.*"%>
<%@ page import="com.board.model.*"%>
<%@ page import="com.boardSignature.model.*" %>


<%
	BoardService boardSvc = new BoardService();
	List<BoardVO> list = boardSvc.getAllApplying();
	int num = list.size();
	pageContext.setAttribute("list",list);
	Boolean fail = (Boolean)request.getAttribute("fail");
%>
<!DOCTYPE html>
<html>
<head>
<title>FanPool</title>
<meta charset="UTF-8">
<meta name="description" content="Food Blog Web Template">
<meta name="keywords" content="food, unica, creative, html">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- Favicon -->   
<link href="<%=request.getContextPath()%>/front-end/img/favicon.ico" rel="shortcut icon"/>
<!-- Google Fonts -->
<link href="https://fonts.googleapis.com/css?family=Poppins:400,400i,500,500i,600,600i,700" rel="stylesheet">
<!-- Stylesheets -->
<link rel="stylesheet" href="<%=request.getContextPath()%>/front-end/css/bootstrap.min.css">
<link rel="stylesheet" href="<%=request.getContextPath()%>/front-end/css/font-awesome.min.css">
<link rel="stylesheet" href="<%=request.getContextPath()%>/front-end/css/owl.carousel.css">
<link rel="stylesheet" href="<%=request.getContextPath()%>/front-end/css/animate.css">
<link rel="stylesheet" href="<%=request.getContextPath()%>/front-end/css/style.css">
<link rel="stylesheet" href="<%=request.getContextPath()%>/front-end/board/css/styleBoard.css">
<script src="<%=request.getContextPath()%>/back-end/js/jquery-3.2.1.min.js"></script>
<script src="<%=request.getContextPath()%>/back-end/js/owl.carousel.min.js"></script>
<script src="<%=request.getContextPath()%>/back-end/js/main.js"></script>

<style type="text/css">
	body{
		font-family:\5FAE\8EDF\6B63\9ED1\9AD4,\65B0\7D30\660E\9AD4;
	}

</style>

</head>
<body>
	<!-- Page Preloder -->
	<div id="preloder">
		<div class="loader"></div>
	</div>
<!-- Header section -->
	<header class="header-section">
		
		<div class="header-bottom">
			<div class="container">
			
	<!-- 返回後台首頁							 -->
				<a href="<%=request.getContextPath()%>/back-end/administrator.jsp">
					<button style="background-color:white;border: none;">
						<img  src="<%=request.getContextPath() %>/front-end/img/logo-1.png">
					</button>
				</a>
	<!-- 返回後台首頁end							 -->			
				
				<div class="nav-switch">
					<i class="fa fa-bars"></i>
				</div>
				
				<ul class="main-menu">
			
					<li>Super Administrator，你好</li>
					<li>
						<i><a href="<%=request.getContextPath()%>/back-end/login.jsp"  onclick="logout()"><img src="<%=request.getContextPath() %>/back-end/img/signout_106525.png"></a></i>						
					</li>
				</ul>
			</div>
		</div>
	</header>
<!-- Header section end -->

	<section class="recipes-section spad pt-0">
		<div class="container">
			<div class="section-title">
				<h2>聯署中專板列表</h2>
			</div>
			
			<div class="row">
				<div class="col-lg-4"></div>
				<div class="col-lg-6 col-md-6">
					<c:forEach var="boardVO" items="${list}">			
						<div class="recipe">
							<FORM METHOD="post" ACTION="<%=request.getContextPath()%>/front-end/boardSignature/boardSignature.do">
							<input type="hidden" name="boardID"  value="${boardVO.boardID }">
							<input type="hidden" name="memberID" value="${memberVO.memberID }">
							<input type="hidden" name="action"	value="joinSignature">
							<img src="<%= request.getContextPath()%>/board/boardImg.do?boardID=${boardVO.boardID}">
							</FORM>
						</div>
						<ul class="sp-recipes-list">
							<li>
								<div class="rl-info">
									<span>聯署標題</span>
									<h6>${boardVO.boardSignatureTitle }</h6>
								</div>
							</li>
							<li>
								<div class="rl-info">
									<span>聯署目標人數</span>
									<h6>${boardVO.boardSignatureTarget }</h6>
								</div>
							</li>
							<li>
								<div class="rl-info">
									<span>目前聯署人數</span>
									<h6>${boardVO.count }</h6>
								</div>
							</li>
							<li>
								<div class="rl-info">
									<span>連署結束時間</span>
									<h6>${boardVO.boardSignatureEndDate }</h6>
								</div>
							</li>
							<li>
								<div class="rl-info">
									<span>聯署內文</span>
									<h6>${boardVO.boardSignatureInfo }</h6>
								</div>
							</li>
						</ul>
							<input type="button" value="核准聯署" id="submitBtn${boardVO.boardID }" class="btn btn-adfund-back">
<script>
	if(${boardVO.count} < ${boardVO.boardSignatureTarget}){
		document.getElementById("submitBtn${boardVO.boardID }").style.display="none";
	}
	
	$(document).ready(function() {
		$('#submitBtn${boardVO.boardID }').click(function(){
			
		    $.ajax({
		     url : '<%=request.getContextPath()%>/front-end/boardSignature/boardSignature.do',
		     type : 'Post',
		     data : {
		      action : 'sendNotification',
		      boardID : '${boardVO.boardID}',
		     }
		    }).done(function(){
				alert("消息已送出!");
				window.location.reload();
		    });
	    
		});
	  })
</script>
							
							
							
							
							
					</c:forEach>
				</div>
			</div>
		</div>
		
	</section>	
		


	
	
	<!--====== Javascripts & Jquery ======-->
   	<script src="<%=request.getContextPath()%>/front-end/js/jquery-3.2.1.min.js"></script>
	<script src="<%=request.getContextPath()%>/front-end/js/owl.carousel.min.js"></script>
	<script src="<%=request.getContextPath()%>/front-end/js/main.js"></script>
</body>
</html>