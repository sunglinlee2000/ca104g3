<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="java.util.*"%>
<%@ page import="com.productReport.model.*"%>
<%@ page import="com.adfund.model.*"%>
<%@ page import="com.adfundReport.model.*"%>
<%@ page import="com.board.model.*"%>
<%@ page import="com.boardSignature.model.*" %>
<%@ page import="com.issueReport.model.*"%>
<%@ page import="com.groupReport.model.*"%>
<%@ page import="com.adfundReport.model.*"%>
<%@ page import="com.SNSReport.model.*"%>  

<%
  response.setHeader("Cache-Control","no-store"); //HTTP 1.1
  response.setHeader("Pragma","no-cache");        //HTTP 1.0
  response.setDateHeader ("Expires", 0);
%>
<%
//1募資審核計數adSubmitCount
ADFundService adsSvc = new ADFundService();
ADFundVO adsVO = new ADFundVO();
Integer adSubmitCount = adsSvc.getADSubmitCount();

//2專版審核計數boCount
BoardService boardSvc = new BoardService();
List<BoardVO> bolist = boardSvc.getAllApplying();
int boCount = bolist.size();

// 4商品檢舉計數prReportCount
ProductReportService prSvc = new ProductReportService();
ProductReportVO prVO = new ProductReportVO();
Integer prReportCount = prSvc.getCount();

//5話題檢舉計數isReportCount
IssueReportService isSvc = new IssueReportService();
IssueReportVO isVO = new IssueReportVO();
Integer isReportCount = isSvc.getISRepoetCount();

//6揪團檢舉計數grReportCount
GroupReportService grSvc = new GroupReportService();
GroupReportVO grVO = new GroupReportVO();
Integer grReportCount = grSvc.getAll_unchecked_count();

//7募資檢舉計數adReportCount
ADFundReportService adrSvc = new ADFundReportService();
ADFundReportVO adrVO = new ADFundReportVO();
Integer adReportCount = adrSvc.getADRepoetCount();


//8SNS檢舉計數SNSReportCount
SNSReportService snsReportSvc = new SNSReportService();
List<SNSReportVO> snslist = snsReportSvc.getAll();
int SNSReportCount = snslist.size();
%>



<html>
<head>
<script src='//static.codepen.io/assets/editor/live/console_runner-ce3034e6bde3912cc25f83cccb7caa2b0f976196f2f2d52303a462c826d54a73.js'></script>
<script src='//static.codepen.io/assets/editor/live/css_reload-2a5c7ad0fe826f66e054c6020c99c1e1c63210256b6ba07eb41d7a4cb0d0adab.js'></script>
<meta charset="UTF-8">
<meta name="description" content="Food Blog Web Template">
<meta name="keywords" content="food, unica, creative, html">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="robots" content="noindex">
    <title>FANPOOL 後台</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <!--         Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="<%=request.getContextPath() %>/back-end/css/administrator.css">
    <!-- Favicon -->   
	<link href="img/favicon.ico" rel="shortcut icon"/>

	<!-- Google Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Poppins:400,400i,500,500i,600,600i,700" rel="stylesheet">

	<!-- Stylesheets -->
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/css/bootstrap.min.css"/>
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/css/font-awesome.min.css"/>
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/css/owl.carousel.css"/>
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/css/animate.css"/>
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/css/style.css"/>
	
	<!-- original -->			
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/issue/css/bootstrap.min.css">
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/issue/css/bootstrap-theme.min.css">
	
	<script src="<%=request.getContextPath() %>/front-end/issue/js/jquery-3.2.1.min.js"></script>
	
	<script src="<%=request.getContextPath() %>/front-end/issue/js/bootstrap.min.js"></script>
	
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/issue/css/index.css">
	
<!--水滴hover -->
    <link rel="mask-icon" type="" href="//static.codepen.io/assets/favicon/logo-pin-f2d2b6d2c61838f7e76325261b7195c27224080bc099486ddd6dccb469b8e8e6.svg" color="#111" />
    <link rel="canonical" href="https://codepen.io/anon/pen/aQrLBN" />
    <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Slabo+27px'>	
	
<style type="text/css">        
.btn-primary,
.btn-primary:hover,
.btn-primary:active,
.btn-primary:visited,
.btn-primary:focus {
    background-color: #7dccff;
    border-color: #7dccff;
}

body{
    	font-family:\5FAE\8EDF\6B63\9ED1\9AD4,\65B0\7D30\660E\9AD4;
}

.animate_block {
    -webkit-transition: all 0.3s ease-in-out;
    -moz-transition: all 0.3s ease-in-out;
    -o-transition: all 0.3s ease-in-out;
    -ms-transition: all 0.3s ease-in-out;
    transition: all 0.3s ease-in-out;
}

.overlayCross {
    width:0;
    height:0;
    top:0;
    background: #00000080;
}

.index_img{
    width: 150px;
    height: 50px;
    text-align: center;
    vertical-align: middle;

    transition: transform .75s; /* Animation */
    margin: 0 auto;

}

.animate_block {
    -webkit-transition: all 0.3s ease-in-out;
    -moz-transition: all 0.3s ease-in-out;
    -o-transition: all 0.3s ease-in-out;
    -ms-transition: all 0.3s ease-in-out;
    transition: all 0.3s ease-in-out;
}


.container_for_hover:hover .overlay {
    opacity:1;
}

/* 水滴hover */
body, html {
  height: 100%;
  width: 100%;
}

*, *::before, *::after {
  margin: 0;
  padding: 0;
  box-sizing: border-box;
  -webkit-box-sizing: border-box;
  -moz-box-sizing: border-box;
}

.section {
  float: left;
  font-family: 'Slabo 27px', serif;
  display: flex;
  -webkit-display: flex;
  -webkit-align-items: center;
  align-items: center;
  justify-content: center;
  -webkit-justify-content: center;
}

.button {
  border: none;
  cursor: pointer;
  padding: 35px 70px;
  position: relative;
}
.button .text {
  position: relative;
  z-index: 100;
  font-size: 2em;
}

#section-4 {
  background-color: #90afc5;
}
#section-4 #button4 {
  background-color: #90afc5;
  overflow: hidden;
}
#section-4 #button4 .text {
  color: #16253d;
  transition: all .5s linear;
  -webkit-transition: all .5s linear;
}
#section-4 #button4::after {
  content: '';
  position: absolute;
  transition: top .5s ease-in .5s, width .5s ease-out, height .5s ease-out;
  -webkit-transition: top .5s ease-in .5s, width .5s ease-out, height .5s ease-out;
  left: 50%;
  top: -50%;
  transform: translate(-50%, -50%);
  width: 20px;
  height: 20px;
  border-radius: 50%;
  background-color: #16253d;
}
#section-4 #button4:hover .text {
  color: #90afc5;
  transition: color .5s linear .5s;
  -webkit-transition: color .5s linear .5s;
}
#section-4 #button4:hover::after {
  transition: top .5s ease-in, width .5s ease-out .5s, height .5s ease-out .5s;
  -webkit-transition: top .5s ease-in, width .5s ease-out .5s, height .5s ease-out .5s;
  top: 50%;
  width: 400px;
  height: 400px;
}

</style>
</head>
<body>
<!-- Header section -->
	<header class="header-section">
		
		<div class="header-bottom">
			<div class="container">
				<a href="#" class="site-logo"><img src="<%=request.getContextPath() %>/front-end/img/logo-1.png" alt=""></a>
				<div class="nav-switch">
					<i class="fa fa-bars"></i>
				</div>
				
				<ul class="main-menu">
			
					<li>Super Administrator，你好</li>
					<li>
						<i><a href="<%=request.getContextPath()%>/back-end/login.jsp"  onclick="logout()"><img src="<%=request.getContextPath() %>/back-end/administrator/images/signout_106525.png"></a></i>						
					</li>
				</ul>
			</div>
		</div>
	</header>
<!-- Header section End-->
    
    	<div class="container" style="padding-bottom:15px">
            <div class="row">
                <div class="col-xs-12 col-sm-12">
                

                    	<div class="col-md-4 col-sm-12 col-xs-12" style="max-width:">
	                    	<a href="<%=request.getContextPath()%>/back-end/boardSignature/boardSignature.jsp">
								<div class="container_for_hover animate_block">
	                                <center><img src="<%=request.getContextPath() %>/back-end/administrator/images/3333.png"></center>
	                                <span style="position: absolute; top: 5px; left: 315px;"><h3><b>
	                                	<font color="black"><%= boCount%></font>
	                                </b></h3></span>
								</div>
							</a>
                        </div>
                    	<div class="col-md-4 col-sm-12 col-xs-12" style="max-width:" >
	                    	<a href="<%=request.getContextPath()%>/back-end/adfund/adfund_back_list.jsp">
								<div class="container_for_hover animate_block">
	                                <center><img src="<%=request.getContextPath() %>/back-end/administrator/images/2222.png"></center>
	                                	<span style="position: absolute; top: 5px; left: 315px; color: black;"><h3><b>
	                                		<font color="black"><%= adSubmitCount%></font>
	                                	</b></h3></span> 
	                        	</div>
	                        </a>	
                        </div>
                    	<div class="col-md-4 col-sm-12 col-xs-12" style="max-width:" >
	                    	<a href="<%=request.getContextPath()%>/back-end/artist/listAllArtist.jsp">
								<div class="container_for_hover animate_block">
	                                <center><img src="<%=request.getContextPath() %>/back-end/administrator/images/11111.png"></center>
	                                	<span style="position: absolute; top: 11px; left: 306px; color: black"><h3><b>
<!-- 	                                		xx -->
	                                	</b></h3></span> 
	                        	</div>
	                        </a>	
                        </div>                        
                        
                    </div>
		       </div>
		   </div>
		    
    	<div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12">
                
<!--1商品檢舉-->                
                    	<div class="pic col-md-2 col-sm-6 col-xs-6">
                    		<a href="<%=request.getContextPath()%>/back-end/product/productReport_back_list.jsp">
                                <center><img src="<%=request.getContextPath() %>/back-end/administrator/images/4444.png" class="" alt="">
                                	<span style="position: absolute; top: 5px; left: 145px; color: black;"><h3><b>
	                                	<font color="black"><%= prReportCount%></font>
	                                </b></h3></span>   	
                                </center>
                            </a>
                        </div>
<!--1商品檢舉end-->
                        
                        
<!--2話題檢舉-->                        
                    	<div class="pic col-md-2 col-sm-6 col-xs-6">
							<a href="<%=request.getContextPath()%>/back-end/issueReport/listAllEmp.jsp">
                                <center><img src="<%=request.getContextPath() %>/back-end/administrator/images/5555.png" class="" alt="">
                                	<span style="position: absolute; top: 5px; left: 145px; color: black;"><h3><b>
	                                	<font color="black"><%= isReportCount%></font>
	                                </b></h3></span>
                                </center>
							</a>
                        </div>
<!--2話題檢舉end-->


<!--3揪團檢舉-->                        
                    	<div class="pic col-md-2 col-sm-6 col-xs-6">
                        	<a href="<%=request.getContextPath()%>/back-end/groupList/groupList_back_unchecked.jsp">
                                <center><img src="<%=request.getContextPath() %>/back-end/administrator/images/6666.png" class="" alt="">
                                	<span style="position: absolute; top: 5px; left: 145px; color: black;"><h3><b>
	                                	<font color="black"><%= grReportCount%></font>
	                                </b></h3></span>
                                </center>
                            </a>        
                        </div>
<!--3揪團檢舉end-->


<!--4募資檢舉-->                        
                    	<div class="pic col-md-2 col-sm-6 col-xs-6">
                            <a href="<%=request.getContextPath()%>/back-end/adfund/adfund_back_report_joinlist_all.jsp">
                                <center><img src="<%=request.getContextPath() %>/back-end/administrator/images/7777.png" class="" alt="">
                                	<span style="position: absolute; top: 5px; left: 145px; color: black;"><h3><b>
	                                	<font color="black"><%= adReportCount%></font>
	                                </b></h3></span>
                                </center>
                            </a>        
                        </div>
<!--4募資檢舉end-->



<!--5SNS檢舉-->                         
                    	<div class="pic col-md-2 col-sm-6 col-xs-6">
                            <a href="<%=request.getContextPath()%>/back-end/SNSReport/SNSReport.jsp">
                                 <center><img src="<%=request.getContextPath() %>/back-end/administrator/images/9999.png" class="" alt="">
                                 	<span style="position: absolute; top: 5px; left: 145px; color: black;"><h3><b>
	                                	<font color="black"><%= SNSReportCount%></font>
	                                </b></h3></span>
                                 </center>
                            </a>        
                        </div>
<!--55SNS檢舉end-->


<!--6藝人社群帳號-->                       
                    	<div class="pic col-md-2 col-sm-6 col-xs-6">
                            <a href="<%=request.getContextPath()%>/back-end/SNSAccount/listAllSNSAccount.jsp">
                                <center><img src="<%=request.getContextPath() %>/back-end/administrator/images/8888.png" class="" alt="">
                                	<span style="position: absolute; top: 5px; left: 145px; color: black;"><h3><b>
	                                </b></h3></span>
                                </center>                            
                            </a>        
                        </div>
<!--6藝人社群帳號end-->

                        
		           </div>
		       </div>
		   </div> 

	<!-- Gallery section end -->


	
         
	<script src="<%=request.getContextPath() %>/front-end/js/jquery-3.2.1.min.js"></script>
	<script src="<%=request.getContextPath() %>/front-end/owl.carousel.min.js"></script>
	<script src="<%=request.getContextPath() %>/front-end/js/main.js"></script>		
   	<script src="https://code.jquery.com/jquery.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script src='//static.codepen.io/assets/common/stopExecutionOnTimeout-41c52890748cd7143004e05d3c5f786c66b19939c4500ce446314d1748483e13.js'></script>
	
 </body>
 </html>