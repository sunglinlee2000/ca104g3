<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="com.artist.model.*"%>

<%
	ArtistVO artistVO = (ArtistVO) request.getAttribute("artistVO");
%>

<!DOCTYPE html>
<html>
<head>
<title>FanPool</title>
<meta charset="UTF-8">
<meta name="description" content="Food Blog Web Template">
<meta name="keywords" content="food, unica, creative, html">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- Favicon -->   
<link href="<%=request.getContextPath()%>/front-end/img/favicon.ico" rel="shortcut icon"/>
<!-- Google Fonts -->
<link href="https://fonts.googleapis.com/css?family=Poppins:400,400i,500,500i,600,600i,700" rel="stylesheet">
<!-- Stylesheets -->
<link rel="stylesheet" href="<%=request.getContextPath()%>/back-end/css/bootstrap.min.css">
<link rel="stylesheet" href="<%=request.getContextPath()%>/back-end/css/font-awesome.min.css">
<link rel="stylesheet" href="<%=request.getContextPath()%>/back-end/css/owl.carousel.css">
<link rel="stylesheet" href="<%=request.getContextPath()%>/back-end/css/animate.css">
<link rel="stylesheet" href="<%=request.getContextPath()%>/back-end/css/style.css">
<link rel="stylesheet" href="<%=request.getContextPath()%>/back-end/SNSAccount/css/SNSAccount.css">
<script src="<%=request.getContextPath()%>/back-end/js/jquery-3.2.1.min.js"></script>
<script src="<%=request.getContextPath()%>/back-end/js/owl.carousel.min.js"></script>
<script src="<%=request.getContextPath()%>/back-end/js/main.js"></script>

<style type="text/css">
	body{
		font-family:\5FAE\8EDF\6B63\9ED1\9AD4,\65B0\7D30\660E\9AD4;
	}

</style>
<style type="text/css">
body{
    font-family:\5FAE\8EDF\6B63\9ED1\9AD4,\65B0\7D30\660E\9AD4;
}

.kk-adfund-table {
	margin-top: 20px;
	text-align: center;
	font-size: 13px;
}

.kk-adfund-table thead th {
	margin-top: 20px;
	text-align: center;
	background-color: #DCDCDC;
}

.btn-adfund-back {
	background-color: #9999cc;
	border: 1px solid;
	border-color: #DCDCDC;
	padding: 3px;
	color: white;
	font-size: 11px;
}
</style>
 


</head>

<body>
	<div class="container-fiuld kk-ad-back-con">
		<div class="row">
		   <div class="col-xs-12 col-sm-4">
			</div>
			<div class="col-xs-12 col-sm-4 ">
 			<h3>藝人資料新增</h3>

			<jsp:useBean id="boardSvc" scope="page"	class="com.board.model.BoardService" />

				<FORM METHOD="post" ACTION="<%=request.getContextPath()%>/back-end/artist/artist.do" name="form1">
				<table class="table table-hover table-striped table-condensed table-bordered kk-adfund-table" >
					<tr>
						<td>專板名稱:</td>
						<td>
							<select size="1" name="boardID">
								<c:forEach var="boardVO" items="${boardSvc.getAllOperating()}">
									<option value="${boardVO.boardID}">${boardVO.boardSignatureTitle}
								</c:forEach>
							</select>
						</td>
					</tr>
					<tr>
						<td>藝人名稱:</td>
						<td><input type="TEXT" name="artistName" size="45"
							 value="${param.artistName }" /></td>
					</tr>
				</table>
				<br>
				<input type="hidden" name="action" value="insert">
				<div align="center">
				<input type="submit" value="送出新增" class="btn btn-adfund-back">
				</div>
				</FORM>
			</div>
		</div>
	</div>

<script src="https://code.jquery.com/jquery.js"></script>
<script	src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>   	

</body>
</html>