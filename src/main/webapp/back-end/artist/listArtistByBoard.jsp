<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="java.util.*"%>
<%@ page import="com.artist.model.*"%>

<jsp:useBean id="listArtistByBoard" scope="request" type="java.util.Set<ArtistVO>" /> <!-- 於EL此行可省略 -->
<jsp:useBean id="boardSvc" scope="page" class="com.board.model.BoardService" />


<html>
<head>
<title>藝人資料</title>

<link rel="stylesheet" href="<%=request.getContextPath()%>/back-end/css/bootstrap.min.css">
<link rel="stylesheet" href="<%=request.getContextPath()%>/back-end/css/bootstrap-theme.min.css">
<script src="<%=request.getContextPath()%>/back-end/js/jquery-3.3.1.min.js"></script>
<script src="<%=request.getContextPath()%>/back-end/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="<%=request.getContextPath()%>/back-end/css/index.css">    
<link rel="stylesheet" href="<%=request.getContextPath()%>/back-end/css/singleGanban.css">
   
<style>
  table#table-1 {
	background-color: #CCCCFF;
    border: 2px solid black;
    text-align: center;
  }
  table#table-1 h4 {
    color: red;
    display: block;
    margin-bottom: 1px;
  }
  h4 {
    color: blue;
    display: inline;
  }
</style>
<style type="text/css">
	.boardCard{
		height: 200px;
		background-color: #faa;
		padding-top: : 100px;
		border-radius: 5px;
	}
	#test1,#hide1{
		padding: 5px;
		text-align: center;
		background-color: #faa;
	}

	#c{
		height: 700px;
		background-color: #faa;
		display: none;
		border-radius:5px;
	}

</style>
<style>
  table {
	width: 100%;
	background-color: white;
	margin-top: 5px;
	margin-bottom: 5px;
  }
  table, th, td {
    border: 1px solid #CCCCFF;
  }
  th, td {
    padding: 5px;
    text-align: center;
  }
</style>

</head>
<body>

	<nav class="navbar navbar-expand-lg navbar-light bg-light">
		<a class="navbar-brand" href="select_page.jsp"><h4>FanPool</h4></a> <img
			src="<%=request.getContextPath()%>/back-end/res/gif/index/8757481430283 (3).gif">

		<div class="navbar-right">
			<form class="form-inline my-2 my-lg-0">

				<span class="icons"><img
					src="<%=request.getContextPath()%>/back-end/res/img/index/megaphone(32).png"></span> <span class="icons"><img
					src="<%=request.getContextPath()%>/back-end/res/img/index/chat(32).png"></span> <span class="icons"><img
					src="<%=request.getContextPath()%>/back-end/res/img/index/user(32).png"></span>


			</form>
		</div>
	</nav>

<div class="container-fiuld aa">
		
		<div>
			<div class="row">
				<div class="col-xs-12 col-sm-12">
				<div align="center">
					<h3>所有藝人資料</h3>
					<h4><a href="select_page.jsp">回首頁</a></h4>
				</div>
				
<table>
	<tr>
		
		<th>藝人編號</th>
		<th>藝人專版編號</th>
		<th>藝人名稱</th>
		
	</tr>
	<div align="center">
	
	<c:forEach var="artistVO" items="${listArtistByBoard}">
	</div>	
		<tr>
			
			<td>${artistVO.artistID}</td>
			<td>${artistVO.boardID}</td>
			<td>${artistVO.artistName}</td>
			<td>
			  <FORM METHOD="post" ACTION="<%=request.getContextPath()%>/back-end/artist/artist.do" style="margin-bottom: 0px;">
			     <input type="submit" value="修改">
			     <input type="hidden" name="artistID"  value="${artistVO.artistID}">
			     <input type="hidden" name="action"	value="getOne_For_Update"></FORM>
			</td>
			<td>
			  <FORM METHOD="post" ACTION="<%=request.getContextPath()%>/back-end/artist/artist.do" style="margin-bottom: 0px;">
			     <input type="submit" value="刪除">
			     <input type="hidden" name="artistID"  value="${artistVO.artistID}">
			     <input type="hidden" name="action" value="delete"></FORM>
			</td>
			
		</tr>
	</c:forEach>		
</table>
<div align="center">
<hr>

</div>

			</div>
		</div>
	</div>
</div>


<div class="footer">
	<div class="row">
		<div class="col-xs-12 col-sm-12 footer-text">
			<h6>連絡電話:03 425 7387</h6>
			<h6>地址:320 桃園市中壢區中大路300號 CA104班第三組</h6>
		</div>
	</div>
</div>


</body>
</html>