<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="com.artist.model.*"%>

<%
ArtistVO artistVO = (ArtistVO) request.getAttribute("artistVO");
%>
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
<title>藝人資料修改</title>

<link rel="stylesheet" href="<%=request.getContextPath()%>/back-end/css/bootstrap.min.css">
<link rel="stylesheet" href="<%=request.getContextPath()%>/back-end/css/bootstrap-theme.min.css">
<script src="<%=request.getContextPath()%>/back-end/js/jquery-3.3.1.min.js"></script>
<script src="<%=request.getContextPath()%>/back-end/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="<%=request.getContextPath()%>/back-end/css/index.css">    
<link rel="stylesheet" href="<%=request.getContextPath()%>/back-end/css/singleGanban.css">
 
<style>
  table#table-1 {
	background-color: #CCCCFF;
    border: 2px solid black;
    text-align: center;
  }
  table#table-1 h4 {
    color: red;
    display: block;
    margin-bottom: 1px;
  }
  h4 {
    color: blue;
    display: inline;
  }
</style>

<style>
  table {
	width: 450px;
	background-color: white;
	margin-top: 1px;
	margin-bottom: 1px;
  }
  table, th, td {
    border: 0px solid #CCCCFF;
  }
  th, td {
    padding: 1px;
  }
</style>

</head>
<body bgcolor='white'>

	<nav class="navbar navbar-expand-lg navbar-light bg-light">
		<a class="navbar-brand" href="<%=request.getContextPath()%>/back-end/artist/select_page.jsp">FanPool</a> <img
			src="<%=request.getContextPath()%>/back-end/res/gif/index/8757481430283 (3).gif">

		<div class="navbar-right">
			<form class="form-inline my-2 my-lg-0">

				<span class="icons"><img
					src="<%=request.getContextPath()%>/back-end/res/img/index/megaphone(32).png"></span> <span class="icons"><img
					src="<%=request.getContextPath()%>/back-end/res/img/index/chat(32).png"></span> <span class="icons"><img
					src="<%=request.getContextPath()%>/back-end/res/img/index/user(32).png"></span>


			</form>
		</div>
	</nav>

		 <h3>專板資料修改</h3>
		 <h4><a href="<%=request.getContextPath()%>/back-end/artist/select_page.jsp"><font color="black">回首頁</font></a></h4>

<h3>資料修改:</h3>

<%-- 錯誤表列 --%>
<c:if test="${not empty errorMsgs}">
	<font style="color:red">請修正以下錯誤:</font>
	<ul>
		<c:forEach var="message" items="${errorMsgs}">
			<li style="color:red">${message}</li>
		</c:forEach>
	</ul>
</c:if>

<FORM METHOD="post" ACTION="artist.do" name="form1">
<table style="width:100%" class="table table-hover table-striped table-condensed table-bordered">
	<tr>
		<td>藝人編號:</td>
		<td><%= artistVO.getArtistID()%></td>
	</tr>
	<tr>
		<td>專板編號:</td>
		<td><input type="TEXT" name="boardID" size="45" 
			 value="<%= (artistVO==null)? "M000001" : artistVO.getBoardID()%>" /></td>
	</tr>
	<tr>
		<td>藝人名稱:</td>
		<td><input type="TEXT" name="artistName" size="45"
			 value="<%= (artistVO==null)? "請輸入名稱" : artistVO.getArtistName()%>" /></td>
	</tr>




</table>
<br>
<input type="hidden" name="action" value="update">
<input type="hidden" name="artistID" value="<%=artistVO.getArtistID()%>">
<input type="submit" value="送出修改"></FORM>
</body>




<div class="footer">
	<div class="row">
		<div class="col-xs-12 col-sm-12 footer-text">
			<h6>連絡電話:03 425 7387</h6>
			<h6>地址:320 桃園市中壢區中大路300號 CA104班第三組</h6>
		</div>
	</div>
</div>

</html>