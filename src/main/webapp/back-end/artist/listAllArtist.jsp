<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="java.util.*"%>
<%@ page import="com.artist.model.*"%>


<%
	ArtistService artistSvc = new ArtistService();
	List<ArtistVO> list = artistSvc.getAll();
	pageContext.setAttribute("list",list);
%>

<!DOCTYPE html>
<html>
<head>
<title>FanPool</title>
<meta charset="UTF-8">
<meta name="description" content="Food Blog Web Template">
<meta name="keywords" content="food, unica, creative, html">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- Favicon -->   
<link href="<%=request.getContextPath()%>/front-end/img/favicon.ico" rel="shortcut icon"/>
<!-- Google Fonts -->
<link href="https://fonts.googleapis.com/css?family=Poppins:400,400i,500,500i,600,600i,700" rel="stylesheet">
<!-- Stylesheets -->
<link rel="stylesheet" href="<%=request.getContextPath()%>/back-end/css/bootstrap.min.css">
<link rel="stylesheet" href="<%=request.getContextPath()%>/back-end/css/font-awesome.min.css">
<link rel="stylesheet" href="<%=request.getContextPath()%>/back-end/css/owl.carousel.css">
<link rel="stylesheet" href="<%=request.getContextPath()%>/back-end/css/animate.css">
<link rel="stylesheet" href="<%=request.getContextPath()%>/back-end/css/style.css">
<link rel="stylesheet" href="<%=request.getContextPath()%>/back-end/SNSAccount/css/SNSAccount.css">
<script src="<%=request.getContextPath()%>/back-end/js/jquery-3.2.1.min.js"></script>
<script src="<%=request.getContextPath()%>/back-end/js/owl.carousel.min.js"></script>
<script src="<%=request.getContextPath()%>/back-end/js/main.js"></script>

<style type="text/css">
	body{
		font-family:\5FAE\8EDF\6B63\9ED1\9AD4,\65B0\7D30\660E\9AD4;
	}

</style>
<style type="text/css">
body{
    font-family:\5FAE\8EDF\6B63\9ED1\9AD4,\65B0\7D30\660E\9AD4;
}

.kk-adfund-table {
	margin-top: 20px;
	text-align: center;
	font-size: 13px;
}

.kk-adfund-table thead th {
	margin-top: 20px;
	text-align: center;
	background-color: #DCDCDC;
}

.btn-adfund-back {
	background-color: #9999cc;
	border: 1px solid;
	border-color: #DCDCDC;
	padding: 3px;
	color: white;
	font-size: 11px;
}
</style>  

</head>
<body>
<!-- Header section -->
	<header class="header-section">
		
		<div class="header-bottom">
			<div class="container">
			
	<!-- 返回後台首頁							 -->
				<a href="<%=request.getContextPath()%>/back-end/administrator.jsp">
					<button style="background-color:white;border: none;">
						<img  src="<%=request.getContextPath() %>/front-end/img/logo-1.png">
					</button>
				</a>
	<!-- 返回後台首頁end							 -->			
				
				<div class="nav-switch">
					<i class="fa fa-bars"></i>
				</div>
				
				<ul class="main-menu">
			
					<li>Super Administrator，你好</li>
					<li>
						<i><a href="<%=request.getContextPath()%>/back-end/login.jsp"  onclick="logout()"><img src="<%=request.getContextPath() %>/back-end/img/signout_106525.png"></a></i>						
					</li>
				</ul>
			</div>
		</div>
	</header>
<!-- Header section end -->
	<div class="container-fiuld kk-ad-back-con">
		<div class="row">
		   <div class="col-xs-12 col-sm-3">
			</div>
			<div class="col-xs-12 col-sm-6 ">
				<h3>所有藝人資料</h3>
			<table class="table table-hover table-striped table-condensed table-bordered kk-adfund-table">
				<thead class="kk-adfund-thead">
					<tr>
						<th>藝人編號</th>
						<th>藝人專版編號</th>
						<th>藝人名稱</th>
						<th>修改</th>
						
					</tr>
				</thead>
				<div align="center">
				<%@ include file="page1.file"%>
				<c:forEach var="artistVO" items="${list}" begin="<%=pageIndex%>" end="<%=pageIndex+rowsPerPage-1%>">
				</div>	
					<tr>
						<td>${artistVO.artistID}</td>
						<td>${artistVO.boardID}</td>
						<td>${artistVO.artistName}</td>
						<td>
						  <FORM METHOD="post" ACTION="<%=request.getContextPath()%>/back-end/artist/artist.do" style="margin-bottom: 0px;">
						     <input type="submit" value="修改" class="btn btn-adfund-back">
						     <input type="hidden" name="artistID"  value="${artistVO.artistID}">
						     <input type="hidden" name="action"	value="getOne_For_Update"></FORM>
						</td>
						
					</tr>
				</c:forEach>		
			</table>
			<div align="center">
				<FORM METHOD="post" ACTION="<%=request.getContextPath()%>/back-end/artist/artist.do">
					<input type="hidden" name="action" value="insert">
					<input type="submit" value="新增藝人" class="btn btn-adfund-back">
				</FORM>
			</div>
			<div align="center">
				<%@ include file="page2.file"%>
			</div>
		</div>
	</div>
</div>

<script src="https://code.jquery.com/jquery.js"></script>
<script	src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>   


</body>
</html>