<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page import="java.util.*"%>
<%@ page import="com.issueReport.model.*"%>
<%@ page import="com.issue.model.*"%>
<%-- 此頁練習採用 EL 的寫法取值 --%>

<%
  response.setHeader("Cache-Control","no-store"); //HTTP 1.1
  response.setHeader("Pragma","no-cache");        //HTTP 1.0
  response.setDateHeader ("Expires", 0);
%>

<%
    IssueReportService issueReportSvc = new IssueReportService();
    List<IssueReportVO> list = issueReportSvc.getAll();
    pageContext.setAttribute("list",list);
%>



<html>
<head>
<title>話題檢舉審核</title>
<meta charset="UTF-8">
<meta name="description" content="Food Blog Web Template">
<meta name="keywords" content="food, unica, creative, html">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>FANPOOL 後台</title>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
    <!--[if lt IE 9]>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
        <!-- Favicon -->   
	<link href="img/favicon.ico" rel="shortcut icon"/>

	<!-- Google Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Poppins:400,400i,500,500i,600,600i,700" rel="stylesheet">

	<!-- Stylesheets -->
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/css/bootstrap.min.css"/>
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/css/font-awesome.min.css"/>
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/css/owl.carousel.css"/>
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/css/animate.css"/>
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/css/style.css"/>
	
	<!-- original -->			
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/issue/css/bootstrap.min.css">
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/issue/css/bootstrap-theme.min.css">
	<script src="<%=request.getContextPath() %>/front-end/js/jquery-3.2.1.min.js"></script>
	<script src="<%=request.getContextPath() %>/front-end/owl.carousel.min.js"></script>
	<script src="<%=request.getContextPath() %>/front-end/js/main.js"></script>		
   	<script src="https://code.jquery.com/jquery.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
	
	<script src="<%=request.getContextPath() %>/front-end/issue/js/jquery-3.3.1.min.js"></script>
	
	<script src="<%=request.getContextPath() %>/front-end/issue/js/bootstrap.min.js"></script>
	
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/issue/css/index.css">
	<script src="<%=request.getContextPath() %>/ckeditor/ckeditor.js"></script>

<style>
  table#table-1 {
	background-color: #CCCCFF;
    border: 2px solid black;
    text-align: center;
  }
  table#table-1 h4 {
    color: red;
    display: block;
    margin-bottom: 1px;
  }
  h4 {
    color: blue;
    display: inline;
  }
  
  table {
	width: 800px;
	background-color: white;
	margin-top: 5px;
	margin-bottom: 5px;
  }
  table, th, td {
    border: 1px solid #CCCCFF;
  }
  th, td {
    padding: 5px;
    text-align: center;
  }
  
body{
    font-family:\5FAE\8EDF\6B63\9ED1\9AD4,\65B0\7D30\660E\9AD4;
}

.kk-adfund-table {
	margin-top: 20px;
	text-align: center;
	font-size: 13px;
}

.kk-adfund-table thead th {
	margin-top: 20px;
	text-align: center;
	background-color: #DCDCDC;
}

.btn-adfund-back {
	background-color: #9999cc;
	border: 1px solid;
	border-color: #DCDCDC;
	padding: 3px;
	color: white;
	font-size: 11px;
}
</style>

</head>
<body bgcolor='white'>
<!-- Header section -->
	<header class="header-section">
		
		<div class="header-bottom">
			<div class="container">
			
	<!-- 返回後台首頁							 -->
				<a href="<%=request.getContextPath()%>/back-end/administrator.jsp">
					<button style="background-color:white;border: none;">
						<img  src="<%=request.getContextPath() %>/front-end/img/logo-1.png">
					</button>
				</a>
	<!-- 返回後台首頁end							 -->			
				
				<div class="nav-switch">
					<i class="fa fa-bars"></i>
				</div>
				
				<ul class="main-menu">
			
					<li>Super Administrator，你好</li>
					<li>
						<i><a href="<%=request.getContextPath()%>/back-end/login.jsp"  onclick="logout()"><img src="<%=request.getContextPath() %>/back-end/img/signout_106525.png"></a></i>						
					</li>
				</ul>
			</div>
		</div>
	</header>
<!-- Header section end -->

<%-- 錯誤表列 --%>
<%if (list != null && (list.size() > 0)) {%>
	<c:if test="${not empty errorMsgs}">
		<font style="color:red">請修正以下錯誤:</font>
		<ul>
			<c:forEach var="message" items="${errorMsgs}">
				<li style="color:red">${message}</li>
			</c:forEach>
		</ul>
	</c:if>
	    	<div class="container-fiuld kk-ad-back-con">
	            <div class="row">
	                <div class="col-xs-12 col-sm-12">
	                
	<%--留空--%>        <div class="col-md-1 col-sm-12 col-xs-12" style="max-width:"></div>
	
							<div class="col-md-10 col-sm-12 col-xs-12" style="max-width:">
								<center><h2>話題檢舉管理</h2>
									<table class="table table-hover table-striped table-condensed table-bordered kk-adfund-table">
										<%@ include file="page1.file"%>
											<thead class="kk-adfund-thead">
												<tr>
													<th>檢舉編號</th>
	<!-- 												<th>所屬專版</th> -->
													<th>話題編號</th>
													<th>會員編號</th>
													<th>檢舉內容</th>
													<th>檢舉時間</th>
													<th>檢舉事由</th>
													<th>查看詳情</th>
													<th>通過</th>
													<th>不通過</th>
												</tr>
											 </thead>
										 
											<c:forEach var="issueReportVO" items="${list}" begin="<%=pageIndex%>" end="<%=pageIndex+rowsPerPage-1%>">
												
												<tr>
													<td>${issueReportVO.issueReportID}</td>
<%-- 	 												<td>${issueReportVO.boardID}</td> --%>
													<td><a href="<%=request.getContextPath()%>/issue/issue.do?issueID=${issueReportVO.issueID}&action=getOne_For_Display">${issueReportVO.issueID}</a></td>
													<td>${issueReportVO.memberID}</td>
													<td>${issueReportVO.issueReportContent}</td>
													<td><fmt:formatDate value = "${issueReportVO.issueReportDate}" pattern="YYYY-MM-dd  hh:mm:ss"/></td>
													<td>${issueReportVO.issueReportReason}</td>
													<td><a href="#modal-id" data-toggle="modal" class="btn btn-adfund-back" id="look${issueReportVO.issueReportID}">查看詳情</a></td>
													<td>
													  <FORM METHOD="post" ACTION="<%=request.getContextPath()%>/issueReport/issueReport.do" style="margin-bottom: 0px;">
													     <input type="submit" value="通過" class="btn btn-adfund-back" id="${issueReportVO.issueReportID}">
													     
													     <input type="hidden" name="issueID"  value="${issueVO.issueID}">
											    		 <input type="hidden" name="boardID"  value="${issueVO.boardID}">
													     <input type="hidden" name="issueReportID"  value="${issueReportVO.issueReportID}">
													     <input type="hidden" name="action" value="deleteByIssueID"></FORM>
													</td>
													
													<td>
													  <FORM METHOD="post" ACTION="<%=request.getContextPath()%>/issueReport/issueReport.do" style="margin-bottom: 0px;">
													     <input type="submit" value="不通過" class="btn btn-adfund-back" id="${issueReportVO.issueReportID}">
													     <input type="hidden" name="issueReportID"  value="${issueReportVO.issueReportID}" >
													     <input type="hidden" name="action" value="delete"></FORM>
													</td>
												</tr>
												
											    <script>	   
											    $(document).ready(function() {  
												     $('#look${issueReportVO.issueReportID}').click(function() {
												    		$.ajax({  
													             url : '<%=request.getContextPath()%>/issue/issue.do',  
													             type : 'Post',  
													             data : {  
													              action : 'getOne_For_Display2',  
													              issueID:'${issueReportVO.issueID}'
													             }  
													            }).done(function(res){ 
													             var obj = JSON.parse(res);
													            	if(obj.boardID=='B000001'){
													            	$('#boardID').text('Seventeen');
													            	}
													            	if(obj.boardID=='B000002'){
														            	$('#boardID').text('Twice');
														            	}
													            	if(obj.boardID=='B000003'){
														            	$('#boardID').text('新垣結衣');
														            	}
													            	if(obj.boardID=='B000004'){
														            	$('#boardID').text('Maroon5');
														            	}
													            	if(obj.boardID=='B000005'){
														            	$('#boardID').text('五月天');
														            	}
													            	if(obj.boardID=='B000006'){
														            	$('#boardID').text('蘇打綠');
														            	}
													            	$('#issueTitle').text(obj.issueTitle);
													            	$('#issueContenPhoto').html(obj.issueContenPhoto);
													            	$('#modal-id').modal();
														            });												    		 												    
												     });
											      });
											    </script>    												
												
											</c:forEach>
										</table>
									</center>
								</div>
										
	<%--留空--%>        <div class="col-md-1 col-sm-12 col-xs-12" style="max-width:"></div>	
						
	               </div>
		       </div>
		   </div>		   					
<center><%@ include file="page2.file"%></center>



					
	        <div class="modal fade" id="modal-id">
	            <div class="modal-dialog" style="width:1100px;height:90px;">
	                <div class="modal-content">
	                    <div class="modal-header">
	                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	                        <center><h3 id="issueTitle"></h3></center>
	                    </div>
	                    <div class="modal-body">
	                        <center><h2 id="boardID"></h2></center>   
	                    </div>
	                    <div class="modal-footer">
	                    	<div>
	                    		<center><h4 id="issueContenPhoto"></h4></center>																			                        																					                       																				                                     																						                    
		                    </div> 
		                    <center><button type="button" class="btn btn-default" data-dismiss="modal">Close</button></center>  
	                    </div>
	                </div>
	            </div>
	        </div>			
				
<%}else{%>
			<div align="center">
				<img  src="<%=request.getContextPath()%>/back-end/img/coffee2.png" style="width:400px;height:400px;">
			</div>
			<div align="center">
				<h1><b>此頁審核完畢!! Take a break~</b></h1>
			</div>
			<div align="center">
				<a href="<%=request.getContextPath()%>/back-end/administrator.jsp">
					<button style="background-color:white;border: none;">
						<img  src="<%=request.getContextPath()%>/back-end/img/goback2.png" style="width:300px;height:150px;">
					</button>
				</a>
			</div>	
<%} %>



</body>
</html>