<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="com.issueReport.model.*"%>
<%@ page import="com.issue.model.*"%>
<%@ page import="com.member.model.*"%>

<%
  response.setHeader("Cache-Control","no-store"); //HTTP 1.1
  response.setHeader("Pragma","no-cache");        //HTTP 1.0
  response.setDateHeader ("Expires", 0);
%>

<%
  IssueVO issueVO = (IssueVO) session.getAttribute("issueVO");
  IssueReportVO issueReportVO = (IssueReportVO) request.getAttribute("issueReportVO");
  String issueID = request.getParameter("issueID");
 
  MemberVO memberVO = (MemberVO)session.getAttribute("memberVO");
	String memberID = memberVO.getMemberID(); 
	System.out.println(memberID);
%>

<html>
<head>
<meta charset="UTF-8">
<meta name="description" content="Food Blog Web Template">
<meta name="keywords" content="food, unica, creative, html">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>檢舉話題</title>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
	<!--[if lt IE 9]>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
	<script src="https://code.jquery.com/jquery-1.9.1.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<!-- Google Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Poppins:400,400i,500,500i,600,600i,700" rel="stylesheet">

	<!-- Stylesheets -->
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/css/bootstrap.min.css"/>
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/css/font-awesome.min.css"/>
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/css/owl.carousel.css"/>
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/css/animate.css"/>
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/css/style.css"/>
	
	<!-- original -->			
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/issue/css/bootstrap.min.css">
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/issue/css/bootstrap-theme.min.css">
	
	<script src="<%=request.getContextPath() %>/front-end/issue/js/jquery-3.2.1.min.js"></script>
	
	<script src="<%=request.getContextPath() %>/front-end/issue/js/bootstrap.min.js"></script>
	
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/issue/css/index.css">
<style>
</style>

</head>
<body bgcolor='white'>
	<!-- Header section -->
	<header class="header-section">
		
		<div class="header-bottom">
			<div class="container">
				<a href="<%=request.getContextPath()%>/front-end/index.jsp" class="site-logo">
					<img src="<%=request.getContextPath() %>/front-end/img/logo-1.png" alt="">
				</a>
				<div class="nav-switch">
					<i class="fa fa-bars"></i>
				</div>

				<ul class="main-menu">
					<li><a href="<%= request.getContextPath()%>/front-end/board/listAllBoard.jsp">藝人專板</a></li>
					<li><a href="<%= request.getContextPath()%>/front-end/product/Mall.jsp">商城</a></li>
					
					
					<li>
					<span class="icons">
				     <span class="dropdown">
					 <a href="#" data-toggle="dropdown">會員</a>
					    <ul class="dropdown-menu">
					      <li><a href="<%=request.getContextPath()%>/front-end/member/listOneMember.jsp">會員資料</a></li>
					      <li><a href="#"  onclick="logout()">登出</a>
					    </ul>
				     </span>
				    </span>
					</li>																			
					<li><a href="recipes.html">通知</a></li>
					<li><a href="<%=request.getContextPath()%>/front-end/chatBox/chatBox.jsp">聊天室</a></li>
	
					<li><a href="<%=request.getContextPath()%>/front-end/personalPage/myPersonalPage.jsp">${memberVO.memberNickName}，您好</a></li>	
					<a href="<%=request.getContextPath()%>/front-end/personalPage/myPersonalPage.jsp">	
					<li><img src="<%= request.getContextPath()%>/front-end/member/memberImg.do?memberID=${memberVO.memberID}" id="memberPhoto"></li>		
					</a>
			</div>
		</div>
	</header>
	<!-- Header section end -->
<FORM METHOD="post" ACTION="<%=request.getContextPath()%>/front-end/member/member.do" id="logout">
	<input type="hidden" name="action"	value="logOut">
</FORM>
<script type="text/javascript">
	function logout(){
		document.getElementById("logout").submit();
	}
</script>
	
							<div class="container">
                                 <div class="row">
                                    <div class="col-xs-12 col-sm-6 col-sm-offset-3">
                                    	
 					<%-- 錯誤表列 --%>
										<c:if test="${not empty errorMsgs}">
											
											<ul>
												<c:forEach var="message" items="${errorMsgs}">
													<li style="color:red">${message}</li>
												</c:forEach>
											</ul>
										</c:if>
                                    
										<form METHOD="post" ACTION="<%=request.getContextPath() %>/issueReport/issueReport.do" name="form1" >                                            
											<div class="form-group">
												<label><h3>選擇檢舉事由&nbsp</h3></span></label>											
												<select class="text-select" name="issueReportReason" >
													<option value="重傷、歧視、挑釁或謾罵他人">重傷、歧視、挑釁或謾罵他人</option>
													<option value="惡意洗版、重複張貼">惡意洗版、重複張貼</option>
													<option value="含有色情及暴力內容">含有色情及暴力內容</option>
													<option value="廣告或商業宣傳內容">廣告或商業宣傳內容</option>
													<option value="文章發錯板">文章發錯板</option>
													<option value="OTHER">OTHER(需填寫其他檢舉事由)</option>
												</select>										
											</div>
										                                                                                      
											<div class="form-group">
												<label><h3>其它詳細敘述:&nbsp</h3>  
												<textarea rows="5" name="issueReportContent" style="font-size:31px; border:4px #7dccff solid;"></textarea>
												</label>
											<br>
												<input type="hidden" name="issueID" value="<%=issueID%>">
												<input type="hidden" name="action" value="insert">
												<input type="hidden" name="memberID" value="${memberVO.memberID}" >
												<input type="submit" class="btn btn-danger btn-md" value="送出新增">
												<a href="<%=request.getContextPath()%>/issue/issue.do?issueID=${issueVO.issueID}&action=getOne_For_Display" class="btn btn-primary btn-md">取消</a>
											</form>
											</div>							         
										</div>
									</div>

	<script src="<%=request.getContextPath() %>/front-end/js/jquery-3.2.1.min.js"></script>
	<script src="<%=request.getContextPath() %>/front-end/owl.carousel.min.js"></script>
	<script src="<%=request.getContextPath() %>/front-end/js/main.js"></script>		
   	<script src="https://code.jquery.com/jquery.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
</body>
</html>