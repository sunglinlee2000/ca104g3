<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%
  response.setHeader("Cache-Control","no-store"); //HTTP 1.1
  response.setHeader("Pragma","no-cache");        //HTTP 1.0
  response.setDateHeader ("Expires", 0);
%>

<html>
<head>
<title> IssueReport: Home</title>

<style>
  table#table-1 {
	width: 450px;
	background-color: #CCCCFF;
	margin-top: 5px;
	margin-bottom: 10px;
    border: 3px ridge Gray;
    height: 80px;
    text-align: center;
  }
  table#table-1 h4 {
    color: red;
    display: block;
    margin-bottom: 1px;
  }
  h4 {
    color: blue;
    display: inline;
  }
</style>

</head>
<body bgcolor='white'>

<table id="table-1">
   <tr><td><h3>IssueReport: Home</h3><h4>( MVC )</h4></td></tr>
</table>

<p>This is the Home page for IssueReport: Home</p>

<h3>資料查詢:</h3>
	
<%-- 錯誤表列 --%>
<c:if test="${not empty errorMsgs}">
	<font style="color:red">請修正以下錯誤:</font>
	<ul>
	    <c:forEach var="message" items="${errorMsgs}">
			<li style="color:red">${message}</li>
		</c:forEach>
	</ul>
</c:if>

<ul>
  <li><a href='<%=request.getContextPath() %>/front-end/issueReport/listAllEmp.jsp'>List</a> all Comments.  <br><br></li>
  
  
  <li>
    <FORM METHOD="post" ACTION="<%=request.getContextPath() %>/issueReport/issueReport.do" >
        <b>輸入檢舉編號 (如IR000001):</b>
        <input type="text" name="issueReportID">
        <input type="hidden" name="action" value="getOne_For_Display">
        <input type="submit" value="送出">
    </FORM>
  </li>

  <jsp:useBean id="issueReportSvc" scope="page" class="com.issueReport.model.IssueReportService" />
   
  <li>
     <FORM METHOD="post" ACTION="<%=request.getContextPath() %>/issueReport/issueReport.do" >
       <b>選擇檢舉編號:</b>
       
       <select size="1" name="issueReportID">
         <c:forEach var="issueReportVO" items="${issueReportSvc.all}" > 
          <option value="${issueReportVO.issueReportID}">${issueReportVO.issueReportID}
         </c:forEach>   
       </select>
       <input type="hidden" name="action" value="getOne_For_Display">
       <input type="submit" value="送出">
    </FORM>
  </li>
  
  <li>
     <FORM METHOD="post" ACTION="<%=request.getContextPath() %>/issueReport/issueReport.do" >
       <b>選擇會員編號:</b>
       <select size="1" name="">
         <c:forEach var="issueReportVO" items="${issueReportSvc.all}" > 
          <option value="${issueReportVO.issueReportID}">${issueReportVO.memberID}
         </c:forEach>   
       </select>
       <input type="hidden" name="action" value="getOne_For_Display">
       <input type="submit" value="送出">
     </FORM>
  </li>
</ul>


<h3>留言管理</h3>

<ul>
  <li><a href='addEmp.jsp'>Add</a> a new IssueReport.</li>
</ul>

</body>
</html>