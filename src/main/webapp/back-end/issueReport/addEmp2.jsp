<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="com.issueReport.model.*"%>
<%@ page import="com.issue.model.*"%>

<%
  response.setHeader("Cache-Control","no-store"); //HTTP 1.1
  response.setHeader("Pragma","no-cache");        //HTTP 1.0
  response.setDateHeader ("Expires", 0);
%>

<%
  IssueReportVO issueReportVO = (IssueReportVO) request.getAttribute("issueReportVO");
  String issueID = request.getParameter("issueID");
 
%>

<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
<title>新增話題檢舉</title>

<style>
  table#table-1 {
	background-color: #CCCCFF;
    border: 2px solid black;
    text-align: center;
  }
  table#table-1 h4 {
    color: red;
    display: block;
    margin-bottom: 1px;
  }
  h4 {
    color: blue;
    display: inline;
  }
</style>

<style>
  table {
	width: 450px;
	background-color: white;
	margin-top: 1px;
	margin-bottom: 1px;
  }
  table, th, td {
    border: 0px solid #CCCCFF;
  }
  th, td {
    padding: 1px;
  }
</style>

</head>
<body bgcolor='white'>

<%-- 錯誤表列 --%>
<c:if test="${not empty errorMsgs}">
	<font style="color:red">請修正以下錯誤:</font>
	<ul>
		<c:forEach var="message" items="${errorMsgs}">
			<li style="color:red">${message}</li>
		</c:forEach>
	</ul>
</c:if>

<FORM METHOD="post" ACTION="<%=request.getContextPath() %>/issueReport/issueReport.do" name="form1" enctype="multipart/form=data">
<table>
	
	<tr>
		<td>選擇檢舉事由:</td>
		
		<td><select class="text-select" name="issueReportReason" >
			<option value="重傷、歧視、挑釁或謾罵他人">重傷、歧視、挑釁或謾罵他人</option>
			<option value="惡意洗版、重複張貼">惡意洗版、重複張貼</option>
			<option value="含有色情及暴力內容">含有色情及暴力內容</option>
			<option value="廣告或商業宣傳內容">廣告或商業宣傳內容</option>
			<option value="文章發錯板">文章發錯板</option>
			<option value="OTHER">OTHER(需填寫其他檢舉事由)</option>
			
		</select></td>
	</tr>	
	<tr>
		<td>詳細敘述:</td>
		
		<td><textarea rows="10" name="issueReportContent"></textarea></td>
	</tr>	
	

</table>
<br>
<input type="hidden" name="issueID" value="<%=issueID%>">
<input type="hidden" name="action" value="insert">
<input type="submit" value="送出新增">
</FORM>
</body>
</html>