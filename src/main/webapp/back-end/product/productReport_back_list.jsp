<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page import="java.util.*"%>
<%@ page import="com.productReport.model.*"%>
<%@ page import="com.product.model.*"%>
<%@ page import="com.product.controller.*"%>



<%
	
	ProductReportService prSvc=new ProductReportService();
    List<ProductReportVO> list = prSvc.getAll();
    pageContext.setAttribute("list",list);
    Integer count = prSvc.getCount();
    System.out.print(count);
    
//     ProductService productSvc=new ProductService();
//     List<ProductVO> productList=productSvc.getAllByProductReportID();
//     pageContext.setAttribute("productList",productList);
%>

<html lang="">
<head>
<title>商品檢舉管理列表</title>
<meta charset="UTF-8">
	<meta name="description" content="Food Blog Web Template">
	<meta name="keywords" content="food, unica, creative, html">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- Favicon -->   
	<link href="<%=request.getContextPath()%>/front-end/img/favicon.ico" rel="shortcut icon"/>

	<!-- Google Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Poppins:400,400i,500,500i,600,600i,700" rel="stylesheet">

	<!-- Stylesheets -->
	<link rel="stylesheet" href="<%=request.getContextPath()%>/front-end/css/bootstrap.min.css"/>
	<link rel="stylesheet" href="<%=request.getContextPath()%>/front-end/css/font-awesome.min.css"/>
	<link rel="stylesheet" href="<%=request.getContextPath()%>/front-end/css/owl.carousel.css"/>
	<link rel="stylesheet" href="<%=request.getContextPath()%>/front-end/css/animate.css"/>
	<link rel="stylesheet" href="<%=request.getContextPath()%>/front-end/css/style.css"/>
	<link rel="stylesheet" href="<%=request.getContextPath()%>/front-end/member/css/styleLogin.css">
	<script src="<%=request.getContextPath() %>/front-end/js/owl.carousel.min.js"></script>
	<script src="https://code.jquery.com/jquery.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<style type="text/css">
body{
    font-family:\5FAE\8EDF\6B63\9ED1\9AD4,\65B0\7D30\660E\9AD4;
}

.kk-adfund-table {
	margin-top: 20px;
	text-align: center;
	font-size: 13px;
}

.kk-adfund-table thead th {
	margin-top: 20px;
	text-align: center;
	background-color: #DCDCDC;
}

.btn-adfund-back {
	background-color: #9999cc;
	border: 1px solid;
	border-color: #DCDCDC;
	padding: 3px;
	color: white;
	font-size: 11px;
}
</style>

</head>
<body>
<!-- Header section -->
	<header class="header-section">
		
		<div class="header-bottom">
			<div class="container">
			
	<!-- 返回後台首頁							 -->
				<a href="<%=request.getContextPath()%>/back-end/administrator.jsp">
					<button style="background-color:white;border: none;">
						<img  src="<%=request.getContextPath() %>/front-end/img/logo-1.png">
					</button>
				</a>
	<!-- 返回後台首頁end							 -->			
				
				<div class="nav-switch">
					<i class="fa fa-bars"></i>
				</div>
				
				<ul class="main-menu">
			
					<li>Super Administrator，你好</li>
					<li>
						<i><a href="<%=request.getContextPath()%>/back-end/login.jsp"  onclick="logout()"><img src="<%=request.getContextPath() %>/back-end/img/signout_106525.png"></a></i>						
					</li>
				</ul>
			</div>
		</div>
	</header>
<!-- Header section end -->
        <!-- Page Preloder -->
	
	<div class="container kk-ad-back-con">
		<div class="row">
			
			<div class="col-xs-12 col-sm-12 ">
			<center>
				<h2>商品檢舉管理</h2>

				<table class="table table-hover table-striped table-condensed table-bordered kk-adfund-table">

					<%@ include file="page1.file"%>
				</center>
					<thead class="kk-adfund-thead">
						<tr>
							<th>商品檢舉編號</th>
							<th>會員編號(檢舉人)</th>
							<th>商品編號</th>
							<th>檢舉時間</th>
							<th>審核狀態</th>
							<th>檢舉事由</th>
							<th>查看商品詳情</th>
							<th>審核</th>
							<th></th>
						</tr>
						
					</thead>

					<tbody>
						<c:forEach var="prVO" items="${list}" begin="<%=pageIndex%>" end="<%=pageIndex+rowsPerPage-1%>">
							
							<script>
// 								審核狀態
								<c:if test="${prVO.productReportStatus=='REPORT_UNCHECKED'}">
									$(document).ready(function(){
										$('#status${prVO.productreportID}').html('未審核');
									});
								</c:if>
									<c:if test="${prVO.productReportStatus=='REPORT_ACCEPTED'}">
									$(document).ready(function(){
										$('#status${prVO.productreportID}').html('審核通過');
									});
								</c:if>
								<c:if test="${prVO.productReportStatus=='REPORT_REJECTED'}">
									$(document).ready(function(){
										$('#status${prVO.productreportID}').html('審核駁回');
									});
								</c:if>
// 								檢舉事由
								<c:if test="${prVO.productReportReason=='IMAGE_WRONG'}">
									$(document).ready(function(){
										$('#reason${prVO.productreportID}').html('圖文不符');
									});
								</c:if>
								<c:if test="${prVO.productReportReason=='ARTIST_WRONG'}">
									$(document).ready(function(){
										$('#reason${prVO.productreportID}').html('非明星相關商品');
									});
								</c:if>
								<c:if test="${prVO.productReportReason=='SEX'}">
								$(document).ready(function(){
									$('#reason${prVO.productreportID}').html('含有色情暴力成分');
								});
								</c:if>
								//按鍵
								<c:if test="${prVO.productReportStatus=='REPORT_ACCEPTED' || prVO.productReportStatus=='REPORT_REJECTED'}">
								$(document).ready(function(){
									$('#submit${prVO.productreportID}').prop("disabled", true);
								});
								</c:if>
							</script>
							<tr>
								<td>${prVO.productreportID}</td>
								<td>${prVO.memberID}</td>
								<td>${prVO.productID}</td>
								<td><fmt:formatDate value="${prVO.productReportDate}" pattern="YYYY-MM-dd HH:mm" /></td>
								<td id="status${prVO.productreportID}">${prVO.productReportStatus}</td>
								<td id="reason${prVO.productreportID}">${prVO.productReportReason}</td>
								<td>
										<input type="submit" id="look${prVO.productreportID}" class="btn btn-adfund-back" value="查看詳情">
								</td>
								<td>
									<select id="productReportStatus${prVO.productreportID}">
										<option value="REPORT_ACCEPTED">審核通過</option>
										<option value="REPORT_REJECTED">審核駁回</option>
									</select>
								</td>
								<td>
									<input type="submit" class="btn btn-warning" value="送出" id="submit${prVO.productreportID}">
								</td>
							</tr>
					</tbody>
					<script>
					$(document).ready(function() {
						$('#look${prVO.productreportID}').click(function(){
							$.ajax({ 
							     url : '<%=request.getContextPath()%>/product/product.do', 
							     type : 'Post', 
							     data : { 
							      action : 'getOne_For_Display', 
							      productID : '${prVO.productID}' 
							     } 
							    }).done(function(res){
							    	var obj = JSON.parse(res);
							    	var temp = obj.productID1;
							    	console.log(temp);
							    	$('#imgAppend').html("<img class='d-block w-100' src='<%=request.getContextPath()%>/product/productImg.do?productID=" + obj.productID1 + "' alt='First slide' style='width:450px;height:400px;'>");
							    	$('#productNameAppend').text(obj.productName);
							    	$('#productInfoAppend').text(obj.productInfo);
							    	$('#productPriceAppend').text(obj.productPrice);
							    	$('#modalQuickView').modal();
							     });
						});
						$('#submit${prVO.productreportID}').click(function(){
							$.ajax({ 
							     url : '<%=request.getContextPath()%>/productReport/productReport.do', 
							     type : 'Post', 
							     data : { 
							      action : 'update_For_Status', 
							      productreportID : '${prVO.productreportID}',
							      productReportStatus:$('#productReportStatus${prVO.productreportID}').val(),
							      productID : '${prVO.productID}'
							     } 
							    }).done(function(res){
							    	var obj = JSON.parse(res);
							    	if(obj.productReportStatus=="REPORT_ACCEPTED"){
							    		$('#status${prVO.productreportID}').html('審核通過');
							    	}else{
							    		$('#status${prVO.productreportID}').html('審核駁回');
							    	}
							    	$('#submit${prVO.productreportID}').prop("disabled", true);
							    	
							     });
						});
				});
					</script>
							
					</c:forEach>
				</table>
						<center><%@ include file="page2.file"%></center>
					
					
			</div>
		</div>
	</div>

					<div class="modal fade" id="modalQuickView" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
								  <div class="modal-dialog modal-lg" role="document">
								    <div class="modal-content" style="background-color:#fbfffd">
								      <div class="modal-body">
								        <div class="row">
								          <div class="col-xs-12 col-sm-5">
								            <div id="carousel-thumb" class="carousel slide carousel-fade carousel-thumbnails" data-ride="carousel">
								             
								              <div class="carousel-inner" role="listbox">
								                <div class="carousel-item active" id="imgAppend">
								                
								                </div>
								              </div>
								            </div>
								          </div>
								         <div class="col-xs-12 col-sm-7">
								          <div>
								            <h2 class="h2-responsive product-name" id="productNameAppend">
								              
								            </h2>
								          </div>
								          <div style="padding-top: 20px;">
								            <h4>
								              <span>
								                <strong>商品詳情:</strong>
								              </span>
								            </h4>
								          </div>
								          <div style="padding-top: 15px;">
								          	<p id="productInfoAppend">
								          		
								          	</p>
								          </div>
								          <div style="padding-top: 20px;">
								            <h4>
						            			價格:		
								              <span style="color:red;font-size: 50px;padding-left: 15px;">
								                <strong id="productPriceAppend">
								                		
								                </strong>
								              </span>
								               		 元
								            </h4>
								          </div>
								          <div style="padding-top: 20px;">
								          <div>
								           </div>
								          </div>
								        </div>
								      </div>
								    </div>
								  </div>
								</div>
							</div>


	
</body>
</html>
