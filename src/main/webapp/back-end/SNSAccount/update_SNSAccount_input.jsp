<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="java.util.*"%>
<%@ page import="com.SNSAccount.model.*"%>

<%
	SNSAccountVO snsAccountVO = (SNSAccountVO) request.getAttribute("snsAccountVO");
%>
<!DOCTYPE html>
<html>
<head>
<title>FanPool</title>
<meta charset="UTF-8">
<meta name="description" content="Food Blog Web Template">
<meta name="keywords" content="food, unica, creative, html">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- Favicon -->   
<link href="<%=request.getContextPath()%>/front-end/img/favicon.ico" rel="shortcut icon"/>
<!-- Google Fonts -->
<link href="https://fonts.googleapis.com/css?family=Poppins:400,400i,500,500i,600,600i,700" rel="stylesheet">
<!-- Stylesheets -->
<link rel="stylesheet" href="<%=request.getContextPath()%>/back-end/css/bootstrap.min.css">
<link rel="stylesheet" href="<%=request.getContextPath()%>/back-end/css/font-awesome.min.css">
<link rel="stylesheet" href="<%=request.getContextPath()%>/back-end/css/owl.carousel.css">
<link rel="stylesheet" href="<%=request.getContextPath()%>/back-end/css/animate.css">
<link rel="stylesheet" href="<%=request.getContextPath()%>/back-end/css/style.css">
<link rel="stylesheet" href="<%=request.getContextPath()%>/back-end/SNSAccount/css/SNSAccount.css">
<script src="<%=request.getContextPath()%>/back-end/js/jquery-3.2.1.min.js"></script>
<script src="<%=request.getContextPath()%>/back-end/js/owl.carousel.min.js"></script>
<script src="<%=request.getContextPath()%>/back-end/js/main.js"></script>

<style type="text/css">
	body{
		font-family:\5FAE\8EDF\6B63\9ED1\9AD4,\65B0\7D30\660E\9AD4;
	}

</style>
<style type="text/css">
body{
    font-family:\5FAE\8EDF\6B63\9ED1\9AD4,\65B0\7D30\660E\9AD4;
}

.kk-adfund-table {
	margin-top: 20px;
	text-align: center;
	font-size: 13px;
}

.kk-adfund-table thead th {
	margin-top: 20px;
	text-align: center;
	background-color: #DCDCDC;
}

.btn-adfund-back {
	background-color: #9999cc;
	border: 1px solid;
	border-color: #DCDCDC;
	padding: 3px;
	color: white;
	font-size: 11px;
}
</style>

</head>
<body>

	<div class="container-fiuld kk-ad-back-con">
		<div class="row">
		   <div class="col-xs-12 col-sm-4">
			</div>
			<div class="col-xs-12 col-sm-4 ">
 			<h3>藝人帳號資料修改</h3>	

					<FORM METHOD="post" ACTION="<%=request.getContextPath()%>/back-end/SNSAccount/SNSAccount.do">
							<table style="width:100%" class="table table-hover table-striped table-condensed table-bordered kk-adfund-table">
								<tr>
									<td>藝人帳號編號 :</td>
									<td><%= snsAccountVO.getSnsAccountID()%></td>
								</tr>
								<tr>
									<td>藝人編號 :</td>
									<td><%= snsAccountVO.getArtistID()%></td>
								</tr>
								<tr>
									<td>社群帳號 :</td>
									<td><input type="TEXT" name="snsAccount" size="45"
										 value="<%= (snsAccountVO==null)? "請輸入姓名" : snsAccountVO.getSnsAccount()%>" /></td>
								</tr>
								<tr>
									<td>社群帳號連結 :</td>
									<td><input type="TEXT" name="snsLink" size="45"
										 value="<%= (snsAccountVO==null)? "請輸入姓名" : snsAccountVO.getSnsLink()%>" /></td>
								</tr>
								<tr>
									<td>社群帳號類型 :</td>
									<td>
										<select name="snsAccountType">
											<option value="IG">IG</option>
											<option value="TWITTER">TWITTER</option>
										</select>
									</td>
								</tr>
							
							</table>
							<br>
							<input type="hidden" name="snsAccountID" value="<%= snsAccountVO.getSnsAccountID()%>">
							<input type="hidden" name="artistID" value="<%= snsAccountVO.getArtistID()%>">
							<input type="hidden" name="action" value="update">
							<div align="center">
								<input type="submit" value="送出修改" class="btn btn-adfund-back">
							</div>
							</FORM>
			</div>
		</div>
	</div>	
	<!--====== Javascripts & Jquery ======-->
   	<script src="<%=request.getContextPath()%>/back-end/js/jquery-3.2.1.min.js"></script>
	<script src="<%=request.getContextPath()%>/back-end/js/owl.carousel.min.js"></script>
	<script src="<%=request.getContextPath()%>/back-end/js/main.js"></script>
</body>
</html>