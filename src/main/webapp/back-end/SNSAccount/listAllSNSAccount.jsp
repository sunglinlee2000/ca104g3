<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="java.util.*"%>
<%@ page import="com.SNSAccount.model.*"%>

<%
	SNSAccountService SNSAccountSvc = new SNSAccountService();
	List<SNSAccountVO> list = SNSAccountSvc.getAllSNSAccount();
	pageContext.setAttribute("list",list);
	
%>
<!DOCTYPE html>
<html>
<head>
<title>FanPool</title>
<meta charset="UTF-8">
<meta name="description" content="Food Blog Web Template">
<meta name="keywords" content="food, unica, creative, html">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- Favicon -->   
<link href="<%=request.getContextPath()%>/front-end/img/favicon.ico" rel="shortcut icon"/>
<!-- Google Fonts -->
<link href="https://fonts.googleapis.com/css?family=Poppins:400,400i,500,500i,600,600i,700" rel="stylesheet">
<!-- Stylesheets -->
<link rel="stylesheet" href="<%=request.getContextPath()%>/back-end/css/bootstrap.min.css">
<link rel="stylesheet" href="<%=request.getContextPath()%>/back-end/css/font-awesome.min.css">
<link rel="stylesheet" href="<%=request.getContextPath()%>/back-end/css/owl.carousel.css">
<link rel="stylesheet" href="<%=request.getContextPath()%>/back-end/css/animate.css">
<link rel="stylesheet" href="<%=request.getContextPath()%>/back-end/css/style.css">
<link rel="stylesheet" href="<%=request.getContextPath()%>/back-end/SNSAccount/css/SNSAccount.css">
<script src="<%=request.getContextPath()%>/back-end/js/jquery-3.2.1.min.js"></script>
<script src="<%=request.getContextPath()%>/back-end/js/owl.carousel.min.js"></script>
<script src="<%=request.getContextPath()%>/back-end/js/main.js"></script>

<style type="text/css">
	body{
		font-family:\5FAE\8EDF\6B63\9ED1\9AD4,\65B0\7D30\660E\9AD4;
	}

</style>
<style type="text/css">
body{
    font-family:\5FAE\8EDF\6B63\9ED1\9AD4,\65B0\7D30\660E\9AD4;
}

.kk-adfund-table {
	margin-top: 20px;
	text-align: center;
	font-size: 13px;
}

.kk-adfund-table thead th {
	margin-top: 20px;
	text-align: center;
	background-color: #DCDCDC;
}

.btn-adfund-back {
	background-color: #9999cc;
	border: 1px solid;
	border-color: #DCDCDC;
	padding: 3px;
	color: white;
	font-size: 11px;
}
</style>
</head>
<body>
<!-- Header section -->
	<header class="header-section">
		
		<div class="header-bottom">
			<div class="container">
			
	<!-- 返回後台首頁							 -->
				<a href="<%=request.getContextPath()%>/back-end/administrator.jsp">
					<button style="background-color:white;border: none;">
						<img  src="<%=request.getContextPath() %>/front-end/img/logo-1.png">
					</button>
				</a>
	<!-- 返回後台首頁end							 -->			
				
				<div class="nav-switch">
					<i class="fa fa-bars"></i>
				</div>
				
				<ul class="main-menu">
			
					<li>Super Administrator，你好</li>
					<li>
						<i><a href="<%=request.getContextPath()%>/back-end/login.jsp"  onclick="logout()"><img src="<%=request.getContextPath() %>/back-end/img/signout_106525.png"></a></i>						
					</li>
				</ul>
			</div>
		</div>
	</header>
<!-- Header section end -->

			


		
	<div class="container-fiuld kk-ad-back-con">
		<div class="row">
		   <div class="col-xs-12 col-sm-4">
			</div>
			<div class="col-xs-12 col-sm-4 ">
 			<h3>藝人帳號列表</h3>


				<table
					class="table table-hover table-striped table-condensed table-bordered kk-adfund-table">


					<thead class="kk-adfund-thead">
						<tr>
							<th>藝人編號</th>
							<th>社群帳號</th>
							<th>社群連結</th>
							<th>社群種類</th>
							<th>修改</th>
						</tr>
					</thead>

					<tbody>
						<c:forEach var="SNSAccountVO" items="${list}">	
							<tr>
								<td>${SNSAccountVO.artistID}</td>
								<td>${SNSAccountVO.snsAccount}</td>
								<td>${SNSAccountVO.snsLink}</td>
								<td>${SNSAccountVO.snsAccountType}</td>
								<td>
									<FORM METHOD="post" ACTION="<%=request.getContextPath()%>/back-end/SNSAccount/SNSAccount.do">
										<input type="hidden" name="snsAccountID"  value="${SNSAccountVO.snsAccountID}">
										<input type="hidden" name="action"	value="getOne_For_Update">
										<input type="submit" value="修改" class="btn btn-adfund-back">
									</FORM>
								</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</div>
	</div>
		<div align="center">
				<a href="<%=request.getContextPath()%>/back-end/SNSAccount/addSNSAccount.jsp"><input type="button" value="新增帳號" class="btn btn-adfund-back"></a>
			<hr>
			<FORM METHOD="post" ACTION="<%=request.getContextPath()%>/back-end/SNSAccount/SNSAccount.do">
				<input type="hidden" name="action"	value="getSNS">
				<input type="submit" value="爬起來" class="btn btn-adfund-back">
			</FORM>
		</div>	
	
	<!--====== Javascripts & Jquery ======-->
   	<script src="<%=request.getContextPath()%>/back-end/js/jquery-3.2.1.min.js"></script>
	<script src="<%=request.getContextPath()%>/back-end/js/owl.carousel.min.js"></script>
	<script src="<%=request.getContextPath()%>/back-end/js/main.js"></script>
</body>
</html>