<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page import="com.member.model.*"%>

<%
	MemberVO memberVO = (MemberVO) session.getAttribute("memberVO");
%>


<!DOCTYPE html>
<html>
<head>
<title>FanPool</title>
<meta charset="UTF-8">
<meta name="description" content="Food Blog Web Template">
<meta name="keywords" content="food, unica, creative, html">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- Favicon -->   
<link href="<%=request.getContextPath()%>/front-end/img/favicon.ico" rel="shortcut icon"/>
<!-- Google Fonts -->
<link href="https://fonts.googleapis.com/css?family=Poppins:400,400i,500,500i,600,600i,700" rel="stylesheet">
<!-- Stylesheets -->
<link rel="stylesheet" href="<%=request.getContextPath()%>/front-end/css/bootstrap.min.css">
<link rel="stylesheet" href="<%=request.getContextPath()%>/front-end/css/font-awesome.min.css">
<link rel="stylesheet" href="<%=request.getContextPath()%>/front-end/css/owl.carousel.css">
<link rel="stylesheet" href="<%=request.getContextPath()%>/front-end/css/animate.css">
<link rel="stylesheet" href="<%=request.getContextPath()%>/front-end/css/style.css">
<link rel="stylesheet" href="<%=request.getContextPath()%>/front-end/board/css/styleBoard.css">
<link rel="stylesheet" href="<%=request.getContextPath()%>/front-end/member/css/styleLogin.css">
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="<%=request.getContextPath()%>/front-end/js/jquery-3.2.1.min.js"></script>
<script src="<%=request.getContextPath()%>/front-end/js/owl.carousel.min.js"></script>
<script src="<%=request.getContextPath()%>/front-end/js/main.js"></script>
<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/adfund/css/bootstrap-theme.min.css">
<script src="<%=request.getContextPath() %>/front-end/adfund/js/bootstrap.min.js"></script>
<style type="text/css">
	body{
		font-family:\5FAE\8EDF\6B63\9ED1\9AD4,\65B0\7D30\660E\9AD4;
	}

</style>
</head>
<body>
	<div id="preloder">
		<div class="loader"></div>
	</div>

<!-- Header section --> 
 <header class="header-section">	
  <div class="header-bottom"> 
   <div class="container"> 
    <a href="<%=request.getContextPath()%>/front-end/index.jsp" class="site-logo"> 
     <img src="<%=request.getContextPath() %>/front-end/img/logo-1.png" alt=""> 
    </a> 
    <div class="nav-switch"> 
     <i class="fa fa-bars"></i> 
    </div> 

    <ul class="main-menu"> 
     <li><a href="<%= request.getContextPath()%>/front-end/board/listAllBoard.jsp"><img alt="藝人專板"  title="藝人專板" src="<%= request.getContextPath()%>/front-end/res/img/index/board.png" style="height:30px;width:30px"></a></li> 
     <li><a href="<%= request.getContextPath()%>/front-end/product/Mall.jsp"><img alt="商城"  title="商城" src="<%= request.getContextPath()%>/front-end/res/img/index/mall.png" style="height:30px;width:30px"></a></li> 
     <li><a href="<%=request.getContextPath()%>/front-end/gif_collection/gif_list.jsp"><img alt="GIF"  title="GIF" src="<%= request.getContextPath()%>/front-end/res/img/index/gif.png" style="height:30px;width:30px"></a></li> 
     <li><a href="<%=request.getContextPath()%>/front-end/member/listOneMember.jsp"><img alt="會員資料" title="會員資料" src="<%= request.getContextPath()%>/front-end/res/img/index/user.png" style="height:30px;width:30px"></a></li> 
     <li><a href="#" onclick="logout()"><img alt="登出" title="登出" src="<%= request.getContextPath()%>/front-end/res/img/index/logout.png" style="height:30px;width:30px"></a> 
     <li><a href="<%=request.getContextPath()%>/front-end/chatBox/chatBox.jsp"><img alt="聊天室" title="聊天室" src="<%= request.getContextPath()%>/front-end/res/img/index/chat.png" style="height:30px;width:30px"></a></li> 
     <li>${memberVO.memberNickName }</li>
     <li><a href="<%=request.getContextPath()%>/front-end/personalPage/myPersonalPage.jsp"><img src="<%= request.getContextPath()%>/front-end/member/memberImg.do?memberID=${memberVO.memberID}" id="memberPhoto"></a></li>	
     <li id="notifi"><a href="#modal-id-notifi" data-toggle="modal"> 
      <div style="position: relative;"> 
      <img src="<%=request.getContextPath()%>/front-end/res/img/index/notification(32).png" alt="通知" id="notifiPhoto"> 
      <span style="position: absolute; top: 0; left: 17px; color: white" id="notifiCount"></span> 
      </div> 
     </a></li> 
     </ul>
   </div> 
  </div> 
 </header> 
 <!-- Header section end -->
<FORM METHOD="post" ACTION="<%=request.getContextPath()%>/front-end/member/member.do" id="logout">
	<input type="hidden" name="action"	value="logOut">
</FORM>
<script type="text/javascript">
	function logout(){
		document.getElementById("logout").submit();
	}
</script>


	<!-- Footer widgets section -->
	<section class="bottom-widgets-section spad">
		<div class="container">
			<div class="row">
				<div class="col-lg-2 col-md-6 ftw-warp">
					<div class="section-title">
						<h3>${memberVO.memberName }會員資料</h3>
					</div>

						<div class="rl-info">
							<FORM METHOD="post"	ACTION="<%=request.getContextPath()%>/front-end/member/member.do">
								<input type="hidden" name="memberID" value="${memberVO.memberID}">
								<input type="hidden" name="action" value="getOne_For_Update">
								<input type="submit" value="修改資料" style="background-color: #7dccff;border-radius: 8px;font-size: 16px; padding: 8px 16px;border:0px;">
							</FORM>
						</div>
						<br>
						<div class="rl-info">
							<FORM METHOD="post"	ACTION="<%=request.getContextPath()%>/front-end/member/member.do">
								<input type="hidden" name="memberID" value="${memberVO.memberID}">
								<input type="hidden" name="memberPhone" value="${memberVO.memberPhone }">
								<input type="hidden" name="action" value="checkPhone">
								<input type="submit" value="驗證手機" style="background-color: #7dccff;border-radius: 8px;font-size: 16px; padding: 8px 16px;border:0px;" id="checkPhoneBtn">
							</FORM>
						</div>
						<br>
						<div class="rl-info">
							<FORM METHOD="post"	ACTION="<%=request.getContextPath()%>/front-end/member/member.do">
								<input type="hidden" name="memberID" value="${memberVO.memberID}">
								<input type="text" name="checkNum" id="checkNumInput1" style="display:none">
								<input type="hidden" name="action" value="confirmNum">
								<input type="submit" value="送出驗證碼"  id="checkNumInput2" style="background-color: #7dccff;border-radius: 8px;font-size: 16px; padding: 8px 16px;border:0px;display:none">
							</FORM>
						</div>
						<br>
						<div class="rl-info">
							<FORM METHOD="post"	ACTION="<%=request.getContextPath()%>/front-end/member/member.do">
								<input type="hidden" name="memberID" value="${memberVO.memberID}">
								<input type="hidden" name="memberEmail" value="${memberVO.memberEmail }">
								<input type="hidden" name="memberBankAccount" value="${memberVO.memberBankAccount }">
								<input type="hidden" name="action" value="checkAccount">
								<input type="submit" value="驗證帳號"  id="checkBankBtn" style="background-color: #7dccff;border-radius: 8px;font-size: 16px; padding: 8px 16px;border:0px;">
							</FORM>
						</div>
						<div class="rl-info">
							<FORM METHOD="post"	ACTION="<%=request.getContextPath()%>/front-end/member/member.do">
								<input type="hidden" name="memberID" value="${memberVO.memberID}">
								<input type="text" name="checkNum" id="checkNumInput3" style="display:none">
								<input type="hidden" name="action" value="confirmNumBank">
								<input type="submit" value="送出驗證碼"  id="checkNumInput4" style="background-color: #7dccff;border-radius: 8px;font-size: 16px; padding: 8px 16px;border:0px;display:none">
							</FORM>
						</div>
				</div>
				<div class="col-lg-4 col-md-6 ftw-warp">
					<ul class="sp-recipes-list">
						<li>
							<div class="rl-info">
								<span>暱稱</span>
								<h6>${memberVO.memberNickName }</h6>
							</div>
						</li>
						<li>
							<div class="rl-info">
								<span>信箱</span>
								<h6>${memberVO.memberEmail }</h6>
							</div>
						</li>
						<li>
							<div class="rl-info">
								<span>性別</span>
								<h6>${memberVO.memberSex }</h6>
							</div>
						</li>
						<li>
							<div class="rl-info">
								<span>生日</span>
								<h6><fmt:formatDate value="${memberVO.memberBirthday }" pattern="YYYY-MM-dd"/></h6>
							</div>
						</li>
						<li>
							<div class="rl-info">
								<span>手機</span>
								<h6>${memberVO.memberPhone }</h6>
							</div>
						</li>
					</ul>
				</div>
				<div class="col-lg-4 col-md-6 ftw-warp">
					<ul class="sp-recipes-list">
						<li>
							<div class="rl-info">
								<span>銀行帳號</span>
								<h6>${memberVO.memberBankAccount }</h6>
							</div>
						</li>
						<li>
							<div class="rl-info">
								<span>地址</span>
								<h6>${memberVO.memberAddress }</h6>
							</div>
						</li>
						<li>
							<div class="rl-info">
								<span>代幣</span>
								<h6>${memberVO.memberCoin }</h6>
							</div>
						</li>
						<li>
							<div class="rl-info">
								<span>加入日</span>
								<h6><fmt:formatDate value="${memberVO.memberJoinDate }" pattern="YYYY-MM-dd HH:mm"/></h6>
							</div>
						</li>
						<li>
							<div class="rl-info">
								<span>狀態</span>
								<h6>${memberVO.memberStatus }</h6>
							</div>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</section>
	<!-- Footer widgets section end -->




	<!-- Footer section  -->
	<footer class="footer-section set-bg" data-setbg="">
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-6">
					<div class="footer-logo">
						<img src="<%=request.getContextPath()%>/front-end/img/logo-1.png" alt="">
					</div>
				</div>
				<div class="col-lg-6 text-lg-right">
					
					<p class="copyright">
						電話： 03-4257387
						<br>
						地址： 320桃園市中壢區中大路300號
					</p>
				</div>
			</div>
		</div>
	</footer>
	<!-- Footer section end -->
<script>
if("${memberVO.memberStatus}"==="買家"||"${memberVO.memberStatus}"==="賣家"){
	document.getElementById("checkPhoneBtn").style.display = "none";		
}
if(<%=(Integer)request.getAttribute("numPhone")%>!= null){
	document.getElementById("checkPhoneBtn").style.display = "none";	
	document.getElementById("checkNumInput1").style.display = "" ;
	document.getElementById("checkNumInput2").style.display = "" ;
}
if(<%=(Boolean)request.getAttribute("Phonefail")%>==true){
	swal({
	   title: 'Oops...',
	   text: '驗證碼錯誤,請重新思考人生'
	})}
if(<%=(Boolean)request.getAttribute("Phonefail")%>==false){
	swal({
		text: "驗證成功，恭喜您成為買家，現在可以到商城進行購物囉!"
	})}
	
if("${memberVO.memberStatus}"==="一般會員"||"${memberVO.memberStatus}"==="賣家"){
	document.getElementById("checkBankBtn").style.display = "none";		
}	
if(<%=(Integer)request.getAttribute("num")%>!= null){
	document.getElementById("checkBankBtn").style.display = "none";	
	document.getElementById("checkNumInput3").style.display = "" ;
	document.getElementById("checkNumInput4").style.display = "" ;
}
if(<%=(Boolean)request.getAttribute("Bankfail")%>==true){
	swal({
	   title: 'Oops...',
	   text: '驗證碼錯誤,請重新思考人生'
	})}
if(<%=(Boolean)request.getAttribute("Bankfail")%>==false){
	swal({
		text: "驗證成功，恭喜您成為賣家!"
	})}
</script>


	<!--====== Javascripts & Jquery ======-->
   	<script src="<%=request.getContextPath()%>/front-end/js/jquery-3.2.1.min.js"></script>
	<script src="<%=request.getContextPath()%>/front-end/js/owl.carousel.min.js"></script>
	<script src="<%=request.getContextPath()%>/front-end/js/main.js"></script>
	<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
</body>
</html>