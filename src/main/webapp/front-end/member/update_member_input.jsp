<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="com.member.model.*"%>

<%
	MemberVO memberVO = (MemberVO) request.getAttribute("memberVO");
%>
<!DOCTYPE html>
<html>
<head>
<title>FanPool</title>
<meta charset="UTF-8">
<meta name="description" content="Food Blog Web Template">
<meta name="keywords" content="food, unica, creative, html">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- Favicon -->   
<link href="<%=request.getContextPath()%>/front-end/img/favicon.ico" rel="shortcut icon"/>
<!-- Google Fonts -->
<link href="https://fonts.googleapis.com/css?family=Poppins:400,400i,500,500i,600,600i,700" rel="stylesheet">
<!-- Stylesheets -->
<link rel="stylesheet" href="<%=request.getContextPath()%>/front-end/css/bootstrap.min.css">
<link rel="stylesheet" href="<%=request.getContextPath()%>/front-end/css/font-awesome.min.css">
<link rel="stylesheet" href="<%=request.getContextPath()%>/front-end/css/owl.carousel.css">
<link rel="stylesheet" href="<%=request.getContextPath()%>/front-end/css/animate.css">
<link rel="stylesheet" href="<%=request.getContextPath()%>/front-end/css/style.css">
<link rel="stylesheet" href="<%=request.getContextPath()%>/front-end/member/css/styleLogin.css">
<script src="<%=request.getContextPath()%>/front-end/member/js/tw-city-selector.min.js"></script>
<style type="text/css">
	body{
		font-family:\5FAE\8EDF\6B63\9ED1\9AD4,\65B0\7D30\660E\9AD4;
	}

</style>
</head>
<body>
	<div id="preloder">
		<div class="loader"></div>
	</div>

<!-- Header section --> 
 <header class="header-section">	
  <div class="header-bottom"> 
   <div class="container"> 
    <a href="<%=request.getContextPath()%>/front-end/index.jsp" class="site-logo"> 
     <img src="<%=request.getContextPath() %>/front-end/img/logo-1.png" alt=""> 
    </a> 
    <div class="nav-switch"> 
     <i class="fa fa-bars"></i> 
    </div> 

    <ul class="main-menu"> 
     <li><a href="<%= request.getContextPath()%>/front-end/board/listAllBoard.jsp"><img alt="藝人專板"  title="藝人專板" src="<%= request.getContextPath()%>/front-end/res/img/index/board.png" style="height:30px;width:30px"></a></li> 
     <li><a href="<%= request.getContextPath()%>/front-end/product/Mall.jsp"><img alt="商城"  title="商城" src="<%= request.getContextPath()%>/front-end/res/img/index/mall.png" style="height:30px;width:30px"></a></li> 
     <li><a href="<%=request.getContextPath()%>/front-end/gif_collection/gif_list.jsp"><img alt="GIF"  title="GIF" src="<%= request.getContextPath()%>/front-end/res/img/index/gif.png" style="height:30px;width:30px"></a></li> 
     <li><a href="<%=request.getContextPath()%>/front-end/member/listOneMember.jsp"><img alt="會員資料" title="會員資料" src="<%= request.getContextPath()%>/front-end/res/img/index/user.png" style="height:30px;width:30px"></a></li> 
     <li><a href="#" onclick="logout()"><img alt="登出" title="登出" src="<%= request.getContextPath()%>/front-end/res/img/index/logout.png" style="height:30px;width:30px"></a> 
     <li><a href="<%=request.getContextPath()%>/front-end/chatBox/chatBox.jsp"><img alt="聊天室" title="聊天室" src="<%= request.getContextPath()%>/front-end/res/img/index/chat.png" style="height:30px;width:30px"></a></li> 
     <li>${memberVO.memberNickName }</li>
     <li><a href="<%=request.getContextPath()%>/front-end/personalPage/myPersonalPage.jsp"><img src="<%= request.getContextPath()%>/front-end/member/memberImg.do?memberID=${memberVO.memberID}" id="memberPhoto"></a></li>	
     <li id="notifi"><a href="#modal-id-notifi" data-toggle="modal"> 
      <div style="position: relative;"> 
      <img src="<%=request.getContextPath()%>/front-end/res/img/index/notification(32).png" alt="通知" id="notifiPhoto"> 
      <span style="position: absolute; top: 0; left: 17px; color: white" id="notifiCount"></span> 
      </div> 
     </a></li> 
     </ul>
   </div> 
  </div> 
 </header> 
 <!-- Header section end -->
<FORM METHOD="post" ACTION="<%=request.getContextPath()%>/front-end/member/member.do" id="logout">
	<input type="hidden" name="action"	value="logOut">
</FORM>
<script type="text/javascript">
	function logout(){
		document.getElementById("logout").submit();
	}
</script>





	<section class="add-section spad">
		<div class="container">
			<div class="row">
				<div class="col-lg-2 col-md-1 ftw-warp"></div>
				<div class="col-lg-8 col-md-6 ftw-warp">
					<div class="section-title">
						<h3>${memberVO.memberName }會員資料</h3>
		</h4> <%-- 錯誤表列 --%> <c:if test="${not empty errorMsgs}">
			<font style="color: red">請修正以下錯誤</font>
			<ul>
				<c:forEach var="message" items="${errorMsgs}">
					<li style="color: red">${message}</li>
				</c:forEach>
			</ul>
		</c:if>						
						
							<FORM METHOD="post" ACTION="member.do" name="form1" enctype="multipart/form-data">
							<table style="width:100%" class="table table-hover table-striped table-condensed table-bordered">
								<tr>
									<td>會員編號 :</td>
									<td><%= memberVO.getMemberID()%></td>
								</tr>
								<tr>
									<td>email:</td>
									<td><input type="TEXT" name="memberEmail" size="45" 
										 value="<%= (memberVO==null)? "請輸入信箱" : memberVO.getMemberEmail()%>" /></td>
								</tr>
								<tr>
									<td>會員姓名 :</td>
									<td><input type="TEXT" name="memberName" size="45"
										 value="<%= (memberVO==null)? "請輸入姓名" : memberVO.getMemberName()%>" /></td>
								</tr>
								<tr>
									<td>會員暱稱 :</td>
									<td><input type="TEXT" name="memberNickName" size="45"
										 value="<%= (memberVO==null)? "請輸入暱稱" : memberVO.getMemberNickName()%>" /></td>
								</tr>
								<tr>
									<td>會員性別 :</td>
									<td>
										<select name="memberSex">
											<option value="男">男</option>
											<option value="女">女</option>
										</select>
									</td>
								</tr>
								<tr>
									<td>會員生日 :</td>
									<td><input name="memberBirthday" id="f_date1" type="text"></td>
								</tr>
								<tr>
									<td>會員照片 :</td>
									<td><input type="File" name="memberPhoto"><img src="<%= request.getContextPath()%>/front-end/member/memberImg.do?memberID=${memberVO.memberID}"/></td>
								</tr>
								<tr>
									<td>會員手機 :</td>
									<td><input type="TEXT" name="memberPhone" size="45"
										 value="<%= (memberVO==null)? "請輸入手機" : memberVO.getMemberPhone()%>" /></td>
								</tr>
								<tr>
									<td>會員銀行帳號 :</td>
									<td><input type="TEXT" name="memberBankAccount" size="45"
										 value="<%= (memberVO==null)? "請輸入銀行帳號" : memberVO.getMemberBankAccount()%>" /></td>
								</tr>
								<tr>
									<td>會員地址:</td>
									<td>
									<div class="my-selector-c">
									  <div>
									    <select class="county"></select>
									  </div>
									  <div>
									    <select class="district"></select>
									  </div>
									</div>
									<input type="TEXT" name="memberAddress" size="45" 
										 value=""/>
									</td>
								</tr>
								<tr>
									<td>會員狀態 :</td>
									<td><%= memberVO.getMemberStatus()%></td>
								</tr>
								<tr>
									<td>加入會員日 :</td>
									<td><%= memberVO.getMemberJoinDate()%></td>
								</tr>
							
							</table>
							<br>
							<input type="hidden" name="action" value="update">
							<input type="hidden" name="memberID" value="<%=memberVO.getMemberID()%>">
							<div align="center">
								<input type="submit" value="送出修改" style="background-color: #7dccff;border-radius: 8px;font-size: 16px; padding: 8px 16px;border:0px;">
							</div>
							</FORM>
						
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Footer widgets section end -->
<script>
  new TwCitySelector({
    el: ".my-selector-c",
    elCounty: ".county", // 在 el 裡查找 dom
    elDistrict: ".district", // 在 el 裡查找 dom
    elZipcode: ".zipcode" // 在 el 裡查找 dom
  });
</script>






<!-- =========================================以下為 datetimepicker 之相關設定========================================== -->

<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/datetimepicker/jquery.datetimepicker.css" />
<script src="<%=request.getContextPath()%>/datetimepicker/jquery.js"></script>
<script src="<%=request.getContextPath()%>/datetimepicker/jquery.datetimepicker.full.js"></script>

<style>
  .xdsoft_datetimepicker .xdsoft_datepicker {
           width:  300px;   /* width:  300px; */
  }
  .xdsoft_datetimepicker .xdsoft_timepicker .xdsoft_time_box {
           height: 151px;   /* height:  151px; */
  }
</style>

<script>
$.datetimepicker.setLocale('zh');
$('#f_date1').datetimepicker({
   theme: '',              //theme: 'dark',
   timepicker:true,       //timepicker:true,
   step: 1,                //step: 60 (這是timepicker的預設間隔60分鐘)
   format:'Y-m-d H:i:s',         //format:'Y-m-d H:i:s',
   value: '<%=memberVO.getMemberBirthday() %>',
   maxDate:               '+1970-01-01'  // 去除今日(不含)之後
   // value:   new Date(),
   //disabledDates:        ['2017/06/08','2017/06/09','2017/06/10'], // 去除特定不含
   //startDate:	            '2017/07/10',  // 起始日
   //minDate:               '-1970-01-01', // 去除今日(不含)之前
   //maxDate:               '+1970-01-01'  // 去除今日(不含)之後
});
        
        
   
        // ----------------------------------------------------------以下用來排定無法選擇的日期-----------------------------------------------------------

        //      1.以下為某一天之前的日期無法選擇
        //      var somedate1 = new Date('2017-06-15');
        //      $('#f_date1').datetimepicker({
        //          beforeShowDay: function(date) {
        //        	  if (  date.getYear() <  somedate1.getYear() || 
        //		           (date.getYear() == somedate1.getYear() && date.getMonth() <  somedate1.getMonth()) || 
        //		           (date.getYear() == somedate1.getYear() && date.getMonth() == somedate1.getMonth() && date.getDate() < somedate1.getDate())
        //              ) {
        //                   return [false, ""]
        //              }
        //              return [true, ""];
        //      }});

        
        //      2.以下為某一天之後的日期無法選擇
        //      var somedate2 = new Date('2017-06-15');
        //      $('#f_date1').datetimepicker({
        //          beforeShowDay: function(date) {
        //        	  if (  date.getYear() >  somedate2.getYear() || 
        //		           (date.getYear() == somedate2.getYear() && date.getMonth() >  somedate2.getMonth()) || 
        //		           (date.getYear() == somedate2.getYear() && date.getMonth() == somedate2.getMonth() && date.getDate() > somedate2.getDate())
        //              ) {
        //                   return [false, ""]
        //              }
        //              return [true, ""];
        //      }});


        //      3.以下為兩個日期之外的日期無法選擇 (也可按需要換成其他日期)
        //      var somedate1 = new Date('2017-06-15');
        //      var somedate2 = new Date('2017-06-25');
        //      $('#f_date1').datetimepicker({
        //          beforeShowDay: function(date) {
        //        	  if (  date.getYear() <  somedate1.getYear() || 
        //		           (date.getYear() == somedate1.getYear() && date.getMonth() <  somedate1.getMonth()) || 
        //		           (date.getYear() == somedate1.getYear() && date.getMonth() == somedate1.getMonth() && date.getDate() < somedate1.getDate())
        //		             ||
        //		            date.getYear() >  somedate2.getYear() || 
        //		           (date.getYear() == somedate2.getYear() && date.getMonth() >  somedate2.getMonth()) || 
        //		           (date.getYear() == somedate2.getYear() && date.getMonth() == somedate2.getMonth() && date.getDate() > somedate2.getDate())
        //              ) {
        //                   return [false, ""]
        //              }
        //              return [true, ""];
        //      }});
        
</script>


	<!-- Footer section  -->
	<footer class="footer-section set-bg" data-setbg="">
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-6">
					<div class="footer-logo">
						<img src="<%=request.getContextPath()%>/front-end/img/logo-1.png" alt="">
					</div>
				</div>
				<div class="col-lg-6 text-lg-right">
					
					<p class="copyright">
						電話： 03-4257387
						<br>
						地址： 320桃園市中壢區中大路300號
					</p>
				</div>
			</div>
		</div>
	</footer>
	<!-- Footer section end -->



	<!--====== Javascripts & Jquery ======-->
   	<script src="<%=request.getContextPath()%>/front-end/js/jquery-3.2.1.min.js"></script>
	<script src="<%=request.getContextPath()%>/front-end/js/owl.carousel.min.js"></script>
	<script src="<%=request.getContextPath()%>/front-end/js/main.js"></script>
</body>
</html>