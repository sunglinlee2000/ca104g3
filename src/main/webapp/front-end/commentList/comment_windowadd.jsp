<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="java.util.*"%>
<%@ page import="com.commentList.model.*"%>

<%
  response.setHeader("Cache-Control","no-store"); //HTTP 1.1
  response.setHeader("Pragma","no-cache");        //HTTP 1.0
  response.setDateHeader ("Expires", 0);
  
//   pageContext.setAttribute("testMemberID","M000001"); //假裝登入
%>

<%
  CommentListVO commentListVO = (CommentListVO) request.getAttribute("commentListVO");
	String issueID = request.getParameter("issueID");
%>

<html lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
        <title>新增一篇留言</title>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
        <!--[if lt IE 9]>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css">

		<script src="https://code.jquery.com/jquery-1.9.1.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>

		<!-- 下載來用可以額外對 config.js 修改樣式顏色之類的 -->
		<!-- 連結 ： https://ckeditor.com/cke4/release/CKEditor-4.4.1 -->
		<!-- <script src="ckeditor_4.4.1/ckeditor.js"></script> -->
		<!-- 直接連 cdn 用也是可以啦 -->
		<script src="http://cdn.ckeditor.com/4.4.1/standard/ckeditor.js"></script>
    </head>
    <body>
        


        <br>
        <br>
        <br>
            <div class="container">
                                  <div class="row">
                                    <div class="col-xs-12 col-sm-6 col-sm-offset-3">
                                    
 <%-- 錯誤表列 --%>
										<c:if test="${not empty errorMsgs}">
											
											<ul>
												<c:forEach var="message" items="${errorMsgs}">
													<li style="color:red">${message}</li>
												</c:forEach>
											</ul>
										</c:if>
                                    
                                        <form method="post" action="<%=request.getContextPath() %>/commentList/commentList.do" enctype="multipart/form=data">

                                            <div class="form-group">
                                                <label>輸入內容</label>                              
                                                <textarea name="commentContent" id="txa" class="form-control" rows="5"></textarea>
                                            </div>

											<div>    
                                                <input type="hidden" name="action" value="insert">
												<input type="hidden" name="commentID" value="<%=issueID%>" >
												<input class="btn btn-success" type="submit"  value="送出">                                                 
		                                   </div>
													

                                        </form>
                                    </div>
                                  </div>
                                </div>
          
        
        <script src="https://code.jquery.com/jquery.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
    </body>
</html>