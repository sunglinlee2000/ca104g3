<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="com.commentList.model.*"%>

<%
  response.setHeader("Cache-Control","no-store"); //HTTP 1.1
  response.setHeader("Pragma","no-cache");        //HTTP 1.0
  response.setDateHeader ("Expires", 0);
%>

<%
CommentListVO commentListVO = (CommentListVO) request.getAttribute("commentListVO");//EmpServlet.java (Concroller) 存入req的empVO物件 (包括幫忙取出的empVO, 也包括輸入資料錯誤時的empVO物件)
%>

<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
<title>員工資料修改 - update_emp_input.jsp</title>

<style>
  table#table-1 {
	background-color: #CCCCFF;
    border: 2px solid black;
    text-align: center;
  }
  table#table-1 h4 {
    color: red;
    display: block;
    margin-bottom: 1px;
  }
  h4 {
    color: blue;
    display: inline;
  }
</style>

<style>
  table {
	width: 450px;
	background-color: white;
	margin-top: 1px;
	margin-bottom: 1px;
  }
  table, th, td {
    border: 0px solid #CCCCFF;
  }
  th, td {
    padding: 1px;
  }
</style>

</head>
<body bgcolor='white'>

<table id="table-1">
	<tr><td>
		 <h3>留言資料修改 - update_emp_input.jsp</h3>
		 <h4><a href="<%=request.getContextPath()%>/front-end/commentList/select_page.jsp"><img src="<%=request.getContextPath()%>/front-end/commentList/images/tomcat.png" width="100" height="100" border="0">回首頁</a></h4>
	</td></tr>
</table>

<h3>資料修改:</h3>

<%-- 錯誤表列 --%>
<c:if test="${not empty errorMsgs}">
	<font style="color:red">請修正以下錯誤:</font>
	<ul>
		<c:forEach var="message" items="${errorMsgs}">
			<li style="color:red">${message}</li>
		</c:forEach>
	</ul>
</c:if>

<FORM METHOD="post" ACTION="<%=request.getContextPath() %>/commentList/commentList.do" name="form1">
<table>
	<tr>
		<td>留言編號:<font color=red><b>*</b></font></td>
		<td><%=commentListVO.getCommentID()%></td>
	</tr>
	
	<tr>
		<td>內容:</td>
		<td><input type="TEXT" name="commentContent" size="45"	value="<%=commentListVO.getCommentContent()%>" /></td>
	</tr>
	
	
</table>
<br>
<input type="hidden" name="action" value="update">
<input type="hidden" name="memberID" value="${commentListVO.memberID}" >
<input type="hidden" name="issueID" value="${commentListVO.issueID}" >
<input type="hidden" name="gifID" value="${commentListVO.gifID}" >
<input type="hidden" name="commentID" value="<%=commentListVO.getCommentID()%>">
<input type="submit" value="送出修改"></FORM>
</body>
</html>