<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page import="java.util.*"%>
<%@ page import="com.commentList.model.*"%>
<%-- 此頁練習採用 EL 的寫法取值 --%>

<%
	response.setHeader("Cache-Control", "no-store"); //HTTP 1.1
	response.setHeader("Pragma", "no-cache"); //HTTP 1.0
	response.setDateHeader("Expires", 0);
%>

<%
	CommentListService commentListSvc = new CommentListService();
	List<CommentListVO> list = commentListSvc.getAll();
	pageContext.setAttribute("list", list);
%>


<html>
<head>
<title>所有留言 - listAllEmp.jsp</title>

<style>
</style>

</head>
<body bgcolor='white'>

	<%-- 錯誤表列 --%>
	<c:if test="${not empty errorMsgs}">
		<font style="color: red">請修正以下錯誤:</font>
		<ul>
			<c:forEach var="message" items="${errorMsgs}">
				<li style="color: red">${message}</li>
			</c:forEach>
		</ul>
	</c:if>

	<table>

		</tr>


		<hr>
		<c:forEach var="commentListVO" items="${list}">
            <div class="container">
                <div class="row">           
                    <div class="col-xs-12 col-sm-8 col-sm-offset-2"> 		
				
							<div class="media-heading">
								<a href="#"> <img src="" class="img-circle"
									style="height: 50px; width: 50px; margin: auto;">
								</a>
							</div>
							<div class="media-body">
			
								<h4 class="media-heading">${commentListVO.memberID}<small>回覆於&nbsp;<i>${commentListVO.commentDate}</i></small>
								</h4>
			
								<p class="text"
									style="margin-left: 15px; font-size: 20px; color: black;">
									${commentListVO.commentContent}<br>
									<FORM METHOD="post"
										ACTION="<%=request.getContextPath()%>/commentList/commentList.do"
										style="margin-bottom: 0px;">
										<input type="submit" class="btn btn-primary" value="修改"> <input
											type="hidden" name="commentID" value="${commentListVO.commentID}">
										<input type="hidden" name="action" value="getOne_For_Update">
									
										<input type="submit" class="btn btn-danger" value="刪除"> <input
											type="hidden" name="commentID" value="${commentListVO.commentID}">
										<input type="hidden" name="action" value="delete">
									</FORM>
									<hr>
								</p>
							</div>
					
					</div>
				</div>

			</div>
			<br>			
		</c:forEach>
	</table>


</body>
</html>