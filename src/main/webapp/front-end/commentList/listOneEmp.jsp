<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.commentList.model.*"%>
<%-- 此頁暫練習採用 Script 的寫法取值 --%>

<%
  response.setHeader("Cache-Control","no-store"); //HTTP 1.1
  response.setHeader("Pragma","no-cache");        //HTTP 1.0
  response.setDateHeader ("Expires", 0);
%>

<%
	CommentListVO commentListVO = (CommentListVO) request.getAttribute("commentListVO"); //EmpServlet.java(Concroller), 存入req的empVO物件
%>

<html>
<head>
<title>留言資料 - listOneEmp.jsp</title>

<style>
  table#table-1 {
	background-color: #CCCCFF;
    border: 2px solid black;
    text-align: center;
  }
  table#table-1 h4 {
    color: red;
    display: block;
    margin-bottom: 1px;
  }
  h4 {
    color: blue;
    display: inline;
  }
</style>

<style>
  table {
	width: 600px;
	background-color: white;
	margin-top: 5px;
	margin-bottom: 5px;
  }
  table, th, td {
    border: 1px solid #CCCCFF;
  }
  th, td {
    padding: 5px;
    text-align: center;
  }
</style>

</head>
<body bgcolor='white'>

<h4>此頁暫練習採用 Script 的寫法取值:</h4>
<table id="table-1">
	<tr><td>
		 <h3>留言資料 - ListOneEmp.jsp</h3>
		 <h4><a href="<%=request.getContextPath()%>/front-end/commentList/select_page.jsp"><img src="<%=request.getContextPath()%>/front-end/commentList/images/tomcat.png" width="100" height="100" border="0">回首頁</a></h4>
	</td></tr>
</table>

<table>
	<tr>
		<th>留言編號</th>
		<th>會員編號</th>
		<th>話題編號</th>
		<th>GIF編號</th>
		<th>留言內容</th>
		<th>留言時間</th>
		
		<th>修改</th>
		<th>刪除</th>
		
	</tr>
	<tr>
		<td><%=commentListVO.getCommentID()%></td>
		<td><%=commentListVO.getMemberID()%></td>
		<td><%=commentListVO.getIssueID()%></td>
		<td><%=commentListVO.getGifID()%></td>
		<td><%=commentListVO.getCommentContent()%></td>
		<td><%=commentListVO.getCommentDate()%></td>
	</tr>
</table>

</body>
</html>