<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">        
        <!--<script src="/ckeditor/ckeditor.js"></script>-->
        <script src="https://cdn.ckeditor.com/4.7.3/standard/ckeditor.js"></script>
        <title>ckEditor</title>

    </head>
    <body>
        <form name='form' action='#' method='post'>
            <textarea name="content" id="content" rows="10" cols="80"></textarea>
            <script>
                CKEDITOR.replace( 'content', {
                    //輸入客製化條件
                    width:1000, //設定編輯器寬度
                    height:500,
                });
            </script>
            <input type = 'button' value = '送出' onclick = 'processData()'>
        </form>



    </body>
</html>