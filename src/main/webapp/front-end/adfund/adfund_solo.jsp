<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page import="java.util.*"%>
<%@ page import="com.adfund.model.*"%>
<%@ page import="com.adfundReport.model.*"%>
<%@ page import="com.member.model.*"%>
<%@ page import="com.adfundRecord.model.*"%>
<%@ page import="com.notificationList.model.*"%>

<%
	ADFundVO adfundVO = (ADFundVO) session.getAttribute("adfundVO"); 
    ADFundReportVO adfundReportVO = (ADFundReportVO) request.getAttribute("adfundReportVO");
    ADFundRecordVO adfundRecordVO = (ADFundRecordVO) request.getAttribute("adfundRecordVO");
    MemberVO memberVO = (MemberVO) session.getAttribute("memberVO");
%>

<%
	response.setHeader("Cache-Control", "no-store");
	response.setHeader("Pragma", "no-cache");
	response.setDateHeader("Expires", 0);
%>

<%
    NotificationListService noSvc = new NotificationListService();
    List<NotificationListVO> nolists = noSvc.getAllNoList(memberVO.getMemberID());
    pageContext.setAttribute("nolists",nolists);
%>

<jsp:useBean id="memberSvc" scope="page" class="com.member.model.MemberService" />

<html lang="">
<head>

<title>廣告募資詳情</title>
<meta charset="UTF-8">
<meta name="description" content="Food Blog Web Template">
<meta name="keywords" content="food, unica, creative, html">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

   
	<!-- Favicon -->   
	<link href="img/favicon.ico" rel="shortcut icon"/>

	<!-- Google Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Poppins:400,400i,500,500i,600,600i,700" rel="stylesheet">

	<!-- Stylesheets -->
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/css/bootstrap.min.css"/>
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/css/font-awesome.min.css"/>
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/css/owl.carousel.css"/>
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/css/animate.css"/>
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/css/style.css"/>
	<link rel="stylesheet" href="<%=request.getContextPath()%>/front-end/member/css/styleLogin.css">
	
	<!-- original -->			
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/adfund/css/bootstrap.min.css">
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/adfund/css/bootstrap-theme.min.css">
	
	<script src="<%=request.getContextPath() %>/front-end/adfund/js/jquery-3.3.1.min.js"></script>
	
	<script src="<%=request.getContextPath() %>/front-end/adfund/js/bootstrap.min.js"></script>
	<script src="<%=request.getContextPath() %>/front-end/adfund/js/notiWebSocket.js"></script>
	<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
	

	
	

<style type="text/css">

body{
	background-color:white;
	font-family:\5FAE\8EDF\6B63\9ED1\9AD4,\65B0\7D30\660E\9AD4;
}

.wrap{
	width: 100%;
	max-width: 1000px;
	margin: auto;
	display: flex;
	flex-wrap: wrap;
	flex-direction: column;
	
}
.kk-ad-item{
	width: 100%;
	margin: 10px;
	padding: 15px;
	background-color: #92F2FF;
	flex-grow: 1;
	color: #000000;
	border:1px solid;
	border-color: #E3E0E0;
	border-radius: 10px;
}
@media screen and (min-width:768px){
	.kk-ad-item{
		width: 100%;
	}
}
@media screen and (min-width:1200px){
	.kk-ad-item{
		width: 100%;
	}
}
.kk-ad-item img{
	width: 100%;
	border-radius: 5px;
	border:1px solid;
	border-color:#E3E0E0;

}
.kk-ad-item:empty{
	background: none;
	border:0px;
}
.btn-g{
	display: flex;
	flex-direction: row-reverse;
}

.ad-look-btn{ 	
	background-color: #E3E0E0; 
	color:black;
	border: 1px solid;
	border-color: white;
	width: 150px;
	height: 30px;
	border-radius: 4px;
}

.kk-ad-list-group{
	padding:10px;   
}

.ad-join-btn{
	width: 100%;
	height:40px;
	background-color: #E3E0E0;
	color:black;
}


.progress{
    margin-bottom:10px;
    margin-top:10px;
}

.progress-bar{
    background-image: linear-gradient(to bottom,#638ff9 0,#638ff9 100%);

}


p {
  text-align: center;
  font-size: 20px;
  color:deeppink;
  margin-top: 0px;
  margin-bottom: 0px;
}

.kk-no-list{
   height:auto;
   border:1px solid;
   border-color: #E3E0E0;
   background-color: #E3E0E0;
   margin: 2px;
   padding: 2px;
   font-size:15px;
}

.kk-modal-body{
   height:300px;
   overflow-y: scroll;
   overflow-x: hidden;

}

</style>

</head>
<body onload="connect()" onunload="disconnect()">
	<div id="preloder">
		<div class="loader"></div>
	</div>

<!-- Header section --> 
 <header class="header-section">	
  <div class="header-bottom"> 
   <div class="container"> 
    <a href="<%=request.getContextPath()%>/front-end/index.jsp" class="site-logo"> 
     <img src="<%=request.getContextPath() %>/front-end/img/logo-1.png" alt=""> 
    </a> 
    <div class="nav-switch"> 
     <i class="fa fa-bars"></i> 
    </div> 

    <ul class="main-menu"> 
     <li><a href="<%= request.getContextPath()%>/front-end/board/listAllBoard.jsp"><img alt="藝人專板"  title="藝人專板" src="<%= request.getContextPath()%>/front-end/res/img/index/board.png" style="height:30px;width:30px"></a></li> 
     <li><a href="<%= request.getContextPath()%>/front-end/product/Mall.jsp"><img alt="商城"  title="商城" src="<%= request.getContextPath()%>/front-end/res/img/index/mall.png" style="height:30px;width:30px"></a></li> 
     <li><a href="<%=request.getContextPath()%>/front-end/gif_collection/gif_list.jsp"><img alt="GIF"  title="GIF" src="<%= request.getContextPath()%>/front-end/res/img/index/gif.png" style="height:30px;width:30px"></a></li> 
     <li><a href="<%=request.getContextPath()%>/front-end/member/listOneMember.jsp"><img alt="會員資料" title="會員資料" src="<%= request.getContextPath()%>/front-end/res/img/index/user.png" style="height:30px;width:30px"></a></li> 
     <li><a href="#" onclick="logout()"><img alt="登出" title="登出" src="<%= request.getContextPath()%>/front-end/res/img/index/logout.png" style="height:30px;width:30px"></a> 
     <li><a href="<%=request.getContextPath()%>/front-end/chatBox/chatBox.jsp"><img alt="聊天室" title="聊天室" src="<%= request.getContextPath()%>/front-end/res/img/index/chat.png" style="height:30px;width:30px"></a></li> 
     <li>${memberVO.memberNickName }</li>
     <li><a href="<%=request.getContextPath()%>/front-end/personalPage/myPersonalPage.jsp"><img src="<%= request.getContextPath()%>/front-end/member/memberImg.do?memberID=${memberVO.memberID}" id="memberPhoto"></a></li>	
     <li id="notifi"><a href="#modal-id-notifi" data-toggle="modal"> 
      <div style="position: relative;"> 
      <img src="<%=request.getContextPath()%>/front-end/res/img/index/notification(32).png" alt="通知" id="notifiPhoto"> 
      <span style="position: absolute; top: 0; left: 17px; color: white" id="notifiCount"></span> 
      </div> 
     </a></li> 
     </ul>
   </div> 
  </div> 
 </header> 
 <!-- Header section end -->
<FORM METHOD="post" ACTION="<%=request.getContextPath()%>/front-end/member/member.do" id="logout">
	<input type="hidden" name="action"	value="logOut">
</FORM>
<script type="text/javascript">
	function logout(){
		document.getElementById("logout").submit();
	}
</script>

	

	<div class="container-fiuld">
		<div class="row">
		
			<div class="col-xs-12 col-sm-8 col-sm-offset-2">
				<div class="wrap">
					<div class="item kk-ad-item">
						<center>
							<h1><b>- ${adfundVO.adfundTitle} -</b></h2>
						</center>
						
						<center>
						    <a href="<%=request.getContextPath()%>/personalPage/personalPage.do?memberID=${adfundVO.memberID}&action=toOnePersonalPage">By ${memberSvc.getOneMember(adfundVO.memberID).memberNickName}
							</a>
						</center>
						
						<center><h4><span id="kk${adfundVO.adfundID}">${adfundVO.adfundStatus}</span></h4></center>
						    
						    <script> 
	                            $(document).ready(function() {

									if($('#kk${adfundVO.adfundID}').html() == 'AD_SUBMITTED'){
										$('#kk${adfundVO.adfundID}').html('目標達成 待審核');
									}else if($('#kk${adfundVO.adfundID}').html() == 'AD_FUNDING'){
										$('#kk${adfundVO.adfundID}').html('募資中');
									}else if($('#kk${adfundVO.adfundID}').html() == 'AD_PASSED'){
										$('#kk${adfundVO.adfundID}').html('募資通過!');
									}else if($('#kk${adfundVO.adfundID}').html() == 'AD_NOT_FINISH'){
										$('#kk${adfundVO.adfundID}').html('未達目標');
									}
									
	                            });
                            
                            </script>
						
						
						<!-- 倒數計時 -->
                        <p id="demo"></p>
												
						<h4>首頁圖片</h4>

						<img src="<%=request.getContextPath()%>/adfundImg/adfund.do?adfundID=${adfundVO.adfundID}&adfundHomePhoto=adfundHomePhoto">
						<p></p>
						<h4>藝人專板圖片</h4>

						<img src="<%=request.getContextPath()%>/adfundImg/adfund.do?adfundID=${adfundVO.adfundID}&adfundBoardPhoto=adfundBoardPhoto">
						<br>
<!-- 						<div class="progress"> -->
<!-- 							<div class="progress-bar" role="progressbar" -->
<%-- 								style="width: <fmt:formatNumber value="${adfundVO.adfundCoinNow / adfundVO.adfundCoinTarget * 100}" pattern="#,#00.0"/>%;" --%>
<%-- 								aria-valuenow="<fmt:formatNumber value="${adfundVO.adfundCoinNow / adfundVO.adfundCoinTarget * 100}" pattern="#,#00.0"/>" --%>
<%-- 								aria-valuemin="0" aria-valuemax="100"><fmt:formatNumber value="${adfundVO.adfundCoinNow / adfundVO.adfundCoinTarget * 100}" pattern="#,#00.0"/>%</div> --%>
<!-- 						</div> -->
		    

						<h3 id="adfundCoinNow"><b><font color=deeppink>$${adfundVO.adfundCoinNow} </font></b>/ $3000</h3>
						<h4>▪ 時程: <fmt:formatDate value="${adfundVO.adfundEndDate}" pattern="yyyy-MM-dd HH:mm"/> 到 <fmt:formatDate value="${adfundVO.adfundEndDate}" pattern="yyyy-MM-dd HH:mm"/></h4>
						<h4>▪ 刊登時間: <fmt:formatDate value="${adfundVO.adfundDisplayDate}" pattern="yyyy-MM-dd HH:mm"/></h4>						
                           
						<h4>▪ 募資說明:</h4>
						<h4>${adfundVO.adfundText}</h4>
						
                        
						<br>
						<div class="btn-g">
							<a href="#modal-id-adjoin" data-toggle="modal">
							<input type="button" value="加入" class="ad-look-btn"></a>
							
							<!-- 如果會員是發文者本人 就不會出現出現修改的按鈕可以按 --> 
							<c:if test="${memberVO.memberID == adfundVO.memberID }">
							<a href="#modal-id-adreport" data-toggle="modal">
							<input type="button" value="檢舉" class="ad-look-btn" style="display:none"></a>
							</c:if>
							<c:if test="${memberVO.memberID != adfundVO.memberID }">
							<a href="#modal-id-adreport" data-toggle="modal">
							<input type="button" value="檢舉" class="ad-look-btn" style="display:block"></a>
							</c:if>
							
							<FORM METHOD="post" ACTION="<%=request.getContextPath()%>/adfund/adfund.do">
								
								<!-- 如果會員是發文者本人 就會出現修改的按鈕可以按 -->
								<c:if test="${memberVO.memberID == adfundVO.memberID }">
								<input type="submit" value="修改" class="ad-look-btn" style="display:block"> 
								</c:if>
								<c:if test="${memberVO.memberID != adfundVO.memberID }">
								<input type="submit" value="修改" class="ad-look-btn" style="display:none"> 
								</c:if>
								
								<input type="hidden" name="adfundID" value="${adfundVO.adfundID}">
								<input type="hidden" name="action" value="getOne_For_Update">
							</FORM>
							
							<a href="<%=request.getContextPath() %>/front-end/adfund/adfund_list.jsp?boardID=<%=adfundVO.getBoardID()%>">
	                        <input type="button" value="返回募資列表" class="ad-look-btn"></a></center>

						</div>
					</div>
				</div>

			</div>
		</div>
	</div>


    

	<!--加入募資跳窗-->
	<div class="modal fade" id="modal-id-adjoin">
		<div class="modal-dialog modal-sm">
			<div class="modal-content">
<%-- 			   <FORM METHOD="post" ACTION="<%=request.getContextPath()%>/adfundRecord/adfundRecord.do"> --%>
			   <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<div class="modal-header">					
					<h4 class="modal-title">想使用多少代幣呢?</h4>					
				</div>
				<div class="modal-body">
					<input type="number" name="adfundCoinUsed" id="adfundCoinUsed" class="form-control">
					
					<div id="error-div" style="color:red;">					  
					</div>
					
										
				</div>
				<div class="modal-footer">
<!-- 					<input type="hidden" name="action" value="insert">  -->
					<input type="hidden" name="memberID" value="${memberVO.memberID}">
					<input type="hidden" name="adfundRecordID" value="${adfundRecord.adfundRecordID }"> 
					<input type="hidden" name="adfundID" value="${adfundVO.adfundID}">
					<center><input type="submit" value="確認" class="btn btn-info" disabled onclick="sendMessage()" id="coin-submit"></center>
					
				</div>
<!-- 				</form> -->
			</div>
		</div>
	</div>
	
	
	<!--檢舉募資跳窗-->
	<div class="modal fade" id="modal-id-adreport">
		<div class="modal-dialog modal-sm">
			<div class="modal-content">
<%-- 			    <FORM METHOD="post" ACTION="<%=request.getContextPath()%>/adfundReport/adfundReport.do"> --%>
			    						
				<div class="modal-header">	
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>			
					<h4 class="modal-title">檢舉事項</h4>
					
				</div>
				<div class="modal-body">
				
						<input type="radio" name="adfundReportReason" value="BULLY"> 重傷、歧視、挑釁或謾罵他人<br>
						<input type="radio" name="adfundReportReason" value="BOARD_WASH"> 惡意洗版、重複張貼<br>
						<input type="radio" name="adfundReportReason" value="SEX"> 含有色情及暴力內容<br>
						<input type="radio" name="adfundReportReason" value="BOARD_WRONG"> 文章發錯板<br>  
						<input type="radio" name="adfundReportReason" value="OTHER"> 其他違規之項目<br>	
						
						<div id="error-div2" style="color:red;">				  
						  </div>					
										
				</div>
				<div class="modal-footer">
<!-- 					<input type="hidden" name="action" value="insert">  -->
					<input type="hidden" name="adfundReportID" value="${adfundReportVO.adfundReportID}">
					<input type="hidden" name="adfundID" value="${adfundVO.adfundID}">
					<input type="hidden" name="memberID" value="${adfundVO.memberID}">
					<center><input type="submit" value="提交" class="btn btn-info" id="report-submit" >
				</div>
<!-- 				</form> -->
			</div>
		</div>
	</div>
	
	
	
		<!-- 通知列表的跳窗 -->
		<div class="modal fade" id="modal-id-notifi">
			<div class="modal-dialog modal-sm">
			   <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<div class="modal-content">
				   
					<div class="modal-header">					
						<h4 class="modal-title">通知列表</h4>
					</div>
					<div class="modal-body kk-modal-body" >

                    <!-- 及時通知顯示的地方 -->
					<div id="nolistsssss" class="kk-no-list">
					  <a href="<%=request.getContextPath()%>/adfundTest/adfund.do?action=getOne_For_Display&adfundID=${adfundVO.adfundID}" ></a>
					 </div>
                        
						<c:forEach var="NotificationListVO" items="${nolists}" >
					      <div class="kk-no-list">					      
					         <a href="<%=request.getContextPath()%>${NotificationListVO.notificationpath}${NotificationListVO.notificationtarget}" id="${NotificationListVO.notificationid}">${NotificationListVO.notificationcontent}</a>
					         <h6><fmt:formatDate value="${NotificationListVO.notificationdate}" pattern="yyyy-MM-dd HH:mm"/></h6>				       
					      </div>
					    </c:forEach>
					</div>
					<div class="modal-footer">

					</div>
				</div>
			</div>
		</div>
	<!-- 通知的跳窗 -->
	  
	
	<script src="<%=request.getContextPath() %>/front-end/js/jquery-3.2.1.min.js"></script>
	<script src="<%=request.getContextPath() %>/front-end/owl.carousel.min.js"></script>
	<script src="<%=request.getContextPath() %>/front-end/js/main.js"></script>	
	<script src="https://code.jquery.com/jquery.js"></script>

	<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>


    
	<script type="text/javascript">
	
	//即時判斷代幣
	$(document).ready(function() { 
		$('#modal-id-adjoin').find('input[name="adfundCoinUsed"]').on('keyup', function(){
			var errorDiv = $('#modal-id-adjoin').find('#error-div');
			errorDiv.html('');
			
			var joinTime = $.now();
			var endTime = new Date("${adfundVO.adfundEndDate}").getTime();
			
			var val = $(this).val().trim();
			if(/^\d+$/.test(val)){
				val = parseInt(val);
				var memCoin = 0;
				if("${memberVO.memberCoin}"){
					memCoin = parseInt("${memberVO.memberCoin}");					
				}
				
				if(joinTime > endTime){
					$('#modal-id-adjoin').find('input[type="submit"]').attr('disabled', true);
					errorDiv.html("時間已過 已無法參加");
				}			
				
				if(val <= memCoin){
					$('#modal-id-adjoin').find('input[type="submit"]').attr('disabled', false);
				} else {
					$('#modal-id-adjoin').find('input[type="submit"]').attr('disabled', true);
					errorDiv.html('您的代幣沒有那麼多喔 您的代幣為 ${memberVO.memberCoin}');					
				}
			} else {
				$('#modal-id-adjoin').find('input[type="submit"]').attr('disabled', true);
				errorDiv.html('您還未輸入數字');
			}
		});
	});
	
	
	//即時判斷檢舉
	$(document).ready(function(){
		$('#modal-id-adreport').find('input[name="adfundReportReason"]').on('mousedown', function(){
			var errorDiv2 = $('#modal-id-adreport').find('#error-div2');
			errorDiv2.html('');
			
			var joinTime = $.now();
			var endTime = new Date("${adfundVO.adfundEndDate}").getTime();
			
			if(joinTime > endTime){
				$('#modal-id-adreport').find('input[type="submit"]').attr('disabled', true);
				errorDiv2.html("時間已過 已無法參加");
			}			
		});		
	});
	
	
	
	//倒數計時	
	// Set the date we're counting down to
	var countDownDate = new Date("${adfundVO.adfundEndDate}").getTime();
	
	// Update the count down every 1 second
	var x = setInterval(function() {
	
	    // Get todays date and time
	    var now = new Date().getTime();
	    
	    // Find the distance between now and the count down date
	    var distance = countDownDate - now;
	    
	    // Time calculations for days, hours, minutes and seconds
	    var days = Math.floor(distance / (1000 * 60 * 60 * 24));
	    var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
	    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
	    var seconds = Math.floor((distance % (1000 * 60)) / 1000);
	    
	    // Output the result in an element with id="demo"
	    document.getElementById("demo").innerHTML = "還有" + days + "天  " + hours + "時 "
	    + minutes + "分 " + seconds + "秒 ";
	    
	    // If the count down is over, write some text 
	    if (distance < 0) {
	        clearInterval(x);
	        document.getElementById("demo").innerHTML = "";
	    }
	}, 1000);
	//倒數計時
	
	</script>
	
	
<script>

$(document).ready(function() { 
	
	//點擊鈴鐺時，通知要全部變為已讀
    $('#notifi').click(function(){
    	
//              alert("!!")
				$.ajax({ 
				     url : '<%=request.getContextPath()%>/front-end/NotificationListServlet/NotificationListServlet.do', 
				     type : 'Post', 
				     data : { 
				      action : 'isClick', 
				      memberID : '${memberVO.memberID}' 

				     } 
				    }).done(function(res){
				    	var obj = JSON.parse(res);
				    	document.getElementById("notifiPhoto").src = "<%=request.getContextPath()%>/front-end/res/img/index/notification(32).png";
				    	$('#notifiCount').html("");
	                });				     
     });
 
	
//    alert("noti!!");
    //網頁一打開時，要立刻判斷是否有未讀訊息，看是否要顯示有消息的鈴鐺
    $.ajax({ 
	     url : '<%=request.getContextPath()%>/front-end/NotificationListServlet/NotificationListServlet.do', 
	     type : 'Post', 
	     data : { 
	      action : 'isRead',  
	      memberID : '${memberVO.memberID}'
	     } 
	    }).done(function(res){
	    	var obj = JSON.parse(res);
	    	if(obj.notiStatus==false){
	    	   document.getElementById("notifiPhoto").src = "<%=request.getContextPath()%>/front-end/res/img/index/notification(32)red.png";
	    	   $('#notifiCount').html(obj.falseCount);
	    	}
	    });
    
});
</script>
	
	
 	<script> 
    //WebSocket
    var MyPoint = "/MyNotiServer/peter/309";
    var host = window.location.host;
    var path = window.location.pathname;
    var webCtx = path.substring(0, path.indexOf('/', 1));
    var endPointURL = "wss://" + window.location.host + webCtx + MyPoint;
    
	var webSocket;
	
	function connect() {
//		alert("load");
		// 建立 websocket 物件
		webSocket = new WebSocket(endPointURL);
		
		webSocket.onopen = function(event) {
//           alert("連線成功");
					

		};

		webSocket.onmessage = function(event) {
			alert("訊息接成功");		
			 
			var messagesArea = document.getElementById("nolistsssss");

	        var jsonObj = JSON.parse(event.data);
	        var message = jsonObj.joinMemberNickName + "參加了【 " + jsonObj.adfundTitle + " 】募資案 已達成"+ jsonObj.sum +" 代幣"+"\r\n";
	        
	        
	        $('#nolistsssss').find('a').html(message).css("color","red");
	        $('#notifiCount').html(jsonObj.falseCount);

	        messagesArea.scrollTop = messagesArea.scrollHeight;
	        document.getElementById("notifiPhoto").src = "<%=request.getContextPath()%>/front-end/res/img/index/notification(32)red.png";	
		};

		webSocket.onclose = function(event) {
			alert("已離線");
		};
	}
	
	function sendMessage() {
//            alert("check");
            
            var inputCoinNumber = document.getElementById("adfundCoinUsed").value.trim();
            
            
	        var jsonObj = {
	        		       "startMember" : "${memberSvc.getOneMember(adfundVO.memberID).memberNickName}", 
	        		       "startMemberID" : "${adfundVO.memberID}",
	        		       "adfundTitle" : "${adfundVO.adfundTitle}",
	        		       "adfundCoinNow" : "${adfundVO.adfundCoinNow}",
	        		       "adfundCoinUsed" : inputCoinNumber,
	        		       "adfundID" : "${adfundVO.adfundID}",
	        		       "joinMemberID" : "${memberVO.memberID}",
	        		       "joinMemberNickName" : "${memberVO.memberNickName}"
	        		       };
	        webSocket.send(JSON.stringify(jsonObj));	        

	 }
	
	function disconnect () {
		alert("disconnect");
		webSocket.close();

	}

</script> 



<script>

$(document).ready(function() { 
    $('#report-submit').click(function(){
    	
      	$('#modal-id-adreport').trigger( "click" );
				$.ajax({ 
				     url : '<%=request.getContextPath()%>/adfundReport/adfundReport.do', 
				     type : 'Post', 
				     data : { 
				      action : 'insert', 
				      memberID : '${memberVO.memberID}' ,
				      adfundID : '${adfundVO.adfundID}',
				      adfundReportID: '${adfundReportVO.adfundReportID}',
				      adfundReportReason : $('input[name="adfundReportReason"]:checked').val()
				     } 
				    }).done(function(res){
				    	var obj = JSON.parse(res);
				    	
				    	swal({
		    		    	  title: "您已提交檢舉",
		    		    	  button: "確定",
		    		    	});				    	
	                });				     
         });
    
});



$(document).ready(function() { 
    $('#coin-submit').click(function(){
    	
  	$('#modal-id-adjoin').trigger( "click" );
			$.ajax({ 
			     url : '<%=request.getContextPath()%>/adfundRecord/adfundRecord.do', 
			     type : 'Post', 
			     data : { 
			      action : 'insert', 
			      memberID : '${memberVO.memberID}' ,
			      adfundID : '${adfundVO.adfundID}',
			      adfundReportID: '${adfundReportVO.adfundReportID}',
			      adfundCoinUsed : $('input[name="adfundCoinUsed"]').val(),
			     } 
			    }).done(function(res){
			    	var obj = JSON.parse(res);
			    	$('#adfundCoinNow').html("<b><font color=deeppink>"+obj.adfundCoinNow+"</font></b>/ $3000");
			    	swal({
	    		    	  title: "謝謝您參與募資",
	    		    	  button: "確定",
	    		    	});	
			    	
	             });			    	
           });	
});


</script>


</body>
</html>