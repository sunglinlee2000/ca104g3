<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page import="java.util.*"%>
<%@ page import="com.adfund.model.*"%>
<%@ page import="com.board.model.*"%>
<%@ page import="com.member.model.*"%>
<%@ page import="com.notificationList.model.*"%>

<%
    
    String boardID = request.getParameter("boardID");

    ADFundService adfundSvc = new ADFundService();
    List<ADFundVO> list = adfundSvc.getAllByBoardID(boardID); 
    pageContext.setAttribute("list",list);
    
    pageContext.setAttribute("adfundSvc", adfundSvc);
%>

<%
    MemberVO memberVO = (MemberVO) session.getAttribute("memberVO");
    NotificationListService noSvc = new NotificationListService();
    List<NotificationListVO> nolists = noSvc.getAllNoList(memberVO.getMemberID());
    pageContext.setAttribute("nolists",nolists);
%>

<!DOCTYPE html>
<html lang="en">
<head>
<title>廣告募資列表</title>
<meta charset="UTF-8">
<meta name="description" content="Food Blog Web Template">
<meta name="keywords" content="food, unica, creative, html">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<!-- Favicon -->   
	<link href="img/favicon.ico" rel="shortcut icon"/>

	<!-- Google Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Poppins:400,400i,500,500i,600,600i,700" rel="stylesheet">

	<!-- Stylesheets -->
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/css/bootstrap.min.css"/>
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/css/font-awesome.min.css"/>
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/css/owl.carousel.css"/>
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/css/animate.css"/>
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/css/style.css"/>
	<link rel="stylesheet" href="<%=request.getContextPath()%>/front-end/member/css/styleLogin.css">
	
	<!-- original -->			
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/adfund/css/bootstrap.min.css">
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/adfund/css/bootstrap-theme.min.css">
	
	<script src="<%=request.getContextPath() %>/front-end/adfund/js/jquery-3.3.1.min.js"></script>
	
	<script src="<%=request.getContextPath() %>/front-end/adfund/js/bootstrap.min.js"></script>
	




<style type="text/css">

body{
	background-color:white;
	font-family:\5FAE\8EDF\6B63\9ED1\9AD4,\65B0\7D30\660E\9AD4;
}

.wrap{
	width: 100%;
	max-width: 1000px;
	margin: auto;
	display: flex;
	flex-wrap: wrap;
	flex-direction: column;
	
}
.kk-ad-item{
	width: 100%;
	margin: 10px;
	padding: 15px;
	background-color: #92F2FF;
	flex-grow: 1;
	color: #000000;
	border:1px solid;
	border-color: #E3E0E0;
	border-radius: 10px;
}
@media screen and (min-width:768px){
	.kk-ad-item{
		width: 100%;
	}
}
@media screen and (min-width:1200px){
	.kk-ad-item{
		width: 100%;
	}
}
.kk-ad-item img{
	width: 100%;
	border-radius: 5px;
	border:1px solid;
	border-color:#E3E0E0;

}
.kk-ad-item:empty{
	background: none;
	border:0px;
}
.btn-g{
	display: flex;
	flex-direction: row-reverse;
}

.ad-look-btn{ 	
	background-color: #E3E0E0; 
	color:black;
	border: 1px solid;
	border-color: white;
	width: 150px;
	height: 30px;
	border-radius: 4px;
}

.kk-ad-list-group{
	padding:10px;   
}

.ad-join-btn{
	width: 100%;
	height:40px;
	background-color: #E3E0E0;
	color:black;
}


.progress{
    margin-bottom:10px;
    margin-top:10px;
}

.progress-bar{
    background-image: linear-gradient(to bottom,#638ff9 0,#638ff9 100%);

}


p {
  text-align: center;
  font-size: 20px;
  color:deeppink;
  margin-top: 0px;
  margin-bottom: 0px;
}

.kk-no-list{
   height:auto;
   border:1px solid;
   border-color: #E3E0E0;
   background-color: #E3E0E0;
   margin: 2px;
   padding: 2px;
   font-size:15px;
}

.kk-modal-body{
   height:300px;
   overflow-y: scroll;
   overflow-x: hidden;

}


</style>

</head>
<body onload="connect()" onunload="disconnect()">

	<div id="preloder">
		<div class="loader"></div>
	</div>

<!-- Header section --> 
 <header class="header-section">	
  <div class="header-bottom"> 
   <div class="container"> 
    <a href="<%=request.getContextPath()%>/front-end/index.jsp" class="site-logo"> 
     <img src="<%=request.getContextPath() %>/front-end/img/logo-1.png" alt=""> 
    </a> 
    <div class="nav-switch"> 
     <i class="fa fa-bars"></i> 
    </div> 

    <ul class="main-menu"> 
     <li><a href="<%= request.getContextPath()%>/front-end/board/listAllBoard.jsp"><img alt="藝人專板"  title="藝人專板" src="<%= request.getContextPath()%>/front-end/res/img/index/board.png" style="height:30px;width:30px"></a></li> 
     <li><a href="<%= request.getContextPath()%>/front-end/product/Mall.jsp"><img alt="商城"  title="商城" src="<%= request.getContextPath()%>/front-end/res/img/index/mall.png" style="height:30px;width:30px"></a></li> 
     <li><a href="<%=request.getContextPath()%>/front-end/gif_collection/gif_list.jsp"><img alt="GIF"  title="GIF" src="<%= request.getContextPath()%>/front-end/res/img/index/gif.png" style="height:30px;width:30px"></a></li> 
     <li><a href="<%=request.getContextPath()%>/front-end/member/listOneMember.jsp"><img alt="會員資料" title="會員資料" src="<%= request.getContextPath()%>/front-end/res/img/index/user.png" style="height:30px;width:30px"></a></li> 
     <li><a href="#" onclick="logout()"><img alt="登出" title="登出" src="<%= request.getContextPath()%>/front-end/res/img/index/logout.png" style="height:30px;width:30px"></a> 
     <li><a href="<%=request.getContextPath()%>/front-end/chatBox/chatBox.jsp"><img alt="聊天室" title="聊天室" src="<%= request.getContextPath()%>/front-end/res/img/index/chat.png" style="height:30px;width:30px"></a></li> 
     <li>${memberVO.memberNickName }</li>
     <li><a href="<%=request.getContextPath()%>/front-end/personalPage/myPersonalPage.jsp"><img src="<%= request.getContextPath()%>/front-end/member/memberImg.do?memberID=${memberVO.memberID}" id="memberPhoto"></a></li>	
     <li id="notifi"><a href="#modal-id-notifi" data-toggle="modal"> 
      <div style="position: relative;"> 
      <img src="<%=request.getContextPath()%>/front-end/res/img/index/notification(32).png" alt="通知" id="notifiPhoto"> 
      <span style="position: absolute; top: 0; left: 17px; color: white" id="notifiCount"></span> 
      </div> 
     </a></li> 
     </ul>
   </div> 
  </div> 
 </header> 
 <!-- Header section end -->
<FORM METHOD="post" ACTION="<%=request.getContextPath()%>/front-end/member/member.do" id="logout">
	<input type="hidden" name="action"	value="logOut">
</FORM>
<script type="text/javascript">
	function logout(){
		document.getElementById("logout").submit();
	}
</script>


	<%-- 錯誤表列 --%>
	<c:if test="${not empty errorMsgs}">
		<font style="color: red">請修正以下錯誤:</font>
		<ul>
			<c:forEach var="message" items="${errorMsgs}">
				<li style="color: red">${message}</li>
			</c:forEach>
		</ul>
	</c:if>


	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-3">
			
			   	<div class="list-group kk-ad-list-group">
					<FORM METHOD="post" ACTION="<%=request.getContextPath()%>/adfund/adfund.do" name="form1">
					<h4>依關鍵字搜尋:</h4>					
					<input type="text" name="adfundTitle" class="form-control"> 
					<input type="submit" value="送出" class="btn ad-join-btn">
                    <input type="hidden" name="action" value="adfund_list_ByCompositeQuery">
                 </FORM>
				</div>
				
				<div class="list-group kk-ad-list-group">
					<a href="<%=request.getContextPath() %>/front-end/adfund/adfund_input.jsp?boardID=<%=boardID%>" class="btn ad-join-btn">我要申請募資</a>
				</div>
					     
                <div class="list-group kk-ad-list-group">
					<a href="<%=request.getContextPath() %>/front-end/board/listAllBoard.jsp" class="btn ad-join-btn">回專板列表</a>
				</div>
				
				<div class="list-group kk-ad-list-group">
					<a href="<%=request.getContextPath() %>/front-end/board/listOneBoard.jsp?boardID=<%=boardID%>" class="btn ad-join-btn">回專板首頁</a>
				</div>
			</div>
	
			
			<div class="col-xs-12 col-sm-9">								
				<div class="wrap">
					<%@ include file="page1.file"%>
					<c:forEach var="adfundVO" items="${list}" begin="<%=pageIndex%>" end="<%=pageIndex+rowsPerPage-1%>">												
						<div class="item kk-ad-item">
						    <center><h2><b>- ${adfundVO.adfundTitle} -</b></h2></center>
						    <center><h4><span id="kk${adfundVO.adfundID}">${adfundVO.adfundStatus}</span></h4></center>
						    
						    <script> 
	                            $(document).ready(function() {

									if($('#kk${adfundVO.adfundID}').html() == 'AD_SUBMITTED'){
										$('#kk${adfundVO.adfundID}').html('目標達成 待審核');
									}else if($('#kk${adfundVO.adfundID}').html() == 'AD_FUNDING'){
										$('#kk${adfundVO.adfundID}').html('募資中');
									}else if($('#kk${adfundVO.adfundID}').html() == 'AD_PASSED'){
										$('#kk${adfundVO.adfundID}').html('募資通過!');
									}else if($('#kk${adfundVO.adfundID}').html() == 'AD_NOT_FINISH'){
										$('#kk${adfundVO.adfundID}').html('未達目標');
									}
									
	                            });
                            
                            </script>
						    
						   <!-- 倒數計時 -->
                           <p id="${adfundVO.adfundID}"></p>

                            <script type="text/javascript">
                            $(document).ready(function() {
                        	//倒數計時	
                        	// Set the date we're counting down to
                        	var countDownDate = new Date("${adfundVO.adfundEndDate}").getTime();
                        	
                        	// Update the count down every 1 second
                        	var x = setInterval(function() {
                        	
                        	    // Get todays date and time
                        	    var now = new Date().getTime();
                        	    
                        	    // Find the distance between now and the count down date
                        	    var distance = countDownDate - now;
                        	    
                        	    // Time calculations for days, hours, minutes and seconds
                        	    var days = Math.floor(distance / (1000 * 60 * 60 * 24));
                        	    var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                        	    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                        	    var seconds = Math.floor((distance % (1000 * 60)) / 1000);
                        	    
                        	    // Output the result in an element with id="demo"
                        	    document.getElementById("${adfundVO.adfundID}").innerHTML = "剩下" + days + "天  " + hours + "時 "
                        	    + minutes + "分 " + seconds + "秒 ";
                        	    
                        	    // If the count down is over, write some text 
                        	    if (distance < 0) {
                        	        clearInterval(x);
                        	        document.getElementById("${adfundVO.adfundID}").innerHTML = "";
                        	    }
                        	}, 1000);            	
                            });
                        	//倒數計時
                            
                            </script>
						    
							<img src="<%=request.getContextPath()%>/adfundImg/adfund.do?adfundID=${adfundVO.adfundID}&adfundHomePhoto=adfundHomePhoto">
<%--                              <img src="${adfundSvc.bytesToImage(adfundVO.adfundHomePhoto)}"> --%>
                             
                             
                             <div class="progress">
								<div class="progress-bar" role="progressbar" 
									style="width: <fmt:formatNumber value="${adfundVO.adfundCoinNow / adfundVO.adfundCoinTarget * 100}" pattern="#,#00.0"/>% ;"
									aria-valuenow="<fmt:formatNumber value="${adfundVO.adfundCoinNow / adfundVO.adfundCoinTarget * 100}" pattern="#,#00.0"/>"
									aria-valuemin="0" aria-valuemax="100"><fmt:formatNumber value="${adfundVO.adfundCoinNow / adfundVO.adfundCoinTarget * 100}" pattern="#,#00.0"/>%</div>
							</div>
							
													
							<h3><b><font color=deeppink>$${adfundVO.adfundCoinNow} </font></b>/ $3000</h3>
							<h4>▪ 時程: <fmt:formatDate value="${adfundVO.adfundEndDate}" pattern="yyyy-MM-dd HH:mm"/> 到 <fmt:formatDate value="${adfundVO.adfundEndDate}" pattern="yyyy-MM-dd HH:mm"/></h4>
							<h4>▪ 刊登時間: <fmt:formatDate value="${adfundVO.adfundDisplayDate}" pattern="yyyy-MM-dd HH:mm"/></h4>
   

							<div class="btn-g">

								<FORM METHOD="post"
									ACTION="<%=request.getContextPath()%>/adfund/adfund.do">
									<input type="submit" value="查看詳情" class="ad-look-btn">
									<input type="hidden" name="adfundID" value="${adfundVO.adfundID}"> 
									<input type="hidden" name="action" value="getOne_For_Display">
								</FORM>
							</div>
						</div>
					</c:forEach>

					<div class="item kk-ad-item"></div>
				</div>
				<center><%@ include file="page2.file"%></center>
			</div>
		</div>
	</div>
	
	
		<!-- 通知列表的跳窗 -->
		<div class="modal fade" id="modal-id-notifi">
			<div class="modal-dialog modal-sm">
			   <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<div class="modal-content">
				   
					<div class="modal-header">					
						<h4 class="modal-title">通知列表</h4>
					</div>
					<div class="modal-body kk-modal-body" >

                    <!-- 及時通知顯示的地方 -->
					<div id="nolistsssss" class="kk-no-list">
					  <a href="<%=request.getContextPath()%>/adfundTest/adfund.do?action=getOne_For_Display&adfundID=${adfundVO.adfundID}" ></a>
					 </div>
                        
						<c:forEach var="NotificationListVO" items="${nolists}" >
					      <div class="kk-no-list">					      
					         <a href="<%=request.getContextPath()%>${NotificationListVO.notificationpath}${NotificationListVO.notificationtarget}" id="${NotificationListVO.notificationid}">${NotificationListVO.notificationcontent}</a>
					         <h6><fmt:formatDate value="${NotificationListVO.notificationdate}" pattern="yyyy-MM-dd HH:mm"/></h6>
					      </div>
					    </c:forEach>
					</div>
					<div class="modal-footer">

					</div>
				</div>
			</div>
		</div>
	<!-- 通知的跳窗 -->

	<!--====== Javascripts & Jquery ======-->
	<script src="<%=request.getContextPath() %>/front-end/js/jquery-3.2.1.min.js"></script>
	<script src="<%=request.getContextPath() %>/front-end/owl.carousel.min.js"></script>
	<script src="<%=request.getContextPath() %>/front-end/js/main.js"></script>		
    <script src="https://code.jquery.com/jquery.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script>

$(document).ready(function() { 
	
	//點擊鈴鐺時，通知要全部變為已讀
    $('#notifi').click(function(){
    	
//              alert("!!")
				$.ajax({ 
				     url : '<%=request.getContextPath()%>/front-end/NotificationListServlet/NotificationListServlet.do', 
				     type : 'Post', 
				     data : { 
				      action : 'isClick', 
				      memberID : '${memberVO.memberID}' 

				     } 
				    }).done(function(res){
				    	var obj = JSON.parse(res);
				    	document.getElementById("notifiPhoto").src = "<%=request.getContextPath()%>/front-end/res/img/index/notification(32).png";
				    	$('#notifiCount').html("");
	                });				     
     });
 
	
//    alert("noti!!");
    //網頁一打開時，要立刻判斷是否有未讀訊息，看是否要顯示有消息的鈴鐺
    $.ajax({ 
	     url : '<%=request.getContextPath()%>/front-end/NotificationListServlet/NotificationListServlet.do', 
	     type : 'Post', 
	     data : { 
	      action : 'isRead',  
	      memberID : '${memberVO.memberID}'
	     } 
	    }).done(function(res){
	    	var obj = JSON.parse(res);
	    	if(obj.notiStatus==false){
	    	   document.getElementById("notifiPhoto").src = "<%=request.getContextPath()%>/front-end/res/img/index/notification(32)red.png";
	    	   $('#notifiCount').html(obj.falseCount);
	    	}
	    });
    
});
</script>

</body>
</html>