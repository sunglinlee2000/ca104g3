<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="com.adfund.model.*"%>
<%@ page import="com.board.model.*"%>
<%@ page import="com.notificationList.model.*"%>
<%@ page import="com.member.model.*"%>
<%@ page import="java.util.*"%>


<%

	//	會員
	MemberVO memberVO = (MemberVO) session.getAttribute("memberVO");
	String memberID = memberVO.getMemberID();
	
	//	通知
	NotificationListService noSvc = new NotificationListService();
	List<NotificationListVO> nolists = noSvc.getAllNoList(memberID);
	pageContext.setAttribute("nolists",nolists);
	
    ADFundVO adfundVO = (ADFundVO) session.getAttribute("adfundVO");
    String boardID = request.getParameter("boardID");
    BoardVO boardVO = (BoardVO) session.getAttribute("boardVO");
%>

<%
	response.setHeader("Cache-Control", "no-store");
	response.setHeader("Pragma", "no-cache");
	response.setDateHeader("Expires", 0);
%>

<!DOCTYPE html>
<html lang="en">
<head>
<title>申請廣告募資</title>
<meta charset="UTF-8">
<meta name="description" content="Food Blog Web Template">
<meta name="keywords" content="food, unica, creative, html">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<!-- Favicon -->   
	<link href="img/favicon.ico" rel="shortcut icon"/>

	<!-- Google Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Poppins:400,400i,500,500i,600,600i,700" rel="stylesheet">

	<!-- Stylesheets -->
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/css/bootstrap.min.css"/>
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/css/font-awesome.min.css"/>
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/css/owl.carousel.css"/>
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/css/animate.css"/>
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/css/style.css"/>
	<link rel="stylesheet" href="<%=request.getContextPath()%>/front-end/member/css/styleLogin.css">
	
	<!-- original -->			
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/adfund/css/bootstrap.min.css">
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/adfund/css/bootstrap-theme.min.css">
	
	<script src="<%=request.getContextPath() %>/front-end/adfund/js/jquery-3.3.1.min.js"></script>
	
	<script src="<%=request.getContextPath() %>/front-end/adfund/js/bootstrap.min.js"></script>
	
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<style type="text/css">
body{
	background-color:white;
	font-family:\5FAE\8EDF\6B63\9ED1\9AD4,\65B0\7D30\660E\9AD4;
}
.checkbox-inline + .checkbox-inline, .radio-inline + .radio-inline{
	margin-left: 0;
}
.checkbox-inline, .radio-inline{
	margin-right: 1em;
}

#preview_progressbarTW_img1, #preview_progressbarTW_img2 {
	border: 1px solid #ddd;
	border-radius: 4px;
	padding: 2px;
	width: 384px;
	margin-top: 5px;
	margin-bottom: 20px;
}

#preview_progressbarTW_img1:hover, #preview_progressbarTW_img2:hover {
	box-shadow: 0 0 2px 1px rgba(0, 140, 186, 0.5);
}

.kk-ad-input{
	border: 1px solid #ddd;
	padding:10px;
    font-size:20px;
    margin-top:5px;
}

.kk-no-list{
   height:auto;
   border:1px solid;
   border-color: #E3E0E0;
   background-color: #E3E0E0;
   margin: 2px;
   padding: 2px;
   font-size:15px;
}

.kk-modal-body{
   height:300px;
   overflow-y: scroll;
   overflow-x: hidden;

}

</style>

</head>
<body>

	<div id="preloder">
		<div class="loader"></div>
	</div>
	
	
<!-- Header section --> 
 <header class="header-section">	
  <div class="header-bottom"> 
   <div class="container"> 
    <a href="<%=request.getContextPath()%>/front-end/index.jsp" class="site-logo"> 
     <img src="<%=request.getContextPath() %>/front-end/img/logo-1.png" alt=""> 
    </a> 
    <div class="nav-switch"> 
     <i class="fa fa-bars"></i> 
    </div> 

    <ul class="main-menu"> 
     <li><a href="<%= request.getContextPath()%>/front-end/board/listAllBoard.jsp"><img alt="藝人專板"  title="藝人專板" src="<%= request.getContextPath()%>/front-end/res/img/index/board.png" style="height:30px;width:30px"></a></li> 
     <li><a href="<%= request.getContextPath()%>/front-end/product/Mall.jsp"><img alt="商城"  title="商城" src="<%= request.getContextPath()%>/front-end/res/img/index/mall.png" style="height:30px;width:30px"></a></li> 
     <li><a href="<%=request.getContextPath()%>/front-end/gif_collection/gif_list.jsp"><img alt="GIF"  title="GIF" src="<%= request.getContextPath()%>/front-end/res/img/index/gif.png" style="height:30px;width:30px"></a></li> 
     <li><a href="<%=request.getContextPath()%>/front-end/member/listOneMember.jsp"><img alt="會員資料" title="會員資料" src="<%= request.getContextPath()%>/front-end/res/img/index/user.png" style="height:30px;width:30px"></a></li> 
     <li><a href="#" onclick="logout()"><img alt="登出" title="登出" src="<%= request.getContextPath()%>/front-end/res/img/index/logout.png" style="height:30px;width:30px"></a> 
     <li><a href="<%=request.getContextPath()%>/front-end/chatBox/chatBox.jsp"><img alt="聊天室" title="聊天室" src="<%= request.getContextPath()%>/front-end/res/img/index/chat.png" style="height:30px;width:30px"></a></li> 
     <li>${memberVO.memberNickName }</li>
     <li><a href="<%=request.getContextPath()%>/front-end/personalPage/myPersonalPage.jsp"><img src="<%= request.getContextPath()%>/front-end/member/memberImg.do?memberID=${memberVO.memberID}" id="memberPhoto"></a></li>	
     <li id="notifi"><a href="#modal-id-notifi" data-toggle="modal"> 
      <div style="position: relative;"> 
      <img src="<%=request.getContextPath()%>/front-end/res/img/index/notification(32).png" alt="通知" id="notifiPhoto"> 
      <span style="position: absolute; top: 0; left: 17px; color: white" id="notifiCount"></span> 
      </div> 
     </a></li> 
     </ul>
   </div> 
  </div> 
 </header> 
 <!-- Header section end -->
<FORM METHOD="post" ACTION="<%=request.getContextPath()%>/front-end/member/member.do" id="logout">
	<input type="hidden" name="action"	value="logOut">
</FORM>
<script type="text/javascript">
	function logout(){
		document.getElementById("logout").submit();
	}
</script>

		</h4> <%-- 錯誤表列 --%> <c:if test="${not empty errorMsgs}">
			<font style="color: red">請修正以下錯誤</font>
			<ul>
				<c:forEach var="message" items="${errorMsgs}">
					<li style="color: red">${message}</li>
				</c:forEach>
			</ul>
		</c:if>

		<div class="container kk-ad-input">
			<div class="row">
				<div class="col-xs-12 col-sm-6 col-sm-offset-3">
					<form METHOD="post" ACTION="<%=request.getContextPath() %>/adfund/adfund.do" name="form1"
						enctype="multipart/form-data">

						<center>
							<div>
								<h3>
									<b>申請廣告募資</b>
								</h3>
							</div>
						</center>
						<br>

						<div class="form-group">
							<label>標題</label> <input type="text" class="form-control"
								required="required" placeholder="請填入標題" name="adfundTitle"
								value="<%= (adfundVO==null)? "五月天 FOREVER" : adfundVO.getAdfundTitle()%>" />
						</div>

						<div class="form-group">
							<label>刊登日期與時間</label> 
							<input type="text" class="form-control"
								required="required" name="adfundDisplayDate" id="adfundDisplayDate" onkeydown="return false">
								
							<div id="error-div" style="color:red;">					  
					        </div>
						</div>
						

						<div class="form-group">
							<label>廣告類型</label> <select class="form-control"
								required="required" name="adfundType">
								<option value="">-請選擇-</option>
								<option value="BIRTHDAY">慶生</option>
								<option value="COMEBACK">發片/回歸</option>
								<option value="ANNIVERSARY">周年活動</option>
								<option value="OTHER">其他</option>
							</select>
						</div>

						
						<jsp:useBean id="boardSvc" scope="page" class="com.board.model.BoardService" />
						<tr>
							<td><label>藝人:</label></td><br>
							<td><select size="1" name="boardID" class="form-control">
								<c:forEach var="boardVO" items="${boardSvc.allOperating}">
									<option value="${boardVO.boardID}" ${(adfundVO.boardID==boardVO.boardID)? 'selected':'' } >${boardVO.boardSignatureTitle}
								</c:forEach>
							</select></td>
						</tr>
						
						<br>


						<div class="from-group">
							<label>首頁圖片上傳</label> <input type="file" required="required"
								class="form-control" accept="image/jpeg,image/png" id="imgInp1"
								name="adfundHomePhoto" onchange="checkImg(this)" />
							<center>
								<img id="preview_progressbarTW_img1"
									src="<%=request.getContextPath() %>/front-end/res/img/adfund/other/18.png" />
							</center>
						</div>


						<div class="from-group">
							<label>藝人專板圖片上傳</label> <input type="file" required="required"
								class="form-control" accept="image/jpeg, image/png" id="imgInp2"
								name="adfundBoardPhoto" onchange="checkImg(this)" />
							<center>
								<img id="preview_progressbarTW_img2"
									src="<%=request.getContextPath() %>/front-end/res/img/adfund/other/18.png" />
							</center>
						</div>

						<div class="from-group"> 
							<label>募資說明</label>
							<textarea rows="5" required="required" class="form-control"
								name="adfundText"
								value="<%= (adfundVO==null)? "寫一個" : adfundVO.getAdfundText()%>"></textarea>
						</div>
						<br>
						<div>
							
							<input type="hidden" name="action" value="insert"> 
							<input type="hidden" name="adfundID" value="${adfundVO.adfundID}">
							<input type="hidden" name="memberID" value="${memberVO.memberID}">
							<center><input type="submit" value="提交" class="btn btn-info" id="submit-adfund">
							<a href="<%=request.getContextPath() %>/front-end/adfund/adfund_list.jsp?boardID=${boardVO.boardID}">
							<input type="button" value="返回募資列表" class="btn btn-info"></a></center>

							<%-- <center><a href="#" class="btn btn-info"></a></center>--%>
						</div>
					</form>
				</div>
			</div>
		</div> 
		<!--====== Javascripts & Jquery ======-->
	<script src="<%=request.getContextPath() %>/front-end/js/jquery-3.2.1.min.js"></script>
	<script src="<%=request.getContextPath() %>/front-end/owl.carousel.min.js"></script>
	<script src="<%=request.getContextPath() %>/front-end/js/main.js"></script>		
    <script src="https://code.jquery.com/jquery.js"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script type="text/javascript">

		//預覽圖片
		$('#imgInp1').change(function() {
			var file = $('#imgInp1')[0].files[0];
			var reader = new FileReader;
			reader.onload = function(e) {
				$('#preview_progressbarTW_img1').attr('src', e.target.result);
			};
			reader.readAsDataURL(file);
		});


		$('#imgInp2').change(function() {
			var file = $('#imgInp2')[0].files[0];
			var reader = new FileReader;
			reader.onload = function(e) {
				$('#preview_progressbarTW_img2').attr('src', e.target.result);
			};
			reader.readAsDataURL(file);
		});
        //預覽圖片


       
        
        
        //限制圖片上傳尺寸
        function checkImg(img){

        	var reader = new FileReader();
        	reader.onload = function(evt) {
        	var image = new Image();   
        	image.src = evt.target.result;
        	var height = image.height;   
        	var width = image.width;   
        	var filesize = img.files[0].size; 
        	  if(width!=1920 && height!=670){
        		img.value="";
        		alert('請上傳1920*670像素的圖片!');
        		
        	} 
        	}
        	reader.readAsDataURL(img.files[0]); 
        }
        
        
        //sweet alert
        $('#submit-adfund').click(function(){
        	swal("已成功提交一筆募資");
        });
</script>

		<!-- 通知列表的跳窗 -->
		<div class="modal fade" id="modal-id-notifi">
			<div class="modal-dialog modal-sm">
			   <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<div class="modal-content">
				   
					<div class="modal-header">					
						<h4 class="modal-title">通知列表</h4>
					</div>
					<div class="modal-body kk-modal-body" >

                    <!-- 及時通知顯示的地方 -->
					<div id="nolistsssss" class="kk-no-list">
					  <a href="<%=request.getContextPath()%>/adfundTest/adfund.do?action=getOne_For_Display&adfundID=${adfundVO.adfundID}" ></a>
					 </div>
                        
						<c:forEach var="NotificationListVO" items="${nolists}" >
					      <div class="kk-no-list">					      
					         <a href="<%=request.getContextPath()%>${NotificationListVO.notificationpath}${NotificationListVO.notificationtarget}" id="${NotificationListVO.notificationid}">${NotificationListVO.notificationcontent}</a>
					         <h6><fmt:formatDate value="${NotificationListVO.notificationdate}" pattern="yyyy-MM-dd HH:mm"/></h6>
					      </div>
					    </c:forEach>
					</div>
					<div class="modal-footer">

					</div>
				</div>
			</div>
		</div>
	<!-- 通知的跳窗 -->

<script>

$(document).ready(function() { 
	
	//點擊鈴鐺時，通知要全部變為已讀
    $('#notifi').click(function(){
    	
//              alert("!!")
				$.ajax({ 
				     url : '<%=request.getContextPath()%>/front-end/NotificationListServlet/NotificationListServlet.do', 
				     type : 'Post', 
				     data : { 
				      action : 'isClick', 
				      memberID : '${memberVO.memberID}' 

				     } 
				    }).done(function(res){
				    	var obj = JSON.parse(res);
				    	document.getElementById("notifiPhoto").src = "<%=request.getContextPath()%>/front-end/res/img/index/notification(32).png";
				    	$('#notifiCount').html("");
	                });				     
     });
 
	
//    alert("noti!!");
    //網頁一打開時，要立刻判斷是否有未讀訊息，看是否要顯示有消息的鈴鐺
    $.ajax({ 
	     url : '<%=request.getContextPath()%>/front-end/NotificationListServlet/NotificationListServlet.do', 
	     type : 'Post', 
	     data : { 
	      action : 'isRead',  
	      memberID : '${memberVO.memberID}'
	     } 
	    }).done(function(res){
	    	var obj = JSON.parse(res);
	    	if(obj.notiStatus==false){
	    	   document.getElementById("notifiPhoto").src = "<%=request.getContextPath()%>/front-end/res/img/index/notification(32)red.png";
	    	   $('#notifiCount').html(obj.falseCount);
	    	}
	    });
    
});
</script>    
</body>
<% 
   java.sql.Timestamp adfundDisplayDate = null;
   try {
	     
	   adfundDisplayDate = adfundVO.getAdfundDisplayDate();
 	  	  
    } catch (Exception e) {
    	adfundDisplayDate = new java.sql.Timestamp(System.currentTimeMillis());
    }
%>

<link rel="stylesheet" type="text/css"
	href="<%=request.getContextPath()%>/datetimepicker/jquery.datetimepicker.css" />
<script src="<%=request.getContextPath()%>/datetimepicker/jquery.js"></script>
<script
	src="<%=request.getContextPath()%>/datetimepicker/jquery.datetimepicker.full.js"></script>

<style>
.xdsoft_datetimepicker .xdsoft_datepicker {
	width: 300px; /* width:  300px; */
}

.xdsoft_datetimepicker .xdsoft_timepicker .xdsoft_time_box {
	height: 151px; /* height:  151px; */
}
</style>

<script>

$.datetimepicker.setLocale('zh'); // kr ko ja en
$(function(){
	 $('#adfundDisplayDate').datetimepicker({
	  format:'Y-m-d H:i',
	  onShow:function(){
	   this.setOptions({
	       minDate:$.now(),
	       maxDate:($.now()+180*24*60*60*1000)
	   })
	  },
	  timepicker:true,
	  step: 1
	 });
	 
	 
});
</script>
</html>