<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="com.member.model.*"%>

<%
	MemberVO memberVO = (MemberVO) request.getAttribute("memberVO");
%>

<!DOCTYPE html>
<html>
<head>
<title>FanPool</title>
<meta charset="UTF-8">
<meta name="description" content="Food Blog Web Template">
<meta name="keywords" content="food, unica, creative, html">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- Favicon -->   
<link href="<%=request.getContextPath()%>/front-end/img/favicon.ico" rel="shortcut icon"/>

<!-- Google Fonts -->
<link href="https://fonts.googleapis.com/css?family=Poppins:400,400i,500,500i,600,600i,700" rel="stylesheet">

<link rel="stylesheet" href="<%=request.getContextPath()%>/front-end/css/bootstrap.min.css">
<link rel="stylesheet" href="<%=request.getContextPath()%>/front-end/css/font-awesome.min.css">
<link rel="stylesheet" href="<%=request.getContextPath()%>/front-end/css/owl.carousel.css">
<link rel="stylesheet" href="<%=request.getContextPath()%>/front-end/css/animate.css">
<link rel="stylesheet" href="<%=request.getContextPath()%>/front-end/css/style.css">
<link rel="stylesheet" href="<%=request.getContextPath()%>/front-end/member/css/styleLogin.css">	



</head>
<body>
	<!-- Page Preloder -->
	<div id="preloder">
		<div class="loader"></div>
	</div>

	<!-- Header section -->
	<header class="header-section">

		<div class="header-bottom">
			<div class="container">
				<a href="<%=request.getContextPath()%>/front-end/index.jsp" class="site-logo">
					<img src="<%=request.getContextPath()%>/front-end/img/logo-1.png" alt="">
				</a>
				<div class="nav-switch">
					<i class="fa fa-bars"></i>
				</div>
			</div>
		</div>
	</header>
	<!-- Header section end -->


	<!-- Hero section -->
	<section class="hero-section">
		<div class="hero-slider owl-carousel">
			<div class="hero-slide-item set-bg" data-setbg="<%=request.getContextPath()%>/front-end/res/img/index/1.jpg">
			</div>
			<div class="hero-slide-item set-bg" data-setbg="<%=request.getContextPath()%>/front-end/res/img/index/2.jpg">
		</div>
	</section>
	<!-- Hero section end -->






	<!--區塊-->
	<section class="recipes-section spad">
		<div class="container">
			<div class="row">
				<div class="col-lg-3 ">
				</div>
				<div class="col-lg-6 col-md-6 col-md-6">
		  	    	<input type="image" src="<%=request.getContextPath()%>/front-end/res/img/index/btn_google_signin_dark_normal_web@2x.png" width="80%" alt="login google" id="loginBtn" onclick="googleSignIn()">
	  				<br>
	  				<input type="image" src="<%=request.getContextPath()%>/front-end/res/img/index/fblogin.png" width="80%"  alt="login facebook" id="loginBtn" onclick="fbLogin()">
				</div>
				<div class="col-lg-3 ">
				</div>
			</div>
		</div>
	</section>
	



	<!--footer-->
	<footer class="footer-section set-bg" data-setbg="">
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-6">
					<div class="footer-logo">
						<img src="<%=request.getContextPath()%>/front-end/img/logo-1.png" alt="">
					</div>
				</div>
				<div class="col-lg-6 text-lg-right">
					
					<p class="copyright">
						電話： 03-4257387
						<br>
						地址： 320桃園市中壢區中大路300號
					</p>
				</div>
			</div>
		</div>
	</footer>
	
	<FORM METHOD="post"	ACTION="<%=request.getContextPath()%>/front-end/member/member.do" id="form">
		<input type="hidden" name="memberName" id="name" value="">
		<input type="hidden" name="thirdPartyID" id="uid" value="">
		<input type="hidden" name="memberEmail" id="email" value="">
		<input type="hidden" name="memberPhotoURL" id="photo" value="">
		<input type="hidden" name="action" value="check_Member">
	</FORM>
	
    <script>
    
        //初始化設定載入Facebook SDK
        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = 'https://connect.facebook.net/zh_TW/sdk.js#xfbml=1&version=v3.0&appId=977377015768344';
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));

        //登入按鈕 //取得授權並登入應用程式
        function fbLogin() {
            FB.login(function(response) {
                statusChangeCallback(response);
            }, {scope: 'public_profile,email'});
        }
        //登出按鈕 //把FB真的登出
//         function fbLogout() {
//             FB.logout(function(response) {
//                 statusChangeCallback(response);
//             });
//         }
        //檢查登入狀態並取得資料
        function statusChangeCallback(response) {
            if (response.status === 'connected') {
                console.log('Facebook已登入')
                FB.api('/me?fields=id,name,picture,email', function(response) {
                    document.getElementById('uid').value=response.id
                    document.getElementById('name').value=response.name
                    document.getElementById('email').value=response.email
                    console.log("Image URL: " + response.picture.data.url);
                    document.getElementById('photo').value = response.picture.data.url;
                    document.getElementById("form").submit();
                });
            } else {
                console.log('Facebook未登入')
            }
        }
        
    </script>
	<script type="text/javascript">
        //初始化設定
        function handleClientLoad() {
          gapi.load('client:auth2', initClient);
        }
        function initClient() {
          gapi.client.init({
            clientId: '783649424635-tfcbl9s6c848pkgjo7o7eg4a2iifk1a9.apps.googleusercontent.com',
            scope: 'profile email openid'
          });
        }
        //登入按鈕
        function googleSignIn() {
            gapi.auth2.getAuthInstance().signIn().then(
           		function(){
           			updateSigninStatus();	
           		}
            );
        }
        //登出按鈕
//         function googleSignOut() {
//             gapi.auth2.getAuthInstance().signOut().then(
//            		function(){
//            			updateSigninStatus();	
//            		}
//             );
//         }
        //檢查登入狀態
        function updateSigninStatus() {
        	var isSignedIn = gapi.auth2.getAuthInstance().isSignedIn.get()
            if (isSignedIn) {
                console.log('google已登入')
                var profile = gapi.auth2.getAuthInstance().currentUser.get().getBasicProfile();
                document.getElementById('uid').value = profile.getId();
                document.getElementById('name').value = profile.getName();
                document.getElementById('email').value = profile.getEmail();
                console.log("Image URL: " + profile.getImageUrl());
                document.getElementById('photo').value = profile.getImageUrl();
                document.getElementById("form").submit();

            }else{
                console.log('google未登入')
                document.getElementById('com').innerText = "";
                document.getElementById('uid').innerText = "";
                document.getElementById('name').innerText = "";
                document.getElementById('email').innerText = "";
                document.getElementById('picture').src = "";
            }
        }
    </script>
    <script async defer src="https://apis.google.com/js/api.js" 
            onload="this.onload=function(){};handleClientLoad()" 
            onreadystatechange="if (this.readyState === 'complete') this.onload()">
    </script>
   	<script src="<%=request.getContextPath()%>/front-end/js/jquery-3.2.1.min.js"></script>
	<script src="<%=request.getContextPath()%>/front-end/js/owl.carousel.min.js"></script>
	<script src="<%=request.getContextPath()%>/front-end/js/main.js"></script>
</body>
</html>