<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page import="java.util.*"%>
<%@ page import="com.SNS.model.*"%>
<%@ page import="com.board.model.*"%>
<%@ page import="com.artist.model.*"%>
<%@ page import="com.SNSTranslated.model.*"%>
<%@ page import="com.notificationList.model.*"%>
<%@ page import="com.member.model.*"%>


<%

	//	會員
	MemberVO memberVO = (MemberVO) session.getAttribute("memberVO");
	String memberID = memberVO.getMemberID();
	
	//	通知
	NotificationListService noSvc = new NotificationListService();
	List<NotificationListVO> nolists = noSvc.getAllNoList(memberID);
	pageContext.setAttribute("nolists",nolists);

	
	ArtistVO artistVO = (ArtistVO) request.getAttribute("artistVO");
	String boardID = (String) request.getAttribute("boardID");
	SNSService snsSvc = new SNSService();
	List<SNSVO> list = snsSvc.getOne(artistVO.getArtistID());
	Collections.reverse(list);
	pageContext.setAttribute("list",list);
	
%>
<!DOCTYPE html>
<html>
<head>
<title>FanPool</title>
<meta charset="UTF-8">
<meta name="description" content="Food Blog Web Template">
<meta name="keywords" content="food, unica, creative, html">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- Favicon -->   
<link href="<%=request.getContextPath()%>/front-end/img/favicon.ico" rel="shortcut icon"/>
<!-- Google Fonts -->
<link href="https://fonts.googleapis.com/css?family=Poppins:400,400i,500,500i,600,600i,700" rel="stylesheet">
<!-- Stylesheets -->
<link rel="stylesheet" href="<%=request.getContextPath()%>/front-end/css/bootstrap.min.css">
<link rel="stylesheet" href="<%=request.getContextPath()%>/front-end/css/font-awesome.min.css">
<link rel="stylesheet" href="<%=request.getContextPath()%>/front-end/css/owl.carousel.css">
<link rel="stylesheet" href="<%=request.getContextPath()%>/front-end/css/animate.css">
<link rel="stylesheet" href="<%=request.getContextPath()%>/front-end/css/style.css">
<link rel="stylesheet" href="<%=request.getContextPath()%>/front-end/board/css/styleBoard.css">
<link rel="stylesheet" href="<%=request.getContextPath()%>/front-end/SNS/css/SNS.css">
<script src="<%=request.getContextPath()%>/front-end/js/jquery-3.2.1.min.js"></script>
<script src="<%=request.getContextPath()%>/front-end/js/owl.carousel.min.js"></script>
<script src="<%=request.getContextPath()%>/front-end/js/main.js"></script>
<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/adfund/css/bootstrap-theme.min.css">
<script src="<%=request.getContextPath() %>/front-end/adfund/js/bootstrap.min.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
	
<style type="text/css">
	body{
		font-family:\5FAE\8EDF\6B63\9ED1\9AD4,\65B0\7D30\660E\9AD4;
	}
	
	.kk-no-list{
	   height:auto;
	   border:1px solid;
	   border-color: #E3E0E0;
	   background-color: #E3E0E0;
	   margin: 2px;
	   padding: 2px;
	   font-size:15px;
	}
	
	.kk-modal-body{
	   height:300px;
	   overflow-y: scroll;
	   overflow-x: hidden;
	
	}

</style>

</head>
<body>
	<!-- Page Preloder -->
	<div id="preloder">
		<div class="loader"></div>
	</div>

<!-- Header section --> 
 <header class="header-section">	
  <div class="header-bottom"> 
   <div class="container"> 
    <a href="<%=request.getContextPath()%>/front-end/index.jsp" class="site-logo"> 
     <img src="<%=request.getContextPath() %>/front-end/img/logo-1.png" alt=""> 
    </a> 
    <div class="nav-switch"> 
     <i class="fa fa-bars"></i> 
    </div> 

    <ul class="main-menu"> 
     <li><a href="<%= request.getContextPath()%>/front-end/board/listAllBoard.jsp"><img alt="藝人專板"  title="藝人專板" src="<%= request.getContextPath()%>/front-end/res/img/index/board.png" style="height:30px;width:30px"></a></li> 
     <li><a href="<%= request.getContextPath()%>/front-end/product/Mall.jsp"><img alt="商城"  title="商城" src="<%= request.getContextPath()%>/front-end/res/img/index/mall.png" style="height:30px;width:30px"></a></li> 
     <li><a href="<%=request.getContextPath()%>/front-end/gif_collection/gif_list.jsp"><img alt="GIF"  title="GIF" src="<%= request.getContextPath()%>/front-end/res/img/index/gif.png" style="height:30px;width:30px"></a></li> 
     <li><a href="<%=request.getContextPath()%>/front-end/member/listOneMember.jsp"><img alt="會員資料" title="會員資料" src="<%= request.getContextPath()%>/front-end/res/img/index/user.png" style="height:30px;width:30px"></a></li> 
     <li><a href="#" onclick="logout()"><img alt="登出" title="登出" src="<%= request.getContextPath()%>/front-end/res/img/index/logout.png" style="height:30px;width:30px"></a> 
     <li><a href="<%=request.getContextPath()%>/front-end/chatBox/chatBox.jsp"><img alt="聊天室" title="聊天室" src="<%= request.getContextPath()%>/front-end/res/img/index/chat.png" style="height:30px;width:30px"></a></li> 
     <li>${memberVO.memberNickName }</li>
     <li><a href="<%=request.getContextPath()%>/front-end/personalPage/myPersonalPage.jsp"><img src="<%= request.getContextPath()%>/front-end/member/memberImg.do?memberID=${memberVO.memberID}" id="memberPhoto"></a></li>	
     <li id="notifi"><a href="#modal-id-notifi" data-toggle="modal"> 
      <div style="position: relative;"> 
      <img src="<%=request.getContextPath()%>/front-end/res/img/index/notification(32).png" alt="通知" id="notifiPhoto"> 
      <span style="position: absolute; top: 0; left: 17px; color: white" id="notifiCount"></span> 
      </div> 
     </a></li> 
     </ul>
   </div> 
  </div> 
 </header> 
 <!-- Header section end -->
<FORM METHOD="post" ACTION="<%=request.getContextPath()%>/front-end/member/member.do" id="logout">
	<input type="hidden" name="action"	value="logOut">
</FORM>
<script type="text/javascript">
	function logout(){
		document.getElementById("logout").submit();
	}
</script>



	<section class="recipes-section spad pt-0">
		<div class="container">
			<div class="section-title">
				<h2>社群文列表</h2>
			</div>
			
			<div class="row">
				<c:forEach var="snsVO" items="${list}">			
					<div class="row" style="border:1px solid black;border-radius: 8px;padding:10px;margin:5px;">
					<div class="col-lg-6 col-md-6 boardlist">
							<div class="recipe-info-warp">
								<div class="recipe-info">
									<h3><fmt:formatDate value="${snsVO.snsDate}" pattern="YYYY-MM-dd HH:mm"/></h3>
								</div>
							</div>
						<div class="recipe">
							<img src="<%= request.getContextPath()%>/front-end/SNSImg/SNSImg.do?snsID=${snsVO.snsID}" id="image${snsVO.snsID}" style="">

							<div style="padding:10px;margin:5px;border: 1px solid black;border-radius: 8px;"><span>${snsVO.snsContent }</span></div>
						</div>
					</div>
					<div class="col-lg-6 col-md-6 boardlist" >
						<div style="width:100%;padding:10px;margin-top:25px;" id="tt${snsVO.snsID}">
							<div style="height:80%;width:95%;margin-left:12px;margin-top:5px;overflow:scroll;overflow-X:hidden; "id="showTrans${snsVO.snsID}">

							<script>
							    
							$(document).ready(function(){
							    var img = document.getElementById('image${snsVO.snsID}'); 
								var height = img.clientHeight;
								document.getElementById('tt${snsVO.snsID}').style.height = height + "px";						    
							});
							 
							</script>

						<jsp:useBean id="SNSTranslatedSvc" scope="page"
							class="com.SNSTranslated.model.SNSTranslatedService" />
						<jsp:useBean id="memberSvc" scope="page"
							class="com.member.model.MemberService" />
								<c:forEach var="snsTranslatedVO" items="${SNSTranslatedSvc.getAllSNSTranslated(snsVO.snsID)}">
									<div style="padding:10px;margin:5px;border: 1px solid black;border-radius: 8px;" id="translated${snsTranslatedVO.snsTranslatedID}">${snsTranslatedVO.snsTranslatedContent}</div>
									<div class="row" style="padding:10px;margin-right:15px;margin-left:15px;border: 1px solid black;border-radius: 8px;display:none" id="info${snsTranslatedVO.snsTranslatedID}" >
									發文人:${(memberSvc.getOneMember(snsTranslatedVO.memberID).memberNickName)}
										<c:if test="${memberVO.memberID == snsTranslatedVO.memberID}">
										<input type="button" value="修改" style="background-color: #7dccff;border-radius: 8px;font-size: 10px; padding: 6px 12px;border:0px;display:block" id="updateBtn${snsTranslatedVO.snsTranslatedID}">
										</c:if>
										<c:if test="${memberVO.memberID != snsTranslatedVO.memberID}">
										<input type="button" value="修改" style="display:none">
										</c:if>
										<input type="hidden" value="${snsTranslatedVO.snsTranslatedID}" id="snstID${snsTranslatedVO.snsTranslatedID}">
								
										<c:if test="${memberVO.memberID == snsTranslatedVO.memberID}">
										<input type="button" value="檢舉" style="display:none">
										</c:if>
										<c:if test="${memberVO.memberID != snsTranslatedVO.memberID}">
										<input type="button" value="檢舉" style="background-color: #7dccff;border-radius: 8px;font-size: 10px; padding: 6px 12px;border:0px;display:block" id="snsReport${snsTranslatedVO.snsTranslatedID}">
										</c:if>
									
										<c:if test="${memberVO.memberID == snsTranslatedVO.memberID}">
										<input type="button" value="刪除" style="background-color: #7dccff;border-radius: 8px;font-size: 10px; padding: 6px 12px;border:0px;display:block" id="delete${snsTranslatedVO.snsTranslatedID}">
										</c:if>
										<c:if test="${memberVO.memberID != snsTranslatedVO.memberID}">
										<input type="button" value="刪除" style="display:none">
										</c:if>
									</div>
									
<div class="modal fade" id="myModal${snsTranslatedVO.snsTranslatedID}" role="dialog">
    <div class="modal-dialog" align="center">
    
      <!-- Modal content-->
      <div class="modal-content" style="width: 400px;" >
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          
          <center>
          	<h4 class="modal-title">留言檢舉</h4>
          </center>
        </div>
        <div class="modal-body" style="text-align: left; width: 250px;">
        	
        		  <input type="radio" name="snsReportReason" value="BULLY">重傷、歧視、挑釁或謾罵他人<br>
        		
			      <input type="radio" name="snsReportReason" value="BOARD_WASH">惡意洗版、重複張貼<br>							        		
        		
			      <input type="radio" name="snsReportReason" value="SEX">含有色情暴力成分<br>
			      
			      <input type="radio" name="snsReportReason" value="MARKETING">廣告或商業宣傳內容<br>
			      
			      <input type="radio" name="snsReportReason" value="OTHER">其他違規之項目<br>
        </div>
        <div align="center">
        	<button class="btn btn-default" id="sendReport${snsTranslatedVO.snsTranslatedID}">送出</button>
        </div>
        
      </div>
      
    </div>
 </div>
<script>
$(document).ready(function(){
	  $("#snsReport${snsTranslatedVO.snsTranslatedID}").click(function(){
	        $("#myModal${snsTranslatedVO.snsTranslatedID}").modal();
	    });
	
	$('#translated${snsTranslatedVO.snsTranslatedID}').click(function(){
		$('#info${snsTranslatedVO.snsTranslatedID}').toggle("fast");
	});

	$('#updateBtn${snsTranslatedVO.snsTranslatedID}').click(function(){
		$('#addTrans${snsVO.snsID}').hide();
		$('#updateTrans${snsVO.snsID}').show();
		$('#content${snsVO.snsID}').val('${snsTranslatedVO.snsTranslatedContent}');
		$('#Date${snsVO.snsID}').val('${snsTranslatedVO.snsTranslatedDate}');
		$('#update${snsVO.snsID}').val('${snsTranslatedVO.snsTranslatedID}');
	});

	$('#delete${snsTranslatedVO.snsTranslatedID}').click(function(){
		$.ajax({
		     url : '<%=request.getContextPath()%>/front-end/SNSTranslated/SNSTranslated.do',
		     type : 'Post',
		     data : {
		      action : 'delete',
		      snsTranslatedID : '${snsTranslatedVO.snsTranslatedID}'
		     }
	    }).done(function(){
	    	alert("刪除留言成功!!!");
	    	window.location.reload();
	    });			 
	});
	
	 $('#sendReport${snsTranslatedVO.snsTranslatedID}').click(function(){
		  $('#myModal${snsTranslatedVO.snsTranslatedID}').modal('hide');
			$.ajax({ 
			     url : '<%=request.getContextPath()%>/front-end/SNSReport/SNSReport.do', 
			     type : 'Post', 
			     data : { 
			      action : 'insert', 
			      boardID : '<%=boardID%>',
			      snsTranslatedID : '${snsTranslatedVO.snsTranslatedID}',
			      snsTranslatedContent : '${snsTranslatedVO.snsTranslatedContent}',
			      memberID :'${memberVO.memberID}',
			      snsReportReason:$('#myModal${snsTranslatedVO.snsTranslatedID} input[name="snsReportReason"]:checked').val()
			     } 
			    }).done(function(){
			    	swal({
			    		  title: "檢舉成功!",
			    		  icon: "success",
			    		  button: "確定"
			    	});
			});
	});
	
});

</script>
								</c:forEach>
	
							</div>
							<div style="height:25%;width:95%;margin-left:12px;margin-top:10px">
								<textarea rows="4" cols="50" style="width:100%;border-radius: 8px;" id="content${snsVO.snsID}"></textarea>
								<input type="hidden" value="${snsVO.snsID}" id="${snsVO.snsID}">
								<input type="hidden" value="" id="Date${snsVO.snsID}">
								<input type="button" value="新增留言" id="addTrans${snsVO.snsID}" style="background-color: #7dccff;border-radius: 8px;font-size: 10px; padding: 6px 12px;border:0px;display:block">
								<input type="hidden" value="" id="update${snsVO.snsID}">
								<input type="button" value="修改留言" id="updateTrans${snsVO.snsID}" style="background-color: #7dccff;border-radius: 8px;font-size: 10px; padding: 6px 12px;border:0px;display:none">
							</div>
						</div>
					</div>
<script>
	
	$(document).ready(function(){
		$('#addTrans${snsVO.snsID}').click(function(){
			$.ajax({
			     url : '<%=request.getContextPath()%>/front-end/SNSTranslated/SNSTranslated.do',
			     type : 'Post',
			     data : {
			      action : 'insert',
			      snsID : $('input#${snsVO.snsID}').val(),
			      memberID : '${memberVO.memberID}',
			      snsTranslatedContent : $('textarea#content${snsVO.snsID}').val()
			     }
		    }).done(function(data){
		    	data = JSON.parse(data);
		    	var id = data.id;
		    	var content = data.content;
		    	console.log(id);
		    	console.log(content);
		    	$('textarea#content${snsVO.snsID}').val('');
		    	alert("新增留言成功!!!");
		    	window.location.reload();
// 		    	$('#showTrans${snsVO.snsID}').append('<div style="padding:10px;margin:5px;border: 1px solid black;border-radius: 8px;" id="translated+id"></div>');
// 		    	$('#showTrans${snsVO.snsID}').append(`
// 			    	<div class="row" style="padding:10px;margin-right:15px;margin-left:15px;border: 1px solid black;border-radius: 8px;display:none" id="info+id">發文人:松霖李
// 						<input type="button" value="修改" style="display:block" id="updateBtn+id">
// 						<input type="hidden" value="id" id="snstID+id">
// 						<input type="button" value="讚" style="display:none">
// 						<input type="button" value="檢舉" style="display:none">
// 						<input type="button" value="刪除" style="display:block" id="delete+id">
// 					</div>
// 				`);
		    	
// 	    		$('#showTrans${snsVO.snsID}').on('click', '#translated+id', function(){
// 	    			$('#info+id').toggle("fast");
// 	    		});
	
// 	    		$('#showTrans${snsVO.snsID}').on('click', '#updateBtn+id', function(){
// 	    			$('#addTrans${snsVO.snsID}').hide();
// 	    			$('#updateTrans${snsVO.snsID}').show();
// 	    			$('#content${snsVO.snsID}').val('');
// 	    			$('#update${snsVO.snsID}').val('id');
// 	    		});
	 
// 	    		$('#showTrans${snsVO.snsID}').on('click', '#delete+id', function(){
// 	    			$.ajax({
<%-- 	    			     url : '<%=request.getContextPath()%>/front-end/SNSTranslated/SNSTranslated.do', --%>
// 	    			     type : 'Post',
// 	    			     data : {
// 	    			      action : 'delete',
// 	    			      snsTranslatedID : 'id'
// 	    			     }
// 	    		    }).done(function(){
// 	    		    	alert("刪除翻譯成功!!!");
// 	    		    });			 
// 	    		});
		    });			
		});
	});
	
	$(document).ready(function(){
		$('#updateTrans${snsVO.snsID}').click(function(){
			$.ajax({
			     url : '<%=request.getContextPath()%>/front-end/SNSTranslated/SNSTranslated.do',
			     type : 'Post',
			     data : {
			      action : 'update',
			      snsTranslatedID : $('#update${snsVO.snsID}').val(),
			      snsTranslatedContent : $('textarea#content${snsVO.snsID}').val()
			     }
		    }).done(function(){
		    	$('textarea#content${snsVO.snsID}').val('');
		    	alert("修改留言成功!!!");
		    	window.location.reload();
		    });			 
		});
	});
</script>		
				</div>
				</c:forEach>		
			</div>
		</div>
	</section>		
	


	<footer class="footer-section set-bg" data-setbg="">
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-6">
					<div class="footer-logo">
						<img src="<%=request.getContextPath()%>/front-end/img/logo-1.png">
					</div>
				</div>
				<div class="col-lg-6 text-lg-right">
					
					<p class="copyright">
						電話： 03-4257387
						<br>
						地址： 320桃園市中壢區中大路300號
					</p>
				</div>
			</div>
		</div>
	</footer>
	
			<!-- 通知列表的跳窗 -->
		<div class="modal fade" id="modal-id-notifi">
			<div class="modal-dialog modal-sm">
			   <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<div class="modal-content">
				   
					<div class="modal-header">					
						<h4 class="modal-title">通知列表</h4>
					</div>
					<div class="modal-body kk-modal-body" >

                    <!-- 及時通知顯示的地方 -->
					<div id="nolistsssss" class="kk-no-list">
					  <a href="<%=request.getContextPath()%>/adfundTest/adfund.do?action=getOne_For_Display&adfundID=${adfundVO.adfundID}" ></a>
					 </div>
                        
						<c:forEach var="NotificationListVO" items="${nolists}" >
					      <div class="kk-no-list">					      
					         <a href="<%=request.getContextPath()%>${NotificationListVO.notificationpath}${NotificationListVO.notificationtarget}" id="${NotificationListVO.notificationid}">${NotificationListVO.notificationcontent}</a>
					         <h6><fmt:formatDate value="${NotificationListVO.notificationdate}" pattern="yyyy-MM-dd HH:mm"/></h6>
					      </div>
					    </c:forEach>
					</div>
					<div class="modal-footer">

					</div>
				</div>
			</div>
		</div>
	<!-- 通知的跳窗 -->

<script>

$(document).ready(function() { 
	
	//點擊鈴鐺時，通知要全部變為已讀
    $('#notifi').click(function(){
    	
//              alert("!!")
				$.ajax({ 
				     url : '<%=request.getContextPath()%>/front-end/NotificationListServlet/NotificationListServlet.do', 
				     type : 'Post', 
				     data : { 
				      action : 'isClick', 
				      memberID : '${memberVO.memberID}' 

				     } 
				    }).done(function(res){
				    	var obj = JSON.parse(res);
				    	document.getElementById("notifiPhoto").src = "<%=request.getContextPath()%>/front-end/res/img/index/notification(32).png";
				    	$('#notifiCount').html("");
	                });				     
     });
 
	
//    alert("noti!!");
    //網頁一打開時，要立刻判斷是否有未讀訊息，看是否要顯示有消息的鈴鐺
    $.ajax({ 
	     url : '<%=request.getContextPath()%>/front-end/NotificationListServlet/NotificationListServlet.do', 
	     type : 'Post', 
	     data : { 
	      action : 'isRead',  
	      memberID : '${memberVO.memberID}'
	     } 
	    }).done(function(res){
	    	var obj = JSON.parse(res);
	    	if(obj.notiStatus==false){
	    	   document.getElementById("notifiPhoto").src = "<%=request.getContextPath()%>/front-end/res/img/index/notification(32)red.png";
	    	   $('#notifiCount').html(obj.falseCount);
	    	}
	    });
    
});
</script>   
</body>
</html>