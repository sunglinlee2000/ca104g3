<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page import="com.member.model.*"%>
<%@ page import="com.friend.model.*"%>
<%@ page import="com.issue.model.*"%>
<%@ page import="com.groupList.model.*"%>
<%@ page import="com.adfund.model.*"%>
<%@ page import="java.util.*"%>
<%@ page import="com.notificationList.model.*"%>

<%
// 	會員自己的memberID
	MemberVO memberVO = (MemberVO) session.getAttribute("memberVO");
	String memberID = memberVO.getMemberID();
	
	//	通知
	NotificationListService noSvc = new NotificationListService();
	List<NotificationListVO> nolists = noSvc.getAllNoList(memberID);
	pageContext.setAttribute("nolists",nolists);
	
		
// 	好友列表
	FriendService friendSvc = new FriendService();
	Set<FriendVO> set = friendSvc.getFriendsByMemberID(memberID);
	pageContext.setAttribute("set", set);
	
// 	閒聊話題
	IssueService issueSvc = new IssueService();
	List<IssueVO> listIssue = issueSvc.getAllByMemberID(memberID);
	pageContext.setAttribute("listIssue", listIssue);	
	
// 	揪團
	GroupListService groupListSvc = new GroupListService();
	List<GroupListVO> listGroupList = groupListSvc.getAllByMemberID(memberID);
	pageContext.setAttribute("listGroupList", listGroupList);
	
// 	募資
	ADFundService adfundSvc = new ADFundService();
	List<ADFundVO> listADFund = adfundSvc.getADFundsByMemberID(memberID);
	pageContext.setAttribute("listADFund", listADFund);
	
	response.setHeader("Cache-Control", "no-store");
	response.setHeader("Pragma", "no-cache");
	response.setDateHeader("Expires", 0);
%>
<jsp:useBean id="memSvc" scope="page" class="com.member.model.MemberService" />

<!DOCTYPE html>
<html>
<head>
<title>${memberVO.memberNickName}</title>
<meta charset="utf-8">
<meta name="description" content="Food Blog Web Template">
<meta name="keywords" content="food, unica, creative, html">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1">

	<!-- Favicon -->   
	<link href="img/favicon.ico" rel="shortcut icon"/>

	<!-- Google Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Poppins:400,400i,500,500i,600,600i,700" rel="stylesheet">

	<!-- Stylesheets -->
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/css/bootstrap.min.css"/>
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/css/bootstrap-theme.min.css">
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/css/font-awesome.min.css"/>
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/css/owl.carousel.css"/>
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/css/animate.css"/>
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/css/style.css"/>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="<%=request.getContextPath()%>/front-end/member/css/styleLogin.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css">

	<!--====== Javascripts & Jquery ======-->
	<script src="<%=request.getContextPath() %>/front-end/js/jquery-3.3.1.min.js"></script>
	<script src="<%=request.getContextPath()%>/front-end/js/bootstrap.min.js"></script>
	<script src="<%=request.getContextPath() %>/front-end/js/owl.carousel.min.js"></script>
	<script src="<%=request.getContextPath() %>/front-end/js/main.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>		
    <script src="https://code.jquery.com/jquery.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
	
<style type="text/css">
	.page-container {
		margin-top: 6px; 
		height: 100%;
	}
	
	#memberPhotoBig{
		border-radius: 50%;
		height:75px;
		width:75px;
		margin-bottom:10px;
	}
	
	#member-name {
		margin: 10px 0px;
		color: #bfbfbf;
		font-size: 150%;
		
	}
	
	ul {
		list-style-type: none;
	}
	
	.li-manipulation {
		margin:10px 0px;
		color: #bfbfbf;
	}
	
	.li-manipulation:hover {
		cursor: pointer;
	} 
		
	.friend-list {
		padding: 10px 33%;
	}
	
	.friend-list:hover {
		background-color: rgba(200,200,200,0.2);
		cursor: pointer;
	}
	
	.personal-content {
		height : 73vh;
		border-left: 1px solid rgba(200,200,200,0.2);
		border-right: 1px solid rgba(200,200,200,0.2);
		overflow: auto;
	}
		
	.issue, .group, .adfund {
		background-color: #f5f5f5;
		color: #f7cac9;
		height: 100%;
		margin-bottom: 15px;
		padding: 20px 10px;
		border-radius: 15px;
		
	}	
		
	.issue-float, .group-float, .adfund-float {
		float: right;
	}
	
	a {
		text-decoration: none;
	}
	
	a:hover {
		text-decoration: none;
	}
	
	.a-color {
		color: #91A8D0;
	}
	
	.a-color:hover {
		color: #404040;
	}

	
	.btn-gray-border {
		border: 1px solid #bfbfbf;
		background-color:white;
		color: #bfbfbf;
		text-align: center;
	}
	
	.btn-gray-border:hover {
		border: 1px solid #404040;
		background-color:white;
		color: #404040;
	}
	
	.btn-margin {
		margin: 10px 0px;
	}
	
	.modal-title{
	    float:left;
	}
	
	.kk-no-list{
	   height:auto;
	   border:1px solid;
	   border-color: #E3E0E0;
	   background-color: #E3E0E0;
	   margin: 2px;
	   padding: 2px;
	   font-size:15px;
	}
	
	.kk-modal-body{
	   height:300px;
	   overflow-y: scroll;
	   overflow-x: hidden;
	
	}

</style>
</head>
<body>
	 <!-- Page Preloder -->
	<div id="preloder">
		<div class="loader"></div>
	</div>

<!-- Header section --> 
 <header class="header-section">	
  <div class="header-bottom"> 
   <div class="container"> 
    <a href="<%=request.getContextPath()%>/front-end/index.jsp" class="site-logo"> 
     <img src="<%=request.getContextPath() %>/front-end/img/logo-1.png" alt=""> 
    </a> 
    <div class="nav-switch"> 
     <i class="fa fa-bars"></i> 
    </div> 

    <ul class="main-menu"> 
     <li><a href="<%= request.getContextPath()%>/front-end/board/listAllBoard.jsp"><img alt="藝人專板"  title="藝人專板" src="<%= request.getContextPath()%>/front-end/res/img/index/board.png" style="height:30px;width:30px"></a></li> 
     <li><a href="<%= request.getContextPath()%>/front-end/product/Mall.jsp"><img alt="商城"  title="商城" src="<%= request.getContextPath()%>/front-end/res/img/index/mall.png" style="height:30px;width:30px"></a></li> 
     <li><a href="<%=request.getContextPath()%>/front-end/gif_collection/gif_list.jsp"><img alt="GIF"  title="GIF" src="<%= request.getContextPath()%>/front-end/res/img/index/gif.png" style="height:30px;width:30px"></a></li> 
     <li><a href="<%=request.getContextPath()%>/front-end/member/listOneMember.jsp"><img alt="會員資料" title="會員資料" src="<%= request.getContextPath()%>/front-end/res/img/index/user.png" style="height:30px;width:30px"></a></li> 
     <li><a href="#" onclick="logout()"><img alt="登出" title="登出" src="<%= request.getContextPath()%>/front-end/res/img/index/logout.png" style="height:30px;width:30px"></a> 
     <li><a href="<%=request.getContextPath()%>/front-end/chatBox/chatBox.jsp"><img alt="聊天室" title="聊天室" src="<%= request.getContextPath()%>/front-end/res/img/index/chat.png" style="height:30px;width:30px"></a></li> 
     <li>${memberVO.memberNickName }</li>
     <li><a href="<%=request.getContextPath()%>/front-end/personalPage/myPersonalPage.jsp"><img src="<%= request.getContextPath()%>/front-end/member/memberImg.do?memberID=${memberVO.memberID}" id="memberPhoto"></a></li>	
     <li id="notifi"><a href="#modal-id-notifi" data-toggle="modal"> 
      <div style="position: relative;"> 
      <img src="<%=request.getContextPath()%>/front-end/res/img/index/notification(32).png" alt="通知" id="notifiPhoto"> 
      <span style="position: absolute; top: 0; left: 17px; color: white" id="notifiCount"></span> 
      </div> 
     </a></li> 
     </ul>
   </div> 
  </div> 
 </header> 
 <!-- Header section end -->
<FORM METHOD="post" ACTION="<%=request.getContextPath()%>/front-end/member/member.do" id="logout">
	<input type="hidden" name="action"	value="logOut">
</FORM>
<script type="text/javascript">
	function logout(){
		document.getElementById("logout").submit();
	}
</script>
		
	<div class="container-fluid">
		<div class="row">
			<div class="col-xs-12 col-sm-3">

	
				<div align="center">
					<img src="<%= request.getContextPath()%>/front-end/member/memberImg.do?memberID=${memberVO.memberID}" 
						id="memberPhotoBig">
					<div id="member-name">${memSvc.getOneMember(memberVO.memberID).memberNickName}</div>
					<ul>
						<li class="li-manipulation" id="tab1">閒聊話題</li>
						<li class="li-manipulation" id="tab2">揪團</li>
						<li class="li-manipulation" id="tab3">募資</li>
					</ul>
				</div>
			</div>


			<div class="col-xs-12 col-sm-6 personal-content">
				<!-- =========================================閒聊話題========================================= -->
				<div class="issueList" id="issueList">
					<c:forEach var="issueVO" items="${listIssue}">
						<div class="col-xs-12 col-sm-12">

							<div class="issue">
								<a class="a-color" href="<%=request.getContextPath()%>/issue/issue.do?issueID=${issueVO.issueID}
								&boardID=${issueVO.boardID}&action=getOne_For_Display">
									<span>${issueVO.issueTitle}</span>
								</a>
								<span class="issue-float"><fmt:formatDate value="${issueVO.issueDate}" pattern="yyyy-MM-dd"/>
								</span>
							</div>


						</div>
					</c:forEach>
				</div>


				<!-- =========================================揪團========================================= -->
				<div class="groupList" id="groupList">
					<c:forEach var="groupListVO" items="${listGroupList}">
						<div class="col-xs-12 col-sm-12">

							<div class="group">
								<a class="a-color" href="<%=request.getContextPath()%>/groupList/groupList.do?groupID=${groupListVO.groupID}
								&boardID=${groupListVO.boardID}&action=getOne_For_Display">
									<span>${groupListVO.groupTitle}</span>
								</a>
								<span class="group-float"><fmt:formatDate value="${groupListVO.groupStartDate}" pattern="yyyy-MM-dd"/>
								</span>
							</div>


						</div>
					</c:forEach>
				</div>

				<!-- =========================================募資========================================= -->
				<div class="adfundList" id="adfundList">
					<c:forEach var="adfundVO" items="${listADFund}">
						<div class="col-xs-12 col-sm-12">

							<div class="adfund">
								<a class="a-color" href="<%=request.getContextPath()%>/adfund/adfund.do?adfundID=${adfundVO.adfundID}
								&boardID=${adfundVO.boardID}&action=getOne_For_Display">
									<span>${adfundVO.adfundTitle}</span>
								</a>
								<span class="adfund-float"><fmt:formatDate value="${adfundVO.adfundStartDate}" pattern="yyyy-MM-dd"/>
								</span>
							</div>


						</div>
					</c:forEach>	
				</div>
			</div>

			<!-- =========================================好友列表========================================= -->
			<div class="col-xs-12 col-sm-3">
				<c:forEach var="friendVO" items="${set}">
					<div class="col-xs-12 col-sm-12 friend-list">
						<a href="<%=request.getContextPath()%>/personalPage/personalPage.do?memberID=${friendVO.friendID}&action=toOnePersonalPage">
							<img src="<%= request.getContextPath()%>/front-end/member/memberImg.do?memberID=${friendVO.friendID}" 
								id="memberPhoto">
						</a>
						<a href="<%=request.getContextPath()%>/personalPage/personalPage.do?memberID=${friendVO.friendID}&action=toOnePersonalPage">
							${memSvc.getOneMember(friendVO.friendID).memberNickName}
						</a>
					</div>
				</c:forEach>
			</div>	
		</div>
	</div>


	
	
		<!-- 通知列表的跳窗 -->
		<div class="modal fade" id="modal-id-notifi">
			<div class="modal-dialog modal-sm">
			   <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<div class="modal-content">
				   
					<div class="modal-header">					
						<h4 class="modal-title">通知列表</h4>
					</div>
					<div class="modal-body kk-modal-body" >

                    <!-- 及時通知顯示的地方 -->
					<div id="nolistsssss" class="kk-no-list">
					  <a href="<%=request.getContextPath()%>/adfundTest/adfund.do?action=getOne_For_Display&adfundID=${adfundVO.adfundID}" ></a>
					 </div>
                        
						<c:forEach var="NotificationListVO" items="${nolists}" >
					      <div class="kk-no-list">					      
					         <a href="<%=request.getContextPath()%>${NotificationListVO.notificationpath}${NotificationListVO.notificationtarget}" id="${NotificationListVO.notificationid}">${NotificationListVO.notificationcontent}</a>
					         <h6><fmt:formatDate value="${NotificationListVO.notificationdate}" pattern="yyyy-MM-dd HH:mm"/></h6>
					      </div>
					    </c:forEach>
					</div>
					<div class="modal-footer">

					</div>
				</div>
			</div>
		</div>
	<!-- 通知的跳窗 -->

<script>

$(document).ready(function() { 
	
	//點擊鈴鐺時，通知要全部變為已讀
    $('#notifi').click(function(){
    	
//              alert("!!")
				$.ajax({ 
				     url : '<%=request.getContextPath()%>/front-end/NotificationListServlet/NotificationListServlet.do', 
				     type : 'Post', 
				     data : { 
				      action : 'isClick', 
				      memberID : '${memberVO.memberID}' 

				     } 
				    }).done(function(res){
				    	var obj = JSON.parse(res);
				    	document.getElementById("notifiPhoto").src = "<%=request.getContextPath()%>/front-end/res/img/index/notification(32).png";
				    	$('#notifiCount').html("");
	                });				     
     });
 
	
//    alert("noti!!");
    //網頁一打開時，要立刻判斷是否有未讀訊息，看是否要顯示有消息的鈴鐺
    $.ajax({ 
	     url : '<%=request.getContextPath()%>/front-end/NotificationListServlet/NotificationListServlet.do', 
	     type : 'Post', 
	     data : { 
	      action : 'isRead',  
	      memberID : '${memberVO.memberID}'
	     } 
	    }).done(function(res){
	    	var obj = JSON.parse(res);
	    	if(obj.notiStatus==false){
	    	   document.getElementById("notifiPhoto").src = "<%=request.getContextPath()%>/front-end/res/img/index/notification(32)red.png";
	    	   $('#notifiCount').html(obj.falseCount);
	    	}
	    });
    
});
</script>
</body>
<script type="text/javascript">
	$(document).ready(function(e) {
		$('#issueList').show();
		$('#tab1').css({"color":"#404040"});
		$('#groupList').hide();
		$('#adfundList').hide();	
	});
	//=========================================閒聊話題=========================================
	$('#tab1').click(function(e) {
		$('#issueList').show();
		$('#tab1').css({"color":"#404040"});
		$('#groupList').hide();
		$('#tab2').css({"color":"#bfbfbf"});
		$('#adfundList').hide();
		$('#tab3').css({"color":"#bfbfbf"});
	});
	//=========================================揪團========================================
	$('#tab2').click(function(e) {
		$('#issueList').hide();
		$('#tab1').css({"color":"#bfbfbf"});
		$('#groupList').show();
		$('#tab2').css({"color":"#404040"});
		$('#adfundList').hide();
		$('#tab3').css({"color":"#bfbfbf"});
	});
	//=========================================募資=========================================
	$('#tab3').click(function(e) {
		$('#issueList').hide();
		$('#tab1').css({"color":"#bfbfbf"});
		$('#groupList').hide();
		$('#tab2').css({"color":"#bfbfbf"});
		$('#adfundList').show();
		$('#tab3').css({"color":"#404040"});
	});
		
</script>
</html>