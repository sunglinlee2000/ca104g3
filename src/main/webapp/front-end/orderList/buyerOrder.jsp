<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page import="java.util.*"%>
<%@ page import="com.product.model.*"%>
<%@ page import="com.orderList.model.*"%>
<%@ page import="com.member.model.*" %>
<%@ page import="com.notificationList.model.*"%>
<%-- 此頁練習採用 EL 的寫法取值 --%> 
<%
	//	會員
	MemberVO memberVO = (MemberVO) session.getAttribute("memberVO");
	String memberID = memberVO.getMemberID();
	
	//	通知
	NotificationListService noSvc = new NotificationListService();
	List<NotificationListVO> nolists = noSvc.getAllNoList(memberID);
	pageContext.setAttribute("nolists",nolists);

	OrderListService ordSvc=new OrderListService();
	List<OrderListVO> list=ordSvc.getAllBuyer(memberID);
	pageContext.setAttribute("list",list);
	System.out.println(list);
%>
	
	<!DOCTYPE html>
	<html lang="en">
	<head>
	<title>buyerOrder.jsp</title>
	<meta charset="UTF-8">
	<meta name="description" content="Food Blog Web Template">
	<meta name="keywords" content="food, unica, creative, html">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- Favicon -->   
	<link href="<%=request.getContextPath()%>/front-end/img/favicon.ico" rel="shortcut icon"/>

	<!-- Google Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Poppins:400,400i,500,500i,600,600i,700" rel="stylesheet">

	<!-- Stylesheets -->
	<link rel="stylesheet" href="<%=request.getContextPath()%>/front-end/css/bootstrap.min.css"/>
	<link rel="stylesheet" href="<%=request.getContextPath()%>/front-end/css/font-awesome.min.css"/>
	<link rel="stylesheet" href="<%=request.getContextPath()%>/front-end/css/owl.carousel.css"/>
	<link rel="stylesheet" href="<%=request.getContextPath()%>/front-end/css/animate.css"/>
	<link rel="stylesheet" href="<%=request.getContextPath()%>/front-end/css/style.css"/>
	
<!-- 原本	--- -->
	<link rel="stylesheet" href="<%=request.getContextPath()%>/front-end/product/css/bootstrap.min.css">
	<link rel="stylesheet" href="<%=request.getContextPath()%>/front-end/product/css/bootstrap-theme.min.css">
	
	<script src="<%=request.getContextPath()%>/front-end/product/js/jquery-3.3.1.min.js"></script>
	<!--         Latest compiled JavaScript -->
	<script src="<%=request.getContextPath()%>/front-end/product/js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="<%=request.getContextPath()%>/front-end/product/css/index.css">
	<link rel="stylesheet" href="<%=request.getContextPath()%>/front-end/product/css/seller.css">
	<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/adfund/css/bootstrap-theme.min.css"> 

			<style>
			 		body  {
						font-family: \5FAE\8EDF\6B63\9ED1\9AD4, \65B0\7D30\660E\9AD4;
				   }
					
					.stab {
						color: black;
					}
					
					
					
					#customers {
					    border-collapse: collapse;
					    width: 75%;
					    margin: auto;
					}
					
					#customers td, #customers th {
					    border: 1px solid #ddd;
					    padding: 8px;
					    text-align:center;
					    
					}
					
					
					#customers tr:nth-child(even){background-color: #f2f2f2;}
					
					#customers tr:hover {background-color: #ddd;}
					
					#customers th {
					    padding-top: 12px;
					    padding-bottom: 12px;
					    text-align: center;
					    background-color: #7dccff;
					    color: white;
					}
					
					#memberPhoto {
				    border-radius: 50%;
				    height: 30px;
				    width: 30px;
					}	
					.checked {
    					color: orange;	
    				}
    				
    				.kk-no-list{
					   height:auto;
					   border:1px solid;
					   border-color: #E3E0E0;
					   background-color: #E3E0E0;
					   margin: 2px;
					   padding: 2px;
					   font-size:15px;
					}
					
					.kk-modal-body{
					   height:300px;
					   overflow-y: scroll;
					   overflow-x: hidden;
					
					}	
			</style>
</head>
<body>
<!-- 		<div id="preloder"> -->
<!-- 		<div class="loader"></div> -->
<!-- 	</div> -->

		
			<!-- Header section --> 
 <header class="header-section">	
  <div class="header-bottom"> 
   <div class="container"> 
    <a href="<%=request.getContextPath()%>/front-end/index.jsp" class="site-logo"> 
     <img src="<%=request.getContextPath() %>/front-end/img/logo-1.png" alt=""> 
    </a> 
    <div class="nav-switch"> 
     <i class="fa fa-bars"></i> 
    </div> 

    <ul class="main-menu"> 
     <li><a href="<%= request.getContextPath()%>/front-end/board/listAllBoard.jsp"><img alt="藝人專板"  title="藝人專板" src="<%= request.getContextPath()%>/front-end/res/img/index/board.png" style="height:30px;width:30px"></a></li> 
     <li><a href="<%= request.getContextPath()%>/front-end/product/Mall.jsp"><img alt="商城"  title="商城" src="<%= request.getContextPath()%>/front-end/res/img/index/mall.png" style="height:30px;width:30px"></a></li> 
     <li><a href="<%=request.getContextPath()%>/front-end/gif_collection/gif_list.jsp"><img alt="GIF"  title="GIF" src="<%= request.getContextPath()%>/front-end/res/img/index/gif.png" style="height:30px;width:30px"></a></li> 
     <li><a href="<%=request.getContextPath()%>/front-end/member/listOneMember.jsp"><img alt="會員資料" title="會員資料" src="<%= request.getContextPath()%>/front-end/res/img/index/user.png" style="height:30px;width:30px"></a></li> 
     <li><a href="#" onclick="logout()"><img alt="登出" title="登出" src="<%= request.getContextPath()%>/front-end/res/img/index/logout.png" style="height:30px;width:30px"></a> 
     <li><a href="<%=request.getContextPath()%>/front-end/chatBox/chatBox.jsp"><img alt="聊天室" title="聊天室" src="<%= request.getContextPath()%>/front-end/res/img/index/chat.png" style="height:30px;width:30px"></a></li> 
     <li>${memberVO.memberNickName }</li>
     <li><a href="<%=request.getContextPath()%>/front-end/personalPage/myPersonalPage.jsp"><img src="<%= request.getContextPath()%>/front-end/member/memberImg.do?memberID=${memberVO.memberID}" id="memberPhoto"></a></li>	
     <li id="notifi"><a href="#modal-id-notifi" data-toggle="modal"> 
      <div style="position: relative;"> 
      <img src="<%=request.getContextPath()%>/front-end/res/img/index/notification(32).png" alt="通知" id="notifiPhoto"> 
      <span style="position: absolute; top: 0; left: 17px; color: white" id="notifiCount"></span> 
      </div> 
     </a></li> 
     </ul>
   </div> 
  </div> 
 </header> 
 <!-- Header section end -->
<FORM METHOD="post" ACTION="<%=request.getContextPath()%>/front-end/member/member.do" id="logout">
	<input type="hidden" name="action"	value="logOut">
</FORM>
<script type="text/javascript">
	function logout(){
		document.getElementById("logout").submit();
	}
</script>

			
			<div class="container-fluit">
				<div class="row">
					<div class="col-xs-12 col-sm-12 sellerbg">
						<center><h1>買家中心</h1></center>
					</div>
				</div>
			</div>
			
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-12">
									<br>
									<br>
									<table id="customers">
										<tr>
											<th>訂單編號</th>
											<th>商品編號</th>
											<th>購買數量</th>
											<th>訂單金額</th>
											<th>下單時間</th>
											<th>賣方評價</th>
											<th>訂單狀態</th>
											<th>評分</th>
											<th></th>
										</tr>
										
										
										
									<c:forEach var="orderListVO" items="${list}">
										<script>
											<c:if test="${orderListVO.vendorScore>0}">
											
												$(document).ready(function(){
													$('#addStar${orderListVO.orderID}').prop("disabled", true);
												});
											</c:if>
											<c:if test="${orderListVO.orderStatus=='PAID'}">
												$(document).ready(function(){
													$('#status${orderListVO.orderID}').html('已付款');
													$('#Receipt${orderListVO.orderID}').prop("disabled", true);
													$('#addStar${orderListVO.orderID}').prop("disabled", true);
												});
											</c:if>
											<c:if test="${orderListVO.orderStatus=='SHIPPED'}">
											$(document).ready(function(){
												$('#status${orderListVO.orderID}').html('已出貨');
											});
											</c:if>
											<c:if test="${orderListVO.orderStatus=='GOT'}">
											$(document).ready(function(){
												$('#status${orderListVO.orderID}').html('已收貨');
												$('#Receipt${orderListVO.orderID}').prop("disabled", true);
											});
											</c:if>
										
										</script>
										
										<tr ${orderListVO.orderID==param.orderID ? 'bgcolor=#ffecf5':'' }>
											<td>${orderListVO.orderID}</td>
											<td>${orderListVO.productID}</td>
											<td>${orderListVO.buyAmount}</td>
											<td>${orderListVO.orderPrice}</td>
											<td><fmt:formatDate value="${orderListVO.orderDate}" pattern="yyyy-MM-dd HH:mm" /></td>
											<td id="score${orderListVO.orderID}">${orderListVO.vendorScore}星</td>
											<td id="status${orderListVO.orderID}">${orderListVO.orderStatus}</td>
											<td><input type="submit" class="btn btn-warning" id="addStar${orderListVO.orderID}" value="評分"></td>
											<td> 
												<input type="submit" class="btn btn-warning" value="收貨" id="Receipt${orderListVO.orderID}">
											</td>
										</tr>
										<!-- 評分 -->
											<div class="modal fade" id="myModal${orderListVO.orderID}" role="dialog">
											    <div class="modal-dialog" align="center">
											    
											      <!-- Modal content-->
											      <div class="modal-content" style="width: 250px;height:240px;" >
											        <div class="modal-header">
											          <button type="button" class="close" data-dismiss="modal">&times;</button>
											        <center>
											          	<h4 class="modal-title">評價</h4>
											         </center>
											        </div>
											          <div class="modal-body" style="text-align: left; width: 110px;">
							        	
											        		  <input type="radio" name="vendorScore" value="1"><span class="fa fa-star checked"></span><br>
											        		
														      <input type="radio" name="vendorScore" value="2"><span class="fa fa-star checked"></span><span class="fa fa-star checked"></span> <br>							        		
											        		
														      <input type="radio" name="vendorScore" value="3"><span class="fa fa-star checked"></span><span class="fa fa-star checked"></span><span class="fa fa-star checked"></span> <br>
														      
														      <input type="radio" name="vendorScore" value="4"><span class="fa fa-star checked"></span><span class="fa fa-star checked"></span><span class="fa fa-star checked"></span><span class="fa fa-star checked"></span><br>
														      
														      <input type="radio" name="vendorScore" value="5"><span class="fa fa-star checked"></span><span class="fa fa-star checked"></span><span class="fa fa-star checked"></span><span class="fa fa-star checked"></span><span class="fa fa-star checked"></span><br>
											        	
											        	</div>
											        
											        <div align="center">
											        	<button class="btn btn-default" id="addStar2${orderListVO.orderID}">送出
											        	</button>
											        </div>
											        
											      </div>
											      
											    </div>
											</div>
										<script type="text/javascript">
											$(document).ready(function(){
												$('#addStar${orderListVO.orderID}').click(function(){
									    	        $("#myModal${orderListVO.orderID}").modal();
									    	    });
												
											});
											$('#addStar2${orderListVO.orderID}').click(function(){
									    		  $('#myModal${orderListVO.orderID}').modal('hide');
													$.ajax({ 
													     url : '<%=request.getContextPath()%>/orderList/orderList.do', 
													     type : 'Post', 
													     data : { 
													      action : 'addStar2', 
													      orderID :'${orderListVO.orderID}',
													      memberID:'${memberVO.memberID}',
													      vendorScore:$('#myModal${orderListVO.orderID} input[name="vendorScore"]:checked').val()
													     } 
													    }).done(function(res){
													    	var obj = JSON.parse(res);
													    	$('#score${orderListVO.orderID}').html(obj.vendorScore+'星');
													    	$('#addStar${orderListVO.orderID}').prop("disabled", true);
													    	swal({
													    		  title: "評價成功得到10代幣!",
													    		  icon: "success",
													    		  button: "確定"
													    		});
													    });
											});
											$('#Receipt${orderListVO.orderID}').click(function(){
									    		 
												$.ajax({ 
												     url : '<%=request.getContextPath()%>/orderList/orderList.do', 
												     type : 'Post', 
												     data : { 
												      action : 'update_Order_Status', 
												      orderID:'${orderListVO.orderID}',
												      orderStatus:'GOT'
												     } 
												    }).done(function(res){
												    	var obj = JSON.parse(res);
												    	$('#status${orderListVO.orderID}').html('已收貨');
												    	$('#Receipt${orderListVO.orderID}').prop("disabled", true);
												    });
											});
										
											</script>
										
									</c:forEach>
										</table>
										
								
							</div>
									
					</div>
							
				</div>
				
						<!-- 通知列表的跳窗 -->
		<div class="modal fade" id="modal-id-notifi">
			<div class="modal-dialog modal-sm">
			   <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<div class="modal-content">
				   
					<div class="modal-header">					
						<h4 class="modal-title">通知列表</h4>
					</div>
					<div class="modal-body kk-modal-body" >

                    <!-- 及時通知顯示的地方 -->
					<div id="nolistsssss" class="kk-no-list">
					  <a href="<%=request.getContextPath()%>/adfundTest/adfund.do?action=getOne_For_Display&adfundID=${adfundVO.adfundID}" ></a>
					 </div>
                        
						<c:forEach var="NotificationListVO" items="${nolists}" >
					      <div class="kk-no-list">					      
					         <a href="<%=request.getContextPath()%>${NotificationListVO.notificationpath}${NotificationListVO.notificationtarget}" id="${NotificationListVO.notificationid}">${NotificationListVO.notificationcontent}</a>
					         <h6><fmt:formatDate value="${NotificationListVO.notificationdate}" pattern="yyyy-MM-dd HH:mm"/></h6>
					      </div>
					    </c:forEach>
					</div>
					<div class="modal-footer">

					</div>
				</div>
			</div>
		</div>
	<!-- 通知的跳窗 -->

<script>

$(document).ready(function() { 
	
	//點擊鈴鐺時，通知要全部變為已讀
    $('#notifi').click(function(){
    	
//              alert("!!")
				$.ajax({ 
				     url : '<%=request.getContextPath()%>/front-end/NotificationListServlet/NotificationListServlet.do', 
				     type : 'Post', 
				     data : { 
				      action : 'isClick', 
				      memberID : '${memberVO.memberID}' 

				     } 
				    }).done(function(res){
				    	var obj = JSON.parse(res);
				    	document.getElementById("notifiPhoto").src = "<%=request.getContextPath()%>/front-end/res/img/index/notification(32).png";
				    	$('#notifiCount').html("");
	                });				     
     });
 
	
//    alert("noti!!");
    //網頁一打開時，要立刻判斷是否有未讀訊息，看是否要顯示有消息的鈴鐺
    $.ajax({ 
	     url : '<%=request.getContextPath()%>/front-end/NotificationListServlet/NotificationListServlet.do', 
	     type : 'Post', 
	     data : { 
	      action : 'isRead',  
	      memberID : '${memberVO.memberID}'
	     } 
	    }).done(function(res){
	    	var obj = JSON.parse(res);
	    	if(obj.notiStatus==false){
	    	   document.getElementById("notifiPhoto").src = "<%=request.getContextPath()%>/front-end/res/img/index/notification(32)red.png";
	    	   $('#notifiCount').html(obj.falseCount);
	    	}
	    });
    
});
</script>
							
		</body>
		
</html>