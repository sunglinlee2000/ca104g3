<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<!DOCTYPE html>
<html lang="en">
<head>
	<title>FanPool</title>
	<meta charset="UTF-8">
	<meta name="description" content="Food Blog Web Template">
	<meta name="keywords" content="food, unica, creative, html">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- Favicon -->   
	<link href="img/favicon.ico" rel="shortcut icon"/>

	<!-- Google Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Poppins:400,400i,500,500i,600,600i,700" rel="stylesheet">

	<!-- Stylesheets -->
	<link rel="stylesheet" href="css/bootstrap.min.css"/>
	<link rel="stylesheet" href="css/font-awesome.min.css"/>
	<link rel="stylesheet" href="css/owl.carousel.css"/>
	<link rel="stylesheet" href="css/animate.css"/>
	<link rel="stylesheet" href="css/style.css"/>
	<link rel="stylesheet" href="<%=request.getContextPath()%>/front-end/member/css/styleLogin.css">	
	
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/adfund/css/bootstrap.min.css">
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/adfund/css/bootstrap-theme.min.css">
	
	<script src="<%=request.getContextPath() %>/front-end/adfund/js/jquery-3.3.1.min.js"></script>
	
	<script src="<%=request.getContextPath() %>/front-end/adfund/js/bootstrap.min.js"></script>


<style type="text/css">
    body{
    	font-family:\5FAE\8EDF\6B63\9ED1\9AD4,\65B0\7D30\660E\9AD4;
    }

</style>
</head>
<body>
	<div id="preloder">
		<div class="loader"></div>
	</div>

	<!-- Header section -->
	<header class="header-section">
		
		<div class="header-bottom">
			<div class="container">
				<a href="<%=request.getContextPath()%>/front-end/index.jsp" class="site-logo">
					<img src="<%=request.getContextPath() %>/front-end/img/logo-1.png" alt="">
				</a>
				<div class="nav-switch">
					<i class="fa fa-bars"></i>
				</div>

				<ul class="main-menu">
					<li><a href="<%= request.getContextPath()%>/front-end/board/listAllBoard.jsp">藝人專板</a></li>
					<li><a href="about.html">商城</a></li>
					
					
					<li>
					<span class="icons">
				     <span class="dropdown">
					 <a href="#" data-toggle="dropdown">會員</a>
					    <ul class="dropdown-menu">
					      <li><a href="#">會員資料</a></li>
					      <li><a href="#">登出</a>
					    </ul>
				     </span>
				    </span>
					</li>																			
					<li><a href="recipes.html">通知</a></li>
					<li><a href="#">聊天室</a></li>
					<li><a href="#" onclick="logout()">登出</a></li>
					<li><a href="<%=request.getContextPath()%>/front-end/member/listOneMember.jsp">${memberVO.memberNickName}，您好</a></li>	
					<a href="<%=request.getContextPath()%>/front-end/member/listOneMember.jsp">	
					<li><img src="<%= request.getContextPath()%>/front-end/member/memberImg.do?memberID=${memberVO.memberID}" id="memberPhoto"></li>		
					</a>
			</div>
		</div>
	</header>
	<!-- Header section end -->
<FORM METHOD="post" ACTION="<%=request.getContextPath()%>/front-end/member/member.do" id="logout">
	<input type="hidden" name="action"	value="logOut">
</FORM>
<script type="text/javascript">
	function logout(){
		document.getElementById("logout").submit();
	}
</script>

	<!-- Hero section -->
	<section class="hero-section">
		<div class="hero-slider owl-carousel">
			<div class="hero-slide-item set-bg" data-setbg="res/img/index/12.png">
				<div class="hs-text">
					<h2 class="hs-title-1"><span></span></h2>
					<h2 class="hs-title-2"><span>FAN POOL</span></h2>
					<h2 class="hs-title-3"><span>JUST FOR FUN</span></h2>
				</div>
			</div>
			<div class="hero-slide-item set-bg" data-setbg="res/img/index/1.png">
			</div>
			<div class="hero-slide-item set-bg" data-setbg="res/img/index/2.png">
			</div>
			<div class="hero-slide-item set-bg" data-setbg="res/img/index/19.png">
			</div>
		</div>
	</section>
	<!-- Hero section end -->


	<!-- Recipes section -->
	<section class="recipes-section spad pt-0">
		<div class="container">
			<div class="section-title">
				<h2>商城</h2>
			</div>
			<div class="row">
				<div class="col-lg-4 col-md-6">
					<div class="recipe">
						<img src="img/recipes/1.jpg" alt="">
						<div class="recipe-info-warp">
							<div class="recipe-info">
								<h3>Traditional Pizza</h3>
								<div class="rating">
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star is-fade"></i>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-6 col-md-6">
					<div class="recipe">
						<img src="img/recipes/2.jpg" alt="">
						<div class="recipe-info-warp">
							<div class="recipe-info">
								<h3>Italian home-made pasta</h3>
								<div class="rating">
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star is-fade"></i>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-6">
					<div class="recipe">
						<img src="img/recipes/3.jpg" alt="">
						<div class="recipe-info-warp">
							<div class="recipe-info">
								<h3>Chesse Cake Tart</h3>
								<div class="rating">
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star is-fade"></i>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-6">
					<div class="recipe">
						<img src="img/recipes/4.jpg" alt="">
						<div class="recipe-info-warp">
							<div class="recipe-info">
								<h3>Traditional Pizza</h3>
								<div class="rating">
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star is-fade"></i>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-6">
					<div class="recipe">
						<img src="img/recipes/5.jpg" alt="">
						<div class="recipe-info-warp">
							<div class="recipe-info">
								<h3>Italian home-made pasta</h3>
								<div class="rating">
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star is-fade"></i>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-6">
					<div class="recipe">
						<img src="img/recipes/6.jpg" alt="">
						<div class="recipe-info-warp">
								<div class="recipe-info">
								<h3>Chesse Cake Tart</h3>
								<div class="rating">
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star is-fade"></i>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Recipes section end -->


	<!-- Gallery section -->
	<div class="gallery">
		<div class="gallery-slider owl-carousel">
			<div class="gs-item set-bg" data-setbg="res/img/board/2-1.jpg"></div>
			<div class="gs-item set-bg" data-setbg="res/img/board/2.jpg"></div>
			<div class="gs-item set-bg" data-setbg="res/img/board/3.jpg"></div>
			<div class="gs-item set-bg" data-setbg="res/img/board/4.jpg"></div>
			<div class="gs-item set-bg" data-setbg="res/img/board/5.jpg"></div>
			<div class="gs-item set-bg" data-setbg="res/img/board/6.jpg"></div>
		</div>
	</div>
	<!-- Gallery section end -->


	<!-- Footer section  -->
	<footer class="footer-section set-bg" data-setbg="">
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-6">
					<div class="footer-logo">
						<img src="<%=request.getContextPath()%>/front-end/img/logo-1.png" alt="">
					</div>
				</div>
				<div class="col-lg-6 text-lg-right">
					
					<p class="copyright">
						電話： 03-4257387
						<br>
						地址： 320桃園市中壢區中大路300號
					</p>
				</div>
			</div>
		</div>
	</footer>
	<!-- Footer section end -->



	<!--====== Javascripts & Jquery ======-->
	<script src="js/jquery-3.2.1.min.js"></script>
	<script src="js/owl.carousel.min.js"></script>
	<script src="js/main.js"></script>
</body>
</html>