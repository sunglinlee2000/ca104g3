<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page import="java.sql.*"%>
<%@ page import="java.util.*"%>
<%@ page import="com.groupList.model.*"%>
<%@ page import="com.notificationList.model.*"%>
<%@ page import="com.member.model.*"%>

<%

	//	會員
	MemberVO memberVO = (MemberVO) session.getAttribute("memberVO");
	String memberID = memberVO.getMemberID();
	
	//	通知
	NotificationListService noSvc = new NotificationListService();
	List<NotificationListVO> nolists = noSvc.getAllNoList(memberID);
	pageContext.setAttribute("nolists",nolists);

	GroupListVO groupListVO = (GroupListVO) request.getAttribute("groupListVO");
	response.setHeader("Cache-Control", "no-store");
	response.setHeader("Pragma", "no-cache");
	response.setDateHeader("Expires", 0);
%>

<!DOCTYPE html>
<html>
<head>
<title>揪團列表</title>
<meta charset="utf-8">
<meta name="description" content="Food Blog Web Template">
<meta name="keywords" content="food, unica, creative, html">
<meta name="viewport" content="width=device-width, initial-scale=1.0">


	<!-- Favicon -->   
	<link href="img/favicon.ico" rel="shortcut icon"/>

	<!-- Google Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Poppins:400,400i,500,500i,600,600i,700" rel="stylesheet">

	<!-- Stylesheets -->
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/css/bootstrap.min.css"/>
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/css/bootstrap-theme.min.css">
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/css/font-awesome.min.css"/>
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/css/owl.carousel.css"/>
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/css/animate.css"/>
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/css/style.css"/>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="<%=request.getContextPath()%>/front-end/board/css/styleBoard.css">
<!-- 	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"> -->
<!-- 	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"> -->

	<!--====== Javascripts & Jquery ======-->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>	
	<script src="<%=request.getContextPath() %>/front-end/js/owl.carousel.min.js"></script>
	<script src="<%=request.getContextPath() %>/front-end/js/main.js"></script>
	<script src="<%=request.getContextPath()%>/ckeditor/ckeditor.js"></script>	
    <script src="https://code.jquery.com/jquery.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>

<style type="text/css">
.wrongTable-margin {
	margin-top: 15px;
}

.group-input {
	margin-bottom: 15px;
}

.table {
	width: 97%;
}

.table-margin {
	margin-top: 15px;
}

.btn-gray-border {
	border: 1px solid #bfbfbf;
	background-color:white;
	color: #bfbfbf;
	text-align: center;
}

.btn-gray-border:hover {
	border: 1px solid #404040;
	background-color:white;
	color: #404040;
}

.btn-margin {
	margin: 10px 0px;
}

.modal-title{
    float:left;
}

.kk-no-list{
   height:auto;
   border:1px solid;
   border-color: #E3E0E0;
   background-color: #E3E0E0;
   margin: 2px;
   padding: 2px;
   font-size:15px;
}

.kk-modal-body{
   height:300px;
   overflow-y: scroll;
   overflow-x: hidden;

}

</style>


</head>
<body>
	 <!-- Page Preloder -->
	<div id="preloder">
		<div class="loader"></div>
	</div>

<!-- Header section --> 
 <header class="header-section">	
  <div class="header-bottom"> 
   <div class="container"> 
    <a href="<%=request.getContextPath()%>/front-end/index.jsp" class="site-logo"> 
     <img src="<%=request.getContextPath() %>/front-end/img/logo-1.png" alt=""> 
    </a> 
    <div class="nav-switch"> 
     <i class="fa fa-bars"></i> 
    </div> 

    <ul class="main-menu"> 
     <li><a href="<%= request.getContextPath()%>/front-end/board/listAllBoard.jsp"><img alt="藝人專板"  title="藝人專板" src="<%= request.getContextPath()%>/front-end/res/img/index/board.png" style="height:30px;width:30px"></a></li> 
     <li><a href="<%= request.getContextPath()%>/front-end/product/Mall.jsp"><img alt="商城"  title="商城" src="<%= request.getContextPath()%>/front-end/res/img/index/mall.png" style="height:30px;width:30px"></a></li> 
     <li><a href="<%=request.getContextPath()%>/front-end/gif_collection/gif_list.jsp"><img alt="GIF"  title="GIF" src="<%= request.getContextPath()%>/front-end/res/img/index/gif.png" style="height:30px;width:30px"></a></li> 
     <li><a href="<%=request.getContextPath()%>/front-end/member/listOneMember.jsp"><img alt="會員資料" title="會員資料" src="<%= request.getContextPath()%>/front-end/res/img/index/user.png" style="height:30px;width:30px"></a></li> 
     <li><a href="#" onclick="logout()"><img alt="登出" title="登出" src="<%= request.getContextPath()%>/front-end/res/img/index/logout.png" style="height:30px;width:30px"></a> 
     <li><a href="<%=request.getContextPath()%>/front-end/chatBox/chatBox.jsp"><img alt="聊天室" title="聊天室" src="<%= request.getContextPath()%>/front-end/res/img/index/chat.png" style="height:30px;width:30px"></a></li> 
     <li>${memberVO.memberNickName }</li>
     <li><a href="<%=request.getContextPath()%>/front-end/personalPage/myPersonalPage.jsp"><img src="<%= request.getContextPath()%>/front-end/member/memberImg.do?memberID=${memberVO.memberID}" id="memberPhoto"></a></li>	
     <li id="notifi"><a href="#modal-id-notifi" data-toggle="modal"> 
      <div style="position: relative;"> 
      <img src="<%=request.getContextPath()%>/front-end/res/img/index/notification(32).png" alt="通知" id="notifiPhoto"> 
      <span style="position: absolute; top: 0; left: 17px; color: white" id="notifiCount"></span> 
      </div> 
     </a></li> 
     </ul>
   </div> 
  </div> 
 </header> 
 <!-- Header section end -->
<FORM METHOD="post" ACTION="<%=request.getContextPath()%>/front-end/member/member.do" id="logout">
	<input type="hidden" name="action"	value="logOut">
</FORM>
<script type="text/javascript">
	function logout(){
		document.getElementById("logout").submit();
	}
</script>


	<%-- =========================================error message========================================= --%>
	<div class="container-fluid aa">
		<div class="row">
			<div class="col-xs-12 col-sm-6 col-sm-offset-3">
				<c:if test="${not empty errorMsgs}">
					<table class="table table-condensed table-margin">
						<thead>
							<tr>
								<th>
									<font style="color: red">請修正以下錯誤：</font>
								</th>
							</tr>
						</thead>
						
						<tbody>
							
							<c:forEach var="message" items="${errorMsgs}">
								<tr>
									<td>
									 <font style="color: red">${message}</font>
									</td>
								</tr>
							</c:forEach>
							
						</tbody>
					</table>
				</c:if>
			</div>
		</div>
	</div>

	<!-- =========================================form========================================= -->
	<div class="container-fluid aa">
		<div class="row">
			<div class="col-xs-12 col-sm-6 col-sm-offset-3">
				<form method="post" action="<%=request.getContextPath()%>/groupList/groupList.do" id="form-updateOneGroup">
					<div class="input-group group-input">
						<div class="input-group-addon">揪團名稱</div>
						<input class="form-control" type="text" name="groupTitle"
							value="<%=(groupListVO == null) ? "" : groupListVO.getGroupTitle()%>">
					</div>
					<div class="input-group group-input">
						<div class="input-group-addon">集合時間</div>
						<input class="form-control" type="text" name="groupMeetDate"
							id="f_date1">
					</div>
					<div class="input-group group-input">
						<div class="input-group-addon">集合地點</div>
						<input class="form-control" type="text" name="groupLocation"
							id="groupLocation" value="<%=(groupListVO == null) ? "" : groupListVO.getGroupLocation()%>">

					</div>
					<div class="input-group group-input">
						<div class="input-group-addon">揪團內容</div>
						<textarea form="form-updateOneGroup" class="form-control" name="groupContent">
							<%=(groupListVO == null) ? "" : groupListVO.getGroupContent()%>
                        </textarea>
					</div>
					<center>
						<input type="hidden" name="action" value="update">
						<input type="hidden" name="latitude" id="latitude">
						<input type="hidden" name="longitude" id="longitude"> 
						<input type="hidden" name="groupID" value="${groupListVO.groupID}">
						<input type="hidden" name="whichPage" value="<%=request.getParameter("whichPage") %>"> 
						<input type="hidden" name="boardID" value="<%=request.getParameter("boardID")%>">
						<input id="formBtn"type="submit" class="btn btn-gray-border btn-margin" value="確定修改">
					</center>
				</form>
			</div>
		</div>
	</div>










	<!-- =========================================footer=========================================-->
<!-- 	<div class="footer"> -->
<!-- 		<div class="row"> -->
<!-- 			<div class="col-xs-12 col-sm-12 footer-text"> -->
<!-- 				<h6>電話： 03-4257387</h6> -->
<!-- 				<h6>地址： 320桃園市中壢區中大路300號</h6> -->
<!-- 			</div> -->
<!-- 		</div> -->
<!-- 	</div> -->
</body>


		<!-- 通知列表的跳窗 -->
		<div class="modal fade" id="modal-id-notifi">
			<div class="modal-dialog modal-sm">
			   <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<div class="modal-content">
				   
					<div class="modal-header">					
						<h4 class="modal-title">通知列表</h4>
					</div>
					<div class="modal-body kk-modal-body" >

                    <!-- 及時通知顯示的地方 -->
					<div id="nolistsssss" class="kk-no-list">
					  <a href="<%=request.getContextPath()%>/adfundTest/adfund.do?action=getOne_For_Display&adfundID=${adfundVO.adfundID}" ></a>
					 </div>
                        
						<c:forEach var="NotificationListVO" items="${nolists}" >
					      <div class="kk-no-list">					      
					         <a href="<%=request.getContextPath()%>${NotificationListVO.notificationpath}${NotificationListVO.notificationtarget}" id="${NotificationListVO.notificationid}">${NotificationListVO.notificationcontent}</a>
					         <h6><fmt:formatDate value="${NotificationListVO.notificationdate}" pattern="yyyy-MM-dd HH:mm"/></h6>
					      </div>
					    </c:forEach>
					</div>
					<div class="modal-footer">

					</div>
				</div>
			</div>
		</div>
	<!-- 通知的跳窗 -->

<script>

$(document).ready(function() { 
	
	//點擊鈴鐺時，通知要全部變為已讀
    $('#notifi').click(function(){
    	
//              alert("!!")
				$.ajax({ 
				     url : '<%=request.getContextPath()%>/front-end/NotificationListServlet/NotificationListServlet.do', 
				     type : 'Post', 
				     data : { 
				      action : 'isClick', 
				      memberID : '${memberVO.memberID}' 

				     } 
				    }).done(function(res){
				    	var obj = JSON.parse(res);
				    	document.getElementById("notifiPhoto").src = "<%=request.getContextPath()%>/front-end/res/img/index/notification(32).png";
				    	$('#notifiCount').html("");
	                });				     
     });
 
	
//    alert("noti!!");
    //網頁一打開時，要立刻判斷是否有未讀訊息，看是否要顯示有消息的鈴鐺
    $.ajax({ 
	     url : '<%=request.getContextPath()%>/front-end/NotificationListServlet/NotificationListServlet.do', 
	     type : 'Post', 
	     data : { 
	      action : 'isRead',  
	      memberID : '${memberVO.memberID}'
	     } 
	    }).done(function(res){
	    	var obj = JSON.parse(res);
	    	if(obj.notiStatus==false){
	    	   document.getElementById("notifiPhoto").src = "<%=request.getContextPath()%>/front-end/res/img/index/notification(32)red.png";
	    	   $('#notifiCount').html(obj.falseCount);
	    	}
	    });
    
});
</script>

<!-- =========================================datetimepicker========================================== -->

<%
	Timestamp groupMeetDate = null;
	try {
		groupMeetDate = groupListVO.getGroupMeetDate();
	} catch (Exception e) {
		groupMeetDate = new Timestamp(System.currentTimeMillis());
	}
%>
<link rel="stylesheet" type="text/css"
	href="<%=request.getContextPath()%>/datetimepicker/jquery.datetimepicker.css" />
<script src="<%=request.getContextPath()%>/datetimepicker/jquery.js"></script>
<script
	src="<%=request.getContextPath()%>/datetimepicker/jquery.datetimepicker.full.js"></script>

<style>
.xdsoft_datetimepicker .xdsoft_datepicker {
	width: 300px; /* width:  300px; */
}

.xdsoft_datetimepicker .xdsoft_timepicker .xdsoft_time_box {
	height: 151px; /* height:  151px; */
}
</style>

<script>
        $.datetimepicker.setLocale('zh');
           // value:   new Date(),
           //disabledDates:        ['2017/06/08','2017/06/09','2017/06/10'], // 去除特定不含
           //startDate:             '2017/07/10',  // 起始日
           //minDate:               '-1970-01-01', // 去除今日(不含)之前
           //maxDate:               '+1970-01-01'  // 去除今日(不含)之後
        $.datetimepicker.setLocale('zh');
        $('#f_date1').datetimepicker({
           theme: '',              //theme: 'dark',
           timepicker:true,       //timepicker:true,
           step: 10,                //step: 60 (這是timepicker的預設間隔60分鐘)
           format:'Y-m-d H:i:s',         //format:'Y-m-d H:i:s',
           value:'<%=groupMeetDate%>', 
           minDate:'<%=groupMeetDate%>',
		});
</script>

<!-- =========================================ckeditor========================================== -->
<script type="text/javascript">
	CKEDITOR.replace('groupContent',{
		height:500,
	    extraPlugins: 'easyimage',
	    cloudServices_tokenUrl: 'https://35969.cke-cs.com/token/dev/Wrw2VWZZ7IMjlbo5A5S5hp2GG3eBRgEkYbFOLMmC5uQgJcCy0hDWuInlSLTT',
	    cloudServices_uploadUrl: 'https://35969.cke-cs.com/easyimage/upload/'
		});
</script>
<!-- =========================================google map========================================== -->
<script type="text/javascript">
	var geocoder;
	var latitude;
	var latval;
	var longitude;
	var lngval;

      function initMap() {
        var options = {types: ['establishment']};
        var input = document.getElementById('groupLocation');
        var autocomplete = new google.maps.places.Autocomplete(input, options);
        geocoder = new google.maps.Geocoder();

//         input.addEventListener('change', function() {
//           geocodeAddress(geocoder);
//         });
      }

      function geocodeAddress(geocoder, callback) {
        var address = document.getElementById('groupLocation').value;
        geocoder.geocode({'address': address}, function(results, status) {
       	  if(results[0]){
              latitude = results[0].geometry.location.lat();
              latval = latitude.toFixed(4);
              longitude = results[0].geometry.location.lng();
              lngval = longitude.toFixed(4);
              document.getElementById("latitude").value = latval;
              document.getElementById("longitude").value = lngval;
              console.log(latitude);
              console.log(latval);
              console.log(longitude);
              console.log(lngval);
              callback();
       	  } else {
       		  alert('Address not found!');
       		  $('#groupLocation').focus();
       	  }
        });
      }
      
      $(function(){
    		$('#formBtn').on('click', function(e){
    			e.preventDefault();
    			geocodeAddress(geocoder, function(){
    				$('#form-updateOneGroup').submit();
    			});
    		});
    	});
    </script>
    <script async defer 
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDx_PNAXvyDXMYfsnlDLaAJrqAw4_HeObM&libraries=places&callback=initMap">
    </script>
</html>