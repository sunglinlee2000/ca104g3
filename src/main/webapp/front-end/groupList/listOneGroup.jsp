<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page import="java.util.*"%>
<%@ page import="com.groupList.model.*"%>
<%@ page import="com.groupReport.model.*"%>
<%@ page import="com.member.model.*"%>
<%@ page import="com.notificationList.model.*"%>

<%

	//	會員
	MemberVO memberVO = (MemberVO) session.getAttribute("memberVO");
	String memberID = memberVO.getMemberID();
	
	//	通知
	NotificationListService noSvc = new NotificationListService();
	List<NotificationListVO> nolists = noSvc.getAllNoList(memberID);
	pageContext.setAttribute("nolists",nolists);

	GroupListVO groupListVO = (GroupListVO) request.getAttribute("groupListVO");
	GroupReportVO groupReportVO = (GroupReportVO) request.getAttribute("groupReportVO");
	Long groupMeetDate = groupListVO.getGroupMeetDate().getTime();
	pageContext.setAttribute("groupMeetDate", groupMeetDate);
	response.setHeader("Cache-Control", "no-store");
	response.setHeader("Pragma", "no-cache");
	response.setDateHeader("Expires", 0);
%>
<jsp:useBean id="memberSvc" scope="page" class="com.member.model.MemberService" />
<jsp:useBean id="friendSvc" scope="page" class="com.friend.model.FriendService" />

<!DOCTYPE html>
<html>
<head>
<title>揪團列表</title>
<meta charset="utf-8">
<meta name="description" content="Food Blog Web Template">
<meta name="keywords" content="food, unica, creative, html">
<meta name="viewport" content="width=device-width, initial-scale=1.0">


	<!-- Favicon -->   
	<link href="img/favicon.ico" rel="shortcut icon"/>

	<!-- Google Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Poppins:400,400i,500,500i,600,600i,700" rel="stylesheet">

	<!-- Stylesheets -->
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/css/bootstrap.min.css"/>
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/css/bootstrap-theme.min.css">
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/css/font-awesome.min.css"/>
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/css/owl.carousel.css"/>
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/css/animate.css"/>
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/css/style.css"/>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="<%=request.getContextPath()%>/front-end/member/css/styleLogin.css">
<!-- 	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"> -->
<!-- 	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"> -->

	<!--====== Javascripts & Jquery ======-->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>	
	<script src="<%=request.getContextPath() %>/front-end/js/owl.carousel.min.js"></script>
	<script src="<%=request.getContextPath() %>/front-end/js/main.js"></script>
	<script src="<%=request.getContextPath()%>/ckeditor/ckeditor.js"></script>	
    <script src="https://code.jquery.com/jquery.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
	
	
<style type="text/css">
.wrongTable-margin {
	margin-top: 15px;
}


.accordion-manipulation {
	width: 33%;
	margin-left:33%;
}

.panel-gray-border {
	border: 1px solid #bfbfbf;
	color: #bfbfbf;
	text-align: center;
}

.panel-gray-border:hover {
	border: 1px solid #404040;
	color: #404040;
}

a {
	text-decoration: none;
}

a:hover {
	text-decoration: none;
}


.panel-body {
	padding: 5px;
}

.btn-gray-border {
	border: 1px solid #bfbfbf;
	background-color:white;
	color: #bfbfbf;
	text-align: center;
}

.btn-gray-border:hover {
	border: 1px solid #404040;
	background-color:white;
	color: #404040;
}

.btn-margin {
	margin: 10px 0px;
}

.table-striped>tbody>tr:nth-of-type(odd) {
	background-color: #eee;
}

.table-striped>tbody>tr:nth-of-type(even) {
	background-color: #91A8D0;
}

.th-center {
	width: 15%;
	text-align: center;
}
.cke_top {
	display: none !important;
}

.modal-title{
    float:left;
}

.kk-no-list{
   height:auto;
   border:1px solid;
   border-color: #E3E0E0;
   background-color: #E3E0E0;
   margin: 2px;
   padding: 2px;
   font-size:15px;
}

.kk-modal-body{
   height:300px;
   overflow-y: scroll;
   overflow-x: hidden;

}

</style>

</head>
<body>
	 <!-- Page Preloder -->
	<div id="preloder">
		<div class="loader"></div>
	</div>

<!-- Header section --> 
 <header class="header-section">	
  <div class="header-bottom"> 
   <div class="container"> 
    <a href="<%=request.getContextPath()%>/front-end/index.jsp" class="site-logo"> 
     <img src="<%=request.getContextPath() %>/front-end/img/logo-1.png" alt=""> 
    </a> 
    <div class="nav-switch"> 
     <i class="fa fa-bars"></i> 
    </div> 

    <ul class="main-menu"> 
     <li><a href="<%= request.getContextPath()%>/front-end/board/listAllBoard.jsp"><img alt="藝人專板"  title="藝人專板" src="<%= request.getContextPath()%>/front-end/res/img/index/board.png" style="height:30px;width:30px"></a></li> 
     <li><a href="<%= request.getContextPath()%>/front-end/product/Mall.jsp"><img alt="商城"  title="商城" src="<%= request.getContextPath()%>/front-end/res/img/index/mall.png" style="height:30px;width:30px"></a></li> 
     <li><a href="<%=request.getContextPath()%>/front-end/gif_collection/gif_list.jsp"><img alt="GIF"  title="GIF" src="<%= request.getContextPath()%>/front-end/res/img/index/gif.png" style="height:30px;width:30px"></a></li> 
     <li><a href="<%=request.getContextPath()%>/front-end/member/listOneMember.jsp"><img alt="會員資料" title="會員資料" src="<%= request.getContextPath()%>/front-end/res/img/index/user.png" style="height:30px;width:30px"></a></li> 
     <li><a href="#" onclick="logout()"><img alt="登出" title="登出" src="<%= request.getContextPath()%>/front-end/res/img/index/logout.png" style="height:30px;width:30px"></a> 
     <li><a href="<%=request.getContextPath()%>/front-end/chatBox/chatBox.jsp"><img alt="聊天室" title="聊天室" src="<%= request.getContextPath()%>/front-end/res/img/index/chat.png" style="height:30px;width:30px"></a></li> 
     <li>${memberVO.memberNickName }</li>
     <li><a href="<%=request.getContextPath()%>/front-end/personalPage/myPersonalPage.jsp"><img src="<%= request.getContextPath()%>/front-end/member/memberImg.do?memberID=${memberVO.memberID}" id="memberPhoto"></a></li>	
     <li id="notifi"><a href="#modal-id-notifi" data-toggle="modal"> 
      <div style="position: relative;"> 
      <img src="<%=request.getContextPath()%>/front-end/res/img/index/notification(32).png" alt="通知" id="notifiPhoto"> 
      <span style="position: absolute; top: 0; left: 17px; color: white" id="notifiCount"></span> 
      </div> 
     </a></li> 
     </ul>
   </div> 
  </div> 
 </header> 
 <!-- Header section end -->
<FORM METHOD="post" ACTION="<%=request.getContextPath()%>/front-end/member/member.do" id="logout">
	<input type="hidden" name="action"	value="logOut">
</FORM>
<script type="text/javascript">
	function logout(){
		document.getElementById("logout").submit();
	}
</script>
	
	<!-- =========================================error message========================================= -->
	<div class="container-fluid container-padding">
		<div class="row">
			<div class="col-xs-12 col-sm-6 col-sm-offset-3">
				<c:if test="${not empty errorMsgs}">
					<table class="table table-condensed wrongTable-margin">
						<thead>
							<tr>
								<th><font style="color: red">請修正以下錯誤：</font></th>
							</tr>
						</thead>

						<tbody>

							<c:forEach var="message" items="${errorMsgs}">
								<tr>
									<td><font style="color: red">${message}</font></td>
								</tr>
							</c:forEach>

						</tbody>
					</table>
				</c:if>
			</div>
		</div>
	</div>


	<div class="container-fiuld aa">
		<div class="row">
			<!-- =========================================收合========================================= -->
			<div class="col-xs-12 col-sm-3">
				<div class="panel-group accordion-manipulation" id="accordion2"
					role="tablist" aria-multiselectable="true">
					<!-- =========================================好友邀請=========================================-->
					<div class="panel panel-gray-border">
						<div class="panel-heading" role="tab" id="panel1">
							<h4 class="panel-title">
								<a href="#aaa" data-parent="#accordion2" data-toggle="collapse"
									role="button" aria-expanded="false" aria-controls="aaa"
									class="collapsed">邀請好友
								</a>
							</h4>
						</div>
						<div id="aaa" class="panel-collapse collapse" role="tabpanel"
							aria-labelledby="panel1">
							<div class="panel-body">

								<div>
									<form method="post"
										action="<%=request.getContextPath()%>/groupList/groupList.do">
										<c:forEach var="friendVO"
											items="${friendSvc.getFriendsByMemberID(memberVO.memberID)}">
											<div class="checkbox">
												<label> <input type="checkbox" name="memberID"
													value="${memberSvc.getOneMember(friendVO.friendID).memberID}">
													${memberSvc.getOneMember(friendVO.friendID).memberNickName}
												</label>
											</div>
										</c:forEach>
										<center>
											<input type="submit" value="確認邀請" class="btn btn-gray-border btn-margin">
										</center>
										<input type="hidden" name="groupID" value="<%=groupListVO.getGroupID()%>">
										<input type="hidden" name="boardID" value="<%=groupListVO.getBoardID()%>">
										<input type="hidden" name="whichPage" value="<%=request.getParameter("whichPage")%>">
										<input type="hidden" name="action" value="invite_friend">
									</form>
								</div>

							</div>
						</div>
					</div>
					<!-- =========================================檢舉========================================= -->
					<div class="panel panel-gray-border">
						<div class="panel-heading" role="tab" id="panel2">
							<h4 class="panel-title">
								<a href="#bbb" data-parent="#accordion2" data-toggle="collapse"
									role="button" class="collapsed" aria-expanded="false"
									aria-controls="bbb">檢舉
								</a>
							</h4>
						</div>
						<div id="bbb" class="panel-collapse collapse" role="tabpanel"
							aria-labelledby="panel2">
							<div class="panel-body">
								<form method="post"
									action="<%=request.getContextPath()%>/groupReport/groupReport.do"
									id="form-groupReport">
									<div class="radio">
										<label><input type="radio" name="groupReportReason"
											value="BOARD_WRONG">與本專版無關</label>
									</div>

									<div class="radio">
										<label><input type="radio" name="groupReportReason"
											value="ANTI">不正當揪團內容</label>
									</div>

									<div class="radio">
										<label><input type="radio" name="groupReportReason"
											value="OTHER" checked>其他</label>
									</div>

									<div class="input-group group-input">
										<textarea form="form-groupReport" class="form-control"
											name="groupReportContent" rows="10" cols="10" wrap="soft">
									<%=(groupReportVO == null) ? "" : groupReportVO.getGroupReportContent()%>
		                        </textarea>
									</div>

									<center>
										<input type="submit" value="檢舉送出"
											class="btn btn-gray-border btn-margin" id="group-report">
									</center>
									<input type="hidden" name="groupID" value="<%=groupListVO.getGroupID()%>">
									<input type="hidden" name="memberID" value="${memberVO.memberID}"> 
									<input type="hidden" name="whichPage" value="<%=request.getParameter("whichPage")%>">
									<input type="hidden" name="boardID" value="<%=groupListVO.getBoardID()%>">
									<input type="hidden" name="action" value="group_report">

								</form>
							</div>
						</div>
					</div>
					<!-- =========================================返回列表=========================================-->
					<div class="panel panel-gray-border">
						<div class="panel-heading" role="tab" id="panel3">
							<h4 class="panel-title">
								<a href="#" data-parent="#accordion2" data-toggle="collapse"
									role="button" aria-expanded="false" aria-controls="ccc"
									class="collapsed">回揪團列表
								</a>
							</h4>
						</div>
						
					</div>
					<!-- =========================================返回列表end=========================================-->
				</div>
			</div>


			<!-- =========================================揪團內容本文區塊========================================= -->
			<div class="col-xs-12 col-sm-6">
				<table class="table table-striped table-bordered">
					<tbody>
						<tr>
							<th class="th-center">揪團名稱</th>
							<td><%=groupListVO.getGroupTitle()%></td>
						</tr>
						<tr>
							<th class="th-center">主揪</th>
							<td><a href="<%=request.getContextPath()%>/personalPage/personalPage.do?memberID=${groupListVO.memberID}&action=toOnePersonalPage">
							<img src="<%= request.getContextPath()%>/front-end/member/memberImg.do?memberID=${groupListVO.memberID}" 
							id="memberPhoto"></a><a href="<%=request.getContextPath()%>/personalPage/personalPage.do
							?memberID=${groupListVO.memberID}&action=toOnePersonalPage">${memberSvc.getOneMember(groupListVO.memberID).memberNickName}</a></td>
						</tr>
						<tr>
							<th class="th-center">集合時間</th>
							<td><fmt:formatDate value="${groupListVO.groupMeetDate}"
									pattern="yyyy-MM-dd HH:mm" /></td>
						</tr>
						<tr>
							<th class="th-center">集合地點</th>
							<td><%=groupListVO.getGroupLocation()%></td>
						</tr>
						<tr>
							<th class="th-center">揪團內容</th>
							<td><%=groupListVO.getGroupContent()%></td>
						</tr>
					</tbody>
				</table>
				<iframe
				  width="100%"
				  height="450"
				  frameborder="0" style="border:0"
				  src="https://www.google.com/maps/embed/v1/place?key=AIzaSyDx_PNAXvyDXMYfsnlDLaAJrqAw4_HeObM
				    &q=${groupListVO.groupLocation}" allowfullscreen>
				</iframe>


				
				<center>
					<!-- =========================================修改=========================================-->
					<form method="post" action="<%=request.getContextPath()%>/groupList/groupList.do">
						<input type="submit" value="修改" class="btn btn-gray-border btn-margin" id="edit"> 
						<input type="hidden" name="groupID" value="${groupListVO.groupID}">
						<input type="hidden" name="whichPage" value="<%=request.getParameter("whichPage")%>">
						<input type="hidden" name="boardID" value="${groupListVO.boardID}"> 
						<input type="hidden" name="action" value="getOne_For_Update">
					</form>
					<!-- =========================================加入揪團=========================================-->
					<form method="post" action="<%=request.getContextPath()%>/groupMember/groupMember.do" 
						id="form-groupMemberApplication">
						<input type="submit" value="加入揪團" class="btn btn-gray-border btn-margin" id="participate"> 
						<input type="hidden" name="groupID" value="${groupListVO.groupID}">
						<input type="hidden" name="memberID" value="${memberVO.memberID}">
						<input type="hidden" name="whichPage" value="<%=request.getParameter("whichPage")%>">
						<input type="hidden" name="boardID" value="${groupListVO.boardID}"> 
						<input type="hidden" name="action" value="groupMember_Application">
					</form>
				</center>

			</div>
		</div>
	</div>



	<!-- =========================================footer=========================================-->
<!-- 	<div class="footer"> -->
<!-- 		<div class="row"> -->
<!-- 			<div class="col-xs-12 col-sm-12 footer-text"> -->
<!-- 				<h6>電話： 03-4257387</h6> -->
<!-- 				<h6>地址： 320桃園市中壢區中大路300號</h6> -->
<!-- 			</div> -->
<!-- 		</div> -->
<!-- 	</div> -->
<!-- </body> -->

	<!-- 通知列表的跳窗 -->
		<div class="modal fade" id="modal-id-notifi">
			<div class="modal-dialog modal-sm">
			   <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<div class="modal-content">
				   
					<div class="modal-header">					
						<h4 class="modal-title">通知列表</h4>
					</div>
					<div class="modal-body kk-modal-body" >

                    <!-- 及時通知顯示的地方 -->
					<div id="nolistsssss" class="kk-no-list">
					  <a href="<%=request.getContextPath()%>/adfundTest/adfund.do?action=getOne_For_Display&adfundID=${adfundVO.adfundID}" ></a>
					 </div>
                        
						<c:forEach var="NotificationListVO" items="${nolists}" >
					      <div class="kk-no-list">					      
					         <a href="<%=request.getContextPath()%>${NotificationListVO.notificationpath}${NotificationListVO.notificationtarget}" id="${NotificationListVO.notificationid}">${NotificationListVO.notificationcontent}</a>
					         <h6><fmt:formatDate value="${NotificationListVO.notificationdate}" pattern="yyyy-MM-dd HH:mm"/></h6>
					      </div>
					    </c:forEach>
					</div>
					<div class="modal-footer">

					</div>
				</div>
			</div>
		</div>
	<!-- 通知的跳窗 -->

<script>

$(document).ready(function() { 
	
	//點擊鈴鐺時，通知要全部變為已讀
    $('#notifi').click(function(){
    	
//              alert("!!")
				$.ajax({ 
				     url : '<%=request.getContextPath()%>/front-end/NotificationListServlet/NotificationListServlet.do', 
				     type : 'Post', 
				     data : { 
				      action : 'isClick', 
				      memberID : '${memberVO.memberID}' 

				     } 
				    }).done(function(res){
				    	var obj = JSON.parse(res);
				    	document.getElementById("notifiPhoto").src = "<%=request.getContextPath()%>/front-end/res/img/index/notification(32).png";
				    	$('#notifiCount').html("");
	                });				     
     });
 
	
//    alert("noti!!");
    //網頁一打開時，要立刻判斷是否有未讀訊息，看是否要顯示有消息的鈴鐺
    $.ajax({ 
	     url : '<%=request.getContextPath()%>/front-end/NotificationListServlet/NotificationListServlet.do', 
	     type : 'Post', 
	     data : { 
	      action : 'isRead',  
	      memberID : '${memberVO.memberID}'
	     } 
	    }).done(function(res){
	    	var obj = JSON.parse(res);
	    	if(obj.notiStatus==false){
	    	   document.getElementById("notifiPhoto").src = "<%=request.getContextPath()%>/front-end/res/img/index/notification(32)red.png";
	    	   $('#notifiCount').html(obj.falseCount);
	    	}
	    });
    
});
</script>

<!-- =========================================ckeditor========================================= -->
<script type="text/javascript">
	CKEDITOR.replace('groupReportContent',{
		toolbarCanCollapse : true,
		height:150,
	    extraPlugins: 'easyimage',
	    cloudServices_tokenUrl: 'https://35969.cke-cs.com/token/dev/Wrw2VWZZ7IMjlbo5A5S5hp2GG3eBRgEkYbFOLMmC5uQgJcCy0hDWuInlSLTT',
	    cloudServices_uploadUrl: 'https://35969.cke-cs.com/easyimage/upload/'
		});
</script>
<script type="text/javascript">

	var sessionMemberID = '${memberVO.memberID}';
	var groupMemberID = '${groupListVO.memberID}';
	var groupID = '${groupListVO.groupID}';
	// =========================================加入/修改按鈕控制=========================================
	$(document).ready(function(e) {
		console.log(sessionMemberID);
		console.log(groupMemberID);
		if(sessionMemberID == groupMemberID) {
			$('#participate').hide();
		} else {
			$('#edit').hide();
		}
	});
	
	// =========================================返回揪團列表=========================================
	
	$('#panel3').click(function(e) {
		var href = '<%=request.getContextPath()%>/front-end/groupList/listAllGroup.jsp?boardID=${groupListVO.boardID}';
		window.location.href = href;
	});
	
	// =========================================不能檢舉自己、重複檢舉=========================================
	
	$('#group-report').click(function(e) {
		e.preventDefault();
		
		if(sessionMemberID == groupMemberID) {
			alert('您不能檢舉自己的揪團');
			return;
		} 
		
		$.ajax({
			type: "POST",
			url: "<%=request.getContextPath()%>/groupReport/groupReport.do",
			data: {
				action: 'doubleReport',
				memberID: sessionMemberID,
				groupID: groupID
			}
		}).done(function(res) {
			console.log(res);
			var obj = JSON.parse(res);
			console.log(obj);	
			if(obj.doubleReport == 'doubleReport') {
				alert('您提交的檢舉尚在審核中，請勿重複檢舉');
				return;
			} else {
				$('#form-groupReport').submit();
			}			
		});
	});
	
	// =========================================加入揪團=========================================
	$('#participate').click(function(e) {
		e.preventDefault();
		
		var groupMeetDate = ${groupMeetDate};
		var now = new Date().getTime();
		console.log(groupMeetDate);
		console.log(now);
		
		if(groupMeetDate < now) {
			alert('已過集合時間囉！');
		} else {
			alert('成功加入此揪團');
			$('#form-groupMemberApplication').submit();
		}
	});
	
</script>
</body>
</html>
