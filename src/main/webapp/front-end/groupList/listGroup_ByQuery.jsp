<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page import="java.util.*"%>
<%@ page import="com.groupList.model.*"%>
<%@ page import="com.notificationList.model.*"%>
<%@ page import="com.member.model.*"%>

<%

	//	會員
	MemberVO memberVO = (MemberVO) session.getAttribute("memberVO");
	String memberID = memberVO.getMemberID();
	
	//	通知
	NotificationListService noSvc = new NotificationListService();
	List<NotificationListVO> nolists = noSvc.getAllNoList(memberID);
	pageContext.setAttribute("nolists",nolists);

	String boardID = request.getParameter("boardID");

	response.setHeader("Cache-Control", "no-store");
	response.setHeader("Pragma", "no-cache");
	response.setDateHeader("Expires", 0);
%>
<jsp:useBean id="listGroup_ByQuery" scope="request" type="java.util.List<GroupListVO>"/>

<!DOCTYPE html>
<html>
<head>
<title>揪團列表</title>
<meta charset="utf-8">
<meta name="description" content="Food Blog Web Template">
<meta name="keywords" content="food, unica, creative, html">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<!-- Favicon -->   
	<link href="img/favicon.ico" rel="shortcut icon"/>

	<!-- Google Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Poppins:400,400i,500,500i,600,600i,700" rel="stylesheet">

	<!-- Stylesheets -->
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/css/bootstrap.min.css"/>
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/css/bootstrap-theme.min.css">
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/css/font-awesome.min.css"/>
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/css/owl.carousel.css"/>
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/css/animate.css"/>
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/css/style.css"/>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="<%=request.getContextPath()%>/front-end/member/css/styleLogin.css">
<!-- 	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"> -->
<!-- 	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"> -->

	<!--====== Javascripts & Jquery ======-->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>	
	<script src="<%=request.getContextPath() %>/front-end/js/owl.carousel.min.js"></script>
	<script src="<%=request.getContextPath() %>/front-end/js/main.js"></script>	
    <script src="https://code.jquery.com/jquery.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>

<style type="text/css">

.container-padding {
	padding-bottom: 15px;
}

.btn-margin , .input-margin {
	margin-bottom: 10px;
}

.accordion-manipulation {
	width: 50%;
}

.panel-gray-border {
	border: 1px solid #bfbfbf;
	color: #bfbfbf;
	text-align: center;
}

.panel-gray-border:hover {
	border: 1px solid #404040;
	color: #404040;
}

.group {
	background-image: url('<%=request.getContextPath()%>/front-end/groupList/image/background_02.jpg');
	color: #f7cac9;
	height: 100%;
	margin-bottom: 15px;
	padding: 20px 10px;
	border-radius: 15px;
	
}
.group:hover {
	box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2), 0 6px 20px 0 rgba(0,0,0,0.19);
}

.group-float {
	float: right;
}

a {
	text-decoration: none;
}

a:hover {
	text-decoration: none;
}

.a-color {
	color: #6666ff;
}

.a-color:hover {
	color: #ff0000;
}

#page-index {
	color: #fff;
}
.wrongTable-margin {
	margin-top: 15px;
}

.modal-title{
    float:left;
}

.kk-no-list{
   height:auto;
   border:1px solid;
   border-color: #E3E0E0;
   background-color: #E3E0E0;
   margin: 2px;
   padding: 2px;
   font-size:15px;
}

.kk-modal-body{
   height:300px;
   overflow-y: scroll;
   overflow-x: hidden;.kk-no-list{
   height:auto;
   border:1px solid;
   border-color: #E3E0E0;
   background-color: #E3E0E0;
   margin: 2px;
   padding: 2px;
   font-size:15px;
}

.kk-modal-body{
   height:300px;
   overflow-y: scroll;
   overflow-x: hidden;

}
   
</style>


</head>
<body>
	 <!-- Page Preloder -->
	<div id="preloder">
		<div class="loader"></div>
	</div>

<!-- Header section --> 
 <header class="header-section">	
  <div class="header-bottom"> 
   <div class="container"> 
    <a href="<%=request.getContextPath()%>/front-end/index.jsp" class="site-logo"> 
     <img src="<%=request.getContextPath() %>/front-end/img/logo-1.png" alt=""> 
    </a> 
    <div class="nav-switch"> 
     <i class="fa fa-bars"></i> 
    </div> 

    <ul class="main-menu"> 
     <li><a href="<%= request.getContextPath()%>/front-end/board/listAllBoard.jsp"><img alt="藝人專板"  title="藝人專板" src="<%= request.getContextPath()%>/front-end/res/img/index/board.png" style="height:30px;width:30px"></a></li> 
     <li><a href="<%= request.getContextPath()%>/front-end/product/Mall.jsp"><img alt="商城"  title="商城" src="<%= request.getContextPath()%>/front-end/res/img/index/mall.png" style="height:30px;width:30px"></a></li> 
     <li><a href="<%=request.getContextPath()%>/front-end/gif_collection/gif_list.jsp"><img alt="GIF"  title="GIF" src="<%= request.getContextPath()%>/front-end/res/img/index/gif.png" style="height:30px;width:30px"></a></li> 
     <li><a href="<%=request.getContextPath()%>/front-end/member/listOneMember.jsp"><img alt="會員資料" title="會員資料" src="<%= request.getContextPath()%>/front-end/res/img/index/user.png" style="height:30px;width:30px"></a></li> 
     <li><a href="#" onclick="logout()"><img alt="登出" title="登出" src="<%= request.getContextPath()%>/front-end/res/img/index/logout.png" style="height:30px;width:30px"></a> 
     <li><a href="<%=request.getContextPath()%>/front-end/chatBox/chatBox.jsp"><img alt="聊天室" title="聊天室" src="<%= request.getContextPath()%>/front-end/res/img/index/chat.png" style="height:30px;width:30px"></a></li> 
     <li>${memberVO.memberNickName }</li>
     <li><a href="<%=request.getContextPath()%>/front-end/personalPage/myPersonalPage.jsp"><img src="<%= request.getContextPath()%>/front-end/member/memberImg.do?memberID=${memberVO.memberID}" id="memberPhoto"></a></li>	
     <li id="notifi"><a href="#modal-id-notifi" data-toggle="modal"> 
      <div style="position: relative;"> 
      <img src="<%=request.getContextPath()%>/front-end/res/img/index/notification(32).png" alt="通知" id="notifiPhoto"> 
      <span style="position: absolute; top: 0; left: 17px; color: white" id="notifiCount"></span> 
      </div> 
     </a></li> 
     </ul>
   </div> 
  </div> 
 </header> 
 <!-- Header section end -->
<FORM METHOD="post" ACTION="<%=request.getContextPath()%>/front-end/member/member.do" id="logout">
	<input type="hidden" name="action"	value="logOut">
</FORM>
<script type="text/javascript">
	function logout(){
		document.getElementById("logout").submit();
	}
</script>
	
	<div class="container-fiuld container-padding">
		<div class="row">
			<div class="col-xs-12 col-sm-2 col-sm-offset-1 btn-margin">
				<div class="panel-group accordion-manipulation" id="accordion2" role="tablist" aria-multiselectable="true">
				  <!-- =========================================addGroup========================================= -->
				  <div class="panel panel-gray-border">
				    <div class="panel-heading" role="tab" id="panel1">
				      <h4 class="panel-title">
				        <a href="#" data-parent="#accordion2" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="aaa">
				          <span>新增揪團</span>
				        </a>
				      </h4>
				    </div>
				    <script type="text/javascript">
						$('#panel1').click(function(e) {
							var href = '<%=request.getContextPath()%>/front-end/groupList/addGroup.jsp?boardID=<%=request.getParameter("boardID")%>';
							window.location.href = href;
						});
					</script>
				  </div>
				 <!-- =========================================返回專板列表========================================= -->
				  <div class="panel panel-gray-border">
				    <div class="panel-heading" role="tab" id="panel2">
				      <h4 class="panel-title">
				        <a href="#" data-parent="#accordion2" data-toggle="collapse" role="button" class="collapsed" aria-expanded="false" aria-controls="bbb">
				           回專板列表
				        </a>
				      </h4>
				    </div>
				    <script type="text/javascript">
						$('#panel2').click(function(e) {
							var href = '<%=request.getContextPath()%>/front-end/board/board.do?boardID=<%=request.getParameter("boardID")%>&action=getOne_For_Display';
							window.location.href = href;
						});
				    </script>
				  </div>
				  <!-- =========================================返回專板列表end========================================= -->
				</div>
			</div>
			
			<div class="col-xs-12 col-sm-6">
				<!-- =========================================關鍵字查詢========================================= -->	
				<form method="post" action="<%=request.getContextPath()%>/groupList/groupList.do">
					<div class="input-group input-margin">
						<input class="form-control" type="text" name="groupTitle" placeholder="請輸入關鍵字"
						value="">
						<div class="input-group-btn">
							<input type="hidden" name="boardID" value="${listGroup_ByQuery[0].boardID}">
							<input type="submit" value="送出查詢" class="btn bnt-info">
						</div>
					</div>
					<input type="hidden" name="action" value="group_byQuery">
				</form>	
				<!-- =========================================groupList========================================= -->
				<%@ include file="/front-end/groupList/pages/page1_ByQuery.file"%>
				<c:forEach var="groupListVO" items="${listGroup_ByQuery}" begin="<%=pageIndex%>"
					end="<%=pageIndex+rowsPerPage-1%>">
						<div class="group">
							<a class="a-color" href="<%=request.getContextPath()%>/groupList/groupList.do?groupID=${groupListVO.groupID}
							&boardID=${groupListVO.boardID}&whichPage=<%=whichPage%>&action=getOne_For_Display">
								<span>${groupListVO.groupTitle}</span>
							</a>
							<span class="group-float"><fmt:formatDate value="${groupListVO.groupStartDate}" pattern="yyyy-MM-dd"/></span>
						</div>
				</c:forEach>
			</div>
		</div>
	</div>

	<div class="container-fiuld container-padding">
		<div class="row">
			<div class="col-xs-12 col-sm-12">
				<center>
					<%@ include file="/front-end/groupList/pages/page2_ByQuery.file"%>
				</center>
			</div>
		</div>
	</div>



	<!--=========================================footer=========================================-->
<!-- 	<div class="footer"> -->
<!-- 		<div class="row"> -->
<!-- 			<div class="col-xs-12 col-sm-12 footer-text"> -->
<!-- 				<h6>電話： 03-4257387</h6> -->
<!-- 				<h6>地址： 320桃園市中壢區中大路300號</h6> -->
<!-- 			</div> -->
<!-- 		</div> -->
<!-- 	</div> -->

		<!-- 通知列表的跳窗 -->
		<div class="modal fade" id="modal-id-notifi">
			<div class="modal-dialog modal-sm">
			   <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<div class="modal-content">
				   
					<div class="modal-header">					
						<h4 class="modal-title">通知列表</h4>
					</div>
					<div class="modal-body kk-modal-body" >

                    <!-- 及時通知顯示的地方 -->
					<div id="nolistsssss" class="kk-no-list">
					  <a href="<%=request.getContextPath()%>/adfundTest/adfund.do?action=getOne_For_Display&adfundID=${adfundVO.adfundID}" ></a>
					 </div>
                        
						<c:forEach var="NotificationListVO" items="${nolists}" >
					      <div class="kk-no-list">					      
					         <a href="<%=request.getContextPath()%>${NotificationListVO.notificationpath}${NotificationListVO.notificationtarget}" id="${NotificationListVO.notificationid}">${NotificationListVO.notificationcontent}</a>
					         <h6><fmt:formatDate value="${NotificationListVO.notificationdate}" pattern="yyyy-MM-dd HH:mm"/></h6>
					      </div>
					    </c:forEach>
					</div>
					<div class="modal-footer">

					</div>
				</div>
			</div>
		</div>
	<!-- 通知的跳窗 -->

<script>

$(document).ready(function() { 
	
	//點擊鈴鐺時，通知要全部變為已讀
    $('#notifi').click(function(){
    	
//              alert("!!")
				$.ajax({ 
				     url : '<%=request.getContextPath()%>/front-end/NotificationListServlet/NotificationListServlet.do', 
				     type : 'Post', 
				     data : { 
				      action : 'isClick', 
				      memberID : '${memberVO.memberID}' 

				     } 
				    }).done(function(res){
				    	var obj = JSON.parse(res);
				    	document.getElementById("notifiPhoto").src = "<%=request.getContextPath()%>/front-end/res/img/index/notification(32).png";
				    	$('#notifiCount').html("");
	                });				     
     });
 
	
//    alert("noti!!");
    //網頁一打開時，要立刻判斷是否有未讀訊息，看是否要顯示有消息的鈴鐺
    $.ajax({ 
	     url : '<%=request.getContextPath()%>/front-end/NotificationListServlet/NotificationListServlet.do', 
	     type : 'Post', 
	     data : { 
	      action : 'isRead',  
	      memberID : '${memberVO.memberID}'
	     } 
	    }).done(function(res){
	    	var obj = JSON.parse(res);
	    	if(obj.notiStatus==false){
	    	   document.getElementById("notifiPhoto").src = "<%=request.getContextPath()%>/front-end/res/img/index/notification(32)red.png";
	    	   $('#notifiCount').html(obj.falseCount);
	    	}
	    });
    
});
</script>

</body>
</html>