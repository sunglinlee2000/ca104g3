<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page import="java.util.*"%>
<%@ page import="com.issueReport.model.*"%>
<%@ page import="com.issue.model.*"%>
<%-- 此頁練習採用 EL 的寫法取值 --%>

<%
  response.setHeader("Cache-Control","no-store"); //HTTP 1.1
  response.setHeader("Pragma","no-cache");        //HTTP 1.0
  response.setDateHeader ("Expires", 0);
%>

<%
    IssueReportService issueReportSvc = new IssueReportService();
    List<IssueReportVO> listAllIssueReport = issueReportSvc.getAll();
    pageContext.setAttribute("listAllIssueReport",listAllIssueReport);
%>



<html>
<head>
<title>所有檢舉 - listAllEmp.jsp</title>

<style>
  table#table-1 {
	background-color: #CCCCFF;
    border: 2px solid black;
    text-align: center;
  }
  table#table-1 h4 {
    color: red;
    display: block;
    margin-bottom: 1px;
  }
  h4 {
    color: blue;
    display: inline;
  }
</style>

<style>
  table {
	width: 800px;
	background-color: white;
	margin-top: 5px;
	margin-bottom: 5px;
  }
  table, th, td {
    border: 1px solid #CCCCFF;
  }
  th, td {
    padding: 5px;
    text-align: center;
  }
</style>

</head>
<body bgcolor='white'>



<%-- 錯誤表列 --%>
<c:if test="${not empty errorMsgs}">
	<font style="color:red">請修正以下錯誤:</font>
	<ul>
		<c:forEach var="message" items="${errorMsgs}">
			<li style="color:red">${message}</li>
		</c:forEach>
	</ul>
</c:if>

<table>
	<tr>
		<th>檢舉編號</th>
		<th>話題編號</th>
		<th>會員編號</th>
		<th>管理員編號</th>
		<th>檢舉內容</th>
		<th>檢舉時間</th>
		<th>檢舉狀態</th>
		<th>檢舉事由</th>
		
		<th>通過</th>
		<th>不通過</th>
	</tr>
 
	<c:forEach var="issueReportVO" items="${listAllIssueReport}">
		
		<tr>
			<td>${issueReportVO.issueReportID}</td>
			<td><a href="<%=request.getContextPath()%>/issue/issue.do?issueID=${issueReportVO.issueID}&action=getOne_For_Display">${issueReportVO.issueID}</a></td>
			<td>${issueReportVO.memberID}</td>
			<td>${issueReportVO.administratorID}</td>
			<td>${issueReportVO.issueReportContent}</td>
<%-- 			<td><fmt:formatDate value="${now}" pattern="yyyy-MM-dd HH:mm:ss"/></td> --%>
			<td>${issueReportVO.issueReportDate}</td>
			<td>${issueReportVO.issueReportStatus}</td>
			<td>${issueReportVO.issueReportReason}</td>
			
			<td>
			  <FORM METHOD="post" ACTION="<%=request.getContextPath()%>/issueReport/issueReport.do" style="margin-bottom: 0px;">
			     <input type="submit" value="通過">
			     
			     <input type="hidden" name="issueReportID"  value="${issueReportVO.issueReportID}">
			     <input type="hidden" name="action" value="delete"></FORM>
			</td>
			
			<td>
			  <FORM METHOD="post" ACTION="<%=request.getContextPath()%>/issueReport/issueReport.do" style="margin-bottom: 0px;">
			     <input type="submit" value="不通過">
			     <input type="hidden" name="issueReportID"  value="${issueReportVO.issueReportID}">
			     <input type="hidden" name="action" value="delete"></FORM>
			</td>
		</tr>
	</c:forEach>
</table>


</body>
</html>