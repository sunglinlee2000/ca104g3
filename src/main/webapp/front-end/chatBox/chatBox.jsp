<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page import="java.util.*"%>
<%@ page import="com.chatMember.model.*"%>
<%@ page import="com.member.model.*"%>
<%@ page import="com.gif.model.*"%>
<%@ page import="com.notificationList.model.*"%>

<%
	//	會員自己的memberID
	MemberVO memberVO = (MemberVO) session.getAttribute("memberVO");
	String memberID = memberVO.getMemberID();
	
	//	通知
	NotificationListService noSvc = new NotificationListService();
	List<NotificationListVO> nolists = noSvc.getAllNoList(memberID);
	pageContext.setAttribute("nolists",nolists);
	
// 	取得會員擁有的聊天室
	ChatMemberService chatMemberSvc = new ChatMemberService();
	List<ChatMemberVO> list = chatMemberSvc.getAllByMemberID(memberID);
	pageContext.setAttribute("list", list);
	
// 	gif列表
	GifService gifSvc = new GifService();
	List<GifVO> listGif = gifSvc.getAllGif_inUse(memberID, "GIF_IN_USE");
	pageContext.setAttribute("listGif", listGif);
	
	response.setHeader("Cache-Control", "no-store");
	response.setHeader("Pragma", "no-cache");
	response.setDateHeader("Expires", 0);
%>

<jsp:useBean id="chatBoxSvc" scope="page" class="com.chatBox.model.ChatBoxService" />

<!DOCTYPE html>
<html>
<head>
<title>聊天室</title>
<meta charset="utf-8">
<meta name="description" content="Food Blog Web Template">
<meta name="keywords" content="food, unica, creative, html">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1">

	<!-- Favicon -->   
	<link href="img/favicon.ico" rel="shortcut icon"/>

	<!-- Google Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Poppins:400,400i,500,500i,600,600i,700" rel="stylesheet">

	<!-- Stylesheets -->
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/css/bootstrap.min.css"/>
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/css/bootstrap-theme.min.css">
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/css/font-awesome.min.css"/>
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/css/owl.carousel.css"/>
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/css/animate.css"/>
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/css/style.css"/>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="<%=request.getContextPath()%>/front-end/member/css/styleLogin.css">
<!-- 	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"> -->
<!-- 	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"> -->

	<!--====== Javascripts & Jquery ======-->
	<script src="<%=request.getContextPath() %>/front-end/js/jquery-3.3.1.min.js"></script>
	<script src="<%=request.getContextPath()%>/front-end/js/bootstrap.min.js"></script>
	<script src="<%=request.getContextPath() %>/front-end/js/owl.carousel.min.js"></script>
	<script src="<%=request.getContextPath() %>/front-end/js/main.js"></script>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>		
    <script src="https://code.jquery.com/jquery.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>

<style type="text/css">
	.friend-column {
		height: 100%;
		overflow: auto;
		margin: 6px;
		padding: 6px;
	}

	.chatBox {
		height: 50px;
		text-align: center;
		padding-top: 10px;
	}

	.chatBox:hover {
		background-color: rgba(200,200,200,0.2);
		cursor: pointer;
	}
	
	.message-column {
		border: 2px solid #808080;
	}
	
	.message-area {		
	    height: 70vh;
	    background-color: #f5f5f5;
	    box-sizing: border-box;    
	    overflow: auto;
	    margin: 6px;
		padding: 6px;
	}
	
	.input-group-margin {
		margin: 20px 0px;	
	}
	ul {
		list-style-type: none;
	}
	
	.float-left, .float-right {
		word-wrap:break-word;
		max-width:45%;
		margin: 6px 15px;
		clear: both;
	}
	
	.float-right {
		float: right;
	}
	
	.span-blue, .span-gray {
		border-radius: 15px;
		padding: 2px 4px;
	}
	
	.span-blue {
		background-color: #0084ff;
		color: #fff;
	}
	
	.span-gray {
		background-color: rgba(200,200,200,0.2);
	}
	
	.gif-column {
		overflow: auto;
	}
	
	.gif-column:hover {
		cursor: pointer;
	}
	
	.a-gif {
		margin-left:15%;
		margin-bottom: 10px;
	}
	
	.modal-title{
	    float:left;
	}
	
	.kk-no-list{
	   height:auto;
	   border:1px solid;
	   border-color: #E3E0E0;
	   background-color: #E3E0E0;
	   margin: 2px;
	   padding: 2px;
	   font-size:15px;
	}
	
	.kk-modal-body{
	   height:300px;
	   overflow-y: scroll;
	   overflow-x: hidden;
	
	}
</style>
</head>
<body onunload="disconnect();">
	 <!-- Page Preloder -->
	<div id="preloder">
		<div class="loader"></div>
	</div>

<!-- Header section --> 
 <header class="header-section">	
  <div class="header-bottom"> 
   <div class="container"> 
    <a href="<%=request.getContextPath()%>/front-end/index.jsp" class="site-logo"> 
     <img src="<%=request.getContextPath() %>/front-end/img/logo-1.png" alt=""> 
    </a> 
    <div class="nav-switch"> 
     <i class="fa fa-bars"></i> 
    </div> 

    <ul class="main-menu"> 
     <li><a href="<%= request.getContextPath()%>/front-end/board/listAllBoard.jsp"><img alt="藝人專板"  title="藝人專板" src="<%= request.getContextPath()%>/front-end/res/img/index/board.png" style="height:30px;width:30px"></a></li> 
     <li><a href="<%= request.getContextPath()%>/front-end/product/Mall.jsp"><img alt="商城"  title="商城" src="<%= request.getContextPath()%>/front-end/res/img/index/mall.png" style="height:30px;width:30px"></a></li> 
     <li><a href="<%=request.getContextPath()%>/front-end/gif_collection/gif_list.jsp"><img alt="GIF"  title="GIF" src="<%= request.getContextPath()%>/front-end/res/img/index/gif.png" style="height:30px;width:30px"></a></li> 
     <li><a href="<%=request.getContextPath()%>/front-end/member/listOneMember.jsp"><img alt="會員資料" title="會員資料" src="<%= request.getContextPath()%>/front-end/res/img/index/user.png" style="height:30px;width:30px"></a></li> 
     <li><a href="#" onclick="logout()"><img alt="登出" title="登出" src="<%= request.getContextPath()%>/front-end/res/img/index/logout.png" style="height:30px;width:30px"></a> 
     <li><a href="<%=request.getContextPath()%>/front-end/chatBox/chatBox.jsp"><img alt="聊天室" title="聊天室" src="<%= request.getContextPath()%>/front-end/res/img/index/chat.png" style="height:30px;width:30px"></a></li> 
     <li>${memberVO.memberNickName }</li>
     <li><a href="<%=request.getContextPath()%>/front-end/personalPage/myPersonalPage.jsp"><img src="<%= request.getContextPath()%>/front-end/member/memberImg.do?memberID=${memberVO.memberID}" id="memberPhoto"></a></li>	
     <li id="notifi"><a href="#modal-id-notifi" data-toggle="modal"> 
      <div style="position: relative;"> 
      <img src="<%=request.getContextPath()%>/front-end/res/img/index/notification(32).png" alt="通知" id="notifiPhoto"> 
      <span style="position: absolute; top: 0; left: 17px; color: white" id="notifiCount"></span> 
      </div> 
     </a></li> 
     </ul>
   </div> 
  </div> 
 </header> 
 <!-- Header section end -->
<FORM METHOD="post" ACTION="<%=request.getContextPath()%>/front-end/member/member.do" id="logout">
	<input type="hidden" name="action"	value="logOut">
</FORM>
<script type="text/javascript">
	function logout(){
		document.getElementById("logout").submit();
	}
</script>


	<div class="container-fluid">
		<div class="row">
<!-- 		聊天室列表 -->
			<div class="col-xs-12 col-sm-2">
				<div class="friend-column" align="center">
					<c:forEach var="chatMemberVO" items="${list}">
						<div class="chatBox" onclick="connect('${chatMemberVO.chatBoxID}', '${memberVO.memberNickName}');">
							${chatBoxSvc.getChatBoxName(chatMemberVO.chatBoxID, memberVO.memberID)}
						</div>
					</c:forEach>
				</div>		
			</div>
			
<!-- 			聊天室介面 -->
			<div class="col-xs-12 col-sm-8 message-column">
<!-- 				<h3 id="statusOutput" class="statusOutput"></h3> -->
				<div class="message-area" id="message-area"><ul></ul></div>
				<div class="input-group input-group-margin">
					<input id="message" class="form-control" type="text" onkeydown="if (event.keyCode == 13) sendMessage();" 
						placeholder="輸入訊息……">
					<div class="input-group-btn">
						<a href="#" class="btn btn-default" id="show-gif"><i class="fa fa-picture-o"></i></a>
					</div>	
				</div>
			</div>

			<!-- gif列表 -->
			<div class="col-xs-12 col-sm-2">
				<div class="gif-column">
					<c:forEach var="gifVO" items="${listGif}">
						<div class="col-xs-12 col-sm-8 col-offset-2 a-gif">
							<div onclick="sendImg('<%=request.getContextPath()%>/gifImg/gif.do?gifID=${gifVO.gifID}&gifFile=gifFile')">
								<img src="<%=request.getContextPath()%>/gifImg/gif.do?gifID=${gifVO.gifID}&gifFile=gifFile" >
							</div>
						</div>
					</c:forEach>
				</div>	
			</div>
		</div>
	</div>
	
	
		<!-- 通知列表的跳窗 -->
		<div class="modal fade" id="modal-id-notifi">
			<div class="modal-dialog modal-sm">
			   <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<div class="modal-content">
				   
					<div class="modal-header">					
						<h4 class="modal-title">通知列表</h4>
					</div>
					<div class="modal-body kk-modal-body" >

                    <!-- 及時通知顯示的地方 -->
					<div id="nolistsssss" class="kk-no-list">
					  <a href="<%=request.getContextPath()%>/adfundTest/adfund.do?action=getOne_For_Display&adfundID=${adfundVO.adfundID}" ></a>
					 </div>
                        
						<c:forEach var="NotificationListVO" items="${nolists}" >
					      <div class="kk-no-list">					      
					         <a href="<%=request.getContextPath()%>${NotificationListVO.notificationpath}${NotificationListVO.notificationtarget}" id="${NotificationListVO.notificationid}">${NotificationListVO.notificationcontent}</a>
					         <h6><fmt:formatDate value="${NotificationListVO.notificationdate}" pattern="yyyy-MM-dd HH:mm"/></h6>
					      </div>
					    </c:forEach>
					</div>
					<div class="modal-footer">

					</div>
				</div>
			</div>
		</div>
	<!-- 通知的跳窗 -->

<script>

$(document).ready(function() { 
	
	//點擊鈴鐺時，通知要全部變為已讀
    $('#notifi').click(function(){
    	
//              alert("!!")
				$.ajax({ 
				     url : '<%=request.getContextPath()%>/front-end/NotificationListServlet/NotificationListServlet.do', 
				     type : 'Post', 
				     data : { 
				      action : 'isClick', 
				      memberID : '${memberVO.memberID}' 

				     } 
				    }).done(function(res){
				    	var obj = JSON.parse(res);
				    	document.getElementById("notifiPhoto").src = "<%=request.getContextPath()%>/front-end/res/img/index/notification(32).png";
				    	$('#notifiCount').html("");
	                });				     
     });
 
	
//    alert("noti!!");
    //網頁一打開時，要立刻判斷是否有未讀訊息，看是否要顯示有消息的鈴鐺
    $.ajax({ 
	     url : '<%=request.getContextPath()%>/front-end/NotificationListServlet/NotificationListServlet.do', 
	     type : 'Post', 
	     data : { 
	      action : 'isRead',  
	      memberID : '${memberVO.memberID}'
	     } 
	    }).done(function(res){
	    	var obj = JSON.parse(res);
	    	if(obj.notiStatus==false){
	    	   document.getElementById("notifiPhoto").src = "<%=request.getContextPath()%>/front-end/res/img/index/notification(32)red.png";
	    	   $('#notifiCount').html(obj.falseCount);
	    	}
	    });
    
});
</script>

</body>

<script type="text/javascript">
// 秀gif列表
function test(gifIDStr) {
	console.log(gifIDStr);
	var gifID = gifIDStr;
	$('#ItemPreview').attr('src', gifID);
}
</script>
<script type="text/javascript">
// 秀gif列表
$(document).ready(function() {
	$('.gif-column').hide();
	$('#show-gif').click( function(e) {
		$('.gif-column').toggle();
	});	
});

</script>
<script>
   
// 	var statusOutput = document.getElementById("statusOutput");
// 	var messagesArea = document.getElementById("messagesArea");
	var webSocket;
	var chatBoxID;
	var memberNickName;
	var totalMessage = "";
	
	function connect(chatBoxIDStr, memberNickNameStr) {
		chatBoxID = chatBoxIDStr;
		memberNickName = memberNickNameStr;
// 		messagesArea.value = null;
		$('#message-area>ul').html("");
		
		var MyPoint = "/MyEchoServer/"+ chatBoxID +"/" + memberNickName;
	    var host = window.location.host;
	    var path = window.location.pathname;
	    var webCtx = path.substring(0, path.indexOf('/', 1));
	    var endPointURL = "wss://" + window.location.host + webCtx + MyPoint;
	    
		console.log(endPointURL);
		if(webSocket != null) {
			disconnect();
		}
		
		// 建立 websocket 物件
		webSocket = new WebSocket(endPointURL);
		
		webSocket.onopen = function(event) {
// 			updateStatus("WebSocket 成功連線");
		};

		webSocket.onmessage = function(event) {
// 			console.log(event.data);
			var message = "";
			var jsonObj = JSON.parse(event.data);
			
			message = handleMessage(jsonObj);
			console.log(message);
	        $('#message-area>ul').append(message);
	       
		};

		webSocket.onclose = function(event) {
// 			updateStatus("WebSocket 已離線");
		};
	}
	

	
	function sendMessage() {
		console.log(chatBoxID);
		console.log(memberNickName);

	    var inputMessage = document.getElementById("message");
	    var messageContent = inputMessage.value.trim();
	    
	    if (messageContent === ""){
	        inputMessage.focus();
	        return;
	    }else{
	        var jsonObj = {"type" : "","chatBoxID" : chatBoxID, "memberNickName" : memberNickName, 
	        		"messageContent" : messageContent};
	        webSocket.send(JSON.stringify(jsonObj));
	        inputMessage.value = "";
	        inputMessage.focus();
	    }
	}
	
	function sendImg(url) {
		console.log(url);

        var jsonObj = {"type" : "picture","chatBoxID" : chatBoxID, "memberNickName" : memberNickName, 
        		"messageContent" : url};
        webSocket.send(JSON.stringify(jsonObj));
	    
	}
	
	
 	function handleMessage(jsonObj) {
 		var message = "";
 		
        if(jsonObj.type === "history") {
        	var historyList  = JSON.parse(jsonObj.messageContent);
        	var historyMessage = "";
        	for (i = 0; i < historyList.length; i++) {
        		if(JSON.parse(historyList[i]).memberNickName == memberNickName) {
        			if(JSON.parse(historyList[i]).type == "picture") {
        				historyMessage = "<li class='float-right'><img src='" + JSON.parse(historyList[i]).messageContent + 
        				"'></li>";
        				message= message + historyMessage;
        			} else {
        				historyMessage = "<li class='float-right'><span class='span-blue'>" + JSON.parse(historyList[i]).messageContent + "</span></li>";
                		message= message + historyMessage;
        			}			
        		} else {
        			if(JSON.parse(historyList[i]).type == "picture") {
        				historyMessage = "<li class='float-left'>" + JSON.parse(historyList[i]).memberNickName +
                		"：<br><img src='" + JSON.parse(historyList[i]).messageContent + "'></li>";
        				message= message + historyMessage;
        			} else {
        				historyMessage = "<li class='float-left'>" + JSON.parse(historyList[i]).memberNickName +
                		"：<span class='span-gray'>" + JSON.parse(historyList[i]).messageContent + "</span></li>";
                		message= message + historyMessage;
        			}		
        		}
        	}
        } else {
        	if (jsonObj.memberNickName == memberNickName) {
        		if(jsonObj.type == "picture") {
        			message = "<li class='float-right'><img src='" + jsonObj.messageContent + "'></li>";
        		} else {
        			message = "<li class='float-right'><span class='span-blue'>" + jsonObj.messageContent + "</span></li>";
        		}
        	} else {
				if(jsonObj.type == "picture") {
					message = "<li class='float-left'>" + jsonObj.memberNickName + "：<br><img src='" +
					jsonObj.messageContent + "'></li>";
        		} else {
        			message = "<li class='float-left'>" + jsonObj.memberNickName + "：<span class='span-gray'>" + jsonObj.messageContent + "</span></li>";
        		}		
        	}
        }
        
        
 		return message;
 	}
	
	
	function disconnect () {
		webSocket.close();
	}
	
// 	function updateStatus(newStatus) {
// 		statusOutput.innerHTML = newStatus;
// 	}
    
</script>
</html>