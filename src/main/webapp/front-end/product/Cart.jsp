<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page import="java.util.*"%>
<%@ page import="com.product.model.*"%>
<%@ page import="com.member.model.*" %>
<%@ page import="com.notificationList.model.*"%>

<%
	//	會員
	MemberVO memberVO = (MemberVO) session.getAttribute("memberVO");
	String memberID = memberVO.getMemberID();
	
	//	通知
	NotificationListService noSvc = new NotificationListService();
	List<NotificationListVO> nolists = noSvc.getAllNoList(memberID);
	pageContext.setAttribute("nolists",nolists);

%>
<!DOCTYPE html>
	<html lang="en">
	<head>
	<title>buyerOrder.jsp</title>
	<meta charset="UTF-8">
	<meta name="description" content="Food Blog Web Template">
	<meta name="keywords" content="food, unica, creative, html">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- Favicon -->   
	<link href="<%=request.getContextPath()%>/front-end/img/favicon.ico" rel="shortcut icon"/>

	<!-- Google Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Poppins:400,400i,500,500i,600,600i,700" rel="stylesheet">

	<!-- Stylesheets -->
	<link rel="stylesheet" href="<%=request.getContextPath()%>/front-end/css/bootstrap.min.css"/>
	<link rel="stylesheet" href="<%=request.getContextPath()%>/front-end/css/font-awesome.min.css"/>
	<link rel="stylesheet" href="<%=request.getContextPath()%>/front-end/css/owl.carousel.css"/>
	<link rel="stylesheet" href="<%=request.getContextPath()%>/front-end/css/animate.css"/>
	<link rel="stylesheet" href="<%=request.getContextPath()%>/front-end/css/style.css"/>
	
<!-- 原本	--- -->
	<link rel="stylesheet" href="<%=request.getContextPath()%>/front-end/product/css/bootstrap.min.css">
	<link rel="stylesheet" href="<%=request.getContextPath()%>/front-end/product/css/bootstrap-theme.min.css">
	
	<script src="<%=request.getContextPath()%>/front-end/product/js/jquery-3.3.1.min.js"></script>
	<!--         Latest compiled JavaScript -->
	<script src="<%=request.getContextPath()%>/front-end/product/js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="<%=request.getContextPath()%>/front-end/product/css/index.css">
	<link rel="stylesheet" href="<%=request.getContextPath()%>/front-end/product/css/seller.css">
	<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/adfund/css/bootstrap-theme.min.css"> 


	<!--[if lt IE 9]>
	  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	<style>
		body {
	  		font-family: \5FAE\8EDF\6B63\9ED1\9AD4, \65B0\7D30\660E\9AD4;
			 }
		#customers {
		    border-collapse: collapse;
		    width: 75%;
		}
		
		#customers td, #customers th {
		    border: 1px solid #ddd;
		    padding: 8px;
		    text-align:center;
		    
		}
		
		
		#customers tr:nth-child(even){background-color: #f2f2f2;}
		
		#customers tr:hover {background-color: #ddd;}
		
		#customers th {
		    padding-top: 12px;
		    padding-bottom: 12px;
		    text-align: center;
		    background-color: #7dccff;
		    color: white;
		}
		#memberPhoto {
		    border-radius: 50%;
		    height: 30px;
		    width: 30px;
		}
		
		.kk-no-list{
		   height:auto;
		   border:1px solid;
		   border-color: #E3E0E0;
		   background-color: #E3E0E0;
		   margin: 2px;
		   padding: 2px;
		   font-size:15px;
		}
		
		.kk-modal-body{
		   height:300px;
		   overflow-y: scroll;
		   overflow-x: hidden;
		
		}
	</style>
	</head>
<body>

<!-- 	<div id="preloder"> -->
<!-- 		<div class="loader"></div> -->
<!-- 	</div> -->

		
			<!-- Header section --> 
 <header class="header-section">	
  <div class="header-bottom"> 
   <div class="container"> 
    <a href="<%=request.getContextPath()%>/front-end/index.jsp" class="site-logo"> 
     <img src="<%=request.getContextPath() %>/front-end/img/logo-1.png" alt=""> 
    </a> 
    <div class="nav-switch"> 
     <i class="fa fa-bars"></i> 
    </div> 

    <ul class="main-menu"> 
     <li><a href="<%= request.getContextPath()%>/front-end/board/listAllBoard.jsp"><img alt="藝人專板"  title="藝人專板" src="<%= request.getContextPath()%>/front-end/res/img/index/board.png" style="height:30px;width:30px"></a></li> 
     <li><a href="<%= request.getContextPath()%>/front-end/product/Mall.jsp"><img alt="商城"  title="商城" src="<%= request.getContextPath()%>/front-end/res/img/index/mall.png" style="height:30px;width:30px"></a></li> 
     <li><a href="<%=request.getContextPath()%>/front-end/gif_collection/gif_list.jsp"><img alt="GIF"  title="GIF" src="<%= request.getContextPath()%>/front-end/res/img/index/gif.png" style="height:30px;width:30px"></a></li> 
     <li><a href="<%=request.getContextPath()%>/front-end/member/listOneMember.jsp"><img alt="會員資料" title="會員資料" src="<%= request.getContextPath()%>/front-end/res/img/index/user.png" style="height:30px;width:30px"></a></li> 
     <li><a href="#" onclick="logout()"><img alt="登出" title="登出" src="<%= request.getContextPath()%>/front-end/res/img/index/logout.png" style="height:30px;width:30px"></a> 
     <li><a href="<%=request.getContextPath()%>/front-end/chatBox/chatBox.jsp"><img alt="聊天室" title="聊天室" src="<%= request.getContextPath()%>/front-end/res/img/index/chat.png" style="height:30px;width:30px"></a></li> 
     <li>${memberVO.memberNickName }</li>
     <li><a href="<%=request.getContextPath()%>/front-end/personalPage/myPersonalPage.jsp"><img src="<%= request.getContextPath()%>/front-end/member/memberImg.do?memberID=${memberVO.memberID}" id="memberPhoto"></a></li>	
     <li id="notifi"><a href="#modal-id-notifi" data-toggle="modal"> 
      <div style="position: relative;"> 
      <img src="<%=request.getContextPath()%>/front-end/res/img/index/notification(32).png" alt="通知" id="notifiPhoto"> 
      <span style="position: absolute; top: 0; left: 17px; color: white" id="notifiCount"></span> 
      </div> 
     </a></li> 
     </ul>
   </div> 
  </div> 
 </header> 
 <!-- Header section end -->
<FORM METHOD="post" ACTION="<%=request.getContextPath()%>/front-end/member/member.do" id="logout">
	<input type="hidden" name="action"	value="logOut">
</FORM>
<script type="text/javascript">
	function logout(){
		document.getElementById("logout").submit();
	}
</script>
 
   <% @SuppressWarnings("unchecked")
   Vector<ProductVO> buylist = (Vector<ProductVO>) session.getAttribute("shoppingcart");
   
   %>
<%if (buylist != null && (buylist.size() > 0)) {%>

<div align="center"><font size="+3">目前購物車的內容如下：</font></div>

<div align="center">
			<table id="customers">
			    <tr>
			      <th width="100"></th> 
			      <th width="100">商品編號</th>
			      <th width="100">商品名稱</th>
			      <th width="100">賣家編號</th>
			      <th width="100">價格</th>
			      <th width="50">數量</th>
			      <th></th>
			    </tr>	
			
				<%
				 for (int index = 0; index < buylist.size(); index++) {
					ProductVO productVO = buylist.get(index);
				%>
				<tr>
					<td><img src="<%=request.getContextPath()%>/product/productImg.do?productID=<%=productVO.getProductID()%>" style="width:150px; height:150px;"></td>
					<td width="100"><%=productVO.getProductID()%></td>
					<td width="100"><%=productVO.getProductName()%></td>
					<td width="100"><%=productVO.getMemberID()%></td>
					<td width="100"><%=productVO.getProductPrice()%></td>
					<td><%=productVO.getQuantity()%></td>
					
			        <td width="50">
			          <form name="deleteForm" action="<%=request.getContextPath()%>/product/product.do" method="POST">
			              <input type="hidden" name="action"  value="DELETE">
			              <input type="hidden" name="del" value="<%= index %>">
			              <input type="hidden" name="requestURL" value="<%=request.getServletPath()%>">
			              <input type="submit" class="btn btn-warning"value="刪 除" class="button">
			          </form>
			        </td>
				</tr>
				<%}%>
			</table>
			<div>
			<table>
				<td style="padding: 30px;">	
					<a href="<%=request.getContextPath()%>/front-end/product/Mall.jsp"><button class="btn btn-danger">繼續購物</button></a>
				</td>
				<td style="padding: 30px;">	
		          	<form name="checkoutForm" action="<%=request.getContextPath()%>/product/product.do" method="POST">
		              <input type="hidden" name="action"  value="CHECKOUT"> 
		              <input type="submit" class="btn btn-warning" value="付款結帳" class="button">
		          	</form>
		        </td>
          </table>
          </div>
<%}else{%>
			<div align="center">
				<img  src="<%=request.getContextPath()%>/front-end/res/img/mall/empty_cart.jpeg" style="width:400px;height:300px;">
			</div>
			<div align="center">
				<p>您的購物車空空的...</p>
			</div>
			<div align="center">
				<a href="<%=request.getContextPath()%>/front-end/product/Mall.jsp">
					<button style="background-color:white;border: none;">
						<img  src="<%=request.getContextPath()%>/front-end/res/img/mall/buyNow.png" style="width:150px;height:50px;">
					</button>
				</a>
			</div>
<%} %>
</div>


		<!-- 通知列表的跳窗 -->
		<div class="modal fade" id="modal-id-notifi">
			<div class="modal-dialog modal-sm">
			   <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<div class="modal-content">
				   
					<div class="modal-header">					
						<h4 class="modal-title">通知列表</h4>
					</div>
					<div class="modal-body kk-modal-body" >

                    <!-- 及時通知顯示的地方 -->
					<div id="nolistsssss" class="kk-no-list">
					  <a href="<%=request.getContextPath()%>/adfundTest/adfund.do?action=getOne_For_Display&adfundID=${adfundVO.adfundID}" ></a>
					 </div>
                        
						<c:forEach var="NotificationListVO" items="${nolists}" >
					      <div class="kk-no-list">					      
					         <a href="<%=request.getContextPath()%>${NotificationListVO.notificationpath}${NotificationListVO.notificationtarget}" id="${NotificationListVO.notificationid}">${NotificationListVO.notificationcontent}</a>
					         <h6><fmt:formatDate value="${NotificationListVO.notificationdate}" pattern="yyyy-MM-dd HH:mm"/></h6>
					      </div>
					    </c:forEach>
					</div>
					<div class="modal-footer">

					</div>
				</div>
			</div>
		</div>
	<!-- 通知的跳窗 -->

<script>

$(document).ready(function() { 
	
	//點擊鈴鐺時，通知要全部變為已讀
    $('#notifi').click(function(){
    	
//              alert("!!")
				$.ajax({ 
				     url : '<%=request.getContextPath()%>/front-end/NotificationListServlet/NotificationListServlet.do', 
				     type : 'Post', 
				     data : { 
				      action : 'isClick', 
				      memberID : '${memberVO.memberID}' 

				     } 
				    }).done(function(res){
				    	var obj = JSON.parse(res);
				    	document.getElementById("notifiPhoto").src = "<%=request.getContextPath()%>/front-end/res/img/index/notification(32).png";
				    	$('#notifiCount').html("");
	                });				     
     });
 
	
//    alert("noti!!");
    //網頁一打開時，要立刻判斷是否有未讀訊息，看是否要顯示有消息的鈴鐺
    $.ajax({ 
	     url : '<%=request.getContextPath()%>/front-end/NotificationListServlet/NotificationListServlet.do', 
	     type : 'Post', 
	     data : { 
	      action : 'isRead',  
	      memberID : '${memberVO.memberID}'
	     } 
	    }).done(function(res){
	    	var obj = JSON.parse(res);
	    	if(obj.notiStatus==false){
	    	   document.getElementById("notifiPhoto").src = "<%=request.getContextPath()%>/front-end/res/img/index/notification(32)red.png";
	    	   $('#notifiCount').html(obj.falseCount);
	    	}
	    });
    
});
</script>
</body>
</html>