<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page import="java.util.*" %>
<%@ page import="com.product.model.*"%>
<%@ page import="com.notificationList.model.*"%>
<%@ page import="com.member.model.*"%>

<%

	//	會員
	MemberVO memberVO = (MemberVO) session.getAttribute("memberVO");
	String memberID1 = memberVO.getMemberID();
	
	//	通知
	NotificationListService noSvc = new NotificationListService();
	List<NotificationListVO> nolists = noSvc.getAllNoList(memberID1);
	pageContext.setAttribute("nolists",nolists);

%>
<!DOCTYPE html> 
<html lang="en"> 
<head> 
 <title>Checkout.jsp</title> 
 <meta charset="UTF-8"> 
 <meta name="description" content="Food Blog Web Template"> 
 <meta name="keywords" content="food, unica, creative, html"> 
 <meta name="viewport" content="width=device-width, initial-scale=1.0"> 
 <!-- Favicon -->  
 <link href="img/favicon.ico" rel="shortcut icon"/> 
 
 <!-- Google Fonts --> 
 <link href="https://fonts.googleapis.com/css?family=Poppins:400,400i,500,500i,600,600i,700" rel="stylesheet"> 
 
 <!-- Stylesheets --> 
 <link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/css/bootstrap.min.css"/> 
 <link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/css/font-awesome.min.css"/> 
 <link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/css/owl.carousel.css"/> 
 <link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/css/animate.css"/> 
 <link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/css/style.css"/> 
 
 
 <link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/product/css/bootstrap.min.css"> 
 <link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/product/css/bootstrap-theme.min.css"> 
 <link rel="stylesheet" href="<%=request.getContextPath()%>/front-end/member/css/styleLogin.css"> 
 
 <script src="//code.jquery.com/jquery-1.11.1.min.js"></script> 

 
 <script src="<%=request.getContextPath() %>/front-end/product/js/bootstrap.min.js"></script> 
 <script src="<%=request.getContextPath() %>/front-end/product/js/tw-city-selector.min.js"></script> 
<!-- form 表單 --> 
 <link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.min.css" rel="stylesheet" id="bootstrap-css"> 
 <script src="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/js/bootstrap.min.js"></script> 
 <link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/adfund/css/bootstrap-theme.min.css">  
 <style>
		body  {
			font-family: \5FAE\8EDF\6B63\9ED1\9AD4, \65B0\7D30\660E\9AD4;
			  }
		.customers {
					    border-collapse: collapse;
					    width: 100%;
		}
		
		.customers td, .customers th {
		    border: 1px solid #ddd;
		    padding: 8px;
		    text-align:center;
		    
		}
		
		
		.customers tr:nth-child(even){background-color: #f2f2f2;}
		
		.customers tr:hover {background-color: #ddd;}
		
		.customers th {
		    padding-top: 12px;
		    padding-bottom: 12px;
		    text-align: center;
		    background-color: #7dccff;
		    color: white;
		}
					
		#memberPhoto {
		    border-radius: 50%;
		    height: 30px;
		    width: 30px;
		}
		
		.kk-no-list{
		   height:auto;
		   border:1px solid;
		   border-color: #E3E0E0;
		   background-color: #E3E0E0;
		   margin: 2px;
		   padding: 2px;
		   font-size:15px;
		}
		
		.kk-modal-body{
		   height:300px;
		   overflow-y: scroll;
		   overflow-x: hidden;
		
		}
	</style>
 </head>
<body>

<!-- 	<div id="preloder"> -->
<!-- 		<div class="loader"></div> -->
<!-- 	</div> -->

		
			<!-- Header section --> 
 <header class="header-section">	
  <div class="header-bottom"> 
   <div class="container"> 
    <a href="<%=request.getContextPath()%>/front-end/index.jsp" class="site-logo"> 
     <img src="<%=request.getContextPath() %>/front-end/img/logo-1.png" alt=""> 
    </a> 
    <div class="nav-switch"> 
     <i class="fa fa-bars"></i> 
    </div> 

    <ul class="main-menu"> 
     <li><a href="<%= request.getContextPath()%>/front-end/board/listAllBoard.jsp"><img alt="藝人專板"  title="藝人專板" src="<%= request.getContextPath()%>/front-end/res/img/index/board.png" style="height:30px;width:30px"></a></li> 
     <li><a href="<%= request.getContextPath()%>/front-end/product/Mall.jsp"><img alt="商城"  title="商城" src="<%= request.getContextPath()%>/front-end/res/img/index/mall.png" style="height:30px;width:30px"></a></li> 
     <li><a href="<%=request.getContextPath()%>/front-end/gif_collection/gif_list.jsp"><img alt="GIF"  title="GIF" src="<%= request.getContextPath()%>/front-end/res/img/index/gif.png" style="height:30px;width:30px"></a></li> 
     <li><a href="<%=request.getContextPath()%>/front-end/member/listOneMember.jsp"><img alt="會員資料" title="會員資料" src="<%= request.getContextPath()%>/front-end/res/img/index/user.png" style="height:30px;width:30px"></a></li> 
     <li><a href="#" onclick="logout()"><img alt="登出" title="登出" src="<%= request.getContextPath()%>/front-end/res/img/index/logout.png" style="height:30px;width:30px"></a> 
     <li><a href="<%=request.getContextPath()%>/front-end/chatBox/chatBox.jsp"><img alt="聊天室" title="聊天室" src="<%= request.getContextPath()%>/front-end/res/img/index/chat.png" style="height:30px;width:30px"></a></li> 
     <li>${memberVO.memberNickName }</li>
     <li><a href="<%=request.getContextPath()%>/front-end/personalPage/myPersonalPage.jsp"><img src="<%= request.getContextPath()%>/front-end/member/memberImg.do?memberID=${memberVO.memberID}" id="memberPhoto"></a></li>	
     <li id="notifi"><a href="#modal-id-notifi" data-toggle="modal"> 
      <div style="position: relative;"> 
      <img src="<%=request.getContextPath()%>/front-end/res/img/index/notification(32).png" alt="通知" id="notifiPhoto"> 
      <span style="position: absolute; top: 0; left: 17px; color: white" id="notifiCount"></span> 
      </div> 
     </a></li> 
     </ul>
   </div> 
  </div> 
 </header> 
 <!-- Header section end -->
<FORM METHOD="post" ACTION="<%=request.getContextPath()%>/front-end/member/member.do" id="logout">
	<input type="hidden" name="action"	value="logOut">
</FORM>
<script type="text/javascript">
	function logout(){
		document.getElementById("logout").submit();
	}
</script>

 			<center><h2>購物車結帳:<h2></center>
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-12">

						<table class="customers">
							<tr>
								<th>商品編號</th>
								<th>商品名稱</th>
								<th>賣家編號</th>
								<th>商品價格</th>
								<th>數量</th>
								<th>金額</th>
							</tr>
							<%  @SuppressWarnings("unchecked")
								Vector<ProductVO> buylist = (Vector<ProductVO>) session.getAttribute("shoppingcart");
								String amount =  (String) session.getAttribute("amount");
							%>	
							<%	for (int i = 0; i < buylist.size(); i++) {
									ProductVO productVO = buylist.get(i);
									String productID = productVO.getProductID();
									String productName = productVO.getProductName();
									String memberID = productVO.getMemberID();
									Integer productPrice = productVO.getProductPrice();
									Integer quantity = productVO.getQuantity();
							%>
							<tr>
								<td><%=productID%>     </td>
								<td><%=productName%>   </td>
								<td><%=memberID%>	   </td>
								<td><%=productPrice%> </td>
								<td><%=quantity%>      </td>
								<td><%=productPrice*quantity%></td>
							</tr>
							<%
								}
							%>
							<tr>
								<td colspan="6" style="text-align:right; font-size:18px"> 
								   總金額：<span style="color:red">$<%=amount%></span> 
							    </td>
							</tr>
						</table>
					</div>
				</div>
			</div>
			
			
			<hr>
					<div>
						<h2>
							<center>填寫信用卡資訊及送貨資訊</center>
						</h2>
					</div>
					
						<div class="container">
							  <div class="row" >
							  <div class="col-sm-6 col-sm-offset-2 creditinfo">	
							    <div class="span12" >
							    <c:if test="${not empty errorMsgs}">
									<font style="color:red">請修正以下錯誤:</font>
									<ul>
										<c:forEach var="message" items="${errorMsgs}">
											<li style="color:red">${message}</li>
										</c:forEach>
									</ul>
								</c:if>
							      <form class="form-horizontal span6" method="post" action="<%=request.getContextPath()%>/orderList/orderList.do">
							        <fieldset>
							          <legend>信用卡</legend>
							       
							          <div class="control-group">
							            <label class="control-label">持卡人姓名</label>
							            <div class="controls">
							              <input type="text" class="input-block-level"  title="Fill your first and last name" required style="width:200px" value="柯P">
							            </div>
							          </div>
							       
							          <div class="control-group">
							            <label class="control-label">信用卡卡號</label>
							            <div class="controls">
							              <div class="row-fluid">
							                <div class="span3">
							                  <input type="text" name="credit1" id="credit1" class="input-block-level" autocomplete="off" maxlength="4" pattern="\d{4}" title="First four digits" value="6666" required>
							                </div>
							                <div class="span3">
							                  <input type="text" name="credit2" id="credit2" class="input-block-level" autocomplete="off" maxlength="4" pattern="\d{4}" title="Second four digits"  value="6666" required>
							                </div>
							                <div class="span3">
							                  <input type="text" name="credit3" id="credit3" class="input-block-level" autocomplete="off" maxlength="4" pattern="\d{4}" title="Third four digits" value="6666"  required>
							                </div>
							                <div class="span3">
							                  <input type="text" name="credit4" id="credit4" class="input-block-level" autocomplete="off" maxlength="4" pattern="\d{4}" title="Fourth four digits"  value="6666" required>
							                </div>
							              </div>
							            </div>
							          </div>
							       
							          <div class="control-group">
							            <label class="control-label">信用卡有效月年</label>
							            <div class="controls">
							              <div class="row-fluid">
							                
							                  <select class="input-block-level" style="width:100px">
							                    <option>1月</option>
							                    <option>2月</option>
							                    <option>3月</option>
							                    <option>4月</option>
							                    <option>5月</option>
							                    <option>6月</option>
							                    <option>7月</option>
							                    <option>8月</option>
							                    <option>9月</option>
							                    <option>10月</option>
							                    <option>11月</option>
							                    <option>12月</option>
							                  </select>
							                
							                
							                  <select class="input-block-level" style="width:100px">
							                    <option>2013</option>
							                    <option>2014</option>
							                    <option>2015</option>
							                    <option>2016</option>
							                    <option>2017</option>
							                    <option>2018</option>
							                    <option>2019</option>
							                    <option>2020</option>
							                    <option>2021</option>
							                    <option>2022</option>
							                    <option>2023</option>
							                    <option>2024</option>
							                    <option>2025</option>
							                    <option>2026</option>
							                    <option>2027</option>
							           
							                  </select>
							                
							              </div>
							            </div>
							          </div>
							       
							          <div class="control-group">
							            <label class="control-label">卡片背後末三碼</label>
							            <div class="controls">
							              <div class="row-fluid">
							                <div class="span3">
							                  <input type="text" class="input-block-level" autocomplete="off" maxlength="3" pattern="\d{3}" title="Three digits at back of your card" required>
							                </div>
							                <div class="span8">
							                  <!-- screenshot may be here -->
							                </div>
							              </div>
							            </div>
							          </div>
							       
							          
							        </fieldset>
							        <fieldset>
							          <legend>送貨資訊　　<input type="checkbox" id="check"><span style="font-size:13px;">同會員資料</span></legend>
							        
							          <div class="control-group">
							            <label class="control-label">收貨人姓名:</label>
							            <div class="controls">
							              <input name="buyerName" id="buyerName" type="text" class="input-block-level" style="width:200px;"  required>
							            </div>
							          </div>
							       
							          <div class="control-group">
							            <label class="control-label">收貨人手機:</label>
							            <div class="controls">
							                 <input name="buyerPhone" id="buyerPhone" type="text" class="input-block-level" autocomplete="on" maxlength="10" style="width:200px;"   required>
							              </div>
							          </div>
							          
							  			<div class="my-selector-c control-group ">
							  			<label class="control-label">請輸入地址:</label>
											  <div class="controls">
											    <select name="add1" id="add1" class="county" style="width:100px;"></select>
											  </div>
											 <div class="controls">
											    <select name="add2" id="add2" class="district" style="width:100px;"></select>
											   </div>
											  <div class="controls">
											  	<input name="add4" id="add4" type="text" class="" style="width:300px;height:30px">
											  </div>	
										</div>
											
											<script>
											  new TwCitySelector({
											    el: ".my-selector-c",
											    elCounty: ".county", // 在 el 裡查找 dom
											    elDistrict: ".district", // 在 el 裡查找 dom
											    elZipcode: ".zipcode" // 在 el 裡查找 dom
											  });
											</script>
							        </fieldset>
							        <center>
							        	<input type="hidden" name="transactionMethod" value="CREDITCARD">
							        	<input type="hidden" name="orderStatus" value="PAID">
							        	<input type="hidden" name="action" value="insert">
									   <button type="submit" id="submit"class="btn btn-primary">送出</button>
									</center>
							      </form>
							    </div>
							    
							  </div>
							</div>
						</div>
							
				<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
				
				
		<!-- 通知列表的跳窗 -->
		<div class="modal fade" id="modal-id-notifi">
			<div class="modal-dialog modal-sm">
			   <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<div class="modal-content">
				   
					<div class="modal-header">					
						<h4 class="modal-title">通知列表</h4>
					</div>
					<div class="modal-body kk-modal-body" >

                    <!-- 及時通知顯示的地方 -->
					<div id="nolistsssss" class="kk-no-list">
					  <a href="<%=request.getContextPath()%>/adfundTest/adfund.do?action=getOne_For_Display&adfundID=${adfundVO.adfundID}" ></a>
					 </div>
                        
						<c:forEach var="NotificationListVO" items="${nolists}" >
					      <div class="kk-no-list">					      
					         <a href="<%=request.getContextPath()%>${NotificationListVO.notificationpath}${NotificationListVO.notificationtarget}" id="${NotificationListVO.notificationid}">${NotificationListVO.notificationcontent}</a>
					         <h6><fmt:formatDate value="${NotificationListVO.notificationdate}" pattern="yyyy-MM-dd HH:mm"/></h6>
					      </div>
					    </c:forEach>
					</div>
					<div class="modal-footer">

					</div>
				</div>
			</div>
		</div>
	<!-- 通知的跳窗 -->

<script>

$(document).ready(function() { 
	
	//點擊鈴鐺時，通知要全部變為已讀
    $('#notifi').click(function(){
    	
//              alert("!!")
				$.ajax({ 
				     url : '<%=request.getContextPath()%>/front-end/NotificationListServlet/NotificationListServlet.do', 
				     type : 'Post', 
				     data : { 
				      action : 'isClick', 
				      memberID : '${memberVO.memberID}' 

				     } 
				    }).done(function(res){
				    	var obj = JSON.parse(res);
				    	document.getElementById("notifiPhoto").src = "<%=request.getContextPath()%>/front-end/res/img/index/notification(32).png";
				    	$('#notifiCount').html("");
	                });				     
     });
 
	
//    alert("noti!!");
    //網頁一打開時，要立刻判斷是否有未讀訊息，看是否要顯示有消息的鈴鐺
    $.ajax({ 
	     url : '<%=request.getContextPath()%>/front-end/NotificationListServlet/NotificationListServlet.do', 
	     type : 'Post', 
	     data : { 
	      action : 'isRead',  
	      memberID : '${memberVO.memberID}'
	     } 
	    }).done(function(res){
	    	var obj = JSON.parse(res);
	    	if(obj.notiStatus==false){
	    	   document.getElementById("notifiPhoto").src = "<%=request.getContextPath()%>/front-end/res/img/index/notification(32)red.png";
	    	   $('#notifiCount').html(obj.falseCount);
	    	}
	    });
});

$("#check").change(function(){
	var memberName='${memberVO.memberName}';
	var memberPhone='${memberVO.memberPhone}';
	var memberAddress='${memberVO.memberAddress}';
	var add01=memberAddress.substring(0,3);
	var add02=memberAddress.substring(3,6);
	var add03=memberAddress.substring(6,memberAddress.length);
	console.log(add01);
	console.log(add02);
	console.log(add03);
	
	if($("#check").prop("checked")==true){
		$("#buyerName").val(memberName);
		$("#buyerPhone").val(memberPhone);
		$("#add1 option[value="+add01+"]").prop('selected',true);
// 		setTimeout(function(){
		$("#add2 option[value="+add02+"]").prop('selected',true);
// 	     }, 100);
// 		$("#add1").selected(add01);
// 		$("#add2").val(add02);
		$("#add4").val(add03);
	}else{
	}
})
	
</script>			
	</body>
</html>