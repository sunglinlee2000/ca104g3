<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page import="com.product.model.*"%>
<%@ page import="com.notificationList.model.*"%>
<%@ page import="com.member.model.*"%>
<%@ page import="java.util.*"%>

<%

//	會員
MemberVO memberVO = (MemberVO) session.getAttribute("memberVO");
String memberID = memberVO.getMemberID();

//	通知
NotificationListService noSvc = new NotificationListService();
List<NotificationListVO> nolists = noSvc.getAllNoList(memberID);
pageContext.setAttribute("nolists",nolists);

ProductVO productVO = (ProductVO) request.getAttribute("productVO");
%>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>update_product_input.jsp</title>
	<meta charset="UTF-8">
	<meta name="description" content="Food Blog Web Template">
	<meta name="keywords" content="food, unica, creative, html">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- Favicon -->   
	<link href="<%=request.getContextPath()%>/front-end/img/favicon.ico" rel="shortcut icon"/>

	<!-- Google Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Poppins:400,400i,500,500i,600,600i,700" rel="stylesheet">

	<!-- Stylesheets -->
	<link rel="stylesheet" href="<%=request.getContextPath()%>/front-end/css/bootstrap.min.css"/>
	<link rel="stylesheet" href="<%=request.getContextPath()%>/front-end/css/font-awesome.min.css"/>
	<link rel="stylesheet" href="<%=request.getContextPath()%>/front-end/css/owl.carousel.css"/>
	<link rel="stylesheet" href="<%=request.getContextPath()%>/front-end/css/animate.css"/>
	<link rel="stylesheet" href="<%=request.getContextPath()%>/front-end/css/style.css"/>
	<link rel="stylesheet" href="<%=request.getContextPath()%>/front-end/member/css/styleLogin.css">
	<script src="<%=request.getContextPath() %>/front-end/js/owl.carousel.min.js"></script>
	<script src="https://code.jquery.com/jquery.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/adfund/css/bootstrap-theme.min.css"> 
	<script src="<%=request.getContextPath() %>/front-end/adfund/js/bootstrap.min.js"></script>
	<style>
	body {
 		 font-family: \5FAE\8EDF\6B63\9ED1\9AD4, \65B0\7D30\660E\9AD4;
 	}
	.ggyy{
	width: 600px;
	border: 2px solid gray;
	border-radius: 8px;
	padding: 10px;
	background-color: #d2e9ff;
	margin:5px
	}
	
	.kk-no-list{
	   height:auto;
	   border:1px solid;
	   border-color: #E3E0E0;
	   background-color: #E3E0E0;
	   margin: 2px;
	   padding: 2px;
	   font-size:15px;
	}
	
	.kk-modal-body{
	   height:300px;
	   overflow-y: scroll;
	   overflow-x: hidden;
	
	}
		
	</style>
</head>

		<body>
		
			<!-- Header section --> 
 <header class="header-section">	
  <div class="header-bottom"> 
   <div class="container"> 
    <a href="<%=request.getContextPath()%>/front-end/index.jsp" class="site-logo"> 
     <img src="<%=request.getContextPath() %>/front-end/img/logo-1.png" alt=""> 
    </a> 
    <div class="nav-switch"> 
     <i class="fa fa-bars"></i> 
    </div> 

    <ul class="main-menu"> 
     <li><a href="<%= request.getContextPath()%>/front-end/board/listAllBoard.jsp"><img alt="藝人專板"  title="藝人專板" src="<%= request.getContextPath()%>/front-end/res/img/index/board.png" style="height:30px;width:30px"></a></li> 
     <li><a href="<%= request.getContextPath()%>/front-end/product/Mall.jsp"><img alt="商城"  title="商城" src="<%= request.getContextPath()%>/front-end/res/img/index/mall.png" style="height:30px;width:30px"></a></li> 
     <li><a href="<%=request.getContextPath()%>/front-end/gif_collection/gif_list.jsp"><img alt="GIF"  title="GIF" src="<%= request.getContextPath()%>/front-end/res/img/index/gif.png" style="height:30px;width:30px"></a></li> 
     <li><a href="<%=request.getContextPath()%>/front-end/member/listOneMember.jsp"><img alt="會員資料" title="會員資料" src="<%= request.getContextPath()%>/front-end/res/img/index/user.png" style="height:30px;width:30px"></a></li> 
     <li><a href="#" onclick="logout()"><img alt="登出" title="登出" src="<%= request.getContextPath()%>/front-end/res/img/index/logout.png" style="height:30px;width:30px"></a> 
     <li><a href="<%=request.getContextPath()%>/front-end/chatBox/chatBox.jsp"><img alt="聊天室" title="聊天室" src="<%= request.getContextPath()%>/front-end/res/img/index/chat.png" style="height:30px;width:30px"></a></li> 
     <li>${memberVO.memberNickName }</li>
     <li><a href="<%=request.getContextPath()%>/front-end/personalPage/myPersonalPage.jsp"><img src="<%= request.getContextPath()%>/front-end/member/memberImg.do?memberID=${memberVO.memberID}" id="memberPhoto"></a></li>	
     <li id="notifi"><a href="#modal-id-notifi" data-toggle="modal"> 
      <div style="position: relative;"> 
      <img src="<%=request.getContextPath()%>/front-end/res/img/index/notification(32).png" alt="通知" id="notifiPhoto"> 
      <span style="position: absolute; top: 0; left: 17px; color: white" id="notifiCount"></span> 
      </div> 
     </a></li> 
     </ul>
   </div> 
  </div> 
 </header> 
 <!-- Header section end -->
<FORM METHOD="post" ACTION="<%=request.getContextPath()%>/front-end/member/member.do" id="logout">
	<input type="hidden" name="action"	value="logOut">
</FORM>
<script type="text/javascript">
	function logout(){
		document.getElementById("logout").submit();
	}
</script>
		
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-sm-offset-3">
				
				<FORM METHOD="post" ACTION="<%=request.getContextPath()%>/product/product.do" name="form1" class=" ggyy bootstrap-frm"
					enctype="multipart/form-data">
					
						<center>
							<h1>商品資料修改</h1>
							<hr sytle="background-color:black">
						</center>
								<c:if test="${not empty errorMsgs}">
								<font style="color: red">請修正以下錯誤:</font>
								<ul>
									<c:forEach var="message" items="${errorMsgs}">
									<li style="color: red">${message}</li>
									</c:forEach>
								</ul>
								</c:if>
								
						
							<div class="form-group"><lable>商品編號:</lable><%=productVO.getProductID()%></div>
						
						
							<div class="form-group">
								<lable>會員編號:</lable><%=productVO.getMemberID()%>
								<input type="hidden"  class="form-control"name="memberID" size="45" value="<%=productVO.getMemberID()%>" />
							</div>
							<script>
								$(document).ready(function() {
				// 					$('#productStatus option[value=${productVO.productStatus}]').attr('selected',true);
				// 					$('#artistID option[value=ART000032').attr('selected',true);
									$('#productStatus').val('<%=productVO.getProductStatus()%>');
									$('#artistID').val('<%=productVO.getArtistID()%>');
// 									document.getElementById('artistID').value = "ART000038";
									
								});
							</script>
							<div class="form-group">
								<label>所屬分類</label> 
								<select name="artistID" class="form-control" id="artistID">
									<option value="0">請選擇</option>
									<option value="ART000032">五月天</option>
									<option value="ART000038">蘇打綠</option>
									<option value="ART000001">SEVENTEEN</option>
									<option value="ART000015">TWICE</option>
									<option value="ART000025">新垣結衣</option>
									<option value="ART000026">Maroon5</option>
								</select>
							</div>
						
							<div class="form-group">
								<lable>產品照片欲改成:</lable>
								
								<input id="file" class="filepath" onchange="changepic(this)" type="file" name="productPhoto"><img src="" id="show" width="200">
							</div>
							
							
								<div class="form-group">
									<lable>產品資訊:</lable><textarea class="form-control" rows="5" name="productInfo"　placeholder="請輸入商品描述"
									><%=productVO.getProductInfo()%></textarea>
								</div>
							
							
								<div class="form-group">
									<lable>產品數量:</lable><input type="TEXT" class="form-control" name="productAmount" size="45"
									value="<%=productVO.getProductAmount()%>" />
								</div>
							
							
							
								<div class="form-group">
									<lable>產品價格:</lable><input type="TEXT" class="form-control" name="productPrice" size="45"
									value="<%=productVO.getProductPrice()%>" />
								</div>
							
							
								<div class="form-group">
									<lable>產品名稱:</lable><input type="TEXT" class="form-control" name="productName" size="45"
									value="<%=productVO.getProductName()%>" />
								</div>
							
							
								
								 <div class="form-group">
								 	<lable>產品狀態:</lable>
								 			<select name=productStatus id=productStatus class="form-control">
		 										<option value="PRODUCT_ON">上架</option>
		 										<option value="PRODUCT_OFF">下架</option>
		 										<option value="PRODUCT_OUT">缺貨</option>
		 									</select>
								 </div>
							
								<br> 
							 <input type="hidden" class="form-control" name="action" value="update"> 
							 <input	type="hidden" name="productID" value="<%=productVO.getProductID()%>">
							 <input	type="hidden" name="requestURL" value="<%=request.getParameter("requestURL")%>">
							 <input	type="hidden" name="whichPage"  value="<%=request.getParameter("whichPage")%>">
							 
						<center><input type="submit" class="btn btn-info" value="送出修改"></center>
					</FORM>
					</div>
				</div>
			</div>
				<script>
				//可以預覽File的圖片
					function changepic(obj) {
						//console.log(obj.files[0]);//这里可以获取上传文件的name
						var newsrc = getObjectURL(obj.files[0]);
						document.getElementById('show').src = newsrc;
					}
					//建立一個可存取到該file的url
					function getObjectURL(file) {
						var url = null;
						// 下面函数执行的效果是一样的，只是需要针对不同的浏览器执行不同的 js 函数而已
						if (window.createObjectURL != undefined) { // basic
							url = window.createObjectURL(file);
						} else if (window.URL != undefined) { // mozilla(firefox)
							url = window.URL.createObjectURL(file);
						} else if (window.webkitURL != undefined) { // webkit or chrome
							url = window.webkitURL.createObjectURL(file);
						}
						return url;
					}
				</script>
				
		<!-- 通知列表的跳窗 -->
		<div class="modal fade" id="modal-id-notifi">
			<div class="modal-dialog modal-sm">
			   <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<div class="modal-content">
				   
					<div class="modal-header">					
						<h4 class="modal-title">通知列表</h4>
					</div>
					<div class="modal-body kk-modal-body" >

                    <!-- 及時通知顯示的地方 -->
					<div id="nolistsssss" class="kk-no-list">
					  <a href="<%=request.getContextPath()%>/adfundTest/adfund.do?action=getOne_For_Display&adfundID=${adfundVO.adfundID}" ></a>
					 </div>
                        
						<c:forEach var="NotificationListVO" items="${nolists}" >
					      <div class="kk-no-list">					      
					         <a href="<%=request.getContextPath()%>${NotificationListVO.notificationpath}${NotificationListVO.notificationtarget}" id="${NotificationListVO.notificationid}">${NotificationListVO.notificationcontent}</a>
					         <h6><fmt:formatDate value="${NotificationListVO.notificationdate}" pattern="yyyy-MM-dd HH:mm"/></h6>
					      </div>
					    </c:forEach>
					</div>
					<div class="modal-footer">

					</div>
				</div>
			</div>
		</div>
	<!-- 通知的跳窗 -->

<script>

$(document).ready(function() { 
	
	//點擊鈴鐺時，通知要全部變為已讀
    $('#notifi').click(function(){
    	
//              alert("!!")
				$.ajax({ 
				     url : '<%=request.getContextPath()%>/front-end/NotificationListServlet/NotificationListServlet.do', 
				     type : 'Post', 
				     data : { 
				      action : 'isClick', 
				      memberID : '${memberVO.memberID}' 

				     } 
				    }).done(function(res){
				    	var obj = JSON.parse(res);
				    	document.getElementById("notifiPhoto").src = "<%=request.getContextPath()%>/front-end/res/img/index/notification(32).png";
				    	$('#notifiCount').html("");
	                });				     
     });
 
	
//    alert("noti!!");
    //網頁一打開時，要立刻判斷是否有未讀訊息，看是否要顯示有消息的鈴鐺
    $.ajax({ 
	     url : '<%=request.getContextPath()%>/front-end/NotificationListServlet/NotificationListServlet.do', 
	     type : 'Post', 
	     data : { 
	      action : 'isRead',  
	      memberID : '${memberVO.memberID}'
	     } 
	    }).done(function(res){
	    	var obj = JSON.parse(res);
	    	if(obj.notiStatus==false){
	    	   document.getElementById("notifiPhoto").src = "<%=request.getContextPath()%>/front-end/res/img/index/notification(32)red.png";
	    	   $('#notifiCount').html(obj.falseCount);
	    	}
	    });
    
});
</script>				
		</body>
</html>