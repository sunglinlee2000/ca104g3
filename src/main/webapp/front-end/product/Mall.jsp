<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page import="java.util.*"%>
<%@ page import="java.io.*"%>
<%@ page import="javax.servlet.*"%>
<%@ page import="javax.servlet.http.*"%>
<%@ page import="com.product.model.*"%>
<%@ page import="com.member.model.*" %>
<%@ page import="com.notificationList.model.*"%>
<%-- 此頁練習採用 EL 的寫法取值 --%>

    <%
    
	//	會員
	MemberVO memberVO = (MemberVO) session.getAttribute("memberVO");
	String memberID = memberVO.getMemberID();
	
	//	通知
	NotificationListService noSvc = new NotificationListService();
	List<NotificationListVO> nolists = noSvc.getAllNoList(memberID);
	pageContext.setAttribute("nolists",nolists);
    
    ProductService productSvc = new ProductService();
    List<ProductVO> list = productSvc.getAll();
    pageContext.setAttribute("list",list);
    %>
<!DOCTYPE html>
<html lang="en">
<head>
	<title>Mall.jsp</title>
	<meta charset="UTF-8">
	<meta name="description" content="Food Blog Web Template">
	<meta name="keywords" content="food, unica, creative, html">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- Favicon -->   
	<link href="<%=request.getContextPath()%>/front-end/img/favicon.ico" rel="shortcut icon"/>

	<!-- Google Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Poppins:400,400i,500,500i,600,600i,700" rel="stylesheet">

	<!-- Stylesheets -->
	<link rel="stylesheet" href="<%=request.getContextPath()%>/front-end/css/bootstrap.min.css"/>
	<link rel="stylesheet" href="<%=request.getContextPath()%>/front-end/css/font-awesome.min.css"/>
	<link rel="stylesheet" href="<%=request.getContextPath()%>/front-end/css/owl.carousel.css"/>
	<link rel="stylesheet" href="<%=request.getContextPath()%>/front-end/css/animate.css"/>
	<link rel="stylesheet" href="<%=request.getContextPath()%>/front-end/css/style.css"/>
	<link rel="stylesheet" href="<%=request.getContextPath()%>/front-end/member/css/styleLogin.css">
	<script src="<%=request.getContextPath() %>/front-end/js/owl.carousel.min.js"></script>
	<script src="https://code.jquery.com/jquery.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
<!-- 	<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script> -->
	<script src="<%=request.getContextPath() %>/front-end/product/js/sweetalert2.all.min.js"></script>
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/adfund/css/bootstrap-theme.min.css">
	

	<!--[if lt IE 9]>
	  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	<style>
	body {
 		 font-family: \5FAE\8EDF\6B63\9ED1\9AD4, \65B0\7D30\660E\9AD4;
 	}
	.product{
	background-color:#ffffff;
	width:100%;
	height:300px;
	}
	@media screen and (min-width:768px){
		.item{
			width: 30%;
			flex-grow: 1;
			width: 200px;
	    	height: 330px;
	    	border: 1px lightgray;
		    border-radius: 15px;
		    border-style: solid;
		}
	}
	.productitem{
	padding:10px;
	}
	pic{overflow:hidden;}
	.pic img{transform:scale(1,1);transition: all 1s ease-out;}
	.pic img:hover{transform:scale(1.2,1.2);}
	.wrap{
		width: 100%;
		border-style: 1px slide lightgray;
		padding: 10px;
		display: flex;
		flex-wrap: wrap;
		margin: auto;
		box-sizing: border-box;
	}
	#memberPhoto {
    border-radius: 50%;
    height: 30px;
    width: 30px;
	}
	
/* 	hoverCSS */
	.container_for_hover {
    position:relative;
    width:60px;
    height:60px;
    display:inline-block;
	}
	
	
	.animate_block {
	    -webkit-transition: all 0.3s ease-in-out;
	    -moz-transition: all 0.3s ease-in-out;
	    -o-transition: all 0.3s ease-in-out;
	    -ms-transition: all 0.3s ease-in-out;
	    transition: all 0.3s ease-in-out;
	}
	
	
	.overlay {
	    position:absolute;
	    transition:all .2s ease;
	    opacity:0;
	    transition:0.5s;
	    background: #00000080;
	}
	.container_for_hover:hover .overlay {
	    opacity:1;
	}
	.hover_text {
	    color:white;
	    font-family: Microsoft JhengHei , sans-serif;
	    position:absolute;
	    top:50%;
	    left:50%;
	    transform:translate(-50%,-50%);
	    font-size: 16px;
	    font-weight: bold;
	    text-align:center;
	}
	.overlayTop {
	    width:100%;
	    height:0;
	    top:0;
	    background: #00000080;
	}
	.overlayBottom {
	    width:100%;
	    height:0;
	    bottom:0;
	    background: #00000080;
	}
	.container_for_hover:hover .overlayTop, .container_for_hover:hover .overlayBottom {
	    height:100%;
	}
	.overlayLeft {
	    height:100%;
	    width:0;
	    top:0;
	    background: #00000080;
	}
	.overlayRight {
	    height:100%;
	    width:0;
	    top:0;
	    left:0px;
	    background: #00000080;
	}
	.container_for_hover:hover .overlayLeft, .container_for_hover:hover .overlayRight {
	    width:100%;
	}
	
	.overlayCross {
	    width:0;
	    height:0;
	    top:0;
	    background: #00000080;
	}
	.container_for_hover:hover .overlayCross {
	    height:100%;
	    width:100%;
	}
	
	.overlayFade {
	    height:100%;
	    width:100%;
	    top:0;
	    background: #00000080;
	}
	
	.index_img{
	    width: 60px;
	    height: 60px;
	    text-align: center;
	    vertical-align: middle;
	
	    transition: transform .75s; /* Animation */
	    margin: 0 auto;
	
	}
	
	.kk-no-list{
	   height:auto;
	   border:1px solid;
	   border-color: #E3E0E0;
	   background-color: #E3E0E0;
	   margin: 2px;
	   padding: 2px;
	   font-size:15px;
	}
	
	.kk-modal-body{
	   height:300px;
	   overflow-y: scroll;
	   overflow-x: hidden;
	
	}
	#sweetalert{
		color:white;
	}
		
/* 	加減數量CSS */
	
	
	</style>
</head>
<body onload="connect();" onunload="disconnect();">


		
			<!-- Header section --> 
 <header class="header-section">	
  <div class="header-bottom"> 
   <div class="container"> 
    <a href="<%=request.getContextPath()%>/front-end/index.jsp" class="site-logo"> 
     <img src="<%=request.getContextPath() %>/front-end/img/logo-1.png" alt=""> 
    </a> 
    <div class="nav-switch"> 
     <i class="fa fa-bars"></i> 
    </div> 

    <ul class="main-menu"> 
     <li><a href="<%= request.getContextPath()%>/front-end/board/listAllBoard.jsp"><img alt="藝人專板"  title="藝人專板" src="<%= request.getContextPath()%>/front-end/res/img/index/board.png" style="height:30px;width:30px"></a></li> 
     <li><a href="<%= request.getContextPath()%>/front-end/product/Mall.jsp"><img alt="商城"  title="商城" src="<%= request.getContextPath()%>/front-end/res/img/index/mall.png" style="height:30px;width:30px"></a></li> 
     <li><a href="<%=request.getContextPath()%>/front-end/gif_collection/gif_list.jsp"><img alt="GIF"  title="GIF" src="<%= request.getContextPath()%>/front-end/res/img/index/gif.png" style="height:30px;width:30px"></a></li> 
     <li><a href="<%=request.getContextPath()%>/front-end/member/listOneMember.jsp"><img alt="會員資料" title="會員資料" src="<%= request.getContextPath()%>/front-end/res/img/index/user.png" style="height:30px;width:30px"></a></li> 
     <li><a href="#" onclick="logout()"><img alt="登出" title="登出" src="<%= request.getContextPath()%>/front-end/res/img/index/logout.png" style="height:30px;width:30px"></a> 
     <li><a href="<%=request.getContextPath()%>/front-end/chatBox/chatBox.jsp"><img alt="聊天室" title="聊天室" src="<%= request.getContextPath()%>/front-end/res/img/index/chat.png" style="height:30px;width:30px"></a></li> 
     <li>${memberVO.memberNickName }</li>
     <li><a href="<%=request.getContextPath()%>/front-end/personalPage/myPersonalPage.jsp"><img src="<%= request.getContextPath()%>/front-end/member/memberImg.do?memberID=${memberVO.memberID}" id="memberPhoto"></a></li>	
     <li id="notifi"><a href="#modal-id-notifi" data-toggle="modal"> 
      <div style="position: relative;"> 
      <img src="<%=request.getContextPath()%>/front-end/res/img/index/notification(32).png" alt="通知" id="notifiPhoto"> 
      <span style="position: absolute; top: 0; left: 17px; color: white" id="notifiCount"></span> 
      </div> 
     </a></li> 
     </ul>
   </div> 
  </div> 
 </header> 
 <!-- Header section end -->
<FORM METHOD="post" ACTION="<%=request.getContextPath()%>/front-end/member/member.do" id="logout">
	<input type="hidden" name="action"	value="logOut">
</FORM>
<script type="text/javascript">
	function logout(){
		document.getElementById("logout").submit();
	}
</script>


<FORM METHOD="post" ACTION="<%=request.getContextPath()%>/front-end/member/member.do" id="logout">
	<input type="hidden" name="action"	value="logOut">
</FORM>
<script type="text/javascript">
	function logout(){
		document.getElementById("logout").submit();
	}
</script>


	<!-- Search section -->
	<div class="search-form-section">
		<div class="sf-warp">
			<div class="container">
				<form method="post" class="big-search-form" action="<%=request.getContextPath()%>/product/product.do">
					
						<select name="artistID">
							<option value="">全部</option>
							<option value="ART000032">五月天</option>
							<option value="ART000038">蘇打綠</option>
							<option value="ART000001">SEVENTEEN</option>
							<option value="ART000015">TWICE</option>
							<option value="ART000025">新垣結衣</option>
							<option value="ART000026">Maroon5</option>
						</select>
					
					
						<input type="text" name="productName" placeholder="依關鍵字搜尋">
						<button type="submit "class="bsf-btn">Search</button>
						<input type="hidden" name="action" value="Mall_ByCompositeQuery">
						<a href="<%=request.getContextPath()%>/front-end/product/Seller.jsp" data-toggle="tooltip" title="賣家中心" id="store">
							<div class="container_for_hover animate_block" style="margin-left: 15px;">
							<img src="<%=request.getContextPath()%>/front-end/res/img/mall/001-auction.png" class="index_img" >
							<div class="overlay overlayRight"><div class="hover_text">賣東西</div></div>
							</div>
						</a>
					    <a href="<%=request.getContextPath()%>/front-end/orderList/buyerOrder.jsp" data-toggle="tooltip" title="賣家中心" id="store">
							<div class="container_for_hover animate_block" style="margin-left: 15px;">
							<img src="<%=request.getContextPath()%>/front-end/res/img/mall/003-buy.png" title="買家中心" class="index_img" >
							<div class="overlay overlayRight"><div class="hover_text">買家</div></div>
							</div>
						</a>
						<a href="<%=request.getContextPath()%>/front-end/product/Cart.jsp" data-toggle="tooltip" title="購物車">
							<div class="container_for_hover animate_block" style="margin-left: 15px;">	
							<img src="<%=request.getContextPath()%>/front-end/res/img/mall/004-fast-delivery.png"  class="index_img" >
							<div class="overlay overlayRight"><div class="hover_text">購物車 </div></div>
							</div>
						</a>
				</form>
			</div>
		</div>
	</div>
	
	<!-- Search section end -->
		<div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12">
					<div style="font-size:22px;background-color:white;">       
	            		<marquee scrollamount="12">
	            			<c:forEach var="productVO" items="${list}">
	            				<img src="<%=request.getContextPath()%>/front-end/res/gif/mall/run.gif" style="width:40px;height:40px;">
	            		     	<img src="<%=request.getContextPath()%>/front-end/res/img/mall/hawhy.jpg" style="width:30px;height:30px;"><b>${productVO.productName}目前特價<span style="color:red;">NT${productVO.productPrice}</span>還不買在等甚麼!<b>
							</c:forEach>          		
	            		</marquee>
	            	</div>
            	</div>
          	</div>
        </div>

	<!-- Recipes section -->
	
		<div class="container">
			<div class="row">
				<div class="col-md-8">
					<div class="section-title">
						<h2>Latest Product</h2>
					</div>
				</div>
				<div class="col-md-4">
				</div>
			</div>
			<div class="row">
				<div class="container">
		            <div class="row">
		                <div class="col-xs-12 col-sm-3">
		                    <div class="panel panel-default">
		                        <div class="panel-heading">
		                            <h3 class="panel-title">條件搜尋</h3>
		                        </div>
		                        <ul class="list-group">
		                            <a href="<%=request.getContextPath()%>/product/product.do?artistID=ART000032&action=Mall_ByCompositeQuery"><li class="list-group-item">五月天</li></a>
		                            <a href="<%=request.getContextPath()%>/product/product.do?artistID=ART000038&action=Mall_ByCompositeQuery"><li class="list-group-item">蘇打綠</li></a>
		                            <a href="<%=request.getContextPath()%>/product/product.do?artistID=ART000001&action=Mall_ByCompositeQuery"><li class="list-group-item">SEVENTEEN</li></a>
		                            <a href="<%=request.getContextPath()%>/product/product.do?artistID=ART000015&action=Mall_ByCompositeQuery"><li class="list-group-item">TWICE</li></a>
		                            <a href="<%=request.getContextPath()%>/product/product.do?artistID=ART000025&action=Mall_ByCompositeQuery"><li class="list-group-item">新垣結衣</li></a>
		                            <a href="<%=request.getContextPath()%>/product/product.do?artistID=ART000026&action=Mall_ByCompositeQuery"><li class="list-group-item">Maroon5</li></a>
		                        </ul>
		                    </div>
		                </div>  
		                <div class="col-xs-12 col-sm-9">
		
		                <center><%@ include file="/front-end/product/pages/page1.file" %></center>
		                <br>
		                <div class="container-fluid">
		            	<div class="row">
		                <c:forEach var="productVO" items="${list}" begin="<%=pageIndex%>" end="<%=pageIndex+rowsPerPage-1%>">
		                    <div class="col-xs-12 col-sm-4 productitem">
			                        <div class="wrap item product">
			                        
				                        <div class="pic" style="width:100%; height:370px;">
					                                 
					                     	<div style="text-align:center;">                     	
					                           	<button type="button"  data-toggle="modal" data-target="#modalQuickView${productVO.productID}" id="b${productVO.productID}" style="background-color:white;border: none;">
					                           		<img src="<%=request.getContextPath()%>/product/productImg.do?productID=${productVO.productID}" style="width:150px; height:150px;">
					                           	</button>
					                           		
				                           	</div>
				                        
				                           	<div id="productdesc" style="text-align:left;font-size:19px;margin-top:10px">
				                           		${productVO.productName}
				                           	</div>
				                           	
				                            <div style="color:red;font-size:19px";>
				                            	NT${productVO.productPrice}
				                            </div> 
				                             
				                             
				                             <div style="display:inline-flex;position:absolute;bottom:15px;left:25%">
                        	                      <c:if test="${memberVO.memberID==productVO.memberID}">         
				                               			<script>
				                               				$(document).ready(function() {
				                               					$('#myBtn${productVO.productID}').prop("disabled", true);
				                               					$('#add${productVO.productID}').prop("disabled", true);
				                               					$('#add2${productVO.productID}').prop("disabled", true);
				                               				});
				                               			</script>
				                          			
				                                  </c:if>											
												      <input type="button" class="btn btn-sm btn-warning " value="加入購物車" id="add${productVO.productID}" style="margin-right:15px">
				                                	 <input type="submit" class="btn btn-sm btn-danger" id="myBtn${productVO.productID}" value="檢舉">
											</div>
										</div>
			                        
			                        </div>
		                    </div>
		                    <div class="modal fade" id="modalQuickView${productVO.productID}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
								  <div class="modal-dialog modal-lg" role="document">
								    <div class="modal-content" style="background-color:#fbfffd">
								      <div class="modal-body">
								        <div class="row">
								          <div class="col-xs-12 col-sm-5">
								            <div id="carousel-thumb" class="carousel slide carousel-fade carousel-thumbnails" data-ride="carousel">
								             
								              <div class="carousel-inner" role="listbox">
								                <div class="carousel-item active">
								                  <img class="d-block w-100" src="<%=request.getContextPath()%>/product/productImg.do?productID=${productVO.productID}"
								                    alt="First slide" style="width:450px;height:400px;">
								                </div>
								              </div>
								            </div>
								          </div>
								         <div class="col-xs-12 col-sm-7">
								          <div>
								            <h2 class="h2-responsive product-name">
								              <strong>${productVO.productName}</strong>
								            </h2>
								          </div>
								          <div style="padding-top: 20px;">
								            <h4>
								              <span>
								                <strong>商品詳情:</strong>
								              </span>
								            </h4>
								          </div>
								          <div style="padding-top: 15px;">
								          	<p>
								          		${productVO.productInfo}
								          	</p>
								          </div>
								          <div style="padding-top: 20px;">
								            <h4>
								              <span style="color:red;font-size: 50px;padding-left: 15px;">
								                <strong>$${productVO.productPrice}</strong>
								              </span>
								            </h4>
								          </div>
								          <div style="padding-top: 20px;">
								          <div>
								            <div class="col-lg-2" style="width:465px">
								            	<table>
								            		<tr>
								            			<td>
										                    <div class="input-group">
										                       <span class="input-group-btn">
										                           <button type="button" class="quantity-left-minus${productVO.productID} btn btn-default btn-number"  data-type="minus" data-field="">
										                             <span class="glyphicon glyphicon-minus"></span>
										                           </button>
										                       </span>
										                       <input type="text" id="quantity${productVO.productID}" name="quantity${productVO.productID}" class="form-control input-number" value="1" min="1" max="100">
										                       <span class="input-group-btn">
										                           <button type="button" class="quantity-right-plus${productVO.productID} btn btn-default btn-number" data-type="plus" data-field="">
										                               <span class="glyphicon glyphicon-plus"></span>
										                           </button>
										                       </span>
										                   </div>
										               </td>
										               <td style="padding-left: 60px;">
										                   <div>
										                   		<button class="btn btn-warning btn-lg" id="add2${productVO.productID}">加入購物車</button>
										                   </div>
										               </td> 
										               <td style="padding-left: 60px;">
										                   <div>
										                   		<button class="btn btn-danger btn-lg" id="exit${productVO.productID}">離開</button>
										                   </div>
										               </td>   
									               </tr>
								               </table>
								               </div>
								           </div>
								          </div>
								        </div>
								      </div>
								    </div>
								  </div>
								</div>
							</div>
						<!-- 檢舉modal -->
							<div class="modal fade" id="myModal${productVO.productID}" role="dialog">
							    <div class="modal-dialog" align="center">
							    
							      <!-- Modal content-->
							      <div class="modal-content" style="width: 400px;" >
							        <div class="modal-header">
							          <button type="button" class="close" data-dismiss="modal">&times;</button>
							          
							          <center>
							          	<h4 class="modal-title">商品檢舉</h4>
							          </center>
							        </div>
							        <div class="modal-body" style="text-align: left; width: 250px;">
							        	
							        		<input type="radio" name="productReportReason" value="IMAGE_WRONG"> 圖文不符<br>
							        		
										      <input type="radio" name="productReportReason" value="ARTIST_WRONG">非明星相關商品 <br>							        		
							        		
										      <input type="radio" name="productReportReason" value="SEX"> 含有色情暴力成分 <br>
							        </div>
							        <div align="center">
							        	<button class="btn btn-default" id="report${productVO.productID}">送出
							        	</button>
							        </div>
							        
							      </div>
							      
							    </div>
							 </div>
							<script>
							$(document).ready(function() { 
						    	  $('#add${productVO.productID}').click(function(){
						    		  var quantity=1;
										$.ajax({ 
										     url : '<%=request.getContextPath()%>/product/product.do', 
										     type : 'Post', 
										     data : { 
										      action : 'ADD', 
										      memberID : '${memberVO.memberID}',
										      productID:'${productVO.productID}',
										      productName:'${productVO.productName}',
										      quantity:quantity,
										      productPrice:'${productVO.productPrice}'
										     } 
										    }).done(function(res){
										    	var obj = JSON.parse(res);
										    	swal({
										    		  title: "加入購物車成功!",
										    		  icon: "success",
										    		  button: "確定",
										    		});
										    });
									});
						    	  $('#add2${productVO.productID}').click(function(){
						    		  var quantity=$('#quantity${productVO.productID}').val();
										$.ajax({ 
										     url : '<%=request.getContextPath()%>/product/product.do', 
										     type : 'Post', 
										     data : { 
										      action : 'ADD', 
										      memberID : '${memberVO.memberID}',
										      productID:'${productVO.productID}',
										      productName:'${productVO.productName}',
										      quantity:quantity,
										      productPrice:'${productVO.productPrice}'
										     } 
										    }).done(function(res){
										    	var obj = JSON.parse(res);
										    	swal({
										    		  title: "加入購物車成功!",
										    		  icon: "success",
										    		  button: "確定",
										    		});
										    });
									});
						    	  $('#report${productVO.productID}').click(function(){
								    		  $('#myModal${productVO.productID}').modal('hide');
												$.ajax({ 
												     url : '<%=request.getContextPath()%>/productReport/productReport.do', 
												     type : 'Post', 
												     data : { 
												      action : 'insert', 
												      memberID :'${memberVO.memberID}',
												      productID:'${productVO.productID}',
												      productReportReson:$('#myModal${productVO.productID} input[name="productReportReason"]:checked').val()
												     } 
												    }).done(function(res){
												    	var obj = JSON.parse(res);
												    	swal({
												    		  title: "檢舉成功!",
												    		  icon: "success",
												    		  button: "確定"
												    		});
												    });
									});
						    	  
						    	  $('#exit${productVO.productID}').click(function(){
									  $('#modalQuickView${productVO.productID}').modal('hide');
								  });
						    	  $("#myBtn${productVO.productID}").click(function(){
						    	        $("#myModal${productVO.productID}").modal();
						    	    });
						      });
							
								$('#b${productVO.productID}').click(function(e){
								      $('#quantity${productVO.productID}').val(1);
								 
								 });
								var quantitiy${productVO.productID}=0;
								$('#quantity${productVO.productID}').val(0);
								$('.quantity-right-plus${productVO.productID}').click(function(e){
	
								     e.preventDefault();
										
								     var quantity${productVO.productID} = parseInt($('#quantity${productVO.productID}').val());
								      
								         $('#quantity${productVO.productID}').val(quantity${productVO.productID} + 1);
								 
								 }); 
							
							  $('.quantity-left-minus${productVO.productID}').click(function(e){

							     e.preventDefault();
							  
							     var quantity${productVO.productID} = parseInt($('#quantity${productVO.productID}').val());
							      
							
							         if(quantity${productVO.productID}>0){
							         $('#quantity${productVO.productID}').val(quantity${productVO.productID} - 1);
							         }
							 });
							  
							  
							  
							</script>
		                </c:forEach>	
		                </div>
		            </div>
		            </div>
		            </div>
        </div>
        
       					    <div class="modal fade" id="PushmyModal" role="dialog">
							    <div class="modal-dialog" align="center">
							    
							      <!-- Modal content-->
							      <div class="modal-content">
							        <div class="modal-header">
							          <button type="button" class="close" data-dismiss="modal">&times;</button>
							          
							          <center>
							          	<h4 class="modal-title">最新商品</h4>
							          </center>
							        </div>
							        <div id="pushProductName">
							        	
							        </div>
							        <div id="pushProductPrice">
							        	
							        </div>
							        <div id="pushProductInfo">
							        	
							        </div>
							        <div align="center">
							        	<button class="btn btn-default" id="report${productVO.productID}">送出
							        	</button>
							        </div>
							        
							      </div>
							      
							    </div>
							 </div>
        
        
        
        
        
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-3"></div>
                <div class="col-xs-12 col-sm-9">
                    <center>
                        <%@ include file="/front-end/product/pages/page2.file" %>
                    </center>
                </div>
            </div>
        </div>
				
				
				
			</div>
		</div>

	<div class="gallery">
		<div class="gallery-slider owl-carousel">
			<c:forEach var="productVO" items="${list}">
				<div class="gs-item set-bg" data-setbg="<%=request.getContextPath()%>/product/productImg.do?productID=${productVO.productID}"></div>
			</c:forEach>
		</div>
	</div>
	<!-- Gallery section end -->


	<!-- Footer section  -->
	<footer class="footer-section set-bg" data-setbg="">
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-6">
					<div class="footer-logo">
						<img src="<%=request.getContextPath()%>/front-end/img/logo-1.png" alt="">
					</div>
				</div>
				<div class="col-lg-6 text-lg-right">
					
					<p class="copyright">
						電話： 03-4257387
						<br>
						地址： 320桃園市中壢區中大路300號
					</p>
				</div>
			</div>
		</div>
	</footer>
	<script>
		var MyPoint = "/MyProductServer";
	    var host = window.location.host;
	    var path = window.location.pathname;
	    var webCtx = path.substring(0, path.indexOf('/', 1));
	    var endPointURL = "wss://" + window.location.host + webCtx + MyPoint;
		var webSocket;
		
		function connect() {
			// 建立 websocket 物件
			webSocket = new WebSocket(endPointURL);
			
			webSocket.onopen = function(event) {
				console.log("連線成功!!!");
			};
	
			webSocket.onmessage = function(event) {
		        var jsonObj = JSON.parse(event.data);
		        var productID=jsonObj.productID;
		        var productName=jsonObj.productName;
		        var productPrice=jsonObj.productPrice;
		        var productInfo=jsonObj.productInfo;
		        var artistID=jsonObj.artistID;
		        
		        swal({
		        	  imageUrl:'<%=request.getContextPath()%>/product/productImg.do?productID='+productID+'',
		        	  imageWidth: 400,
		        	  imageHeight: 200,
		        	  html:'<h2 style="color:red">新品上市</h2><br>'+
		        		  	'<strong style="font-size:25px">'+productName+'</strong><br>',
		        	  animation: false,
		        	  customClass: 'animated flip',
		              confirmButtonText: '<a id="sweetalert" href="<%=request.getContextPath()%>/product/product.do?productName='+productName+'&action=Mall_ByCompositeQuery">馬上看~</a>'
		        	  
		        	})
		        
			};
	
			webSocket.onclose = function(event) {
				
			};
		}
		function disconnect () {
			webSocket.close();
			
		}
	
	
	</script>
	<script>
		$(document).ready(function() { 
				$.ajax({ 
				     url : '<%=request.getContextPath()%>/product/product.do', 
				     type : 'Post', 
				     data : { 
				      action : 'isBuyer', 
				      memberStatus : '${memberVO.memberStatus}' 
				     } 
				    }).done(function(res){
				    	var obj = JSON.parse(res);
				    	if(obj.status==true){
				    		swal({
			    		    	  title: "請先去會員中心註冊為買家!",
			    		    	  text: "3秒後為您跳轉到會員資料....",
			    		    	  icon: "warning",
			    		    	  button: "確定",
			    		    	});
					     document.addEventListener("click",handler,true);
					     function handler(e){
					         e.stopPropagation();
					         e.preventDefault();
					     }
					     setTimeout(function(){
					    	 window.location.href="<%=request.getContextPath()%>/front-end/member/listOneMember.jsp"; 
					     }, 3000);
				    	}
				     });
				$.ajax({ 
				     url : '<%=request.getContextPath()%>/product/product.do', 
				     type : 'Post', 
				     data : { 
				      action : 'isSeller', 
				      memberStatus : '${memberVO.memberStatus}' 
				     } 
				    }).done(function(res){
				    	var obj = JSON.parse(res);
				    	if(obj.status==true){
				    		$('#store').click(function(e) {
				    		    e.preventDefault();
				    		    swal({
				    		    	  title: "請先去會員中心註冊為賣家!",
				    		    	  icon: "warning",
				    		    	  button: "確定",
				    		    	});
				    		});
				    	}
				     });
				
				
				
		});
	</script>
	
		<!-- 通知列表的跳窗 -->
		<div class="modal fade" id="modal-id-notifi">
			<div class="modal-dialog modal-sm">
			   <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<div class="modal-content">
				   
					<div class="modal-header">					
						<h4 class="modal-title">通知列表</h4>
					</div>
					<div class="modal-body kk-modal-body" >

                    <!-- 及時通知顯示的地方 -->
					<div id="nolistsssss" class="kk-no-list">
					  <a href="<%=request.getContextPath()%>/adfundTest/adfund.do?action=getOne_For_Display&adfundID=${adfundVO.adfundID}" ></a>
					 </div>
                        
						<c:forEach var="NotificationListVO" items="${nolists}" >
					      <div class="kk-no-list">					      
					         <a href="<%=request.getContextPath()%>${NotificationListVO.notificationpath}${NotificationListVO.notificationtarget}" id="${NotificationListVO.notificationid}">${NotificationListVO.notificationcontent}</a>
					         <h6><fmt:formatDate value="${NotificationListVO.notificationdate}" pattern="yyyy-MM-dd HH:mm"/></h6>
					      </div>
					    </c:forEach>
					</div>
					<div class="modal-footer">

					</div>
				</div>
			</div>
		</div>
	<!-- 通知的跳窗 -->

<script>

$(document).ready(function() { 
	
	//點擊鈴鐺時，通知要全部變為已讀
    $('#notifi').click(function(){
    	
//              alert("!!")
				$.ajax({ 
				     url : '<%=request.getContextPath()%>/front-end/NotificationListServlet/NotificationListServlet.do', 
				     type : 'Post', 
				     data : { 
				      action : 'isClick', 
				      memberID : '${memberVO.memberID}' 

				     } 
				    }).done(function(res){
				    	var obj = JSON.parse(res);
				    	document.getElementById("notifiPhoto").src = "<%=request.getContextPath()%>/front-end/res/img/index/notification(32).png";
				    	$('#notifiCount').html("");
	                });				     
     });
 
	
//    alert("noti!!");
    //網頁一打開時，要立刻判斷是否有未讀訊息，看是否要顯示有消息的鈴鐺
    $.ajax({ 
	     url : '<%=request.getContextPath()%>/front-end/NotificationListServlet/NotificationListServlet.do', 
	     type : 'Post', 
	     data : { 
	      action : 'isRead',  
	      memberID : '${memberVO.memberID}'
	     } 
	    }).done(function(res){
	    	var obj = JSON.parse(res);
	    	if(obj.notiStatus==false){
	    	   document.getElementById("notifiPhoto").src = "<%=request.getContextPath()%>/front-end/res/img/index/notification(32)red.png";
	    	   $('#notifiCount').html(obj.falseCount);
	    	}
	    });
    
});
</script>
</body>
</html>