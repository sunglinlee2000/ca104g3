<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page import="java.util.*"%>
<%@ page import="com.product.model.*"%>
<%@ page import="com.orderList.model.*"%>
<%@ page import="com.member.model.*" %>
<%@ page import="com.notificationList.model.*"%>
<%-- 此頁練習採用 EL 的寫法取值 --%>
<%

	//	會員
	MemberVO memberVO = (MemberVO) session.getAttribute("memberVO");
	String memberID = memberVO.getMemberID();
	
	//	通知
	NotificationListService noSvc = new NotificationListService();
	List<NotificationListVO> nolists = noSvc.getAllNoList(memberID);
	pageContext.setAttribute("nolists",nolists);

	response.setHeader("Cache-Control","no-store");
	response.setHeader("Pragma","no-cache"); 
 	response.setDateHeader("Expires", 0); 
%> 
<%
	ProductService productSvc = new ProductService();
	List<ProductVO> list = productSvc.getAll(memberID);
	pageContext.setAttribute("list",list);
	
	OrderListService ordSvc=new OrderListService();
	List<OrderListVO> orderList=ordSvc.getAllSeller(memberID);
	pageContext.setAttribute("orderList",orderList);
%>
	
	<!DOCTYPE html>
	<html lang="en">
	<head>
	<title>Seller.jsp</title>
	<meta charset="UTF-8">
	<meta name="description" content="Food Blog Web Template">
	<meta name="keywords" content="food, unica, creative, html">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- Favicon -->   
	<link href="<%=request.getContextPath()%>/front-end/img/favicon.ico" rel="shortcut icon"/>

	<!-- Google Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Poppins:400,400i,500,500i,600,600i,700" rel="stylesheet">

	<!-- Stylesheets -->
	<link rel="stylesheet" href="<%=request.getContextPath()%>/front-end/css/bootstrap.min.css"/>
	<link rel="stylesheet" href="<%=request.getContextPath()%>/front-end/css/font-awesome.min.css"/>
	<link rel="stylesheet" href="<%=request.getContextPath()%>/front-end/css/owl.carousel.css"/>
	<link rel="stylesheet" href="<%=request.getContextPath()%>/front-end/css/animate.css"/>
	<link rel="stylesheet" href="<%=request.getContextPath()%>/front-end/css/style.css"/>
	
<!-- 原本	--- -->
	<link rel="stylesheet" href="<%=request.getContextPath()%>/front-end/product/css/bootstrap.min.css">
	<link rel="stylesheet" href="<%=request.getContextPath()%>/front-end/product/css/bootstrap-theme.min.css">
	
	<script src="<%=request.getContextPath()%>/front-end/product/js/jquery-3.3.1.min.js"></script>
	<!--         Latest compiled JavaScript -->
	<script src="<%=request.getContextPath()%>/front-end/product/js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="<%=request.getContextPath()%>/front-end/product/css/index.css">
	<link rel="stylesheet" href="<%=request.getContextPath()%>/front-end/product/css/seller.css">
	<link rel="stylesheet" href="<%=request.getContextPath()%>/front-end/member/css/styleLogin.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
	<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/adfund/css/bootstrap-theme.min.css"> 
	

	<!--[if lt IE 9]>
	  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
			<style>
			 		body  {
						font-family: \5FAE\8EDF\6B63\9ED1\9AD4, \65B0\7D30\660E\9AD4;
				   }
					
					.stab {
						color: black;
					}
					
					.sellerbg {
						background-color: lightblue;
					}
					
					.customers {
/* 					    border-collapse: collapse; */
					    width: 100%;
					  
					
						margin-top: 5px;
						margin-bottom: 5px;
						table-layout: fixed;
						word-break: break-all;
						border-collapse: separate;
						border-spacing: 0;
					}
					.aaa td {
						
						word-break: keep-all; /* 不换行 */
						white-space: nowrap; /* 不换行 */
						overflow: hidden; /* 内容超出宽度时隐藏超出部分的内容 */
						text-overflow: ellipsis;
					}
					
					.customers td, .customers th {
					    border: 1px solid #ddd;
					    padding: 8px;
					    text-align:center;
					    width:auto;
					    
					}
					
					
					.customers tr:nth-child(even){background-color: #f2f2f2;}
					
					.customers tr:hover {background-color: #ddd;}
					
					.customers th {
					    padding-top: 12px;
					    padding-bottom: 12px;
					    text-align: center;
					    background-color: #7dccff;
					    color: white;
					    white-space:nowrap;	
					}
					
					#memberPhoto {
				    border-radius: 50%;
				    height: 30px;
				    width: 30px;
					}	
					.checked {
    					color: orange;	
    				}
    				
    				.kk-no-list{
					   height:auto;
					   border:1px solid;
					   border-color: #E3E0E0;
					   background-color: #E3E0E0;
					   margin: 2px;
					   padding: 2px;
					   font-size:15px;
					}
					
					.kk-modal-body{
					   height:300px;
					   overflow-y: scroll;
					   overflow-x: hidden;
					
					}
			</style>
</head>
<body>

			 <!-- Header section --> 
 <header class="header-section">	
  <div class="header-bottom"> 
   <div class="container"> 
    <a href="<%=request.getContextPath()%>/front-end/index.jsp" class="site-logo"> 
     <img src="<%=request.getContextPath() %>/front-end/img/logo-1.png" alt=""> 
    </a> 
    <div class="nav-switch"> 
     <i class="fa fa-bars"></i> 
    </div> 

    <ul class="main-menu"> 
     <li><a href="<%= request.getContextPath()%>/front-end/board/listAllBoard.jsp"><img alt="藝人專板"  title="藝人專板" src="<%= request.getContextPath()%>/front-end/res/img/index/board.png" style="height:30px;width:30px"></a></li> 
     <li><a href="<%= request.getContextPath()%>/front-end/product/Mall.jsp"><img alt="商城"  title="商城" src="<%= request.getContextPath()%>/front-end/res/img/index/mall.png" style="height:30px;width:30px"></a></li> 
     <li><a href="<%=request.getContextPath()%>/front-end/gif_collection/gif_list.jsp"><img alt="GIF"  title="GIF" src="<%= request.getContextPath()%>/front-end/res/img/index/gif.png" style="height:30px;width:30px"></a></li> 
     <li><a href="<%=request.getContextPath()%>/front-end/member/listOneMember.jsp"><img alt="會員資料" title="會員資料" src="<%= request.getContextPath()%>/front-end/res/img/index/user.png" style="height:30px;width:30px"></a></li> 
     <li><a href="#" onclick="logout()"><img alt="登出" title="登出" src="<%= request.getContextPath()%>/front-end/res/img/index/logout.png" style="height:30px;width:30px"></a> 
     <li><a href="<%=request.getContextPath()%>/front-end/chatBox/chatBox.jsp"><img alt="聊天室" title="聊天室" src="<%= request.getContextPath()%>/front-end/res/img/index/chat.png" style="height:30px;width:30px"></a></li> 
     <li>${memberVO.memberNickName }</li>
     <li><a href="<%=request.getContextPath()%>/front-end/personalPage/myPersonalPage.jsp"><img src="<%= request.getContextPath()%>/front-end/member/memberImg.do?memberID=${memberVO.memberID}" id="memberPhoto"></a></li>	
     <li id="notifi"><a href="#modal-id-notifi" data-toggle="modal"> 
      <div style="position: relative;"> 
      <img src="<%=request.getContextPath()%>/front-end/res/img/index/notification(32).png" alt="通知" id="notifiPhoto"> 
      <span style="position: absolute; top: 0; left: 17px; color: white" id="notifiCount"></span> 
      </div> 
     </a></li> 
     </ul>
   </div> 
  </div> 
 </header> 
 <!-- Header section end -->
<FORM METHOD="post" ACTION="<%=request.getContextPath()%>/front-end/member/member.do" id="logout">
	<input type="hidden" name="action"	value="logOut">
</FORM>
<script type="text/javascript">
	function logout(){
		document.getElementById("logout").submit();
	}
</script>

			
			<div class="container-fluit">
				<div class="row">
					<div class="col-xs-12 col-sm-12 ">
						
						<center><h1>賣家中心</h1></center>
					</div>
				</div>
			</div>
			
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-12">
						
						<div role="tabpanel">
							<!-- 標籤面板：標籤區 -->
							<ul class="nav nav-tabs" role="tablist">
								<li role="presentation" class="active">
									<a href="#tab1" class="stab" aria-controls="tab1" role="tab" data-toggle="tab"><b>商品管理</b></a>
								</li>
								<li role="presentation">
									<a href="#tab2" class="stab" aria-controls="tab2" role="tab" data-toggle="tab"><b>訂單管理</b></a>
								</li>
								
							</ul>
							
							<!-- 標籤面板：內容區 -->
							<div class="tab-content">
								<div role="tabpanel" class="tab-pane active" id="tab1">
									<div align="right">
									<a href="<%=request.getContextPath()%>/front-end/product/product_on.jsp"><input type="button" class="btn " value="新增商品" name="addbtn" style="background-color:#f4a2b9;"></a>
									</div>
									<table class="stable customers aaa">
											<tr>
												<th>商品編號</th>
												<th>會員編號</th>
												<th>藝人編號</th>
												<th>產品照片</th>
												<th>產品數量</th>
												<th>產品資訊</th>
												<th>產品價格</th>
												<th>產品名稱</th>
												<th>產品狀態</th>
												<th>修改</th>
											</tr>
										<center><%@ include file="/front-end/product/pages/page1.file"%></center>
											<c:forEach var="productVO" items="${list}" begin="<%=pageIndex%>" end="<%=pageIndex+rowsPerPage-1%>">
												<script>
													<c:if test="${productVO.productStatus=='PRODUCT_ON'}">
														$(document).ready(function(){
															$('#status${productVO.productID}').html('上架');
														});
													</c:if>
													<c:if test="${productVO.productStatus=='PRODUCT_OFF'}">
														$(document).ready(function(){
															$('#status${productVO.productID}').html('下架');
														});
													</c:if>
													<c:if test="${productVO.productStatus=='PRODUCT_OUT'}">
														$(document).ready(function(){
															$('#status${productVO.productID}').html('缺貨');
														});
													</c:if>
													<c:if test="${productVO.productStatus=='PRODUCT_REMOVE'}">
														$(document).ready(function(){
															$('#status${productVO.productID}').html('關閉交易修改功能');
															$('#update${productVO.productID}').prop("disabled", true);
														});
													</c:if>
												</script>
												
												<tr ${productVO.productID==param.productID ? 'bgcolor=#ffecf5':'' }>
													<td>${productVO.productID}</td>
													<td>${productVO.memberID}</td>
													<td>${productVO.artistID}</td>
													<td><img src="<%=request.getContextPath()%>/product/productImg.do?productID=${productVO.productID}" style="width:100px; height:100px"></td>
													<td>${productVO.productAmount}</td>
													<td>${productVO.productInfo}</td>
													<td>${productVO.productPrice}</td>
													<td>${productVO.productName}</td>
													<td id="status${productVO.productID}">${productVO.productStatus}</td>
													<td>
														<FORM METHOD="post" ACTION="<%=request.getContextPath()%>/product/product.do" style="margin-bottom: 0px;">
															<input type="submit" class="btn btn-warning" value="修改" id="update${productVO.productID}">
															<input type="hidden" name="productID"  value="${productVO.productID}">
															<input type="hidden" name=requestURL   value="<%=request.getServletPath() %>">
															<input type="hidden" name=whichPage   value="<%=whichPage %>">
															<input type="hidden" name="action"	value="getOne_For_Update">
														</FORM>
													</td>
												</tr>
											 </c:forEach>
										</table>
										<center><%@ include file="/front-end/product/pages/page2.file" %></center>
									</div>
									
<!-- 									訂單------------------------------------------- -->
									<div role="tabpanel" class="tab-pane" id="tab2">
									<br>
									<br>
									<table class="customers">
										<tr>
											<th>訂單編號</th>
											<th>商品編號</th>
											<th>購買數量</th>
											<th>訂單金額</th>
											<th>下單時間</th>
											<th>買方評價</th>
											<th>交易方式</th>
											<th>買家姓名</th>
											<th>買家電話</th>
											<th>買家地址</th>
											<th>訂單狀態</th>
											<th>評分</th>
											<th></th>
										</tr>
										
										
										<center><%@ include file="/front-end/orderList/pages/page1OrderList.file"%></center>
									<c:forEach var="orderListVO" items="${orderList}" begin="<%=pageIndexOrder%>" end="<%=pageIndexOrder+rowsPerPageOrder-1%>">
										<script>
											<c:if test="${orderListVO.buyerScore>0}">
												$(document).ready(function(){
													$('#addStar${orderListVO.orderID}').prop("disabled", true);
												});
											</c:if>
											
											<c:if test="${orderListVO.transactionMethod=='CREDITCARD'}">
												$(document).ready(function(){
													$('#transactionMethod${orderListVO.orderID}').html('信用卡');
												});
											</c:if>
											<c:if test="${orderListVO.orderStatus=='PAID'}">
												$(document).ready(function(){
													$('#orderStatus${orderListVO.orderID}').html('已付款');
													$('#addStar${orderListVO.orderID}').prop("disabled", true);
												});
											</c:if>
											<c:if test="${orderListVO.orderStatus=='SHIPPED'}">
											$(document).ready(function(){
												$('#orderStatus${orderListVO.orderID}').html('已出貨');
												$('#addStar${orderListVO.orderID}').prop("disabled", true);
												$('#Shipment${orderListVO.orderID}').prop("disabled", true);
											});
											</c:if>
											<c:if test="${orderListVO.orderStatus=='GOT'}">
											$(document).ready(function(){
												$('#orderStatus${orderListVO.orderID}').html('已收貨');
												$('#Shipment${orderListVO.orderID}').prop("disabled", true);
											});
											</c:if>
										
										</script>
										<tr ${orderListVO.orderID==param.orderID ? 'bgcolor=#ffecf5':'' }>
											<td>${orderListVO.orderID}</td>
											<td>${orderListVO.productID}</td>
											<td>${orderListVO.buyAmount}</td>
											<td>${orderListVO.orderPrice}</td>
											<td><fmt:formatDate value="${orderListVO.orderDate}" pattern="yyyy-MM-dd HH:mm" /></td>
											<td id="score${orderListVO.orderID}">${orderListVO.buyerScore}星</td>
											<td id="transactionMethod${orderListVO.orderID}">${orderListVO.transactionMethod}</td>
											<td>${orderListVO.buyerName}</td>
											<td>${orderListVO.buyerPhone}</td>
											<td>${orderListVO.buyerAddress}</td>
											<td id="orderStatus${orderListVO.orderID}">${orderListVO.orderStatus}</td>
											<td><input type="submit" class="btn btn-warning" id="addStar${orderListVO.orderID}" value="評分"></td>
											<td> 
													<input type="submit" class="btn btn-warning" value="出貨" id="Shipment${orderListVO.orderID}">
											</td>
										</tr>
										<!-- 評分 -->
											<div class="modal fade" id="myModal${orderListVO.orderID}" role="dialog">
											    <div class="modal-dialog" align="center">
											    
											      <!-- Modal content-->
											      <div class="modal-content" style="width:250px; height:240px;" >
											        <div class="modal-header">
											          <button type="button" class="close" data-dismiss="modal">&times;</button>
											        <center>
											          	<h4 class="modal-title">評價賣方</h4>
											         </center>
											        </div>
											          <div class="modal-body" style="text-align: left; width: 110px;">
							        	
											        		  <input type="radio" name="buyerScore" value="1"><span class="fa fa-star checked"></span><br>
											        		
														      <input type="radio" name="buyerScore" value="2"><span class="fa fa-star checked"></span><span class="fa fa-star checked"></span> <br>							        		
											        		
														      <input type="radio" name="buyerScore" value="3"><span class="fa fa-star checked"></span><span class="fa fa-star checked"></span><span class="fa fa-star checked"></span> <br>
														      
														      <input type="radio" name="buyerScore" value="4"><span class="fa fa-star checked"></span><span class="fa fa-star checked"></span><span class="fa fa-star checked"></span><span class="fa fa-star checked"></span><br>
														      
														      <input type="radio" name="buyerScore" value="5"><span class="fa fa-star checked"></span><span class="fa fa-star checked"></span><span class="fa fa-star checked"></span><span class="fa fa-star checked"></span><span class="fa fa-star checked"></span><br>
											        	
											        	</div>
											        
											        <div  align="center">
											        	<button class="btn btn-default" id="addStar2${orderListVO.orderID}">送出
											        	</button>
											        </div>
											        
											      </div>
											      
											    </div>
											</div>
											<script type="text/javascript">
											$(document).ready(function(){
												$('#addStar${orderListVO.orderID}').click(function(){
									    	        $("#myModal${orderListVO.orderID}").modal();
									    	    });
												
											});
											$('#addStar2${orderListVO.orderID}').click(function(){
									    		  $('#myModal${orderListVO.orderID}').modal('hide');
													$.ajax({ 
													     url : '<%=request.getContextPath()%>/orderList/orderList.do', 
													     type : 'Post', 
													     data : { 
													      action : 'addStar', 
													      orderID :'${orderListVO.orderID}',
													      memberID:'${memberVO.memberID}',
													      buyerScore:$('#myModal${orderListVO.orderID} input[name="buyerScore"]:checked').val()
													     } 
													    }).done(function(res){
													    	var obj = JSON.parse(res);
													    	$('#score${orderListVO.orderID}').html(obj.buyerScore+'星');
													    	$('#addStar${orderListVO.orderID}').prop("disabled", true);
													    	swal({
													    		  title: "評價成功得到10代幣!",
													    		  icon: "success",
													    		  button: "確定"
													    		});
													    });
											});
											
											$('#Shipment${orderListVO.orderID}').click(function(){
													$.ajax({ 
													     url : '<%=request.getContextPath()%>/orderList/orderList.do', 
													     type : 'Post', 
													     data : { 
													      action : 'update_Order_Status', 
													      orderID:'${orderListVO.orderID}',
													      orderStatus:'SHIPPED'
													     } 
													    }).done(function(res){
													    	var obj = JSON.parse(res);
													    	$('#orderStatus${orderListVO.orderID}').html('已出貨');
													    	$('#orderStatus${orderListVO.orderID}').prop("disabled", true);
													    	$('#Shipment${orderListVO.orderID}').prop("disabled", true);
													    });
											});
										
											</script>
									</c:forEach>
										</table>
										
										<center><%@ include file="/front-end/orderList/pages/page2OrderList.file" %></center>
								
									</div>
									
								</div>
							</div>
						</div>
					</div>
				</div>
				

		<!-- 通知列表的跳窗 -->
		<div class="modal fade" id="modal-id-notifi">
			<div class="modal-dialog modal-sm">
			   <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<div class="modal-content">
				   
					<div class="modal-header">					
						<h4 class="modal-title">通知列表</h4>
					</div>
					<div class="modal-body kk-modal-body" >

                    <!-- 及時通知顯示的地方 -->
					<div id="nolistsssss" class="kk-no-list">
					  <a href="<%=request.getContextPath()%>/adfundTest/adfund.do?action=getOne_For_Display&adfundID=${adfundVO.adfundID}" ></a>
					 </div>
                        
						<c:forEach var="NotificationListVO" items="${nolists}" >
					      <div class="kk-no-list">					      
					         <a href="<%=request.getContextPath()%>${NotificationListVO.notificationpath}${NotificationListVO.notificationtarget}" id="${NotificationListVO.notificationid}">${NotificationListVO.notificationcontent}</a>
					         <h6><fmt:formatDate value="${NotificationListVO.notificationdate}" pattern="yyyy-MM-dd HH:mm"/></h6>
					      </div>
					    </c:forEach>
					</div>
					<div class="modal-footer">

					</div>
				</div>
			</div>
		</div>
	<!-- 通知的跳窗 -->

<script>

$(document).ready(function() { 
	
	//點擊鈴鐺時，通知要全部變為已讀
    $('#notifi').click(function(){
    	
//              alert("!!")
				$.ajax({ 
				     url : '<%=request.getContextPath()%>/front-end/NotificationListServlet/NotificationListServlet.do', 
				     type : 'Post', 
				     data : { 
				      action : 'isClick', 
				      memberID : '${memberVO.memberID}' 

				     } 
				    }).done(function(res){
				    	var obj = JSON.parse(res);
				    	document.getElementById("notifiPhoto").src = "<%=request.getContextPath()%>/front-end/res/img/index/notification(32).png";
				    	$('#notifiCount').html("");
	                });				     
     });
 
	
//    alert("noti!!");
    //網頁一打開時，要立刻判斷是否有未讀訊息，看是否要顯示有消息的鈴鐺
    $.ajax({ 
	     url : '<%=request.getContextPath()%>/front-end/NotificationListServlet/NotificationListServlet.do', 
	     type : 'Post', 
	     data : { 
	      action : 'isRead',  
	      memberID : '${memberVO.memberID}'
	     } 
	    }).done(function(res){
	    	var obj = JSON.parse(res);
	    	if(obj.notiStatus==false){
	    	   document.getElementById("notifiPhoto").src = "<%=request.getContextPath()%>/front-end/res/img/index/notification(32)red.png";
	    	   $('#notifiCount').html(obj.falseCount);
	    	}
	    });
    
});
</script>											
		</body>
		
</html>