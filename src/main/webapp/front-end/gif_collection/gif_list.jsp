<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page import="java.util.*"%>
<%@ page import="com.gif.model.*"%>
<%@ page import="com.gifCollect.model.*"%>
<%@ page import="com.member.model.*"%>
<%@ page import="com.notificationList.model.*"%>

<%
// 	會員
	MemberVO memberVO = (MemberVO) session.getAttribute("memberVO");
	String memberID = memberVO.getMemberID();
	
// 	通知
	NotificationListService noSvc = new NotificationListService();
    List<NotificationListVO> nolists = noSvc.getAllNoList(memberID);
    pageContext.setAttribute("nolists",nolists);

	GifService gifService = new GifService();
	List<GifVO> list = gifService.getAllGif(memberID);
	request.setAttribute("list", list);
	
	response.setHeader("Cache-Control", "no-store");
	response.setHeader("Pragma", "no-cache");
	response.setDateHeader("Expires", 0);

%>

<!DOCTYPE html>
<html lang="en">
<head>
<title>GIF</title>
<meta charset="UTF-8">
<meta name="description" content="Food Blog Web Template">
<meta name="keywords" content="food, unica, creative, html">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<!-- Favicon -->   
	<link href="img/favicon.ico" rel="shortcut icon"/>

	<!-- Google Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Poppins:400,400i,500,500i,600,600i,700" rel="stylesheet">

	<!-- Stylesheets -->
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/css/bootstrap.min.css"/>
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/css/font-awesome.min.css"/>
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/css/owl.carousel.css"/>
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/css/animate.css"/>
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/css/style.css"/>
	<link rel="stylesheet" href="<%=request.getContextPath()%>/front-end/member/css/styleLogin.css">
	<!-- original -->			
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/adfund/css/bootstrap.min.css">
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/adfund/css/bootstrap-theme.min.css">
	
	<script src="<%=request.getContextPath() %>/front-end/adfund/js/jquery-3.3.1.min.js"></script>
	
	<script src="<%=request.getContextPath() %>/front-end/adfund/js/bootstrap.min.js"></script>
	
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/adfund/css/index.css">
	
	
	<style type="text/css">
	    body{
			background-color:white;
			font-family:\5FAE\8EDF\6B63\9ED1\9AD4,\65B0\7D30\660E\9AD4;
		}
	
		.wrap{
			width:100%;
			display: flex;
			flex-wrap: wrap;
		}

		.kk-item{
			width: 100%;
			margin: 10px;
			padding: 10px;
			background-color: white;
			border:1px solid;
			border-color:#DCDCDC;
			flex-grow: 1;
		}

		@media screen and (min-width:768px){
			.kk-item{
				width: 30%;
			}
			
		}

		@media screen and (min-width:1200px){
			.kk-item{
				width:22.5%;
			}
		}


		.kk-item img{
			width: 100%;
		}

		/*如果kk-item為空就背景色為空白*/
		.kk-item:empty{
			background: none;
			border-color: white;
		}

		.gif-add-btn, .gif-up-btn, .gif-delete-btn{
			background-color: lightblue;
			margin-top: 6px;
			border:1px;
			padding: 7px;
			color: white;
			border-color: #DCDCDC;
			border-radius:3px;
		}
		
		.gif-delete-btn {
			background-color: red;
		}
		
		.gif-add-btn:hover, .gif-delete-btn:hover {
			cursor: pointer;
		}  
		
		#preview_progressbarTW_img1 {
		border: 1px solid #ddd;
		border-radius: 4px;
		padding: 2px;
		width: 200px;
		margin-top: 5px;
		margin-bottom: 20px;
	    }
	
		#preview_progressbarTW_img1:hover {
			box-shadow: 0 0 2px 1px rgba(0, 140, 186, 0.5);
		} 
		
		
		.container-fiuld {
		width:auto;
		height:800px;
		overflow: auto;
		}
		
		.modal-title{
		    float:left;
		}   

		.kk-no-list{
		   height:auto;
		   border:1px solid;
		   border-color: #E3E0E0;
		   background-color: #E3E0E0;
		   margin: 2px;
		   padding: 2px;
		   font-size:15px;
		}
		
		.kk-modal-body{
		   height:300px;
		   overflow-y: scroll;
		   overflow-x: hidden;
		
		}
	</style>

</head>
<body>
	<div id="preloder">
		<div class="loader"></div>
	</div>

<!-- Header section --> 
 <header class="header-section">	
  <div class="header-bottom"> 
   <div class="container"> 
    <a href="<%=request.getContextPath()%>/front-end/index.jsp" class="site-logo"> 
     <img src="<%=request.getContextPath() %>/front-end/img/logo-1.png" alt=""> 
    </a> 
    <div class="nav-switch"> 
     <i class="fa fa-bars"></i> 
    </div> 

    <ul class="main-menu"> 
     <li><a href="<%= request.getContextPath()%>/front-end/board/listAllBoard.jsp"><img alt="藝人專板"  title="藝人專板" src="<%= request.getContextPath()%>/front-end/res/img/index/board.png" style="height:30px;width:30px"></a></li> 
     <li><a href="<%= request.getContextPath()%>/front-end/product/Mall.jsp"><img alt="商城"  title="商城" src="<%= request.getContextPath()%>/front-end/res/img/index/mall.png" style="height:30px;width:30px"></a></li> 
     <li><a href="<%=request.getContextPath()%>/front-end/gif_collection/gif_list.jsp"><img alt="GIF"  title="GIF" src="<%= request.getContextPath()%>/front-end/res/img/index/gif.png" style="height:30px;width:30px"></a></li> 
     <li><a href="<%=request.getContextPath()%>/front-end/member/listOneMember.jsp"><img alt="會員資料" title="會員資料" src="<%= request.getContextPath()%>/front-end/res/img/index/user.png" style="height:30px;width:30px"></a></li> 
     <li><a href="#" onclick="logout()"><img alt="登出" title="登出" src="<%= request.getContextPath()%>/front-end/res/img/index/logout.png" style="height:30px;width:30px"></a> 
     <li><a href="<%=request.getContextPath()%>/front-end/chatBox/chatBox.jsp"><img alt="聊天室" title="聊天室" src="<%= request.getContextPath()%>/front-end/res/img/index/chat.png" style="height:30px;width:30px"></a></li> 
     <li>${memberVO.memberNickName }</li>
     <li><a href="<%=request.getContextPath()%>/front-end/personalPage/myPersonalPage.jsp"><img src="<%= request.getContextPath()%>/front-end/member/memberImg.do?memberID=${memberVO.memberID}" id="memberPhoto"></a></li>	
     <li id="notifi"><a href="#modal-id-notifi" data-toggle="modal"> 
      <div style="position: relative;"> 
      <img src="<%=request.getContextPath()%>/front-end/res/img/index/notification(32).png" alt="通知" id="notifiPhoto"> 
      <span style="position: absolute; top: 0; left: 17px; color: white" id="notifiCount"></span> 
      </div> 
     </a></li> 
     </ul>
   </div> 
  </div> 
 </header> 
 <!-- Header section end -->
<FORM METHOD="post" ACTION="<%=request.getContextPath()%>/front-end/member/member.do" id="logout">
	<input type="hidden" name="action"	value="logOut">
</FORM>
<script type="text/javascript">
	function logout(){
		document.getElementById("logout").submit();
	}
</script>

        <center><input type="button"  value="上傳專屬GIF" class="gif-up-btn" data-toggle="modal" data-target="#modal-id"></center>
        <%-- 錯誤表列 --%>
					<center><c:if test="${not empty errorMsgs}">
						<ul>
							<c:forEach var="message" items="${errorMsgs}">
								<li style="color: red">${message}</li>
							</c:forEach>
						</ul>
					</c:if></center>
		<br>
  		<div class="container-fiuld">				
			<div class="row">

				<div class="col-xs-12 col-sm-8 col-sm-offset-2">
					<!-- <div class="wrap"> -->
                    	<c:forEach var="gifVO" items="${list}">	
							<div class="col-xs-12 col-sm-3 item kk-item"> 
								<img src="<%=request.getContextPath()%>/gifImg/gif.do?gifID=${gifVO.gifID}&gifFile=gifFile">					
								<center>
									<div class="gif-add-btn" id="${gifVO.gifID}_add"  onclick="gifAdd('${gifVO.gifID}');">
										加入貼圖
									</div>
									<div class="gif-delete-btn" id="${gifVO.gifID}_delete" onclick="gifDelete('${gifVO.gifID}');">
										移除貼圖
									</div>
								</center>	
							</div>
							<script>
								$(document).ready(function(e) {
									var gifStatus = '${gifVO.gifStatus}';
									console.log(gifStatus);
									if(gifStatus == 'GIF_OUT_OF_USE') {
										$('#${gifVO.gifID}_add').show();
										$('#${gifVO.gifID}_delete').hide();
									} else if (gifStatus == 'GIF_IN_USE') {
										$('#${gifVO.gifID}_add').hide();
										$('#${gifVO.gifID}_delete').show();
									}	
								});
							</script>	
						</c:forEach>
					<!-- </div>		 -->
				</div>
			</div>
		</div>



		<div class="modal fade" id="modal-id">
			<div class="modal-dialog modal-sm">
				<div class="modal-content">
				   <form METHOD="post" ACTION="<%=request.getContextPath()%>/gif/gif.do" name="form2" enctype="multipart/form-data">
					<div class="modal-header">
					  
					    <h4 class="modal-title">上傳GIF</h4>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>						
					</div>
					<div class="modal-body">
							<div class="from-group">
							
								<center>
									<img id="preview_progressbarTW_img1"
										src="<%=request.getContextPath() %>/front-end/res/img/adfund/other/19.png" />
								</center>
								<input type="file" required="required"
									class="form-control" accept="image/gif" id="imgInp1"
									name="gifFile" onchange="checkImg(this)" />
								<center><h5>請上傳200像素*200像素的GIF</h5></center>
							</div>
					</div>
					<div class="modal-footer">
							<input type="hidden" name="action" value="insert"> 			
							<input type="hidden" name="gifID" value="${gifVO.gifID}">
							<input type="hidden" name="memberID" value="${memberVO.memberID}">
							<input type="hidden" name="gifStatus" value="GIF_OUT_OF_USE">
							<center><input type="submit" value="提交" class="btn btn-info" >
						
					</div>
					</form>
				</div>
			</div>
		</div>
				<!-- 通知列表的跳窗 -->
		<div class="modal fade" id="modal-id-notifi">
			<div class="modal-dialog modal-sm">
			   <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<div class="modal-content">
				   
					<div class="modal-header">					
						<h4 class="modal-title">通知列表</h4>
					</div>
					<div class="modal-body kk-modal-body" >

                    <!-- 及時通知顯示的地方 -->
					<div id="nolistsssss" class="kk-no-list">
					  <a href="<%=request.getContextPath()%>/adfundTest/adfund.do?action=getOne_For_Display&adfundID=${adfundVO.adfundID}" ></a>
					 </div>
                        
						<c:forEach var="NotificationListVO" items="${nolists}" >
					      <div class="kk-no-list">					      
					         <a href="<%=request.getContextPath()%>${NotificationListVO.notificationpath}${NotificationListVO.notificationtarget}" id="${NotificationListVO.notificationid}">${NotificationListVO.notificationcontent}</a>
					         <h6><fmt:formatDate value="${NotificationListVO.notificationdate}" pattern="yyyy-MM-dd HH:mm"/></h6>
					      </div>
					    </c:forEach>
					</div>
					<div class="modal-footer">

					</div>
				</div>
			</div>
		</div>
	<!-- 通知的跳窗 -->

<script>

$(document).ready(function() { 
	
	//點擊鈴鐺時，通知要全部變為已讀
    $('#notifi').click(function(){
    	
//              alert("!!")
				$.ajax({ 
				     url : '<%=request.getContextPath()%>/front-end/NotificationListServlet/NotificationListServlet.do', 
				     type : 'Post', 
				     data : { 
				      action : 'isClick', 
				      memberID : '${memberVO.memberID}' 

				     } 
				    }).done(function(res){
				    	var obj = JSON.parse(res);
				    	document.getElementById("notifiPhoto").src = "<%=request.getContextPath()%>/front-end/res/img/index/notification(32).png";
				    	$('#notifiCount').html("");
	                });				     
     });
 
	
//    alert("noti!!");
    //網頁一打開時，要立刻判斷是否有未讀訊息，看是否要顯示有消息的鈴鐺
    $.ajax({ 
	     url : '<%=request.getContextPath()%>/front-end/NotificationListServlet/NotificationListServlet.do', 
	     type : 'Post', 
	     data : { 
	      action : 'isRead',  
	      memberID : '${memberVO.memberID}'
	     } 
	    }).done(function(res){
	    	var obj = JSON.parse(res);
	    	if(obj.notiStatus==false){
	    	   document.getElementById("notifiPhoto").src = "<%=request.getContextPath()%>/front-end/res/img/index/notification(32)red.png";
	    	   $('#notifiCount').html(obj.falseCount);
	    	}
	    });
    
});
</script>

		
	<!--====== Javascripts & Jquery ======-->
	<script src="<%=request.getContextPath() %>/front-end/js/jquery-3.2.1.min.js"></script>
	<script src="<%=request.getContextPath() %>/front-end/owl.carousel.min.js"></script>
	<script src="<%=request.getContextPath() %>/front-end/js/main.js"></script>		
    <script src="https://code.jquery.com/jquery.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script type="text/javascript">

		//預覽圖片
		$('#imgInp1').change(function() {
			var file = $('#imgInp1')[0].files[0];
			var reader = new FileReader;
			reader.onload = function(e) {
				$('#preview_progressbarTW_img1').attr('src', e.target.result);
			};
			reader.readAsDataURL(file);
		});


        //限制圖片上傳尺寸
        function checkImg(img){

        	var reader = new FileReader();
        	reader.onload = function(evt) {
        	var image = new Image();   
        	image.src = evt.target.result;
        	var height = image.height;   
        	var width = image.width;   
        	var filesize = img.files[0].size; 
        	  if(width!=200 && height!=200){
        		img.value="";
        		alert('請上傳200*200像素的圖片!');
        		
        	} 
        	}
        	reader.readAsDataURL(img.files[0]); 
        }

        </script>
   

</body>
<script>

	function gifAdd(gifIDStr) {
		var gifID = gifIDStr;
		console.log(gifID);
		
		$.ajax({
			type: "POST",
			url: "<%=request.getContextPath()%>/gif/gif.do",
			data: {
				action: "gif_in_use",
				gifID: gifID,
				gifStatus: "GIF_IN_USE"
			}
		}).done(function(res) {
			var href = '<%=request.getContextPath()%>/front-end/gif_collection/gif_list.jsp';
			window.location.href = href;
		});		
		
	}
	
	function gifDelete(gifIDStr) {
		var gifID = gifIDStr;
		console.log(gifID);
		
		$.ajax({
			type: "POST",
			url: "<%=request.getContextPath()%>/gif/gif.do",
			data: {
				action: "gif_out_of_use",
				gifID: gifID,
				gifStatus: "GIF_OUT_OF_USE"
			}
		}).done(function(res) {
			var href = '<%=request.getContextPath()%>/front-end/gif_collection/gif_list.jsp';
			window.location.href = href;
		});	
	}

</script>
</html>