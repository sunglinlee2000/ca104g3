<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="java.util.*"%>
<%@ page import="com.issue.model.*"%>
<%@ page import="com.issueLike.model.*"%>
<%@ page import="com.issueReport.model.*"%>
<%@ page import="com.member.model.*"%>
<%@ page import="com.commentList.model.*"%>
<%@ page import="com.notificationList.model.*"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%

	//	會員
	MemberVO memberVO = (MemberVO) session.getAttribute("memberVO");
	String memberID = memberVO.getMemberID();
	
	//	通知
	NotificationListService noSvc = new NotificationListService();
	List<NotificationListVO> nolists = noSvc.getAllNoList(memberID);
	pageContext.setAttribute("nolists",nolists);

  response.setHeader("Cache-Control","no-store"); //HTTP 1.1
  response.setHeader("Pragma","no-cache");        //HTTP 1.0
  response.setDateHeader ("Expires", 0);
  
%>
<%
  	
	IssueVO issueVO = (IssueVO) session.getAttribute("issueVO");
	String issueID = issueVO.getIssueID();

	if(request.getAttribute("boardID") != null){
  		issueVO.setBoardID((String)request.getAttribute("boardID"));
  	}
	
	pageContext.setAttribute("issueID", issueID);
	CommentListService commentListSvc = new CommentListService();
	List<CommentListVO> commentlist = commentListSvc.getAllByIssueID(issueID);
	pageContext.setAttribute("commentlist", commentlist);
	
	CommentListVO commentListVO = (CommentListVO) request.getAttribute("commentListVO");
	String issueIDxx = request.getParameter("issueID");
	
	IssueLikeService issueLikeService = new IssueLikeService();
	pageContext.setAttribute("issueLikeService", issueLikeService);
	
%>	




<html lang="">
    <head>
    
    
    
<meta charset="UTF-8">
<meta name="description" content="Food Blog Web Template">
<meta name="keywords" content="food, unica, creative, html">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Title Page${coin}</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
    <!--[if lt IE 9]>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
        <!-- Favicon -->   
	<link href="img/favicon.ico" rel="shortcut icon"/>

	<!-- Google Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Poppins:400,400i,500,500i,600,600i,700" rel="stylesheet">

	<!-- Stylesheets -->
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/css/bootstrap.min.css"/>
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/css/font-awesome.min.css"/>
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/css/owl.carousel.css"/>
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/css/animate.css"/>
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/css/style.css"/>
	
	<!-- original -->			
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/issue/css/bootstrap.min.css">
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/issue/css/bootstrap-theme.min.css">
	<link rel="stylesheet" href="<%=request.getContextPath()%>/front-end/member/css/styleLogin.css">
	
	<script src="<%=request.getContextPath() %>/front-end/js/jquery-3.2.1.min.js"></script>
	<script src="<%=request.getContextPath() %>/front-end/owl.carousel.min.js"></script>
	<script src="<%=request.getContextPath() %>/front-end/js/main.js"></script>		
   	<script src="https://code.jquery.com/jquery.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
	
	<script src="<%=request.getContextPath() %>/front-end/issue/js/jquery-3.3.1.min.js"></script>
	
	<script src="<%=request.getContextPath() %>/front-end/issue/js/bootstrap.min.js"></script>
	
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/issue/css/index.css">
	<script src="<%=request.getContextPath() %>/ckeditor/ckeditor.js"></script>
	
    <!-- alert -->
   	<script src="<%=request.getContextPath() %>/front-end/issue/js/sweetalert2.all.min.js"></script>
   	
   	
   	<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

     	
<style>
body{
    font-family:\5FAE\8EDF\6B63\9ED1\9AD4,\65B0\7D30\660E\9AD4;
    background-color: white;
  	font-family: 'Ubuntu', sans-serif;
}	
	
#content{
  width:740px
}
.button {
  border-radius: 4px;
  background-color: #7dccff;
  border: none;
  color: #FFFFFF;
  text-align: center;
  font-size: 28px;
  padding: 12px;
  width: 130px;
  height: 55px;
  transition: all 0.5s;
  cursor: pointer;
  margin: 5px;
  border: 1px solid gray;
  font-size: 25px;
}

.button span {
  cursor: pointer;
  display: inline-block;
  position: relative;
  transition: 0.5s;
}

.button span:after {
  content: '\00bb';
  position: absolute;
  opacity: 0;
  top: 0;
  right: -20px;
  transition: 0.5s;
}

.button:hover span {
  padding-right: 25px;
}

.button:hover span:after {
  opacity: 1;
  right: 0;
}


.button2 {
  border-radius: 4px;
  background-color: #FF3333;
  border: none;
  color: #FFFFFF;
  text-align: center;
  font-size: 28px;
  padding: 12px;
  width: 85px;
  height: 55px;
  transition: all 0.5s;
  cursor: pointer;
  margin: 5px;
  border: 1px solid gray;
  font-size: 25px;
}

.button2 span {
  cursor: pointer;
  display: inline-block;
  position: relative;
  transition: 0.5s;
}

.button2 span:after {
  content: '\00bb';
  position: absolute;
  opacity: 0;
  top: 0;
  right: 0px;
  transition: 0.5s;
}

.button2:hover span {
  padding-right: 25px;
}

.button2:hover span:after {
  opacity: 1;
  right: 0;
}

.kk-no-list{
   height:auto;
   border:1px solid;
   border-color: #E3E0E0;
   background-color: #E3E0E0;
   margin: 2px;
   padding: 2px;
   font-size:15px;
}

.kk-modal-body{
   height:300px;
   overflow-y: scroll;
   overflow-x: hidden;

}
	
</style>
	
    </head>
    <body>
    <jsp:useBean id="MemberService" scope="page" class="com.member.model.MemberService"/>
    
<!-- Header section --> 
 <header class="header-section">	
  <div class="header-bottom"> 
   <div class="container"> 
    <a href="<%=request.getContextPath()%>/front-end/index.jsp" class="site-logo"> 
     <img src="<%=request.getContextPath() %>/front-end/img/logo-1.png" alt=""> 
    </a> 
    <div class="nav-switch"> 
     <i class="fa fa-bars"></i> 
    </div> 

    <ul class="main-menu"> 
     <li><a href="<%= request.getContextPath()%>/front-end/board/listAllBoard.jsp"><img alt="藝人專板"  title="藝人專板" src="<%= request.getContextPath()%>/front-end/res/img/index/board.png" style="height:30px;width:30px"></a></li> 
     <li><a href="<%= request.getContextPath()%>/front-end/product/Mall.jsp"><img alt="商城"  title="商城" src="<%= request.getContextPath()%>/front-end/res/img/index/mall.png" style="height:30px;width:30px"></a></li> 
     <li><a href="<%=request.getContextPath()%>/front-end/gif_collection/gif_list.jsp"><img alt="GIF"  title="GIF" src="<%= request.getContextPath()%>/front-end/res/img/index/gif.png" style="height:30px;width:30px"></a></li> 
     <li><a href="<%=request.getContextPath()%>/front-end/member/listOneMember.jsp"><img alt="會員資料" title="會員資料" src="<%= request.getContextPath()%>/front-end/res/img/index/user.png" style="height:30px;width:30px"></a></li> 
     <li><a href="#" onclick="logout()"><img alt="登出" title="登出" src="<%= request.getContextPath()%>/front-end/res/img/index/logout.png" style="height:30px;width:30px"></a> 
     <li><a href="<%=request.getContextPath()%>/front-end/chatBox/chatBox.jsp"><img alt="聊天室" title="聊天室" src="<%= request.getContextPath()%>/front-end/res/img/index/chat.png" style="height:30px;width:30px"></a></li> 
     <li>${memberVO.memberNickName }</li>
     <li><a href="<%=request.getContextPath()%>/front-end/personalPage/myPersonalPage.jsp"><img src="<%= request.getContextPath()%>/front-end/member/memberImg.do?memberID=${memberVO.memberID}" id="memberPhoto"></a></li>	
     <li id="notifi"><a href="#modal-id-notifi" data-toggle="modal"> 
      <div style="position: relative;"> 
      <img src="<%=request.getContextPath()%>/front-end/res/img/index/notification(32).png" alt="通知" id="notifiPhoto"> 
      <span style="position: absolute; top: 0; left: 17px; color: white" id="notifiCount"></span> 
      </div> 
     </a></li> 
     </ul>
   </div> 
  </div> 
 </header> 
 <!-- Header section end -->
<FORM METHOD="post" ACTION="<%=request.getContextPath()%>/front-end/member/member.do" id="logout">
	<input type="hidden" name="action"	value="logOut">
</FORM>
<script type="text/javascript">
	function logout(){
		document.getElementById("logout").submit();
	}
</script>

            <div class="container">
                <div class="row">           
                    <div class="col-xs-12 col-sm-8 col-sm-offset-2">                 
                            <div class="page-header"> 
                            	<p>
<%--會員--%> 						<a href="<%=request.getContextPath()%>/personalPage/personalPage.do?memberID=${issueVO.memberID}&action=toOnePersonalPage">                        
			                        	<img src="<%= request.getContextPath()%>/front-end/member/memberImg.do?memberID=${issueVO.memberID}" class="img-circle" style="height: 100px; width: 100px; margin: auto;" id="memberPhoto">
			                        </a>
<!--                                        String memID = issueVO.getMemberID(); -->
<!--                                        MemberVO memberVO = MemberService.getOneMember(memID); -->
<!--                                        String memberName = memberVO.getMemberName(); -->

<!--等同下面-->
									${MemberService.getOneMember(issueVO.memberID).memberName} 發表於:&nbsp; <fmt:formatDate value = "${issueVO.issueDate}" pattern="YYYY-MM-dd  hh:mm:ss"/>                                                             			
                           		</p>                     
<!--標題-->                     <h2> <%=issueVO.getIssueTitle()%></h2><br>
								<div>
               					 <table>

<!--發文時間-->					  <tr> 										
										<td>
				     						<input type="hidden" name="issueID"  value="${issueVO.issueID}">
									        <a href="<%=request.getContextPath() %>/front-end/issue/issue_list.jsp?boardID=${issueVO.boardID}" class=" button btn-md  btn pull-right">
									        	<span>Back</span>
								        	</a>
				     					</td>	
				     					
										<td>    
										    <c:if test="${memberVO.memberID == issueVO.memberID }">
										    <FORM METHOD="post" ACTION="<%=request.getContextPath()%>/issue/issue.do" style="margin-bottom: 0px;">
											    <span>
										  			<input type="submit" class=" button btn pull-right" style="display:block;" value="Revise">
												    <input type="hidden" name="issueID"  value="${issueVO.issueID}">
												    <input type="hidden" name="memberID"  value="${issueVO.memberID}">
												    <input type="hidden" name="issueDate"  value="${issueVO.issueDate}">
												    <input type="hidden" name="issueLikeCount"  value="${issueVO.issueLikeCount}">
		<%-- 										<input type="hidden" name="issueContentPhoto"  value="${issueVO.issueContentPhoto}"> --%>
												    <input type="hidden" name="action"	value="getOne_For_Update">
												</span>   
										    </FORM>
				     						</c:if>
				     						<c:if test="${memberVO.memberID != issueVO.memberID }">
										    <FORM METHOD="post" ACTION="<%=request.getContextPath()%>/issue/issue.do" style="margin-bottom: 0px;">
											    <input type="submit" class="button btn pull-right" style="display:none;" value="Revise">
											    <input type="hidden" name="issueID"  value="${issueVO.issueID}">
											    <input type="hidden" name="memberID"  value="${issueVO.memberID}">
											    <input type="hidden" name="issueDate"  value="${issueVO.issueDate}">
											    <input type="hidden" name="issueLikeCount"  value="${issueVO.issueLikeCount}">
	<%-- 										<input type="hidden" name="issueContentPhoto"  value="${issueVO.issueContentPhoto}"> --%>
											    <input type="hidden" name="action"	value="getOne_For_Update">
										    </FORM>
				     						</c:if>
			     						</td>
			     						
<!-- 				     					<td>	 -->
<%-- 				     						<c:if test="${memberVO.memberID == issueVO.memberID }"> --%>
<!-- 	                                        	<a href="#" class="button btn pull-right" style="display:none;"><span>Share</span></a> -->
<%-- 	                                        </c:if> --%>
                                      
<%-- 	                                        <c:if test="${memberVO.memberID != issueVO.memberID }"> --%>
<!-- 	                                        	<a href="#" class="button btn pull-right" style="display:block;"><span>Share</span></a> -->
<%-- 	                                        </c:if>                                       --%>
<!-- 										</td>		 -->
										<td>		
<%-- 											<form style="display: inline;" method="post" action="<%=request.getContextPath()%>/issueLike/issueLike.do"> --%>
												<button class="button " type="submit" id="like">
													
	 
														<c:if test="${issueLikeService.getOneIssueLike(issueVO.getIssueID(),memberVO.getMemberID())!=null}">
<!--收回讚 -->												<span><img src="<%=request.getContextPath() %>/front-end/issue/images/001-redheart32.png" class="redheart" id="redheart"></span>&nbsp;&nbsp;${issueLikeService.getCount(issueVO.getIssueID())} 
															<input type="hidden" name="action" value="delete">
														</c:if>
		
														<c:if test="${issueLikeService.getOneIssueLike(issueVO.getIssueID(),memberVO.getMemberID())==null}">
<!--點讚 -->												<span><img src="<%=request.getContextPath() %>/front-end/issue/images/002-heart32.png" class="whiteheart" id="whiteheart"></span>&nbsp;&nbsp;${issueLikeService.getCount(issueVO.getIssueID())}
															<input type="hidden" name="action" value="insert">
														</c:if>
												</button>
												
												<input type="hidden" name="memberID" value="${memberVO.memberID}">
												<input type="hidden" name="issueID" value="${issueVO.issueID}">
<!-- 											</form>	 -->
										</td>
										<td>
	                                        <c:if test="${memberVO.memberID == issueVO.memberID }">                                   
	                                        <FORM METHOD="post" ACTION="<%=request.getContextPath()%>/issue/issue.do" style="margin-bottom: 0px;">
											    <input type="submit" class="button2 btn pull-right" style="display:block;" value="X">
											    <input type="hidden" name="issueID"  value="${issueVO.issueID}">
											    <input type="hidden" name="boardID"  value="${issueVO.boardID}">
											    <input type="hidden" name="action" value="deleteByIssueID">
										    </FORM>
										    </c:if>
										    <c:if test="${memberVO.memberID != issueVO.memberID }">                                   
	                                        <FORM METHOD="post" ACTION="<%=request.getContextPath()%>/issue/issue.do" style="margin-bottom: 0px;">
											    <input type="submit" class="button btn pull-right" style="display:none;" value="X">
											    <input type="hidden" name="issueID"  value="${issueVO.issueID}">
											    <input type="hidden" name="boardID"  value="${issueVO.boardID}">
											    <input type="hidden" name="action" value="deleteByIssueID">
										    </FORM>
										    </c:if>
									    </td>
									    <td>
										    <c:if test="${memberVO.memberID == issueVO.memberID }">   
										    <a href="<%=request.getContextPath()%>/front-end/issueReport/addEmp.jsp?issueID=${issueVO.issueID}&boardID=${issueVO.boardID}" class="button2 btn pull-right" style="display:none;">
<!--檢舉-->										<img src="<%=request.getContextPath() %>/front-end/issue/images/004-alert32.png">
										    </a>
											</c:if>
											<c:if test="${memberVO.memberID != issueVO.memberID }">   
										    <a href="<%=request.getContextPath()%>/front-end/issueReport/addEmp.jsp?issueID=${issueVO.issueID}&boardID=${issueVO.boardID}" class="button2 btn pull-right" style="display:block;">
<!--檢舉-->										<span><img src="<%=request.getContextPath() %>/front-end/issue/images/004-alert32.png"></span>
										    </a>
											</c:if>
										</td>
									    
									    
                                     </tr>
                             	</table>
                             </div>
                             	<br>
                                                 
                         </div>
                    </div>
                        
                       <div class="col-xs-12 col-sm-6 col-sm-offset-2">
                            <div class="container">
                              <div class="row">                              
                                 <form>         
<!--文章內容-->                     <div class="container"><span><div id="content"><%=issueVO.getIssueContentPhoto()%> </div></span></div>
                                 </form>   
                              </div>
                            </div>
                        </div>
                        
                        <center><button class="button" id="123456789" style=" width: 545px;"> Click Me To Leave A Message</button></center>
						<script>
							$(document).ready(function(){
								$('#987654321').hide();
								$('#123456789').click(function(){
									$('#987654321').toggle(300);
								})
							})
						</script>
	                    <div class="" id="987654321">   
	                        <div class="container">
								<div class="row">
									<div class="col-xs-12 col-sm-6 col-sm-offset-3">
										<c:if test="${not empty errorMsgs}">	
												<ul>
													<c:forEach var="message" items="${errorMsgs}">
														<li style="color:red">${message}</li>
													</c:forEach>
												</ul>
										</c:if>
	                                    
                                  		 <form method="post" action="<%=request.getContextPath() %>/commentList/commentList.do" enctype="multipart/form=data">

	                                          <div class="form-group">
	                                              <center><label><h2>Enter Your Comment</h2></label></center>                              
	                                              <textarea name="commentContent" id="txa" class="form-control" rows="5"></textarea>
	                                          </div>

											  <div>    
			                                        <input type="hidden" name="action" value="insert">
													<input type="hidden" name="issueID" value="<%=issueID%>" >
													<input type="hidden" name="memberID" value="${memberVO.memberID}" >
													<input class="button2" type="submit" style=" width: 545px; background-color: #00DD77;"  value="Submit" id="${memberVO.memberID}">                                                 
			                                  </div>
                                      	</form>
									</div>
								</div>
							</div>
						</div>
                        
                        <div class="">   
	                        <div class="container">
								<div class="row">
											<c:forEach var="commentListVO" items="${commentlist}">
									            <div class="container">
									                <div class="row">           
									                    <div class="col-xs-12 col-sm-8 col-sm-offset-2"> 		
													
																<div class="media-heading">
																	<a href="#"> 
																		<img src="<%= request.getContextPath()%>/front-end/member/memberImg.do?memberID=${commentListVO.memberID}" class="img-circle"style="height: 100px; width: 100px; margin: auto;" id="memberPhoto">	
																	</a>
																</div>
																<div class="media-body">
																	<table>
																	<tr>
																		<td>	
																			<h4 class="media-heading">${MemberService.getOneMember(commentListVO.memberID).memberName}<small>&nbsp;回覆於&nbsp;<i><fmt:formatDate value = "${commentListVO.commentDate}" pattern="YYYY-MM-dd  hh:mm:ss"/> </i></small></h4>&nbsp;
																		</td>
																		<td>																		
																			<FORM METHOD="post"
																				ACTION="<%=request.getContextPath()%>/commentList/commentList.do"
																				style="margin-bottom: 0px;">
																				<c:if test="${memberVO.memberID == commentListVO.memberID}">
<!-- 																					<input type="submit" class="btn btn-primary" value="修改"> <input -->
<%-- 																						type="hidden" name="commentID" value="${commentListVO.commentID}"> --%>
<!-- 																					<input type="hidden" name="action" value="getOne_For_Update"> -->

																					<a href="#modal-id${commentListVO.commentID}" data-toggle="modal" class="btn " style="background-color: #7dccff;"><span style="color: white;"><b>修改</b></span></a>
																				        <div class="modal fade" id="modal-id${commentListVO.commentID}">
																				            <div class="modal-dialog">
																				                <div class="modal-content">
																				                    <div class="modal-header">
																				                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
																				                        <h4 class="modal-title">修改留言</h4>
																				                    </div>
																				                    <div class="modal-body">
																				                        <textarea name="commentContent" class="form-control" rows="5" >${commentListVO.commentContent}</textarea>
																				                    </div>
																				                    <div class="modal-footer">
																				                    	<div>																				                        																					                       
																											<input type="hidden" name="action" value="update">
																																															
																											<input type="hidden" name="memberID" value="${commentListVO.memberID}" >
																											<input type="hidden" name="issueID" value="${commentListVO.issueID}" >
																											<input type="hidden" name="gifID" value="${commentListVO.gifID}" >
																											<input type="hidden" name="commentID" value="${commentListVO.commentID}">																			
																					                        <button type="submit" class="btn btn-primary">Save</button>
																					                        																					                    
																					                    	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>																					                    
																					                    </div> 
																					                      
																				                    </div>
																				                </div>
																				            </div>
																				        </div>

																				</c:if>
																				</FORM>
																			</td>
																			<td>
																				<FORM METHOD="post"
																				ACTION="<%=request.getContextPath()%>/commentList/commentList.do"
																				style="margin-bottom: 0px;">
																					<c:if test="${memberVO.memberID == commentListVO.memberID || memberVO.memberID == issueVO.memberID}" >
																						<span style="color: white;"><b><h4><input type="submit" class="btn " value="X" style="background-color: #FF3333;"><input
																							type="hidden" name="commentID" value="${commentListVO.commentID}">
																						<input type="hidden" name="action" value="delete"></h4></b></span>
																					</c:if>	
																				</FORM>																																					
																			</td>
																		</tr>	
																	</table>
																	<p class="text"
																		style="margin-left: 15px; font-size: 20px; color: black;">
																		${commentListVO.commentContent}<br>
																		
																		<hr>
																	
																</div>
														
														</div>
													</div>
									
												</div>
												<br>			
											</c:forEach>
								</div>
							</div>
						</div>
						
						
								
                    </div>
                </div>
        
    <script>
 		   
    $(document).ready(function() {  
	     $('#like').click(function() {
	    	 
	    	if($('.whiteheart').attr("id")!="whiteheart"){
	    		$.ajax({  
		             url : '<%=request.getContextPath()%>/issueLike/issueLike.do',  
		             type : 'Post',  
		             data : {  
		              action : 'delete',  
		              memberID:'${memberVO.memberID}',
		              issueID:'${issueVO.issueID}'
		             }  
		            }).done(function(res){ 
		             var obj = JSON.parse(res); 
		             $('#like').html(
			             '<span><img src="<%=request.getContextPath()%>/front-end/issue/images/002-heart32.png" class="whiteheart" id="whiteheart"></span>'+'  &nbsp;  '+ obj.likes)
			             });
	    		 
	    	}else{
	    		$.ajax({  
		             url : '<%=request.getContextPath()%>/issueLike/issueLike.do',  
		             type : 'Post',  
		             data : {  
		              action : 'insert',  
		              memberID:'${memberVO.memberID}',
		              issueID:'${issueVO.issueID}'
		             }  
		            }).done(function(res){ 
		             var obj = JSON.parse(res);
		             $('#like').html(
		             '<span><img src="<%=request.getContextPath()%>/front-end/issue/images/001-redheart32.png" class="redheart" id="redheart"></span>'+'   &nbsp; '+ obj.likes)
		             });
	    	} 
	     });
      });
    
    </script> 
    
    		<!-- 通知列表的跳窗 -->
		<div class="modal fade" id="modal-id-notifi">
			<div class="modal-dialog modal-sm">
			   <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<div class="modal-content">
				   
					<div class="modal-header">					
						<h4 class="modal-title">通知列表</h4>
					</div>
					<div class="modal-body kk-modal-body" >

                    <!-- 及時通知顯示的地方 -->
					<div id="nolistsssss" class="kk-no-list">
					  <a href="<%=request.getContextPath()%>/adfundTest/adfund.do?action=getOne_For_Display&adfundID=${adfundVO.adfundID}" ></a>
					 </div>
                        
						<c:forEach var="NotificationListVO" items="${nolists}" >
					      <div class="kk-no-list">					      
					         <a href="<%=request.getContextPath()%>${NotificationListVO.notificationpath}${NotificationListVO.notificationtarget}" id="${NotificationListVO.notificationid}">${NotificationListVO.notificationcontent}</a>
					         <h6><fmt:formatDate value="${NotificationListVO.notificationdate}" pattern="yyyy-MM-dd HH:mm"/></h6>
					      </div>
					    </c:forEach>
					</div>
					<div class="modal-footer">

					</div>
				</div>
			</div>
		</div>
	<!-- 通知的跳窗 -->

<script>

$(document).ready(function() { 
	
	//點擊鈴鐺時，通知要全部變為已讀
    $('#notifi').click(function(){
    	
//              alert("!!")
				$.ajax({ 
				     url : '<%=request.getContextPath()%>/front-end/NotificationListServlet/NotificationListServlet.do', 
				     type : 'Post', 
				     data : { 
				      action : 'isClick', 
				      memberID : '${memberVO.memberID}' 

				     } 
				    }).done(function(res){
				    	var obj = JSON.parse(res);
				    	document.getElementById("notifiPhoto").src = "<%=request.getContextPath()%>/front-end/res/img/index/notification(32).png";
				    	$('#notifiCount').html("");
	                });				     
     });
 
	
//    alert("noti!!");
    //網頁一打開時，要立刻判斷是否有未讀訊息，看是否要顯示有消息的鈴鐺
    $.ajax({ 
	     url : '<%=request.getContextPath()%>/front-end/NotificationListServlet/NotificationListServlet.do', 
	     type : 'Post', 
	     data : { 
	      action : 'isRead',  
	      memberID : '${memberVO.memberID}'
	     } 
	    }).done(function(res){
	    	var obj = JSON.parse(res);
	    	if(obj.notiStatus==false){
	    	   document.getElementById("notifiPhoto").src = "<%=request.getContextPath()%>/front-end/res/img/index/notification(32)red.png";
	    	   $('#notifiCount').html(obj.falseCount);
	    	}
	    });
    
});
</script>

<%	
	if (request.getAttribute("coin") != null) {
%>
<script type="text/javascript">
		swal("Get 300 Coin!", "發文得代幣唷!!", "<%=request.getContextPath()%>/front-end/issue/images/coin.gif");	
</script>		
<%	
	}
%>
	
    </body>
</html>