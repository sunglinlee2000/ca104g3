<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="java.util.*"%>
<%@ page import="com.issue.model.*"%>

<%
	response.setHeader("Cache-Control", "no-store"); //HTTP 1.1 
	response.setHeader("Pragma", "no-cache"); //HTTP 1.0 
	response.setDateHeader("Expires", 0);
%>

<%
	IssueVO issueVO = (IssueVO) request.getAttribute("issueVO");
%>

<html lang="">
<head>
<meta charset="UTF-8">
<meta name="description" content="Food Blog Web Template">
<meta name="keywords" content="food, unica, creative, html">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>修改文章</title>
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
<!--[if lt IE 9]> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script> 
<![endif]-->

<script src="https://code.jquery.com/jquery-1.9.1.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>


<link href="img/favicon.ico" rel="shortcut icon" />

<!-- Google Fonts -->
<link
	href="https://fonts.googleapis.com/css?family=Poppins:400,400i,500,500i,600,600i,700"
	rel="stylesheet">

<!-- Stylesheets -->
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/front-end/css/bootstrap.min.css" />
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/front-end/css/font-awesome.min.css" />
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/front-end/css/owl.carousel.css" />
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/front-end/css/animate.css" />
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/front-end/css/style.css" />
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/front-end/member/css/styleLogin.css">

<!-- original -->
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/front-end/issue/css/bootstrap.min.css">
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/front-end/issue/css/bootstrap-theme.min.css">

<script
	src="<%=request.getContextPath()%>/front-end/issue/js/jquery-3.2.1.min.js"></script>

<script
	src="<%=request.getContextPath()%>/front-end/issue/js/bootstrap.min.js"></script>
<script src="<%=request.getContextPath()%>/ckeditor/ckeditor.js"></script>

<!-- 下載來用可以額外對 config.js 修改樣式顏色之類的 -->
<!-- 連結 ： https://ckeditor.com/cke4/release/CKEditor-4.4.1 -->
<!-- <script src="ckeditor_4.4.1/ckeditor.js"></script> -->
<!-- 直接連 cdn 用也是可以啦 -->
<!-- <script src="http://cdn.ckeditor.com/4.4.1/standard/ckeditor.js"></script> -->


 <!-- alert -->
<script src="<%=request.getContextPath() %>/front-end/issue/js/sweetalert2.all.min.js"></script>
     
<style type="text/css">
.btn-primary, .btn-primary:hover, .btn-primary:active, .btn-primary:visited,
	.btn-primary:focus {
	background-color: #7dccff;
	border-color: #7dccff;
}
</style>

</head>
<body>
<!-- Header section --> 
 <header class="header-section">	
  <div class="header-bottom"> 
   <div class="container"> 
    <a href="<%=request.getContextPath()%>/front-end/index.jsp" class="site-logo"> 
     <img src="<%=request.getContextPath() %>/front-end/img/logo-1.png" alt=""> 
    </a> 
    <div class="nav-switch"> 
     <i class="fa fa-bars"></i> 
    </div> 

    <ul class="main-menu"> 
     <li><a href="<%= request.getContextPath()%>/front-end/board/listAllBoard.jsp"><img alt="藝人專板"  title="藝人專板" src="<%= request.getContextPath()%>/front-end/res/img/index/board.png" style="height:30px;width:30px"></a></li> 
     <li><a href="<%= request.getContextPath()%>/front-end/product/Mall.jsp"><img alt="商城"  title="商城" src="<%= request.getContextPath()%>/front-end/res/img/index/mall.png" style="height:30px;width:30px"></a></li> 
     <li><a href="<%=request.getContextPath()%>/front-end/gif_collection/gif_list.jsp"><img alt="GIF"  title="GIF" src="<%= request.getContextPath()%>/front-end/res/img/index/gif.png" style="height:30px;width:30px"></a></li> 
     <li><a href="<%=request.getContextPath()%>/front-end/member/listOneMember.jsp"><img alt="會員資料" title="會員資料" src="<%= request.getContextPath()%>/front-end/res/img/index/user.png" style="height:30px;width:30px"></a></li> 
     <li><a href="#" onclick="logout()"><img alt="登出" title="登出" src="<%= request.getContextPath()%>/front-end/res/img/index/logout.png" style="height:30px;width:30px"></a> 
     <li><a href="<%=request.getContextPath()%>/front-end/chatBox/chatBox.jsp"><img alt="聊天室" title="聊天室" src="<%= request.getContextPath()%>/front-end/res/img/index/chat.png" style="height:30px;width:30px"></a></li> 
     <li>${memberVO.memberNickName }</li>
     <li><a href="<%=request.getContextPath()%>/front-end/personalPage/myPersonalPage.jsp"><img src="<%= request.getContextPath()%>/front-end/member/memberImg.do?memberID=${memberVO.memberID}" id="memberPhoto"></a></li>	
     <li id="notifi"><a href="#modal-id-notifi" data-toggle="modal"> 
      <div style="position: relative;"> 
      <img src="<%=request.getContextPath()%>/front-end/res/img/index/notification(32).png" alt="通知" id="notifiPhoto"> 
      <span style="position: absolute; top: 0; left: 17px; color: white" id="notifiCount"></span> 
      </div> 
     </a></li> 
     </ul>
   </div> 
  </div> 
 </header> 
 <!-- Header section end -->
<FORM METHOD="post"
							ACTION="<%=request.getContextPath()%>/front-end/member/member.do"
							id="logout">
							<input type="hidden" name="action" value="logOut"> 
</FORM> 
<script type="text/javascript">
	function logout() {
		document.getElementById("logout").submit();
	}
</script> 

<div class="container"> 
<div class="row"> 
<div class=""> 

<%-- 錯誤表列 --%> 
<c:if test="${not empty errorMsgs}"> 

<ul> 
<c:forEach var="message" items="${errorMsgs}"> 
<li style="color:red">${message}</li> 
</c:forEach> 
</ul> 
</c:if> 

<form method="post"
										action="<%=request.getContextPath()%>/issue/issue.do"
										enctype="multipart/form=data"> 

<div class="form-group"> 
<label>輸入標題</label> 
<input type="text" class="form-control" name="title" size="45"
												value="<%=issueVO.getIssueTitle()%>" /> 

</div> 
<div class="form-group"> 
<label>輸入內容</label> 
<textarea id="ckeditor" name="issueContentPhoto"> 

<input type="hidden" name="issueContentPhoto"
													value="${issueVO.getIssueContentPhoto()}"> 

</textarea> 
<script>
	CKEDITOR
			.replace(
					'issueContentPhoto',
					{
						height : 500,
						cloudServices_tokenUrl : 'https://35969.cke-cs.com/token/dev/Wrw2VWZZ7IMjlbo5A5S5hp2GG3eBRgEkYbFOLMmC5uQgJcCy0hDWuInlSLTT',
						cloudServices_uploadUrl : 'https://35969.cke-cs.com/easyimage/upload/'
					});
</script> 
</div> 

<center>
											<div> 
<input type="hidden" name="action" value="update"> 
<input type="hidden" name="issueID" value="${issueVO.issueID}"> 
<input type="hidden" name="memberID" value="${issueVO.memberID}"> 
<input type="hidden" name="title" value="${issueVO.issueTitle}"> 

<input type="hidden" name="issueDate" value="${issueVO.issueDate}"> 
<input type="hidden" name="issueLikeCount"
													value="${issueVO.issueLikeCount}"> 
<input class="btn btn-success" type="submit" value="確定修改"> 

<a
													href="<%=request.getContextPath()%>/issue/issue.do?issueID=${issueVO.issueID}&action=getOne_For_Display"
													class="btn btn-danger btn-md">取消</a> 
</div>
										</center> 
<script type="text/javascript">
	function getdatas() {
		var edit_text = CKEDITOR.instances.ckeditor.getSnapshot();
		console.log(edit_text);
	}
</script> 


</form> 
</div> 
</div> 
</div> 


<script
							src="<%=request.getContextPath()%>/front-end/js/jquery-3.2.1.min.js"></script> 
<script
							src="<%=request.getContextPath()%>/front-end/owl.carousel.min.js"></script> 
<script src="<%=request.getContextPath()%>/front-end/js/main.js"></script>	
<script src="https://code.jquery.com/jquery.js"></script> 
<script
							src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script> 

					</body> 
</html>