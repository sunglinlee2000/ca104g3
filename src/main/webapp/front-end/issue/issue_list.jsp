<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="java.util.*"%>
<%@ page import="com.issue.model.*"%>
<%@ page import="com.notificationList.model.*"%>
<%@ page import="com.member.model.*"%>

<%
	
	//	會員
	MemberVO memberVO = (MemberVO) session.getAttribute("memberVO");
	String memberID = memberVO.getMemberID();
	
	//	通知
	NotificationListService noSvc = new NotificationListService();
	List<NotificationListVO> nolists = noSvc.getAllNoList(memberID);
	pageContext.setAttribute("nolists",nolists);

  String boardID = request.getParameter("boardID");
  session.setAttribute("boardID", boardID);
  
  if(request.getAttribute("boardIDxx")!=null){
	  boardID = (String)request.getAttribute("boardIDxx");
  }

  
  IssueVO issueVO = (IssueVO) session.getAttribute("issueVO");
	
  response.setHeader("Cache-Control","no-store"); //HTTP 1.1
  response.setHeader("Pragma","no-cache");        //HTTP 1.0
  response.setDateHeader ("Expires", 0);
  
//   pageContext.setAttribute("testMemberID","M000001"); //假裝登入
%>

<%
//     IssueService issueSvc = new IssueService();
//     List<IssueVO> list = issueSvc.getAll();
//     pageContext.setAttribute("list",list);
    
    IssueService issueSvc = new IssueService();
	List<IssueVO> issuelist = issueSvc.getAllByBoardID(boardID);
	pageContext.setAttribute("issuelist",issuelist);
	

	
%>

<!DOCTYPE html>
<html lang="">
<head>
<meta charset="UTF-8">
<meta name="description" content="Food Blog Web Template">
<meta name="keywords" content="food, unica, creative, html">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>閒聊區</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
    <!--[if lt IE 9]>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>           
    <![endif]-->
    <!-- Favicon -->   
	<link href="img/favicon.ico" rel="shortcut icon"/>

	<!-- Google Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Poppins:400,400i,500,500i,600,600i,700" rel="stylesheet">

	<!-- Stylesheets -->
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/css/bootstrap.min.css"/>
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/css/font-awesome.min.css"/>
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/css/owl.carousel.css"/>
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/css/animate.css"/>
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/css/style.css"/>
	<!-- original -->			
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/issue/css/bootstrap.min.css">
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/issue/css/bootstrap-theme.min.css">
	<link rel="stylesheet" href="<%=request.getContextPath()%>/front-end/member/css/styleLogin.css">
	
	<script src="<%=request.getContextPath() %>/front-end/issue/js/jquery-3.2.1.min.js"></script>
	
	<script src="<%=request.getContextPath() %>/front-end/issue/js/bootstrap.min.js"></script>
	
	<link rel="stylesheet" href="<%=request.getContextPath() %>/front-end/issue/css/index.css">
	
	<!-- 按鈕 -->
	<link rel="stylesheet" type="text/css" href="http://yui.yahooapis.com/2.8.1/build/reset/reset-min.css"/>    
    <link rel="stylesheet" type="text/css" href="buttons.css"/>
    <!-- alert -->
   	<script src="<%=request.getContextPath() %>/front-end/issue/js/sweetalert2.all.min.js"></script>
     
<style type="text/css">        
body{
    font-family:\5FAE\8EDF\6B63\9ED1\9AD4,\65B0\7D30\660E\9AD4;
   	background-color: white;
  	font-family: 'Ubuntu', sans-serif;
}

.btn-primary,
.btn-primary:hover,
.btn-primary:active,
.btn-primary:visited,
.btn-primary:focus {
    background-color: #7dccff;
    border-color: #7dccff;
}

.thumbnail {
    display: block;
    padding: 0px;
    margin-bottom: 20px;
    line-height: 3.42857143;
	background-image: url('<%= request.getContextPath()%>/front-end/issue/images/gpp.png');
    border: 1px solid gray;
    border-radius: 7px 7px 7px 7px;
}

#memberPhoto2 {
    border: 3px solid #004b97;
    border-radius:  7px 7px 7px 7px; 
	}
	
html {
	background-color: #fbfbff;
	font: 62.5%/1 "Lucida Sans Unicode","Lucida Grande",Verdana,Arial,Helvetica,sans-serif;
}
body { padding: 20px 100px; }
#wrapper { text-align: center; }
   	
   	
/*It appears that the Pictos Font resets the line-height of the icon.. so if you are using them, delete the line below. */
.icon:before { line-height: .7em; }
   	
.button {
  background-color: #0d8fcc;
  border: none;
  color: #FFFFFF;
  text-align: center;
  padding: 12px;
  width: 320px;
  transition: all 0.5s;
  cursor: pointer;
  margin: 5px;
  border: 1px solid #0d8fcc;
  font-size: 35px;
  border-radius:  15px 15px 15px 15px;
}

.button span {
  cursor: pointer;
  display: inline-block;
  position: relative;
  transition: 0.5s;
}

.button span:after {
  content: '\00bb';
  position: absolute;
  opacity: 0;
  top: 0;
  right: -20px;
  transition: 0.5s;
}

.button:hover span {
  padding-right: 25px;
}

.button:hover span:after {
  opacity: 1;
  right: 0;
}

 /*       Just setting CSS for the page   */

* {
  margin: 0;
  padding: 0;
}

html,
css {
  width: 100%;
  height: 100%;
}

.position {
  margin-left: auto;
  margin-right: auto;
  text-align: center;
  margin-top: 15%;
}

#workarea {
  position: absolute;
  width: 100%;
  height: 100%;
  background-color: #1e1a3e;
  font-family: Raleway;
}

#personal {
  color:white;
  text-decoration:none;
  position:absolute;
  bottom:30px;
  right:2%;
}




#button {
        font-family: "Gill Sans", "Gill Sans MT", Calibri, sans-serif;
        position: absolute;
        font-size: 1.5em;
        text-transform: uppercase;
        padding: 7px 20px;
        left: 50%;
        width: 300px;
        margin-left: -150px;
        top: 74%;
        border-radius: 10px;
        color: white;
        text-shadow: -1px -1px 1px rgba(0,0,0,0.8);
        border: 5px solid transparent;
        border-bottom-color: rgba(0,0,0,0.35);
        background: #0d8fcc;
        cursor: pointer;
    outline: 0 !important;

        animation: pulse 1s infinite alternate;
        transition: background 0.4s, border 0.2s, margin 0.2s;
    }
    #button:hover {
        background: hsla(1670, 50%, 50%, 1);
        margin-top: -1px;

        animation: none;
    }
    #button:active {
        border-bottom-width: 0;
        margin-top: 5px;
    }
    @keyframes pulse {
        0% {
            margin-top: 0px;
        }
        100% {
            margin-top: 6px; 
        } 
    }
    

a {
  text-transform: uppercase;
  text-decoration: none;
  font-weight: 700;
}

@-webkit-keyframes top {
  from {
    -webkit-transform: translate(0rem, 0);
            transform: translate(0rem, 0);
  }
  to {
    -webkit-transform: translate(0rem, 3.5rem);
            transform: translate(0rem, 3.5rem);
  }
}

@keyframes top {
  from {
    -webkit-transform: translate(0rem, 0);
            transform: translate(0rem, 0);
  }
  to {
    -webkit-transform: translate(0rem, 3.5rem);
            transform: translate(0rem, 3.5rem);
  }
}
@-webkit-keyframes bottom {
  from {
    -webkit-transform: translate(-3.5rem, 0);
            transform: translate(-3.5rem, 0);
  }
  to {
    -webkit-transform: translate(0rem, 0);
            transform: translate(0rem, 0);
  }
}
@keyframes bottom {
  from {
    -webkit-transform: translate(-3.5rem, 0);
            transform: translate(-3.5rem, 0);
  }
  to {
    -webkit-transform: translate(0rem, 0);
            transform: translate(0rem, 0);
  }

}
.btn {
  position: relative;
  letter-spacing: 0.15em;
  margin: auto;
  padding: 1rem 2.5rem;
  background: transparent;
  outline: none;
  font-size: 160px;
  color: #111111;
  transition: all 0.5s cubic-bezier(0.68, -0.55, 0.265, 1.55) 0.15s;
}
.btn::after, .btn::before {
  content: "";
  position: absolute;
  height: 40%;
  width: 10%;
  transition: all 0.5s cubic-bezier(0.68, -0.55, 0.265, 1.55);
  z-index: -2;
  border-radius: 50%;
}
.btn::before {
  background-color: #7dccff;
  top: -0.75rem;
  left: 0.5rem;
  -webkit-animation: top 2s cubic-bezier(0.68, -0.55, 0.265, 1.55) 0.25s infinite alternate;
          animation: top 2s cubic-bezier(0.68, -0.55, 0.265, 1.55) 0.25s infinite alternate;
}
.btn::after {
  background-color: #2177af;
  top: 3rem;
  left: 13rem;
  -webkit-animation: bottom 2s cubic-bezier(0.68, -0.55, 0.265, 1.55) 0.5s infinite alternate;
          animation: bottom 2s cubic-bezier(0.68, -0.55, 0.265, 1.55) 0.5s infinite alternate;
}
.btn:hover {
  color: white;
}
.btn:hover::before, .btn:hover::after {
  top: 0;
  height: 100%;
  width: 100%;
  border-radius: 0;
  -webkit-animation: none;
          animation: none;
}
.btn:hover::after {
  left: 0rem;
}
.btn:hover::before {
  top: 0.5rem;
  left: 0.35rem;
}

.kk-no-list{
   height:auto;
   border:1px solid;
   border-color: #E3E0E0;
   background-color: #E3E0E0;
   margin: 2px;
   padding: 2px;
   font-size:15px;
}

.kk-modal-body{
   height:300px;
   overflow-y: scroll;
   overflow-x: hidden;

}
</style>
   
</head>
<body>
<jsp:useBean id="MemberService" scope="page" class="com.member.model.MemberService"/>

<!-- Header section --> 
 <header class="header-section">	
  <div class="header-bottom"> 
   <div class="container"> 
    <a href="<%=request.getContextPath()%>/front-end/index.jsp" class="site-logo"> 
     <img src="<%=request.getContextPath() %>/front-end/img/logo-1.png" alt=""> 
    </a> 
    <div class="nav-switch"> 
     <i class="fa fa-bars"></i> 
    </div> 

    <ul class="main-menu"> 
     <li><a href="<%= request.getContextPath()%>/front-end/board/listAllBoard.jsp"><img alt="藝人專板"  title="藝人專板" src="<%= request.getContextPath()%>/front-end/res/img/index/board.png" style="height:30px;width:30px"></a></li> 
     <li><a href="<%= request.getContextPath()%>/front-end/product/Mall.jsp"><img alt="商城"  title="商城" src="<%= request.getContextPath()%>/front-end/res/img/index/mall.png" style="height:30px;width:30px"></a></li> 
     <li><a href="<%=request.getContextPath()%>/front-end/gif_collection/gif_list.jsp"><img alt="GIF"  title="GIF" src="<%= request.getContextPath()%>/front-end/res/img/index/gif.png" style="height:30px;width:30px"></a></li> 
     <li><a href="<%=request.getContextPath()%>/front-end/member/listOneMember.jsp"><img alt="會員資料" title="會員資料" src="<%= request.getContextPath()%>/front-end/res/img/index/user.png" style="height:30px;width:30px"></a></li> 
     <li><a href="#" onclick="logout()"><img alt="登出" title="登出" src="<%= request.getContextPath()%>/front-end/res/img/index/logout.png" style="height:30px;width:30px"></a> 
     <li><a href="<%=request.getContextPath()%>/front-end/chatBox/chatBox.jsp"><img alt="聊天室" title="聊天室" src="<%= request.getContextPath()%>/front-end/res/img/index/chat.png" style="height:30px;width:30px"></a></li> 
     <li>${memberVO.memberNickName }</li>
     <li><a href="<%=request.getContextPath()%>/front-end/personalPage/myPersonalPage.jsp"><img src="<%= request.getContextPath()%>/front-end/member/memberImg.do?memberID=${memberVO.memberID}" id="memberPhoto"></a></li>	
     <li id="notifi"><a href="#modal-id-notifi" data-toggle="modal"> 
      <div style="position: relative;"> 
      <img src="<%=request.getContextPath()%>/front-end/res/img/index/notification(32).png" alt="通知" id="notifiPhoto"> 
      <span style="position: absolute; top: 0; left: 17px; color: white" id="notifiCount"></span> 
      </div> 
     </a></li> 
     </ul>
   </div> 
  </div> 
 </header> 
 <!-- Header section end -->
<FORM METHOD="post" ACTION="<%=request.getContextPath()%>/front-end/member/member.do" id="logout">
	<input type="hidden" name="action"	value="logOut">
</FORM>
<script type="text/javascript">
	function logout(){
		document.getElementById("logout").submit();
	}
</script>


        
            <div class="container">
                <div class="row">           
                    <div class="col-xs-12 col-sm-12">                 
                            <div class="page-header">                      
                                    <div class="input-group input-group-lg">
                                        <div class="input-group-btn">
                                        	
<a href="<%=request.getContextPath() %>/front-end/board/listAllBoard.jsp" class="btn pull-left btn-lg" ><b>◁ previous page</b></a>


<!--                                             <a href="#" class="btn btn-primary dropdown-toggle" data-toggle="dropdown"> Twice <b class="caret"></b></a> -->
<!--                                             <ul class="dropdown-menu"> -->
<!--                                                 <li><a href="#">Twice</a></li> -->
<!--                                                 <li><a href="#">娜璉</a></li> -->
<!--                                                 <li><a href="#">定延</a></li> -->
<!--                                                 <li><a href="#">Momo</a></li> -->
<!--                                                 <li><a href="#">Sana</a></li> -->
<!--                                                 <li><a href="#">志效</a></li> -->
<!--                                                 <li><a href="#">Mina</a></li> -->
<!--                                                 <li><a href="#">彩瑛</a></li> -->
<!--                                                 <li><a href="#">多賢</a></li> -->
<!--                                                 <li><a href="#">子瑜</a></li> -->
<!--                                             </ul> -->

<a href="<%=request.getContextPath() %>/front-end/issue/issue_windowadd.jsp?boardID=${issueVO.boardID}" class="btn pull-right btn-lg" ><b> Write a post ▷</b></a>                                                  
                                                                        
                                 </div>                       
                             </div>
                        </div>
                        	 
					<c:forEach var="issueVO" items="${issuelist}">		                      
                        <div class="col-xs-12 col-sm-4" >
                       
                            <div class="thumbnail">
                                
                                <div class="caption">
                               		<a href="<%=request.getContextPath()%>/personalPage/personalPage.do?memberID=${issueVO.memberID}&action=toOnePersonalPage">                       
			                        	<img src="<%= request.getContextPath()%>/front-end/member/memberImg.do?memberID=${issueVO.memberID}" class="" style="height: 200px; width: 350px; margin: auto;" id="memberPhoto2">
			                        </a>

<%--會員--%>                        <font color="#004b97"><h3> ${MemberService.getOneMember(issueVO.memberID).memberName}</h3></font>
<!--標題 -->                        <font color="#000000"><h4><b>${issueVO.issueTitle}</b></h4></font>
									
                                                   
                                    <div class="btn-group">
									   

											
											
<%-- 									    <a href="<%=request.getContextPath()%>/issue/issue.do?issueID=${issueVO.issueID}&boardID=${sessionScope.boardID}&action=getOne_For_Display" class=""><button class="button"><span>More </span></button></a> --%>
									 
									 
									 
									 
<%-- 									  	<c:if test="${memberVO.memberID == issueVO.memberID }">                              	 --%>
<%-- 	                                        <a href="<%=request.getContextPath()%>/front-end/issueReport/addEmp.jsp?issueID=${issueVO.issueID}" class=" btn pull-right" style="display:none;">		                                      		                                        	 --%>
<%--                                         		<img src="<%=request.getContextPath() %>/front-end/issue/images/004-alert.png" alt="">		                                        		                                        	 --%>
<!-- 	                                        </a> -->
<%--                							</c:if> --%>
<%--                							<c:if test="${memberVO.memberID != issueVO.memberID }">                              	 --%>
<%-- 	                                        <a href="<%=request.getContextPath()%>/front-end/issueReport/addEmp.jsp?issueID=${issueVO.issueID}" class=" btn pull-right" >		                                           --%>
<%-- 	                                        	<img src="<%=request.getContextPath() %>/front-end/issue/images/004-alert.png" alt="">			                                       		                                        --%>
<!-- 	                                        </a> -->
<%--                							</c:if>  --%>
               							
									</div>
									
<!--                                     <p>  -->
<%--                                     	<a href="#" class="  btn pull-left"><img src="<%=request.getContextPath() %>/front-end/issue/images/001-heart2.png" alt=""></a> --%>
                                    	        
<%--                                         <c:if test="${memberVO.memberID == issueVO.memberID }">                              	 --%>
<%--                                         <a href="<%=request.getContextPath()%>/front-end/issueReport/addEmp.jsp?issueID=${issueVO.issueID}" class=" btn pull-left" style="display:none;"><img src="<%=request.getContextPath() %>/front-end/issue/images/004-warning.png" alt=""></a> --%>
<%--                							</c:if> --%>
<%--                							<c:if test="${memberVO.memberID != issueVO.memberID }">                              	 --%>
<%--                                         <a href="<%=request.getContextPath()%>/front-end/issueReport/addEmp.jsp?issueID=${issueVO.issueID}" class=" btn pull-left" ><img src="<%=request.getContextPath() %>/front-end/issue/images/004-warning.png" alt=""></a> --%>
<%--                							</c:if>  --%>
               							
                                     	
<%--                                         <h5>${issueVO.issueLikeCount}</h5>                                                            --%>
<!--                                     </p> -->
										
									    <a href="<%=request.getContextPath()%>/issue/issue.do?issueID=${issueVO.issueID}&boardID=${sessionScope.boardID}&action=getOne_For_Display" class=""><button id="button"><b><h2>Read More  ➤</h2></b></button>
										<canvas id="myCanvas" width="50" height="80"></canvas></a>
                                </div>
                            </div>
                        </div>
                        
 					</c:forEach>


                </div>
            </div>
        </div> 
        
    <div style="text-align:center;clear:both;">
	<script src="/gg_bd_ad_720x90.js" type="text/javascript"></script>
	<script src="/follow.js" type="text/javascript"></script>
	</div> 
	<script src="<%=request.getContextPath() %>/front-end/js/jquery-3.2.1.min.js"></script>
	<script src="<%=request.getContextPath() %>/front-end/owl.carousel.min.js"></script>
	<script src="<%=request.getContextPath() %>/front-end/js/main.js"></script>		
   	<script src="https://code.jquery.com/jquery.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
	
	
			<!-- 通知列表的跳窗 -->
		<div class="modal fade" id="modal-id-notifi">
			<div class="modal-dialog modal-sm">
			   <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<div class="modal-content">
				   
					<div class="modal-header">					
						<h4 class="modal-title">通知列表</h4>
					</div>
					<div class="modal-body kk-modal-body" >

                    <!-- 及時通知顯示的地方 -->
					<div id="nolistsssss" class="kk-no-list">
					  <a href="<%=request.getContextPath()%>/adfundTest/adfund.do?action=getOne_For_Display&adfundID=${adfundVO.adfundID}" ></a>
					 </div>
                        
						<c:forEach var="NotificationListVO" items="${nolists}" >
					      <div class="kk-no-list">					      
					         <a href="<%=request.getContextPath()%>${NotificationListVO.notificationpath}${NotificationListVO.notificationtarget}" id="${NotificationListVO.notificationid}">${NotificationListVO.notificationcontent}</a>
					         <h6><fmt:formatDate value="${NotificationListVO.notificationdate}" pattern="yyyy-MM-dd HH:mm"/></h6>
					      </div>
					    </c:forEach>
					</div>
					<div class="modal-footer">

					</div>
				</div>
			</div>
		</div>
	<!-- 通知的跳窗 -->

<script>

$(document).ready(function() { 
	
	//點擊鈴鐺時，通知要全部變為已讀
    $('#notifi').click(function(){
    	
//              alert("!!")
				$.ajax({ 
				     url : '<%=request.getContextPath()%>/front-end/NotificationListServlet/NotificationListServlet.do', 
				     type : 'Post', 
				     data : { 
				      action : 'isClick', 
				      memberID : '${memberVO.memberID}' 

				     } 
				    }).done(function(res){
				    	var obj = JSON.parse(res);
				    	document.getElementById("notifiPhoto").src = "<%=request.getContextPath()%>/front-end/res/img/index/notification(32).png";
				    	$('#notifiCount').html("");
	                });				     
     });
 
	
//    alert("noti!!");
    //網頁一打開時，要立刻判斷是否有未讀訊息，看是否要顯示有消息的鈴鐺
    $.ajax({ 
	     url : '<%=request.getContextPath()%>/front-end/NotificationListServlet/NotificationListServlet.do', 
	     type : 'Post', 
	     data : { 
	      action : 'isRead',  
	      memberID : '${memberVO.memberID}'
	     } 
	    }).done(function(res){
	    	var obj = JSON.parse(res);
	    	if(obj.notiStatus==false){
	    	   document.getElementById("notifiPhoto").src = "<%=request.getContextPath()%>/front-end/res/img/index/notification(32)red.png";
	    	   $('#notifiCount').html(obj.falseCount);
	    	}
	    });
    
});
</script>
    </body>
</html>