package jedis;

public class ChatMessage {
	private String type;
	private String chatBoxID;
	private String memberNickName;
	private String messageContent;

	public ChatMessage(String type, String chatBoxID, String memberNickName, String messageContent) {
		super();
		this.type = type;
		this.chatBoxID = chatBoxID;
		this.memberNickName = memberNickName;
		this.messageContent = messageContent;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getChatBoxID() {
		return chatBoxID;
	}

	public void setChatBoxID(String chatBoxID) {
		this.chatBoxID = chatBoxID;
	}

	public String getMemberNickName() {
		return memberNickName;
	}

	public void setMemberNickName(String memberNickName) {
		this.memberNickName = memberNickName;
	}

	public String getMessageContent() {
		return messageContent;
	}

	public void setMessageContent(String messageContent) {
		this.messageContent = messageContent;
	}

}
