package jdbc.util.CompositeQuery_adfund;

import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class jdbcUtil_CompositeQuery_adfund {
	
	public static String get_aCondition_For_Oracle(String columnName, String value) {

		String aCondition = null;

		if ("adfundID".equals(columnName) || "memberID".equals(columnName) || "memberID".equals(columnName) || "administratorID".equals(columnName)
				|| "adfundTitle".equals(columnName) || "adfundType".equals(columnName) || "adfundText".equals(columnName) || "adfundStatus".equals(columnName)) // 用於varchar
			aCondition = columnName + " like '%" + value.toUpperCase() + "%'";			
		else if ("adfundCoinTarget".equals(columnName) || "adfundCoinNow".equals(columnName)) // 用於其他
			aCondition = columnName + "=" + value;
//		else if ("adfundDisplayDate".equals(columnName))                          // 用於Oracle的date
//			aCondition = "to_char(" + columnName + ",'yyyy-mm-dd')='" + value + "'";

		return aCondition + " ";
	}

	public static String get_WhereCondition(Map<String, String[]> map) {
		Set<String> keys = map.keySet();
		StringBuffer whereCondition = new StringBuffer();
		int count = 0;
		for (String key : keys) {
			String value = map.get(key)[0];
			if (value != null && value.trim().length() != 0	&& !"action".equals(key)) {
				count++;
				String aCondition = get_aCondition_For_Oracle(key, value.trim());

				if (count == 1)
					whereCondition.append(" where " + aCondition);
				else
					whereCondition.append(" and " + aCondition);

				System.out.println("有送出查詢資料的欄位數count = " + count);
			}
		}
		
		return whereCondition.toString();
	}

	public static void main(String argv[]) {

		// 配合 req.getParameterMap()方法 回傳 java.util.Map<java.lang.String,java.lang.String[]> 之測試
		Map<String, String[]> map = new TreeMap<String, String[]>();
		map.put("adfundID", new String[] { "AD000001" });
		map.put("memberID", new String[] { "M000001" });
		map.put("artistID", new String[] { "ART000001" });
		map.put("administratorID", new String[] { "A000001" });
		map.put("adfundTitle", new String[] { "TWICE" });
		map.put("adfundType", new String[] { "OTHER" });
		map.put("adfundText", new String[] { "TWICE" });
		map.put("adfundStatus", new String[] { "AD_FUNDING" });
		map.put("adfundCoinTarget", new String[] { "3000" });
		map.put("adfundCoinNow", new String[] { "1500" });
		map.put("action", new String[] { "getXXX" }); // 注意Map裡面會含有action的key

		String finalSQL = "select * from adfund "
				          + jdbcUtil_CompositeQuery_adfund.get_WhereCondition(map)
				          + " order by adfunddisplaydate";
		System.out.println("●●finalSQL = " + finalSQL);

	}

}
