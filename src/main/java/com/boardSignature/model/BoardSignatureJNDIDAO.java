package com.boardSignature.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

public class BoardSignatureJNDIDAO implements BoardSignatureDAO_interface{
	
	private static DataSource ds = null;
	static {
		try {
			Context ctx = new InitialContext();
			ds = (DataSource) ctx.lookup("java:comp/env/jdbc/CA104G3DB");
		} catch (NamingException e) {
			e.printStackTrace();
		}
	}
	
	private static final String INSERT_STMT = "INSERT INTO BOARDSIGNATURE VALUES(?,?)";
	private static final String DELETE = "DELETE FROM boardSignature WHERE boardID=? AND memberID=?";
	private static final String GET_ONE_STMT = "SELECT * FROM boardSignature WHERE boardID=? AND memberID=?";
	private static final String GET_MEMBERS_STMT = "SELECT * FROM boardSignature WHERE boardID=?";
	
	
	@Override
	public void insert(BoardSignatureVO boardSignatureVO) {
		Connection con = null;
		PreparedStatement pstmt = null;
		
		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(INSERT_STMT);

			pstmt.setString(1, boardSignatureVO.getBoardID());
			pstmt.setString(2, boardSignatureVO.getMemberID());

			pstmt.executeUpdate();		

		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
	}
	

	@Override
	public void delete(String boardID, String memberID) {
		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(DELETE);
			
			pstmt.setString(1,boardID);
			pstmt.setString(2,memberID);

			pstmt.executeUpdate();

			
		} catch (SQLException se) {
			if (con != null) {
				try {
					con.rollback();
				} catch (SQLException excep) {
					throw new RuntimeException("rollback error occured. "
							+ excep.getMessage());
				}
			}
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
	}

	@Override
	public BoardSignatureVO findByPrimaryKey(String boardID, String memberID) {
		BoardSignatureVO boardSignatureVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ONE_STMT);

			pstmt.setString(1, boardID);
			pstmt.setString(2, memberID);

			rs = pstmt.executeQuery();

			while (rs.next()) {
				boardSignatureVO = new BoardSignatureVO();
				boardSignatureVO.setBoardID(rs.getString("boardID"));
				boardSignatureVO.setMemberID(rs.getString("memberID"));

			}

		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return boardSignatureVO;
	}

	@Override
	public Set<BoardSignatureVO> getMemberByBoardID(String boardID) {
		Set<BoardSignatureVO> set = new LinkedHashSet<BoardSignatureVO>();
		BoardSignatureVO boardSignatureVO = null;
		
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		try {
			
			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_MEMBERS_STMT);
			pstmt.setString(1, boardID);
			rs = pstmt.executeQuery();
	
			while (rs.next()) {
				boardSignatureVO = new BoardSignatureVO();
				boardSignatureVO.setBoardID(rs.getString("boardID"));
				boardSignatureVO.setMemberID(rs.getString("memberID"));
				set.add(boardSignatureVO); // Store the row in the vector
			}
	
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		
		return set;
	}
}
