package com.boardSignature.model;
import java.util.Set;

public interface BoardSignatureDAO_interface {
	public void insert(BoardSignatureVO boardSignatureVO);
	public void delete(String boardID,String memberID);
	public BoardSignatureVO findByPrimaryKey(String boardID,String memberID);
	public Set<BoardSignatureVO> getMemberByBoardID(String boardiD);
}
