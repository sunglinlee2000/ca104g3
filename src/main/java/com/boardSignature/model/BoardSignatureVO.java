package com.boardSignature.model;

public class BoardSignatureVO implements java.io.Serializable{
	private String	boardID;
	private String  memberID;
	
	public String getBoardID() {
		return boardID;
	}
	public void setBoardID(String boardID) {
		this.boardID = boardID;
	}
	public String getMemberID() {
		return memberID;
	}
	public void setMemberID(String memberID) {
		this.memberID = memberID;
	}
	
}
