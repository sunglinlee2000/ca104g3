package com.boardSignature.model;

import java.util.Set;

public class BoardSignatureTest {
	
	public static void main(String[] args) {
		BoardSignatureJDBCDAO boardSignatureJDBCDAO = new BoardSignatureJDBCDAO();
//		1.------------------------
//		BoardSignatureVO boardSignatureVO = new BoardSignatureVO();
//		boardSignatureVO.setBoardID("B000002");
//		boardSignatureVO.setMemberID("M000002");
//		boardSignatureJDBCDAO.insert(boardSignatureVO);
//		
//		System.out.println("OK");
		
//		2.------------------------
//		BoardSignatureVO boardSignatureVO2 = new BoardSignatureVO();
//		boardSignatureVO2.setBoardID("B000001");
//		boardSignatureVO2.setMemberID("M000002");
//		boardSignatureJDBCDAO.update(boardSignatureVO2);
//		
//		System.out.println("OK");
		
//		3.------------------------
//		boardSignatureJDBCDAO.delete("B000001", "M000002");
//				
//		System.out.println("OK");
		
//		4.---------------------
//		BoardSignatureVO boardSignatureVO3 = boardSignatureJDBCDAO.findByPrimaryKey("B000002", "M000002");
//		System.out.println(boardSignatureVO3.getBoardID());
//		System.out.println(boardSignatureVO3.getMemberID());
//		System.out.println("==================================");
//		System.out.println("OK");
		
//		5.---------------------
		Set<BoardSignatureVO> set = boardSignatureJDBCDAO.getMemberByBoardID("B000001");
		for(BoardSignatureVO boardSignatureVO4 : set) {
			System.out.println(boardSignatureVO4.getBoardID());
			System.out.println(boardSignatureVO4.getMemberID());
			System.out.println("==================================");
		}
		System.out.println("OK");
	}
}
