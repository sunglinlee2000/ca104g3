package com.boardSignature.model;

import java.util.Set;

public class BoardSignatureService {

	private BoardSignatureDAO_interface dao;
	
	public BoardSignatureService() {
		dao = new BoardSignatureDAO();
	}

	public BoardSignatureVO addSignature(String boardID,String memberID) {
		BoardSignatureVO boardSignatureVO = new BoardSignatureVO();
		
		boardSignatureVO.setBoardID(boardID);
		boardSignatureVO.setMemberID(memberID);
		dao.insert(boardSignatureVO);
				
		return boardSignatureVO;
	}
	
	
	public void deleteSignature(String boardID,String memberID) {
		dao.delete(boardID,memberID);
	}	
	
	public BoardSignatureVO getOneSignature(String boardID,String memberID) {
		return dao.findByPrimaryKey(boardID, memberID);
	}
	
	public Set<BoardSignatureVO> getByBoard(String boardID) {
		return dao.getMemberByBoardID(boardID);
	}
}
