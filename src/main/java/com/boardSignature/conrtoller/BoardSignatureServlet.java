package com.boardSignature.conrtoller;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.boardSignature.model.*;


public class BoardSignatureServlet extends HttpServlet{
	private static final long serialVersionUID = 1L;
	public void doGet(HttpServletRequest req,HttpServletResponse res) 
			throws ServletException,IOException{
		doPost(req,res);
	}

	public void doPost(HttpServletRequest req,HttpServletResponse res) 
			throws ServletException,IOException {
		
		req.setCharacterEncoding("UTF-8");
		res.setContentType("text/html; charset=UTF-8");
		String action = req.getParameter("action");
		

        if ("insert".equals(action)) { 
			
			List<String> errorMsgs = new LinkedList<String>();
			req.setAttribute("errorMsgs", errorMsgs);

			try {
				/***********************1.接收請求參數 - 輸入格式的錯誤處理*************************/
				String boardID = req.getParameter("boardID");
				String memberID = req.getParameter("memberID");
				
				BoardSignatureVO boardSignatureVO = new BoardSignatureVO();
				boardSignatureVO.setBoardID(boardID);
				boardSignatureVO.setMemberID(memberID);
				
				// Send the use back to the form, if there were errors
				if (!errorMsgs.isEmpty()) {
					req.setAttribute("boardSignatureVO", boardSignatureVO); // 含有輸入格式錯誤的empVO物件,也存入req
					RequestDispatcher failureView = req
							.getRequestDispatcher("/front-end/index.jsp");
					failureView.forward(req, res);
					return;
				}
				
				/***************************2.開始新增資料***************************************/
				BoardSignatureService boardSignatureSvc = new BoardSignatureService();
				boardSignatureVO = boardSignatureSvc.addSignature(boardID, memberID);
				/***************************3.新增完成,準備轉交(Send the Success view)***********/
				String url = "/front-end/board/listAllApplying.jsp";
				RequestDispatcher successView = req.getRequestDispatcher(url); // 新增成功後轉交listAllEmp.jsp
				successView.forward(req, res);				
				/***************************其他可能的錯誤處理**********************************/
			} catch (Exception e) {
				e.printStackTrace(System.err);
				errorMsgs.add(e.getMessage());
				RequestDispatcher failureView = req
						.getRequestDispatcher("/front-end/index.jsp");
				failureView.forward(req, res);
			}
		}
		
		
		if ("delete".equals(action)) { 

			List<String> errorMsgs = new LinkedList<String>();
			req.setAttribute("errorMsgs", errorMsgs);
	
			try {
				/***************************1.接收請求參數***************************************/
				String boardID = new String(req.getParameter("boardID"));
				String memberID = new String(req.getParameter("memberID"));
				
				/***************************2.開始刪除資料***************************************/
				BoardSignatureService boardSignatureSvc = new BoardSignatureService();
				boardSignatureSvc.deleteSignature(boardID, memberID);;
				
				/***************************3.刪除完成,準備轉交(Send the Success view)***********/								
				String url = "/front-end/board/listAllApplying.jsp";
				RequestDispatcher successView = req.getRequestDispatcher(url);// 刪除成功後,轉交回送出刪除的來源網頁
				successView.forward(req, res);
				
				/***************************其他可能的錯誤處理**********************************/
			} catch (Exception e) {
				errorMsgs.add("刪除資料失敗:"+e.getMessage());
				RequestDispatcher failureView = req
						.getRequestDispatcher("/front-end/index.jsp");
				failureView.forward(req, res);
			}
		}
		
		if("joinSignature".equals(action)) {
			
			List<String> errorMsgs = new LinkedList<String>();
			req.setAttribute("errorMsgs", errorMsgs);

			try {
				/***************************1.接收請求參數***************************************/
				String boardID = new String(req.getParameter("boardID"));
				String memberID = new String(req.getParameter("memberID"));
				
				/***************************2.開始查詢資料***************************************/
				BoardSignatureService boardSignatureSvc = new BoardSignatureService();
				BoardSignatureVO boardSignatureVO = boardSignatureSvc.getOneSignature(boardID, memberID);
				
				/***************************3.查詢完成,準備轉交(Send the Success view)***********/	
				if(boardSignatureVO == null) {
					Boolean fail = false;
					boardSignatureSvc.addSignature(boardID, memberID);
					req.setAttribute("fail", fail);
					String url = "/front-end/board/listAllApplying.jsp";
					RequestDispatcher successView = req.getRequestDispatcher(url);
					successView.forward(req, res);
				} else {
					Boolean fail = true;
					req.setAttribute("fail", fail);
					String url = "/front-end/board/listAllApplying.jsp";
					RequestDispatcher successView = req.getRequestDispatcher(url);
					successView.forward(req, res);
				}
							

				
				/***************************其他可能的錯誤處理**********************************/
			} catch (Exception e) {
				errorMsgs.add("查詢失敗:"+e.getMessage());
				RequestDispatcher failureView = req
						.getRequestDispatcher("/front-end/index.jsp");
				failureView.forward(req, res);
			}
	
		}
		
	}
	
	
}
