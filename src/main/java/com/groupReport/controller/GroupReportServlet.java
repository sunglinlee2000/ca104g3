package com.groupReport.controller;

import java.io.*;
import java.util.*;
import java.util.Date;
import java.sql.*;

import javax.servlet.*;
import javax.servlet.http.*;

import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.groupList.model.*;
import com.groupReport.model.*;
import com.member.model.MemberService;
import com.notificationList.model.NotificationListService;

public class GroupReportServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		doPost(req, res);
	}

	public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {

		req.setCharacterEncoding("UTF-8");
		String action = req.getParameter("action");
		res.setHeader("Cache-Control", "no-store");
		res.setHeader("Pragma", "no-cache");
		res.setDateHeader("Expires", 0);

		if ("doubleReport".equals(action)) {
			String groupID = req.getParameter("groupID");
			String memberID = req.getParameter("memberID");
			GroupReportService groupReportSvc = new GroupReportService();
			GroupReportVO groupReportVO = groupReportSvc.getOneGroupReport(groupID, memberID);
			JSONObject obj = new JSONObject();
			if (groupReportVO != null) {
				try {
					obj.put("doubleReport", "doubleReport");
				} catch (JSONException e) {
					e.printStackTrace();
				}
			} else {
				try {
					obj.put("doubleReport", "safe");
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
			res.setContentType("text/plain");
			res.setCharacterEncoding("UTF-8");
			PrintWriter out = res.getWriter();
			out.write(obj.toString());
			out.flush();
			out.close();
		}

		if ("group_report".equals(action)) {

			List<String> errorMsgs = new LinkedList<String>();
			req.setAttribute("errorMsgs", errorMsgs);

			try {
//				1.錯誤處理

				String groupID = req.getParameter("groupID");
				String memberID = req.getParameter("memberID");
				String groupReportContent = req.getParameter("groupReportContent").trim();
				String groupReportReason = req.getParameter("groupReportReason");

				if ("OTHER".equals(groupReportReason) && groupReportContent.length() == 0) {
					errorMsgs.add("選擇「其他」需填寫詳細檢舉內容！");
				}

				if (!errorMsgs.isEmpty()) {
					GroupListService groupListSvc = new GroupListService();
					GroupListVO groupListVO = groupListSvc.getOneGroup(groupID);
					req.setAttribute("groupListVO", groupListVO);
					RequestDispatcher failureView = req.getRequestDispatcher("/front-end/groupList/listOneGroup.jsp");
					failureView.forward(req, res);
					return;
				}

				/*************************** 2.開始新增資料 ***************************************/
				GroupReportService groupReportSvc = new GroupReportService();
				groupReportSvc.addGroupReport(groupID, memberID, groupReportContent, groupReportReason);

				/*************************** 3.新增完成,準備轉交(Send the Success view) ***********/
				String url = "/front-end/groupList/listAllGroup.jsp";
				RequestDispatcher successView = req.getRequestDispatcher(url);
				successView.forward(req, res);

				/*************************** 其他可能的錯誤處理 **********************************/
			} catch (Exception e) {
				errorMsgs.add(e.getMessage());
				RequestDispatcher failureView = req.getRequestDispatcher("/front-end/groupList/listOneGroup.jsp");
				failureView.forward(req, res);
			}

		}

		if ("showGroupReport".equals(action)) {
			Gson gson = new Gson();
//			System.out.println("收到！");
			String groupReportID = req.getParameter("groupReportID");
//			System.out.println(groupReportID);

			/*************************** 2.開始查詢資料 ***************************************/
			GroupReportService groupReportSvc = new GroupReportService();
			GroupReportVO groupReportVO = groupReportSvc.getOneGroupReport(groupReportID);

			/*************************** 3.新增完成,準備轉交(Send the Success view) ***********/
			String jsonObj = gson.toJson(groupReportVO);
			res.setContentType("text/plain");
			res.setCharacterEncoding("UTF-8");
			PrintWriter out = res.getWriter();
			out.write(jsonObj);
			out.flush();
			out.close();

		}

		if ("reportAccept".equals(action)) {

			String groupReportID = req.getParameter("groupReportID");
			String groupID = req.getParameter("groupID");
//			System.out.println(groupID);
			String groupReportStatus = req.getParameter("groupReportStatus");

			/*************************** 2.開始新增資料 ***************************************/

			GroupReportService groupReportSvc = new GroupReportService();
			groupReportSvc.update_reportAccept(groupReportID, groupID, groupReportStatus); 
			
//			新增通知
			GroupListService groupListSvc = new GroupListService();
			String memberID = groupListSvc.getOneGroup(groupID).getMemberID();
			String groupTitle = groupListSvc.getOneGroup(groupID).getGroupTitle();
			String notificationContent =  "您的揪團：" + groupTitle + "，經檢舉、審核通過後已被刪除。";
			Timestamp now = new Timestamp(new Date().getTime());
			NotificationListService notificationListSvc = new NotificationListService();
			notificationListSvc.addNoList("NT000001", memberID, notificationContent, now, memberID, "FALSE", 
					"/personalPage/personalPage.do?action=toOnePersonalPage&memberID=");

		}

		if ("reportReject".equals(action)) {

			String groupReportID = req.getParameter("groupReportID");
			String groupID = req.getParameter("groupID");
			String groupReportStatus = req.getParameter("groupReportStatus");

			/*************************** 2.開始新增資料 ***************************************/

			GroupReportService groupReportSvc = new GroupReportService();
			groupReportSvc.update_reportReject(groupReportID, groupID, groupReportStatus);

		}
	}

}
