package com.groupReport.model;

import java.sql.Timestamp;
import java.util.List;

public class GroupReportService {

	private GroupReportDAO_interface dao;

	public GroupReportService() {
		dao = new GroupReportDAO();
	}

	public GroupReportVO addGroupReport(String groupID, String memberID, String groupReportContent, String groupReportReason) {

		GroupReportVO groupReportVO = new GroupReportVO();

		groupReportVO.setGroupID(groupID);
		groupReportVO.setMemberID(memberID);
//		groupReportVO.setAdministratorID(administratorID);
		groupReportVO.setGroupReportContent(groupReportContent);
//		groupReportVO.setGroupReportDate(groupReportDate);
//		groupReportVO.setGroupReportStatus(groupReportStatus);
		groupReportVO.setGroupReportReason(groupReportReason);
		dao.insert(groupReportVO);

		return groupReportVO;
	}

	public GroupReportVO update_reportAccept(String groupReportID, String groupID, String groupReportStatus) {

		GroupReportVO groupReportVO = new GroupReportVO();

		groupReportVO.setGroupReportID(groupReportID);
		groupReportVO.setGroupID(groupID);
//		groupReportVO.setMemberID(memberID);
//		groupReportVO.setAdministratorID(administratorID);
//		groupReportVO.setGroupReportContent(groupReportContent);
//		groupReportVO.setGroupReportDate(groupReportDate);
		groupReportVO.setGroupReportStatus(groupReportStatus);
//		groupReportVO.setGroupReportReason(groupReportReason);
		dao.update_reportAccept(groupReportVO);

		return groupReportVO;
	}
	
	public GroupReportVO update_reportReject(String groupReportID, String groupID, String groupReportStatus) {

		GroupReportVO groupReportVO = new GroupReportVO();

		groupReportVO.setGroupReportID(groupReportID);
		groupReportVO.setGroupID(groupID);
//		groupReportVO.setMemberID(memberID);
//		groupReportVO.setAdministratorID(administratorID);
//		groupReportVO.setGroupReportContent(groupReportContent);
//		groupReportVO.setGroupReportDate(groupReportDate);
		groupReportVO.setGroupReportStatus(groupReportStatus);
//		groupReportVO.setGroupReportReason(groupReportReason);
		dao.update_reportReject(groupReportVO);

		return groupReportVO;
	}

	public GroupReportVO getOneGroupReport(String groupReportID) {
		return dao.findByPrimaryKey(groupReportID);
	}
	
	public GroupReportVO getOneGroupReport(String groupID, String memberID) {
		return dao.findByGroupIDnMemberID(groupID, memberID);
	}

	public List<GroupReportVO> getAll_unchecked() {
		return dao.getAll_unchecked();
	}
	
	public List<GroupReportVO> getAll_accepted() {
		return dao.getAll_accepted();
	}
	
	public List<GroupReportVO> getAll_rejected() {
		return dao.getAll_rejected();
	}

	public Integer getAll_unchecked_count() {
		return dao.getAll_unchecked_count();
	}
}
