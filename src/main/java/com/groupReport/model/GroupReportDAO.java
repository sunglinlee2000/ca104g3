package com.groupReport.model;

import java.util.*;
import java.sql.*;
import javax.naming.*;
import javax.sql.*;

import com.groupList.model.GroupListService;

public class GroupReportDAO implements GroupReportDAO_interface {
	private static DataSource ds = null;
	static {
		try {
			Context ctx = new InitialContext();
			ds = (DataSource) ctx.lookup("java:comp/env/jdbc/CA104G3DB");
		} catch (NamingException e) {
			e.printStackTrace();
		}
	}

	private static final String INSERT_STMT = "INSERT INTO groupReport (groupReportID, groupID, memberID, administratorID, groupReportContent, "
			+ "groupReportDate, groupReportStatus, groupReportReason) "
			+ "VALUES ('GR'||LPAD(to_char(groupReport_seq.NEXTVAL), 6, '0'), ?, ?, ?, ?, ?, ?, ?)";
	private static final String GET_ALL_UNCHECKED_STMT = "SELECT groupReportID, groupID, memberID, administratorID, groupReportContent, "
			+ "groupReportDate, groupReportStatus, groupReportReason FROM groupReport WHERE groupReportStatus = 'REPORT_UNCHECKED'";
	private static final String GET_ALL_ACCEPTED_STMT = "SELECT groupReportID, groupID, memberID, administratorID, groupReportContent, "
			+ "groupReportDate, groupReportStatus, groupReportReason FROM groupReport WHERE groupReportStatus = 'REPORT_ACCEPTED'";
	private static final String GET_ALL_REJECTED_STMT = "SELECT groupReportID, groupID, memberID, administratorID, groupReportContent, "
			+ "groupReportDate, groupReportStatus, groupReportReason FROM groupReport WHERE groupReportStatus = 'REPORT_REJECTED'";
	private static final String GET_ONE_STMT = "SELECT groupReportid, groupID, memberID, administratorID, groupReportContent, "
			+ "groupReportDate, groupReportStatus, groupReportReason FROM groupReport WHERE groupReportID = ?";
	private static final String GET_ONE_STMT2 = "SELECT groupReportid, groupID, memberID, administratorID, groupReportContent, "
			+ "groupReportDate, groupReportStatus, groupReportReason FROM groupReport WHERE groupID = ? and memberID = ?";
	private static final String UPDATE = "UPDATE groupReport set administratorID = ?, groupReportStatus = ? WHERE groupReportID = ?";

	private static final String UNCHECKED = "REPORT_UNCHECKED";
	private static final String ACCEPTED = "REPORT_ACCEPTED";
	private static final String REJECTED = "REPORT_REJECTED";

	private static final String ANTI = "ANTI";
	private static final String WRONG = "BOARD_WRONG";
	private static final String OTHER = "OTHER";

	@Override
	public void insert(GroupReportVO groupReportVO) {
		Connection con = null;
		PreparedStatement pstmt = null;

		try {
			con = ds.getConnection();
			pstmt = con.prepareStatement(INSERT_STMT);

			pstmt.setString(1, groupReportVO.getGroupID());
			pstmt.setString(2, groupReportVO.getMemberID());
			pstmt.setNull(3, java.sql.Types.VARCHAR);
			pstmt.setObject(4, groupReportVO.getGroupReportContent(), java.sql.Types.VARCHAR);
			pstmt.setTimestamp(5, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(6, UNCHECKED);
			pstmt.setString(7, groupReportVO.getGroupReportReason());

			pstmt.executeUpdate();
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. " + se.getMessage());
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}

	@Override
	public void update_reportAccept(GroupReportVO groupReportVO) {
		Connection con = null;
		PreparedStatement pstmt = null;

		try {
			con = ds.getConnection();
			con.setAutoCommit(false);

//			先檢舉通過
			pstmt = con.prepareStatement(UPDATE);
			pstmt.setNull(1, java.sql.Types.VARCHAR);
			pstmt.setString(2, groupReportVO.getGroupReportStatus());
			pstmt.setString(3, groupReportVO.getGroupReportID());
			pstmt.executeUpdate();

//			再下架揪團
			GroupListService groupListSvc = new GroupListService();
			groupListSvc.update_noLonger(groupReportVO.getGroupID(), con);

//			2●設定於 pstm.executeUpdate()之後
			con.commit();
			con.setAutoCommit(true);
			System.out.println(groupReportVO.getGroupID() + "已為被下架的揪團");

		} catch (SQLException se) {
			if (con != null) {
				try {
					// 3●設定於當有exception發生時之catch區塊內
					System.err.print("Transaction is being ");
					System.err.println("rolled back由groupReport");
					con.rollback();
				} catch (SQLException excep) {
					throw new RuntimeException("rollback error occured. " + excep.getMessage());
				}
			}
			throw new RuntimeException("A database error occured. " + se.getMessage());
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}

	@Override
	public void update_reportReject(GroupReportVO groupReportVO) {
		Connection con = null;
		PreparedStatement pstmt = null;

		try {
			con = ds.getConnection();
			pstmt = con.prepareStatement(UPDATE);

			pstmt.setNull(1, java.sql.Types.VARCHAR);
			pstmt.setString(2, groupReportVO.getGroupReportStatus());
			pstmt.setString(3, groupReportVO.getGroupReportID());

			pstmt.executeUpdate();
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. " + se.getMessage());
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}

	@Override
	public GroupReportVO findByPrimaryKey(String groupReportID) {
		GroupReportVO groupReportVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ONE_STMT);

			pstmt.setString(1, groupReportID);

			rs = pstmt.executeQuery();

			while (rs.next()) {
				groupReportVO = new GroupReportVO();
				groupReportVO.setGroupReportID(rs.getString("groupReportID"));
				groupReportVO.setGroupID(rs.getString("groupID"));
				groupReportVO.setMemberID(rs.getString("memberID"));
				groupReportVO.setAdministratorID(rs.getString("administratorID"));
				groupReportVO.setGroupReportContent(rs.getString("groupReportContent"));
				groupReportVO.setGroupReportDate(rs.getTimestamp("groupReportDate"));
				groupReportVO.setGroupReportStatus(rs.getString("groupReportStatus"));
				groupReportVO.setGroupReportReason(rs.getString("groupReportReason"));

			}

		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. " + se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return groupReportVO;
	}
	
	@Override
	public GroupReportVO findByGroupIDnMemberID(String groupID, String memberID) {
		GroupReportVO groupReportVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ONE_STMT2);

			pstmt.setString(1, groupID);
			pstmt.setString(2, memberID);

			rs = pstmt.executeQuery();

			while (rs.next()) {
				groupReportVO = new GroupReportVO();
				groupReportVO.setGroupReportID(rs.getString("groupReportID"));
				groupReportVO.setGroupID(rs.getString("groupID"));
				groupReportVO.setMemberID(rs.getString("memberID"));
				groupReportVO.setAdministratorID(rs.getString("administratorID"));
				groupReportVO.setGroupReportContent(rs.getString("groupReportContent"));
				groupReportVO.setGroupReportDate(rs.getTimestamp("groupReportDate"));
				groupReportVO.setGroupReportStatus(rs.getString("groupReportStatus"));
				groupReportVO.setGroupReportReason(rs.getString("groupReportReason"));

			}

		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. " + se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return groupReportVO;
	}

	@Override
	public List<GroupReportVO> getAll_unchecked() {
		List<GroupReportVO> list = new ArrayList<GroupReportVO>();
		GroupReportVO groupReportVO = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ALL_UNCHECKED_STMT);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				groupReportVO = new GroupReportVO();
				groupReportVO.setGroupReportID(rs.getString("groupReportID"));
				groupReportVO.setGroupID(rs.getString("groupID"));
				groupReportVO.setMemberID(rs.getString("memberID"));
				groupReportVO.setAdministratorID(rs.getString("administratorID"));
				groupReportVO.setGroupReportContent(rs.getString("groupReportContent"));
				groupReportVO.setGroupReportDate(rs.getTimestamp("groupReportDate"));
				groupReportVO.setGroupReportStatus(rs.getString("groupReportStatus"));
				groupReportVO.setGroupReportReason(rs.getString("groupReportReason"));
				list.add(groupReportVO);

			}

		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. " + se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return list;

	}
	
	@Override
	public List<GroupReportVO> getAll_accepted() {
		List<GroupReportVO> list = new ArrayList<GroupReportVO>();
		GroupReportVO groupReportVO = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ALL_ACCEPTED_STMT);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				groupReportVO = new GroupReportVO();
				groupReportVO.setGroupReportID(rs.getString("groupReportID"));
				groupReportVO.setGroupID(rs.getString("groupID"));
				groupReportVO.setMemberID(rs.getString("memberID"));
				groupReportVO.setAdministratorID(rs.getString("administratorID"));
				groupReportVO.setGroupReportContent(rs.getString("groupReportContent"));
				groupReportVO.setGroupReportDate(rs.getTimestamp("groupReportDate"));
				groupReportVO.setGroupReportStatus(rs.getString("groupReportStatus"));
				groupReportVO.setGroupReportReason(rs.getString("groupReportReason"));
				list.add(groupReportVO);

			}

		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. " + se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return list;
	}

	@Override
	public List<GroupReportVO> getAll_rejected() {
		List<GroupReportVO> list = new ArrayList<GroupReportVO>();
		GroupReportVO groupReportVO = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ALL_REJECTED_STMT);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				groupReportVO = new GroupReportVO();
				groupReportVO.setGroupReportID(rs.getString("groupReportID"));
				groupReportVO.setGroupID(rs.getString("groupID"));
				groupReportVO.setMemberID(rs.getString("memberID"));
				groupReportVO.setAdministratorID(rs.getString("administratorID"));
				groupReportVO.setGroupReportContent(rs.getString("groupReportContent"));
				groupReportVO.setGroupReportDate(rs.getTimestamp("groupReportDate"));
				groupReportVO.setGroupReportStatus(rs.getString("groupReportStatus"));
				groupReportVO.setGroupReportReason(rs.getString("groupReportReason"));
				list.add(groupReportVO);

			}

		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. " + se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return list;
	}

	@Override
	public Integer getAll_unchecked_count() {
		Integer count = 0;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ALL_UNCHECKED_STMT);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				count++;
			}

		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. " + se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return count;

	}


}
