package com.groupReport.model;

import java.util.*;

public interface GroupReportDAO_interface {
	public void insert (GroupReportVO groupReportVO);
//	檢舉通過
	public void update_reportAccept (GroupReportVO groupReportVO);
//	檢舉駁回
	public void update_reportReject (GroupReportVO groupReportVO);
//	public void delete (String groupreportid);
	public GroupReportVO findByPrimaryKey(String groupReportID);
	public GroupReportVO findByGroupIDnMemberID(String groupID, String memberID);
	public List<GroupReportVO> getAll_unchecked();
	public List<GroupReportVO> getAll_accepted();
	public List<GroupReportVO> getAll_rejected();

//	檢舉計次
	public Integer getAll_unchecked_count();
}
