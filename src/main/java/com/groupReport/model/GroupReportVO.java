package com.groupReport.model;

import java.sql.*;

public class GroupReportVO implements java.io.Serializable {
	private String groupReportID;
	private String groupID;
	private String memberID;
	private String administratorID;
	private String groupReportContent;
	private Timestamp groupReportDate;
	private String groupReportStatus;
	private String groupReportReason;

	public String getGroupReportID() {
		return groupReportID;
	}

	public void setGroupReportID(String groupReportID) {
		this.groupReportID = groupReportID;
	}

	public String getGroupID() {
		return groupID;
	}

	public void setGroupID(String groupID) {
		this.groupID = groupID;
	}

	public String getMemberID() {
		return memberID;
	}

	public void setMemberID(String memberID) {
		this.memberID = memberID;
	}

	public String getAdministratorID() {
		return administratorID;
	}

	public void setAdministratorID(String administratorID) {
		this.administratorID = administratorID;
	}

	public String getGroupReportContent() {
		return groupReportContent;
	}

	public void setGroupReportContent(String groupReportContent) {
		this.groupReportContent = groupReportContent;
	}

	public Timestamp getGroupReportDate() {
		return groupReportDate;
	}

	public void setGroupReportDate(Timestamp groupReportDate) {
		this.groupReportDate = groupReportDate;
	}

	public String getGroupReportStatus() {
		return groupReportStatus;
	}

	public void setGroupReportStatus(String groupReportStatus) {
		this.groupReportStatus = groupReportStatus;
	}

	public String getGroupReportReason() {
		return groupReportReason;
	}

	public void setGroupReportReason(String groupReportReason) {
		this.groupReportReason = groupReportReason;
	}

}
