
package com.adfund.controller;

import java.io.*;
import java.util.*;
import java.util.Date;
import java.sql.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.servlet.*;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.*;

import com.adfund.model.*;
import com.adfundRecord.model.ADFundRecordVO;
import com.adfundReport.model.*;


@MultipartConfig(fileSizeThreshold = 1024*1024, maxFileSize = 5*1024*1024, maxRequestSize = 5*5*1024*1024)
public class ADFundServlet extends HttpServlet {

	public void doGet(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		doPost(req, res);
	}

	@SuppressWarnings("unused")
	public void doPost(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		
		System.out.println(1);
		
		req.setCharacterEncoding("UTF-8");
		String action = req.getParameter("action");
		System.out.println("ads:" + action);
		
		
//	    res.setHeader("Cache-Control", "no-store");
//	    res.setHeader("Pragma", "no-cache");
//	    res.setDateHeader("Expires", 0);
		
		
		if ("getOne_For_Display".equals(action)) { // 來自select_page.jsp的請求

			List<String> errorMsgs = new LinkedList<String>();
			// Store this set in the request scope, in case we need to
			// send the ErrorPage view.
			req.setAttribute("errorMsgs", errorMsgs);

			try {
				/***************************1.接收請求參數 - 輸入格式的錯誤處理**********************/
				
			   System.out.println(55);
				
				String str = req.getParameter("adfundID");

				
				String adfundID = null;
				try {
					adfundID = new String(str);
				} catch (Exception e) {
					errorMsgs.add("編號格式不正確");
				}
				// Send the use back to the form, if there were errors
				if (!errorMsgs.isEmpty()) {
					RequestDispatcher failureView = req
							.getRequestDispatcher("/front-end/adfund/adfund_list.jsp");
					failureView.forward(req, res);
					return;//程式中斷
				}
				
				/***************************2.開始查詢資料*****************************************/
				ADFundService adfundSvc = new ADFundService();
				ADFundVO adfundVO = adfundSvc.getOneADFund(adfundID);
				if (adfundVO == null) {
					errorMsgs.add("查無資料");
				}
				// Send the use back to the form, if there were errors
				if (!errorMsgs.isEmpty()) {
					RequestDispatcher failureView = req
							.getRequestDispatcher("/front-end/adfund/adfund_list.jsp");
					failureView.forward(req, res);
					return;//程式中斷
				}
				
				/***************************3.查詢完成,準備轉交(Send the Success view)*************/
				HttpSession session = req.getSession();
				session.setAttribute("adfundVO", adfundVO); 
				String url = "/front-end/adfund/adfund_solo.jsp";
				RequestDispatcher successView = req.getRequestDispatcher(url); 
				successView.forward(req, res);
				

				/***************************其他可能的錯誤處理*************************************/
			} catch (Exception e) {
				errorMsgs.add("無法取得資料:" + e.getMessage());
				RequestDispatcher failureView = req
						.getRequestDispatcher("/front-end/adfund/adfund_list.jsp");
				failureView.forward(req, res);
			}
		}
		
		
		if ("getOne_For_Update".equals(action)) { // 來自listAllEmp.jsp的請求            
			
			List<String> errorMsgs = new LinkedList<String>();
			// Store this set in the request scope, in case we need to
			// send the ErrorPage view.
			req.setAttribute("errorMsgs", errorMsgs);
			
			try {
				/***************************1.接收請求參數****************************************/
				String adfundID = new String(req.getParameter("adfundID"));

				
				/***************************2.開始查詢資料****************************************/
				ADFundService adfundSvc = new ADFundService();
				ADFundVO adfundVO = adfundSvc.getOneADFund(adfundID);
								
				/***************************3.查詢完成,準備轉交(Send the Success view)************/
				req.setAttribute("adfundVO", adfundVO);         // 資料庫取出的empVO物件,存入req
				String url = "/front-end/adfund/adfund_update.jsp";
				RequestDispatcher successView = req.getRequestDispatcher(url);// 成功轉交 update_emp_input.jsp
				successView.forward(req, res);
				
				System.out.println(555);

				/***************************其他可能的錯誤處理**********************************/
			} catch (Exception e) {
				errorMsgs.add("無法取得要修改的資料:" + e.getMessage());
				RequestDispatcher failureView = req
						.getRequestDispatcher("/front-end/adfund/adfund_list.jsp");
				failureView.forward(req, res);
			}
		}
		
		
		if ("update".equals(action)) { // 來自update_emp_input.jsp的請求
			
			System.out.println(5);
			
			List<String> errorMsgs = new LinkedList<String>();
			// Store this set in the request scope, in case we need to
			// send the ErrorPage view.
			req.setAttribute("errorMsgs", errorMsgs);
		
			try {
				/***************************1.接收請求參數 - 輸入格式的錯誤處理**********************/
				String adfundID = new String(req.getParameter("adfundID").trim());
				
				String boardID = req.getParameter("boardID");
				System.out.println(boardID);
												
				String adfundTitle = req.getParameter("adfundTitle");				
				if (adfundTitle == null || adfundTitle.trim().length() == 0) {
					errorMsgs.add("標題 請勿空白");
				} 
											
				
				String adfundText = req.getParameter("adfundText").trim();
				if (adfundText == null || adfundText.trim().length() == 0) {
					errorMsgs.add("說明 請勿空白");
				}
				
				//圖一
				//取舊圖
				ADFundService adfundSvcTest = new ADFundService();
				ADFundVO adfundVOtest = adfundSvcTest.getOneADFund(adfundID);
				byte[] oldAdfundHomePhoto = adfundVOtest.getAdfundHomePhoto();
				System.out.println("old=" + oldAdfundHomePhoto);
				
				
   				//或傳新圖				
				Part part1 = req.getPart("adfundHomePhoto");
				String photo1Name = part1.getSubmittedFileName();
				System.out.println("Part=" + part1);
				System.out.println("photo1Name=" + photo1Name);
				InputStream is1 = part1.getInputStream();				
				byte[] adfundHomePhoto = new byte[is1.available()];				
				is1.read(adfundHomePhoto);
				System.out.println("adfundHomePhoto="+adfundHomePhoto);
				
				
			
				//圖二
				//取舊圖
				byte[] oldAdfundBoardPhoto = adfundVOtest.getAdfundBoardPhoto();
				System.out.println("old=" + oldAdfundBoardPhoto);
				
				//或傳新圖
				Part part2 = req.getPart("adfundBoardPhoto");
				String photo2Name = part2.getSubmittedFileName();
				System.out.println(part2);
				System.out.println("photo2Name=" + photo2Name);
				InputStream is2 = part2.getInputStream();				
				byte[] adfundBoardPhoto = new byte[is2.available()];				
				is2.read(adfundBoardPhoto);
				System.out.println(adfundBoardPhoto);
				
				


				ADFundVO adfundVO = new ADFundVO();
				adfundVO.setAdfundID(adfundID);
				adfundVO.setAdfundTitle(adfundTitle);				
				adfundVO.setAdfundText(adfundText);
				//沒有上傳新圖 就用舊圖，有新圖就用新圖
				if(photo1Name != "" ) {
				    adfundVO.setAdfundHomePhoto(adfundHomePhoto);
				}else if (photo1Name == "" ){
					adfundVO.setAdfundHomePhoto(oldAdfundHomePhoto);
				}
				
				//沒有上傳新圖 就用舊圖，有新圖就用新圖
				if(photo2Name != "" ) {
				    adfundVO.setAdfundBoardPhoto(adfundBoardPhoto);
				}else if (photo2Name == "" ){
					adfundVO.setAdfundBoardPhoto(oldAdfundBoardPhoto);
				}

				adfundVO.setAdfundCoinTarget(300);



				// Send the use back to the form, if there were errors
				if (!errorMsgs.isEmpty()) {
					req.setAttribute("adfundVO", adfundVO); // 含有輸入格式錯誤的empVO物件,也存入req
					RequestDispatcher failureView = req
							.getRequestDispatcher("/front-end/adfund/adfund_update.jsp");
					failureView.forward(req, res);
					return; //程式中斷
				}
				
				/***************************2.開始修改資料*****************************************/
				ADFundService adfundSvc = new ADFundService();
				adfundVO = adfundSvc.updateADFundByMem(adfundID, adfundTitle, 
						adfundText, oldAdfundHomePhoto, adfundBoardPhoto, 3000);
				
				
				if(photo1Name != "" && photo2Name != "") {
					adfundVO = adfundSvc.updateADFundByMem(adfundID, adfundTitle, 
							adfundText, adfundHomePhoto, adfundBoardPhoto, 3000);
				}else if (photo1Name == "" && photo2Name != ""){
					adfundVO = adfundSvc.updateADFundByMem(adfundID, adfundTitle, 
							adfundText, oldAdfundHomePhoto, adfundBoardPhoto, 3000);
				}else if (photo1Name != "" && photo2Name == ""){
					adfundVO = adfundSvc.updateADFundByMem(adfundID, adfundTitle, 
							adfundText, adfundHomePhoto, oldAdfundBoardPhoto, 3000);
				}else if (photo1Name == "" && photo2Name == ""){
					adfundVO = adfundSvc.updateADFundByMem(adfundID, adfundTitle, 
							adfundText, oldAdfundHomePhoto, oldAdfundBoardPhoto, 3000);
				}
				
				/***************************3.修改完成,準備轉交(Send the Success view)*************/
				req.setAttribute("adfundVO", adfundVO); // 資料庫update成功後,正確的的empVO物件,存入req
				String url = ("/front-end/adfund/adfund_update_check.jsp") ;
				RequestDispatcher successView = req.getRequestDispatcher(url); // 修改成功後,轉交listOneEmp.jsp
				successView.forward(req, res);

				/***************************其他可能的錯誤處理*************************************/
			} catch (Exception e) {
				errorMsgs.add("修改資料失敗:"+e.getMessage());
				RequestDispatcher failureView = req
						.getRequestDispatcher("/front-end/adfund/adfund_update.jsp");
				failureView.forward(req, res);
			}
		}

		
		
        if ("insert".equals(action)) { 
        	
        	System.out.println(3);
        	
			List<String> errorMsgs = new LinkedList<String>();
			// Store this set in the request scope, in case we need to
			// send the ErrorPage view.
			req.setAttribute("errorMsgs", errorMsgs);

			try {
				/***********************1.接收請求參數 - 輸入格式的錯誤處理*************************/
				String memberID = req.getParameter("memberID");
				System.out.println(memberID);
				
				String adfundTitle = req.getParameter("adfundTitle");
				if (adfundTitle == null || adfundTitle.trim().length() == 0) {
					errorMsgs.add("標題 請勿空白");
				} 
				
				String boardID = req.getParameter("boardID").trim();
				if (boardID == null || boardID.trim().length() == 0) {
					errorMsgs.add("藝人請勿空白");
				}
				
				String adfundText = req.getParameter("adfundText").trim();
				if (adfundText == null || adfundText.trim().length() == 0) {
					errorMsgs.add("內容請勿空白");
				}
				
				String adfundType = req.getParameter("adfundType");
				if (adfundType == null || adfundType.trim().length() == 0) {
					errorMsgs.add("類型請勿空白");
				}
				
				Part part1 = req.getPart("adfundHomePhoto");
				InputStream is1 = part1.getInputStream();				
				byte[] adfundHomePhoto = new byte[is1.available()];				
				is1.read(adfundHomePhoto);
				
				
				Part part2 = req.getPart("adfundBoardPhoto");
				InputStream is2 = part2.getInputStream();				
				byte[] adfundBoardPhoto = new byte[is2.available()];				
				is2.read(adfundBoardPhoto);
				
								
				

				Timestamp adfundDisplayDate = null;
				try {
					DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
					System.out.println(req.getParameter("adfundDisplayDate"));
					Date date = df.parse(req.getParameter("adfundDisplayDate"));
					adfundDisplayDate = new Timestamp(date.getTime());
				} catch (ParseException e) {
					adfundDisplayDate=new Timestamp(System.currentTimeMillis());
					errorMsgs.add("請輸入日期!");
				}
				
				
				Timestamp adfundStartDate = new Timestamp(System.currentTimeMillis());
							    
				Timestamp adfundEndDate = new Timestamp(adfundDisplayDate.getTime()-(5*60*1000*1));
				
				Timestamp adfundSubmitDate = new Timestamp(adfundDisplayDate.getTime()+(24*60*60*1000*1));

				ADFundVO adfundVO = new ADFundVO();
				adfundVO.setMemberID(memberID);
				adfundVO.setBoardID(boardID);
				adfundVO.setAdministratorID("A000001");
				adfundVO.setAdfundStartDate(adfundStartDate);
				adfundVO.setAdfundEndDate(adfundEndDate);
				adfundVO.setAdfundDisplayDate(adfundDisplayDate);
				adfundVO.setAdfundTitle(adfundTitle);				
				adfundVO.setAdfundType(adfundType);
				adfundVO.setAdfundText(adfundText);
				adfundVO.setAdfundHomePhoto(adfundHomePhoto);
				adfundVO.setAdfundBoardPhoto(adfundBoardPhoto);
				adfundVO.setAdfundCoinTarget(3000);
				adfundVO.setAdfundCoinNow(0);
				adfundVO.setAdfundSubmitDate(adfundSubmitDate);
				adfundVO.setAdfundStatus("AD_FUNDING");

				// Send the use back to the form, if there were errors
				if (!errorMsgs.isEmpty()) {
					req.setAttribute("adfundVO", adfundVO); // 含有輸入格式錯誤的empVO物件,也存入req
					RequestDispatcher failureView = req
							.getRequestDispatcher("/front-end/adfund/adfund_input.jsp");
					failureView.forward(req, res);
					return;
				}
				
				/***************************2.開始新增資料***************************************/
				ADFundService adfundSvc = new ADFundService();
				adfundVO = adfundSvc.addADFund(memberID, boardID, "A000001", adfundStartDate, adfundEndDate, adfundDisplayDate,
						adfundTitle, adfundType, adfundText, adfundHomePhoto, adfundBoardPhoto, 3000, 0, adfundSubmitDate, "AD_FUNDING");
				
				/***************************3.新增完成,準備轉交(Send the Success view)***********/
//				String url = "/front-end/adfund/adfund_list.jsp";
//				RequestDispatcher successView = req.getRequestDispatcher(url); // 新增成功後轉交listAllEmp.jsp
//				successView.forward(req, res);	
				
				res.sendRedirect(req.getContextPath() + "/front-end/adfund/adfund_list.jsp?boardID="+boardID);
				
				/***************************其他可能的錯誤處理**********************************/
			} 
			catch (Exception e) {
				errorMsgs.add(e.getMessage());
				RequestDispatcher failureView = req
						.getRequestDispatcher("/front-end/adfund/adfund_input.jsp");
				failureView.forward(req, res);
			}
		}
        
		if ("delete".equals(action)) { // 來自listAllEmp.jsp 或  /dept/listEmps_ByDeptno.jsp的請求

			List<String> errorMsgs = new LinkedList<String>();
			// Store this set in the request scope, in case we need to
			// send the ErrorPage view.
			req.setAttribute("errorMsgs", errorMsgs);
			
			String requestURL = req.getParameter("requestURL");
			String whichPage = req.getParameter("whichPage");
	
			try {
				/***************************1.接收請求參數***************************************/
				String adfundID = new String(req.getParameter("adfundID"));
				
				/***************************2.開始刪除資料***************************************/
				ADFundService adfundSvc = new ADFundService();
				
				adfundSvc.deleteADFund(adfundID);
				
				/***************************3.刪除完成,準備轉交(Send the Success view)***********/				
				
				String url = requestURL + "?whichPage="+ whichPage;
//				String url = "/back-end/adfund/adfund_back_list.jsp";
				RequestDispatcher successView = req.getRequestDispatcher(url); // 刪除成功後,轉交回送出刪除的來源網頁
				successView.forward(req, res);
				
				/***************************其他可能的錯誤處理**********************************/
			} catch (Exception e) {
				errorMsgs.add("刪除資料失敗:"+e.getMessage());
				RequestDispatcher failureView = req
						.getRequestDispatcher("/back-end/adfund/adfund_back_list.jsp");
				failureView.forward(req, res);
			}
		}
		
		
        if ("getBackOne_For_Update".equals(action)) { // 來自listAllEmp.jsp的請求            
			
			List<String> errorMsgs = new LinkedList<String>();
			// Store this set in the request scope, in case we need to
			// send the ErrorPage view.
			req.setAttribute("errorMsgs", errorMsgs);
			
			
			try {
				/***************************1.接收請求參數****************************************/
				String adfundID = new String(req.getParameter("adfundID"));

				
				/***************************2.開始查詢資料****************************************/
				ADFundService adfundSvc = new ADFundService();
				ADFundVO adfundVO = adfundSvc.getOneADFund(adfundID);
								
				/***************************3.查詢完成,準備轉交(Send the Success view)************/
				req.setAttribute("adfundVO", adfundVO);         // 資料庫取出的empVO物件,存入req
				String url = "/back-end/adfund/adfund_back_updateStatus.jsp";
				RequestDispatcher successView = req.getRequestDispatcher(url);// 成功轉交 update_emp_input.jsp
				successView.forward(req, res);

				/***************************其他可能的錯誤處理**********************************/
			} catch (Exception e) {
				errorMsgs.add("無法取得要修改的資料:" + e.getMessage());
				RequestDispatcher failureView = req
						.getRequestDispatcher("/back-end/adfund/adfund_back_list.jsp");
				failureView.forward(req, res);
			}
		}
		 
        
		if ("Update_For_Status".equals(action)) { // 來自listAllEmp.jsp的請求            
			
			System.out.println(11);
			
			List<String> errorMsgs = new LinkedList<String>();
			// Store this set in the request scope, in case we need to
			// send the ErrorPage view.
			req.setAttribute("errorMsgs", errorMsgs);
			
			String requestURL = req.getParameter("requestURL");
			
			try {
				/***************************1.接收請求參數****************************************/
				String adfundID = new String(req.getParameter("adfundID"));
				
				String adfundStatus = new String(req.getParameter("adfundStatus"));
                System.out.println(adfundStatus);
				
				/***************************2.開始查詢資料****************************************/
				ADFundService adfundSvc = new ADFundService();
				ADFundVO adfundVO = adfundSvc.updateADFundByAd(adfundID, adfundStatus);
								
				/***************************3.查詢完成,準備轉交(Send the Success view)************/
				req.setAttribute("adfundVO", adfundVO);         // 資料庫取出的empVO物件,存入req
				String url = requestURL;
//				String url = "/back-end/adfund/adfund_back_list.jsp";
				RequestDispatcher successView = req.getRequestDispatcher(url);// 成功轉交 update_emp_input.jsp
				successView.forward(req, res);

				/***************************其他可能的錯誤處理**********************************/
			} catch (Exception e) {
				errorMsgs.add("無法取得要修改的資料:" + e.getMessage());
				RequestDispatcher failureView = req
						.getRequestDispatcher("/back-end/adfund/adfund_back_updateStatus.jsp");
				failureView.forward(req, res);
			}
		}
		
		
		if ("listEmps_ByDeptno_B".equals(action)) {

			System.out.println(77);
			
			List<String> errorMsgs = new LinkedList<String>();
			req.setAttribute("errorMsgs", errorMsgs);
			
			String requestURL = req.getParameter("requestURL");
			String whichPage = req.getParameter("whichPage");

			try {
				/*************************** 1.接收請求參數 ****************************************/
				String adfundID = new String(req.getParameter("adfundID"));
				
				System.out.println(adfundID);

				/*************************** 2.開始查詢資料 ****************************************/
				ADFundService adfundSvc = new ADFundService();
				Set<ADFundReportVO> set = adfundSvc.getADFundReportsByADFundID(adfundID);

				/*************************** 3.查詢完成,準備轉交(Send the Success view) ************/
				req.setAttribute("adfund_back_report_joinlist", set);    // 資料庫取出的set物件,存入request


				String url = null;
				if ("listEmps_ByDeptno_B".equals(action))
					url = requestURL +"?whichPage="+ whichPage;              // 成功轉交 dept/listAllDept.jsp
				
				
				RequestDispatcher successView = req.getRequestDispatcher(url);
				successView.forward(req, res);
				
				System.out.println("-------");

				/*************************** 其他可能的錯誤處理 ***********************************/
			} catch (Exception e) {
				throw new ServletException(e);
			}
		}
		
		
		if ("listEmps_ByDeptno_A".equals(action)) {

			System.out.println(88);
			
			List<String> errorMsgs = new LinkedList<String>();
			req.setAttribute("errorMsgs", errorMsgs);
			
			String requestURL = req.getParameter("requestURL");
			String whichPage = req.getParameter("whichPage");

			try {
				/*************************** 1.接收請求參數 ****************************************/
				String adfundID = new String(req.getParameter("adfundID"));
				
				System.out.println(adfundID);

				/*************************** 2.開始查詢資料 ****************************************/
				ADFundService adfundSvc = new ADFundService();
				Set<ADFundRecordVO> set = adfundSvc.getADFundRecordsByADFundID(adfundID);
				for(ADFundRecordVO set1 : set ) {
					System.out.println(set1.getAdfundRecordID());
				}

				/*************************** 3.查詢完成,準備轉交(Send the Success view) ************/
				req.setAttribute("adfund_coin_joinlist", set);    // 資料庫取出的set物件,存入request


				String url = null;
				if ("listEmps_ByDeptno_A".equals(action))
					url = requestURL +"?whichPage="+ whichPage;
				
				RequestDispatcher successView = req.getRequestDispatcher(url);
				successView.forward(req, res);
				
				System.out.println("-------");

				/*************************** 其他可能的錯誤處理 ***********************************/
			} catch (Exception e) {
				throw new ServletException(e);
			}
		}
		
		
		if("adfund_list_ByCompositeQuery".equals(action)) {
			List<String> errorMsgs = new LinkedList<String>();
			req.setAttribute("errorMsgs", errorMsgs);
			
			try {
				//採用Map<String,String[]> getParameterMap()的方法 
				//注意:an immutable java.util.Map 
				//Map<String, String[]> map = req.getParameterMap();				
				HttpSession session= req.getSession();
				Map<String, String[]> map = (Map<String,String[]>)session.getAttribute("map");
				if(req.getParameter("whichPage") ==null) {
					HashMap<String, String[]> map1 = new HashMap<String, String[]>(req.getParameterMap());
					session.setAttribute("map", map1);
					map = map1;
				}
												
				ADFundService adfundSvc = new ADFundService();
				List<ADFundVO> list = adfundSvc.getAll(map);
				
				System.out.println(list);
				
				req.setAttribute("adfund_list_ByCompositeQuery", list);
				
				RequestDispatcher successView = req.getRequestDispatcher("/front-end/adfund/adfund_list_ByCompositeQuery.jsp");
				
				successView.forward(req, res);
				System.out.println(6666);
				
				
				return;
			} catch (Exception e) {
				errorMsgs.add(e.getMessage());
				RequestDispatcher failureView = req
						.getRequestDispatcher("/front-end/adfund/adfund_list.jsp");
				failureView.forward(req, res);
				System.out.println(errorMsgs);
			}
			
		}
		
		
}
}
