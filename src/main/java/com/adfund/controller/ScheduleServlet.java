package com.adfund.controller;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.Month;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.adfund.model.*;
import com.adfundRecord.model.*;
import com.member.model.*;


public class ScheduleServlet extends HttpServlet {

	Timer tm = null;
	private int count = 0;
	
	TimerTask tt = new TimerTask() {
		public void run() {	
			
			ADFundService adfundSvc = new ADFundService();
			ADFundRecordService adfundRecSvc = new ADFundRecordService();
			MemberService memSvc = new MemberService();
			
			//取得未達目標又過期的募資list
			List<ADFundVO> adList = adfundSvc.getAllWithADFundingStatus();
			

			
			for(ADFundVO adList1 : adList) {
				System.out.println(adList1.getAdfundID());
				
				//取得每筆募資的狀態並修改成"AD_NOT_FINISH"
				ADFundVO adfundVO = adfundSvc.getOneADFund(adList1.getAdfundID());
				adfundVO = adfundSvc.updateADFundByAd(adList1.getAdfundID(), "AD_NOT_FINISH");
				System.out.println("已改狀態");
				
				//取得每個募資的參與會員list
				List<ADFundRecordVO> joinlist = adfundRecSvc.getAllADFundRecord(adList1.getAdfundID());
				for(ADFundRecordVO joinlist1 : joinlist) {
										
					//退代幣給每個有參加的會員
					System.out.println(joinlist1.getMemberID());
					MemberVO memberVO =  memSvc.getOneMember(joinlist1.getMemberID());
					Integer memberCoin = memberVO.getMemberCoin() + joinlist1.getAdfundCoinUsed();
					
					memberVO.setMemberID(joinlist1.getMemberID());
					memberVO.setMemberCoin(memberCoin);
					
					memberVO = memSvc.updateMemberCoin(memberCoin, joinlist1.getMemberID());
					
					//將募資參與明細之每個參與者的代幣歸零(
					System.out.println(joinlist1.getAdfundRecordID());
					ADFundRecordVO adfundRecordVO = adfundRecSvc.getOneADFundRecord(joinlist1.getAdfundRecordID());
					Integer adfundCoinUsed = adfundRecordVO.getAdfundCoinUsed() - adfundRecordVO.getAdfundCoinUsed();
					
					adfundRecordVO.setAdfundRecordID(joinlist1.getAdfundRecordID());
					adfundRecordVO.setAdfundCoinUsed(adfundCoinUsed);
					
					adfundRecordVO = adfundRecSvc.updateADRecCoin(adfundCoinUsed, joinlist1.getAdfundRecordID());
					
				}
			}
			
			
//			System.out.println("The count number is:" + count);	
//			count++;
		}
	};
	
	public void init() throws ServletException {
		tm = new Timer();
		Calendar firstTime = new GregorianCalendar(2018, Calendar.NOVEMBER, 14, 0, 0, 0);
		Date date = firstTime.getTime();
      Long period = (long) (24 * 60 * 60* 1000);
		
		tm.scheduleAtFixedRate(tt, date, period);		
	}

	public void destroy() {
		tm.cancel();
	}

	public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		res.setContentType("text/plain");
		PrintWriter out = res.getWriter();
	}



}

