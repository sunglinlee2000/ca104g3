package com.adfund.controller;

import java.io.IOException;

import javax.servlet.*;
import javax.servlet.http.*;

import com.adfund.model.*;

public class ADFundImgServlet extends HttpServlet{
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException{
		req.setCharacterEncoding("UTF-8");
		
		String adfundID = req.getParameter("adfundID");
		String adfundHomePhoto = req.getParameter("adfundHomePhoto");
		String adfundBoardPhoto = req.getParameter("adfundBoardPhoto");
		ADFundService adfundSvc = new ADFundService();
		ADFundVO adfundVO = adfundSvc.getOneADFund(adfundID);
		
		
		byte[] pic = null;
		
		ServletOutputStream out = res.getOutputStream();
		
		if("adfundHomePhoto".equals(adfundHomePhoto)) {
		    pic = adfundVO.getAdfundHomePhoto();
		}
		if("adfundBoardPhoto".equals(adfundBoardPhoto)) {
			pic = adfundVO.getAdfundBoardPhoto();
		}
		
		
		res.setContentType("image/jpeg");
		res.setContentLength(pic.length);
		
		out.write(pic);
		out.flush();	
		out.close();
		
	}

}
