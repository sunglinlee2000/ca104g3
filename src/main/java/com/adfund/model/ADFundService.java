

package com.adfund.model;

import java.sql.Timestamp;
import java.util.Base64;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.adfundRecord.model.ADFundRecordVO;
import com.adfundReport.model.ADFundReportVO;

public class ADFundService {
	
	private ADFundDAO_interface dao;
	
	public ADFundService() {
		dao = new ADFundDAO();
	}

	public ADFundVO addADFund(String memberID, String boardID, String administratorID, Timestamp adfundStartDate,
			Timestamp adfundEndDate, Timestamp adfundDisplayDate, String adfundTitle, String adfundType, String adfundText,
			byte[] adfundHomePhoto, byte[] adfundBoardPhoto, Integer adfundCoinTarget, Integer adfundCoinNow,
			Timestamp adfundSubmitDate, String adfundStatus) {
		
		ADFundVO adfundVO = new ADFundVO();
		
		adfundVO.setMemberID(memberID);
		adfundVO.setBoardID(boardID);
		adfundVO.setAdministratorID(administratorID);
		adfundVO.setAdfundStartDate(adfundStartDate);
		adfundVO.setAdfundEndDate(adfundEndDate);
		adfundVO.setAdfundDisplayDate(adfundDisplayDate);
		adfundVO.setAdfundTitle(adfundTitle);
		adfundVO.setAdfundType(adfundType);
		adfundVO.setAdfundText(adfundText);
		adfundVO.setAdfundHomePhoto(adfundHomePhoto);
		adfundVO.setAdfundBoardPhoto(adfundBoardPhoto);
		adfundVO.setAdfundCoinTarget(adfundCoinTarget);
		adfundVO.setAdfundCoinNow(adfundCoinNow);
		adfundVO.setAdfundSubmitDate(adfundSubmitDate);
		adfundVO.setAdfundStatus(adfundStatus);
		dao.insert(adfundVO);
		
		return adfundVO;		
	}
	
	public ADFundVO updateADFundByMem(String adfundID, String adfundTitle,
			String adfundText, byte[] adfundHomePhoto,byte[] adfundBoardPhoto, Integer adfundCoinTarget) {
		
		ADFundVO adfundVO = new ADFundVO();
	
		adfundVO.setAdfundID(adfundID);
		adfundVO.setAdfundTitle(adfundTitle);
		adfundVO.setAdfundText(adfundText);
		adfundVO.setAdfundHomePhoto(adfundHomePhoto);
		adfundVO.setAdfundBoardPhoto(adfundBoardPhoto);
		adfundVO.setAdfundCoinTarget(adfundCoinTarget);
		dao.updateByMem(adfundVO);
		
		return adfundVO;
	}
	
	public ADFundVO updateADFundByAd(String adfundID, String adfundStatus) {
		
		ADFundVO adfundVO = new ADFundVO();
		
		adfundVO.setAdfundID(adfundID);
		adfundVO.setAdfundStatus(adfundStatus);
		dao.updateByAd(adfundVO);
		
		return adfundVO;	
	}
	
	
    public ADFundVO updateADFundCoinNow(String adfundID, Integer adfundCoinNow) {
		
		ADFundVO adfundVO = new ADFundVO();
		
		adfundVO.setAdfundID(adfundID);
		adfundVO.setAdfundCoinNow(adfundCoinNow);
		dao.updateCoinNow(adfundVO);
		
		return adfundVO;	
	}
	
	public void deleteADFund(String adfundID) {
		dao.delete(adfundID);
	}
	
	public ADFundVO getOneADFund(String adfundID) {
		return dao.findByPrimaryKey(adfundID);		
	}
	
	public List<ADFundVO> getAllADFund(){
		return dao.getAll();
	}
	
	public Set<ADFundReportVO> getADFundReportsByADFundID(String adfundID) {
		return dao.getADFundReportsByADFundID(adfundID);
	}
	
	public Set<ADFundRecordVO> getADFundRecordsByADFundID(String adfundID) {
		return dao.getADFundRecordsByADFundID(adfundID);
	}
	
	public List<ADFundVO> getAllADFundWithStatus(){
		return dao.getAllWithStatus();
	}
	
	public List<ADFundVO> getAllWithNotFinishStatus(){
		return dao.getAllWithNotFinishStatus();
	}
	
	public List<ADFundVO> getAllWithSubmitStatus(){
		return dao.getAllWithSubmitStatus();
	}
	
	public List<ADFundVO> getAllWithPassStatus(){
		return dao.getAllWithPassStatus();
	}
	
	public List<ADFundVO> getAllWithUnpassStatus(){
		return dao.getAllWithUnpassStatus();
	}
	
	public String bytesToImage (byte[] srcImageData) {
		System.out.println("data:image/png;base64," + Base64.getEncoder().encodeToString(srcImageData));
		return ("data:image/png;base64," + Base64.getEncoder().encodeToString(srcImageData));		
	}
	
	public List<ADFundVO> getAll(Map<String, String[]> map) {
		return dao.getAll(map);
	}
	
	public List<ADFundVO> getAllOkPicWithStatus(){
		return dao.getAllOkPicWithStatus();
	}
	
	public List<ADFundVO> getAllByBoardID(String boardID){
		return dao.getAllByBoardID(boardID);
	}
	
	public List<ADFundVO> getAllOkBoardPicWithStatus(String boardID){
		return dao.getAllOkBoardPicWithStatus(boardID);
	}
	
	public List<ADFundVO> getAllWithADFundingStatus(){
		return dao.getAllWithADFundingStatus();
	}
	
	public List<ADFundVO> getADFundsByMemberID(String memberID){
		return dao.getADFundsByMemberID(memberID);
	}
	
	public Integer getADSubmitCount(){
		return dao.getADSubmitCount();
	}

}
