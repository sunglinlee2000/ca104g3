package com.adfund.model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.*;

import com.adfundRecord.model.ADFundRecordVO;
import com.adfundReport.model.ADFundReportVO;

import jdbc.util.CompositeQuery_adfund.jdbcUtil_CompositeQuery_adfund;

public class ADFundDAO implements ADFundDAO_interface{

	private static DataSource ds = null;
	static {
		try {
			Context ctx = new InitialContext();
			ds = (DataSource) ctx.lookup("java:comp/env/jdbc/CA104G3DB");
		} catch (NamingException e) {
			e.printStackTrace();
		}
	}
	
	
	private static final String INSERT_STMT = "INSERT INTO ADFUND VALUES "
			+ "('AD'||(LPAD(TO_CHAR(ADFUND_SEQ.NEXTVAL),6,'0')),?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	
	private static final String GET_ALL_STMT = "select * from adfund order by adfundid desc";
	private static final String GET_ONE_STMT = "SELECT * FROM ADFUND WHERE ADFUNDID = ?";
	private static final String UPDATE_BY_MEM = "UPDATE ADFUND SET "
			+ "ADFUNDTITLE=?, ADFUNDTEXT=?, ADFUNDHOMEPHOTO=?, ADFUNDBOARDPHOTO=?, ADFUNDCOINTARGET=? WHERE ADFUNDID = ?";

	private static final String UPDATE_BY_AD = "UPDATE ADFUND SET "
			+ "ADFUNDSTATUS =? WHERE ADFUNDID = ?";
	private static final String UPDATE_COINNOW = "UPDATE ADFUND SET "
			+ "ADFUNDCOINNOW =? WHERE ADFUNDID = ?";
	private static final String DELETE = "DELETE FROM ADFUND WHERE ADFUNDID = ?";
	
	private static final String GET_ADFUNDREPORTS_BY_ADFUNDID_STMT = "SELECT * FROM ADFUNDREPORT where ADFUNDID = ? order by ADFUNDREPORTID";
	
	private static final String GET_ADFUNDRECORDS_BY_ADFUNDID_STMT = "SELECT * FROM ADFUNDRECORD where ADFUNDID = ? order by ADFUNDRECORDID";
	
	private static final String GET_ALL_WITH_STATUS = "select * from adfund where adfundstatus = 'AD_FUNDING' order by adfunddisplaydate asc";
	
	private static final String GET_ALL_WITH_NOTFINISH_STATUS = "select * from adfund where adfundstatus = 'AD_NOT_FINISH' order by adfunddisplaydate asc";
	
	private static final String GET_ALL_WITH_SUBMIT_STATUS = "select * from adfund where adfundstatus = 'AD_SUBMITTED' order by adfunddisplaydate asc";
	
	private static final String GET_ALL_WITH_PASS_STATUS = "select * from adfund where adfundstatus = 'AD_PASSED' order by adfunddisplaydate asc";
	
	private static final String GET_ALL_WITH_UNPASS_STATUS = "select * from adfund where adfundstatus = 'AD_REJECTED' order by adfunddisplaydate asc";
	
	private static final String AD_OK_TO_DISPLAY = "select * from adfund where ADFUNDSTATUS = 'AD_PASSED' AND ADFUNDDISPLAYDATE < SYSDATE AND SYSDATE < ADFUNDSUBMITDATE order by adfundid desc";
    
	private static final String AD_BOARD_OK_TO_DISPLAY = "select * from adfund where ADFUNDSTATUS = 'AD_PASSED' AND boardid = ? AND ADFUNDDISPLAYDATE < SYSDATE AND SYSDATE < ADFUNDSUBMITDATE order by adfundid desc";
	
	private static final String GET_ALL_STMT_BY_BOARDID = "select * from adfund where boardid = ? AND ADFUNDSTATUS != 'AD_REJECTED' order by adfundid desc";
	
	private static final String GET_ALL_BY_AD_FUNDING_STATUS = "select * from adfund where ADFUNDSTATUS = 'AD_FUNDING' AND ADFUNDENDDATE < SYSDATE order by ADFUNDENDDATE asc";
	
	private static final String INPUT_PHOTO = "UPDATE ADFUND SET ADFUNDHOMEPHOTO=?, ADFUNDBOARDPHOTO=? WHERE ADFUNDID = ?";
	
	private static final String GET_ALL_BY_MEMBERID = "select * from adfund where memberid = ? AND ADFUNDSTATUS != 'AD_REJECTED'";
	
	private static final String GET_AD_SUBMIT_COUNT = "SELECT COUNT(*) AS COUNT FROM ADFUND WHERE adfundStatus = 'AD_SUBMITTED'";
	
	@Override
	public void insert(ADFundVO adfundVO) {

		Connection con = null;
		PreparedStatement pstmt = null;

		try {
		
			con = ds.getConnection();
			pstmt = con.prepareStatement(INSERT_STMT);

			pstmt.setString(1, adfundVO.getMemberID());
			pstmt.setString(2, adfundVO.getBoardID());
			pstmt.setString(3, adfundVO.getAdministratorID());
			pstmt.setTimestamp(4, adfundVO.getAdfundStartDate());
			pstmt.setTimestamp(5, adfundVO.getAdfundEndDate());
			pstmt.setTimestamp(6, adfundVO.getAdfundDisplayDate());
			pstmt.setString(7, adfundVO.getAdfundTitle());
			pstmt.setString(8, adfundVO.getAdfundType());
			pstmt.setString(9, adfundVO.getAdfundText());
			pstmt.setBytes(10, adfundVO.getAdfundHomePhoto());
			pstmt.setBytes(11, adfundVO.getAdfundBoardPhoto());
			pstmt.setInt(12, adfundVO.getAdfundCoinTarget());
			pstmt.setInt(13, adfundVO.getAdfundCoinNow());
			pstmt.setTimestamp(14, adfundVO.getAdfundSubmitDate());
			pstmt.setString(15, adfundVO.getAdfundStatus());

			pstmt.executeUpdate();

		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. " + se.getMessage());

		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {

				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}

		}
	}

	@Override
	public void updateByMem(ADFundVO adfundVO) {

		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(UPDATE_BY_MEM);

			pstmt.setString(1, adfundVO.getAdfundTitle());
			pstmt.setString(2, adfundVO.getAdfundText());
			pstmt.setBytes(3, adfundVO.getAdfundHomePhoto());
			pstmt.setBytes(4, adfundVO.getAdfundBoardPhoto());
			pstmt.setInt(5, adfundVO.getAdfundCoinTarget());
			pstmt.setString(6, adfundVO.getAdfundID());

			pstmt.executeUpdate();
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. " + se.getMessage());
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
					e.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace(System.err);
				}

			}
		}

	}
	
	
	@Override
	public void updateByAd(ADFundVO adfundVO) {
		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(UPDATE_BY_AD);

			pstmt.setString(1, adfundVO.getAdfundStatus());
			pstmt.setString(2, adfundVO.getAdfundID());


			pstmt.executeUpdate();
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. " + se.getMessage());
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
					e.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace(System.err);
				}
			}
		}		
	}
	

	@Override
	public ADFundVO findByPrimaryKey(String adfundid) {

		ADFundVO adfVO = new ADFundVO();
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ONE_STMT);

			pstmt.setString(1, adfundid);

			rs = pstmt.executeQuery();
			
			while (rs.next()) {
				adfVO = new ADFundVO();
				adfVO.setAdfundID(rs.getString("adfundid"));
				adfVO.setMemberID(rs.getString("memberid"));
				adfVO.setBoardID(rs.getString("boardid"));
				adfVO.setAdministratorID(rs.getString("administratorid"));
				adfVO.setAdfundStartDate(rs.getTimestamp("adfundstartdate"));
				adfVO.setAdfundEndDate(rs.getTimestamp("adfundenddate"));
				adfVO.setAdfundDisplayDate(rs.getTimestamp("adfunddisplaydate"));
				adfVO.setAdfundTitle(rs.getString("adfundtitle"));
				adfVO.setAdfundType(rs.getString("adfundtype"));
				adfVO.setAdfundText(rs.getString("adfundtext"));
				adfVO.setAdfundHomePhoto(rs.getBytes("adfundhomephoto"));
				adfVO.setAdfundBoardPhoto(rs.getBytes("adfundboardphoto"));
				adfVO.setAdfundCoinTarget(rs.getInt("adfundcointarget"));
				adfVO.setAdfundCoinNow(rs.getInt("adfundcoinnow"));
				adfVO.setAdfundSubmitDate(rs.getTimestamp("adfundsubmitdate"));
				adfVO.setAdfundStatus(rs.getString("adfundstatus"));
			}
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. " + se.getMessage());

		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return adfVO;
	}

	@Override
	public List<ADFundVO> getAll() {
		List<ADFundVO> adlist = new ArrayList<ADFundVO>();
		ADFundVO adfVO = null;
		
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs =null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ALL_STMT);
			rs = pstmt.executeQuery();
			
			while (rs.next()) {
				adfVO = new ADFundVO();
				adfVO.setAdfundID(rs.getString("adfundid"));
				adfVO.setMemberID(rs.getString("memberid"));
				adfVO.setBoardID(rs.getString("boardid"));
				adfVO.setAdministratorID(rs.getString("administratorid"));
				adfVO.setAdfundStartDate(rs.getTimestamp("adfundstartdate"));
				adfVO.setAdfundEndDate(rs.getTimestamp("adfundenddate"));
				adfVO.setAdfundDisplayDate(rs.getTimestamp("adfunddisplaydate"));
				adfVO.setAdfundTitle(rs.getString("adfundtitle"));
				adfVO.setAdfundType(rs.getString("adfundtype"));
				adfVO.setAdfundText(rs.getString("adfundtext"));
				adfVO.setAdfundHomePhoto(rs.getBytes("adfundhomephoto"));
				adfVO.setAdfundBoardPhoto(rs.getBytes("adfundboardphoto"));
				adfVO.setAdfundCoinTarget(rs.getInt("adfundcointarget"));
				adfVO.setAdfundCoinNow(rs.getInt("adfundcoinnow"));
				adfVO.setAdfundSubmitDate(rs.getTimestamp("adfundsubmitdate"));
				adfVO.setAdfundStatus(rs.getString("adfundstatus"));
				adlist.add(adfVO);
				
			}
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. " + se.getMessage());
			
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

		return adlist;
	}

	@Override
	public void delete(String adfundID) {
		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(DELETE);

			pstmt.setString(1, adfundID);

			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		
	}

	@Override
	public void updateCoinNow(ADFundVO adfundVO) {
		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(UPDATE_COINNOW);

			pstmt.setInt(1, adfundVO.getAdfundCoinNow());
			pstmt.setString(2, adfundVO.getAdfundID());


			pstmt.executeUpdate();
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. " + se.getMessage());
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
					e.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace(System.err);
				}
			}
		}		
		
	}

	@Override
	public Set<ADFundReportVO> getADFundReportsByADFundID(String adfundID) {
		Set<ADFundReportVO> set = new LinkedHashSet<ADFundReportVO>();
		ADFundReportVO adfundReportVO = null;
	
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
	
		try {
	
			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ADFUNDREPORTS_BY_ADFUNDID_STMT);
			pstmt.setString(1, adfundID);
			rs = pstmt.executeQuery();
	
			while (rs.next()) {
				adfundReportVO = new ADFundReportVO();				
				adfundReportVO.setAdfundReportID(rs.getString("adfundreportid"));
				adfundReportVO.setAdfundID(rs.getString("adfundid"));
				adfundReportVO.setMemberID(rs.getString("memberid"));
				adfundReportVO.setAdministratorID(rs.getString("administratorID"));
				adfundReportVO.setAdfundReportContent(rs.getString("adfundreportcontent"));
				adfundReportVO.setAdfundReportDate(rs.getTimestamp("adfundreportdate"));
				adfundReportVO.setAdfundReportStatus(rs.getString("adfundreportstatus"));
				adfundReportVO.setAdfundReportReason(rs.getString("adfundreportreason"));
				set.add(adfundReportVO); // Store the row in the vector
			}
	
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return set;
		
	}

	@Override
	public Set<ADFundRecordVO> getADFundRecordsByADFundID(String adfundID) {
		Set<ADFundRecordVO> set = new LinkedHashSet<ADFundRecordVO>();
		ADFundRecordVO adfundRecordVO = null;
	
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
	
		try {
	
			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ADFUNDRECORDS_BY_ADFUNDID_STMT);
			pstmt.setString(1, adfundID);
			rs = pstmt.executeQuery();
	
			while (rs.next()) {
				adfundRecordVO = new ADFundRecordVO();				
				adfundRecordVO.setAdfundRecordID(rs.getString("adfundrecordid"));
				adfundRecordVO.setAdfundID(rs.getString("adfundid"));
				adfundRecordVO.setMemberID(rs.getString("memberid"));
				adfundRecordVO.setAdfundCoinUsed(rs.getInt("adfundcoinused"));
				adfundRecordVO.setAdfundJoinDate(rs.getTimestamp("adfundjoindate"));				
				set.add(adfundRecordVO); // Store the row in the vector
			}
	
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return set;
	}

	@Override
	public List<ADFundVO> getAllWithStatus() {
		List<ADFundVO> adlist = new ArrayList<ADFundVO>();
		ADFundVO adfVO = null;
		
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs =null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ALL_WITH_STATUS);
			rs = pstmt.executeQuery();
			
			while (rs.next()) {
				adfVO = new ADFundVO();
				adfVO.setAdfundID(rs.getString("adfundid"));
				adfVO.setMemberID(rs.getString("memberid"));
				adfVO.setBoardID(rs.getString("boardid"));
				adfVO.setAdministratorID(rs.getString("administratorid"));
				adfVO.setAdfundStartDate(rs.getTimestamp("adfundstartdate"));
				adfVO.setAdfundEndDate(rs.getTimestamp("adfundenddate"));
				adfVO.setAdfundDisplayDate(rs.getTimestamp("adfunddisplaydate"));
				adfVO.setAdfundTitle(rs.getString("adfundtitle"));
				adfVO.setAdfundType(rs.getString("adfundtype"));
				adfVO.setAdfundText(rs.getString("adfundtext"));
				adfVO.setAdfundHomePhoto(rs.getBytes("adfundhomephoto"));
				adfVO.setAdfundBoardPhoto(rs.getBytes("adfundboardphoto"));
				adfVO.setAdfundCoinTarget(rs.getInt("adfundcointarget"));
				adfVO.setAdfundCoinNow(rs.getInt("adfundcoinnow"));
				adfVO.setAdfundSubmitDate(rs.getTimestamp("adfundsubmitdate"));
				adfVO.setAdfundStatus(rs.getString("adfundstatus"));
				adlist.add(adfVO);
				
			}
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. " + se.getMessage());
			
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

		return adlist;
	}



	@Override
	public List<ADFundVO> getAllWithSubmitStatus() {
		List<ADFundVO> adlist = new ArrayList<ADFundVO>();
		ADFundVO adfVO = null;
		
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs =null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ALL_WITH_SUBMIT_STATUS);
			rs = pstmt.executeQuery();
			
			while (rs.next()) {
				adfVO = new ADFundVO();
				adfVO.setAdfundID(rs.getString("adfundid"));
				adfVO.setMemberID(rs.getString("memberid"));
				adfVO.setBoardID(rs.getString("boardid"));
				adfVO.setAdministratorID(rs.getString("administratorid"));
				adfVO.setAdfundStartDate(rs.getTimestamp("adfundstartdate"));
				adfVO.setAdfundEndDate(rs.getTimestamp("adfundenddate"));
				adfVO.setAdfundDisplayDate(rs.getTimestamp("adfunddisplaydate"));
				adfVO.setAdfundTitle(rs.getString("adfundtitle"));
				adfVO.setAdfundType(rs.getString("adfundtype"));
				adfVO.setAdfundText(rs.getString("adfundtext"));
				adfVO.setAdfundHomePhoto(rs.getBytes("adfundhomephoto"));
				adfVO.setAdfundBoardPhoto(rs.getBytes("adfundboardphoto"));
				adfVO.setAdfundCoinTarget(rs.getInt("adfundcointarget"));
				adfVO.setAdfundCoinNow(rs.getInt("adfundcoinnow"));
				adfVO.setAdfundSubmitDate(rs.getTimestamp("adfundsubmitdate"));
				adfVO.setAdfundStatus(rs.getString("adfundstatus"));
				adlist.add(adfVO);
				
			}
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. " + se.getMessage());
			
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

		return adlist;
	}


	@Override
	public List<ADFundVO> getAllWithPassStatus() {
		List<ADFundVO> adlist = new ArrayList<ADFundVO>();
		ADFundVO adfVO = null;
		
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs =null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ALL_WITH_PASS_STATUS);
			rs = pstmt.executeQuery();
			
			while (rs.next()) {
				adfVO = new ADFundVO();
				adfVO.setAdfundID(rs.getString("adfundid"));
				adfVO.setMemberID(rs.getString("memberid"));
				adfVO.setBoardID(rs.getString("boardid"));
				adfVO.setAdministratorID(rs.getString("administratorid"));
				adfVO.setAdfundStartDate(rs.getTimestamp("adfundstartdate"));
				adfVO.setAdfundEndDate(rs.getTimestamp("adfundenddate"));
				adfVO.setAdfundDisplayDate(rs.getTimestamp("adfunddisplaydate"));
				adfVO.setAdfundTitle(rs.getString("adfundtitle"));
				adfVO.setAdfundType(rs.getString("adfundtype"));
				adfVO.setAdfundText(rs.getString("adfundtext"));
				adfVO.setAdfundHomePhoto(rs.getBytes("adfundhomephoto"));
				adfVO.setAdfundBoardPhoto(rs.getBytes("adfundboardphoto"));
				adfVO.setAdfundCoinTarget(rs.getInt("adfundcointarget"));
				adfVO.setAdfundCoinNow(rs.getInt("adfundcoinnow"));
				adfVO.setAdfundSubmitDate(rs.getTimestamp("adfundsubmitdate"));
				adfVO.setAdfundStatus(rs.getString("adfundstatus"));
				adlist.add(adfVO);
				
			}
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. " + se.getMessage());
			
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

		return adlist;
	}
	

	@Override
	public List<ADFundVO> getAllWithUnpassStatus() {
		List<ADFundVO> adlist = new ArrayList<ADFundVO>();
		ADFundVO adfVO = null;
		
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs =null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ALL_WITH_UNPASS_STATUS);
			rs = pstmt.executeQuery();
			
			while (rs.next()) {
				adfVO = new ADFundVO();
				adfVO.setAdfundID(rs.getString("adfundid"));
				adfVO.setMemberID(rs.getString("memberid"));
				adfVO.setBoardID(rs.getString("boardid"));
				adfVO.setAdministratorID(rs.getString("administratorid"));
				adfVO.setAdfundStartDate(rs.getTimestamp("adfundstartdate"));
				adfVO.setAdfundEndDate(rs.getTimestamp("adfundenddate"));
				adfVO.setAdfundDisplayDate(rs.getTimestamp("adfunddisplaydate"));
				adfVO.setAdfundTitle(rs.getString("adfundtitle"));
				adfVO.setAdfundType(rs.getString("adfundtype"));
				adfVO.setAdfundText(rs.getString("adfundtext"));
				adfVO.setAdfundHomePhoto(rs.getBytes("adfundhomephoto"));
				adfVO.setAdfundBoardPhoto(rs.getBytes("adfundboardphoto"));
				adfVO.setAdfundCoinTarget(rs.getInt("adfundcointarget"));
				adfVO.setAdfundCoinNow(rs.getInt("adfundcoinnow"));
				adfVO.setAdfundSubmitDate(rs.getTimestamp("adfundsubmitdate"));
				adfVO.setAdfundStatus(rs.getString("adfundstatus"));
				adlist.add(adfVO);
				
			}
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. " + se.getMessage());
			
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

		return adlist;
	}

	@Override
	public List<ADFundVO> getAll(Map<String, String[]> map) {
	    List<ADFundVO> list = new ArrayList<ADFundVO>();
	    ADFundVO adfVO = null;
	    
	    Connection con = null;
	    PreparedStatement pstmt  = null;
	    ResultSet rs = null;
	    
	    try {
	    	
	    	con = ds.getConnection();
	    	String finalSQL = "select * from adfund"
	    			+ jdbcUtil_CompositeQuery_adfund.get_WhereCondition(map)
	    			+ " order by adfunddisplaydate";
	    	pstmt = con.prepareStatement(finalSQL);
	    	System.out.println("●●finalSQL(by DAO) = "+finalSQL);
	    	rs = pstmt.executeQuery();
	    	
	    	while(rs.next()) {
	    		adfVO = new ADFundVO();
				adfVO.setAdfundID(rs.getString("adfundid"));
				adfVO.setBoardID(rs.getString("boardid"));
				adfVO.setAdministratorID(rs.getString("administratorid"));
				adfVO.setAdfundStartDate(rs.getTimestamp("adfundstartdate"));
				adfVO.setAdfundEndDate(rs.getTimestamp("adfundenddate"));
				adfVO.setAdfundDisplayDate(rs.getTimestamp("adfunddisplaydate"));
				adfVO.setAdfundTitle(rs.getString("adfundtitle"));
				adfVO.setAdfundType(rs.getString("adfundtype"));
				adfVO.setAdfundText(rs.getString("adfundtext"));
				adfVO.setAdfundHomePhoto(rs.getBytes("adfundhomephoto"));
				adfVO.setAdfundBoardPhoto(rs.getBytes("adfundboardphoto"));
				adfVO.setAdfundCoinTarget(rs.getInt("adfundcointarget"));
				adfVO.setAdfundCoinNow(rs.getInt("adfundcoinnow"));
				adfVO.setAdfundSubmitDate(rs.getTimestamp("adfundsubmitdate"));
				adfVO.setAdfundStatus(rs.getString("adfundstatus"));
				list.add(adfVO);
	    	}
	    	System.out.println(list);
	    	
	    }catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
	    
		
		
		return list;
	}

	@Override
	public List<ADFundVO> getAllOkPicWithStatus() {
		List<ADFundVO> adlist = new ArrayList<ADFundVO>();
		ADFundVO adfVO = null;
		
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs =null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(AD_OK_TO_DISPLAY);
			rs = pstmt.executeQuery();
			
			while (rs.next()) {
				adfVO = new ADFundVO();
				adfVO.setAdfundID(rs.getString("adfundid"));
				adfVO.setMemberID(rs.getString("memberid"));
				adfVO.setBoardID(rs.getString("boardid"));
				adfVO.setAdministratorID(rs.getString("administratorid"));
				adfVO.setAdfundStartDate(rs.getTimestamp("adfundstartdate"));
				adfVO.setAdfundEndDate(rs.getTimestamp("adfundenddate"));
				adfVO.setAdfundDisplayDate(rs.getTimestamp("adfunddisplaydate"));
				adfVO.setAdfundTitle(rs.getString("adfundtitle"));
				adfVO.setAdfundType(rs.getString("adfundtype"));
				adfVO.setAdfundText(rs.getString("adfundtext"));
				adfVO.setAdfundHomePhoto(rs.getBytes("adfundhomephoto"));
				adfVO.setAdfundBoardPhoto(rs.getBytes("adfundboardphoto"));
				adfVO.setAdfundCoinTarget(rs.getInt("adfundcointarget"));
				adfVO.setAdfundCoinNow(rs.getInt("adfundcoinnow"));
				adfVO.setAdfundSubmitDate(rs.getTimestamp("adfundsubmitdate"));
				adfVO.setAdfundStatus(rs.getString("adfundstatus"));
				adlist.add(adfVO);
				
			}
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. " + se.getMessage());
			
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

		return adlist;
	}


	@Override
	public List<ADFundVO> getAllByBoardID(String boardID) {
		List<ADFundVO> adlist = new ArrayList<ADFundVO>();
		ADFundVO adfVO = null;
		
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs =null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ALL_STMT_BY_BOARDID);
			pstmt.setString(1, boardID);
			rs = pstmt.executeQuery();
			
			while (rs.next()) {
				adfVO = new ADFundVO();
				adfVO.setAdfundID(rs.getString("adfundid"));
				adfVO.setMemberID(rs.getString("memberid"));
				adfVO.setBoardID(rs.getString("boardid"));
				adfVO.setAdministratorID(rs.getString("administratorid"));
				adfVO.setAdfundStartDate(rs.getTimestamp("adfundstartdate"));
				adfVO.setAdfundEndDate(rs.getTimestamp("adfundenddate"));
				adfVO.setAdfundDisplayDate(rs.getTimestamp("adfunddisplaydate"));
				adfVO.setAdfundTitle(rs.getString("adfundtitle"));
				adfVO.setAdfundType(rs.getString("adfundtype"));
				adfVO.setAdfundText(rs.getString("adfundtext"));
				adfVO.setAdfundHomePhoto(rs.getBytes("adfundhomephoto"));
				adfVO.setAdfundBoardPhoto(rs.getBytes("adfundboardphoto"));
				adfVO.setAdfundCoinTarget(rs.getInt("adfundcointarget"));
				adfVO.setAdfundCoinNow(rs.getInt("adfundcoinnow"));
				adfVO.setAdfundSubmitDate(rs.getTimestamp("adfundsubmitdate"));
				adfVO.setAdfundStatus(rs.getString("adfundstatus"));
				adlist.add(adfVO);
				
			}
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. " + se.getMessage());
			
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

		return adlist;
	}

	@Override
	public List<ADFundVO> getAllOkBoardPicWithStatus(String boardID) {
		List<ADFundVO> adlist = new ArrayList<ADFundVO>();
		ADFundVO adfVO = null;
		
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs =null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(AD_BOARD_OK_TO_DISPLAY);
			pstmt.setString(1, boardID);
			rs = pstmt.executeQuery();
			
			while (rs.next()) {
				adfVO = new ADFundVO();
				adfVO.setAdfundID(rs.getString("adfundid"));
				adfVO.setMemberID(rs.getString("memberid"));
				adfVO.setBoardID(rs.getString("boardid"));
				adfVO.setAdministratorID(rs.getString("administratorid"));
				adfVO.setAdfundStartDate(rs.getTimestamp("adfundstartdate"));
				adfVO.setAdfundEndDate(rs.getTimestamp("adfundenddate"));
				adfVO.setAdfundDisplayDate(rs.getTimestamp("adfunddisplaydate"));
				adfVO.setAdfundTitle(rs.getString("adfundtitle"));
				adfVO.setAdfundType(rs.getString("adfundtype"));
				adfVO.setAdfundText(rs.getString("adfundtext"));
				adfVO.setAdfundHomePhoto(rs.getBytes("adfundhomephoto"));
				adfVO.setAdfundBoardPhoto(rs.getBytes("adfundboardphoto"));
				adfVO.setAdfundCoinTarget(rs.getInt("adfundcointarget"));
				adfVO.setAdfundCoinNow(rs.getInt("adfundcoinnow"));
				adfVO.setAdfundSubmitDate(rs.getTimestamp("adfundsubmitdate"));
				adfVO.setAdfundStatus(rs.getString("adfundstatus"));
				adlist.add(adfVO);
				
			}
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. " + se.getMessage());
			
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

		return adlist;
	}

	@Override
	public List<ADFundVO> getAllWithADFundingStatus() {
		List<ADFundVO> adlist = new ArrayList<ADFundVO>();
		ADFundVO adfVO = null;
		
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs =null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ALL_BY_AD_FUNDING_STATUS);
			rs = pstmt.executeQuery();
			
			while (rs.next()) {
				adfVO = new ADFundVO();
				adfVO.setAdfundID(rs.getString("adfundid"));
				adfVO.setMemberID(rs.getString("memberid"));
				adfVO.setBoardID(rs.getString("boardid"));
				adfVO.setAdministratorID(rs.getString("administratorid"));
				adfVO.setAdfundStartDate(rs.getTimestamp("adfundstartdate"));
				adfVO.setAdfundEndDate(rs.getTimestamp("adfundenddate"));
				adfVO.setAdfundDisplayDate(rs.getTimestamp("adfunddisplaydate"));
				adfVO.setAdfundTitle(rs.getString("adfundtitle"));
				adfVO.setAdfundType(rs.getString("adfundtype"));
				adfVO.setAdfundText(rs.getString("adfundtext"));
				adfVO.setAdfundHomePhoto(rs.getBytes("adfundhomephoto"));
				adfVO.setAdfundBoardPhoto(rs.getBytes("adfundboardphoto"));
				adfVO.setAdfundCoinTarget(rs.getInt("adfundcointarget"));
				adfVO.setAdfundCoinNow(rs.getInt("adfundcoinnow"));
				adfVO.setAdfundSubmitDate(rs.getTimestamp("adfundsubmitdate"));
				adfVO.setAdfundStatus(rs.getString("adfundstatus"));
				adlist.add(adfVO);
				
			}
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. " + se.getMessage());
			
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

		return adlist;
	}

	@Override
	public List<ADFundVO> getAllWithNotFinishStatus() {
		List<ADFundVO> adlist = new ArrayList<ADFundVO>();
		ADFundVO adfVO = null;
		
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs =null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ALL_WITH_NOTFINISH_STATUS);
			rs = pstmt.executeQuery();
			
			while (rs.next()) {
				adfVO = new ADFundVO();
				adfVO.setAdfundID(rs.getString("adfundid"));
				adfVO.setMemberID(rs.getString("memberid"));
				adfVO.setBoardID(rs.getString("boardid"));
				adfVO.setAdministratorID(rs.getString("administratorid"));
				adfVO.setAdfundStartDate(rs.getTimestamp("adfundstartdate"));
				adfVO.setAdfundEndDate(rs.getTimestamp("adfundenddate"));
				adfVO.setAdfundDisplayDate(rs.getTimestamp("adfunddisplaydate"));
				adfVO.setAdfundTitle(rs.getString("adfundtitle"));
				adfVO.setAdfundType(rs.getString("adfundtype"));
				adfVO.setAdfundText(rs.getString("adfundtext"));
				adfVO.setAdfundHomePhoto(rs.getBytes("adfundhomephoto"));
				adfVO.setAdfundBoardPhoto(rs.getBytes("adfundboardphoto"));
				adfVO.setAdfundCoinTarget(rs.getInt("adfundcointarget"));
				adfVO.setAdfundCoinNow(rs.getInt("adfundcoinnow"));
				adfVO.setAdfundSubmitDate(rs.getTimestamp("adfundsubmitdate"));
				adfVO.setAdfundStatus(rs.getString("adfundstatus"));
				adlist.add(adfVO);
				
			}
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. " + se.getMessage());
			
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

		return adlist;
	}

	@Override
	public void inputPhoto(ADFundVO adfundVO) {
		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(INPUT_PHOTO);

			pstmt.setBytes(1, adfundVO.getAdfundHomePhoto());
			pstmt.setBytes(2, adfundVO.getAdfundBoardPhoto());
			pstmt.setString(3, adfundVO.getAdfundID());

			pstmt.executeUpdate();
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. " + se.getMessage());
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
					e.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace(System.err);
				}

			}
		}
		
	}

	@Override
	public List<ADFundVO> getADFundsByMemberID(String memberID) {
		List<ADFundVO> adlist = new ArrayList<ADFundVO>();
		ADFundVO adfVO = null;
		
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs =null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ALL_BY_MEMBERID);
			pstmt.setString(1, memberID);
			rs = pstmt.executeQuery();
			
			while (rs.next()) {
				adfVO = new ADFundVO();
				adfVO.setAdfundID(rs.getString("adfundid"));
				adfVO.setMemberID(rs.getString("memberid"));
				adfVO.setBoardID(rs.getString("boardid"));
				adfVO.setAdministratorID(rs.getString("administratorid"));
				adfVO.setAdfundStartDate(rs.getTimestamp("adfundstartdate"));
				adfVO.setAdfundEndDate(rs.getTimestamp("adfundenddate"));
				adfVO.setAdfundDisplayDate(rs.getTimestamp("adfunddisplaydate"));
				adfVO.setAdfundTitle(rs.getString("adfundtitle"));
				adfVO.setAdfundType(rs.getString("adfundtype"));
				adfVO.setAdfundText(rs.getString("adfundtext"));
				adfVO.setAdfundHomePhoto(rs.getBytes("adfundhomephoto"));
				adfVO.setAdfundBoardPhoto(rs.getBytes("adfundboardphoto"));
				adfVO.setAdfundCoinTarget(rs.getInt("adfundcointarget"));
				adfVO.setAdfundCoinNow(rs.getInt("adfundcoinnow"));
				adfVO.setAdfundSubmitDate(rs.getTimestamp("adfundsubmitdate"));
				adfVO.setAdfundStatus(rs.getString("adfundstatus"));
				adlist.add(adfVO);
				
			}
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. " + se.getMessage());
			
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

		return adlist;
	}

	@Override
	public Integer getADSubmitCount() {
		Integer count = 0;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_AD_SUBMIT_COUNT);

			rs = pstmt.executeQuery();

			while (rs.next()) {
				count= rs.getInt(1);
			}
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. " + se.getMessage());

		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

		return count;
	}
	
	
}
	
	

