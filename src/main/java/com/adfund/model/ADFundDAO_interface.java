package com.adfund.model;

import java.util.*;

import com.adfundRecord.model.ADFundRecordVO;
import com.adfundReport.model.*;

public interface ADFundDAO_interface {
	public void insert(ADFundVO adfundVO);
	public void updateByMem(ADFundVO adfundVO);
	public void updateByAd(ADFundVO adfundVO);
	public void updateCoinNow(ADFundVO adfundVO);
	public void delete(String adfundID);
	public ADFundVO  findByPrimaryKey(String adfundID);
	public List<ADFundVO> getAll();
	//查詢某募資的檢舉(一對多)(回傳set)
	public Set<ADFundReportVO> getADFundReportsByADFundID(String adfundID);
	//查詢某募資的參與明細(一對多)(回傳set)
	public Set<ADFundRecordVO> getADFundRecordsByADFundID(String adfundID);
	//查詢正在進行的募資
	public List<ADFundVO> getAllWithStatus();
	//查詢未達成目標的募資
	public List<ADFundVO> getAllWithNotFinishStatus();
	//查詢已提交未審核的募資
	public List<ADFundVO> getAllWithSubmitStatus();
	//查詢已通過審核的募資
	public List<ADFundVO> getAllWithPassStatus();
	//查詢未通過審核的募資
	public List<ADFundVO> getAllWithUnpassStatus();
	//萬用複合查詢(傳入參數型態Map)(回傳list)
	public List<ADFundVO> getAll(Map<String, String[]> map);	
	//挑出符合條件的首頁圖片以上首頁廣告輪播
	public List<ADFundVO> getAllOkPicWithStatus();	
	//挑出符合條件的專板圖片以上專板廣告輪播
	public List<ADFundVO> getAllOkBoardPicWithStatus(String boardID);
    //挑出符合專板編號的全部募資
	public List<ADFundVO> getAllByBoardID(String boardID);
	//挑出已過時間未達成目標的募資 要改募資狀態為AD_NOT_FINISH 並退代幣
	public List<ADFundVO> getAllWithADFundingStatus();
	//把假資料的圖片塞進資料庫
	public void inputPhoto(ADFundVO adfundVO);
	//挑出符合會員編號的募資
	public List<ADFundVO> getADFundsByMemberID(String memberID);
	//計算募資已提交未審核的筆數
	public Integer getADSubmitCount();
	
	
}
