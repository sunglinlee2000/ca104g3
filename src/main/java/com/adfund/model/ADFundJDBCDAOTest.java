//package com.adfund.model;
//
//import java.util.List;
//
//public class ADFundJDBCDAOTest {
//
//	public static void main(String[] args) {
//        ADFundJDBCDAO dao = new ADFundJDBCDAO();
//        
//
//        //新增
//		ADFundVO adfvo = new ADFundVO();
//		adfvo.setMemberID("M000006");
//		adfvo.setArtistID("ART000002");
//		adfvo.setAdministratorID("A000001");
//		adfvo.setAdfundStartDate(java.sql.Timestamp.valueOf("2018-11-20 18:06:30"));
//		adfvo.setAdfundEndDate(java.sql.Timestamp.valueOf("2018-11-27 18:06:30"));
//		adfvo.setAdfundDisplayDate(java.sql.Timestamp.valueOf("2018-12-02 18:06:30"));
//		adfvo.setAdfundTitle("五月天天天");
//        adfvo.setAdfundType("OTHER");
//        adfvo.setAdfundText("");
//        adfvo.setAdfundHomePhoto(null);
//        adfvo.setAdfundBoardPhoto(null);
//        adfvo.setAdfundCoinTarget(new Integer(5000));
//        adfvo.setAdfundCoinNow(new Integer(800));
//        adfvo.setAdfundSubmitDate(java.sql.Timestamp.valueOf("2018-11-30 18:06:30"));
//        adfvo.setAdfundStatus("AD_FUNDING");
//        dao.insert(adfvo);
//        System.out.println("..........");
//        
//        
//        //修改(發起者)
////        ADFundVO adfvo2 = new ADFundVO();
////        adfvo2.setAdfundID("AD000002");		
////		adfvo2.setAdfundDisplayDate(java.sql.Timestamp.valueOf("2018-12-01 18:06:30"));
////		adfvo2.setAdfundTitle("魔力紅來了");
////        adfvo2.setAdfundText("我沒錢");
////        adfvo2.setAdfundHomePhoto(null);
////        adfvo2.setAdfundBoardPhoto(null);
////        adfvo2.setAdfundCoinTarget(new Integer(3000));       
////        dao.updateByMem(adfvo2);
////        System.out.println("..........");
//        
//        
//          //修改(管理者)
////        ADFundVO adfvo5 = new ADFundVO();
////        adfvo5.setAdfundID("AD000003");		
////        adfvo5.setAdfundStatus("AD_FUNDING");
////        dao.updateByAd(adfvo5);
////        System.out.println("..........");
//        
////        
////        
////        //查一筆
////        ADFundVO adfVO3 = dao.findByPrimaryKey("AD000001");
////        System.out.println(adfVO3.getAdfundID());
////        System.out.println(adfVO3.getMemberID());
////        System.out.println(adfVO3.getArtistID());
////        System.out.println(adfVO3.getAdministratorID());
////        System.out.println(adfVO3.getAdfundStartDate());
////        System.out.println(adfVO3.getAdfundEndDate());
////        System.out.println(adfVO3.getAdfundDisplayDate());
////        System.out.println(adfVO3.getAdfundTitle());
////        System.out.println(adfVO3.getAdfundType());
////        System.out.println(adfVO3.getAdfundText());
////        System.out.println(adfVO3.getAdfundHomePhoto());
////        System.out.println(adfVO3.getAdfundBoardPhoto());
////        System.out.println(adfVO3.getAdfundCoinTarget());
////        System.out.println(adfVO3.getAdfundCoinNow());
////        System.out.println(adfVO3.getAdfundSubmitDate());
////        System.out.println(adfVO3.getAdfundStatus());
////        System.out.println("..........");
//        
//        
//        //查多筆
////        List<ADFundVO> adlist = dao.getAll();
////        for (ADFundVO aADFund : adlist) {
////            System.out.println(aADFund.getAdfundID());
////            System.out.println(aADFund.getMemberID());
////            System.out.println(aADFund.getArtistID());
////            System.out.println(aADFund.getAdministratorID());
////            System.out.println(aADFund.getAdfundStartDate());
////            System.out.println(aADFund.getAdfundEndDate());
////            System.out.println(aADFund.getAdfundDisplayDate());
////            System.out.println(aADFund.getAdfundTitle());
////            System.out.println(aADFund.getAdfundType());
////            System.out.println(aADFund.getAdfundText());
////            System.out.println(aADFund.getAdfundHomePhoto());
////            System.out.println(aADFund.getAdfundBoardPhoto());
////            System.out.println(aADFund.getAdfundCoinTarget());
////            System.out.println(aADFund.getAdfundCoinNow());
////            System.out.println(aADFund.getAdfundSubmitDate());
////            System.out.println(aADFund.getAdfundStatus());
////            System.out.println("..........");
////        	
////        }      
//	}
//}
