package com.adfund.model;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public class ADPhotoTest {
	
	public static void main(String[] args) {
		
		ADFundJDBCDAO adfundJDBCDAO = new ADFundJDBCDAO();
		ADFundVO adfundVO = new ADFundVO();
		
		
		for(int i = 1; i <=9; i++) {
			byte[] homePhoto = null;
			byte[] boardPhoto = null;
			adfundVO.setAdfundID("AD00000"+i);
			
			try {
				homePhoto = getPictureByteArray("WebContent/front-end/res/img/inputADPhoto/"+i+".jpg");
				boardPhoto = getPictureByteArray("WebContent/front-end/res/img/inputADPhoto/"+i+".jpg");
			}catch(IOException e ) {
				e.printStackTrace(System.err);
			}
			
			adfundVO.setAdfundHomePhoto(homePhoto);
			adfundVO.setAdfundBoardPhoto(boardPhoto);
			adfundJDBCDAO.inputPhoto(adfundVO);
			System.out.println("OK");
		}
		
	}
	
	
	public static byte[] getPictureByteArray(String path) throws IOException {
		File file = new File(path);
		FileInputStream fis = new FileInputStream(file);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		byte[] buffer = new byte[8192];
		int i;
		while((i = fis.read(buffer)) != -1) {
			baos.write(buffer, 0, i);
		}
		baos.close();
		fis.close();
		
		return baos.toByteArray();
	}

}
