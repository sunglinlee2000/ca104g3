package com.adfund.model;


import java.sql.Timestamp;

public class ADFundVO implements java.io.Serializable{
	private String adfundID;
	private String memberID;
	private String boardID;
	private String administratorID;
	private Timestamp adfundStartDate;
	private Timestamp adfundEndDate;
	private Timestamp adfundDisplayDate;
	private String adfundTitle;
	private String adfundType;
	private String adfundText;
	private byte[] adfundHomePhoto;
	private byte[] adfundBoardPhoto;
	private Integer adfundCoinTarget;
	private Integer adfundCoinNow;
	private Timestamp adfundSubmitDate;
	private String adfundStatus;
	public String getAdfundID() {
		return adfundID;
	}
	public void setAdfundID(String adfundID) {
		this.adfundID = adfundID;
	}
	public String getMemberID() {
		return memberID;
	}
	public void setMemberID(String memberID) {
		this.memberID = memberID;
	}
	public String getBoardID() {
		return boardID;
	}
	public void setBoardID(String boardID) {
		this.boardID = boardID;
	}
	public String getAdministratorID() {
		return administratorID;
	}
	public void setAdministratorID(String administratorID) {
		this.administratorID = administratorID;
	}
	public Timestamp getAdfundStartDate() {
		return adfundStartDate;
	}
	public void setAdfundStartDate(Timestamp adfundStartDate) {
		this.adfundStartDate = adfundStartDate;
	}
	public Timestamp getAdfundEndDate() {
		return adfundEndDate;
	}
	public void setAdfundEndDate(Timestamp adfundEndDate) {
		this.adfundEndDate = adfundEndDate;
	}
	public Timestamp getAdfundDisplayDate() {
		return adfundDisplayDate;
	}
	public void setAdfundDisplayDate(Timestamp adfundDisplayDate) {
		this.adfundDisplayDate = adfundDisplayDate;
	}
	public String getAdfundTitle() {
		return adfundTitle;
	}
	public void setAdfundTitle(String adfundTitle) {
		this.adfundTitle = adfundTitle;
	}
	public String getAdfundType() {
		return adfundType;
	}
	public void setAdfundType(String adfundType) {
		this.adfundType = adfundType;
	}
	public String getAdfundText() {
		return adfundText;
	}
	public void setAdfundText(String adfundText) {
		this.adfundText = adfundText;
	}
	public byte[] getAdfundHomePhoto() {
		return adfundHomePhoto;
	}
	public void setAdfundHomePhoto(byte[] adfundHomePhoto) {
		this.adfundHomePhoto = adfundHomePhoto;
	}
	public byte[] getAdfundBoardPhoto() {
		return adfundBoardPhoto;
	}
	public void setAdfundBoardPhoto(byte[] adfundBoardPhoto) {
		this.adfundBoardPhoto = adfundBoardPhoto;
	}
	public Integer getAdfundCoinTarget() {
		return adfundCoinTarget;
	}
	public void setAdfundCoinTarget(Integer adfundCoinTarget) {
		this.adfundCoinTarget = adfundCoinTarget;
	}
	public Integer getAdfundCoinNow() {
		return adfundCoinNow;
	}
	public void setAdfundCoinNow(Integer adfundCoinNow) {
		this.adfundCoinNow = adfundCoinNow;
	}
	public Timestamp getAdfundSubmitDate() {
		return adfundSubmitDate;
	}
	public void setAdfundSubmitDate(Timestamp adfundSubmitDate) {
		this.adfundSubmitDate = adfundSubmitDate;
	}
	public String getAdfundStatus() {
		return adfundStatus;
	}
	public void setAdfundStatus(String adfundStatus) {
		this.adfundStatus = adfundStatus;
	}
	
	
	
}
