package com.adfund.model;

import java.sql.*;
import java.util.*;

import com.adfundRecord.model.ADFundRecordVO;
import com.adfundReport.model.ADFundReportVO;

public class ADFundJDBCDAO implements ADFundDAO_interface {
	String driver = "oracle.jdbc.driver.OracleDriver";
	String url = "jdbc:oracle:thin:@localhost:1521:XE";
	String userid = "CA104G3";
	String passwd = "123456";

	private static final String INSERT_STMT = "INSERT INTO ADFUND VALUES "
			+ "('AD'||(LPAD(TO_CHAR(ADFUND_SEQ.NEXTVAL),6,'0')),?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	private static final String GET_ALL_STMT = "SELECT * FROM ADFUND";
	private static final String GET_ONE_STMT = "SELECT * FROM ADFUND WHERE ADFUNDID = ?";
	private static final String UPDATE_BY_MEM = "UPDATE ADFUND SET "
			+ "ADFUNDDISPLAYDATE=?, ADFUNDTITLE=?, ADFUNDTEXT=?, ADFUNDHOMEPHOTO=?, ADFUNDBOARDPHOTO=?, ADFUNDCOINTARGET=? WHERE ADFUNDID = ?";

	private static final String UPDATE_BY_AD = "UPDATE ADFUND SET "
			+ "ADFUNDSTATUS =? WHERE ADFUNDID = ?";
	
	//塞圖
	private static final String INPUT_PHOTO = "UPDATE ADFUND SET ADFUNDHOMEPHOTO=?, ADFUNDBOARDPHOTO=? WHERE ADFUNDID = ?";

	@Override
	public void insert(ADFundVO adfundVO) {

		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(INSERT_STMT);

			pstmt.setString(1, adfundVO.getMemberID());
			pstmt.setString(2, adfundVO.getBoardID());
			pstmt.setString(3, adfundVO.getAdministratorID());
			pstmt.setTimestamp(4, adfundVO.getAdfundStartDate());
			pstmt.setTimestamp(5, adfundVO.getAdfundEndDate());
			pstmt.setTimestamp(6, adfundVO.getAdfundDisplayDate());
			pstmt.setString(7, adfundVO.getAdfundTitle());
			pstmt.setString(8, adfundVO.getAdfundType());
			pstmt.setString(9, adfundVO.getAdfundText());
			pstmt.setBytes(10, adfundVO.getAdfundHomePhoto());
			pstmt.setBytes(11, adfundVO.getAdfundBoardPhoto());
			pstmt.setInt(12, adfundVO.getAdfundCoinTarget());
			pstmt.setInt(13, adfundVO.getAdfundCoinNow());
			pstmt.setTimestamp(14, adfundVO.getAdfundSubmitDate());
			pstmt.setString(15, adfundVO.getAdfundStatus());

			pstmt.executeUpdate();

		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. " + e.getMessage());

		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. " + se.getMessage());

		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {

				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}

		}
	}

	@Override
	public void updateByMem(ADFundVO adfundVO) {

		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(UPDATE_BY_MEM);

			pstmt.setTimestamp(1, adfundVO.getAdfundDisplayDate());
			pstmt.setString(2, adfundVO.getAdfundTitle());
			pstmt.setString(3, adfundVO.getAdfundText());
			pstmt.setBytes(4, adfundVO.getAdfundHomePhoto());
			pstmt.setBytes(5, adfundVO.getAdfundBoardPhoto());
			pstmt.setInt(6, adfundVO.getAdfundCoinTarget());
			pstmt.setString(7, adfundVO.getAdfundID());

			pstmt.executeUpdate();
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. " + e.getMessage());
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. " + se.getMessage());
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
					e.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace(System.err);
				}

			}
		}

	}
	
	
	@Override
	public void updateByAd(ADFundVO adfundVO) {
		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(UPDATE_BY_AD);

			pstmt.setString(1, adfundVO.getAdfundStatus());
			pstmt.setString(2, adfundVO.getAdfundID());


			pstmt.executeUpdate();
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. " + e.getMessage());
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. " + se.getMessage());
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
					e.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace(System.err);
				}

			}
		}

		
	}
	

	@Override
	public ADFundVO findByPrimaryKey(String adfundid) {

		ADFundVO adfVO = new ADFundVO();
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(GET_ONE_STMT);

			pstmt.setString(1, adfundid);

			rs = pstmt.executeQuery();
			
			while (rs.next()) {
				adfVO = new ADFundVO();
				adfVO.setAdfundID(rs.getString("adfundid"));
				adfVO.setMemberID(rs.getString("memberid"));
				adfVO.setBoardID(rs.getString("boardid"));
				adfVO.setAdministratorID(rs.getString("administratorid"));
				adfVO.setAdfundStartDate(rs.getTimestamp("adfundstartdate"));
				adfVO.setAdfundEndDate(rs.getTimestamp("adfundenddate"));
				adfVO.setAdfundDisplayDate(rs.getTimestamp("adfunddisplaydate"));
				adfVO.setAdfundTitle(rs.getString("adfundtitle"));
				adfVO.setAdfundType(rs.getString("adfundtype"));
				adfVO.setAdfundText(rs.getString("adfundtext"));
				adfVO.setAdfundHomePhoto(rs.getBytes("adfundhomephoto"));
				adfVO.setAdfundBoardPhoto(rs.getBytes("adfundboardphoto"));
				adfVO.setAdfundCoinTarget(rs.getInt("adfundcointarget"));
				adfVO.setAdfundCoinNow(rs.getInt("adfundcoinnow"));
				adfVO.setAdfundSubmitDate(rs.getTimestamp("adfundsubmitdate"));
				adfVO.setAdfundStatus(rs.getString("adfundstatus"));
			}
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. " + e.getMessage());

		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. " + se.getMessage());

		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return adfVO;
	}

	@Override
	public List<ADFundVO> getAll() {
		List<ADFundVO> adlist = new ArrayList<ADFundVO>();
		ADFundVO adfVO = null;
		
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs =null;

		try {
			
			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(GET_ALL_STMT);
			rs = pstmt.executeQuery();
			
			while (rs.next()) {
				adfVO = new ADFundVO();
				adfVO.setAdfundID(rs.getString("adfundid"));
				adfVO.setMemberID(rs.getString("memberid"));
				adfVO.setBoardID(rs.getString("boardid"));
				adfVO.setAdministratorID(rs.getString("administratorid"));
				adfVO.setAdfundStartDate(rs.getTimestamp("adfundstartdate"));
				adfVO.setAdfundEndDate(rs.getTimestamp("adfundenddate"));
				adfVO.setAdfundDisplayDate(rs.getTimestamp("adfunddisplaydate"));
				adfVO.setAdfundTitle(rs.getString("adfundtitle"));
				adfVO.setAdfundType(rs.getString("adfundtype"));
				adfVO.setAdfundText(rs.getString("adfundtext"));
				adfVO.setAdfundHomePhoto(rs.getBytes("adfundhomephoto"));
				adfVO.setAdfundBoardPhoto(rs.getBytes("adfundboardphoto"));
				adfVO.setAdfundCoinTarget(rs.getInt("adfundcointarget"));
				adfVO.setAdfundCoinNow(rs.getInt("adfundcoinnow"));
				adfVO.setAdfundSubmitDate(rs.getTimestamp("adfundsubmitdate"));
				adfVO.setAdfundStatus(rs.getString("adfundstatus"));
				adlist.add(adfVO);
				
			}
		} catch (ClassNotFoundException e) {

			throw new RuntimeException("Couldn't load database driver. " + e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. " + se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

		return adlist;
	}

	@Override
	public void delete(String adfundID) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updateCoinNow(ADFundVO adfundVO) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Set<ADFundReportVO> getADFundReportsByADFundID(String adfundID) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Set<ADFundRecordVO> getADFundRecordsByADFundID(String adfundID) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<ADFundVO> getAllWithStatus() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<ADFundVO> getAllWithSubmitStatus() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<ADFundVO> getAllWithPassStatus() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<ADFundVO> getAllWithUnpassStatus() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<ADFundVO> getAll(Map<String, String[]> map) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<ADFundVO> getAllOkPicWithStatus() {
		// TODO Auto-generated method stub
		return null;
	}

//	@Override
//	public List<ADFundVO> getAllOkBoardPicWithStatus(String boardID) {
//		// TODO Auto-generated method stub
//		return null;
//	}

	@Override
	public List<ADFundVO> getAllWithNotFinishStatus() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<ADFundVO> getAllOkBoardPicWithStatus(String boardID) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<ADFundVO> getAllByBoardID(String boardID) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<ADFundVO> getAllWithADFundingStatus() {
		// TODO Auto-generated method stub
		return null;
	}


	
	
	@Override
	public void inputPhoto(ADFundVO adfundVO) {
		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			Class.forName(driver);
			con =DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(INPUT_PHOTO);

			pstmt.setBytes(1, adfundVO.getAdfundHomePhoto());
			pstmt.setBytes(2, adfundVO.getAdfundBoardPhoto());
			pstmt.setString(3, adfundVO.getAdfundID());

			pstmt.executeUpdate();
		}  catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver." + e.getMessage());
		
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured." + se.getMessage());
		
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if(con != null) {
				try {
					con.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
		}
		
	}

	@Override
	public List<ADFundVO> getADFundsByMemberID(String memberID) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer getADSubmitCount() {
		// TODO Auto-generated method stub
		return null;
	}


}
