package com.message.model;

import java.util.*;

public interface MessageDAO_interface {
	public void insert(MessageVO messageVO);
	public void update(MessageVO messageVO);
	public void delete(String messageID);
	public MessageVO findByPrimaryKey(String messageID);
	public List<MessageVO> getAll();
}
