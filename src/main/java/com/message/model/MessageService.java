package com.message.model;

import java.sql.Timestamp;
import java.util.List;

public class MessageService {

	private MessageDAO_interface dao;

	public MessageService() {
		dao = new MessageDAO();
	}

	public MessageVO addMessage(String messageID, String chatBoxID, String memberID, String messageContent,
			Timestamp messageDate) {

		MessageVO messageVO = new MessageVO();

		messageVO.setMessageID(messageID);
		messageVO.setChatBoxID(chatBoxID);
		messageVO.setMemberID(memberID);
		messageVO.setMessageContent(messageContent);
		messageVO.setMessageDate(messageDate);
		dao.insert(messageVO);

		return messageVO;
	}

	public MessageVO updateMessage(String messageID, String chatBoxID, String memberID, String messageContent,
			Timestamp messageDate) {

		MessageVO messageVO = new MessageVO();

		messageVO.setMessageID(messageID);
		messageVO.setChatBoxID(chatBoxID);
		messageVO.setMemberID(memberID);
		messageVO.setMessageContent(messageContent);
		messageVO.setMessageDate(messageDate);
		dao.update(messageVO);

		return messageVO;
	}

	public void deleteMessage(String messageID) {
		dao.delete(messageID);
	}

	public MessageVO getOneMessage(String messageID) {
		return dao.findByPrimaryKey(messageID);
	}

	public List<MessageVO> getAll() {
		return dao.getAll();
	}
}
