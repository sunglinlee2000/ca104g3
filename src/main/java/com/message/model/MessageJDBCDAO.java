package com.message.model;

import java.sql.*;
import java.text.*;
import java.util.*;



public class MessageJDBCDAO implements MessageDAO_interface {
	private static final String DRIVER = "oracle.jdbc.driver.OracleDriver";
	private static final String URL = "jdbc:oracle:thin:@localhost:1521:XE";
	private static final String USERID = "CA104G3";
	private static final String PASSWD = "123456";

	private static final String INSERT_STMT = "INSERT INTO message (messageID, chatBoxID, memberID, messageContent, messageDate) "
			+ "VALUES ('MES'||LPAD(to_char(message_seq.NEXTVAL), 6, '0'), ?, ?, ?, ?)";
	private static final String GET_ALL_STMT = "SELECT messageID, chatBoxID, memberID, messageContent, messageDate "
			+ "FROM message";
	private static final String GET_ONE_STMT = "SELECT messageID, chatBoxID, memberID, messageContent, messageDate "
			+ "FROM message WHERE messageID = ?";
	private static final String DELETE_STMT = "DELETE FROM message WHERE messageID = ?";
	private static final String UPDATE = "UPDATE message set chatBoxID = ?, memberID = ?, messageContent = ?, messageDate = ? "
			+ "WHERE messageID = ?";

	@Override
	public void insert(MessageVO messageVO) {
		Connection con = null;
		PreparedStatement pstmt = null;

		try {
			Class.forName(DRIVER);
			con = DriverManager.getConnection(URL, USERID, PASSWD);
			pstmt = con.prepareStatement(INSERT_STMT);
			
			pstmt.setString(1, messageVO.getChatBoxID());
			pstmt.setString(2, messageVO.getMemberID());
			pstmt.setString(3, messageVO.getMessageContent());
			pstmt.setTimestamp(4, messageVO.getMessageDate());
			
			pstmt.executeUpdate();
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. " + e.getMessage());
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. " + se.getMessage());
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		
	}

	@Override
	public void update(MessageVO messageVO) {
		Connection con = null;
		PreparedStatement pstmt = null;

		try {
			Class.forName(DRIVER);
			con = DriverManager.getConnection(URL, USERID, PASSWD);
			pstmt = con.prepareStatement(UPDATE);
			
			pstmt.setString(1, messageVO.getChatBoxID());
			pstmt.setString(2, messageVO.getMemberID());
			pstmt.setString(3, messageVO.getMessageContent());
			pstmt.setTimestamp(4, messageVO.getMessageDate());
			pstmt.setString(5, messageVO.getMessageID());
			
			pstmt.executeUpdate();
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. " + e.getMessage());
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. " + se.getMessage());
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

			
			
		
	}

	@Override
	public void delete(String messageID) {
		Connection con = null;
		PreparedStatement pstmt = null;

		try {
			Class.forName(DRIVER);
			con = DriverManager.getConnection(URL, USERID, PASSWD);
			pstmt = con.prepareStatement(DELETE_STMT);
			
			pstmt.setString(1, messageID);
			
			pstmt.executeUpdate();
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. " + e.getMessage());
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. " + se.getMessage());
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		
	}

	@Override
	public MessageVO findByPrimaryKey(String messageID) {
		MessageVO messageVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			Class.forName(DRIVER);
			con = DriverManager.getConnection(URL, USERID, PASSWD);
			pstmt = con.prepareStatement(GET_ONE_STMT);
			
			pstmt.setString(1, messageID);
			
			rs = pstmt.executeQuery();

			while (rs.next()) {
				messageVO = new MessageVO();
				messageVO.setMessageID(rs.getString("messageID"));
				messageVO.setChatBoxID(rs.getString("chatBoxID"));
				messageVO.setMemberID(rs.getString("memberID"));
				messageVO.setMessageContent(rs.getString("messageContent"));
				messageVO.setMessageDate(rs.getTimestamp("messageDate"));
			}

		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. " + e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. " + se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return messageVO;
	}

	@Override
	public List<MessageVO> getAll() {
		List<MessageVO> list = new ArrayList<MessageVO>();
		MessageVO messageVO = null;
		
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			Class.forName(DRIVER);
			con = DriverManager.getConnection(URL, USERID, PASSWD);
			pstmt = con.prepareStatement(GET_ALL_STMT);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				messageVO = new MessageVO();
				messageVO.setMessageID(rs.getString("messageID"));
				messageVO.setChatBoxID(rs.getString("chatBoxID"));
				messageVO.setMemberID(rs.getString("memberID"));
				messageVO.setMessageContent(rs.getString("messageContent"));
				messageVO.setMessageDate(rs.getTimestamp("messageDate"));
				list.add(messageVO);
			}

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. " + e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. " + se.getMessage());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return list;		
	}
	
	public static void main(String[] arqs) {
		MessageJDBCDAO dao = new MessageJDBCDAO();
		
//		新增
//		MessageVO messageVO1 = new MessageVO();
//		messageVO1.setChatBoxID("CB000001");
//		messageVO1.setMemberID("M000001");
//		messageVO1.setMessageContent("啊啊啊啊啊嗚嗚嗚");
//		messageVO1.setMessageDate(new Timestamp(System.currentTimeMillis()));
//		dao.insert(messageVO1);
		
//		修改
//		MessageVO messageVO2 = new MessageVO();
//		messageVO2.setChatBoxID("CB000001");
//		messageVO2.setMemberID("M000001");
//		messageVO2.setMessageContent("耶耶耶");
//		messageVO2.setMessageDate(new Timestamp(System.currentTimeMillis()));
//		messageVO2.setMessageID("MES000001");
//		dao.update(messageVO2);
		
//		刪除
//		dao.delete("MES000003");
		
//		查詢
//		MessageVO messageVO3 = dao.findByPrimaryKey("MES000001");
//		System.out.println("messageID : " + messageVO3.getMessageID());
//		System.out.println("chatBoxID : " + messageVO3.getChatBoxID());
//		System.out.println("memberID : " + messageVO3.getMemberID());
//		System.out.println("messageContent : " + messageVO3.getMessageContent());
//		System.out.println("messageDate : " + new SimpleDateFormat("yyyy-MM-dd HH:mm").format(messageVO3.getMessageDate()));
		
//		System.out.println("messageDate : " + messageVO3.getMessageDate());

//		查全部
		List<MessageVO> list = dao.getAll();
		for(MessageVO aMessage : list) {
			System.out.println("messageID : " + aMessage.getMessageID());
			System.out.println("chatBoxID : " + aMessage.getChatBoxID());
			System.out.println("memberID : " + aMessage.getMemberID());
			System.out.println("messageContent : " + aMessage.getMessageContent());
			System.out.println("messageDate : " + new SimpleDateFormat("yyyy-MM-dd HH:mm").format(aMessage.getMessageDate()));
			
			System.out.println("messageDate : " + aMessage.getMessageDate());
			System.out.println("----------------------------------------------------------");
		}
		
	}

}
