package com.message.model;

import java.sql.*;

public class MessageVO implements java.io.Serializable {
	private String messageID;
	private String chatBoxID;
	private String memberID;
	private String messageContent;
	private Timestamp messageDate;

	public String getMessageID() {
		return messageID;
	}

	public void setMessageID(String messageID) {
		this.messageID = messageID;
	}

	public String getChatBoxID() {
		return chatBoxID;
	}

	public void setChatBoxID(String chatBoxID) {
		this.chatBoxID = chatBoxID;
	}

	public String getMemberID() {
		return memberID;
	}

	public void setMemberID(String memberID) {
		this.memberID = memberID;
	}

	public String getMessageContent() {
		return messageContent;
	}

	public void setMessageContent(String messageContent) {
		this.messageContent = messageContent;
	}

	public Timestamp getMessageDate() {
		return messageDate;
	}

	public void setMessageDate(Timestamp messageDate) {
		this.messageDate = messageDate;
	}

}
