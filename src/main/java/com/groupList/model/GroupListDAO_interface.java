package com.groupList.model;

import java.sql.Connection;
import java.util.*;
import com.groupMember.model.*;

public interface GroupListDAO_interface {
//  同時新增揪團、主揪、創建聊天室
	public void insert(GroupListVO groupListVO);
	public void update(GroupListVO groupListVO);
	
//	下架揪團
	public void update_noLonger(GroupListVO groupListVO, Connection con);
	
//	public void delete(String groupid);
	public GroupListVO findByPrimaryKey(String groupID);
	public List<GroupListVO> getAllByBoardID(String boardID);
	
//	揪團關鍵字查詢
	public List<GroupListVO> getAllByBoardID(Map<String, String[]> map);
	
//	個人頁面用
	public List<GroupListVO> getAllByMemberID(String memberID);
	
}
