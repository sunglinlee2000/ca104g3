package com.groupList.model;

import java.util.*;
import java.sql.*;
import javax.naming.*;
import javax.sql.*;

import com.chatBox.model.ChatBoxService;
import com.groupMember.model.*;

import jdbc.util.GroupByQuery.jdbcUtil_GroupByQuery;

public class GroupListDAO implements GroupListDAO_interface {
	private static DataSource ds = null;
	static {
		try {
			Context ctx = new InitialContext();
			ds = (DataSource) ctx.lookup("java:comp/env/jdbc/CA104G3DB");
		} catch (NamingException e) {
			e.printStackTrace();
		}
	}

	private static final String INSERT_STMT = "INSERT INTO groupList (groupID, memberID, boardID, groupTitle, groupLocation, latitude, longitude, "
			+ "groupStartDate, groupMeetDate, groupContent, groupStatus) "
			+ "VALUES ('G'||LPAD(to_char(groupList_seq.NEXTVAL), 6, '0'), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
	private static final String GET_ALL_STMT = "SELECT groupID, memberID, boardID, groupTitle, groupLocation, latitude, longitude, "
			+ "groupStartDate, groupMeetDate, groupContent, groupStatus, chatBoxID FROM groupList  WHERE boardID = ? "
			+ " and groupStatus IN ('GROUP_GROUPING', 'GROUP_GROUPED', 'GROUP_FINISHED') ORDER BY groupID DESC";
	private static final String GET_ONE_STMT = "SELECT groupID, memberID, boardID, groupTitle, groupLocation, latitude, longitude, "
			+ "groupStartDate, groupMeetDate, groupContent, groupStatus, chatBoxID FROM groupList WHERE groupID = ?";
	private static final String UPDATE = "UPDATE groupList set groupTitle = ?, groupLocation = ?, latitude = ?, "
			+ "longitude = ?, groupMeetDate = ?, groupContent = ?, groupStatus = ? WHERE groupID = ?";
	private static final String GET_ALL_ByMemberID_STMT = "SELECT groupID, memberID, boardID, groupTitle, groupLocation, latitude, longitude, "
			+ "groupStartDate, groupMeetDate, groupContent, groupStatus, chatBoxID FROM groupList  WHERE memberID = ? "
			+ " and groupStatus IN ('GROUP_GROUPING', 'GROUP_GROUPED', 'GROUP_FINISHED') ORDER BY groupID DESC";
	private static final String UPDATE_CHATBOXID = "UPDATE groupList set chatBoxID = ? WHERE groupID = ?";
	private static final String UPDATE_GROUPSTATUS = "UPDATE groupList set groupStatus = ? WHERE groupID = ?";

	private static final String GROUPING = "GROUP_GROUPING";
	private static final String GROUPED = "GROUP_GROUPED";
	private static final String FAILED = "GROUP_FAILED";
	private static final String FINISHED = "GROUP_FINISHED";

	@Override
	public void insert(GroupListVO groupListVO) {
		Connection con = null;
		PreparedStatement pstmt = null;
		PreparedStatement pstmt2 = null;

		try {
			con = ds.getConnection();
			con.setAutoCommit(false);

//			先新增揪團
			String cols[] = { "groupID" };
			pstmt = con.prepareStatement(INSERT_STMT, cols);
			String memberID = groupListVO.getMemberID();

			pstmt.setString(1, memberID);
			pstmt.setString(2, groupListVO.getBoardID());
			pstmt.setString(3, groupListVO.getGroupTitle());
			pstmt.setString(4, groupListVO.getGroupLocation());
			pstmt.setDouble(5, groupListVO.getLatitude());
			pstmt.setDouble(6, groupListVO.getLongitude());
			pstmt.setTimestamp(7, new Timestamp(System.currentTimeMillis()));
			pstmt.setTimestamp(8, groupListVO.getGroupMeetDate());
			pstmt.setString(9, groupListVO.getGroupContent());
			pstmt.setString(10, GROUPING);

			pstmt.executeUpdate();

//			取得對應的自增主鍵
			String next_groupID = null;
			ResultSet rs = pstmt.getGeneratedKeys();
			if (rs.next()) {
				next_groupID = rs.getString(1);
				System.out.println("自增主鍵值= " + next_groupID + "（剛新增成功的揪團編號）");
			} else {
				System.out.println("未取得自增主鍵值");
			}
			rs.close();

//			再同時新增主揪
			GroupMemberService groupMemberSvc = new GroupMemberService();
			groupMemberSvc.addGroupInitiator(next_groupID, memberID, con);

//			再創建群組聊天室
			ChatBoxService chatBoxSvc = new ChatBoxService();
			String chatBoxID = chatBoxSvc.addChatBox_newGroup(groupListVO, "CHATBOX_GROUP", con);

			pstmt2 = con.prepareStatement(UPDATE_CHATBOXID);
			pstmt2.setString(1, chatBoxID);
			pstmt2.setString(2, next_groupID);
			pstmt2.executeUpdate();

//			2●設定於 pstm.executeUpdate()之後
			con.commit();
			con.setAutoCommit(true);
			System.out.println(memberID + "會員創建揪團且為主揪");

		} catch (SQLException se) {
			if (con != null) {
				try {
					// 3●設定於當有exception發生時之catch區塊內
					System.err.print("Transaction is being ");
					System.err.println("rolled back-由-groupList");
					con.rollback();
				} catch (SQLException excep) {
					throw new RuntimeException("rollback error occured. " + excep.getMessage());
				}
			}
			throw new RuntimeException("A database error occured. " + se.getMessage());
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}

	@Override
	public void update(GroupListVO groupListVO) {
		Connection con = null;
		PreparedStatement pstmt = null;

		try {
			con = ds.getConnection();
			pstmt = con.prepareStatement(UPDATE);

//			pstmt.setString(1, "M000001");
//			pstmt.setString(2, "B000001");
			pstmt.setString(1, groupListVO.getGroupTitle());
			pstmt.setString(2, groupListVO.getGroupLocation());
			pstmt.setDouble(3, groupListVO.getLatitude());
			pstmt.setDouble(4, groupListVO.getLongitude());
			pstmt.setTimestamp(5, groupListVO.getGroupMeetDate());
			pstmt.setString(6, groupListVO.getGroupContent());
			pstmt.setString(7, GROUPING);
			pstmt.setString(8, groupListVO.getGroupID());

			pstmt.executeUpdate();
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. " + se.getMessage());
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}

	@Override
	public void update_noLonger(GroupListVO groupListVO, Connection con) {
		PreparedStatement pstmt = null;

		try {

			pstmt = con.prepareStatement(UPDATE_GROUPSTATUS);
			pstmt.setString(1, FAILED);
			pstmt.setString(2, groupListVO.getGroupID());
			pstmt.executeUpdate();

		} catch (SQLException se) {
			if (con != null) {
				try {
					// 3●設定於當有exception發生時之catch區塊內
					System.err.print("Transaction is being ");
					System.err.println("rolled back由groupList");
					con.rollback();
				} catch (SQLException excep) {
					throw new RuntimeException("rollback error occured. " + excep.getMessage());
				}
			}
			throw new RuntimeException("A database error occured. " + se.getMessage());
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
		}

	}

	@Override
	public GroupListVO findByPrimaryKey(String groupID) {
		GroupListVO groupListVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ONE_STMT);

			pstmt.setString(1, groupID);

			rs = pstmt.executeQuery();

			while (rs.next()) {
				groupListVO = new GroupListVO();
				groupListVO.setGroupID(rs.getString("groupID"));
				groupListVO.setMemberID(rs.getString("memberID"));
				groupListVO.setBoardID(rs.getString("boardID"));
				groupListVO.setGroupTitle(rs.getString("groupTitle"));
				groupListVO.setGroupLocation(rs.getString("groupLocation"));
				groupListVO.setLatitude(rs.getDouble("latitude"));
				groupListVO.setLongitude(rs.getDouble("longitude"));
				groupListVO.setGroupStartDate(rs.getTimestamp("groupStartDate"));
				groupListVO.setGroupMeetDate(rs.getTimestamp("groupMeetDate"));
				groupListVO.setGroupContent(rs.getString("groupcontent"));
				groupListVO.setGroupStatus(rs.getString("groupstatus"));
				groupListVO.setChatBoxID(rs.getString("chatBoxID"));
			}

		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. " + se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return groupListVO;

	}

	@Override
	public List<GroupListVO> getAllByBoardID(String boardID) {
		List<GroupListVO> list = new ArrayList<GroupListVO>();
		GroupListVO groupListVO = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ALL_STMT);

			pstmt.setString(1, boardID);

			rs = pstmt.executeQuery();

			while (rs.next()) {
				groupListVO = new GroupListVO();
				groupListVO.setGroupID(rs.getString("groupID"));
				groupListVO.setMemberID(rs.getString("memberID"));
				groupListVO.setBoardID(rs.getString("boardID"));
				groupListVO.setGroupTitle(rs.getString("groupTitle"));
				groupListVO.setGroupLocation(rs.getString("groupLocation"));
				groupListVO.setLatitude(rs.getDouble("latitude"));
				groupListVO.setLongitude(rs.getDouble("longitude"));
				groupListVO.setGroupStartDate(rs.getTimestamp("groupStartDate"));
				groupListVO.setGroupMeetDate(rs.getTimestamp("groupMeetDate"));
				groupListVO.setGroupContent(rs.getString("groupcontent"));
				groupListVO.setGroupStatus(rs.getString("groupstatus"));
				groupListVO.setChatBoxID(rs.getString("chatBoxID"));
				list.add(groupListVO);
			}

			// Handle any driver errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. " + se.getMessage());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return list;
	}

	@Override
	public List<GroupListVO> getAllByBoardID(Map<String, String[]> map) {
		List<GroupListVO> list = new ArrayList<GroupListVO>();
		GroupListVO groupListVO = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			con = ds.getConnection();
			String finalSQL = "select * from groupList " + jdbcUtil_GroupByQuery.get_WhereCondition(map)
					+ " and groupStatus IN ('GROUP_GROUPING', 'GROUP_GROUPED', 'GROUP_FINISHED') order by groupID DESC";
			pstmt = con.prepareStatement(finalSQL);
			System.out.println("●●finalSQL(by DAO) = " + finalSQL);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				groupListVO = new GroupListVO();
				groupListVO.setGroupID(rs.getString("groupID"));
				groupListVO.setMemberID(rs.getString("memberID"));
				groupListVO.setBoardID(rs.getString("boardID"));
				groupListVO.setGroupTitle(rs.getString("groupTitle"));
				groupListVO.setGroupLocation(rs.getString("groupLocation"));
				groupListVO.setLatitude(rs.getDouble("latitude"));
				groupListVO.setLongitude(rs.getDouble("longitude"));
				groupListVO.setGroupStartDate(rs.getTimestamp("groupStartDate"));
				groupListVO.setGroupMeetDate(rs.getTimestamp("groupMeetDate"));
				groupListVO.setGroupContent(rs.getString("groupcontent"));
				groupListVO.setGroupStatus(rs.getString("groupstatus"));
				groupListVO.setChatBoxID(rs.getString("chatBoxID"));
				list.add(groupListVO);
			}

			// Handle any driver errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. " + se.getMessage());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return list;
	}

	@Override
	public List<GroupListVO> getAllByMemberID(String memberID) {
		List<GroupListVO> list = new ArrayList<GroupListVO>();
		GroupListVO groupListVO = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ALL_ByMemberID_STMT);

			pstmt.setString(1, memberID);

			rs = pstmt.executeQuery();

			while (rs.next()) {
				groupListVO = new GroupListVO();
				groupListVO.setGroupID(rs.getString("groupID"));
				groupListVO.setMemberID(rs.getString("memberID"));
				groupListVO.setBoardID(rs.getString("boardID"));
				groupListVO.setGroupTitle(rs.getString("groupTitle"));
				groupListVO.setGroupLocation(rs.getString("groupLocation"));
				groupListVO.setLatitude(rs.getDouble("latitude"));
				groupListVO.setLongitude(rs.getDouble("longitude"));
				groupListVO.setGroupStartDate(rs.getTimestamp("groupStartDate"));
				groupListVO.setGroupMeetDate(rs.getTimestamp("groupMeetDate"));
				groupListVO.setGroupContent(rs.getString("groupcontent"));
				groupListVO.setGroupStatus(rs.getString("groupstatus"));
				groupListVO.setChatBoxID(rs.getString("chatBoxID"));
				list.add(groupListVO);
			}

			// Handle any driver errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. " + se.getMessage());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return list;

	}

}
