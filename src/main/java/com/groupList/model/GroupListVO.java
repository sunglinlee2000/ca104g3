package com.groupList.model;

import java.sql.*;

public class GroupListVO implements java.io.Serializable {
	private String groupID;
	private String memberID;
	private String boardID;
	private String groupTitle;
	private String groupLocation;
	private Double latitude;
	private Double longitude;
	private Timestamp groupStartDate;
	private Timestamp groupMeetDate;
	private String groupContent;
	private String groupStatus;
	private String chatBoxID;

	public String getGroupID() {
		return groupID;
	}

	public void setGroupID(String groupID) {
		this.groupID = groupID;
	}

	public String getMemberID() {
		return memberID;
	}

	public void setMemberID(String memberID) {
		this.memberID = memberID;
	}

	public String getBoardID() {
		return boardID;
	}

	public void setBoardID(String boardID) {
		this.boardID = boardID;
	}

	public String getGroupTitle() {
		return groupTitle;
	}

	public void setGroupTitle(String groupTitle) {
		this.groupTitle = groupTitle;
	}

	public String getGroupLocation() {
		return groupLocation;
	}

	public void setGroupLocation(String groupLocation) {
		this.groupLocation = groupLocation;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public Timestamp getGroupStartDate() {
		return groupStartDate;
	}

	public void setGroupStartDate(Timestamp groupStartDate) {
		this.groupStartDate = groupStartDate;
	}

	public Timestamp getGroupMeetDate() {
		return groupMeetDate;
	}

	public void setGroupMeetDate(Timestamp groupMeetDate) {
		this.groupMeetDate = groupMeetDate;
	}

	public String getGroupContent() {
		return groupContent;
	}

	public void setGroupContent(String groupContent) {
		this.groupContent = groupContent;
	}

	public String getGroupStatus() {
		return groupStatus;
	}

	public void setGroupStatus(String groupStatus) {
		this.groupStatus = groupStatus;
	}

	public String getChatBoxID() {
		return chatBoxID;
	}

	public void setChatBoxID(String chatBoxID) {
		this.chatBoxID = chatBoxID;
	}

}
