package com.groupList.model;

import java.sql.Connection;
import java.sql.Timestamp;
import java.util.List;
import java.util.Map;

import com.groupMember.model.GroupMemberVO;

public class GroupListService {
	
	private GroupListDAO_interface dao;
	
	public GroupListService() {
		dao = new GroupListDAO(); 
	}
	
	public GroupListVO addGroup(String memberID, String boardID, String groupTitle, String groupLocation, 
			Double latitude, Double longitude, Timestamp groupMeetDate, String groupContent) {
		
		GroupListVO groupListVO = new GroupListVO();
		
		groupListVO.setMemberID(memberID);
		groupListVO.setBoardID(boardID);
		groupListVO.setGroupTitle(groupTitle);
		groupListVO.setGroupLocation(groupLocation);
		groupListVO.setLatitude(latitude);
		groupListVO.setLongitude(longitude);
//		groupListVO.setGroupStartDate(groupStartDate);
		groupListVO.setGroupMeetDate(groupMeetDate);
		groupListVO.setGroupContent(groupContent);
//		groupListVO.setGroupStatus(groupStatus);
		dao.insert(groupListVO);
		
		return groupListVO;
	}
	
	public GroupListVO updateGroup(String groupID, String groupTitle, String groupLocation, Double latitude, Double longitude, 
			Timestamp groupMeetDate, String groupContent) {
		
		GroupListVO groupListVO = new GroupListVO();
		
		groupListVO.setGroupID(groupID);
//		groupListVO.setMemberID(memberID);
//		groupListVO.setBoardID(boardID);
		groupListVO.setGroupTitle(groupTitle);
		groupListVO.setGroupLocation(groupLocation);
		groupListVO.setLatitude(latitude);
		groupListVO.setLongitude(longitude);
//		groupListVO.setGroupStartDate(groupStartDate);
		groupListVO.setGroupMeetDate(groupMeetDate);
		groupListVO.setGroupContent(groupContent);
//		groupListVO.setGroupStatus(groupStatus);
		dao.update(groupListVO);
		
		return groupListVO;
	}
	
	public void update_noLonger(String groupID, Connection con) {
		
		GroupListVO groupListVO = new GroupListVO();
		groupListVO.setGroupID(groupID);
		dao.update_noLonger(groupListVO, con);
		
	}
	
	public GroupListVO getOneGroup(String groupID) {
		return dao.findByPrimaryKey(groupID);
	}
	
	public List<GroupListVO> getAllByBoardID(String boardID) {
		return dao.getAllByBoardID(boardID);
	}
	
	public List<GroupListVO> getAllByBoardID(Map<String, String[]> map) {
		return dao.getAllByBoardID(map);
	}
	
	public List<GroupListVO> getAllByMemberID(String memberID) {
		return dao.getAllByMemberID(memberID);
	}
	
	

}
