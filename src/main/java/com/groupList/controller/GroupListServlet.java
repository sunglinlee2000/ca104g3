package com.groupList.controller;

import java.io.*;
import java.util.*;
import java.util.Date;
import java.sql.*;

import javax.servlet.*;
import javax.servlet.http.*;

import com.groupList.model.*;
import com.member.model.MemberService;
import com.member.model.MemberVO;
import com.notificationList.model.NotificationListService;

public class GroupListServlet extends HttpServlet {

	public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		doPost(req, res);
	}

	public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {

		req.setCharacterEncoding("UTF-8");
		String action = req.getParameter("action");
		res.setHeader("Cache-Control", "no-store");
		res.setHeader("Pragma", "no-cache");
		res.setDateHeader("Expires", 0);
//		System.out.println(action);

		if ("getOne_For_Display".equals(action)) {

			/*************************** 1.接收請求參數 - 輸入格式的錯誤處理 **********************/
			String groupID = req.getParameter("groupID");

			/*************************** 2.開始查詢資料 *****************************************/
			GroupListService groupListSvc = new GroupListService();
			GroupListVO groupListVO = groupListSvc.getOneGroup(groupID);

			/*************************** 3.查詢完成,準備轉交(Send the Success view) *************/
			req.setAttribute("groupListVO", groupListVO);
			String url = "/front-end/groupList/listOneGroup.jsp";
			RequestDispatcher successView = req.getRequestDispatcher(url);
			successView.forward(req, res);

			/*************************** 其他可能的錯誤處理 *************************************/

		}

		if ("getOne_For_Update".equals(action)) {

			/*************************** 1.接收請求參數 ****************************************/
			String groupID = req.getParameter("groupID");

			/*************************** 2.開始查詢資料 ****************************************/
			GroupListService groupListSvc = new GroupListService();
			GroupListVO groupListVO = groupListSvc.getOneGroup(groupID);

			/*************************** 3.查詢完成,準備轉交(Send the Success view) ************/
			req.setAttribute("groupListVO", groupListVO);
			String url = "/front-end/groupList/updateOneGroup.jsp";
			RequestDispatcher successView = req.getRequestDispatcher(url);
			successView.forward(req, res);

			/*************************** 其他可能的錯誤處理 **********************************/

		}
		
		if ("update".equals(action)) {

			List<String> errorMsgs = new LinkedList<String>();
			req.setAttribute("errorMsgs", errorMsgs);

			try {
//				1.錯誤處理
				String groupID = req.getParameter("groupID");
				
				String groupTitle = req.getParameter("groupTitle");
//				String groupTitleReg = "^[(\u4e00-\u9fa5)(a-zA-Z0-9_)]{1,}$";
//				$在正規表示法代表：結尾

				if (groupTitle == null || groupTitle.trim().length() == 0) {
					errorMsgs.add("揪團名稱：請勿空白");
				} 
//				else if (!groupTitle.trim().matches(groupTitleReg)) { // 以下練習正則(規)表示式(regular-expression)
//					errorMsgs.add("揪團名稱: 只能是中、英文字母、數字和_");
//				}
				
				String groupLocation = req.getParameter("groupLocation");
				if (groupLocation == null || groupLocation.trim().length() == 0) {
					errorMsgs.add("集合地點：請勿空白");
				}
				
				Double latitude = null; 
				Double longitude = null; 
				try {
					latitude = new Double(req.getParameter("latitude").trim());
					longitude = new Double(req.getParameter("longitude").trim());
				} catch (NumberFormatException e) {
					errorMsgs.add("請更改集合地點");
				}
				
//				Timestamp groupDueDate = null;
//				try {
//					groupDueDate = Timestamp.valueOf(req.getParameter("groupDueDate").trim());
////					System.out.println(req.getParameter("groupDueDate").trim());
//				} catch (IllegalArgumentException e) {
//					groupDueDate = new Timestamp(System.currentTimeMillis());
//					errorMsgs.add("請選擇加入期限！");
//				}

				Timestamp groupMeetDate = null;
				try {
					groupMeetDate = Timestamp.valueOf(req.getParameter("groupMeetDate").trim());
				} catch (IllegalArgumentException e) {
					groupMeetDate = new Timestamp(System.currentTimeMillis());
					errorMsgs.add("請選擇集合時間！");
				}

				String groupContent = req.getParameter("groupContent");
				if (groupContent == null || groupContent.trim().length() == 0) {
					errorMsgs.add("揪團內容：請勿空白");
				}

				GroupListVO groupListVO = new GroupListVO();
				groupListVO.setGroupID(groupID);
				groupListVO.setGroupTitle(groupTitle);
				groupListVO.setGroupLocation(groupLocation);
				groupListVO.setLatitude(latitude);
				groupListVO.setLongitude(longitude);
				groupListVO.setGroupMeetDate(groupMeetDate);
				groupListVO.setGroupContent(groupContent);

				if (!errorMsgs.isEmpty()) {
					req.setAttribute("groupListVO", groupListVO);
					RequestDispatcher failureView = req.getRequestDispatcher("/front-end/groupList/updateOneGroup.jsp");
					failureView.forward(req, res);
					return;
				}

				/*************************** 2.開始新增資料 ***************************************/
				GroupListService groupListSvc = new GroupListService();
				groupListVO = groupListSvc.updateGroup(groupID, groupTitle, groupLocation, latitude, longitude, 
						 groupMeetDate, groupContent);

				/*************************** 3.新增完成,準備轉交(Send the Success view) ***********/
				String url = "/front-end/groupList/listAllGroup.jsp";
				RequestDispatcher successView = req.getRequestDispatcher(url);
				successView.forward(req, res);

				/*************************** 其他可能的錯誤處理 **********************************/
			} catch (Exception e) {
				errorMsgs.add(e.getMessage());
				RequestDispatcher failureView = req.getRequestDispatcher("/front-end/groupList/updateOneGroup.jsp");
				failureView.forward(req, res);
			}
		}

		if ("insert".equals(action)) {

			List<String> errorMsgs = new LinkedList<String>();
			req.setAttribute("errorMsgs", errorMsgs);

			try {
//				1.錯誤處理
				String memberID = req.getParameter("memberID");
				String boardID = req.getParameter("boardID");
				String groupTitle = req.getParameter("groupTitle");
//				String groupTitleReg = "^[(\u4e00-\u9fa5)(a-zA-Z0-9_)]{1,}$";
//				$在正規表示法代表：結尾

				if (groupTitle == null || groupTitle.trim().length() == 0) {
					errorMsgs.add("揪團名稱：請勿空白");
				} 
//				else if (!groupTitle.trim().matches(groupTitleReg)) { // 以下練習正則(規)表示式(regular-expression)
//					errorMsgs.add("揪團名稱: 只能是中、英文字母、數字和_");
//				}
				
				String groupLocation = req.getParameter("groupLocation");
				if (groupLocation == null || groupLocation.trim().length() == 0) {
					errorMsgs.add("集合地點：請勿空白");
				}
			
				Double latitude = null; 
				Double longitude = null; 
				try {
					latitude = new Double(req.getParameter("latitude").trim());
					longitude = new Double(req.getParameter("longitude").trim());
				} catch (NumberFormatException e) {
					errorMsgs.add("集合地點：請勿空白");
				}

//				Timestamp groupDueDate = null;
//				try {
//					groupDueDate = Timestamp.valueOf(req.getParameter("groupDueDate").trim());
////					System.out.println(req.getParameter("groupDueDate").trim());
//				} catch (IllegalArgumentException e) {
//					groupDueDate = new Timestamp(System.currentTimeMillis());
//					errorMsgs.add("請選擇加入期限！");
//				}

				Timestamp groupMeetDate = null;
				try {
					groupMeetDate = Timestamp.valueOf(req.getParameter("groupMeetDate").trim());
				} catch (IllegalArgumentException e) {
					groupMeetDate = new Timestamp(System.currentTimeMillis());
					errorMsgs.add("請選擇集合時間！");
				}

				String groupContent = req.getParameter("groupContent");
				if (groupContent == null || groupContent.trim().length() == 0) {
					errorMsgs.add("揪團內容：請勿空白");
				}

				GroupListVO groupListVO = new GroupListVO();
				groupListVO.setMemberID(memberID);
				groupListVO.setBoardID(boardID);
				groupListVO.setGroupTitle(groupTitle);
				groupListVO.setGroupLocation(groupLocation);
				groupListVO.setLatitude(latitude);
				groupListVO.setLongitude(longitude);
				groupListVO.setGroupMeetDate(groupMeetDate);
				groupListVO.setGroupContent(groupContent);

				if (!errorMsgs.isEmpty()) {
					req.setAttribute("groupListVO", groupListVO);
					RequestDispatcher failureView = req.getRequestDispatcher("/front-end/groupList/addGroup.jsp");
					failureView.forward(req, res);
					return;
				}

				/*************************** 2.開始新增資料 ***************************************/
//				新增揪團
				GroupListService groupListSvc = new GroupListService();
				groupListVO = groupListSvc.addGroup(memberID, boardID, groupTitle, groupLocation, latitude, longitude, 
						 groupMeetDate, groupContent);

				/*************************** 3.新增完成,準備轉交(Send the Success view) ***********/
				String url = "/front-end/groupList/listAllGroup.jsp";
				RequestDispatcher successView = req.getRequestDispatcher(url);
				successView.forward(req, res);

				/*************************** 其他可能的錯誤處理 **********************************/
			} catch (Exception e) {
				errorMsgs.add(e.getMessage());
				RequestDispatcher failureView = req.getRequestDispatcher("/front-end/groupList/addGroup.jsp");
				failureView.forward(req, res);
//				e.printStackTrace();
			}
		}
		
		
		if ("group_byQuery".equals(action)) { // 來自select_page.jsp的複合查詢請求
			List<String> errorMsgs = new LinkedList<String>();
			// Store this set in the request scope, in case we need to
			// send the ErrorPage view.
			req.setAttribute("errorMsgs", errorMsgs);

			try {
				
				/***************************1.將輸入資料轉為Map**********************************/ 
				//採用Map<String,String[]> getParameterMap()的方法 
				//注意:an immutable java.util.Map 
				//Map<String, String[]> map = req.getParameterMap();
				HttpSession session = req.getSession();
				Map<String, String[]> map = (Map<String, String[]>)session.getAttribute("map");
				if (req.getParameter("whichPage") == null){
					HashMap<String, String[]> map1 = new HashMap<String, String[]>(req.getParameterMap());
					session.setAttribute("map",map1);
					map = map1;
				} 
				
				/***************************2.開始複合查詢***************************************/
				GroupListService groupListSvc = new GroupListService();
				List<GroupListVO> list  = groupListSvc.getAllByBoardID(map);
				
				/***************************3.查詢完成,準備轉交(Send the Success view)************/
				req.setAttribute("listGroup_ByQuery", list); // 資料庫取出的list物件,存入request
				RequestDispatcher successView = req.getRequestDispatcher("/front-end/groupList/listGroup_ByQuery.jsp"); // 成功轉交listEmps_ByCompositeQuery.jsp
				successView.forward(req, res);
				
				/***************************其他可能的錯誤處理**********************************/
			} catch (Exception e) {
				errorMsgs.add(e.getMessage());
				RequestDispatcher failureView = req.getRequestDispatcher("/front-end/groupList/listAllGroup.jsp");
				failureView.forward(req, res);
			}
		}
		
		if ("invite_friend".equals(action)) {
			Enumeration<String> enu = req.getParameterNames();
			HttpSession session = req.getSession();
			MemberVO memberVO = (MemberVO) session.getAttribute("memberVO");
			String memberID = memberVO.getMemberID();
			String groupID = req.getParameter("groupID");
			String memberNickName = new MemberService().getOneMember(memberID).getMemberNickName();
			String notificationContent = memberNickName + "邀請您參加揪團";
			Timestamp now = new Timestamp(new Date().getTime());
			
			/***************************2.開始新增通知***************************************/			
			while(enu.hasMoreElements()) {
				String name = (String) enu.nextElement();
				if ("memberID".equals(name)) {
					String values[] = req.getParameterValues(name);
					for (int i = 0; i < values.length; i++) {
						NotificationListService notificationListSvc = new NotificationListService();
						notificationListSvc.addNoList("NT000001", values[i], notificationContent, now, groupID, "FALSE", 
								"/groupList/groupList.do?action=getOne_For_Display&groupID=");
					}
				}
			}
			
			/***************************3.查詢完成,準備轉交(Send the Success view)************/
			String url = "/groupList/groupList.do?action=getOne_For_Display";
			RequestDispatcher successView = req.getRequestDispatcher(url); // 成功轉交listEmps_ByCompositeQuery.jsp
			successView.forward(req, res);
			
			
		}

	}

}
