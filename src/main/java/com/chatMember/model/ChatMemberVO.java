package com.chatMember.model;

public class ChatMemberVO implements java.io.Serializable {
	private String chatBoxID;
	private String memberID;

	public String getChatBoxID() {
		return chatBoxID;
	}

	public void setChatBoxID(String chatBoxID) {
		this.chatBoxID = chatBoxID;
	}

	public String getMemberID() {
		return memberID;
	}

	public void setMemberID(String memberID) {
		this.memberID = memberID;
	}

}
