package com.chatMember.model;

import java.util.*;
import java.sql.*;
import javax.naming.*;
import javax.sql.*;

import com.groupList.model.GroupListVO;

public class ChatMemberDAO implements ChatMemberDAO_interface {
	private static DataSource ds = null;
	static {
		try {
			Context ctx = new InitialContext();
			ds = (DataSource) ctx.lookup("java:comp/env/jdbc/CA104G3DB");
		} catch (NamingException e) {
			e.printStackTrace();
		}
	}

	private static final String INSERT_STMT = "INSERT INTO chatMember (chatBoxID, memberID) VALUES (?, ?)";
	private static final String GET_ALL_STMT = "SELECT chatBoxID, memberID FROM chatMember WHERE memberID = ?";
	private static final String GET_ALL_BYCHATBOXID_STMT = "SELECT chatBoxID, memberID FROM chatMember WHERE chatBoxID = ?";
	private static final String GET_ONE_STMT = "SELECT chatBoxID, memberID FROM chatMember WHERE chatBoxID = ? and memberID = ?";
	private static final String DELETE_STMT = "DELETE FROM chatMember WHERE chatBoxID = ? and memberID = ?";

	@Override
	public void insert(ChatMemberVO chatMemberVO) {
		Connection con = null;
		PreparedStatement pstmt = null;

		try {
			con = ds.getConnection();
			pstmt = con.prepareStatement(INSERT_STMT);

			pstmt.setString(1, chatMemberVO.getChatBoxID());
			pstmt.setString(2, chatMemberVO.getMemberID());

			pstmt.executeUpdate();
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. " + se.getMessage());
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}

	@Override
	public void delete(String chatBoxID, String memberID) {
		Connection con = null;
		PreparedStatement pstmt = null;

		try {
			con = ds.getConnection();
			pstmt = con.prepareStatement(DELETE_STMT);

			pstmt.setString(1, chatBoxID);
			pstmt.setString(2, memberID);

			pstmt.executeUpdate();
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. " + se.getMessage());
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}

	@Override
	public ChatMemberVO findByPrimaryKey(String chatBoxID, String memberID) {
		ChatMemberVO chatMemberVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ONE_STMT);

			pstmt.setString(1, chatBoxID);
			pstmt.setString(2, memberID);

			rs = pstmt.executeQuery();

			while (rs.next()) {
				chatMemberVO = new ChatMemberVO();
				chatMemberVO.setChatBoxID(rs.getString("chatBoxID"));
				chatMemberVO.setMemberID(rs.getString("memberID"));
			}

		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. " + se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return chatMemberVO;

	}

	@Override
	public List<ChatMemberVO> getAllByMemberID(String memberID) {
		List<ChatMemberVO> list = new ArrayList<ChatMemberVO>();
		ChatMemberVO chatMemberVO = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ALL_STMT);
			pstmt.setString(1, memberID);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				chatMemberVO = new ChatMemberVO();
				chatMemberVO.setChatBoxID(rs.getString("chatBoxID"));
				chatMemberVO.setMemberID(rs.getString("memberID"));
				list.add(chatMemberVO);
			}

			// Handle any driver errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. " + se.getMessage());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return list;
	}

	@Override
	public void insert_becomeFriending(String next_chatBoxID, String memberID, String friendID, Connection con) {

		PreparedStatement pstmt1 = null;
		PreparedStatement pstmt2 = null;

		try {

			pstmt1 = con.prepareStatement(INSERT_STMT);
			pstmt1.setString(1, next_chatBoxID);
			pstmt1.setString(2, memberID);
			pstmt1.executeUpdate();

			pstmt2 = con.prepareStatement(INSERT_STMT);
			pstmt2.setString(1, next_chatBoxID);
			pstmt2.setString(2, friendID);
			pstmt2.executeUpdate();

		} catch (SQLException se) {
			if (con != null) {
				try {
					// 3●設定於當有exception發生時之catch區塊內
					System.err.print("Transaction is being ");
					System.err.println("rolled back由chatMember");
					con.rollback();
				} catch (SQLException excep) {
					throw new RuntimeException("rollback error occured. " + excep.getMessage());
				}
			}
			throw new RuntimeException("A database error occured. " + se.getMessage());
		} finally {
			if (pstmt1 != null) {
				try {
					pstmt1.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt2 != null) {
				try {
					pstmt2.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
		}

	}

	@Override
	public List<ChatMemberVO> getAllByChatBoxID(String chatBoxID) {
		List<ChatMemberVO> list = new ArrayList<ChatMemberVO>();
		ChatMemberVO chatMemberVO = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ALL_BYCHATBOXID_STMT);
			pstmt.setString(1, chatBoxID);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				chatMemberVO = new ChatMemberVO();
				chatMemberVO.setChatBoxID(rs.getString("chatBoxID"));
				chatMemberVO.setMemberID(rs.getString("memberID"));
				list.add(chatMemberVO);
			}

			// Handle any driver errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. " + se.getMessage());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return list;
	}

	@Override
	public String getAnotherMemberID(String chatBoxID, String memberID) {
		List<ChatMemberVO> list = getAllByChatBoxID(chatBoxID);
		String anotherMemberID = null;

		for (ChatMemberVO aChatMemberVO : list) {
			String aChatMemberID = aChatMemberVO.getMemberID();
			if (!memberID.equals(aChatMemberID)) {
				anotherMemberID = aChatMemberID;
			}
		}

		return anotherMemberID;

	}

	@Override
	public void insert_newGroup(String next_chatBoxID, GroupListVO groupListVO, Connection con) {

		PreparedStatement pstmt = null;

		try {

			pstmt = con.prepareStatement(INSERT_STMT);
			pstmt.setString(1, next_chatBoxID);
			pstmt.setString(2, groupListVO.getMemberID());
			pstmt.executeUpdate();

		} catch (SQLException se) {
			if (con != null) {
				try {
					// 3●設定於當有exception發生時之catch區塊內
					System.err.print("Transaction is being ");
					System.err.println("rolled back由chatMember");
					con.rollback();
				} catch (SQLException excep) {
					throw new RuntimeException("rollback error occured. " + excep.getMessage());
				}
			}
			throw new RuntimeException("A database error occured. " + se.getMessage());
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
		}

	}

	@Override
	public void insert_groupMember(ChatMemberVO chatMemberVO, Connection con) {
		PreparedStatement pstmt = null;

		try {

			pstmt = con.prepareStatement(INSERT_STMT);
			pstmt.setString(1, chatMemberVO.getChatBoxID());
			pstmt.setString(2, chatMemberVO.getMemberID());
			pstmt.executeUpdate();

		} catch (SQLException se) {
			if (con != null) {
				try {
					// 3●設定於當有exception發生時之catch區塊內
					System.err.print("Transaction is being ");
					System.err.println("rolled back由chatMember");
					con.rollback();
				} catch (SQLException excep) {
					throw new RuntimeException("rollback error occured. " + excep.getMessage());
				}
			}
			throw new RuntimeException("A database error occured. " + se.getMessage());
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
		}

	}

}
