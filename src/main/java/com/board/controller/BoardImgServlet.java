package com.board.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.board.model.BoardService;
import com.board.model.BoardVO;

public class BoardImgServlet extends HttpServlet {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		req.setCharacterEncoding("UTF-8");
		String boardID = req.getParameter("boardID");
		String signaturePhotoNo = req.getParameter("signaturePhotoNo");
		BoardService boardSvc = new BoardService();
		BoardVO boardVO = boardSvc.getOneBoard(boardID);
		
		byte[] photo = null;
		ServletOutputStream out = res.getOutputStream();
		if ("1".equals(signaturePhotoNo)) {
			photo = boardVO.getBoardSignaturePhoto1();
		} else if ("2".equals(signaturePhotoNo)) {
			photo = boardVO.getBoardSignaturePhoto2();
		} else if ("3".equals(signaturePhotoNo)) {
			photo = boardVO.getBoardSignaturePhoto3();
		} else if ("4".equals(signaturePhotoNo)){
			photo = boardVO.getBoardSignaturePhoto4();
		} else {
			photo = boardVO.getBoardPhoto();
		}
		
		res.setContentType("image/jpeg");
		res.setContentLength(photo.length);
		out.write(photo);
		out.flush();
		out.close();
		
		
	}

}
