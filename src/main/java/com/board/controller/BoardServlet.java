package com.board.controller;

import java.io.*;
import java.util.*;

import javax.servlet.*;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.*;

import com.board.model.*;

@MultipartConfig(fileSizeThreshold=1024*1024, maxFileSize=10*1024*1024, maxRequestSize=5*10*1024*1024)
public class BoardServlet extends HttpServlet{
	private static final long serialVersionUID = 1L;
	String saveDirectory = "/images_uploaded";

	public void doGet(HttpServletRequest req,HttpServletResponse res) 
			throws ServletException,IOException{
		doPost(req,res);
	}

	public void doPost(HttpServletRequest req,HttpServletResponse res) 
			throws ServletException,IOException {
		
		req.setCharacterEncoding("UTF-8");
		res.setContentType("text/html; charset=UTF-8");
		String action = req.getParameter("action");
		
		if("getOne_For_Display".equals(action)) {
			
			List<String> errorMsgs = new LinkedList<String>();
			req.setAttribute("errorMsgs", errorMsgs);
			
			try {
				/***************************1.接收請求參數 - 輸入格式的錯誤處理**********************/
				String str = req.getParameter("boardID");
				if (str == null || (str.trim()).length() == 0) {
					errorMsgs.add("請輸入專版編號");
				}
				// Send the use back to the form, if there were errors
				if (!errorMsgs.isEmpty()) {
					RequestDispatcher failureView = req
							.getRequestDispatcher("/front-end/board/select_page.jsp");
					failureView.forward(req, res);
					return;//程式中斷
				}
				
				String boardID = null;
				try {
					boardID = new String(str);
				} catch (Exception e) {
					errorMsgs.add("專版編號格式不正確");
				}
				// Send the use back to the form, if there were errors
				if (!errorMsgs.isEmpty()) {
					RequestDispatcher failureView = req
							.getRequestDispatcher("/front-end/board/select_page.jsp");
					failureView.forward(req, res);
					return;//程式中斷
				}
				
				/***************************2.開始查詢資料*****************************************/
				BoardService boardSvc = new BoardService();
				BoardVO boardVO = boardSvc.getOneBoard(boardID);
				if (boardVO == null) {
					errorMsgs.add("查無資料");
				}
				// Send the use back to the form, if there were errors
				if (!errorMsgs.isEmpty()) {
					RequestDispatcher failureView = req
							.getRequestDispatcher("/front-end/board/select_page.jsp");
					failureView.forward(req, res);
					return;//程式中斷
				}
				
				/***************************3.查詢完成,準備轉交(Send the Success view)*************/
//				req.setAttribute("boardVO", boardVO); // 資料庫取出的empVO物件,存入req
				HttpSession session = req.getSession();
				session.setAttribute("boardVO", boardVO); 
				
				String url = "/front-end/board/listOneBoard.jsp";
				RequestDispatcher successView = req.getRequestDispatcher(url); // 成功轉交 listOneEmp.jsp
				successView.forward(req, res);

				/***************************其他可能的錯誤處理*************************************/
			} catch (Exception e) {
				errorMsgs.add("無法取得資料:" + e.getMessage());
				RequestDispatcher failureView = req
						.getRequestDispatcher("/front-end/board/select_page.jsp");
				failureView.forward(req, res);
			}
		}
		
		
		if ("getOne_For_Update".equals(action)) { // 來自listAllEmp.jsp的請求

			List<String> errorMsgs = new LinkedList<String>();
			// Store this set in the request scope, in case we need to
			// send the ErrorPage view.
			req.setAttribute("errorMsgs", errorMsgs);
			
			try {
				/***************************1.接收請求參數****************************************/
				String boardID = new String(req.getParameter("boardID"));
				
				/***************************2.開始查詢資料****************************************/
				BoardService boardSvc = new BoardService();
				BoardVO boardVO = boardSvc.getOneBoard(boardID);
								
				/***************************3.查詢完成,準備轉交(Send the Success view)************/
				req.setAttribute("boardVO", boardVO);         // 資料庫取出的empVO物件,存入req
				String url = "/front-end/board/update_board_input.jsp";
				RequestDispatcher successView = req.getRequestDispatcher(url);// 成功轉交 update_emp_input.jsp
				successView.forward(req, res);

				/***************************其他可能的錯誤處理**********************************/
			} catch (Exception e) {
				errorMsgs.add("無法取得要修改的資料:" + e.getMessage());
				RequestDispatcher failureView = req
						.getRequestDispatcher("/front-end/board/listAllBoard.jsp");
				failureView.forward(req, res);
			}
		}
		
		
		if ("update".equals(action)) {
			
			List<String> errorMsgs = new LinkedList<String>();
			// Store this set in the request scope, in case we need to
			// send the ErrorPage view.
			req.setAttribute("errorMsgs", errorMsgs);
		
			try {
				/***************************1.接收請求參數 - 輸入格式的錯誤處理**********************/
				String boardID=req.getParameter("boardID");
								
				String memberID = req.getParameter("memberID");				
				String memberIDReg = "^[(M0-9_)]{7,7}$";
				if (memberID == null || memberID.trim().length() == 0) {
					errorMsgs.add("會員編號: 請勿空白");
				} else if(!memberID.trim().matches(memberIDReg)) { //以下練習正則(規)表示式(regular-expression)
					errorMsgs.add("會員編號: 只能是M開頭，總長度7位");
	            }
				
				String administratorID = req.getParameter("administratorID");
				String administratorIDReg = "^[(A0-9_)]{7,7}$";
				if (administratorID == null || administratorID.trim().length() == 0) {
					errorMsgs.add("管理者編號: 請勿空白");
				} else if(!administratorID.trim().matches(administratorIDReg)) { //以下練習正則(規)表示式(regular-expression)
					errorMsgs.add("管理者編號: 只能是A開頭，總長度7位");
	            }
				
//				java.sql.Timestamp boardDate = new java.sql.Timestamp(System.currentTimeMillis());
				
				java.sql.Timestamp boardSignatureDate = null;
				try {
					boardSignatureDate = java.sql.Timestamp.valueOf(req.getParameter("boardSignatureDate").trim());
				} catch (IllegalArgumentException e) {
					boardSignatureDate=new java.sql.Timestamp(System.currentTimeMillis());
					errorMsgs.add("請輸入日期!");
				}
				
				java.sql.Timestamp boardSignatureEndDate = null;
				try {
					boardSignatureEndDate = java.sql.Timestamp.valueOf(req.getParameter("boardSignatureEndDate").trim());
				} catch (IllegalArgumentException e) {
					boardSignatureEndDate=new java.sql.Timestamp(System.currentTimeMillis());
					errorMsgs.add("請輸入日期!");
				}
				
				Integer boardSignatureTarget = null;
				try {
					boardSignatureTarget = new Integer(req.getParameter("boardSignatureTarget").trim());
				} catch (NumberFormatException e) {
					boardSignatureTarget = 100;
					errorMsgs.add("目標請填數字.");
				}
				
				
				String boardSignatureTitle = req.getParameter("boardSignatureTitle");
				String boardSignatureTitleReg = "^[(\u4e00-\u9fa5)(a-zA-Z0-9_)]{2,10}$";
				if (boardSignatureTitle == null || boardSignatureTitle.trim().length() == 0) {
					errorMsgs.add("專版名稱: 請勿空白");
				} else if(!boardSignatureTitle.trim().matches(boardSignatureTitleReg)) { //以下練習正則(規)表示式(regular-expression)
					errorMsgs.add("專版名稱: 只能是中、英文字母、數字和_ , 且長度必需在2到10之間");
	            }
				Part part = req.getPart("boardPhoto");
				InputStream is = part.getInputStream();
				byte[] boardPhoto = new byte[is.available()];
				is.read(boardPhoto);
				is.close();
				
				Part part1 = req.getPart("boardSignaturePhoto1");
				InputStream is1 = part1.getInputStream();
				byte[] boardSignaturePhoto1 = new byte[is1.available()];
				is1.read(boardSignaturePhoto1);
				is1.close();
				
				Part part2 = req.getPart("boardSignaturePhoto2");
				InputStream is2 = part2.getInputStream();
				byte[] boardSignaturePhoto2 = new byte[is2.available()];
				is2.read(boardSignaturePhoto2);
				is2.close();
				
				Part part3 = req.getPart("boardSignaturePhoto3");
				InputStream is3 = part3.getInputStream();
				byte[] boardSignaturePhoto3 = new byte[is3.available()];
				is3.read(boardSignaturePhoto3);
				is3.close();
				
				Part part4 = req.getPart("boardSignaturePhoto4");
				InputStream is4 = part4.getInputStream();
				byte[] boardSignaturePhoto4 = new byte[is4.available()];
				is4.read(boardSignaturePhoto4);
				is4.close();
				
				String boardSignatureInfo = req.getParameter("boardSignatureInfo");
				if (boardSignatureInfo == null || boardSignatureInfo.trim().length() == 0) {
					errorMsgs.add("內容請勿空白");
				}
				
				String boardStatus = req.getParameter("boardStatus");
				if(boardStatus == null || boardStatus.trim().length()== 0) {
					errorMsgs.add("內容請勿空白");
				}


				BoardVO boardVO = new BoardVO();
				boardVO.setBoardID(boardID);
				boardVO.setMemberID(memberID);
				boardVO.setAdministratorID(administratorID);
				boardVO.setBoardSignatureDate(boardSignatureDate);
				boardVO.setBoardSignatureEndDate(boardSignatureEndDate);
				boardVO.setBoardSignatureTarget(boardSignatureTarget);
				boardVO.setBoardSignatureTitle(boardSignatureTitle);
				boardVO.setBoardPhoto(boardPhoto);
				boardVO.setBoardSignaturePhoto1(boardSignaturePhoto1);
				boardVO.setBoardSignaturePhoto2(boardSignaturePhoto2);
				boardVO.setBoardSignaturePhoto3(boardSignaturePhoto3);
				boardVO.setBoardSignaturePhoto4(boardSignaturePhoto4);
				boardVO.setBoardSignatureInfo(boardSignatureInfo);
				boardVO.setBoardStatus(boardStatus);



				// Send the use back to the form, if there were errors
				if (!errorMsgs.isEmpty()) {
					System.out.println("1");
					req.setAttribute("boardVO", boardVO); // 含有輸入格式錯誤的empVO物件,也存入req
					RequestDispatcher failureView = req
							.getRequestDispatcher("/front-end/board/update_board_input.jsp");
					failureView.forward(req, res);
					return; //程式中斷
				}
				
				/***************************2.開始修改資料*****************************************/
				BoardService boardSvc = new BoardService();
				boardVO = boardSvc.updateBoard(boardID,memberID, administratorID, boardSignatureDate, boardSignatureEndDate, boardSignatureTarget,  boardSignatureTitle, boardPhoto, boardSignaturePhoto1, boardSignaturePhoto2, boardSignaturePhoto3, boardSignaturePhoto4, boardSignatureInfo, boardStatus);
				System.out.println("2");
				/***************************3.修改完成,準備轉交(Send the Success view)*************/
				req.setAttribute("boardVO", boardVO); // 資料庫update成功後,正確的的empVO物件,存入req
				String url = "/front-end/board/listOneBoard.jsp";
				RequestDispatcher successView = req.getRequestDispatcher(url); // 修改成功後,轉交listOneEmp.jsp
				successView.forward(req, res);
				System.out.println("3");
				/***************************其他可能的錯誤處理*************************************/
			} catch (Exception e) {
				errorMsgs.add("修改資料失敗:"+e.getMessage());
				System.out.println("4");
				RequestDispatcher failureView = req
						.getRequestDispatcher("/front-end/board/update_board_input.jsp");
				failureView.forward(req, res);
			}
		}

        if ("insert".equals(action)) { 
			
			List<String> errorMsgs = new LinkedList<String>();
			// Store this set in the request scope, in case we need to
			// send the ErrorPage view.
			req.setAttribute("errorMsgs", errorMsgs);

			try {
				/***********************1.接收請求參數 - 輸入格式的錯誤處理*************************/
				String boardSignatureTitle = req.getParameter("boardSignatureTitle");
				String boardSignatureTitleReg = "^[(\u4e00-\u9fa5)(a-zA-Z0-9_)]{2,10}$";
				if (boardSignatureTitle == null || boardSignatureTitle.trim().length() == 0) {
					errorMsgs.add("專版名稱: 請勿空白");
				} else if(!boardSignatureTitle.trim().matches(boardSignatureTitleReg)) { //以下練習正則(規)表示式(regular-expression)
					errorMsgs.add("專版名稱: 只能是中、英文字母、數字和_ , 且長度必需在2到10之間");
	            }
				
				String memberID = req.getParameter("memberID");
				String memberIDReg = "^[(M0-9_)]{7,7}$";
				if (memberID == null || memberID.trim().length() == 0) {
					errorMsgs.add("會員編號: 請勿空白");
				} else if(!memberID.trim().matches(memberIDReg)) { //以下練習正則(規)表示式(regular-expression)
					errorMsgs.add("會員編號: 只能是M開頭，總長度7位");
	            }
				
				
				java.sql.Timestamp boardSignatureEndDate = null;
				try {
					boardSignatureEndDate = java.sql.Timestamp.valueOf(req.getParameter("boardSignatureEndDate").trim());
				} catch (IllegalArgumentException e) {
					boardSignatureEndDate=new java.sql.Timestamp(System.currentTimeMillis());
					errorMsgs.add("請輸入日期!");
				}
				
				Integer boardSignatureTarget = null;
				try {
					boardSignatureTarget = new Integer(req.getParameter("boardSignatureTarget").trim());
				} catch (NumberFormatException e) {
					boardSignatureTarget = 100;
					errorMsgs.add("目標請填數字.");
				}
				
				
				Part part = req.getPart("boardPhoto");
				InputStream is = part.getInputStream();
				byte[] boardPhoto = new byte[is.available()];
				is.read(boardPhoto);
				is.close();
				
				Part part1 = req.getPart("boardSignaturePhoto1");
				InputStream is1 = part1.getInputStream();
				byte[] boardSignaturePhoto1 = new byte[is1.available()];
				is1.read(boardSignaturePhoto1);
				is1.close();
				
				Part part2 = req.getPart("boardSignaturePhoto2");
				InputStream is2 = part2.getInputStream();
				byte[] boardSignaturePhoto2 = new byte[is2.available()];
				is2.read(boardSignaturePhoto2);
				is2.close();
				
				Part part3 = req.getPart("boardSignaturePhoto3");
				InputStream is3 = part3.getInputStream();
				byte[] boardSignaturePhoto3 = new byte[is3.available()];
				is3.read(boardSignaturePhoto3);
				is3.close();
				
				Part part4 = req.getPart("boardSignaturePhoto4");
				InputStream is4 = part4.getInputStream();
				byte[] boardSignaturePhoto4 = new byte[is4.available()];
				is4.read(boardSignaturePhoto4);
				is4.close();
				
				String boardSignatureInfo = req.getParameter("boardSignatureInfo");
				if (boardSignatureInfo == null || boardSignatureInfo.trim().length() == 0) {
					errorMsgs.add("內容請勿空白");
				}
				

				BoardVO boardVO = new BoardVO();
				boardVO.setMemberID(memberID);
				boardVO.setBoardSignatureEndDate(boardSignatureEndDate);
				boardVO.setBoardSignatureTarget(boardSignatureTarget);
				boardVO.setBoardSignatureTitle(boardSignatureTitle);
				boardVO.setBoardPhoto(boardPhoto);
				boardVO.setBoardSignaturePhoto1(boardSignaturePhoto1);
				boardVO.setBoardSignaturePhoto2(boardSignaturePhoto2);
				boardVO.setBoardSignaturePhoto3(boardSignaturePhoto3);
				boardVO.setBoardSignaturePhoto4(boardSignaturePhoto4);
				boardVO.setBoardSignatureInfo(boardSignatureInfo);
				
				// Send the use back to the form, if there were errors
				if (!errorMsgs.isEmpty()) {
					req.setAttribute("boardVO", boardVO); // 含有輸入格式錯誤的empVO物件,也存入req
					RequestDispatcher failureView = req
							.getRequestDispatcher("/front-end/board/addBoard.jsp");
					failureView.forward(req, res);
					return;
				}
				
				/***************************2.開始新增資料***************************************/
				BoardService boardSvc = new BoardService();
				boardVO = boardSvc.addBoard(memberID,  boardSignatureEndDate, boardSignatureTarget, boardSignatureTitle, boardPhoto, boardSignaturePhoto1, boardSignaturePhoto2, boardSignaturePhoto3, boardSignaturePhoto4, boardSignatureInfo);
				/***************************3.新增完成,準備轉交(Send the Success view)***********/
				String url = "/front-end/board/listAllBoard.jsp";
				RequestDispatcher successView = req.getRequestDispatcher(url); // 新增成功後轉交listAllEmp.jsp
				successView.forward(req, res);				
				/***************************其他可能的錯誤處理**********************************/
			} catch (Exception e) {
				e.printStackTrace(System.err);
				errorMsgs.add(e.getMessage());
				RequestDispatcher failureView = req
						.getRequestDispatcher("/front-end/board/addBoard.jsp");
				failureView.forward(req, res);
			}
		}
		
		
		if ("delete".equals(action)) { 

			List<String> errorMsgs = new LinkedList<String>();
			// Store this set in the request scope, in case we need to
			// send the ErrorPage view.
			req.setAttribute("errorMsgs", errorMsgs);
	
			try {
				/***************************1.接收請求參數***************************************/
				String boardID = new String(req.getParameter("boardID"));
				
				/***************************2.開始刪除資料***************************************/
				BoardService boardSvc = new BoardService();
				boardSvc.deleteBoard(boardID);
				
				/***************************3.刪除完成,準備轉交(Send the Success view)***********/								
				String url = "/front-end/board/listAllBoard.jsp";
				RequestDispatcher successView = req.getRequestDispatcher(url);// 刪除成功後,轉交回送出刪除的來源網頁
				successView.forward(req, res);
				
				/***************************其他可能的錯誤處理**********************************/
			} catch (Exception e) {
				errorMsgs.add("刪除資料失敗:"+e.getMessage());
				RequestDispatcher failureView = req
						.getRequestDispatcher("/front-end/board/listAllBoard.jsp");
				failureView.forward(req, res);
			}
		}
		
		if ("update_Status".equals(action)) { 

			List<String> errorMsgs = new LinkedList<String>();
			req.setAttribute("errorMsgs", errorMsgs);
	
			try {
				/***************************1.接收請求參數***************************************/
				String boardID = new String(req.getParameter("boardID"));
				
				/***************************2.開始更新資料***************************************/
				BoardService boardSvc = new BoardService();
				boardSvc.updateStatus(boardID);
				
				/***************************3.更新完成,準備轉交(Send the Success view)***********/								
				String url = "/back-end/boardSignature/boardSignature.jsp";
				RequestDispatcher successView = req.getRequestDispatcher(url);// 刪除成功後,轉交回送出刪除的來源網頁
				successView.forward(req, res);
				
				/***************************其他可能的錯誤處理**********************************/
			} catch (Exception e) {
				errorMsgs.add("更新資料失敗:"+e.getMessage());
				RequestDispatcher failureView = req
						.getRequestDispatcher("/back-end/boardSignature/boardSignature.jsp");
				failureView.forward(req, res);
			}
		}
		
		
		
	}
	public String getFileNameFromPart(Part part) {
		String header = part.getHeader("content-disposition");
		System.out.println("header=" + header); // 測試用
		String filename = new File(header.substring(header.lastIndexOf("=") + 2, header.length() - 1)).getName();
		System.out.println("filename=" + filename); // 測試用
		if (filename.length() == 0) {
			return null;
		}
		return filename;
	}
}
