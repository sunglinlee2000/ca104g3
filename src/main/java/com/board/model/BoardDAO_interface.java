package com.board.model;
import java.util.List;

public interface BoardDAO_interface {
	public void insert(BoardVO boardVO);
	public void update(BoardVO boardVO);
	public void updateStatus(String boardID);
	public void delete(String boardID);
	public BoardVO findByPrimaryKey(String boardID);
	public List<BoardVO> getAllOperating();
	public List<BoardVO> getAllApplying();
}
