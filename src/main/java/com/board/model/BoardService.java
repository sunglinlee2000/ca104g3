package com.board.model;

import java.sql.Timestamp;
import java.util.List;

public class BoardService {
	
	private BoardDAO_interface dao;

	public BoardService() {
		dao = new BoardDAO();
	}
	
	public BoardVO addBoard(String memberID,Timestamp boardSignatureEndDate,Integer boardSignatureTarget,
			String boardSignatureTitle,byte[] boardPhoto,byte[] boardSignaturePhoto1,byte[] boardSignaturePhoto2,
			byte[] boardSignaturePhoto3,byte[] boardSignaturePhoto4,String boardSignatureInfo) {
		BoardVO boardVO = new BoardVO();
		
		boardVO.setMemberID(memberID);
		boardVO.setBoardSignatureEndDate(boardSignatureEndDate);
		boardVO.setBoardSignatureTarget(boardSignatureTarget);
		boardVO.setBoardSignatureTitle(boardSignatureTitle);
		boardVO.setBoardPhoto(boardPhoto);
		boardVO.setBoardSignaturePhoto1(boardSignaturePhoto1);
		boardVO.setBoardSignaturePhoto2(boardSignaturePhoto2);
		boardVO.setBoardSignaturePhoto3(boardSignaturePhoto3);
		boardVO.setBoardSignaturePhoto4(boardSignaturePhoto4);
		boardVO.setBoardSignatureInfo(boardSignatureInfo);
		dao.insert(boardVO);
		
		return boardVO;
	}
	
	public BoardVO updateBoard(String boardID,String memberID,String administratorID,Timestamp boardSignatureDate,Timestamp boardSignatureEndDate,Integer boardSignatureTarget,
			String boardSignatureTitle,byte[] boardPhoto,byte[] boardSignaturePhoto1,byte[] boardSignaturePhoto2,
			byte[] boardSignaturePhoto3,byte[] boardSignaturePhoto4,String boardSignatureInfo,String boardStatus) {
		BoardVO boardVO = new BoardVO();
		
		boardVO.setBoardID(boardID);
		boardVO.setMemberID(memberID);
		boardVO.setAdministratorID(administratorID);
		boardVO.setBoardSignatureDate(boardSignatureDate);
		boardVO.setBoardSignatureEndDate(boardSignatureEndDate);
		boardVO.setBoardSignatureTarget(boardSignatureTarget);
		boardVO.setBoardSignatureTitle(boardSignatureTitle);
		boardVO.setBoardPhoto(boardPhoto);
		boardVO.setBoardSignaturePhoto1(boardSignaturePhoto1);
		boardVO.setBoardSignaturePhoto2(boardSignaturePhoto2);
		boardVO.setBoardSignaturePhoto3(boardSignaturePhoto3);
		boardVO.setBoardSignaturePhoto4(boardSignaturePhoto4);
		boardVO.setBoardSignatureInfo(boardSignatureInfo);
		boardVO.setBoardStatus(boardStatus);

		dao.update(boardVO);
		
		return boardVO;
	}
	
	public void updateStatus(String boardID) {
		dao.updateStatus(boardID);
	}
	
	public void deleteBoard(String boardID) {
		dao.delete(boardID);
	}
	
	public BoardVO getOneBoard(String boardID) {
		return dao.findByPrimaryKey(boardID);
	}
	
	public List<BoardVO> getAllOperating() {
		return dao.getAllOperating();
	}
	
	public List<BoardVO> getAllApplying() {
		return dao.getAllApplying();
	}
		
}
