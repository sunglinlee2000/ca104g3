package com.board.model;
import java.sql.Timestamp;

public class BoardVO implements java.io.Serializable{
	private String boardID;
	private String memberID;
	private String administratorID;
	private Timestamp boardDate;
	private Timestamp boardSignatureDate;
	private Timestamp boardSignatureEndDate;
	private Integer boardSignatureTarget;
	private String boardSignatureTitle;
	private byte[] boardPhoto;
	private byte[] boardSignaturePhoto1;
	private byte[] boardSignaturePhoto2;
	private byte[] boardSignaturePhoto3;
	private byte[] boardSignaturePhoto4;
	private String boardSignatureInfo;
	private String boardStatus;
	private Integer count;
	
	public String getBoardID() {
		return boardID;
	}
	public void setBoardID(String boardID) {
		this.boardID = boardID;
	}
	public String getMemberID() {
		return memberID;
	}
	public void setMemberID(String memberID) {
		this.memberID = memberID;
	}
	public String getAdministratorID() {
		return administratorID;
	}
	public void setAdministratorID(String administratorID) {
		this.administratorID = administratorID;
	}
	public Timestamp getBoardDate() {
		return boardDate;
	}
	public void setBoardDate(Timestamp boardDate) {
		this.boardDate = boardDate;
	}
	public Timestamp getBoardSignatureDate() {
		return boardSignatureDate;
	}
	public void setBoardSignatureDate(Timestamp boardSignatureDate) {
		this.boardSignatureDate = boardSignatureDate;
	}
	public Timestamp getBoardSignatureEndDate() {
		return boardSignatureEndDate;
	}
	public void setBoardSignatureEndDate(Timestamp boardSignatureEndDate) {
		this.boardSignatureEndDate = boardSignatureEndDate;
	}
	public Integer getBoardSignatureTarget() {
		return boardSignatureTarget;
	}
	public void setBoardSignatureTarget(Integer boardSignatureTarget) {
		this.boardSignatureTarget = boardSignatureTarget;
	}
	public String getBoardSignatureTitle() {
		return boardSignatureTitle;
	}
	public void setBoardSignatureTitle(String boardSignatureTitle) {
		this.boardSignatureTitle = boardSignatureTitle;
	}	
	public byte[] getBoardPhoto() {
		return boardPhoto;
	}
	public void setBoardPhoto(byte[] boardPhoto) {
		this.boardPhoto = boardPhoto;
	}
	public byte[] getBoardSignaturePhoto1() {
		return boardSignaturePhoto1;
	}
	public void setBoardSignaturePhoto1(byte[] boardSignaturePhoto1) {
		this.boardSignaturePhoto1 = boardSignaturePhoto1;
	}
	public byte[] getBoardSignaturePhoto2() {
		return boardSignaturePhoto2;
	}
	public void setBoardSignaturePhoto2(byte[] boardSignaturePhoto2) {
		this.boardSignaturePhoto2 = boardSignaturePhoto2;
	}
	public byte[] getBoardSignaturePhoto3() {
		return boardSignaturePhoto3;
	}
	public void setBoardSignaturePhoto3(byte[] boardSignaturePhoto3) {
		this.boardSignaturePhoto3 = boardSignaturePhoto3;
	}
	public byte[] getBoardSignaturePhoto4() {
		return boardSignaturePhoto4;
	}
	public void setBoardSignaturePhoto4(byte[] boardSignaturePhoto4) {
		this.boardSignaturePhoto4 = boardSignaturePhoto4;
	}
	public String getBoardSignatureInfo() {
		return boardSignatureInfo;
	}
	public void setBoardSignatureInfo(String boardSignatureInfo) {
		this.boardSignatureInfo = boardSignatureInfo;
	}
	public String getBoardStatus() {
		return boardStatus;
	}
	public void setBoardStatus(String boardStatus) {
		this.boardStatus = boardStatus;
	}
	public Integer getCount() {
		return count;
	}
	public void setCount(Integer count) {
		this.count = count;
	}
	

				
}
