package com.board.model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class BoardJDBCDAO implements BoardDAO_interface{
	String driver = "oracle.jdbc.driver.OracleDriver";
	String url = "jdbc:oracle:thin:@localhost:1521:XE";
	String userid = "CA104G3";
	String passwd = "123456";
	
	private static final String INSERT_STMT = "INSERT INTO board VALUES('B'||LPAD(TO_CHAR(BOARD_SEQ.NEXTVAL),6,'0'),?,?,(CURRENT_TIMESTAMP),?,?,\r\n" + 
			"?,?,?,?,?,?,?,?,?,?)";
	private static final String UPDATE = "UPDATE board SET boardPhoto=?,boardSignaturePhoto1=?, boardSignaturePhoto2=?, boardSignaturePhoto3=?,"
			+ "boardSignaturePhoto4=?  WHERE boardid=?";
//	private static final String UPDATE = "UPDATE board SET memberid=?, administratorid=?, boarddate=?, boardSignaturedate=?, boardSignatureenddate=?, "
//			+ "boardSignaturetarget=?, boardSignaturecount=?, boardSignatureTitle=?, boardPhoto=?, boardSignaturePhoto1=?, boardSignaturePhoto2=?, boardSignaturePhoto3=?,"
//			+ "boardSignaturePhoto4=?, boardSignatureInfo=?, boardStatus=? WHERE boardid=?";
	private static final String DELETE = "DELETE FROM board WHERE boardid=?";
	private static final String GET_ONE_STMT = "SELECT * FROM board WHERE boardid = ?";
	private static final String GET_ALL_STMT = "SELECT * FROM board ORDER BY boardid";
	
	

	@Override
	public void insert(BoardVO boardVO) {
		
		Connection con = null;
		PreparedStatement pstmt = null;
		
		try {
			
			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(INSERT_STMT);
			
			pstmt.setString(1, boardVO.getMemberID());
			pstmt.setString(2, boardVO.getAdministratorID());
			pstmt.setTimestamp(3, boardVO.getBoardSignatureDate());
			pstmt.setTimestamp(4, boardVO.getBoardSignatureEndDate());
			pstmt.setInt(5, boardVO.getBoardSignatureTarget());
			pstmt.setString(6, boardVO.getBoardSignatureTitle());
			pstmt.setBytes(7, boardVO.getBoardPhoto());
			pstmt.setBytes(8, boardVO.getBoardSignaturePhoto1());
			pstmt.setBytes(9, boardVO.getBoardSignaturePhoto2());
			pstmt.setBytes(10, boardVO.getBoardSignaturePhoto3());
			pstmt.setBytes(11, boardVO.getBoardSignaturePhoto4());
			pstmt.setString(12, boardVO.getBoardSignatureInfo());
			pstmt.setString(13, boardVO.getBoardStatus());
			
			pstmt.executeUpdate();			
			
			
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver." + e.getMessage());
		
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured." + se.getMessage());
		
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if(con != null) {
				try {
					con.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
		}
	}

	@Override
	public void update(BoardVO boardVO) {
		
		Connection con = null;
		PreparedStatement pstmt = null;
		
		try {
			
			Class.forName(driver);
			con =DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(UPDATE);

			pstmt.setBytes(1, boardVO.getBoardPhoto());
			pstmt.setBytes(2, boardVO.getBoardSignaturePhoto1());
			pstmt.setBytes(3, boardVO.getBoardSignaturePhoto2());
			pstmt.setBytes(4, boardVO.getBoardSignaturePhoto3());
			pstmt.setBytes(5, boardVO.getBoardSignaturePhoto4());
			pstmt.setString(6, boardVO.getBoardID());
			
			
//			pstmt.setString(1, boardVO.getMemberID());
//			pstmt.setString(2, boardVO.getAdministratorID());
//			pstmt.setTimestamp(3, boardVO.getBoardDate());
//			pstmt.setTimestamp(4, boardVO.getBoardSignatureDate());
//			pstmt.setTimestamp(5, boardVO.getBoardSignatureEndDate());
//			pstmt.setInt(6, boardVO.getBoardSignatureTarget());
//			pstmt.setInt(7, boardVO.getBoardSignatureCount());
//			pstmt.setString(8, boardVO.getBoardSignatureTitle());
//			pstmt.setBytes(9, boardVO.getBoardPhoto());
//			pstmt.setBytes(10, boardVO.getBoardSignaturePhoto1());
//			pstmt.setBytes(11, boardVO.getBoardSignaturePhoto2());
//			pstmt.setBytes(12, boardVO.getBoardSignaturePhoto3());
//			pstmt.setBytes(13, boardVO.getBoardSignaturePhoto4());
//			pstmt.setString(14, boardVO.getBoardSignatureInfo());
//			pstmt.setString(15, boardVO.getBoardStatus());
//			pstmt.setString(16, boardVO.getBoardID());
			
			pstmt.executeUpdate();	
			
			
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver." + e.getMessage());
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured." + se.getMessage());
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
	}

	@Override
	public void delete(String boardID) {
		
		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(DELETE);

			pstmt.setString(1, boardID);

			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}

	@Override
	public BoardVO findByPrimaryKey(String boardID) {
		
		BoardVO boardVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		try {
			
			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(GET_ONE_STMT);
			
			pstmt.setString(1, boardID);
			
			rs = pstmt.executeQuery();
			
			while (rs.next()) {
				boardVO = new BoardVO();
				boardVO.setBoardID(rs.getString("boardID"));
				boardVO.setMemberID(rs.getString("memberID"));
				boardVO.setAdministratorID(rs.getString("administratorID"));
				boardVO.setBoardDate(rs.getTimestamp("boardDate"));
				boardVO.setBoardSignatureDate(rs.getTimestamp("boardSignatureDate"));
				boardVO.setBoardSignatureEndDate(rs.getTimestamp("boardSignatureEndDate"));
				boardVO.setBoardSignatureTarget(rs.getInt("boardSignatureTarget"));
				boardVO.setBoardSignatureTitle(rs.getString("boardSignatureTitle"));
				boardVO.setBoardPhoto(rs.getBytes("boardPhoto"));
				boardVO.setBoardSignaturePhoto1(rs.getBytes("boardSignaturePhoto1"));
				boardVO.setBoardSignaturePhoto2(rs.getBytes("boardSignaturePhoto2"));
				boardVO.setBoardSignaturePhoto3(rs.getBytes("boardSignaturePhoto3"));
				boardVO.setBoardSignaturePhoto4(rs.getBytes("boardSignaturePhoto4"));
				boardVO.setBoardSignatureInfo(rs.getString("boardSignatureInfo"));
				boardVO.setBoardStatus(rs.getString("boardStatus"));
				
			}
			
			
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver."+ e.getMessage());
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured." + se.getMessage());
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			} 
			if (con != null) {
				try {
					con.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
		}
		return boardVO;
	}

	@Override
	public List<BoardVO> getAllOperating() {
		List<BoardVO> list = new ArrayList<BoardVO>();
		BoardVO boardVO = null;
		
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		try {
			
			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(GET_ALL_STMT);
			rs = pstmt.executeQuery();
			
			while (rs.next()) {
				boardVO = new BoardVO();
				boardVO.setBoardID(rs.getString("boardID"));
				boardVO.setMemberID(rs.getString("memberID"));
				boardVO.setAdministratorID(rs.getString("administratorID"));
				boardVO.setBoardDate(rs.getTimestamp("boardDate"));
				boardVO.setBoardSignatureDate(rs.getTimestamp("boardSignatureDate"));
				boardVO.setBoardSignatureEndDate(rs.getTimestamp("boardSignatureEndDate"));
				boardVO.setBoardSignatureTarget(rs.getInt("boardSignatureTarget"));
				boardVO.setBoardSignatureTitle(rs.getString("boardSignatureTitle"));
				boardVO.setBoardPhoto(rs.getBytes("boardPhoto"));
				boardVO.setBoardSignaturePhoto1(rs.getBytes("boardSignaturePhoto1"));
				boardVO.setBoardSignaturePhoto2(rs.getBytes("boardSignaturePhoto2"));
				boardVO.setBoardSignaturePhoto3(rs.getBytes("boardSignaturePhoto3"));
				boardVO.setBoardSignaturePhoto4(rs.getBytes("boardSignaturePhoto4"));
				boardVO.setBoardSignatureInfo(rs.getString("boardSignatureInfo"));
				boardVO.setBoardStatus(rs.getString("boardStatus"));
				list.add(boardVO);
			}
			
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver."+ e.getMessage());
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured." + se.getMessage());
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			} 
			if (con != null) {
				try {
					con.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
		}
		
		return list;
	}


	@Override
	public List<BoardVO> getAllApplying() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void updateStatus(String boardID) {
		// TODO Auto-generated method stub
		
	}

}
