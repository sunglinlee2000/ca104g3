



package com.board.model;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;

public class BoardTest {

	public static void main(String[] args) {
		BoardJDBCDAO boardJDBCDAO = new BoardJDBCDAO();
//		1.------------------------	
//		BoardVO boardVO = new BoardVO();
//		boardVO.setMemberID("M000001");
//		boardVO.setAdministratorID("A000001");
//		boardVO.setBoardSignatureDate(java.sql.Timestamp.valueOf("2018-06-01 10:10:10"));
//		boardVO.setBoardSignatureEndDate(java.sql.Timestamp.valueOf("2018-08-01 10:10:10"));
//		boardVO.setBoardSignatureTarget(500);
//		boardVO.setBoardSignatureCount(100);
//		boardVO.setBoardSignatureTitle("聯署");
//		byte[] pic = null;
//		try {
//			pic = getPictureByteArray("WebContent/board/images/DAVID.png");
//		}catch (IOException e) {
//			e.printStackTrace(System.err);
//		}
//		boardVO.setBoardPhoto(pic);
//		boardVO.setBoardSignaturePhoto1(pic);
//		boardVO.setBoardSignaturePhoto2(pic);
//		boardVO.setBoardSignaturePhoto3(pic);
//		boardVO.setBoardSignaturePhoto4(pic);
//		boardVO.setBoardSignatureInfo("聯署");
//		boardVO.setBoardStatus("BOARD_APPLYING");
//		boardJDBCDAO.insert(boardVO);
//		
//		System.out.println("OK");
		
//		2.------------------------
//		BoardVO boardVO2 = new BoardVO();
//		boardVO2.setBoardID("B000002");
//		boardVO2.setMemberID("M000001");
//		boardVO2.setAdministratorID("A000001");
//		boardVO2.setBoardSignatureDate(java.sql.Timestamp.valueOf("2018-06-01 10:10:10"));
//		boardVO2.setBoardSignatureEndDate(java.sql.Timestamp.valueOf("2018-08-01 10:10:10"));
//		boardVO2.setBoardSignatureTarget(500);
//		boardVO2.setBoardSignatureCount(200);
//		boardVO2.setBoardSignatureTitle("聯署");
//		byte[] pic = null;
//		try {
//			pic = getPictureByteArray("WebContent/MEMBER-Photo/DAVID.png");
//		}catch (IOException e) {
//			e.printStackTrace(System.err);
//		}
//		boardVO2.setBoardPhoto(pic);
//		boardVO2.setBoardSignaturePhoto1(pic);
//		boardVO2.setBoardSignaturePhoto2(pic);
//		boardVO2.setBoardSignaturePhoto3(pic);
//		boardVO2.setBoardSignaturePhoto4(pic);
//		boardVO2.setBoardSignatureInfo("聯署");
//		boardVO2.setBoardStatus("BOARD_APPLYING");
//		boardJDBCDAO.update(boardVO2);
//		
//		System.out.println("OK");
		
//		3.------------------------
//		boardJDBCDAO.delete("B000009");
//		
//		System.out.println("OK");
		
//		4.--------------------
//		BoardVO boardVO3 = boardJDBCDAO.findByPrimaryKey("B000003");
//		System.out.println(boardVO3.getBoardID());
//		System.out.println(boardVO3.getMemberID());
//		System.out.println(boardVO3.getAdministratorID());
//		System.out.println(boardVO3.getBoardDate());
//		System.out.println(boardVO3.getBoardSignatureDate());
//		System.out.println(boardVO3.getBoardSignatureEndDate());
//		System.out.println(boardVO3.getBoardSignatureTarget());
//		System.out.println(boardVO3.getBoardSignatureTitle());
//		System.out.println(boardVO3.getBoardPhoto());
//		System.out.println(boardVO3.getBoardSignaturePhoto1());
//		System.out.println(boardVO3.getBoardSignaturePhoto2());
//		System.out.println(boardVO3.getBoardSignaturePhoto3());
//		System.out.println(boardVO3.getBoardSignaturePhoto4());
//		System.out.println(boardVO3.getBoardSignatureInfo());
//		System.out.println(boardVO3.getBoardStatus());
//		System.out.println("==================================");
//		System.out.println("OK");
//		5.--------------------
//		List<BoardVO> list = boardJDBCDAO.getAll();
//		for(BoardVO boardVO4 : list) {
//			System.out.println(boardVO4.getBoardID());
//			System.out.println(boardVO4.getMemberID());
//			System.out.println(boardVO4.getAdministratorID());
//			System.out.println(boardVO4.getBoardDate());
//			System.out.println(boardVO4.getBoardSignatureDate());
//			System.out.println(boardVO4.getBoardSignatureEndDate());
//			System.out.println(boardVO4.getBoardSignatureTarget());
//			System.out.println(boardVO4.getBoardSignatureTitle());
//			System.out.println(boardVO4.getBoardPhoto());
//			System.out.println(boardVO4.getBoardSignaturePhoto1());
//			System.out.println(boardVO4.getBoardSignaturePhoto2());
//			System.out.println(boardVO4.getBoardSignaturePhoto3());
//			System.out.println(boardVO4.getBoardSignaturePhoto4());
//			System.out.println(boardVO4.getBoardSignatureInfo());
//			System.out.println(boardVO4.getBoardStatus());
//			System.out.println("==================================");
//		}
//		System.out.println("OK");
		
//----------------------------------------------------
//		
		BoardVO boardVO = new BoardVO();
//		boardVO.setBoardID("B000001");  
//		byte[] pic = null;
//		try {
//			pic = getPictureByteArray("WebContent/board/images/1-1.jpg");
//		}catch(IOException e) {
//			e.printStackTrace();
//		}
//		boardVO.setBoardSignaturePhoto1(pic1);
//		boardJDBCDAO.update(boardVO);
//		System.out.println("OK");
//		


		
		for (int i = 1 ; i <= 6 ; i++) {
				byte[] pic = null;
				byte[] pic1 = null;
				byte[] pic2 = null;
				byte[] pic3 = null;
				byte[] pic4 = null;
				boardVO.setBoardID(("B00000"+i));
				try {
					pic  = getPictureByteArray("WebContent/front-end/res/img/board/"+i+".jpg");
					pic1 = getPictureByteArray("WebContent/front-end/res/img/board/"+i+"-"+"1.jpg");
					pic2 = getPictureByteArray("WebContent/front-end/res/img/board/"+i+"-"+"2.jpg");
					pic3 = getPictureByteArray("WebContent/front-end/res/img/board/"+i+"-"+"3.jpg");
					pic4 = getPictureByteArray("WebContent/front-end/res/img/board/"+i+"-"+"4.jpg");
				} catch(IOException e ) {
					e.printStackTrace(System.err);
				}
				boardVO.setBoardPhoto(pic);
				boardVO.setBoardSignaturePhoto1(pic1);
				boardVO.setBoardSignaturePhoto2(pic2);
				boardVO.setBoardSignaturePhoto3(pic3);
				boardVO.setBoardSignaturePhoto4(pic4);
				boardJDBCDAO.update(boardVO);
				System.out.println("OK");
		}
//		try {
//			pic = getPictureByteArray("WebContent/board/images/DAVID.png");
//		}catch (IOException e) {
//			e.printStackTrace(System.err);
//		}
//		boardVO.setBoardSignaturePhoto1(pic);		
	}
	public static byte[] getPictureByteArray(String path) throws IOException {
		File file = new File(path);
		FileInputStream fis = new FileInputStream(file);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		byte[] buffer = new byte[8192];
		int i;
		while ((i = fis.read(buffer)) != -1) {
			baos.write(buffer, 0, i);
		}
		baos.close();
		fis.close();

		return baos.toByteArray();
	}
}
