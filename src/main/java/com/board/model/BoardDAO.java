package com.board.model;

import java.sql.*;
import java.util.*;

import javax.naming.*;
import javax.sql.DataSource;

import com.boardSignature.model.BoardSignatureService;
import com.boardSignature.model.BoardSignatureVO;

public class BoardDAO implements BoardDAO_interface{
	
	private static DataSource ds = null;
	static {
		try {
			Context ctx = new InitialContext();
			ds = (DataSource) ctx.lookup("java:comp/env/jdbc/CA104G3DB");
		} catch (NamingException e) {
			e.printStackTrace();
		}
	}

	private static final String INSERT_STMT = "INSERT INTO board VALUES('B'||LPAD(TO_CHAR(BOARD_SEQ.NEXTVAL),6,'0'),?,'A000000',(CURRENT_TIMESTAMP),(CURRENT_TIMESTAMP),?,\r\n" + 
			"?,?,?,?,?,?,?,?,'BOARD_APPLYING')";
	private static final String UPDATE = "UPDATE board SET memberid=?, administratorid=?, boarddate=?, boardSignaturedate=?, boardSignatureenddate=?, "
			+ "boardSignaturetarget=?,  boardSignatureTitle=?, boardPhoto=?, boardSignaturePhoto1=?, boardSignaturePhoto2=?, boardSignaturePhoto3=?,"
			+ "boardSignaturePhoto4=?, boardSignatureInfo=?, boardStatus=? WHERE boardid=?";
	private static final String DELETE = "DELETE FROM board WHERE boardid=?";
	private static final String GET_ONE_STMT = "SELECT * FROM board WHERE boardid = ?";
	private static final String GET_ALL_OPERATING = "SELECT * FROM board WHERE boardStatus='BOARD_OPERATING' ORDER BY boardid";
	private static final String GET_ALL_APPLYING = "SELECT * FROM board WHERE boardStatus='BOARD_APPLYING' ORDER BY boardid";
	private static final String UPDATE_STATUS = "UPDATE board SET boardStatus='BOARD_OPERATING' WHERE boardID=?";
	
	
	@Override
	public void insert(BoardVO boardVO) {
		
		Connection con = null;
		PreparedStatement pstmt = null;
		
		try {
			
			con = ds.getConnection();
			pstmt = con.prepareStatement(INSERT_STMT);
			
			pstmt.setString(1, boardVO.getMemberID());
			pstmt.setTimestamp(2, boardVO.getBoardSignatureEndDate());
			pstmt.setInt(3, boardVO.getBoardSignatureTarget());
			pstmt.setString(4, boardVO.getBoardSignatureTitle());
			pstmt.setBytes(5, boardVO.getBoardPhoto());
			pstmt.setBytes(6, boardVO.getBoardSignaturePhoto1());
			pstmt.setBytes(7, boardVO.getBoardSignaturePhoto2());
			pstmt.setBytes(8, boardVO.getBoardSignaturePhoto3());
			pstmt.setBytes(9, boardVO.getBoardSignaturePhoto4());
			pstmt.setString(10, boardVO.getBoardSignatureInfo());
			
			pstmt.executeUpdate();			
		
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured." + se.getMessage());
		
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if(con != null) {
				try {
					con.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
		}
	}

	@Override
	public void update(BoardVO boardVO) {
		
		Connection con = null;
		PreparedStatement pstmt = null;
		
		try {
			
			con =ds.getConnection();
			pstmt = con.prepareStatement(UPDATE);
			
			
			pstmt.setString(1, boardVO.getMemberID());
			pstmt.setString(2, boardVO.getAdministratorID());
			pstmt.setTimestamp(3, boardVO.getBoardDate());
			pstmt.setTimestamp(4, boardVO.getBoardSignatureDate());
			pstmt.setTimestamp(5, boardVO.getBoardSignatureEndDate());
			pstmt.setInt(6, boardVO.getBoardSignatureTarget());
			pstmt.setString(7, boardVO.getBoardSignatureTitle());
			pstmt.setBytes(8, boardVO.getBoardPhoto());
			pstmt.setBytes(9, boardVO.getBoardSignaturePhoto1());
			pstmt.setBytes(10, boardVO.getBoardSignaturePhoto2());
			pstmt.setBytes(11, boardVO.getBoardSignaturePhoto3());
			pstmt.setBytes(12, boardVO.getBoardSignaturePhoto4());
			pstmt.setString(13, boardVO.getBoardSignatureInfo());
			pstmt.setString(14, boardVO.getBoardStatus());
			pstmt.setString(15, boardVO.getBoardID());
			
			
			pstmt.executeUpdate();	
			
			
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured." + se.getMessage());
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
	}

	@Override
	public void delete(String boardID) {
		
		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(DELETE);

			pstmt.setString(1, boardID);

			pstmt.executeUpdate();

		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}

	@Override
	public BoardVO findByPrimaryKey(String boardID) {
		
		BoardVO boardVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		try {
			
			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ONE_STMT);
			
			pstmt.setString(1, boardID);
			
			rs = pstmt.executeQuery();
			
			while (rs.next()) {
				boardVO = new BoardVO();
				boardVO.setBoardID(rs.getString("boardID"));
				boardVO.setMemberID(rs.getString("memberID"));
				boardVO.setAdministratorID(rs.getString("administratorID"));
				boardVO.setBoardDate(rs.getTimestamp("boardDate"));
				boardVO.setBoardSignatureDate(rs.getTimestamp("boardSignatureDate"));
				boardVO.setBoardSignatureEndDate(rs.getTimestamp("boardSignatureEndDate"));
				boardVO.setBoardSignatureTarget(rs.getInt("boardSignatureTarget"));
				boardVO.setBoardSignatureTitle(rs.getString("boardSignatureTitle"));
				boardVO.setBoardPhoto(rs.getBytes("boardPhoto"));
				boardVO.setBoardSignaturePhoto1(rs.getBytes("boardSignaturePhoto1"));
				boardVO.setBoardSignaturePhoto2(rs.getBytes("boardSignaturePhoto2"));
				boardVO.setBoardSignaturePhoto3(rs.getBytes("boardSignaturePhoto3"));
				boardVO.setBoardSignaturePhoto4(rs.getBytes("boardSignaturePhoto4"));
				boardVO.setBoardSignatureInfo(rs.getString("boardSignatureInfo"));
				boardVO.setBoardStatus(rs.getString("boardStatus"));
				
			}
			
			
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured." + se.getMessage());
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			} 
			if (con != null) {
				try {
					con.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
		}
		return boardVO;
	}

	@Override
	public List<BoardVO> getAllOperating() {
		List<BoardVO> list = new ArrayList<BoardVO>();
		BoardVO boardVO = null;
		
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		try {
			
			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ALL_OPERATING);
			rs = pstmt.executeQuery();
			
			while (rs.next()) {
				boardVO = new BoardVO();
				boardVO.setBoardID(rs.getString("boardID"));
				boardVO.setMemberID(rs.getString("memberID"));
				boardVO.setAdministratorID(rs.getString("administratorID"));
				boardVO.setBoardDate(rs.getTimestamp("boardDate"));
				boardVO.setBoardSignatureDate(rs.getTimestamp("boardSignatureDate"));
				boardVO.setBoardSignatureEndDate(rs.getTimestamp("boardSignatureEndDate"));
				boardVO.setBoardSignatureTarget(rs.getInt("boardSignatureTarget"));
				boardVO.setBoardSignatureTitle(rs.getString("boardSignatureTitle"));
				boardVO.setBoardPhoto(rs.getBytes("boardPhoto"));
				boardVO.setBoardSignaturePhoto1(rs.getBytes("boardSignaturePhoto1"));
				boardVO.setBoardSignaturePhoto2(rs.getBytes("boardSignaturePhoto2"));
				boardVO.setBoardSignaturePhoto3(rs.getBytes("boardSignaturePhoto3"));
				boardVO.setBoardSignaturePhoto4(rs.getBytes("boardSignaturePhoto4"));
				boardVO.setBoardSignatureInfo(rs.getString("boardSignatureInfo"));
				boardVO.setBoardStatus(rs.getString("boardStatus"));
				list.add(boardVO);
			}
			
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured." + se.getMessage());
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			} 
			if (con != null) {
				try {
					con.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
		}
		
		return list;
	}

	@Override
	public List<BoardVO> getAllApplying() {
		List<BoardVO> list = new ArrayList<BoardVO>();
		BoardVO boardVO = null;
		
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		try {
			
			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ALL_APPLYING);
			rs = pstmt.executeQuery();
			
			while (rs.next()) {
				boardVO = new BoardVO();
				boardVO.setBoardID(rs.getString("boardID"));
				boardVO.setMemberID(rs.getString("memberID"));
				boardVO.setAdministratorID(rs.getString("administratorID"));
				boardVO.setBoardDate(rs.getTimestamp("boardDate"));
				boardVO.setBoardSignatureDate(rs.getTimestamp("boardSignatureDate"));
				boardVO.setBoardSignatureEndDate(rs.getTimestamp("boardSignatureEndDate"));
				boardVO.setBoardSignatureTarget(rs.getInt("boardSignatureTarget"));
				boardVO.setBoardSignatureTitle(rs.getString("boardSignatureTitle"));
				boardVO.setBoardPhoto(rs.getBytes("boardPhoto"));
				boardVO.setBoardSignaturePhoto1(rs.getBytes("boardSignaturePhoto1"));
				boardVO.setBoardSignaturePhoto2(rs.getBytes("boardSignaturePhoto2"));
				boardVO.setBoardSignaturePhoto3(rs.getBytes("boardSignaturePhoto3"));
				boardVO.setBoardSignaturePhoto4(rs.getBytes("boardSignaturePhoto4"));
				boardVO.setBoardSignatureInfo(rs.getString("boardSignatureInfo"));
				boardVO.setBoardStatus(rs.getString("boardStatus"));
				
				BoardSignatureService boardSignatureSvc = new BoardSignatureService();
				System.out.println(rs.getString("boardID"));
				Set<BoardSignatureVO> set = boardSignatureSvc.getByBoard(rs.getString("boardID"));
				Integer count = set.size();
				System.out.println("count="+count);				
				
				boardVO.setCount(count);
				
				list.add(boardVO);
			}
			
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured." + se.getMessage());
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			} 
			if (con != null) {
				try {
					con.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
		}
		
		return list;
	}

	@Override
	public void updateStatus(String boardID) {
		Connection con = null;
		PreparedStatement pstmt = null;
		
		try {
			
			con =ds.getConnection();
			pstmt = con.prepareStatement(UPDATE_STATUS);
			
			
			pstmt.setString(1, boardID);
			
			
			pstmt.executeUpdate();	
			
			
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured." + se.getMessage());
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}		
	}

}
