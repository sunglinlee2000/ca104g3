package com.issue.controller;

import java.io.*;
import java.sql.Timestamp;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

import javax.servlet.*;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.*;

import org.json.JSONException;
import org.json.JSONObject;

import com.commentList.model.CommentListService;
import com.issue.model.IssueService;
import com.issue.model.IssueVO;
import com.issueLike.model.IssueLikeService;
import com.issueReport.model.IssueReportService;
import com.issueShare.model.IssueShareService;
import com.member.model.MemberService;
import com.member.model.MemberVO;


@MultipartConfig(fileSizeThreshold=1024*1024, maxFileSize=5*1024*1024, maxRequestSize=5*5*1024*1024)
public class IssueServlet extends HttpServlet {

	
	
	public void doGet(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		doPost(req, res);
	}

	public void doPost(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {

		req.setCharacterEncoding("UTF-8");
		String action = req.getParameter("action");
		
		HttpSession session = req.getSession();
		
		if ("getOne_For_Display".equals(action)) { // 來自select_page.jsp的請求

			List<String> errorMsgs = new LinkedList<String>();
			// Store this set in the request scope, in case we need to
			// send the ErrorPage view.
			req.setAttribute("errorMsgs", errorMsgs);

			try {
				/***************************1.接收請求參數 - 輸入格式的錯誤處理**********************/
				String issueID = req.getParameter("issueID");
				String boardID = req.getParameter("boardID");
				// Send the use back to the form, if there were errors
				if (!errorMsgs.isEmpty()) {
					RequestDispatcher failureView = req
							.getRequestDispatcher("/front-end/issue/issue_detailed.jsp");
					failureView.forward(req, res);
					return;//程式中斷
				}
				
				
				/***************************2.開始查詢資料*****************************************/
				IssueService issueSvc = new IssueService();
				IssueVO issueVO = issueSvc.getOneIssue(issueID);
				if (issueVO == null) {
					errorMsgs.add("查無資料");
				}
				String coin =  req.getParameter("coin");
				
				// Send the use back to the form, if there were errors
				if (!errorMsgs.isEmpty()) {
					RequestDispatcher failureView = req
							.getRequestDispatcher("/front-end/issue/issue_detailed.jsp");
					failureView.forward(req, res);
					return;//程式中斷
				}
				
				/***************************3.查詢完成,準備轉交(Send the Success view)*************/
				
				session.setAttribute("issueVO", issueVO);
				req.setAttribute("issueVO", issueVO); // 資料庫取出的empVO物件,存入req
				req.setAttribute("coin", coin);
				req.setAttribute("boardID", boardID);
				
				String url = "/front-end/issue/issue_detailed.jsp";
				RequestDispatcher successView = req.getRequestDispatcher(url); // 成功轉交 listOneEmp.jsp
				successView.forward(req, res);

				/***************************其他可能的錯誤處理*************************************/
			} catch (Exception e) {
				errorMsgs.add("無法取得資料:" + e.getMessage());
				RequestDispatcher failureView = req
						.getRequestDispatcher("/front-end/issue/select_page.jsp");
				failureView.forward(req, res);
			}
		}
		
		
		if ("getOne_For_Update".equals(action)) { // 來自listAllIssue.jsp的請求

			List<String> errorMsgs = new LinkedList<String>();
			// Store this set in the request scope, in case we need to
			// send the ErrorPage view.
			req.setAttribute("errorMsgs", errorMsgs);
			
			try {
				/***************************1.接收請求參數****************************************/
				String issueID = new String(req.getParameter("issueID"));
				
				/***************************2.開始查詢資料****************************************/
				IssueService issueSvc = new IssueService();
				IssueVO issueVO = issueSvc.getOneIssue(issueID);
								
				/***************************3.查詢完成,準備轉交(Send the Success view)************/
				req.setAttribute("issueVO", issueVO);         // 資料庫取出的empVO物件,存入req
				String url = "/front-end/issue/issue_update.jsp";
				RequestDispatcher successView = req.getRequestDispatcher(url);// 成功轉交 update_emp_input.jsp
				successView.forward(req, res);

				/***************************其他可能的錯誤處理**********************************/
			} catch (Exception e) {
				errorMsgs.add("無法取得要修改的資料:" + e.getMessage());
				RequestDispatcher failureView = req
						.getRequestDispatcher("/front-end/issue/issue_list.jsp");
				failureView.forward(req, res);
			}
		}
		
		
		if ("update".equals(action)) { // 來自update_issue_input.jsp的請求
			
			List<String> errorMsgs = new LinkedList<String>();

			req.setAttribute("errorMsgs", errorMsgs);
			System.out.println(1);
			try {
				/***************************1.接收請求參數 - 輸入格式的錯誤處理**********************/
				String issueID = new String(req.getParameter("issueID").trim());												
				String memberID = req.getParameter("memberID");
				String issueTitle = req.getParameter("title");

				String issueDate = req.getParameter("issueDate");
				Integer issueLikeCount =new Integer (req.getParameter("issueLikeCount"));
				
				if (issueTitle == null || issueTitle.trim().length() == 0) {
					errorMsgs.add("話題標題: 請勿空白");
				} 				

				String issueContentPhoto = req.getParameter("issueContentPhoto").trim();
				if (issueContentPhoto == null || issueContentPhoto.trim().length() == 0) {
					errorMsgs.add("話題內容請勿空白");
				}	

				IssueVO issueVO = new IssueVO();
				issueVO.setIssueID(issueID);
				issueVO.setMemberID(memberID);
				issueVO.setIssueTitle(issueTitle);
				issueVO.setIssueContentPhoto(issueContentPhoto);
				
				Timestamp time = new Timestamp(System.currentTimeMillis());
				issueVO.setIssueDate(java.sql.Timestamp.valueOf(ZonedDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"))));
				
				issueVO.setIssueLikeCount(10);

				// Send the use back to the form, if there were errors
				if (!errorMsgs.isEmpty()) {
					req.setAttribute("issueVO", issueVO); // 含有輸入格式錯誤的empVO物件,也存入req
					RequestDispatcher failureView = req
							.getRequestDispatcher("/front-end/issue/issue_update.jsp");
					failureView.forward(req, res);
					return; //程式中斷
				}
				
				/***************************2.開始修改資料*****************************************/
				IssueService issueSvc = new IssueService();
				issueVO = issueSvc.updateIssue(issueID, memberID, issueTitle, issueContentPhoto, time, issueLikeCount );
				
				/***************************3.修改完成,準備轉交(Send the Success view)*************/
				req.setAttribute("issueVO", issueVO); // 資料庫update成功後,正確的的empVO物件,存入req
				String url = "/front-end/issue/issue_detailed.jsp";
				res.sendRedirect(req.getContextPath() + "/issue/issue.do?action=getOne_For_Display&issueID="+issueID);


				/***************************其他可能的錯誤處理*************************************/
			} catch (Exception e) {
				errorMsgs.add("修改資料失敗:"+e.getMessage());
				RequestDispatcher failureView = req
						.getRequestDispatcher("/front-end/issue/update_issue_input.jsp");
				failureView.forward(req, res);
			}
		}

        if ("insert".equals(action)) { // 來自addEmp.jsp的請求  
			
			List<String> errorMsgs = new LinkedList<String>();
			// Store this set in the request scope, in case we need to
			// send the ErrorPage view.
			req.setAttribute("errorMsgs", errorMsgs);

			try {
				/***********************1.接收請求參數 - 輸入格式的錯誤處理*************************/	

//				String boardID = req.getParameter("boardID");
				String boardID = (String)session.getAttribute("boardID");//取回專版編號，動作完回到專版話題列表
				String memberID = req.getParameter("memberID");
				
	
				String issueTitle = req.getParameter("title");				
				if (issueTitle == null || issueTitle.trim().length() == 0) {
					errorMsgs.add("話題標題: 請勿空白");
				}		

				String issueContentPhoto = req.getParameter("issueContentPhoto").trim();				
				if (issueContentPhoto == null || issueContentPhoto.trim().length() == 0) {
					errorMsgs.add("話題內容請勿空白");
				}	

//				String boardID = new String(req.getParameter("boardID"));
//				String memberID = new String(req.getParameter("memberID"));

				IssueVO issueVO = new IssueVO();
				issueVO.setBoardID(boardID);
				issueVO.setMemberID(memberID);
				issueVO.setIssueTitle(issueTitle);
				issueVO.setIssueContentPhoto(issueContentPhoto);
				
				Timestamp time = new Timestamp(System.currentTimeMillis());
				issueVO.setIssueDate(java.sql.Timestamp.valueOf(ZonedDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"))));
				issueVO.setIssueCoinGained(10);
				issueVO.setIssueLikeCount(10);
				
				//發文增加會員持有的代幣量
				MemberService memberSvc = new MemberService();
				
				MemberVO memberVO = memberSvc.getOneMember(memberID);
				
				Integer memberCoin = memberVO.getMemberCoin() + 300;
								
				memberVO.setMemberID(memberID);
				memberVO.setMemberCoin(memberCoin);
				
				
				// Send the use back to the form, if there were errors
				if (!errorMsgs.isEmpty()) {
					req.setAttribute("issueVO", issueVO); // 含有輸入格式錯誤的empVO物件,也存入req
					RequestDispatcher failureView = req
							.getRequestDispatcher("/front-end/issue/issue_windowadd.jsp");
					failureView.forward(req, res);
					return;
				}
				
				/***************************2.開始新增資料***************************************/
				
				IssueService issueSvc = new IssueService();
				issueVO = issueSvc.addIssue(boardID, memberID, issueTitle, issueContentPhoto,time, 0, 0);
				IssueVO issueVO2 = new IssueVO();
				issueVO2=issueSvc.getOneBy3(memberID, boardID, issueTitle);
				String issueID=issueVO2.getIssueID();
				/***************************3.新增完成,準備轉交(Send the Success view)***********/
				//得代幣alert	
				String coin = "coin";
				
				req.setAttribute("coin",coin);
				
				req.setAttribute("boardIDxx",boardID);//利用取到的boardID返回列表
				
//				String url = "/front-end/issue/issue_list.jsp";
				
				res.sendRedirect(req.getContextPath() + "/issue/issue.do?action=getOne_For_Display&issueID="+issueID+"&coin=coin");
				
				
				//增加會員持有的代幣量
				memberVO = memberSvc.updateMemberCoin(memberCoin,memberID);
				/***************************其他可能的錯誤處理**********************************/
			} catch (Exception e) {
				errorMsgs.add(e.getMessage());
				RequestDispatcher failureView = req
						.getRequestDispatcher("/front-end/issue/issue_windowadd.jsp");
				failureView.forward(req, res);
			}
		}
		
		
//		if ("delete".equals(action)) { 
//
//			List<String> errorMsgs = new LinkedList<String>();
//			// Store this set in the request scope, in case we need to
//			// send the ErrorPage view.
//			req.setAttribute("errorMsgs", errorMsgs);
//	
//			try {
//				/***************************1.接收請求參數***************************************/
//				String issueID = new String(req.getParameter("issueID"));
//				
//				/***************************2.開始刪除資料***************************************/
//				IssueService issueSvc = new IssueService();
//				issueSvc.deleteIssue(issueID);
//				
//				/***************************3.刪除完成,準備轉交(Send the Success view)***********/								
//				String url = "/front-end/issue/issue_list.jsp";
//				RequestDispatcher successView = req.getRequestDispatcher(url);// 刪除成功後,轉交回送出刪除的來源網頁
//				successView.forward(req, res);
//				
//				/***************************其他可能的錯誤處理**********************************/
//			} catch (Exception e) {
//				errorMsgs.add("刪除資料失敗:"+"文章已經被分享過搂!!所以要先去刪小表");
//				RequestDispatcher failureView = req
//						.getRequestDispatcher("/front-end/issue/issue_list.jsp");
//				failureView.forward(req, res);
//			}
//		}
		
		if ("deleteByIssueID".equals(action)) { 

			List<String> errorMsgs = new LinkedList<String>();
			// Store this set in the request scope, in case we need to
			// send the ErrorPage view.
			req.setAttribute("errorMsgs", errorMsgs);
	
			try {
				/***************************1.接收請求參數***************************************/
				String issueID = new String(req.getParameter("issueID"));
				String boardID = (String)session.getAttribute("boardID");//取回專版編號，動作完回到專版話題列表

				/***************************2.開始刪除資料***************************************/
				
				IssueShareService issueShareSvc = new IssueShareService();
				issueShareSvc.deleteByIssueID(issueID);
				
				IssueLikeService issueLikeSvc = new IssueLikeService();
				issueLikeSvc.deleteByIssueID(issueID);
				
				IssueReportService issueReportSvc = new IssueReportService();
				issueReportSvc.deleteByIssueID(issueID);
				
				CommentListService commentListSvc = new CommentListService();
				commentListSvc.deleteByIssueID(issueID);
				
				IssueService issueSvc = new IssueService();
				issueSvc.deleteByIssueID(issueID);
				
				//刪文減少會員持有的代幣量
				String memberID = req.getParameter("memberID");
				
				MemberService memberSvc = new MemberService();
				
				MemberVO memberVO = memberSvc.getOneMember(memberID);
				
				Integer memberCoin = memberVO.getMemberCoin() - 100;

				memberVO.setMemberID(memberID);
				memberVO.setMemberCoin(memberCoin);
				
				/***************************3.刪除完成,準備轉交(Send the Success view)***********/
				req.setAttribute("boardIDxx",boardID);//利用取到的boardID返回列表
				String url = "/front-end/issue/issue_list.jsp";
				
				res.sendRedirect(req.getContextPath() + "/issue/issue.do?action=getOne_For_Display&issueID="+issueID);
				
				//存入減少的代幣量
				memberVO = memberSvc.updateMemberCoin(memberCoin,memberID);
				/***************************其他可能的錯誤處理**********************************/
			} catch (Exception e) {
				errorMsgs.add("刪除資料失敗:"+"文章已經被分享過搂!!所以要先去刪小表");
				RequestDispatcher failureView = req
						.getRequestDispatcher("/front-end/issue/issue_list.jsp");
				failureView.forward(req, res);
			}
		}
		
		if ("getOne_For_Display2".equals(action)) { // 來自select_page.jsp的請求
			System.out.println(action);
			String issueID = req.getParameter("issueID");
			
			JSONObject obj = new JSONObject();
			
			//刪除資料
			
			IssueService issueService = new IssueService();
			IssueVO issueVO = new IssueVO();	
			issueVO=issueService.getOneIssue(issueID);
			
			String boardID=issueVO.getBoardID();
			String issueTitle=issueVO.getIssueTitle();
			String issueContenPhoto=issueVO.getIssueContentPhoto();
			
			try {
				obj.put("boardID", boardID);
				obj.put("issueTitle", issueTitle);
				obj.put("issueContenPhoto", issueContenPhoto);
			} catch (JSONException e) {
				e.printStackTrace();
			}
			
			res.setContentType("text/plain");
			res.setCharacterEncoding("UTF-8");
			PrintWriter out = res.getWriter();
			out.write(obj.toString());
			out.flush();
			out.close();
			
		}		
	}
}
