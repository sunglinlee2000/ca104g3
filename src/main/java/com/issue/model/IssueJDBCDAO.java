package com.issue.model;

import java.util.*;
import java.sql.*;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;


public class IssueJDBCDAO implements IssueDAO_interface {
	String driver = "oracle.jdbc.driver.OracleDriver";
	String url = "jdbc:oracle:thin:@localhost:1521:XE";
	String userid = "CA104G3";
	String passwd = "123456";
	
	private static final String INSERT_STMT =
			"INSERT INTO issue "
			+ "(issueID,boardID,memberID,issueTitle,issueContentPhoto,issueDate,issueCoinGained,issueLikeCount)"
			+ " VALUES "
			+ "('I'||LPAD(to_char(ISSUE_seq.NEXTVAL), 6, '0'),?,?,?,?,?,?,?)";
		private static final String GET_ALL_STMT = 
			"SELECT issueID,boardID,memberID,issueTitle,issueContentPhoto,issueDate,issueCoinGained,issueLikeCount FROM issue order by issueID DESC";
		private static final String GET_ONE_STMT = 
			"SELECT issueID,boardID,memberID,issueTitle,issueContentPhoto,issueDate,issueCoinGained,issueLikeCount FROM issue where issueID = ?";
		private static final String DELETEBYISSUEID = 
			"DELETE FROM issue where issueID = ?";
		private static final String UPDATE = 
			"UPDATE issue set issueTitle=?, issueContentPhoto=? where issueID = ?";
		private static final String GET_ALL_STMT_BY_BOARDID = 
				"select * from issue where boardid = ? order by issueID desc";
		private static final String GET_ALL_STMT_BY_MEMBERID = 
				"select * from issue where memberid = ? order by issueID desc";

	@Override
	public void insert(IssueVO issueVO) {
		Connection con = null;
		PreparedStatement pstmt = null;
		
		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(INSERT_STMT);

			pstmt.setString(1, issueVO.getBoardID());
			pstmt.setString(2, issueVO.getMemberID());
			pstmt.setString(3, issueVO.getIssueTitle());
			pstmt.setString(4, issueVO.getIssueContentPhoto());
			pstmt.setTimestamp(5, issueVO.getIssueDate());
			pstmt.setInt(6, issueVO.getIssueCoinGained());
			pstmt.setInt(7, issueVO.getIssueLikeCount());
			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}

	@Override
	public void update(IssueVO issueVO) {
		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(UPDATE);

			pstmt.setString(1, issueVO.getIssueTitle());
			pstmt.setString(2, issueVO.getIssueContentPhoto());	
			pstmt.setString(3, issueVO.getIssueID());
			
			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

		
	}

	@Override
	public void deleteByIssueID(String issueID) {
		
		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(DELETEBYISSUEID);

			pstmt.setString(1, issueID);
			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

		
	}

	@Override
	public IssueVO findByPrimaryKey(String issueID) {
		IssueVO issueVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(GET_ONE_STMT);

			pstmt.setString(1, issueID);

			rs = pstmt.executeQuery();

			while (rs.next()) {
				
				issueVO = new IssueVO();
				issueVO.setIssueID(rs.getString("issueID"));
				issueVO.setBoardID(rs.getString("boardID"));
				issueVO.setMemberID(rs.getString("memberID"));
				issueVO.setIssueTitle(rs.getString("issueTitle"));
				issueVO.setIssueContentPhoto(rs.getString("issueContentPhoto"));
				issueVO.setIssueDate(rs.getTimestamp("issueDate"));
				issueVO.setIssueCoinGained(rs.getInt("issueCoinGained"));
				issueVO.setIssueLikeCount(rs.getInt("issueLikeCount"));
			}

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return issueVO;
	}

	@Override
	public List<IssueVO> getAll() {
		List<IssueVO> list = new ArrayList<IssueVO>();
		IssueVO issueVO = null;
		
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(GET_ALL_STMT);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				// empVO �]�٬� Domain objects
				
				issueVO = new IssueVO();				
				issueVO.setIssueID(rs.getString("issueID"));
				issueVO.setBoardID(rs.getString("boardID"));
				issueVO.setMemberID(rs.getString("memberID"));
				issueVO.setIssueTitle(rs.getString("issueTitle"));
				issueVO.setIssueContentPhoto(rs.getString("issueContentPhoto"));
				issueVO.setIssueDate(rs.getTimestamp("issueDate"));
				issueVO.setIssueCoinGained(rs.getInt("issueCoinGained"));
				issueVO.setIssueLikeCount(rs.getInt("issueLikeCount"));
				
				list.add(issueVO); // Store the row in the list
			}

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return list;
	}
	
	@Override
	public List<IssueVO> getAllByBoardID(String boardID) {
		List<IssueVO> issuelist = new ArrayList<IssueVO>();
		IssueVO issueVO = null;
		
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs =null;

		try {
			
			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(GET_ALL_STMT_BY_BOARDID);
			rs = pstmt.executeQuery();
			
			while (rs.next()) {
				issueVO = new IssueVO();
				issueVO.setIssueID(rs.getString("issueID"));
				issueVO.setBoardID(rs.getString("boardID"));
				issueVO.setMemberID(rs.getString("memberID"));
				issueVO.setIssueTitle(rs.getString("issueTitle"));
				issueVO.setIssueContentPhoto(rs.getString("issueContentPhoto"));
				issueVO.setIssueDate(rs.getTimestamp("issueDate"));
				issueVO.setIssueCoinGained(rs.getInt("issueCoinGained"));
				issueVO.setIssueLikeCount(rs.getInt("issueLikeCount"));
				issuelist.add(issueVO);
				
			}
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return issuelist;
	}
	
	@Override
	public List<IssueVO> getAllByMemberID(String memberID) {
		List<IssueVO> issuelist = new ArrayList<IssueVO>();
		IssueVO issueVO = null;
		
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs =null;

		try {
			
			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(GET_ALL_STMT_BY_MEMBERID);
			rs = pstmt.executeQuery();
			
			while (rs.next()) {
				issueVO = new IssueVO();
				issueVO.setIssueID(rs.getString("issueID"));
				issueVO.setBoardID(rs.getString("boardID"));
				issueVO.setMemberID(rs.getString("memberID"));
				issueVO.setIssueTitle(rs.getString("issueTitle"));
				issueVO.setIssueContentPhoto(rs.getString("issueContentPhoto"));
				issueVO.setIssueDate(rs.getTimestamp("issueDate"));
				issueVO.setIssueCoinGained(rs.getInt("issueCoinGained"));
				issueVO.setIssueLikeCount(rs.getInt("issueLikeCount"));
				issuelist.add(issueVO);
				
			}
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return issuelist;
	}
	public static void main(String[] args) {
		
		IssueJDBCDAO dao = new IssueJDBCDAO();
		
		// 新增OK
//		IssueVO issueVO1 = new IssueVO();
//		issueVO1.setBoardID("B000006");
//		issueVO1.setMemberID("M000006");
//		issueVO1.setIssueTitle("蘇打綠好厲害");
//		issueVO1.setIssueContentPhoto(null);
//		issueVO1.setIssueDate(java.sql.Timestamp.valueOf(ZonedDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"))));
//		issueVO1.setIssueCoinGained(10);
//		issueVO1.setIssueLikeCount(0);
//		dao.insert(issueVO1);	
		
		// 修改OK
//		IssueVO issueVO2 = new IssueVO();
//		issueVO2.setIssueID("I000001");
//		System.out.println(".......");
//		issueVO2.setIssueTitle("17好帥");
//		System.out.println(".......");
//		issueVO2.setIssueContentPhoto(null);
//		System.out.println(".......");
//		dao.update(issueVO2);
//		System.out.println("OK");
		
		// 刪除OK
//		dao.deleteByIssueID("I000003");
		
		// 查詢OK
		IssueVO issueVO3 = dao.findByPrimaryKey("I000001");
		System.out.print(issueVO3.getIssueID() + ",");
		System.out.print(issueVO3.getBoardID() + ",");
		System.out.print(issueVO3.getMemberID() + ",");
		System.out.print(issueVO3.getIssueTitle() + ",");
		System.out.print(issueVO3.getIssueContentPhoto() + ",");
		System.out.print(issueVO3.getIssueDate() + ",");
		System.out.print(issueVO3.getIssueCoinGained() + ",");
		System.out.println(issueVO3.getIssueLikeCount());
		System.out.println("---------------------");
		
		// 查全部OK
//		List<IssueVO> list = dao.getAll();
//		for(IssueVO aIssue : list) {
//			System.out.print(aIssue.getIssueID() + ",");
//			System.out.println(".......");
//			System.out.print(aIssue.getBoardID() + ",");
//			System.out.println(".......");
//			System.out.print(aIssue.getMemberID() + ",");
//			System.out.println(".......");
//			System.out.print(aIssue.getIssueTitle() + ",");
//			System.out.println(".......");
//			System.out.print(aIssue.getIssueContentPhoto() + ",");
//			System.out.println(".......");
//			System.out.print(aIssue.getIssueDate() + ",");
//			System.out.println(".......");
//			System.out.print(aIssue.getIssueCoinGained() + ",");
//			System.out.println(".......");
//			System.out.println(aIssue.getIssueLikeCount());
//			System.out.println(".......");			
//		}
	}

	@Override
	public IssueVO findBy3(String memberID, String boardID, String issueTitle) {
		// TODO Auto-generated method stub
		return null;
	}

	
}
