package com.issue.model;

import java.util.List;

import com.adfund.model.ADFundVO;

public interface IssueDAO_interface {
	
	public void insert(IssueVO issueVO);
    public void update(IssueVO issueVO);
    public void deleteByIssueID(String issueid);
    public IssueVO findByPrimaryKey(String issueid);
    public List<IssueVO> getAll();
	public List<IssueVO> getAllByBoardID(String boardID);
	public List<IssueVO> getAllByMemberID(String memberID);
	public IssueVO findBy3(String memberID,String boardID,String issueTitle);
}
