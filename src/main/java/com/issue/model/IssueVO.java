package com.issue.model;

import java.sql.Timestamp;

public class IssueVO implements java.io.Serializable{

	private String issueID;
	private String boardID;
	private String memberID;
	private String issueTitle;
	private String issueContentPhoto;
	private Timestamp issueDate;
	private Integer issueCoinGained;
	private Integer issueLikeCount;
	
	
	public String getIssueID() {
		return issueID;
	}
	public void setIssueID(String issueID) {
		this.issueID = issueID;
	}
	public String getBoardID() {
		return boardID;
	}
	public void setBoardID(String boardID) {
		this.boardID = boardID;
	}
	public String getMemberID() {
		return memberID;
	}
	public void setMemberID(String memberID) {
		this.memberID = memberID;
	}
	public String getIssueTitle() {
		return issueTitle;
	}
	public void setIssueTitle(String issueTitle) {
		this.issueTitle = issueTitle;
	}
	public String getIssueContentPhoto() {
		return issueContentPhoto;
	}
	public void setIssueContentPhoto(String issueContentPhoto) {
		this.issueContentPhoto = issueContentPhoto;
	}
	public Timestamp getIssueDate() {
		return issueDate;
	}
	public void setIssueDate(Timestamp issueDate) {
		this.issueDate = issueDate;
	}
	public Integer getIssueCoinGained() {
		return issueCoinGained;
	}
	public void setIssueCoinGained(Integer issueCoinGained) {
		this.issueCoinGained = issueCoinGained;
	}
	public Integer getIssueLikeCount() {
		return issueLikeCount;
	}
	public void setIssueLikeCount(Integer issueLikeCount) {
		this.issueLikeCount = issueLikeCount;
	}
	
	
}