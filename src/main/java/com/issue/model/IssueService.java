package com.issue.model;

import java.sql.Timestamp;
import java.util.List;

import com.adfund.model.ADFundVO;
import com.commentList.model.*;
import com.issueLike.model.*;
import com.issueReport.model.*;
import com.issueShare.model.*;



public class IssueService {

	private IssueDAO_interface issueDAO;
	private CommentListDAO_interface commentListDAO;
	private IssueReportDAO_interface issueReportDAO;
	private IssueLikeDAO_interface issueLikeDAO;
	private IssueShareDAO_interface issueShareDAO;
	
	
	public IssueService() {
		issueDAO = new IssueDAO();
		commentListDAO = new CommentListDAO();
		issueReportDAO = new IssueReportDAO();
		issueLikeDAO = new IssueLikeDAO();
		issueShareDAO = new IssueShareDAO();
		
	}
	
	public IssueVO addIssue(String boardID,String memberID,String issueTitle,String issueContentPhoto,Timestamp issueDate,Integer issueCoinGained,Integer issueLikeCount) {
		
		IssueVO issueVO = new IssueVO();
		
		issueVO.setBoardID(boardID);
		issueVO.setMemberID(memberID);
		issueVO.setIssueTitle(issueTitle);
		issueVO.setIssueContentPhoto(issueContentPhoto);
		issueVO.setIssueDate(issueDate);
		issueVO.setIssueCoinGained(issueCoinGained);
		issueVO.setIssueLikeCount(issueLikeCount);
		issueDAO.insert(issueVO);
		
		return issueVO;		
	}
	
	public IssueVO updateIssue(String issueID,String memberID,String issueTitle,String issueContentPhoto, Timestamp issueDate,Integer issueLikeCount ) {
		
		IssueVO issueVO = new IssueVO();
		
		issueVO.setMemberID(memberID);
		issueVO.setIssueTitle(issueTitle);		
		issueVO.setIssueContentPhoto(issueContentPhoto);
		issueVO.setIssueDate(issueDate);
		issueVO.setIssueLikeCount(issueLikeCount);
		
		issueVO.setIssueID(issueID);
		issueDAO.update(issueVO);
		
		return issueVO;		
	}
	
	public void deleteByIssueID(String issueID) {
		
		issueShareDAO.deleteByIssueID(issueID);
		issueLikeDAO.deleteByIssueID(issueID);
		issueReportDAO.deleteByIssueID(issueID);
		commentListDAO.deleteByIssueID(issueID);
		issueDAO.deleteByIssueID(issueID);
		
	}
	
	public IssueVO getOneIssue(String issueID) {
		return issueDAO.findByPrimaryKey(issueID);		
	}
	
	public List<IssueVO> getAll(){
		return issueDAO.getAll();
	}
	
	public List<IssueVO> getAllByBoardID(String boardID){
		return issueDAO.getAllByBoardID(boardID);
	}
	
	public IssueVO getOneBy3(String memberID,String boardID,String issueTitle) {
		return issueDAO.findBy3( memberID,boardID,issueTitle);		
	}
	
	public List<IssueVO> getAllByMemberID(String memberID) {
		return issueDAO.getAllByMemberID(memberID);
	}
}  

