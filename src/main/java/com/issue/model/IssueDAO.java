package com.issue.model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import com.adfund.model.ADFundVO;

public class IssueDAO implements IssueDAO_interface{
	private static DataSource ds = null;
	static {
		try {
			Context ctx = new InitialContext();
			ds = (DataSource) ctx.lookup("java:comp/env/jdbc/CA104G3DB");
		} catch (NamingException e) {
			e.printStackTrace();
		}
	}
	
	private static final String INSERT_STMT =
			"INSERT INTO issue "
			+ "(issueID,boardID,memberID,issueTitle,issueContentPhoto,issueDate,issueCoinGained,issueLikeCount)"
			+ " VALUES "
			+ "('I'||LPAD(to_char(ISSUE_seq.NEXTVAL), 6, '0'),?,?,?,?,?,?,?)";
		private static final String GET_ALL_STMT = 
			"SELECT issueID,boardID,memberID,issueTitle,issueContentPhoto,issueDate,issueCoinGained,issueLikeCount FROM issue order by issueID DESC";
		private static final String GET_ONE_STMT = 
			"SELECT issueID,boardID,memberID,issueTitle,issueContentPhoto,issueDate,issueCoinGained,issueLikeCount FROM issue where issueID = ?";
		private static final String DELETEBYISSUEID = 
			"DELETE FROM issue where issueID = ?";
		private static final String UPDATE = 
			"UPDATE issue set memberID=?, issueTitle=?, issueContentPhoto=?,issueDate=?,issueLikeCount=?  where issueID = ?";
		private static final String GET_ALL_STMT_BY_BOARDID = 
				"select * from issue where boardid = ? order by issueID desc";
		private static final String GET_ALL_STMT_BY_MEMBERID = 
				"select * from issue where memberid = ? order by issueID desc";
		private static final String GET_ONE_BY_MBT =
				"select * from issue where memberID= ? and boardID= ? and issueTitle like ?";
		@Override
		public void insert(IssueVO issueVO) {
			Connection con = null;
			PreparedStatement pstmt = null;
			
			try {
				
				con = ds.getConnection();
				pstmt = con.prepareStatement(INSERT_STMT);

				pstmt.setString(1, issueVO.getBoardID());
				pstmt.setString(2, issueVO.getMemberID());
				pstmt.setString(3, issueVO.getIssueTitle());
				pstmt.setString(4, issueVO.getIssueContentPhoto());
				pstmt.setTimestamp(5, issueVO.getIssueDate());
				pstmt.setInt(6, issueVO.getIssueCoinGained());
				pstmt.setInt(7, issueVO.getIssueLikeCount());
				pstmt.executeUpdate();

				// Handle any driver errors
			} catch (SQLException se) {
				throw new RuntimeException("A database error occured. "
						+ se.getMessage());
				// Clean up JDBC resources
			} finally {
				if (pstmt != null) {
					try {
						pstmt.close();
					} catch (SQLException se) {
						se.printStackTrace(System.err);
					}
				}
				if (con != null) {
					try {
						con.close();
					} catch (Exception e) {
						e.printStackTrace(System.err);
					}
				}
			}

		}

		@Override
		public void update(IssueVO issueVO) {
			Connection con = null;
			PreparedStatement pstmt = null;

			try {

				con = ds.getConnection();
				pstmt = con.prepareStatement(UPDATE);
				
				pstmt.setString(1, issueVO.getMemberID());
				pstmt.setString(2, issueVO.getIssueTitle());
				pstmt.setString(3, issueVO.getIssueContentPhoto());					
				pstmt.setTimestamp(4, issueVO.getIssueDate());
				pstmt.setInt(5, issueVO.getIssueLikeCount());
				pstmt.setString(6, issueVO.getIssueID());
				
				pstmt.executeUpdate();

				// Handle any driver errors
			} catch (SQLException se) {
				throw new RuntimeException("A database error occured. "
						+ se.getMessage());
				// Clean up JDBC resources
			} finally {
				if (pstmt != null) {
					try {
						pstmt.close();
					} catch (SQLException se) {
						se.printStackTrace(System.err);
					}
				}
				if (con != null) {
					try {
						con.close();
					} catch (Exception e) {
						e.printStackTrace(System.err);
					}
				}
			}

			
		}

		@Override
		public void deleteByIssueID(String issueID) {
			
			Connection con = null;
			PreparedStatement pstmt = null;

			try {

		
				con = ds.getConnection();
				pstmt = con.prepareStatement(DELETEBYISSUEID);

				pstmt.setString(1, issueID);
				pstmt.executeUpdate();

				// Handle any driver errors
			} catch (SQLException se) {
				throw new RuntimeException("A database error occured. "
						+ se.getMessage());
				// Clean up JDBC resources
			} finally {
				if (pstmt != null) {
					try {
						pstmt.close();
					} catch (SQLException se) {
						se.printStackTrace(System.err);
					}
				}
				if (con != null) {
					try {
						con.close();
					} catch (Exception e) {
						e.printStackTrace(System.err);
					}
				}
			}

			
		}

		@Override
		public IssueVO findByPrimaryKey(String issueID) {
			IssueVO issueVO = null;
			Connection con = null;
			PreparedStatement pstmt = null;
			ResultSet rs = null;

			try {

				con = ds.getConnection();
				pstmt = con.prepareStatement(GET_ONE_STMT);

				pstmt.setString(1, issueID);

				rs = pstmt.executeQuery();

				while (rs.next()) {
					
					
					issueVO = new IssueVO();
					issueVO.setIssueID(rs.getString("issueID"));
					issueVO.setBoardID(rs.getString("boardID"));
					issueVO.setMemberID(rs.getString("memberID"));
					issueVO.setIssueTitle(rs.getString("issueTitle"));
					issueVO.setIssueContentPhoto(rs.getString("issueContentPhoto"));
					issueVO.setIssueDate(rs.getTimestamp("issueDate"));
					issueVO.setIssueCoinGained(rs.getInt("issueCoinGained"));
					issueVO.setIssueLikeCount(rs.getInt("issueLikeCount"));
				}

				// Handle any driver errors
			} catch (SQLException se) {
				throw new RuntimeException("A database error occured. "
						+ se.getMessage());
				// Clean up JDBC resources
			} finally {
				if (pstmt != null) {
					try {
						pstmt.close();
					} catch (SQLException se) {
						se.printStackTrace(System.err);
					}
				}
				if (con != null) {
					try {
						con.close();
					} catch (Exception e) {
						e.printStackTrace(System.err);
					}
				}
			}
			return issueVO;
		}

		@Override
		public List<IssueVO> getAll() {
			List<IssueVO> list = new ArrayList<IssueVO>();
			IssueVO issueVO = null;
			
			Connection con = null;
			PreparedStatement pstmt = null;
			ResultSet rs = null;
			
			try {

				con = ds.getConnection();
				pstmt = con.prepareStatement(GET_ALL_STMT);
				rs = pstmt.executeQuery();

				while (rs.next()) {
					// empVO �]�٬� Domain objects
					
					issueVO = new IssueVO();				
					issueVO.setIssueID(rs.getString("issueID"));
					issueVO.setBoardID(rs.getString("boardID"));
					issueVO.setMemberID(rs.getString("memberID"));
					issueVO.setIssueTitle(rs.getString("issueTitle"));
					issueVO.setIssueContentPhoto(rs.getString("issueContentPhoto"));
					issueVO.setIssueDate(rs.getTimestamp("issueDate"));
					issueVO.setIssueCoinGained(rs.getInt("issueCoinGained"));
					issueVO.setIssueLikeCount(rs.getInt("issueLikeCount"));
					
					list.add(issueVO); // Store the row in the list
				}

				// Handle any driver errors
			} catch (SQLException se) {
				throw new RuntimeException("A database error occured. "
						+ se.getMessage());
				// Clean up JDBC resources
			} finally {
				if (pstmt != null) {
					try {
						pstmt.close();
					} catch (SQLException se) {
						se.printStackTrace(System.err);
					}
				}
				if (con != null) {
					try {
						con.close();
					} catch (Exception e) {
						e.printStackTrace(System.err);
					}
				}
			}
			return list;
		}
		
		@Override
		public List<IssueVO> getAllByBoardID(String boardID) {
			List<IssueVO> issuelist = new ArrayList<IssueVO>();
			IssueVO issueVO = null;
			
			Connection con = null;
			PreparedStatement pstmt = null;
			ResultSet rs =null;

			try {

				con = ds.getConnection();
				pstmt = con.prepareStatement(GET_ALL_STMT_BY_BOARDID);
				pstmt.setString(1, boardID);
				rs = pstmt.executeQuery();
				
				while (rs.next()) {
					issueVO = new IssueVO();
					issueVO.setIssueID(rs.getString("issueID"));
					issueVO.setBoardID(rs.getString("boardID"));
					issueVO.setMemberID(rs.getString("memberID"));
					issueVO.setIssueTitle(rs.getString("issueTitle"));
					issueVO.setIssueContentPhoto(rs.getString("issueContentPhoto"));
					issueVO.setIssueDate(rs.getTimestamp("issueDate"));
					issueVO.setIssueCoinGained(rs.getInt("issueCoinGained"));
					issueVO.setIssueLikeCount(rs.getInt("issueLikeCount"));
					issuelist.add(issueVO);
					
				}
			} catch (SQLException se) {
				throw new RuntimeException("A database error occured. " + se.getMessage());
				
			} finally {
				if (rs != null) {
					try {
						rs.close();
					} catch (SQLException se) {
						se.printStackTrace(System.err);
					}
				}
				if (pstmt != null) {
					try {
						pstmt.close();
					} catch (SQLException se) {
						se.printStackTrace(System.err);
					}
				}
				if (con != null) {
					try {
						con.close();
					} catch (Exception e) {
						e.printStackTrace(System.err);
					}
				}
			}

			return issuelist;
		}

		@Override
		public List<IssueVO> getAllByMemberID(String memberID) {
			List<IssueVO> issuelist = new ArrayList<IssueVO>();
			IssueVO issueVO = null;
			
			Connection con = null;
			PreparedStatement pstmt = null;
			ResultSet rs =null;

			try {

				con = ds.getConnection();
				pstmt = con.prepareStatement(GET_ALL_STMT_BY_MEMBERID);
				pstmt.setString(1, memberID);
				rs = pstmt.executeQuery();
				
				while (rs.next()) {
					issueVO = new IssueVO();
					issueVO.setIssueID(rs.getString("issueID"));
					issueVO.setBoardID(rs.getString("boardID"));
					issueVO.setMemberID(rs.getString("memberID"));
					issueVO.setIssueTitle(rs.getString("issueTitle"));
					issueVO.setIssueContentPhoto(rs.getString("issueContentPhoto"));
					issueVO.setIssueDate(rs.getTimestamp("issueDate"));
					issueVO.setIssueCoinGained(rs.getInt("issueCoinGained"));
					issueVO.setIssueLikeCount(rs.getInt("issueLikeCount"));
					issuelist.add(issueVO);
					
				}
			} catch (SQLException se) {
				throw new RuntimeException("A database error occured. " + se.getMessage());
				
			} finally {
				if (rs != null) {
					try {
						rs.close();
					} catch (SQLException se) {
						se.printStackTrace(System.err);
					}
				}
				if (pstmt != null) {
					try {
						pstmt.close();
					} catch (SQLException se) {
						se.printStackTrace(System.err);
					}
				}
				if (con != null) {
					try {
						con.close();
					} catch (Exception e) {
						e.printStackTrace(System.err);
					}
				}
			}

			return issuelist;
		}


		@Override
		public IssueVO findBy3(String memberID, String boardID, String issueTitle) {
			IssueVO issueVO = null;
			Connection con = null;
			PreparedStatement pstmt = null;
			ResultSet rs = null;

			try {

				con = ds.getConnection();
				pstmt = con.prepareStatement(GET_ONE_BY_MBT);

				pstmt.setString(1, memberID);
				pstmt.setString(2, boardID);
				pstmt.setString(3, issueTitle);
				rs = pstmt.executeQuery();

				while (rs.next()) {
					
					
					issueVO = new IssueVO();
					issueVO.setIssueID(rs.getString("issueID"));
					issueVO.setBoardID(rs.getString("boardID"));
					issueVO.setMemberID(rs.getString("memberID"));
					issueVO.setIssueTitle(rs.getString("issueTitle"));
					issueVO.setIssueContentPhoto(rs.getString("issueContentPhoto"));
					issueVO.setIssueDate(rs.getTimestamp("issueDate"));
					issueVO.setIssueCoinGained(rs.getInt("issueCoinGained"));
					issueVO.setIssueLikeCount(rs.getInt("issueLikeCount"));
				}

				// Handle any driver errors
			} catch (SQLException se) {
				throw new RuntimeException("A database error occured. "
						+ se.getMessage());
				// Clean up JDBC resources
			} finally {
				if (pstmt != null) {
					try {
						pstmt.close();
					} catch (SQLException se) {
						se.printStackTrace(System.err);
					}
				}
				if (con != null) {
					try {
						con.close();
					} catch (Exception e) {
						e.printStackTrace(System.err);
					}
				}
			}
			return issueVO;
		}
	}