package com.orderList.model;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

public class OrderListService {
	private OrderListDAO_interface dao;
	
	public OrderListService() {
		dao=new OrderListDAO();
	}
	public OrderListVO addOrder(String memberID,String productID,Integer buyAmount,Integer orderPrice,Integer buyerScore,Integer vendorScore,String memberCreditcardNumber,String transactionMethod,Integer discount,Integer coinused,String buyerAddress,String orderStatus,String buyerName,String buyerPhone) {
		OrderListVO orderListVO=new OrderListVO();
		orderListVO.setMemberID(memberID);
		orderListVO.setProductID(productID);
		orderListVO.setBuyAmount(buyAmount);
		orderListVO.setOrderPrice(orderPrice);	
		orderListVO.setBuyerScore(buyerScore);
		orderListVO.setVendorScore(vendorScore);
		orderListVO.setMemberCreditcardNumber(memberCreditcardNumber);
		orderListVO.setTransactionMethod(transactionMethod);
		orderListVO.setDiscount(discount);
		orderListVO.setCoinused(coinused);
		orderListVO.setBuyerAddress(buyerAddress);
		orderListVO.setOrderStatus(orderStatus);
		orderListVO.setBuyerName(buyerName);
		orderListVO.setBuyerPhone(buyerPhone);
		dao.insert(orderListVO);
		
		return orderListVO;
	}
	public OrderListVO updateOrder(String orderID,String memberID,String productID,Integer buyAmount,Integer orderPrice,Timestamp orderDate,Integer buyerScore,Integer vendorScore,String memberCreditcardNumber,String transactionMethod,Integer discount,Integer coinused,String buyerAddress,String orderStatus,String buyerName,String buyerPhone) {
		OrderListVO orderListVO=new OrderListVO();
		orderListVO.setOrderID(orderID);
		orderListVO.setMemberID(memberID);
		orderListVO.setProductID(productID);
		orderListVO.setBuyAmount(buyAmount);
		orderListVO.setOrderPrice(orderPrice);
		orderListVO.setOrderDate(orderDate);
		orderListVO.setBuyerScore(buyerScore);
		orderListVO.setVendorScore(vendorScore);
		orderListVO.setMemberCreditcardNumber(memberCreditcardNumber);
		orderListVO.setTransactionMethod(transactionMethod);
		orderListVO.setDiscount(discount);
		orderListVO.setCoinused(coinused);
		orderListVO.setBuyerAddress(buyerAddress);
		orderListVO.setOrderStatus(orderStatus);
		orderListVO.setBuyerName(buyerName);
		orderListVO.setBuyerPhone(buyerPhone);
		dao.update(orderListVO);
		
		return orderListVO;	
	}
	public OrderListVO updateOrder(String orderID,Integer buyerScore) {
		OrderListVO orderListVO=new OrderListVO();
		orderListVO.setOrderID(orderID);
		orderListVO.setBuyerScore(buyerScore);
		dao.updateForBuyerScore(orderListVO);
		
		return orderListVO;	
	}
	public OrderListVO updateSellerOrder(String orderID,Integer vendorScore) {
		OrderListVO orderListVO=new OrderListVO();
		orderListVO.setOrderID(orderID);
		orderListVO.setVendorScore(vendorScore);
		dao.updateForSellerScore(orderListVO);
		
		return orderListVO;	
	}
	public OrderListVO updateOrderStatus(String orderStatus,String orderID) {
		OrderListVO orderListVO=new OrderListVO();
		orderListVO.setOrderID(orderID);
		orderListVO.setOrderStatus(orderStatus);
		dao.updateOrderStatus(orderListVO);
		
		return orderListVO;	
	}
	
	public OrderListVO getOneOrder(String orderID) {
		return dao.findByPrimaryKey(orderID);
	}
	
	public List<OrderListVO> getAll(){
		return dao.getAll();
	}
	public List<OrderListVO> getAllSeller(String memberID){
		return dao.getAll(memberID);
	}
	
	public List<OrderListVO> getAllBuyer(String memberID){
		return dao.findBySelfMemberID(memberID);
	}
	
}	
