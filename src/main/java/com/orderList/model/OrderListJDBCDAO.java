package com.orderList.model;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.product.model.ProductVO;


public class OrderListJDBCDAO implements OrderListDAO_interface {
	String driver = "oracle.jdbc.driver.OracleDriver";
	String url = "jdbc:oracle:thin:@localhost:1521:XE";
	String userid = "CA104G3";
	String password = "123456";
	
	private static final String INSERT_STMT =
			"insert into ORDERLIST(ORDERID,MEMBERID,PRODUCTID,BUYAMOUNT,ORDERPRICE,ORDERDATE,BUYERSCORE,VENDORSCORE,MEMBERCREDITCARDNUMBER,TRANSACTIONMETHOD, DISCOUNT, COINUSED, BUYERADDRESS, ORDERSTATUS)"
			+"values(to_char(sysdate,'yyyymmdd')||'-'||LPAD(to_char(ORDERLIST_seq.NEXTVAL), 6, '0'),?,?,?,?,?,?,?,?,?,?,?,?,?)";
	private static final String UPDATE =
			"UPDATE orderlist set memberid=?,productid=?,buyamount=?,orderprice=?,orderdate=?,buyerscore=?,vendorscore=?,membercreditcardnumber=?,transactionmethod=?,discount=?,coinused=?,buyeraddress=?,orderstatus=? where orderid=?";
	private static final String DELETE = 
			"DELETE FROM orderlist where orderid = ?";
	private static final String GET_ONE_STMT = 
			"select * from orderlist where orderid=? ";
	private static final String GET_ALL_STMT =
			"select * from orderlist order by orderid ";
	
	
	
	
	
	@Override
	public void insert(OrderListVO orderListVO) {
		Connection con = null;
		PreparedStatement pstmt = null;
		try {
			Class.forName(driver);
			con=DriverManager.getConnection(url,userid,password);
			pstmt = con.prepareStatement(INSERT_STMT);
			
			pstmt.setString(1, orderListVO.getMemberID());
			pstmt.setString(2, orderListVO.getProductID());
			pstmt.setInt(3, orderListVO.getBuyAmount());
			pstmt.setInt(4, orderListVO.getOrderPrice());
			pstmt.setTimestamp(5, orderListVO.getOrderDate());
			pstmt.setInt(6, orderListVO.getBuyerScore());
			pstmt.setInt(7, orderListVO.getVendorScore());
			pstmt.setString(8, orderListVO.getMemberCreditcardNumber());
			pstmt.setString(9, orderListVO.getTransactionMethod());
			pstmt.setInt(10, orderListVO.getDiscount());
			pstmt.setInt(11, orderListVO.getCoinused());
			pstmt.setString(12, orderListVO.getBuyerAddress());
			pstmt.setString(13, orderListVO.getOrderStatus());
			pstmt.executeUpdate();
			
		}catch(ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
		}catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		
	}

	@Override
	public void update(OrderListVO orderListVO) {
		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, password);
			pstmt = con.prepareStatement(UPDATE);

			pstmt.setString(1, orderListVO.getMemberID());
			pstmt.setString(2, orderListVO.getProductID());
			pstmt.setInt(3, orderListVO.getBuyAmount());
			pstmt.setInt(4, orderListVO.getOrderPrice());
			pstmt.setTimestamp(5, orderListVO.getOrderDate());
			pstmt.setInt(6, orderListVO.getBuyerScore());
			pstmt.setInt(7, orderListVO.getVendorScore());
			pstmt.setString(8, orderListVO.getMemberCreditcardNumber());
			pstmt.setString(9, orderListVO.getTransactionMethod());
			pstmt.setInt(10, orderListVO.getDiscount());
			pstmt.setInt(11, orderListVO.getCoinused());
			pstmt.setString(12, orderListVO.getBuyerAddress());
			pstmt.setString(13, orderListVO.getOrderStatus());
			pstmt.setString(14, orderListVO.getOrderID());

			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		
	}

	@Override
	public void delete(String orderid) {
		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, password);
			pstmt = con.prepareStatement(DELETE);

			pstmt.setString(1, orderid);

			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		
	}

	@Override
	public OrderListVO findByPrimaryKey(String orderID) {
		OrderListVO OrderListVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, password);
			pstmt = con.prepareStatement(GET_ONE_STMT);

			pstmt.setString(1, orderID);

			rs = pstmt.executeQuery();

			while (rs.next()) {
				OrderListVO=new OrderListVO();
				OrderListVO.setOrderID(rs.getString("orderid"));
				OrderListVO.setMemberID(rs.getString("memberid"));
				OrderListVO.setProductID(rs.getString("productid"));
				OrderListVO.setBuyAmount(rs.getInt("buyamount"));
				OrderListVO.setOrderPrice(rs.getInt("orderprice"));
				OrderListVO.setOrderDate(rs.getTimestamp("orderdate"));
				OrderListVO.setBuyerScore(rs.getInt("buyerscore"));
				OrderListVO.setVendorScore(rs.getInt("vendorscore"));
				OrderListVO.setMemberCreditcardNumber(rs.getString("membercreditcardnumber"));
				OrderListVO.setTransactionMethod(rs.getString("transactionmethod"));
				OrderListVO.setDiscount(rs.getInt("discount"));
				OrderListVO.setCoinused(rs.getInt("coinused"));
				OrderListVO.setBuyerAddress(rs.getString("buyeraddress"));
				OrderListVO.setOrderStatus(rs.getString("orderstatus"));
			}

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return OrderListVO;
	}

	@Override
	public List<OrderListVO> getAll() {
		List<OrderListVO> list = new ArrayList<OrderListVO>();
		OrderListVO OrderListVO=null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, password);
			pstmt = con.prepareStatement(GET_ALL_STMT);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				OrderListVO = new OrderListVO();
				OrderListVO.setOrderID(rs.getString("orderid"));
				OrderListVO.setMemberID(rs.getString("memberid"));
				OrderListVO.setProductID(rs.getString("productid"));
				OrderListVO.setBuyAmount(rs.getInt("buyamount"));
				OrderListVO.setOrderPrice(rs.getInt("orderprice"));
				OrderListVO.setOrderDate(rs.getTimestamp("orderdate"));
				OrderListVO.setBuyerScore(rs.getInt("buyerscore"));
				OrderListVO.setVendorScore(rs.getInt("vendorscore"));
				OrderListVO.setMemberCreditcardNumber(rs.getString("membercreditcardnumber"));
				OrderListVO.setTransactionMethod(rs.getString("transactionmethod"));
				OrderListVO.setDiscount(rs.getInt("discount"));
				OrderListVO.setCoinused(rs.getInt("coinused"));
				OrderListVO.setBuyerAddress(rs.getString("buyeraddress"));
				OrderListVO.setOrderStatus(rs.getString("orderstatus"));
				list.add(OrderListVO); // Store the row in the list
			}

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
//			throw new RuntimeException("A database error occured. "
//					+ se.getMessage());
			se.printStackTrace();
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return list;
	}
	public static void main(String[] args) {

		OrderListJDBCDAO dao=new OrderListJDBCDAO();
		
		//		1.�s�W
		
//		OrderListVO OrderListVO=new OrderListVO();
//		OrderListVO.setMemberID("M00008");
//		OrderListVO.setProductID("P00008");
//		OrderListVO.setBuyAmount(10);
//		OrderListVO.setOrderPrice(1000);
//		OrderListVO.setOrderDate(java.sql.Date.valueOf("2018-10-22"));
//		OrderListVO.setBuyerScore(100);
//		OrderListVO.setVendorScore(100);
//		OrderListVO.setMemberCreditcardNumber(100);
//		OrderListVO.setTransactionMethod("creditcard");
//		OrderListVO.setDiscount(50);
//		OrderListVO.setCoinused(50);
//		OrderListVO.setBuyerAddress("�]�߿�");
//		OrderListVO.setOrderStatus("paid-on");
//		dao.insert(OrderListVO);
//		System.out.println("�s�W���\!");
//		2.修改
		OrderListVO orderListVO=new OrderListVO();
		orderListVO.setOrderID("20181103-000005");
		orderListVO.setMemberID("M000001");
		orderListVO.setProductID("P000001");
		orderListVO.setBuyAmount(10);
		orderListVO.setOrderPrice(1000);
		orderListVO.setOrderDate(java.sql.Timestamp.valueOf("2018-10-22 20:55:55"));
		orderListVO.setBuyerScore(100);
		orderListVO.setVendorScore(100);
		orderListVO.setMemberCreditcardNumber("100");
		orderListVO.setTransactionMethod("creditcard");
		orderListVO.setDiscount(20);
		orderListVO.setCoinused(50);
		orderListVO.setBuyerAddress("苗栗縣竹南鎮大埔里公義路");
		orderListVO.setOrderStatus("paid-on");
		dao.update(orderListVO);
//		System.out.println("修改成功");
//		3.�R��
//		dao.delete("20181022-000006");
//		System.out.println("�R�����\!!!!!!!");
//		4.�d�߳�@
//		OrderListVO OrderListVO=dao.findByPrimaryKey("20181025-000002");
//		System.out.println(OrderListVO.getOrderID());
//		System.out.println(OrderListVO.getMemberID());
//		System.out.println(OrderListVO.getProductID());
//		System.out.println(OrderListVO.getBuyAmount());
//		System.out.println(OrderListVO.getOrderPrice());
//		System.out.println(OrderListVO.getOrderDate());
//		System.out.println(OrderListVO.getBuyerScore());
//		System.out.println(OrderListVO.getVendorScore());
//		System.out.println(OrderListVO.getMemberCreditcardNumber());
//		System.out.println(OrderListVO.getTransactionMethod());
//		System.out.println(OrderListVO.getDiscount());
//		System.out.println(OrderListVO.getCoinused());
//		System.out.println(OrderListVO.getBuyerAddress());
//		System.out.println(OrderListVO.getOrderStatus());
//		System.out.println("---------------------------------");
//		System.out.println("sucess!!!!!!");
//		5.�d����
//		List<OrderListVO> list=dao.getAll();
//		for(OrderListVO orderListVO: list) {
//			System.out.println(orderListVO.getOrderID());
//			System.out.println(orderListVO.getMemberID());
//			System.out.println(orderListVO.getProductID());
//			System.out.println(orderListVO.getBuyAmount());
//			System.out.println(orderListVO.getOrderPrice());
//			System.out.println(orderListVO.getOrderDate());
//			System.out.println(orderListVO.getBuyerScore());
//			System.out.println(orderListVO.getVendorScore());
//			System.out.println(orderListVO.getMemberCreditcardNumber());
//			System.out.println(orderListVO.getTransactionMethod());
//			System.out.println(orderListVO.getDiscount());
//			System.out.println(orderListVO.getCoinused());
//			System.out.println(orderListVO.getBuyerAddress());
//			System.out.println(orderListVO.getOrderStatus());
//			System.out.println("---------------------------------");
//		}	
//			System.out.println("sucessOK!!!!!");
	}

	@Override
	public List<OrderListVO> getAll(String memberID) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<OrderListVO> findBySelfMemberID(String memberID) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void updateForBuyerScore(OrderListVO orderListVO) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updateForSellerScore(OrderListVO orderListVO) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updateOrderStatus(OrderListVO orderListVO) {
		// TODO Auto-generated method stub
		
	}
}
	

