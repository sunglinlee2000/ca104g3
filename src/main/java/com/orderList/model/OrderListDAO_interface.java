package com.orderList.model;


import java.util.List;

import com.product.model.ProductVO;

public interface OrderListDAO_interface {
	public void insert(OrderListVO orderListVO);
    public void update(OrderListVO orderListVO);
    public void updateForBuyerScore(OrderListVO orderListVO);
    public void updateForSellerScore(OrderListVO orderListVO);
    public void updateOrderStatus(OrderListVO orderListVO);
    public void delete(String orderID);
    public OrderListVO findByPrimaryKey(String orderID);
    public List<OrderListVO> getAll();
    public List<OrderListVO> getAll(String memberID);
    public List<OrderListVO> findBySelfMemberID(String memberID);
}
