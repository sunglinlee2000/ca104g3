package com.orderList.controller;

import java.io.*;
import java.sql.Timestamp;
import java.util.*;

import javax.servlet.*;
import javax.servlet.http.*;

import org.json.JSONException;
import org.json.JSONObject;

import com.member.model.*;
import com.orderList.model.*;
import com.product.model.ProductVO;


public class OrderListServlet extends HttpServlet {

	public void doGet(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		doPost(req, res);
	}

	public void doPost(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {

		req.setCharacterEncoding("UTF-8");
		String action = req.getParameter("action");
		
		
		
		if ("getOne_For_Update".equals(action)) { 

		List<String> errorMsgs = new LinkedList<String>();
		// Store this set in the request scope, in case we need to
		// send the ErrorPage view.
		req.setAttribute("errorMsgs", errorMsgs);
		try {
			/***************************1.接收請求參數 - 輸入格式的錯誤處理**********************/
			String orderID = req.getParameter("orderID");
			
			/***************************2.開始查詢資料*****************************************/
			OrderListService odSvc=new OrderListService();
			OrderListVO orderListVO=odSvc.getOneOrder(orderID);
			if (orderID == null) {
				errorMsgs.add("查無資料");
			}
			// Send the use back to the form, if there were errors
			if (!errorMsgs.isEmpty()) {
				RequestDispatcher failureView = req
						.getRequestDispatcher("/front-end/Product/Seller.jsp");
				failureView.forward(req, res);
				return;//程式中斷
			}
			
			/***************************3.查詢完成,準備轉交(Send the Success view)*************/
			req.setAttribute("orderListVO", orderListVO); 
			String url = "/front-end/orderList/update_orderList_input.jsp";
			RequestDispatcher successView = req.getRequestDispatcher(url); // 
			successView.forward(req, res);

			/***************************其他可能的錯誤處理*************************************/
		} catch (Exception e) {
			errorMsgs.add("無法取得資料:" + e.getMessage());
			RequestDispatcher failureView = req
					.getRequestDispatcher("/front-end/Product/Seller.jsp");
			failureView.forward(req, res);
		}
	}
		
		
		if ("update".equals(action)) { // 來自addEmp.jsp的請求  
					System.out.println("進入update");
					Map<String,String> errorMsgs = new LinkedHashMap<String,String>();
					req.setAttribute("errorMsgs", errorMsgs);
		
					try {
						/***********************1.接收請求參數 - 輸入格式的錯誤處理*************************/
						String orderID = req.getParameter("orderID");
						System.out.println(orderID);
						if (orderID == null || orderID.trim().length() == 0) {
							errorMsgs.put("orderID","訂單編號: 請勿空白");
						}
						System.out.println("1");
						String memberID = req.getParameter("memberID").trim();
						System.out.println("2");
						if (memberID == null || memberID.trim().length() == 0) {
							errorMsgs.put("memberID","會員編號請勿空白");
						}
						String productID = req.getParameter("productID").trim();
						if (productID == null || productID.trim().length() == 0) {
							errorMsgs.put("productID","商品編號請勿空白");
						}
						Integer buyAmount = null;
						try {
							buyAmount = new Integer(req.getParameter("buyAmount").trim());
						} catch (NumberFormatException e) {
							errorMsgs.put("buyAmount","購買數量請填數字");
						}
						System.out.println("2");
						Integer orderPrice = null;
						try {
							orderPrice = new Integer(req.getParameter("orderPrice").trim());
						} catch (NumberFormatException e) {
							errorMsgs.put("orderPrice","購買價格請填數字");
						}
						Timestamp orderDate = null;
						try {
							orderDate = java.sql.Timestamp.valueOf(req.getParameter("orderDate").trim());
						} catch (IllegalArgumentException e) {
							errorMsgs.put("orderDate","請輸入日期");
						}
						System.out.println("3");
						Integer buyerScore = null;
						try {
							buyerScore = new Integer(req.getParameter("buyerScore").trim());
						} catch (NumberFormatException e) {
							errorMsgs.put("buyerScore","買家評價請填數字");
						}
						Integer vendorScore = null;
						try {
							vendorScore = new Integer(req.getParameter("vendorScore").trim());
						} catch (NumberFormatException e) {
							errorMsgs.put("vendorScore","賣家請填數字");
						}
						String memberCreditcardNumber = req.getParameter("memberCreditcardNumber").trim();
						if (memberCreditcardNumber == null || memberCreditcardNumber.trim().length() == 0) {
							errorMsgs.put("memberCreditcardNumber","信用卡號碼請勿空白");
						}
						String transactionMethod = req.getParameter("transactionMethod").trim();
						if (transactionMethod == null || transactionMethod.trim().length() == 0) {
							errorMsgs.put("transactionMethod","交易方式請勿空白");
						}
						Integer discount = null;
						try {
							discount = new Integer(req.getParameter("discount").trim());
						} catch (NumberFormatException e) {
							errorMsgs.put("discount","折扣請填數字");
						}
						Integer coinused = null;
						try {
							coinused = new Integer(req.getParameter("coinused").trim());
						} catch (NumberFormatException e) {
							errorMsgs.put("coinused","賣家請填數字");
						}
						String buyerAddress = req.getParameter("buyerAddress").trim();
						if (buyerAddress == null || buyerAddress.trim().length() == 0) {
							errorMsgs.put("buyerAddress","地址請勿空白");
						}
						String orderStatus = req.getParameter("orderStatus").trim();
						if (orderStatus == null || orderStatus.trim().length() == 0) {
							errorMsgs.put("orderStatus","交易狀態請勿空白");
						}
						String buyerName = req.getParameter("buyerName").trim();
						if (buyerName == null || buyerName.trim().length() == 0) {
							errorMsgs.put("buyerName","購買者請勿空白");
						}
						String buyerPhone = req.getParameter("buyerPhone").trim();
						if (buyerPhone == null || buyerPhone.trim().length() == 0) {
							errorMsgs.put("buyerPhone","購買者電話請勿空白");
						}
						System.out.println("BUGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGG");
						// Send the use back to the form, if there were errors
						if (!errorMsgs.isEmpty()) {
							RequestDispatcher failureView = req
									.getRequestDispatcher("/front-end/orderList/update_orderList_input.jsp");
							failureView.forward(req, res);
							return;
						}
						
						System.out.println(orderID);
						/***************************2.開始修改資料***************************************/
						System.out.println("show");
						OrderListVO orderListVO=new OrderListVO();
						OrderListService ordSvc=new OrderListService();
						System.out.println("fffff");
						orderListVO=ordSvc.updateOrder(orderID, memberID, productID, buyAmount, orderPrice, orderDate, buyerScore, vendorScore, memberCreditcardNumber, transactionMethod, discount, coinused, buyerAddress, orderStatus,buyerName,buyerPhone);
						System.out.println("修改成功!");
						/***************************3.新增完成,準備轉交(Send the Success view)***********/
						req.setAttribute("orderListVO", orderListVO);
						String url = "/front-end/product/Seller.jsp";
						
						RequestDispatcher successView = req.getRequestDispatcher(url); 
						successView.forward(req, res);
						System.out.println("轉交成功!");
						
						/***************************其他可能的錯誤處理**********************************/
					} catch (Exception e) {
						errorMsgs.put("Exception",e.getMessage());
						RequestDispatcher failureView = req
								.getRequestDispatcher("/front-end/orderList/update_orderList_input.jsp");
						failureView.forward(req, res);
						
						
					}
				}

        if ("insert".equals(action)) { 
        	HttpSession session = req.getSession();
			System.out.println(action);
        	List<String> errorMsgs = new LinkedList<String>();
			req.setAttribute("errorMsgs", errorMsgs);
			Vector<ProductVO> buylist = (Vector<ProductVO>) session.getAttribute("shoppingcart");
			
			try {
				/***********************1.接收請求參數 - 輸入格式的錯誤處理*************************/
				StringBuffer buyerAddress=new StringBuffer();    //買家地址
				
				String add1=req.getParameter("county");
				System.out.println("ffff"+add1);
				if (add1 == null || add1.trim().length() == 0) {
					errorMsgs.add("縣市請選取");
				}else {
					buyerAddress.append(add1);
				}
				String add2=req.getParameter("district");
				if (add2 == null || add2.trim().length() == 0) {
					errorMsgs.add("區/鄉/鎮請選取");
				}else {
					buyerAddress.append(" "+add2);
				}
				
				String add4=req.getParameter("add4");
				if (add4 == null || add4.trim().length() == 0) {
					errorMsgs.add("詳細地址請輸入");
				}else {
					buyerAddress.append(" "+add4);
				}
				String buyerAddress1=new String(buyerAddress);
				
				
				StringBuffer memberCreditcardNumber =new StringBuffer();  //信用卡
				String credit1=req.getParameter("credit1");
				if (credit1 == null || credit1.trim().length() == 0) {
					errorMsgs.add("信用卡號碼輸入不完整");
				}else {
					memberCreditcardNumber.append(credit1);
				}
				String credit2=req.getParameter("credit2");
				if (credit2 == null || credit2.trim().length() == 0) {
					errorMsgs.add("信用卡號碼輸入不完整");
				}else {
					memberCreditcardNumber.append(" "+credit2);
				}
				String credit3=req.getParameter("credit3");
				if (credit3 == null || credit3.trim().length() == 0) {
					errorMsgs.add("信用卡號碼輸入不完整");
				}else {
					memberCreditcardNumber.append(" "+credit3);
				}
				String credit4=req.getParameter("credit4");
				if (credit4 == null || credit4.trim().length() == 0) {
					errorMsgs.add("信用卡號碼輸入不完整");
				}else {
					memberCreditcardNumber.append(" "+credit4);
				}
				
				String memberCreditcardNumber1=new String(memberCreditcardNumber);
				
				String transactionMethod=req.getParameter("transactionMethod");//交易方式  寫死在checkout.jsp 目前:CREDITCARD
				
				String buyerName=req.getParameter("buyerName");					//買家姓名
				if (buyerName == null || buyerName.trim().length() == 0) {
					errorMsgs.add("請輸入買家姓名");
				}
				String buyerPhone=req.getParameter("buyerPhone");				//買家電話
				if (buyerPhone == null || buyerPhone.trim().length() == 0) {
					errorMsgs.add("請輸入買家電話");
				}
				
				String orderStatus=req.getParameter("orderStatus");				//訂單狀態 寫死目前在checkout.jsp 目前都 PAID
				System.out.println("1");
				
				Integer buyerScore=0;
				System.out.println("2");
				Integer vendorScore=0;
				System.out.println("3");
				Integer discount=0;
				Integer coinused=0;
				
				System.out.println("ffffff");
				// Send the use back to the form, if there were errors
				if (!errorMsgs.isEmpty()) {
					RequestDispatcher failureView = req
							.getRequestDispatcher("/front-end/product/Checkout.jsp");
					failureView.forward(req, res);
					System.out.println("666666");
					return;
				}
				
				/***************************2.開始新增資料***************************************/
				for (int i = 0; i < buylist.size(); i++) {
					ProductVO productVO = buylist.get(i);
					
					String productID = productVO.getProductID();			//商品編號
					String productName = productVO.getProductName();		//商品名稱
					String memberID = productVO.getMemberID();				//買家編號
					Integer productPrice = productVO.getProductPrice();		//商品價格
					Integer buyAmount = productVO.getQuantity();			//商品數量
					Integer orderPrice=productPrice*buyAmount;				//訂單金額
				
					OrderListVO orderListVO=new OrderListVO();
					OrderListService ordSvc=new OrderListService();
					orderListVO=ordSvc.addOrder(memberID, productID, buyAmount, orderPrice, buyerScore, vendorScore, memberCreditcardNumber1, transactionMethod, discount, coinused, buyerAddress1, orderStatus, buyerName, buyerPhone);
				
				}
				
				/***************************3.新增完成,準備轉交(Send the Success view)***********/
				
				session.removeAttribute("shoppingcart");
				String url = "/front-end/orderList/buyerOrder.jsp";
				res.sendRedirect(req.getContextPath()+url);
				
				
//				JSONObject obj = new JSONObject(); 
//				Boolean success=true;
//				try { 
//					obj.put("success", success);
//					System.out.println(success);
//				} catch (JSONException e) { 
//					e.printStackTrace(); 
//				} 
//				res.setContentType("text/plain"); 
//				res.setCharacterEncoding("UTF-8"); 
//				PrintWriter out = res.getWriter(); 
//				out.write(obj.toString()); 
//				out.flush(); 
//				out.close();				
				
				/***************************其他可能的錯誤處理**********************************/
			} catch (Exception e) {
				errorMsgs.add(e.getMessage());
				RequestDispatcher failureView = req
						.getRequestDispatcher("/front-end/product/Checkout.jsp");
				failureView.forward(req, res);
			}
		}
        
        if("addStar".equals(action)) {
        	//1.取的請求參數
        	String orderID=req.getParameter("orderID");
        	Integer buyerScore=new Integer(req.getParameter("buyerScore"));
        	String memberID=req.getParameter("memberID");
        	Integer memberCoin=10;
        	System.out.println(memberID);
        	//2.修改開始
        	OrderListService ordSvc=new OrderListService();
        	ordSvc.updateOrder(orderID, buyerScore);
        	System.out.println("評分修改成功!!!");
        	//取得原本的memberCoin 再修改
        	MemberService memSvc=new MemberService();
        	MemberVO memberVO1=new MemberVO();
        	memberVO1=memSvc.getCoin(memberID);
        	Integer memberCoin1=new Integer(memberVO1.getMemberCoin());
        	
        	memSvc.updateMemberCoin(memberCoin+memberCoin1, memberID);
        	System.out.println("得代幣成功!!!");
        	//3.移除member session 再裝起來 這樣session中的資料 才能及時跟新
        	HttpSession session = req.getSession();
        	MemberVO memberVO=new MemberVO();
        	System.out.println(memberID);
        	memberVO=memSvc.getOneMember(memberID);
        	System.out.println(memberVO);
        	session.setAttribute("memberVO", memberVO);
        	
        	
        	//4.回傳參數
        	JSONObject obj = new JSONObject(); 
        	
			try { 
				obj.put("buyerScore", buyerScore);
				System.out.println(buyerScore);
			} catch (JSONException e) { 
				e.printStackTrace(); 
			} 
			res.setContentType("text/plain"); 
			res.setCharacterEncoding("UTF-8"); 
			PrintWriter out = res.getWriter(); 
			out.write(obj.toString()); 
			out.flush(); 
			out.close();
        	
        	
        	
        }
        
        if("addStar2".equals(action)) {
        	//1.取的請求參數
        	String orderID=req.getParameter("orderID");
        	Integer vendorScore=new Integer(req.getParameter("vendorScore"));
        	String memberID=req.getParameter("memberID");
        	Integer memberCoin=10;
        	System.out.println(memberID);
        	//2.修改開始
        	OrderListService ordSvc=new OrderListService();
        	ordSvc.updateSellerOrder(orderID, vendorScore);
        	System.out.println("評分修改成功!!!");
        	//取得原本的memberCoin 再修改
        	MemberService memSvc=new MemberService();
        	MemberVO memberVO1=new MemberVO();
        	memberVO1=memSvc.getCoin(memberID);
        	Integer memberCoin1=new Integer(memberVO1.getMemberCoin());
        	
        	memSvc.updateMemberCoin(memberCoin+memberCoin1, memberID);
        	System.out.println("得代幣成功!!!");
        	//3.移除member session 再裝起來 這樣session中的資料 才能及時跟新
        	HttpSession session = req.getSession();
        	MemberVO memberVO=new MemberVO();
        	System.out.println(memberID);
        	memberVO=memSvc.getOneMember(memberID);
        	System.out.println(memberVO);
        	session.setAttribute("memberVO", memberVO);
        	
        	
        	//4.回傳參數
        	JSONObject obj = new JSONObject(); 
        	
			try { 
				obj.put("vendorScore", vendorScore);
				System.out.println(vendorScore);
			} catch (JSONException e) { 
				e.printStackTrace(); 
			} 
			res.setContentType("text/plain"); 
			res.setCharacterEncoding("UTF-8"); 
			PrintWriter out = res.getWriter(); 
			out.write(obj.toString()); 
			out.flush(); 
			out.close();
        	
        	
        	
        }
        
        if("update_Order_Status".equals(action)) {
        	//1.接受請求參數
        	String orderID=req.getParameter("orderID");
        	String orderStatus=req.getParameter("orderStatus");
        	
        	//2.執行跟改狀態
        	OrderListService orderSvc=new OrderListService();
        	orderSvc.updateOrderStatus(orderStatus, orderID);
        	System.out.println("修改動態成功");
        	
        	JSONObject obj = new JSONObject(); 
        	
			
			res.setContentType("text/plain"); 
			res.setCharacterEncoding("UTF-8"); 
			PrintWriter out = res.getWriter(); 
			out.write(obj.toString()); 
			out.flush(); 
			out.close();
        	
        	
        	
        }

	}
}
