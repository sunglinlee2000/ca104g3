package com.commentList.controller;

import java.io.*;
import java.sql.Timestamp;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

import javax.servlet.*;
import javax.servlet.http.*;

import com.commentList.model.*;
import com.issue.model.IssueService;
import com.issue.model.IssueVO;
import com.issueReport.model.IssueReportVO;
import com.member.model.MemberService;
import com.notificationList.model.NotificationListService;

public class CommentListServlet extends HttpServlet {

	public void doGet(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		doPost(req, res);
	}

	public void doPost(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {

		req.setCharacterEncoding("UTF-8");
		String action = req.getParameter("action");
		
		HttpSession session = req.getSession();

		
		
		if ("getOne_For_Display".equals(action)) { // 來自select_page.jsp的請求

			List<String> errorMsgs = new LinkedList<String>();
			// Store this set in the request scope, in case we need to
			// send the ErrorPage view.
			req.setAttribute("errorMsgs", errorMsgs);

			try {
				/***************************1.接收請求參數 - 輸入格式的錯誤處理**********************/
				String commentID = req.getParameter("commentID");
//				if (commentID == null || (commentID.trim()).length() == 0) {
//					errorMsgs.add("請輸入留言編號");
//				}
				// Send the use back to the form, if there were errors
				if (!errorMsgs.isEmpty()) {
					RequestDispatcher failureView = req
							.getRequestDispatcher("/front-end/commentList/select_page.jsp");
					failureView.forward(req, res);
					return;//程式中斷
				}
				
//				String commentID = null;
//				try {
//					commentID = new String(commentID);
//				} catch (Exception e) {
//					errorMsgs.add("員工編號格式不正確");
//				}
				// Send the use back to the form, if there were errors
				if (!errorMsgs.isEmpty()) {
					RequestDispatcher failureView = req
							.getRequestDispatcher("/emp/select_page.jsp");
					failureView.forward(req, res);
					return;//程式中斷
				}
				
				/***************************2.開始查詢資料*****************************************/
				CommentListService commentListSvc = new CommentListService();
				CommentListVO commentListVO = commentListSvc.getOneComment(commentID);
				if (commentListVO == null) {
					errorMsgs.add("查無資料");
				}
				// Send the use back to the form, if there were errors
				if (!errorMsgs.isEmpty()) {
					RequestDispatcher failureView = req
							.getRequestDispatcher("/front-end/commentList/select_page.jsp");
					failureView.forward(req, res);
					return;//程式中斷
				}
				
				/***************************3.查詢完成,準備轉交(Send the Success view)*************/
				req.setAttribute("commentListVO", commentListVO); // 資料庫取出的empVO物件,存入req
				String url = "/front-end/commentList/listOneEmp.jsp";
				RequestDispatcher successView = req.getRequestDispatcher(url); // 成功轉交 listOneEmp.jsp
				successView.forward(req, res);

				/***************************其他可能的錯誤處理*************************************/
			} catch (Exception e) {
				errorMsgs.add("無法取得資料:" + e.getMessage());
				RequestDispatcher failureView = req
						.getRequestDispatcher("/front-end/commentList/select_page.jsp");
				failureView.forward(req, res);
			}
		}
		
		
		if ("getOne_For_Update".equals(action)) { // 來自listAllEmp.jsp的請求
			
			List<String> errorMsgs = new LinkedList<String>();
			// Store this set in the request scope, in case we need to
			// send the ErrorPage view.
			req.setAttribute("errorMsgs", errorMsgs);
		
			try {
				/***************************1.接收請求參數****************************************/
				String commentID = new String(req.getParameter("commentID"));
			
				/***************************2.開始查詢資料****************************************/
				CommentListService commentListSvc = new CommentListService();
				
				CommentListVO commentListVO = commentListSvc.getOneComment(commentID);
							
				/***************************3.查詢完成,準備轉交(Send the Success view)************/
				req.setAttribute("commentListVO", commentListVO);         // 資料庫取出的empVO物件,存入req
				String url = "/front-end/commentList/update_emp_input.jsp";
				RequestDispatcher successView = req.getRequestDispatcher(url);// 成功轉交 update_emp_input.jsp
				successView.forward(req, res);

				/***************************其他可能的錯誤處理**********************************/
			} catch (Exception e) {
				errorMsgs.add("無法取得要修改的資料:" + e.getMessage());
				RequestDispatcher failureView = req
						.getRequestDispatcher("/front-end/commentList/listAllEmp.jsp");
				failureView.forward(req, res);
			}
		}
		
		
		if ("update".equals(action)) { // 來自update_emp_input.jsp的請求
			
			List<String> errorMsgs = new LinkedList<String>();
			// Store this set in the request scope, in case we need to
			// send the ErrorPage view.
			req.setAttribute("errorMsgs", errorMsgs);
		
			try {
				/***************************1.接收請求參數 - 輸入格式的錯誤處理**********************/
				
				String commentID = new String(req.getParameter("commentID").trim());																
				String memberID = req.getParameter("memberID");
				String issueID = req.getParameter("issueID");
				String gifID = req.getParameter("gifID");
				String commentContent = req.getParameter("commentContent");
				String commentDate = req.getParameter("commentDate");
								
//				if (commentContent == null || commentContent.trim().length() == 0) {
//					errorMsgs.add("內容請勿空白");
//				}	
			
				CommentListVO commentListVO = new CommentListVO();
				commentListVO.setCommentID(commentID);
				commentListVO.setMemberID(memberID);
				commentListVO.setIssueID(issueID);
				commentListVO.setGifID(gifID);
				commentListVO.setCommentContent(commentContent);
				Timestamp time = new Timestamp(System.currentTimeMillis());
				commentListVO.setCommentDate(java.sql.Timestamp.valueOf(ZonedDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"))));

				// Send the use back to the form, if there were errors
				if (!errorMsgs.isEmpty()) {
					req.setAttribute("commentListVO", commentListVO); // 含有輸入格式錯誤的empVO物件,也存入req
					RequestDispatcher failureView = req
							.getRequestDispatcher("/front-end/issue/issue_detailed.jsp");
					failureView.forward(req, res);
					return; //程式中斷
				}
				
				/***************************2.開始修改資料*****************************************/
				CommentListService commentListSvc = new CommentListService();
				commentListVO = commentListSvc.updateComment(commentID, memberID, issueID, gifID, commentContent,time);
				/***************************3.修改完成,準備轉交(Send the Success view)*************/
				req.setAttribute("commentListVO", commentListVO); // 資料庫update成功後,正確的的empVO物件,存入req
				String url = "/front-end/issue/issue_detailed.jsp";
				RequestDispatcher successView = req.getRequestDispatcher(url); // 修改成功後,轉交listOneEmp.jsp
				System.out.println("111154645645");
				successView.forward(req, res);

				/***************************其他可能的錯誤處理*************************************/
			} catch (Exception e) {
				errorMsgs.add("修改資料失敗:"+e.getMessage());
				RequestDispatcher failureView = req
						.getRequestDispatcher("/front-end/issue/issue_detailed.jsp");
				failureView.forward(req, res);
			}
		}

        if ("insert".equals(action)) { // 來自addEmp.jsp的請求  
			
			List<String> errorMsgs = new LinkedList<String>();
			// Store this set in the request scope, in case we need to
			// send the ErrorPage view.
			req.setAttribute("errorMsgs", errorMsgs);

			try {
				/***********************1.接收請求參數 - 輸入格式的錯誤處理*************************/

				String commentContent = req.getParameter("commentContent");
				if (commentContent == null || commentContent.trim().length() == 0) {
					errorMsgs.add("留言請勿空白");
				}
				String memberID = req.getParameter("memberID");
//				String memberID = (String)session.getAttribute("memberID");//取回會員編號，動作完回到專版話題列表
				
				String issueID = req.getParameter("issueID");
//				String issueID = (String)session.getAttribute("issueID");//取回話題編號，動作完回到專版話題列表
				
				
				CommentListVO commentListVO = new CommentListVO();
				commentListVO.setMemberID(memberID);
				commentListVO.setIssueID(issueID);
				commentListVO.setGifID("GIF000001");
				commentListVO.setCommentContent(commentContent);
				Timestamp time = new Timestamp(System.currentTimeMillis());
				commentListVO.setCommentDate(java.sql.Timestamp.valueOf(ZonedDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"))));
				
				IssueService issueSvc = new IssueService();
				IssueVO issueVO = issueSvc.getOneIssue(issueID);

				// Send the use back to the form, if there were errors
				if (!errorMsgs.isEmpty()) {
					req.setAttribute("commentListVO", commentListVO); // 含有輸入格式錯誤的empVO物件,也存入req
					RequestDispatcher failureView = req
							.getRequestDispatcher("/front-end/issue/issue_detailed.jsp");
					failureView.forward(req, res);
					return;
				}
				/***************************2.開始新增資料***************************************/
				CommentListService commentListSvc = new CommentListService();
				commentListVO = commentListSvc.addComment(memberID, issueID, "GIF000001", commentContent, time);

				req.setAttribute("issueVO", issueVO);
				
				
				IssueVO issueVO2 = issueSvc.getOneIssue(memberID);
				String memberID1 = issueVO.getMemberID();
//				新增通知的宣告				
				String theOtherMemberID = req.getParameter("memberID");
//				開始新增通知	
				String memberNickName = new MemberService().getOneMember(memberID1).getMemberNickName();
				String notificationContent = memberNickName + "對您的文章留言";
				Timestamp now = new Timestamp(new Date().getTime());
				NotificationListService notificationListSvc = new NotificationListService();
				notificationListSvc.addNoList("NT000001", memberID1, notificationContent, now, issueID, "FALSE", 
						"/issue/issue.do?action=getOne_For_Display&issueID=");
				
				/***************************3.新增完成,準備轉交(Send the Success view)***********/
//				req.setAttribute("issueIDxx",issueID);//利用取到的issueID返回列表
				String url = "/front-end/issue/issue_detailed.jsp";
				RequestDispatcher successView = req.getRequestDispatcher(url); // 新增成功後轉交listAllEmp.jsp
				successView.forward(req, res);				
				
				/***************************其他可能的錯誤處理**********************************/
			} catch (Exception e) {
				errorMsgs.add(e.getMessage());
				RequestDispatcher failureView = req
						.getRequestDispatcher("/front-end/commentList/addEmp.jsp");
				failureView.forward(req, res);
			}
		}
		
		
		if ("delete".equals(action)) { // 來自listAllEmp.jsp

			List<String> errorMsgs = new LinkedList<String>();
			// Store this set in the request scope, in case we need to
			// send the ErrorPage view.
			req.setAttribute("errorMsgs", errorMsgs);
	
			try {
				/***************************1.接收請求參數***************************************/
				String commentID = new String(req.getParameter("commentID"));
				
				/***************************2.開始刪除資料***************************************/
				CommentListService commentListSvc = new CommentListService();
				commentListSvc.deleteComment(commentID);
				
				/***************************3.刪除完成,準備轉交(Send the Success view)***********/								
				String url = "/front-end/issue/issue_detailed.jsp";
				RequestDispatcher successView = req.getRequestDispatcher(url); // 新增成功後轉交listAllEmp.jsp
				successView.forward(req, res);
				
				/***************************其他可能的錯誤處理**********************************/
			} catch (Exception e) {
				errorMsgs.add("刪除資料失敗:"+e.getMessage());
				RequestDispatcher failureView = req
						.getRequestDispatcher("/front-end/commentList/listAllEmp.jsp");
				failureView.forward(req, res);
			}
		}
	}
}
