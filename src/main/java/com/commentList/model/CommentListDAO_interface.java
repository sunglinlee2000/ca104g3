package com.commentList.model;

import java.util.List;

public interface CommentListDAO_interface {
	public void insert(CommentListVO commentListVO);
    public void update(CommentListVO commentListVO);
    public void delete(String commentListVO);
    public CommentListVO findByPrimaryKey(String commentID);
    public List<CommentListVO> getAll();
    public void deleteByIssueID(String issueID);
    public List<CommentListVO> getAllByIssueID (String issueID);
}
