package com.commentList.model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import com.commentList.model.CommentListVO;
import com.issue.model.IssueVO;

public class CommentListDAO implements CommentListDAO_interface {
	private static DataSource ds = null;
	static {
		try {
			Context ctx = new InitialContext();
			ds = (DataSource) ctx.lookup("java:comp/env/jdbc/CA104G3DB");
		} catch (NamingException e) {
			e.printStackTrace();
		}
	}
		
	private static final String INSERT_STMT =
			"INSERT INTO commentList "
			+ "(commentID,memberID,issueID,gifID,commentContent,commentDate)"
			+ " VALUES "
			+ "('C'||LPAD(to_char(COMMENTLIST_seq.NEXTVAL), 6, '0'),?,?,?,?,?)";
		private static final String GET_ALL_STMT = 
			"SELECT commentID,memberID,issueID,gifID,commentContent,commentDate FROM commentList order by commentID DESC";
		private static final String GET_ONE_STMT = 
			"SELECT commentID,memberID,issueID,gifID,commentContent,commentDate FROM commentList where commentID = ?";
		private static final String DELETE = 
			"DELETE FROM commentList where commentID = ?";
		private static final String UPDATE = 
			"UPDATE commentList set gifID=?, commentContent=? where commentID = ?";		
		private static final String DELETEBYISSUEID = 
				"DELETE FROM commentList where issueID = ?";
		private static final String GET_ALL_STMT_BY_ISSUEID = 
				"select * from commentList where issueID = ? order by commentID DESC";
	
	@Override
	public void insert(CommentListVO commentListVO) {
		Connection con = null;
		PreparedStatement pstmt = null;
		
		try {
			con = ds.getConnection();
			pstmt = con.prepareStatement(INSERT_STMT);
			
			pstmt.setString(1, commentListVO.getMemberID());
			pstmt.setString(2, commentListVO.getIssueID());
			pstmt.setString(3, commentListVO.getGifID());
			pstmt.setString(4, commentListVO.getCommentContent());
			pstmt.setTimestamp(5, commentListVO.getCommentDate());
			
			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}

	@Override
	public void update(CommentListVO commentListVO) {
		Connection con = null;
		PreparedStatement pstmt = null;		
		
		try {
			
			con = ds.getConnection();
			pstmt = con.prepareStatement(UPDATE);
			
			pstmt.setString(1, commentListVO.getGifID());
			pstmt.setString(2, commentListVO.getCommentContent());	
			pstmt.setString(3, commentListVO.getCommentID());
			
			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

		
	}
	@Override
	public void delete(String commentID) {
		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(DELETE);
			
			pstmt.setString(1, commentID);
			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

		
	}

	@Override
	public CommentListVO findByPrimaryKey(String commentID) {
		CommentListVO commentListVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ONE_STMT);

			pstmt.setString(1, commentID);

			rs = pstmt.executeQuery();

			while (rs.next()) {

		
				commentListVO = new CommentListVO();
				commentListVO.setCommentID(rs.getString("commentID"));
				commentListVO.setMemberID(rs.getString("memberID"));
				commentListVO.setIssueID(rs.getString("issueID"));
				commentListVO.setGifID(rs.getString("gifID"));
				commentListVO.setCommentContent(rs.getString("commentContent"));
				commentListVO.setCommentDate(rs.getTimestamp("commentDate"));
			
			}

			// Handle any driver errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return commentListVO;
	}

	@Override
	public List<CommentListVO> getAll() {
		List<CommentListVO> list = new ArrayList<CommentListVO>();
		CommentListVO commentListVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ALL_STMT);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				// empVO �]�٬� Domain objects
				
				commentListVO = new CommentListVO();				
				commentListVO.setCommentID(rs.getString("commentID"));
				commentListVO.setMemberID(rs.getString("memberID"));
				commentListVO.setIssueID(rs.getString("issueID"));
				commentListVO.setGifID(rs.getString("gifID"));
				commentListVO.setCommentContent(rs.getString("commentContent"));
				commentListVO.setCommentDate(rs.getTimestamp("commentDate"));
				
				
				list.add(commentListVO); // Store the row in the list
			}

			// Handle any driver errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return list;
	}
	
	@Override
	public void deleteByIssueID(String issueID) {
		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(DELETEBYISSUEID);

			pstmt.setString(1, issueID);
			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}	
	}
	
	@Override
	public List<CommentListVO> getAllByIssueID(String issueID) {
		List<CommentListVO> commentlist = new ArrayList<CommentListVO>();
		CommentListVO commentListVO = null;
		
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs =null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ALL_STMT_BY_ISSUEID);
			pstmt.setString(1, issueID);
			rs = pstmt.executeQuery();
			
			while (rs.next()) {
				commentListVO = new CommentListVO();
				commentListVO.setIssueID(rs.getString("issueID"));
				commentListVO.setCommentID(rs.getString("commentID"));
				commentListVO.setMemberID(rs.getString("memberID"));
				commentListVO.setGifID(rs.getString("gifID"));
				commentListVO.setCommentContent(rs.getString("commentContent"));
				commentListVO.setCommentDate(rs.getTimestamp("commentDate"));
				commentlist.add(commentListVO);
			}
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. " + se.getMessage());
			
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

		return commentlist;
	}
}
