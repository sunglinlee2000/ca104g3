package com.commentList.model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import com.issue.model.IssueVO;

public class CommentListJDBCDAO implements CommentListDAO_interface {
	String driver = "oracle.jdbc.driver.OracleDriver";
	String url = "jdbc:oracle:thin:@localhost:1521:XE";
	String userid = "CA104G3";
	String passwd = "123456";
	
	private static final String INSERT_STMT =
			"INSERT INTO commentlist "
			+ "(commentID,memberID,issueID,gifID,commentContent,commentDate)"
			+ " VALUES "
			+ "('C'||LPAD(to_char(COMMENTLIST_seq.NEXTVAL), 6, '0'),?,?,?,?,?)";
		private static final String GET_ALL_STMT = 
			"SELECT * FROM commentList order by commentID DESC";
		private static final String GET_ONE_STMT = 
			"SELECT commentID,memberID,issueID,gifID,commentContent,commentDate FROM commentList where commentID = ?";
		private static final String DELETE = 
			"DELETE FROM commentList where commentID = ?";
		private static final String UPDATE = 
			"UPDATE commentList set gifID=?, commentContent=? where commentID = ?";	
		private static final String DELETEBYISSUEID = 
				"DELETE FROM commentList where issueID = ?";
		private static final String GET_ALL_STMT_BY_ISSUEID = 
				"select * from commentList where issueID = ? order by commentID DESC";
	
	@Override
	public void insert(CommentListVO commentListVO) {
		Connection con = null;
		PreparedStatement pstmt = null;
		
		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(INSERT_STMT);
			
			
			pstmt.setString(1, commentListVO.getMemberID());
			pstmt.setString(2, commentListVO.getIssueID());
			pstmt.setString(3, commentListVO.getGifID());
			pstmt.setString(4, commentListVO.getCommentContent());
			pstmt.setTimestamp(5, commentListVO.getCommentDate());
			
			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}	
	}

	@Override
	public void update(CommentListVO commentListVO) {
		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(UPDATE);

			pstmt.setString(1, commentListVO.getGifID());
			pstmt.setString(2, commentListVO.getCommentContent());	
			pstmt.setString(3, commentListVO.getCommentID());
			
			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		
	}

	@Override
	public void delete(String commentID) {
		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(DELETE);

			pstmt.setString(1, commentID);
			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
	}

	@Override
	public CommentListVO findByPrimaryKey(String commentID) {
		CommentListVO commentListVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(GET_ONE_STMT);

			pstmt.setString(1, commentID);

			rs = pstmt.executeQuery();

			while (rs.next()) {

		
				commentListVO = new CommentListVO();
				commentListVO.setCommentID(rs.getString("commentID"));
				commentListVO.setMemberID(rs.getString("memberID"));
				commentListVO.setIssueID(rs.getString("issueID"));
				commentListVO.setGifID(rs.getString("gifID"));
				commentListVO.setCommentContent(rs.getString("commentContent"));
				commentListVO.setCommentDate(rs.getTimestamp("commentDate"));
			
			}

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return commentListVO;
	}

	@Override
	public List<CommentListVO> getAll() {
		List<CommentListVO> list = new ArrayList<CommentListVO>();
		CommentListVO commentListVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(GET_ALL_STMT);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				// empVO �]�٬� Domain objects
				
				commentListVO = new CommentListVO();				
				commentListVO.setCommentID(rs.getString("commentID"));
				commentListVO.setMemberID(rs.getString("memberID"));
				commentListVO.setIssueID(rs.getString("issueID"));
				commentListVO.setGifID(rs.getString("gifID"));
				commentListVO.setCommentContent(rs.getString("commentContent"));
				commentListVO.setCommentDate(rs.getTimestamp("commentDate"));
				
				
				list.add(commentListVO); // Store the row in the list
			}

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return list;
	}
	@Override
	public void deleteByIssueID(String issueID) {
		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(DELETEBYISSUEID);

			pstmt.setString(1, issueID);
			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
	}
	
	@Override
	public List<CommentListVO> getAllByIssueID(String issueID) {
		List<CommentListVO> commentlist = new ArrayList<CommentListVO>();
		CommentListVO commentListVO = null;
		
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs =null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(GET_ALL_STMT_BY_ISSUEID);

			pstmt.setString(1, issueID);
			pstmt.executeUpdate();

			
			while (rs.next()) {
				commentListVO = new CommentListVO();
				commentListVO.setIssueID(rs.getString("issueID"));
				commentListVO.setCommentID(rs.getString("commentID"));
				commentListVO.setMemberID(rs.getString("memberID"));
				commentListVO.setGifID(rs.getString("gifID"));
				commentListVO.setCommentContent(rs.getString("commentContent"));
				commentListVO.setCommentDate(rs.getTimestamp("commentDate"));
				commentlist.add(commentListVO);
			}
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return commentlist;
	}
	
	
	
	public static void main(String[] args) {
		CommentListJDBCDAO dao = new CommentListJDBCDAO();
		
		// 新增OK 
//		CommentListVO commentListVO1 = new CommentListVO();
//		commentListVO1.setMemberID("M000006");
//		commentListVO1.setIssueID("I000005");
//		commentListVO1.setGifID("GIF000005");
//		commentListVO1.setCommentContent(null);
//		commentListVO1.setCommentDate(java.sql.Timestamp.valueOf(ZonedDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"))));		
//		dao.insert(commentListVO1);	
//		System.out.println(".......");
		
		// 修改OK
//		CommentListVO commentListVO2 = new CommentListVO();
//		
//		commentListVO2.setCommentID("C000001");
//		commentListVO2.setGifID("GIF000009");
//		System.out.println(".......");
//		commentListVO2.setCommentContent("xxxxxxxx");
//		System.out.println(".......");
//		dao.update(commentListVO2);
//		System.out.println(".......");
		
		
		// 刪除 OK
//		dao.delete("C000007");
		
		//刪除deleteByIssueID OK
//		dao.deleteByIssueID("I000001");
		
		// 查詢OK
//		CommentListVO commentListVO3 = dao.findByPrimaryKey("C000001");
//		System.out.print(commentListVO3.getCommentID() + ",");
//		System.out.print(commentListVO3.getMemberID() + ",");
//		System.out.print(commentListVO3.getIssueID() + ",");
//		System.out.print(commentListVO3.getGifID() + ",");
//		System.out.print(commentListVO3.getCommentContent() + ",");
//		System.out.print(commentListVO3.getCommentDate() + ",");
//		System.out.println("---------------------");
		
		
		// ALL查詢 OK
//		List<CommentListVO> list = dao.getAll();
//		for(CommentListVO aCommentList : list) {
//			System.out.print(aCommentList.getCommentID() + ",");
//			System.out.print(aCommentList.getMemberID() + ",");
//			System.out.print(aCommentList.getIssueID() + ",");
//			System.out.print(aCommentList.getGifID() + ",");
//			System.out.print(aCommentList.getCommentContent() + ",");
//			System.out.print(aCommentList.getCommentDate() + ",");
//			System.out.println(".......");			
//		}
	}
}
