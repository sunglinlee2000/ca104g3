package com.commentList.model;

import java.sql.Timestamp;
import java.util.List;

public class CommentListService {

	private CommentListDAO_interface commentListDAO;
	
	public CommentListService() {
		commentListDAO = new CommentListDAO();
	}
	
	public CommentListVO addComment(String memberID,String issueID,String gifID,String commentContent,Timestamp commentDate) {
		
		CommentListVO commentListVO = new CommentListVO();
				
		commentListVO.setMemberID(memberID);
		commentListVO.setIssueID(issueID);
		commentListVO.setGifID(gifID);
		commentListVO.setCommentContent(commentContent);
		commentListVO.setCommentDate(commentDate);
		commentListDAO.insert(commentListVO);
		
		return commentListVO;		
	}
	
	public CommentListVO updateComment(String commentID,String memberID,String issueID,String gifID,String commentContent,Timestamp commentDate ) {
		
		CommentListVO commentListVO = new CommentListVO();
		
		commentListVO.setMemberID(memberID);
		commentListVO.setIssueID(issueID);		
		commentListVO.setGifID(gifID);
		commentListVO.setCommentContent(commentContent);
		commentListVO.setCommentDate(commentDate);
		
		commentListVO.setCommentID(commentID);
		commentListDAO.update(commentListVO);
		
		return commentListVO;		
	}
	
	public void deleteComment(String commentID) {
		commentListDAO.delete(commentID);
	}
	
	public CommentListVO getOneComment(String commentID) {
		return commentListDAO.findByPrimaryKey(commentID);		
	}
	
	public List<CommentListVO> getAll(){
		return commentListDAO.getAll();
	}
	
	public void deleteByIssueID(String issueID) {
		commentListDAO.deleteByIssueID(issueID);
	}
	
	public List<CommentListVO> getAllByIssueID(String issueID) {
		return commentListDAO.getAllByIssueID(issueID);
	}
	
}
