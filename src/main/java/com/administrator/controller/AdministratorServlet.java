package com.administrator.controller;

import java.io.IOException;
import java.sql.Timestamp;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.administrator.model.*;



public class AdministratorServlet extends HttpServlet{
	
	public void doGet(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		doPost(req, res);
	}
	
	public void doPost(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {

		req.setCharacterEncoding("UTF-8");
		String action = req.getParameter("action");
		System.out.println(action);
		
		if ("getOne_For_Display".equals(action)) { // 來自select_page.jsp的請求
System.out.println(1);
			List<String> errorMsgs = new LinkedList<String>();
			// Store this set in the request scope, in case we need to
			// send the ErrorPage view.
			req.setAttribute("errorMsgs", errorMsgs);
			
			try {
				System.out.println(2);
				/***************************1.接收請求參數 - 輸入格式的錯誤處理**********************/
				String str = req.getParameter("administratorID");
				System.out.println(str);
				if (str == null || (str.trim()).length() == 0) {
					errorMsgs.add("請輸管理員編號");
				}
				// Send the use back to the form, if there were errors
				if (!errorMsgs.isEmpty()) {
					System.out.println(3);
					RequestDispatcher failureView = req
							.getRequestDispatcher("/back-end/administrator/select_page.jsp");
					failureView.forward(req, res);
					System.out.println(4);
					return;//程式中斷
				}
				
				String administratorID = null;
				System.out.println(5);
				try {
					System.out.println(6);
					administratorID = new String(str);
					System.out.println(administratorID);
					
				} catch (Exception e) {
					errorMsgs.add("管理員編號格式不正確");
				}
				// Send the use back to the form, if there were errors
				if (!errorMsgs.isEmpty()) {
					System.out.println(7);
					RequestDispatcher failureView = req
							.getRequestDispatcher("/back-end/administrator/select_page.jsp");
					failureView.forward(req, res);
					System.out.println(8);
					return;//程式中斷
				}
				
				/***************************2.開始查詢資料*****************************************/
				System.out.println(9);
				AdministratorService adminSvc = new AdministratorService();
				AdministratorVO administratorVO = adminSvc.getOneAdministrator(administratorID);
				System.out.println(10);
				if (administratorVO == null) {
					System.out.println(11);
					errorMsgs.add("查無資料");
				}
				// Send the use back to the form, if there were errors
				if (!errorMsgs.isEmpty()) {
					System.out.println(12);
					RequestDispatcher failureView = req
							.getRequestDispatcher("/back-end/administrator/select_page.jsp");
					failureView.forward(req, res);
					System.out.println(13);
					return;//程式中斷
				}
				
				/***************************3.查詢完成,準備轉交(Send the Success view)*************/
				System.out.println(14);
				req.setAttribute("administratorVO", administratorVO); // 資料庫取出的empVO物件,存入req
				String url = "/back-end/administrator/listOneAdministrator.jsp";
				RequestDispatcher successView = req.getRequestDispatcher(url); // 成功轉交 listOneEmp.jsp
				System.out.println(15);
				successView.forward(req, res);
				System.out.println(15.5);

				/***************************其他可能的錯誤處理*************************************/
			} catch (Exception e) {
				System.out.println(16);
				errorMsgs.add("無法取得資料:" + e.getMessage());
				RequestDispatcher failureView = req
						.getRequestDispatcher("/back-end/administrator/select_page.jsp");
				failureView.forward(req, res);
			}
		}
		
		
		if ("getOne_For_Update".equals(action)) { // 來自listAllEmp.jsp的請求

			List<String> errorMsgs = new LinkedList<String>();
			// Store this set in the request scope, in case we need to
			// send the ErrorPage view.
			req.setAttribute("errorMsgs", errorMsgs);
			System.out.println(1);
			try {
				/***************************1.接收請求參數****************************************/
				String administratorID = new String(req.getParameter("administratorID"));
				System.out.println(2);
				/***************************2.開始查詢資料****************************************/
				AdministratorService adminSvc = new AdministratorService();
				AdministratorVO administratorVO = adminSvc.getOneAdministrator(administratorID);
				System.out.println(3);		
				/***************************3.查詢完成,準備轉交(Send the Success view)************/
				req.setAttribute("administratorVO", administratorVO);         // 資料庫取出的empVO物件,存入req
				String url = "/back-end/administrator/update_administrator_input.jsp";
				RequestDispatcher successView = req.getRequestDispatcher(url);// 成功轉交 update_emp_input.jsp
				successView.forward(req, res);
				System.out.println(4);
				/***************************其他可能的錯誤處理**********************************/
			} catch (Exception e) {
				errorMsgs.add("無法取得要修改的資料:" + e.getMessage());
				RequestDispatcher failureView = req
						.getRequestDispatcher("/back-end/administrator/listAllAdministrator.jsp");
				failureView.forward(req, res);
				System.out.println(5);
			}
		}
		
		
		if ("update".equals(action)) { // 來自update_emp_input.jsp的請求
			
			List<String> errorMsgs = new LinkedList<String>();
			// Store this set in the request scope, in case we need to
			// send the ErrorPage view.
			req.setAttribute("errorMsgs", errorMsgs);
			System.out.println(6);
			try {
				/***************************1.接收請求參數 - 輸入格式的錯誤處理**********************/
				String administratorID = new String(req.getParameter("administratorID"));
				if(administratorID == null || administratorID.trim().length() == 0) {
					errorMsgs.add("沒存到帳號");
				}
				
				String account = req.getParameter("account").trim();
				if (account == null || account.trim().length() == 0) {
					errorMsgs.add("帳號請勿空白");
				}
				System.out.println(6);
				String password = req.getParameter("password").trim();
				if (password == null || password.trim().length() == 0) {
					errorMsgs.add("密碼請勿空白");
				}			
				System.out.println(6);
				
				AdministratorVO administratorVO = new AdministratorVO();
				administratorVO.setAdministratorID(administratorID);
				administratorVO.setAdministratorAccount(account);
				administratorVO.setAdministratorPassword(password);

				// Send the use back to the form, if there were errors
				if (!errorMsgs.isEmpty()) {
					req.setAttribute("administratorVO", administratorVO); // 含有輸入格式錯誤的empVO物件,也存入req
					RequestDispatcher failureView = req
							.getRequestDispatcher("/back-end/administrator/update_administrator_input.jsp");
					failureView.forward(req, res);
					return; //程式中斷
				}
				
				
				
				/***************************2.開始修改資料*****************************************/
				AdministratorService adminSvc = new AdministratorService();
				administratorVO = adminSvc.updateAdministrator(administratorID,account,password);
				
				/***************************3.修改完成,準備轉交(Send the Success view)*************/
				req.setAttribute("administratorVO", administratorVO); // 資料庫update成功後,正確的的empVO物件,存入req
				String url = "/back-end/administrator/listOneAdministrator.jsp";
				RequestDispatcher successView = req.getRequestDispatcher(url); // 修改成功後,轉交listOneEmp.jsp
				successView.forward(req, res);

				/***************************其他可能的錯誤處理*************************************/
			} catch (Exception e) {
				errorMsgs.add("修改資料失敗:"+e.getMessage());
				RequestDispatcher failureView = req
						.getRequestDispatcher("/back-end/administrator/update_administrator_input.jsp");
				failureView.forward(req, res);
			}
		}	
		
	  if ("insert".equals(action)) { // 來自addEmp.jsp的請求  
			
			List<String> errorMsgs = new LinkedList<String>();
			// Store this set in the request scope, in case we need to
			// send the ErrorPage view.
			req.setAttribute("errorMsgs", errorMsgs);

			try {
				/***********************1.接收請求參數 - 輸入格式的錯誤處理*************************/
				
				String administratorAccount = req.getParameter("administratorAccount");
				if (administratorAccount == null || administratorAccount.trim().length() == 0) {
					errorMsgs.add("密碼請勿空白");
				}	
				
				String administratorPassword = req.getParameter("administratorPassword");
				if (administratorPassword == null || administratorPassword.trim().length() == 0) {
					errorMsgs.add("密碼請勿空白");
				}	
				
				AdministratorVO administratorVO = new AdministratorVO();
				administratorVO.setAdministratorAccount(administratorAccount);
				administratorVO.setAdministratorPassword(administratorPassword);
				
				// Send the use back to the form, if there were errors
				if (!errorMsgs.isEmpty()) {
					req.setAttribute("administratorVO", administratorVO); // 含有輸入格式錯誤的empVO物件,也存入req
					RequestDispatcher failureView = req
							.getRequestDispatcher("/back-end/administrator/addAdministrator.jsp");
					failureView.forward(req, res);
					return;
				}
				
				/***************************2.開始新增資料***************************************/
				AdministratorService administratorSvc = new AdministratorService();
				administratorVO = administratorSvc.addAdministrator(administratorAccount, administratorPassword);
				
				/***************************3.新增完成,準備轉交(Send the Success view)***********/
				String url = "/back-end/administrator/listAllAdministrator.jsp";
				RequestDispatcher successView = req.getRequestDispatcher(url); // 新增成功後轉交listAllEmp.jsp
				successView.forward(req, res);				
				
				/***************************其他可能的錯誤處理**********************************/
			} catch (Exception e) {
				errorMsgs.add(e.getMessage());
				RequestDispatcher failureView = req
						.getRequestDispatcher("/back-end/administrator/listAllAdministrator.jsp");
				failureView.forward(req, res);
			}
		}
	  if ("check_Administrator".equals(action)) { // 來自select_page.jsp的請求

			List<String> errorMsgs = new LinkedList<String>();
			req.setAttribute("errorMsgs", errorMsgs);
			

			try {
				/***************************1.接收請求參數 - 輸入格式的錯誤處理**********************/
				System.out.println(req.getParameter("administratorID"));
				System.out.println(req.getParameter("administratorAccount"));
				System.out.println(req.getParameter("administratorPassword"));
				
				String administratorID = req.getParameter("administratorID");
				String administratorAccount = req.getParameter("administratorAccount");
				String administratorPassword = req.getParameter("administratorPassword");
				

					
				AdministratorVO administratorVO = new AdministratorVO();
				administratorVO.setAdministratorID(administratorID);
				administratorVO.setAdministratorAccount(administratorAccount);
				administratorVO.setAdministratorPassword(administratorPassword);

				req.setAttribute("administratorVO", administratorVO);
				/***************************2.開始查詢資料*****************************************/
				AdministratorService administratorSvc = new AdministratorService();
				AdministratorVO administratorVO1 = administratorSvc.findByAccountPassword(administratorAccount,administratorPassword);
				
				if (administratorVO1 == null) {
					RequestDispatcher failureView = req
					.getRequestDispatcher("/back-end/login.jsp");
					failureView.forward(req, res);
					return;//程式中斷
					
				} else {
					AdministratorVO administratorVO2 = administratorSvc.getOneAdministrator(administratorVO1.getAdministratorID());
					HttpSession session = req.getSession();
					session.setAttribute("administratorVO", administratorVO2);
					System.out.println(administratorVO1.getAdministratorID());
					res.sendRedirect(req.getContextPath()+"/back-end/administrator.jsp");
				}
				
				
				} catch(Exception e) {
					e.printStackTrace();
			}
		}
	  
		if ("logOut".equals(action)) { 

			HttpSession session = req.getSession();
			if(session != null) {
				session.removeAttribute("administratorVO");
				res.sendRedirect(req.getContextPath()+"/back-end/login.jsp");
				return;
			}
		}
	  
	}
}

	

