package com.administrator.controller;
import java.io.IOException;

import javax.servlet.*;
import javax.servlet.http.*;

import com.administrator.model.*;

public class LoginFilter implements Filter{
	
	private FilterConfig config;

	public void init (FilterConfig config) {
		this.config = config;
	}
	
	public void destroy() {
		config = null;
	}
	
	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		 
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse res = (HttpServletResponse) response;
		
		HttpSession session = req.getSession();		
		AdministratorVO administratorVO  = (AdministratorVO)session.getAttribute("AdministratorVO");
		if (administratorVO == null) {
			session.setAttribute("location", req.getRequestURI());
			res.sendRedirect(req.getRequestURI() + "/login.jsp");
			res.sendRedirect(req.getContextPath()+"/back-end/login.jsp");
			return;
		} else {
			chain.doFilter(request, response);
		}
		
	}

}
