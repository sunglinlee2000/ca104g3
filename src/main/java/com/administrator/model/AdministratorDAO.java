package com.administrator.model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

public class AdministratorDAO implements AdministratorDAO_interface {
	private static DataSource ds = null;
	static {
		try {
			Context ctx = new InitialContext();
			ds = (DataSource) ctx.lookup("java:comp/env/jdbc/CA104G3DB");
		} catch (NamingException e) {
			e.printStackTrace();
		}
	}
	
	private static final String INSERT_STMT =
			"INSERT INTO administrator "
			+ "(administratorID,administratorAccount,administratorPassword)"
			+ " VALUES "
			+ "('A'||LPAD(to_char(ADMINISTRATOR_seq.NEXTVAL), 6, '0'),?,?)";
		private static final String GET_ALL_STMT = 
			"SELECT administratorID,administratorAccount,administratorPassword FROM administrator order by administratorID";
		private static final String GET_ONE_STMT = 
			"SELECT administratorID,administratorAccount,administratorPassword FROM administrator where administratorID = ?";
		private static final String DELETE = 
			"DELETE FROM administrator where administratorID = ?";
		private static final String UPDATE = 
			"UPDATE administrator set administratorAccount=?, administratorPassword=? where administratorID = ?";
		private static final String CHECK_ADMINISTRATOR = 
			"SELECT administratorID FROM administrator WHERE administratorAccount=? AND administratorPassword=?"; 		
		
		public void insert(AdministratorVO administratorVO) {
			Connection con = null;
			PreparedStatement pstmt = null;
			
			try {

				con = ds.getConnection();
				pstmt = con.prepareStatement(INSERT_STMT);
				
				pstmt.setString(1, administratorVO.getAdministratorAccount());
				pstmt.setString(2, administratorVO.getAdministratorPassword());
			
				pstmt.executeUpdate();

				// Handle any driver errors
			} catch (SQLException se) {
				throw new RuntimeException("A database error occured. "
						+ se.getMessage());
				// Clean up JDBC resources
			} finally {
				if (pstmt != null) {
					try {
						pstmt.close();
					} catch (SQLException se) {
						se.printStackTrace(System.err);
					}
				}
				if (con != null) {
					try {
						con.close();
					} catch (Exception e) {
						e.printStackTrace(System.err);
					}
				}
			}

			
		}

		@Override
		public void update(AdministratorVO administratorVO) {
			Connection con = null;
			PreparedStatement pstmt = null;

			try {

			
				con = ds.getConnection();
				pstmt = con.prepareStatement(UPDATE);
				
				pstmt.setString(1, administratorVO.getAdministratorAccount());	
				pstmt.setString(2, administratorVO.getAdministratorPassword());
				pstmt.setString(3, administratorVO.getAdministratorID());
				pstmt.executeUpdate();

				// Handle any driver errors
			} catch (SQLException se) {
				throw new RuntimeException("A database error occured. "
						+ se.getMessage());
				// Clean up JDBC resources
			} finally {
				if (pstmt != null) {
					try {
						pstmt.close();
					} catch (SQLException se) {
						se.printStackTrace(System.err);
					}
				}
				if (con != null) {
					try {
						con.close();
					} catch (Exception e) {
						e.printStackTrace(System.err);
					}
				}
			}
			
		}

		@Override
		public AdministratorVO findByPrimaryKey(String administratorID) {
			AdministratorVO administratorVO = null;
			Connection con = null;
			PreparedStatement pstmt = null;
			ResultSet rs = null;
			
			try {

				con = ds.getConnection();
				pstmt = con.prepareStatement(GET_ONE_STMT);

				pstmt.setString(1, administratorID);

				rs = pstmt.executeQuery();

				while (rs.next()) {
					// empVo �]�٬� Domain objects
					
					administratorVO = new AdministratorVO();
					administratorVO.setAdministratorID(rs.getString("administratorID"));
					administratorVO.setAdministratorAccount(rs.getString("administratorAccount"));
					administratorVO.setAdministratorPassword(rs.getString("administratorPassword"));
					
				}

				// Handle any driver errors
			} catch (SQLException se) {
				throw new RuntimeException("A database error occured. "
						+ se.getMessage());
				// Clean up JDBC resources
			} finally {
				if (pstmt != null) {
					try {
						pstmt.close();
					} catch (SQLException se) {
						se.printStackTrace(System.err);
					}
				}
				if (con != null) {
					try {
						con.close();
					} catch (Exception e) {
						e.printStackTrace(System.err);
					}
				}
			}
			return administratorVO;
		}

		@Override
		public List<AdministratorVO> getAll() {
			List<AdministratorVO> list = new ArrayList<AdministratorVO>();
			AdministratorVO administratorVO = null;
			
			Connection con = null;
			PreparedStatement pstmt = null;
			ResultSet rs = null;
			
			try {

				con = ds.getConnection();
				pstmt = con.prepareStatement(GET_ALL_STMT);
				rs = pstmt.executeQuery();

				while (rs.next()) {
					// empVO �]�٬� Domain objects
					
					administratorVO = new AdministratorVO();				
					administratorVO.setAdministratorID(rs.getString("administratorID"));
					administratorVO.setAdministratorAccount(rs.getString("administratorAccount"));
					administratorVO.setAdministratorPassword(rs.getString("administratorPassword"));				
					
					list.add(administratorVO); // Store the row in the list
				}

				// Handle any driver errors
			} catch (SQLException se) {
				throw new RuntimeException("A database error occured. "
						+ se.getMessage());
				// Clean up JDBC resources
			} finally {
				if (pstmt != null) {
					try {
						pstmt.close();
					} catch (SQLException se) {
						se.printStackTrace(System.err);
					}
				}
				if (con != null) {
					try {
						con.close();
					} catch (Exception e) {
						e.printStackTrace(System.err);
					}
				}
			}
			return list;
		}
		
		@Override
		public AdministratorVO findByAccountPassword(String administratorAccount,String administratorPassword) {
			AdministratorVO administratorVO = null;
			Connection con = null;
			PreparedStatement pstmt = null;
			ResultSet rs = null;
			
			try {

				con = ds.getConnection();
				pstmt = con.prepareStatement(CHECK_ADMINISTRATOR);

				pstmt.setString(1, administratorAccount);
				pstmt.setString(2, administratorPassword);

				rs = pstmt.executeQuery();

				while (rs.next()) {
					administratorVO = new AdministratorVO();
					administratorVO.setAdministratorID(rs.getString("administratorID"));

				}
				
				
				// Handle any driver errors
			} catch (SQLException se) {
				throw new RuntimeException("A database error occured. "
						+ se.getMessage());
				// Clean up JDBC resources
			} finally {
				if (pstmt != null) {
					try {
						pstmt.close();
					} catch (SQLException se) {
						se.printStackTrace(System.err);
					}
				}
				if (con != null) {
					try {
						con.close();
					} catch (Exception e) {
						e.printStackTrace(System.err);
					}
				}
			}
			return administratorVO;
		}

}
