package com.administrator.model;

import java.util.List;



public class AdministratorService {
	
	private AdministratorDAO_interface dao;
	
	public AdministratorService() {
		dao = new AdministratorDAO();
	}
	
	public AdministratorVO addAdministrator(String administratorAccount,String administratorPassword) {
		
		AdministratorVO administratorVO = new AdministratorVO();
		
		administratorVO.setAdministratorAccount(administratorAccount);
		administratorVO.setAdministratorPassword(administratorPassword);
		dao.insert(administratorVO);
		return administratorVO;
	
	}
	
	public AdministratorVO updateAdministrator(String administratorID,String administratorAccount,String administratorPassword) {
		
		AdministratorVO administratorVO = new AdministratorVO();
		
		administratorVO.setAdministratorID(administratorID);
		administratorVO.setAdministratorAccount(administratorAccount);
		administratorVO.setAdministratorPassword(administratorPassword);
		dao.update(administratorVO);
		
		return administratorVO;	
	}
	
	public AdministratorVO getOneAdministrator(String administratorID) {
		
		return dao.findByPrimaryKey(administratorID);	
	}
	
	public List<AdministratorVO> getAll(){
		return dao.getAll();
	}
	public AdministratorVO findByAccountPassword(String administratorAccount,String administratorPassword) {
		return dao.findByAccountPassword(administratorAccount,administratorPassword);
	}
}
