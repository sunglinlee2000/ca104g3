package com.administrator.model;

import java.util.List;

import com.issue.model.IssueVO;

public interface AdministratorDAO_interface {
	public void insert(AdministratorVO administratorVO);
    public void update(AdministratorVO administratorVO);    
    public AdministratorVO findByPrimaryKey(String administratorID);
    public List<AdministratorVO> getAll();
    public AdministratorVO findByAccountPassword(String administratorAccount,String administratorPassword);

}
