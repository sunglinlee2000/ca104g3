package com.administrator.model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.issue.model.IssueVO;


public class AdministratorJDBCDAO implements AdministratorDAO_interface{
	String driver = "oracle.jdbc.driver.OracleDriver";
	String url = "jdbc:oracle:thin:@localhost:1521:XE";
	String userid = "CA104G3";
	String passwd = "123456";
	
	private static final String INSERT_STMT =
			"INSERT INTO administrator "
			+ "(administratorID,administratorAccount,administratorPassword)"
			+ " VALUES "
			+ "('A'||LPAD(to_char(ADMINISTRATOR_seq.NEXTVAL), 6, '0'),?,?)";
		private static final String GET_ALL_STMT = 
			"SELECT administratorID,administratorAccount,administratorPassword FROM administrator order by administratorID";
		private static final String GET_ONE_STMT = 
			"SELECT administratorID,administratorAccount,administratorPassword FROM administrator where administratorID = ?";
		private static final String DELETE = 
			"DELETE FROM administrator where administratorID = ?";
		private static final String UPDATE = 
			"UPDATE administrator set administratorAccount=?, administratorPassword=? where administratorID = ?";
		private static final String CHECK_ADMINISTRATOR = 
			"SELECT administratorID FROM administrator WHERE administratorAccount=? AND administratorPassword=?"; 

	@Override
	public void insert(AdministratorVO administratorVO) {
		Connection con = null;
		PreparedStatement pstmt = null;
		
		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(INSERT_STMT);
			
			pstmt.setString(1, administratorVO.getAdministratorAccount());
			pstmt.setString(2, administratorVO.getAdministratorPassword());
		
			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

		
	}

	@Override
	public void update(AdministratorVO administratorVO) {
		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(UPDATE);
			
			pstmt.setString(1, administratorVO.getAdministratorAccount());	
			pstmt.setString(2, administratorVO.getAdministratorPassword());
			pstmt.setString(3, administratorVO.getAdministratorID());
			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		
	}

	@Override
	public AdministratorVO findByPrimaryKey(String administratorID) {
		AdministratorVO administratorVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(GET_ONE_STMT);

			pstmt.setString(1, administratorID);

			rs = pstmt.executeQuery();

			while (rs.next()) {
				// empVo �]�٬� Domain objects
				
				administratorVO = new AdministratorVO();
				administratorVO.setAdministratorID(rs.getString("administratorID"));
				administratorVO.setAdministratorAccount(rs.getString("administratorAccount"));
				administratorVO.setAdministratorPassword(rs.getString("administratorPassword"));
				
			}

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return administratorVO;
	}

	@Override
	public List<AdministratorVO> getAll() {
		List<AdministratorVO> list = new ArrayList<AdministratorVO>();
		AdministratorVO administratorVO = null;
		
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(GET_ALL_STMT);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				// empVO �]�٬� Domain objects
				
				administratorVO = new AdministratorVO();				
				administratorVO.setAdministratorID(rs.getString("administratorID"));
				administratorVO.setAdministratorAccount(rs.getString("administratorAccount"));
				administratorVO.setAdministratorPassword(rs.getString("administratorPassword"));				
				
				list.add(administratorVO); // Store the row in the list
			}

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return list;
	}
	
	@Override
	public AdministratorVO findByAccountPassword(String administratorAccount,String administratorPassword) {
		AdministratorVO administratorVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		try {
			
			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(CHECK_ADMINISTRATOR);
			
			pstmt.setString(1, administratorAccount);
			pstmt.setString(2, administratorPassword);
			
			rs = pstmt.executeQuery();
			
			while (rs.next()) {
				administratorVO = new AdministratorVO();
				administratorVO.setAdministratorID(rs.getString("administratorID"));
			}
			
			
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver."+ e.getMessage());
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured." + se.getMessage());
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			} 
			if (con != null) {
				try {
					con.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
		}
		
		return administratorVO;
	}
	
	public static void main(String[] args) {
		AdministratorJDBCDAO dao = new AdministratorJDBCDAO();
		
		// 新增OK
//		AdministratorVO administratorVO1 = new AdministratorVO();
//		administratorVO1.setAdministratorID("A000006");
//		administratorVO1.setAdministratorAccount("ca104g3");
//		administratorVO1.setAdministratorPassword("123456");
//		dao.insert(administratorVO1);
		
		// 修改OK
//		AdministratorVO administratorVO2 = new AdministratorVO();
//		administratorVO2.setAdministratorID("A000007");
//		System.out.println(".......");
//		administratorVO2.setAdministratorAccount("104");
//		System.out.println(".......");
//		administratorVO2.setAdministratorPassword("123");
//		System.out.println(".......");
//		dao.update(administratorVO2);
//		System.out.println(".......");
		
		// 查詢OK
//		AdministratorVO administratorVO3 = dao.findByPrimaryKey("A000001");
//		System.out.print(administratorVO3.getAdministratorID() + ",");
//		System.out.print(administratorVO3.getAdministratorAccount() + ",");
//		System.out.print(administratorVO3.getAdministratorPassword() + ",");
//		System.out.println("---------------------");
		
		// 查全部OK
//		List<AdministratorVO> list = dao.getAll();
//		for(AdministratorVO aAdministrator : list) {
//			System.out.print(aAdministrator.getAdministratorID() + ",");
//			System.out.println(".......");
//			System.out.print(aAdministrator.getAdministratorAccount() + ",");
//			System.out.println(".......");
//			System.out.print(aAdministrator.getAdministratorPassword() + ",");
//			System.out.println("");
//		}
	}
}
