package com.administrator.model;

public class AdministratorVO implements java.io.Serializable{
	private String administratorID;
	private String administratorAccount;
	private String administratorPassword;
	
	public String getAdministratorID() {
		return administratorID;
	}
	public void setAdministratorID(String administratorID) {
		this.administratorID = administratorID;
	}
	public String getAdministratorAccount() {
		return administratorAccount;
	}
	public void setAdministratorAccount(String administratorAccount) {
		this.administratorAccount = administratorAccount;
	}
	public String getAdministratorPassword() {
		return administratorPassword;
	}
	public void setAdministratorPassword(String administratorPassword) {
		this.administratorPassword = administratorPassword;
	}
	 
}
