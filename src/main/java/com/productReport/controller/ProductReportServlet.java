package com.productReport.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;

import com.product.model.ProductService;
import com.product.model.ProductVO;
import com.productReport.model.ProductReportService;

public class ProductReportServlet extends HttpServlet{
	
	private static final long serialVersionUID = 1L;

	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		doPost(req, res);
	}

	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		
		req.setCharacterEncoding("UTF-8");
		
		String action = req.getParameter("action");
		System.out.println(action);
		
		if (action.equals("insert")) {
			
			System.out.println("進入insert");
			
			String memberID=req.getParameter("memberID");
			String productID=req.getParameter("productID");
			String productReportStatus="REPORT_UNCHECKED";
			String productReportReson=req.getParameter("productReportReson");
			
			System.out.println("11111");
			JSONObject obj = new JSONObject(); 
			
			ProductReportService productReportSVC=new ProductReportService();
			//新增檢舉
			productReportSVC.addReport(memberID, productID, productReportStatus, productReportReson);
			
			
			res.setContentType("text/plain"); 
			res.setCharacterEncoding("UTF-8"); 
			PrintWriter out = res.getWriter(); 
			out.write(obj.toString()); 
			out.flush(); 
			out.close();
		 
		}
		
		if (action.equals("update_For_Status")) {
			
			System.out.println("進入update_For_Status");
			
			String productReportStatus=req.getParameter("productReportStatus");
			String productReportID=req.getParameter("productreportID");
			String productID =req.getParameter("productID");
			System.out.println(productReportStatus);
			//修改檢舉狀態
			ProductReportService productReportSVC=new ProductReportService();
			productReportSVC.updateForStatus(productReportStatus, productReportID);
			System.out.println("修改成功");
			if(productReportStatus.equals("REPORT_ACCEPTED")) {
				ProductService productSvc=new ProductService();
				ProductVO productVO =new ProductVO();
				productVO=productSvc.updateProductStatus(productID, "PRODUCT_REMOVE");
				System.out.println("審核成功:PRODUCT_REMOVE");
			}
			JSONObject obj = new JSONObject(); 
			
			try {
				obj.put("productReportStatus", productReportStatus);
				
			}catch(JSONException e) {
				e.printStackTrace();
			}
			
			res.setContentType("text/plain"); 
			res.setCharacterEncoding("UTF-8"); 
			PrintWriter out = res.getWriter(); 
			out.write(obj.toString()); 
			out.flush(); 
			out.close();
		 
		}
	}
}
