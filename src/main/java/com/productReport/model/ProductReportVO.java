package com.productReport.model;


import java.sql.Date;
import java.sql.Timestamp;

public class ProductReportVO implements java.io.Serializable{
	private	String	productreportID;
	private	String	memberID;
	private	String	adminstratorID;
	private	String	productID;
	private	String	productReportInfo;	
	private	Timestamp	productReportDate;
	private	String	productReportStatus;
	private	String	productReportReason;
	
	public String getProductreportID() {
		return productreportID;
	}
	public void setProductreportID(String productreportID) {
		this.productreportID = productreportID;
	}
	public String getMemberID() {
		return memberID;
	}
	public void setMemberID(String memberID) {
		this.memberID = memberID;
	}
	public String getAdminstratorID() {
		return adminstratorID;
	}
	public void setAdminstratorID(String adminstratorID) {
		this.adminstratorID = adminstratorID;
	}
	public String getProductID() {
		return productID;
	}
	public void setProductID(String productID) {
		this.productID = productID;
	}
	public String getProductReportInfo() {
		return productReportInfo;
	}
	public void setProductReportInfo(String productReportInfo) {
		this.productReportInfo = productReportInfo;
	}
	public Timestamp getProductReportDate() {
		return productReportDate;
	}
	public void setProductReportDate(Timestamp productReportDate) {
		this.productReportDate = productReportDate;
	}
	public String getProductReportStatus() {
		return productReportStatus;
	}
	public void setProductReportStatus(String productReportStatus) {
		this.productReportStatus = productReportStatus;
	}
	public String getProductReportReason() {
		return productReportReason;
	}
	public void setProductReportReason(String productReportReason) {
		this.productReportReason = productReportReason;
	}
	
	
	
	
	
}

