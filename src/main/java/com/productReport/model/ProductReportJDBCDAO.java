package com.productReport.model;

import java.sql.*;
import java.util.*;



public class ProductReportJDBCDAO implements ProductReportDAO_interface {
	String driver = "oracle.jdbc.driver.OracleDriver";
	String url = "jdbc:oracle:thin:@localhost:1521:XE";
	String userid = "CA104G3";
	String password = "123456";
	
	private static final String INSERT_STMT =
			"INSERT INTO PRODUCTREPORT(PRODUCTREPORTID,MEMBERID,ADMINISTRATORID,PRODUCTID,PRODUCTREPORTINFO,PRODUCTREPORTDATE,PRODUCTREPORTSTATUS,PRODUCTREPORTREASON)"
			+"values('PR'||LPAD(to_char(PRODUCTREPORT_SEQ.NEXTVAL), 4, '0'),?,?,?,?,?,?,?)";
	private static final String UPDATE =
			"UPDATE PRODUCTREPORT SET MEMBERID=?,ADMINISTRATORID=?,PRODUCTID=?,PRODUCTREPORTINFO=?,PRODUCTREPORTDATE=?,PRODUCTREPORTSTATUS=?,PRODUCTREPORTREASON=? where PRODUCTREPORTID=?";
	private static final String DELETE =
			"DELETE FROM PRODUCTREPORT WHERE PRODUCTREPORTID=?";
	private static final String GET_ONE_STMT =
			"SELECT * FROM PRODUCTREPORT where PRODUCTREPORTID=?";
	private static final String GET_ALL_STMT =
			"SELECT PRODUCTREPORTID,MEMBERID,ADMINISTRATORID,PRODUCTID,PRODUCTREPORTINFO,PRODUCTREPORTDATE,PRODUCTREPORTSTATUS,PRODUCTREPORTREASON FROM PRODUCTREPORT order by PRODUCTREPORTID";
	@Override
	public void insert(ProductReportVO productReportVO) {
		Connection con = null;
		PreparedStatement pstmt = null;
		try {
			Class.forName(driver);
			con=DriverManager.getConnection(url,userid,password);
			pstmt = con.prepareStatement(INSERT_STMT);
			
			pstmt.setString(1, productReportVO.getMemberID());
			pstmt.setString(2, productReportVO.getAdminstratorID());
			pstmt.setString(3,productReportVO.getProductID());
			pstmt.setString(4, productReportVO.getProductReportInfo());
			pstmt.setTimestamp(5, productReportVO.getProductReportDate());
			pstmt.setString(6, productReportVO.getProductReportStatus());
			pstmt.setString(7, productReportVO.getProductReportReason());
			pstmt.executeUpdate();
			
		}catch(ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
		}catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		
	}

	@Override
	public void update(ProductReportVO productReportVO) {
		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, password);
			pstmt = con.prepareStatement(UPDATE);

			
			pstmt.setString(1, productReportVO.getMemberID());
			pstmt.setString(2, productReportVO.getAdminstratorID());
			pstmt.setString(3,productReportVO.getProductID());
			pstmt.setString(4, productReportVO.getProductReportInfo());
			pstmt.setTimestamp(5, productReportVO.getProductReportDate());
			pstmt.setString(6, productReportVO.getProductReportStatus());
			pstmt.setString(7, productReportVO.getProductReportReason());
			pstmt.setString(8, productReportVO.getProductreportID());
			
			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		
	}

	@Override
	public void delete(String productReportID) {
		
		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, password);
			pstmt = con.prepareStatement(DELETE);

			pstmt.setString(1, productReportID);

			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		
		
	}

	@Override
	public ProductReportVO findByPrimaryKey(String productReportID) {
		ProductReportVO productreportvo=null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, password);
			pstmt = con.prepareStatement(GET_ONE_STMT);

			pstmt.setString(1, productReportID);

			rs = pstmt.executeQuery();

			while (rs.next()) {
				productreportvo =new ProductReportVO();
				productreportvo.setProductreportID(rs.getString("productreportid"));
				productreportvo.setMemberID(rs.getString("memberid"));
				productreportvo.setAdminstratorID(rs.getString("administratorid"));
				productreportvo.setProductID(rs.getString("productid"));
				productreportvo.setProductReportInfo(rs.getString("productreportinfo"));
				productreportvo.setProductReportDate(rs.getTimestamp("productreportdate"));
				productreportvo.setProductReportStatus(rs.getString("productreportstatus"));
				productreportvo.setProductReportReason(rs.getString("productreportreason"));
			}

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return productreportvo;
	}

	@Override
	public List<ProductReportVO> getAll() {
		List<ProductReportVO> list = new ArrayList<ProductReportVO>();
		ProductReportVO productreportvo=null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, password);
			pstmt = con.prepareStatement(GET_ALL_STMT);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				productreportvo =new ProductReportVO();
				productreportvo.setProductreportID(rs.getString("productreportid"));
				productreportvo.setMemberID(rs.getString("memberid"));
				productreportvo.setAdminstratorID(rs.getString("administratorid"));
				productreportvo.setProductID(rs.getString("productid"));
				productreportvo.setProductReportInfo(rs.getString("productreportinfo"));
				productreportvo.setProductReportDate(rs.getTimestamp("productreportdate"));
				productreportvo.setProductReportStatus(rs.getString("productreportstatus"));
				productreportvo.setProductReportReason(rs.getString("productreportreason"));
				list.add(productreportvo);
			}

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return list;
	}
	public static void main(String[] args) {
		ProductReportJDBCDAO dao=new ProductReportJDBCDAO();
//		1.�s�W
//		ProductReportVO vo=new ProductReportVO();
//		vo.setMemberID("M00009");
//		vo.setAdminstratorID("A00009");
//		vo.setProductID("P00009");
//		vo.setProductReportInfo("�Ӧⱡ�F��!!!!!!!!");
//		vo.setProductReportDate(java.sql.Date.valueOf("2018-10-22"));
//		vo.setProductReportStatus("REPORT_UNCHECKED");
//		vo.setProductReportReason("SEX");
//		dao.insert(vo);
//		System.out.println("�s�W���\!!!");
//		2.�ק�
//		ProductReportVO vo=new ProductReportVO();
//		vo.setProductreportID("PR0006");
//		vo.setMemberID("M00009");
//		vo.setAdminstratorID("A00009");
//		vo.setProductID("P00009");
//		vo.setProductReportInfo("��skr�F��!!!!!!!!");
//		vo.setProductReportDate(java.sql.Date.valueOf("2018-10-22"));
//		vo.setProductReportStatus("REPORT_UNCHECKED");
//		vo.setProductReportReason("ARTIST_WRONG");
//		dao.update(vo);
//		System.out.println("�ק令�\!!!");
//		3.�R��
//		dao.delete("PR0006");
//		System.out.println("�R�����\!");
//		4.�d�߳�@
//		ProductReportVO vo=dao.findByPrimaryKey("PR000001");
//		System.out.println(vo.getProductreportID());
//		System.out.println(vo.getMemberID());
//		System.out.println(vo.getAdminstratorID());
//		System.out.println(vo.getProductID());
//		System.out.println(vo.getProductReportInfo());
//		System.out.println(vo.getProductReportDate());
//		System.out.println(vo.getProductReportStatus());
//		System.out.println(vo.getProductReportReason());
//		System.out.println("-----------------------------");
//		System.out.println("�d�ߦ��\!!!!");
//		5.�d����
		List<ProductReportVO> list=dao.getAll();
		for(ProductReportVO vo:list) {
			System.out.println(vo.getProductreportID());
			System.out.println(vo.getMemberID());
			System.out.println(vo.getAdminstratorID());
			System.out.println(vo.getProductID());
			System.out.println(vo.getProductReportInfo());
			System.out.println(vo.getProductReportDate());
			System.out.println(vo.getProductReportStatus());
			System.out.println(vo.getProductReportReason());
			System.out.println("-----------------------------");
		}
		System.out.println("sucess!!!!!!!!!!");
			
	}

	@Override
	public void updateForStatus(ProductReportVO productReportVO) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Integer getCount() {
		
		return 0;
	}

}
