package com.productReport.model;

import java.sql.*;
import java.util.*;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;



public class ProductReportDAO implements ProductReportDAO_interface {
	private static DataSource ds = null;
	static {
		try {
			Context ctx = new InitialContext();
			ds = (DataSource) ctx.lookup("java:comp/env/jdbc/CA104G3DB");
		} catch (NamingException e) {
			e.printStackTrace();
		}
	}
	
	private static final String INSERT_STMT =
			"INSERT INTO PRODUCTREPORT(PRODUCTREPORTID,MEMBERID,PRODUCTID,PRODUCTREPORTDATE,PRODUCTREPORTSTATUS,PRODUCTREPORTREASON)"
			+"values('PR'||LPAD(to_char(PRODUCTREPORT_SEQ.NEXTVAL), 6, '0'),?,?,(CURRENT_TIMESTAMP),?,?)";
	private static final String UPDATE =
			"UPDATE PRODUCTREPORT SET MEMBERID=?,ADMINISTRATORID=?,PRODUCTID=?,PRODUCTREPORTINFO=?,PRODUCTREPORTDATE=?,PRODUCTREPORTSTATUS=?,PRODUCTREPORTREASON=? where PRODUCTREPORTID=?";
	private static final String DELETE =
			"DELETE FROM PRODUCTREPORT WHERE PRODUCTREPORTID=?";
	private static final String GET_ONE_STMT =
			"SELECT * FROM PRODUCTREPORT where PRODUCTREPORTID=?";
	private static final String GET_ALL_STMT =
			"SELECT * FROM PRODUCTREPORT order by PRODUCTREPORTID desc";
	private static final String UPDATE_FOR_STATUS =
			"UPDATE PRODUCTREPORT SET PRODUCTREPORTSTATUS=? where PRODUCTREPORTID=?";
	private static final String GET_PRODUCT_REPORT_STATUS_COUNT =
	"SELECT count(*) from productReport where productReportStatus='REPORT_UNCHECKED'";
	@Override
	public void insert(ProductReportVO productReportVO) {
		Connection con = null;
		PreparedStatement pstmt = null;
		try {
			con = ds.getConnection();
			pstmt = con.prepareStatement(INSERT_STMT);
			
			pstmt.setString(1, productReportVO.getMemberID());
			pstmt.setString(2,productReportVO.getProductID());
			pstmt.setString(3, productReportVO.getProductReportStatus());
			pstmt.setString(4, productReportVO.getProductReportReason());
			pstmt.executeUpdate();
			
		}catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		
	}

	@Override
	public void update(ProductReportVO productReportVO) {
		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(UPDATE);

			
			pstmt.setString(1, productReportVO.getMemberID());
			pstmt.setString(2, productReportVO.getAdminstratorID());
			pstmt.setString(3,productReportVO.getProductID());
			pstmt.setString(4, productReportVO.getProductReportInfo());
			pstmt.setTimestamp(5, productReportVO.getProductReportDate());
			pstmt.setString(6, productReportVO.getProductReportStatus());
			pstmt.setString(7, productReportVO.getProductReportReason());
			pstmt.setString(8, productReportVO.getProductreportID());
			
			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		
	}

	@Override
	public void delete(String productReportID) {
		
		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(DELETE);

			pstmt.setString(1, productReportID);

			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		
		
	}

	@Override
	public ProductReportVO findByPrimaryKey(String productReportID) {
		ProductReportVO productreportvo=null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ONE_STMT);

			pstmt.setString(1, productReportID);

			rs = pstmt.executeQuery();

			while (rs.next()) {
				productreportvo =new ProductReportVO();
				productreportvo.setProductreportID(rs.getString("productreportid"));
				productreportvo.setMemberID(rs.getString("memberid"));
				productreportvo.setAdminstratorID(rs.getString("administratorid"));
				productreportvo.setProductID(rs.getString("productid"));
				productreportvo.setProductReportInfo(rs.getString("productreportinfo"));
				productreportvo.setProductReportDate(rs.getTimestamp("productreportdate"));
				productreportvo.setProductReportStatus(rs.getString("productreportstatus"));
				productreportvo.setProductReportReason(rs.getString("productreportreason"));
			}

			// Handle any driver errors
		}  catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return productreportvo;
	}

	@Override
	public List<ProductReportVO> getAll() {
		List<ProductReportVO> list = new ArrayList<ProductReportVO>();
		ProductReportVO productreportvo=null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ALL_STMT);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				productreportvo =new ProductReportVO();
				productreportvo.setProductreportID(rs.getString("productreportid"));
				productreportvo.setMemberID(rs.getString("memberid"));
				productreportvo.setAdminstratorID(rs.getString("administratorid"));
				productreportvo.setProductID(rs.getString("productid"));
				productreportvo.setProductReportInfo(rs.getString("productreportinfo"));
				productreportvo.setProductReportDate(rs.getTimestamp("productReportDate"));
				productreportvo.setProductReportStatus(rs.getString("productreportstatus"));
				productreportvo.setProductReportReason(rs.getString("productreportreason"));
				list.add(productreportvo);
			}

			// Handle any driver errors
		}  catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return list;
	}

	@Override
	public void updateForStatus(ProductReportVO productReportVO) {
		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(UPDATE_FOR_STATUS);
			
			pstmt.setString(1, productReportVO.getProductReportStatus());
			pstmt.setString(2, productReportVO.getProductreportID());
			
			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		
	}

	@Override
	public Integer getCount() {
		Integer count=0;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_PRODUCT_REPORT_STATUS_COUNT);


			rs = pstmt.executeQuery();

			while (rs.next()) {
				count=rs.getInt(1);
			}

			// Handle any driver errors
		}  catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return count;
	}

}
