package com.productReport.model;

import java.sql.Timestamp;
import java.util.List;

public class ProductReportService {
	
	private ProductReportDAO_interface dao;
	
	public ProductReportService() {
		dao=new ProductReportDAO();
	}
	
	public ProductReportVO addReport(String	memberID,String productID,String productReportStatus,String	productReportReason) {
		ProductReportVO productReportVO=new ProductReportVO();
		
		
		productReportVO.setMemberID(memberID); 
		productReportVO.setProductID(productID);
		productReportVO.setProductReportReason(productReportReason);
		productReportVO.setProductReportStatus(productReportStatus);
		
		dao.insert(productReportVO);
		
		return productReportVO;
	}
	
	public List<ProductReportVO> getAll(){
		return dao.getAll();
	}
	
	public ProductReportVO updateForStatus(String productReportStatus,String productReportID) {
			ProductReportVO productReportVO=new ProductReportVO();
			
			productReportVO.setProductReportStatus(productReportStatus);
			productReportVO.setProductreportID(productReportID);
			
			dao.updateForStatus(productReportVO);
			
			return productReportVO;
	}
	
	public Integer getCount(){
		return dao.getCount();
	}
}
