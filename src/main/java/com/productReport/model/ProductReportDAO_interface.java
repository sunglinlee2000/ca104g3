package com.productReport.model;

import java.util.List;

public interface ProductReportDAO_interface {
		public void insert(ProductReportVO productReportVO);
		public void update(ProductReportVO productReportVO);
		public void delete(String productReportID);
		public ProductReportVO findByPrimaryKey(String productReportID);
		public List<ProductReportVO> getAll();
		public void updateForStatus(ProductReportVO productReportVO);
		public Integer getCount();
		

}	
