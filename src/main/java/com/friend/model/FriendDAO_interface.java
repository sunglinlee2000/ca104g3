package com.friend.model;

import java.util.*;
import com.member.model.*;

public interface FriendDAO_interface {
	public void insert(FriendVO friendVO);
	public void update(FriendVO friendVO);
	public void delete(String memberID, String friendID);
	public FriendVO findByPrimaryKey(String memberID, String friendID);
	public List<FriendVO> getAll();
//	個人頁面好友列表
	public Set<FriendVO> getFriendsByMemberID(String memberID);
	
//	成為朋友
	public void becomeFriending(String memberID, String friendID, String friendStatus);
}
