package com.friend.model;

import java.util.*;

public class FriendService {

	private FriendDAO_interface dao;

	public FriendService() {
		dao = new FriendDAO();
	}

	public FriendVO addFriend(String memberID, String friendID, String friendStatus) {
		FriendVO friendVO = new FriendVO();

		friendVO.setFriendID(friendID);
		friendVO.setMemberID(memberID);
		friendVO.setFriendStatus(friendStatus);
		dao.insert(friendVO);

		return friendVO;
	}
	
	public void deleteFriend(String memberID, String friendID) {
		dao.delete(memberID, friendID);
	}
	
	public FriendVO getOneFriend(String memberID, String friendID) {
		return dao.findByPrimaryKey(memberID, friendID);
	}
	
	public List<FriendVO> getAll() {
		return dao.getAll();
	}
	
	public FriendVO updateFriendStatus(String memberID, String friendID, String friendStatus) {
		FriendVO friendVO = new FriendVO();

		friendVO.setFriendID(friendID);
		friendVO.setMemberID(memberID);
		friendVO.setFriendStatus(friendStatus);
		dao.update(friendVO);

		return friendVO;
	}
	
	public Set<FriendVO> getFriendsByMemberID(String memberID) {
		return dao.getFriendsByMemberID(memberID);
	}
	
	public void becomeFriending(String memberID, String friendID, String friendStatus) {
		
		dao.becomeFriending(memberID, friendID, friendStatus);
	}
	
}
