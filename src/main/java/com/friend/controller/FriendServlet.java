package com.friend.controller;

import java.io.*;
import java.util.*;
import java.util.Date;
import java.sql.*;

import javax.servlet.*;
import javax.servlet.http.*;

import org.json.JSONException;
import org.json.JSONObject;

import com.friend.model.*;
import com.member.model.*;
import com.notificationList.model.NotificationListService;

public class FriendServlet extends HttpServlet {

	public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		doPost(req, res);
	}

	public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {

		req.setCharacterEncoding("UTF-8");
		String action = req.getParameter("action");
		res.setHeader("Cache-Control", "no-store");
		res.setHeader("Pragma", "no-cache");
		res.setDateHeader("Expires", 0);

		if ("friend-applying".equals(action)) {
			/*************************** 1.接收請求參數 - 輸入格式的錯誤處理 **********************/
			System.out.println("取得資料");
			HttpSession session = req.getSession();
			MemberVO memberVO = (MemberVO) session.getAttribute("memberVO");
			String memberID = memberVO.getMemberID();
			String theOtherMemberID = req.getParameter("memberID");

			/*************************** 2.開始新增資料 *****************************************/
//			新增好友
			FriendService friendSvc = new FriendService();
			FriendVO friendVO1 = friendSvc.addFriend(memberID, theOtherMemberID, "FRIEND_APPLYING");
			FriendVO friendVO2 = friendSvc.addFriend(theOtherMemberID, memberID, "ANSWER_NOT_YET");
//			新增通知
			String memberNickName = new MemberService().getOneMember(memberID).getMemberNickName();
			String notificationContent = memberNickName + "對您提出好友申請";
			Timestamp now = new Timestamp(new Date().getTime());
			NotificationListService notificationListSvc = new NotificationListService();
			notificationListSvc.addNoList("NT000001", theOtherMemberID, notificationContent, now, memberID, "FALSE", 
					"/personalPage/personalPage.do?action=toOnePersonalPage&memberID=");
			/*************************** 3.查詢完成,準備轉交(Send the Success view) ************/
//			JSONObject obj = new JSONObject();
//			try {
//				obj.put("alert", "回傳成功");
//			} catch (JSONException e) {
//				e.printStackTrace();
//			}
//			res.setContentType("text/plain");
//			res.setCharacterEncoding("UTF-8");
//			PrintWriter out = res.getWriter();
//			out.write(obj.toString());
//			out.flush();
//			out.close();
			/*************************** 其他可能的錯誤處理 **********************************/
		}
		
		if ("friend-answer-yes".equals(action)) {
			/*************************** 1.接收請求參數 - 輸入格式的錯誤處理 **********************/
			System.out.println("取得資料");
			HttpSession session = req.getSession();
			MemberVO memberVO = (MemberVO) session.getAttribute("memberVO");
			String memberID = memberVO.getMemberID();
			String theOtherMemberID = req.getParameter("memberID");

			/*************************** 2.開始新增資料 *****************************************/
//			改好友狀態
			FriendService friendSvc = new FriendService();
			friendSvc.becomeFriending(memberID, theOtherMemberID, "FRIEND_FRIENDING");
			
//			新增通知
			String memberNickName = new MemberService().getOneMember(memberID).getMemberNickName();
			String notificationContent = memberNickName + "同意您的好友申請";
			Timestamp now = new Timestamp(new Date().getTime());
			NotificationListService notificationListSvc = new NotificationListService();
			notificationListSvc.addNoList("NT000001", theOtherMemberID, notificationContent, now, memberID, "FALSE",
					"/personalPage/personalPage.do?action=toOnePersonalPage&memberID=");
			
			/*************************** 3.查詢完成,準備轉交(Send the Success view) ************/
//			JSONObject obj = new JSONObject();
//			try {
//				obj.put("alert", "回傳成功");
//			} catch (JSONException e) {
//				e.printStackTrace();
//			}
//			res.setContentType("text/plain");
//			res.setCharacterEncoding("UTF-8");
//			PrintWriter out = res.getWriter();
//			out.write(obj.toString());
//			out.flush();
//			out.close();
			/*************************** 其他可能的錯誤處理 **********************************/
		}
	}
}
