package com.SNSTranslated.model;

import java.sql.Timestamp;

public class SNSTranslatedVO implements java.io.Serializable{
	private String snsTranslatedID;
	private String snsID;
	private String memberID;
	private Timestamp snsTranslatedDate;
	private String snsTranslatedContent;
	private String snsTranslatedStatus;
	
	public String getSnsID() {
		return snsID;
	}
	public void setSnsID(String snsID) {
		this.snsID = snsID;
	}
	public String getMemberID() {
		return memberID;
	}
	public void setMemberID(String memberID) {
		this.memberID = memberID;
	}
	public String getSnsTranslatedContent() {
		return snsTranslatedContent;
	}
	public void setSnsTranslatedContent(String snsTranslatedContent) {
		this.snsTranslatedContent = snsTranslatedContent;
	}
	public String getSnsTranslatedID() {
		return snsTranslatedID;
	}
	public void setSnsTranslatedID(String snsTranslatedID) {
		this.snsTranslatedID = snsTranslatedID;
	}
	public Timestamp getSnsTranslatedDate() {
		return snsTranslatedDate;
	}
	public void setSnsTranslatedDate(Timestamp snsTranslatedDate) {
		this.snsTranslatedDate = snsTranslatedDate;
	}
	public String getSnsTranslatedStatus() {
		return snsTranslatedStatus;
	}
	public void setSnsTranslatedStatus(String snsTranslatedStatus) {
		this.snsTranslatedStatus = snsTranslatedStatus;
	}

}
