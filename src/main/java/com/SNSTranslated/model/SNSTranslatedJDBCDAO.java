package com.SNSTranslated.model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;


public class SNSTranslatedJDBCDAO implements SNSTranslatedDAO_interface{
	String driver = "oracle.jdbc.driver.OracleDriver";
	String url = "jdbc:oracle:thin:@localhost:1521:XE";
	String userid = "CA104G3";
	String passwd = "123456";
	
	private static final String INSERT_STMT = "INSERT INTO SNSTranslated VALUES(?,?,?)";
	private static final String UPDATE = "UPDATE SNSTranslated SET snsTranslatedContent=? WHERE snsID=? AND memberID=?";
	private static final String DELETE = "DELETE FROM SNSTranslated WHERE snsID=? AND memberID=?";
	private static final String GET_ONE_STMT = "SELECT * FROM SNSTranslated WHERE snsID=? AND memberID=?";
	
	@Override
	public void insert(SNSTranslatedVO snsTranslatedVO) {
		Connection con = null;
		PreparedStatement pstmt = null;
		
		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(INSERT_STMT);

			pstmt.setString(1, snsTranslatedVO.getSnsID());
			pstmt.setString(2, snsTranslatedVO.getMemberID());
			pstmt.setString(3, snsTranslatedVO.getSnsTranslatedContent());

			pstmt.executeUpdate();

		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}		
	}

	@Override
	public void update(SNSTranslatedVO snsTranslatedVO) {
		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(UPDATE);
			
			pstmt.setString(1, snsTranslatedVO.getSnsID());
			pstmt.setString(2, snsTranslatedVO.getMemberID());
			pstmt.setString(3, snsTranslatedVO.getSnsTranslatedContent());

			pstmt.executeUpdate();

		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}		
	}

	@Override
	public void delete(String snsTranslatedID) {
		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(DELETE);
			
			pstmt.setString(1, snsTranslatedID);

			pstmt.executeUpdate();

			
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
		} catch (SQLException se) {
			if (con != null) {
				try {
					con.rollback();
				} catch (SQLException excep) {
					throw new RuntimeException("rollback error occured. "
							+ excep.getMessage());
				}
			}
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}		
	}

	@Override
	public SNSTranslatedVO findByPrimaryKey(String snsTranslatedID) {
		SNSTranslatedVO snsTranslatedVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(GET_ONE_STMT);

			pstmt.setString(1, snsTranslatedID);

			rs = pstmt.executeQuery();

			while (rs.next()) {
				snsTranslatedVO = new SNSTranslatedVO();
				snsTranslatedVO.setSnsID(rs.getString("snsID"));
				snsTranslatedVO.setMemberID(rs.getString("memberID"));
				snsTranslatedVO.setSnsTranslatedContent(rs.getString("snsTranslatedContent"));

			}

		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return snsTranslatedVO;
	}

	@Override
	public List<SNSTranslatedVO> getAll(String snsID) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SNSTranslatedVO getTrans(String snsID, String memberID, String snsTranslatedDate) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void updateStatus(SNSTranslatedVO snsTranslatedVO) {
		// TODO Auto-generated method stub
		
	}

}
