package com.SNSTranslated.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

public class SNSTranslatedJNDIDAO implements SNSTranslatedDAO_interface{
	private static DataSource ds = null;
	static {
		try {
			Context ctx = new InitialContext();
			ds = (DataSource) ctx.lookup("java:comp/env/jdbc/CA104G3DB");
		} catch (NamingException e) {
			e.printStackTrace();
		}
	}
	private static final String INSERT_STMT = "INSERT INTO SNSTranslated VALUES(?,?,?)";
	private static final String UPDATE = "UPDATE SNSTranslated SET snsTranslatedContent=? WHERE snsID=? AND memberID=?";
	private static final String DELETE = "DELETE FROM SNSTranslated WHERE snsID=? AND memberID=?";
	private static final String GET_ONE_STMT = "SELECT * FROM SNSTranslated WHERE snsID=? AND memberID=?";
	
	@Override
	public void insert(SNSTranslatedVO snsTranslatedVO) {
		Connection con = null;
		PreparedStatement pstmt = null;
		
		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(INSERT_STMT);

			pstmt.setString(1, snsTranslatedVO.getSnsID());
			pstmt.setString(2, snsTranslatedVO.getMemberID());
			pstmt.setString(3, snsTranslatedVO.getSnsTranslatedContent());

			pstmt.executeUpdate();

		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}		
	}

	@Override
	public void update(SNSTranslatedVO snsTranslatedVO) {
		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(UPDATE);
			
			pstmt.setString(1, snsTranslatedVO.getSnsID());
			pstmt.setString(2, snsTranslatedVO.getMemberID());
			pstmt.setString(3, snsTranslatedVO.getSnsTranslatedContent());

			pstmt.executeUpdate();

		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}		
	}

	@Override
	public void delete(String snsTranslatedID) {
		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(DELETE);
			
			pstmt.setString(1, snsTranslatedID);

			pstmt.executeUpdate();

			
		} catch (SQLException se) {
			if (con != null) {
				try {
					con.rollback();
				} catch (SQLException excep) {
					throw new RuntimeException("rollback error occured. "
							+ excep.getMessage());
				}
			}
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}		
	}

	@Override
	public SNSTranslatedVO findByPrimaryKey(String snsTranslatedID) {
		SNSTranslatedVO snsTranslatedVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ONE_STMT);

			pstmt.setString(1, snsTranslatedID);

			rs = pstmt.executeQuery();

			while (rs.next()) {
				snsTranslatedVO = new SNSTranslatedVO();
				snsTranslatedVO.setSnsID(rs.getString("snsID"));
				snsTranslatedVO.setMemberID(rs.getString("memberID"));
				snsTranslatedVO.setSnsTranslatedContent(rs.getString("snsTranslatedContent"));

			}

		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return snsTranslatedVO;
	}

	@Override
	public List<SNSTranslatedVO> getAll(String snsID) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SNSTranslatedVO getTrans(String snsID, String memberID, String snsTranslatedDate) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void updateStatus(SNSTranslatedVO snsTranslatedVO) {
		// TODO Auto-generated method stub
		
	}
}
