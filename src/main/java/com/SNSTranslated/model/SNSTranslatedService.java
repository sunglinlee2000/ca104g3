package com.SNSTranslated.model;

import java.util.List;

public class SNSTranslatedService {

	private SNSTranslatedDAO_interface dao;
	
	public SNSTranslatedService() {
		dao = new SNSTranslatedDAO();
	}
	
	public SNSTranslatedVO addSNSTranslated(String snsID,String memberID,String snsTranslatedContent) {
		SNSTranslatedVO snsTranslatedVO = new SNSTranslatedVO();
		
		snsTranslatedVO.setSnsID(snsID);
		snsTranslatedVO.setMemberID(memberID);
		snsTranslatedVO.setSnsTranslatedContent(snsTranslatedContent);
		dao.insert(snsTranslatedVO);
		
		return snsTranslatedVO;
	}
	
	public SNSTranslatedVO updateSNSTranslated(String snsTranslatedID,String snsTranslatedContent) {
		SNSTranslatedVO snsTranslatedVO = new SNSTranslatedVO();
		
		snsTranslatedVO.setSnsTranslatedID(snsTranslatedID);
		snsTranslatedVO.setSnsTranslatedContent(snsTranslatedContent);
		dao.update(snsTranslatedVO);
		
		return snsTranslatedVO;
	}
	
	public SNSTranslatedVO updateSNSTranslatedStatus(String snsTranslatedID,String snsTranslatedStatus) {
		SNSTranslatedVO snsTranslatedVO = new SNSTranslatedVO();
		
		snsTranslatedVO.setSnsTranslatedID(snsTranslatedID);
		snsTranslatedVO.setSnsTranslatedStatus(snsTranslatedStatus);
		dao.updateStatus(snsTranslatedVO);
		
		return snsTranslatedVO;
	}
	
	public void deleteSNSTranslated(String snsTranslatedID) {
		dao.delete(snsTranslatedID);
	}
	
	public SNSTranslatedVO getOneSNSTranslated(String snsTranslatedID) {
		return dao.findByPrimaryKey(snsTranslatedID);
	}
	
	public SNSTranslatedVO getTrans(String snsID,String memberID, String snsTranslatedContent) {
		return dao.getTrans(snsID, memberID,snsTranslatedContent);
	}
	
	public List<SNSTranslatedVO> getAllSNSTranslated(String snsID){
		return dao.getAll(snsID);
	}
}
