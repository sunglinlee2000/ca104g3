package com.SNSTranslated.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;

import com.SNSTranslated.model.SNSTranslatedService;
import com.SNSTranslated.model.SNSTranslatedVO;

public class SNSTranslatedServlet extends HttpServlet{
	private static final long serialVersionUID = 1L;
	public void doGet(HttpServletRequest req,HttpServletResponse res) 
			throws ServletException,IOException{
		doPost(req,res);
	}
	public void doPost(HttpServletRequest req,HttpServletResponse res) 
				throws ServletException,IOException {
		req.setCharacterEncoding("UTF-8");
		res.setContentType("text/html; charset=UTF-8");
		String action = req.getParameter("action");
		
	 
		if("getOne_For_Display".equals(action)) {
			
			List<String> errorMsgs = new LinkedList<String>();
			req.setAttribute("errorMsgs", errorMsgs);
			
			try {
				/***************************1.接收請求參數 - 輸入格式的錯誤處理**********************/
				String snsTranslatedID = req.getParameter("snsTranslatedID");
				
				/***************************2.開始查詢資料*****************************************/
				SNSTranslatedService snsTranslatedSvc = new SNSTranslatedService();
				SNSTranslatedVO snsTranslatedVO = snsTranslatedSvc.getOneSNSTranslated(snsTranslatedID);
				if (snsTranslatedVO == null) {
					errorMsgs.add("查無資料");
				}
				// Send the use back to the form, if there were errors
				if (!errorMsgs.isEmpty()) {
					RequestDispatcher failureView = req
							.getRequestDispatcher("/front-end/SNS/listOneSNS.jsp");
					failureView.forward(req, res);
					return;//程式中斷
				}
				/***************************3.查詢完成,準備轉交(Send the Success view)*************/
				
				req.setAttribute("snsTranslatedVO", snsTranslatedVO); // 資料庫取出的empVO物件,存入req
				String url = "/front-end/SNS/listOneSNS.jsp";
				RequestDispatcher successView = req.getRequestDispatcher(url); // 成功轉交 listOneEmp.jsp
				successView.forward(req, res);

				/***************************其他可能的錯誤處理*************************************/
			} catch (Exception e) {
				errorMsgs.add("無法取得資料:" + e.getMessage());
				RequestDispatcher failureView = req
						.getRequestDispatcher("/front-end/SNS/listOneSNS.jsp");
				failureView.forward(req, res);
			}
		}
		
		
		if ("getOne_For_Update".equals(action)) { // 來自listAllEmp.jsp的請求

			List<String> errorMsgs = new LinkedList<String>();
			req.setAttribute("errorMsgs", errorMsgs);
			
			try {
				/***************************1.接收請求參數****************************************/
				String snsTranslatedID = req.getParameter("snsTranslatedID");
				/***************************2.開始查詢資料****************************************/
				SNSTranslatedService snsTranslatedSvc = new SNSTranslatedService();
				SNSTranslatedVO snsTranslatedVO = snsTranslatedSvc.getOneSNSTranslated(snsTranslatedID);
								
				/***************************3.查詢完成,準備轉交(Send the Success view)************/
				req.setAttribute("snsTranslatedVO", snsTranslatedVO);         // 資料庫取出的empVO物件,存入req
				String url = "/front-end/SNS/listOneSNS.jsp";
				RequestDispatcher successView = req.getRequestDispatcher(url);// 成功轉交 update_emp_input.jsp
				successView.forward(req, res);

				/***************************其他可能的錯誤處理**********************************/
			} catch (Exception e) {
				errorMsgs.add("無法取得要修改的資料:" + e.getMessage());
				RequestDispatcher failureView = req
						.getRequestDispatcher("/front-end/SNS/listOneSNS.jsp");
				failureView.forward(req, res);
			}
		}
		
		
		if ("update".equals(action)) {
		
				/***************************1.接收請求參數 - 輸入格式的錯誤處理**********************/
				String snsTranslatedID = req.getParameter("snsTranslatedID");
				String snsTranslatedContent = req.getParameter("snsTranslatedContent");
				JSONObject obj = new JSONObject();	

				SNSTranslatedVO snsTranslatedVO = new SNSTranslatedVO();
				snsTranslatedVO.setSnsTranslatedID(snsTranslatedID);
				snsTranslatedVO.setSnsTranslatedContent(snsTranslatedContent);


				SNSTranslatedService snsTranslatedSvc = new SNSTranslatedService();
				snsTranslatedVO = snsTranslatedSvc.updateSNSTranslated(snsTranslatedID, snsTranslatedContent);
		
				res.setContentType("text/plain");
				res.setCharacterEncoding("UTF-8");
				PrintWriter out = res.getWriter();
				out.write(obj.toString());
				out.flush();
				out.close();
		}		
		
		
		if ("insert".equals(action)) { 
			
			System.out.println("進入新增翻譯");
				/***********************1.接收請求參數 - 輸入格式的錯誤處理*************************/
				String snsID = req.getParameter("snsID");
				System.out.println(snsID);
				String memberID = req.getParameter("memberID");
				System.out.println(memberID);
				String snsTranslatedContent = req.getParameter("snsTranslatedContent");
				System.out.println(snsTranslatedContent);
				
				
				JSONObject obj = new JSONObject();				

				SNSTranslatedVO snsTranslatedVO = new SNSTranslatedVO();
				snsTranslatedVO.setSnsID(snsID);
				snsTranslatedVO.setMemberID(memberID);
				snsTranslatedVO.setSnsTranslatedContent(snsTranslatedContent);

				System.out.println("準備查詢");
				
				/***************************2.開始新增資料***************************************/
				SNSTranslatedService snsTranslatedSvc = new SNSTranslatedService();
				snsTranslatedVO = snsTranslatedSvc.addSNSTranslated(snsID, memberID,snsTranslatedContent );
				System.out.println("開始查詢");
				SNSTranslatedVO snsTranslatedVO1 = snsTranslatedSvc.getTrans(snsID, memberID, snsTranslatedContent);
				System.out.println("查詢結束");
				String snsTranslatedID = snsTranslatedVO1.getSnsTranslatedID();
				System.out.println("Servlet:"+snsTranslatedID);
				String Content = snsTranslatedVO1.getSnsTranslatedContent();
				System.out.println("Servlet:"+Content);
				try {
					obj.accumulate("id", snsTranslatedID);
					obj.accumulate("content", Content);
				} catch (JSONException e) {
					e.printStackTrace();
				}
				
				res.setContentType("text/plain");
				res.setCharacterEncoding("UTF-8");
				PrintWriter out = res.getWriter();
				out.write(obj.toString());
				out.flush();
				out.close();
		}				
		
		


		if ("delete".equals(action)) { 
				System.out.println("delete in");
				String snsTranslatedID = new String(req.getParameter("snsTranslatedID"));
				JSONObject obj = new JSONObject();					
				SNSTranslatedService snsTranslatedSvc = new SNSTranslatedService();
				snsTranslatedSvc.deleteSNSTranslated(snsTranslatedID);
				res.setContentType("text/plain");
				res.setCharacterEncoding("UTF-8");
				PrintWriter out = res.getWriter();
				out.write(obj.toString());
				out.flush();
				out.close();				
		}	
	
	
	
	
	}
	
}
