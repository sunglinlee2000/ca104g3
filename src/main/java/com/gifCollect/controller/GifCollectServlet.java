package com.gifCollect.controller;

import java.io.IOException;
import java.io.InputStream;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import com.gifCollect.model.*;
import com.member.model.MemberVO;


@MultipartConfig(fileSizeThreshold = 1024*1024, maxFileSize = 5*1024*1024, maxRequestSize = 5*5*1024*1024)
public class GifCollectServlet extends HttpServlet{
	
	public void doGet(HttpServletRequest req,HttpServletResponse res)
	        throws ServletException, IOException{
		doPost(req, res);
	}
	
	public void doPost(HttpServletRequest req, HttpServletResponse res) 
			 throws ServletException, IOException{
		
		req.setCharacterEncoding("UTF-8");
		String action = req.getParameter("action");
		System.out.println("gifCollect="+action);
		
		
		if("insert".equals(action)) {
			
			List<String> errorMsgs = new LinkedList<String>();
			// Store this set in the request scope, in case we need to
			// send the ErrorPage view.
			req.setAttribute("errorMsgs", errorMsgs);

			try {
				
				String gifID = req.getParameter("gifID");
				String memberID = req.getParameter("memberID");
	
				GifCollectVO gifCollectVO = new GifCollectVO();
				gifCollectVO.setGifID(gifID);
				gifCollectVO.setMemberID(memberID);
				
				
				// Send the use back to the form, if there were errors
				if (!errorMsgs.isEmpty()) {
					req.setAttribute("gifCollectVO", gifCollectVO); // 含有輸入格式錯誤的empVO物件,也存入req
					RequestDispatcher failureView = req
							.getRequestDispatcher("/front-end/gif_collection/gif_list.jsp");
					failureView.forward(req, res);
					return;
				}
				
				GifCollectService gifCollectService = new GifCollectService();
				gifCollectVO = gifCollectService.addGifCollect(gifID, memberID);
				
				res.sendRedirect(req.getContextPath() + "/front-end/gif_collection/gif_list.jsp");
				
				
			}
			catch (Exception e) {
				System.out.println("???");
				errorMsgs.add("此貼圖已重複加入過");
				RequestDispatcher failureView = req
						.getRequestDispatcher("/front-end/gif_collection/gif_list.jsp");
				failureView.forward(req, res);
			}
						
		}
		
		
		
		if ("delete".equals(action)) { // 來自listAllEmp.jsp

			List<String> errorMsgs = new LinkedList<String>();
			// Store this set in the request scope, in case we need to
			// send the ErrorPage view.
			req.setAttribute("errorMsgs", errorMsgs);
	
			try {
				/***************************1.接收請求參數***************************************/
				String gifID = req.getParameter("gifID");			
				String memberID = req.getParameter("memberID");
				
				/***************************2.開始刪除資料***************************************/
				GifCollectService gifCollectSvc = new GifCollectService();
				gifCollectSvc.deleteGifCollect(gifID, memberID);
				
				/***************************3.刪除完成,準備轉交(Send the Success view)***********/								
				String url = "/front-end/gif_collection/gif_my_list.jsp";
				RequestDispatcher successView = req.getRequestDispatcher(url);// 刪除成功後,轉交回送出刪除的來源網頁
				successView.forward(req, res);
				
				/***************************其他可能的錯誤處理**********************************/
			} catch (Exception e) {
				errorMsgs.add("刪除資料失敗:"+e.getMessage());
				RequestDispatcher failureView = req
						.getRequestDispatcher("/front-end/gif_collection/gif_my_list.jsp");
				failureView.forward(req, res);
			}
		}
	}
}


