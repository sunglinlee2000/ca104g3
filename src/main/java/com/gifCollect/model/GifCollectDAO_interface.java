package com.gifCollect.model;

import java.util.List;

public interface GifCollectDAO_interface {
	public void insert(GifCollectVO gifcVO);
    public void delete(String gifID, String memberID);
    public List<GifCollectVO> getAll(String memberID);

}
