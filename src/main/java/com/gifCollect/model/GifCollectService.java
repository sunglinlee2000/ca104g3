package com.gifCollect.model;

import java.util.List;

public class GifCollectService {
	
	private GifCollectDAO_interface dao;
	
	public GifCollectService() {
		dao = new GifCollectDAO();
	}

	public GifCollectVO addGifCollect(String gifID, String memberID) {
		
		GifCollectVO gifCollectVO = new GifCollectVO();
		
		gifCollectVO.setGifID(gifID);
		gifCollectVO.setMemberID(memberID);
		dao.insert(gifCollectVO);
		
		return gifCollectVO;
	}
	
	public void deleteGifCollect(String gifID, String memberID) {
		dao.delete(gifID, memberID);
	}
	
	public List<GifCollectVO> getAllGifCollect(String memberID){
		return dao.getAll(memberID);
	}
}
