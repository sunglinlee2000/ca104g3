package com.gifCollect.model;

public class GifCollectVO implements java.io.Serializable{
	private String gifID;
	private String memberID;
	
	public String getGifID() {
		return gifID;
	}
	public void setGifID(String gifID) {
		this.gifID = gifID;
	}
	public String getMemberID() {
		return memberID;
	}
	public void setMemberID(String memberID) {
		this.memberID = memberID;
	}
	
    
	
	

}
