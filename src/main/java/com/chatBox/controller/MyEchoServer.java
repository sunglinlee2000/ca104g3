package com.chatBox.controller;

import java.io.*;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.Gson;

import jedis.ChatMessage;
import jedis.JedisHandleMessage;
import redis.clients.jedis.Jedis;

import javax.websocket.Session;
import javax.websocket.OnOpen;
import javax.websocket.OnMessage;
import javax.websocket.OnError;
import javax.websocket.OnClose;
import javax.websocket.CloseReason;

@ServerEndpoint("/MyEchoServer/{chatBoxID}/{memberNickName}")
public class MyEchoServer {
	
	private static final Set<Session> allSessions = Collections.synchronizedSet(new HashSet<Session>());
	private static final Map<String, Set<Session>> sessionsMap = new ConcurrentHashMap<>();
	Gson gson = new Gson();
	
	@OnOpen
	public void onOpen(@PathParam("chatBoxID") String chatBoxID, @PathParam("memberNickName") String memberNickName,
			Session userSession) throws IOException {
		
		allSessions.add(userSession);
		sessionsMap.put(chatBoxID, allSessions);
		System.out.println(userSession.getId() + ": 已連線");
//		userSession.getBasicRemote().sendText("WebSocket 連線成功");
		
//		傳送歷史訊息
		List<String> historyData = JedisHandleMessage.getHistoryMsg(chatBoxID);
		String historyMsg = gson.toJson(historyData);
		ChatMessage cmHistory = new ChatMessage("history", chatBoxID, memberNickName, historyMsg);
		if (userSession != null && userSession.isOpen()) {
			userSession.getAsyncRemote().sendText(gson.toJson(cmHistory));
			return;
		}
		
	}

	
	@OnMessage
	public void onMessage(@PathParam("chatBoxID") String chatBoxID, Session userSession, String message) throws JSONException {
		ChatMessage chatMessage = gson.fromJson(message, ChatMessage.class);
		
		Set<Session> receiverSessions = sessionsMap.get(chatBoxID);
		System.out.println("set size = " + receiverSessions.size());
		for(Session receiverSession: receiverSessions) {
			if (receiverSession != null && receiverSession.isOpen()) {
				receiverSession.getAsyncRemote().sendText(message);
			}
		}
		JedisHandleMessage.saveChatMessage(chatBoxID, message);
		System.out.println("Message received: " + message);
	}
	
	@OnError
	public void onError(Session userSession, Throwable e){
//		e.printStackTrace();
	}
	
	@OnClose
	public void onClose(Session userSession, CloseReason reason) {
		Set<String> memberNickNames = sessionsMap.keySet();
		for (String memberNickName : memberNickNames) {
			if (sessionsMap.get(memberNickName).equals(userSession)) {
				sessionsMap.remove(memberNickName);
				break;
			}
		}
		System.out.println(userSession.getId() + ": Disconnected: " + Integer.toString(reason.getCloseCode().getCode()));
	}

 
}