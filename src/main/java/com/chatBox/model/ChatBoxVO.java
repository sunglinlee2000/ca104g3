package com.chatBox.model;

public class ChatBoxVO implements java.io.Serializable {
	private String chatBoxID;
	private String chatBoxName;
	private String chatBoxStatus;

	public String getChatBoxID() {
		return chatBoxID;
	}

	public void setChatBoxID(String chatBoxID) {
		this.chatBoxID = chatBoxID;
	}

	public String getChatBoxName() {
		return chatBoxName;
	}

	public void setChatBoxName(String chatBoxName) {
		this.chatBoxName = chatBoxName;
	}

	public String getChatBoxStatus() {
		return chatBoxStatus;
	}

	public void setChatBoxStatus(String chatBoxStatus) {
		this.chatBoxStatus = chatBoxStatus;
	}

}
