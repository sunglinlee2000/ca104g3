package com.chatBox.model;

import java.sql.Connection;
import java.util.List;

import com.groupList.model.GroupListVO;

public class ChatBoxService {

	private ChatBoxDAO_interface dao;

	public ChatBoxService() {
		dao = new ChatBoxDAO();
	}

	public ChatBoxVO addChatBox(String chatBoxName, String chatBoxStatus) {
		ChatBoxVO chatBoxVO = new ChatBoxVO();

		chatBoxVO.setChatBoxName(chatBoxName);
		chatBoxVO.setChatBoxStatus(chatBoxStatus);
		dao.insert(chatBoxVO);

		return chatBoxVO;
	}

	public ChatBoxVO updateChatBox(String chatBoxID, String chatBoxName, String chatBoxStatus) {
		ChatBoxVO chatBoxVO = new ChatBoxVO();

		chatBoxVO.setChatBoxID(chatBoxID);
		chatBoxVO.setChatBoxName(chatBoxName);
		chatBoxVO.setChatBoxStatus(chatBoxStatus);
		dao.update(chatBoxVO);

		return chatBoxVO;
	}

	public ChatBoxVO getOneChatBox(String chatBoxID) {
		return dao.findByPrimaryKey(chatBoxID);
	}

	public List<ChatBoxVO> getAll() {
		return dao.getAll();
	}
	
	public void addChatBox_becomeFriending(String memberID, String friendID, String chatBoxStatus, Connection con) {
		dao.addChatBox_becomeFriending(memberID, friendID, chatBoxStatus, con);
	}
	
	public String getChatBoxName(String chatBoxID, String memberID) {
		return dao.getChatBoxName(chatBoxID, memberID);
	}
	
	public String addChatBox_newGroup(GroupListVO groupListVO, String chatBoxStatus, Connection con) {
		return dao.addChatBox_newGroup(groupListVO, chatBoxStatus, con);
	}

}
