package com.android.sns.model;
import java.sql.Timestamp;
import java.util.List;

public interface SNSDAO_interface {
		public void insert(SNSVO snsVO);
		public SNSVO findByPrimaryKey(String snsID);
		public SNSVO checkSNS(String artistID,Timestamp snsDate);
		public List<SNSVO> getAll();
		public List<SNSVO> getOne(String artistID);
		public List<SNSVO> getSNSList(String boardID);
		public byte[] getImage(String snsID);
		
}
