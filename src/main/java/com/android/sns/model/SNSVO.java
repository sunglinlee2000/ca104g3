package com.android.sns.model;
import java.sql.Timestamp;


public class SNSVO implements java.io.Serializable{
	private String snsID;
	private String artistID;
	private String snsContent;
	private Timestamp snsDate;
	private String time;
	private String snsPhotoURL;
	private byte[] snsPhoto;
	
	public String getSnsID() {
		return snsID;
	}
	public void setSnsID(String snsID) {
		this.snsID = snsID;
	}
	public String getArtistID() {
		return artistID;
	}
	public void setArtistID(String artistID) {
		this.artistID = artistID;
	}
	public String getSnsContent() {
		return snsContent;
	}
	public void setSnsContent(String snsContent) {
		this.snsContent = snsContent;
	}
	public Timestamp getSnsDate() {
		return snsDate;
	}
	public void setSnsDate(Timestamp snsDate) {
		this.snsDate = snsDate;
	}
	public byte[] getSnsPhoto() {
		return snsPhoto;
	}
	public void setSnsPhoto(byte[] snsPhoto) {
		this.snsPhoto = snsPhoto;
	}
	public String getSnsPhotoURL() {
		return snsPhotoURL;
	}
	public void setSnsPhotoURL(String snsPhotoURL) {
		this.snsPhotoURL = snsPhotoURL;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
}
