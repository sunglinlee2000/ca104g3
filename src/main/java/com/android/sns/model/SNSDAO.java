package com.android.sns.model;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

public class SNSDAO implements SNSDAO_interface{
	private static DataSource ds = null;
	static {
		try {
			Context ctx = new InitialContext();
			ds = (DataSource) ctx.lookup("java:comp/env/jdbc/CA104G3DB");
		} catch (NamingException e) {
			e.printStackTrace();
		}
	}
	
	private static final String INSERT_STMT = "INSERT INTO SNS VALUES"
			+ "('SNS'||LPAD(TO_CHAR(SNS_SEQ.NEXTVAL),6,0),?,?,?,?)";
	private static final String GET_ONE_STMT = "SELECT * FROM SNS WHERE SnsID=?";
	private static final String GET_ALL_STMT = "SELECT * FROM SNS ORDER BY SnsID";
	private static final String GET_ONE = "SELECT * FROM SNS WHERE artistID=? ORDER BY snsDate";
	private static final String CHECK_SNS = "SELECT snsID,artistID,snsDate FROM SNS WHERE artistID=? AND snsDate=?";
	/********************************************************************/
	private static final String GET_SNS_BY_BOARD = "SELECT * FROM SNS WHERE ARTISTID IN (SELECT ARTISTID FROM ARTIST WHERE BOARDID= ?)";
	private static final String GET_IMAGE = "SELECT SNSPHOTO FROM SNS WHERE SNSID = ?";
	/********************************************************************/
	
	

	@Override
	public void insert(SNSVO snsVO) {
//		Connection con = null;
//		PreparedStatement pstmt = null;
//		
//		byte[] photo = null;
//		try {
//			URL url = new URL(snsVO.getSnsPhotoURL());
//			ByteArrayOutputStream baos = new ByteArrayOutputStream();
//			InputStream is = null;
//			
//			try {
//				is= url.openStream();
//				byte[] byteChunk = new byte[8192];
//				int n ;
//				while((n = is.read(byteChunk))>0) {
//					baos.write(byteChunk,0,n);
//				}
//			} catch (IOException e) {
//				e.printStackTrace();
//			}
//			
//			photo = baos.toByteArray();
//		} catch (MalformedURLException e1) {
//			e1.printStackTrace();
//		}
//		
//		String time = snsVO.getTime();
//		System.out.println("DAO發出訊息"+time);
//		String [] str = time.split("T|Z");
//		time = (str[0]+" "+str[1]);
//		Timestamp ts = Timestamp.valueOf(time);
//		
//		
////		=========================判斷是否重覆============================
//		
//		SNSService SNSSvc = new SNSService();
//		SNSVO snsVO1 = (SNSVO)SNSSvc.checkSNS(snsVO.getArtistID(), ts);
//		
//		if(snsVO1 == null) {
//		
////		==============================================================
//			try {
//				con = ds.getConnection();
//				pstmt = con.prepareStatement(INSERT_STMT);
//				
//				Blob blob = null;
//				try {
//					blob = con.createBlob();
//					blob.setBytes(1, photo);
//				} catch (SQLException e) {
//					e.printStackTrace();
//				}
//				
//	
//				pstmt.setString(1, snsVO.getArtistID());
//				pstmt.setString(2, snsVO.getSnsContent());
//				pstmt.setTimestamp(3, ts);
//				pstmt.setBlob(4, blob);
//		
//				pstmt.executeUpdate();			
//				
//				
//			} catch (SQLException se) {
//				throw new RuntimeException("A database error occured." + se.getMessage());
//			
//			} finally {
//				if (pstmt != null) {
//					try {
//						pstmt.close();
//					} catch (SQLException se) {
//						se.printStackTrace(System.err);
//					}
//				}
//				if(con != null) {
//					try {
//						con.close();
//					} catch (SQLException se) {
//						se.printStackTrace(System.err);
//					}
//				}
//			}
//		}else {
//			System.out.println("文章已存在");
//		}
	}


	@Override
	public SNSVO findByPrimaryKey(String snsID) {
		SNSVO snsVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		try {
			
			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ONE_STMT);
			
			pstmt.setString(1, snsID);
			
			rs = pstmt.executeQuery();
			
			while (rs.next()) {
				snsVO = new SNSVO();
				snsVO.setSnsID(rs.getString("snsID"));
				snsVO.setArtistID(rs.getString("artistID"));
				snsVO.setSnsContent(rs.getString("snsContent"));
				snsVO.setSnsDate(rs.getTimestamp("snsDate"));
				snsVO.setSnsPhoto(rs.getBytes("snsPhoto"));
			}
			
			
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured." + se.getMessage());
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			} 
			if (con != null) {
				try {
					con.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
		}
		return snsVO;
	}

	@Override
	public List<SNSVO> getAll() {
		List<SNSVO> list = new ArrayList<SNSVO>();
		SNSVO snsVO = null;
		
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		try {
			
			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ALL_STMT);
			rs = pstmt.executeQuery();
			
			while (rs.next()) {
				snsVO = new SNSVO();
				snsVO.setSnsID(rs.getString("snsID"));
				snsVO.setArtistID(rs.getString("artistID"));
				snsVO.setSnsContent(rs.getString("snsContent"));
				snsVO.setSnsDate(rs.getTimestamp("snsDate"));
				snsVO.setSnsPhoto(rs.getBytes("snsPhoto"));
				list.add(snsVO);
			}
			
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured." + se.getMessage());
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			} 
			if (con != null) {
				try {
					con.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
		}
		return list;
	}


	@Override
	public List<SNSVO> getOne(String artistID) {
		List<SNSVO> list = new ArrayList<SNSVO>();
		SNSVO snsVO = null;
		
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		try {
			
			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ONE);
			pstmt.setString(1, artistID);
			
			rs = pstmt.executeQuery();
			
			while (rs.next()) {
				snsVO = new SNSVO();
				snsVO.setSnsID(rs.getString("snsID"));
				snsVO.setArtistID(rs.getString("artistID"));
				snsVO.setSnsContent(rs.getString("snsContent"));
				snsVO.setSnsDate(rs.getTimestamp("snsDate"));
				snsVO.setSnsPhoto(rs.getBytes("snsPhoto"));
				list.add(snsVO);
			}
			
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured." + se.getMessage());
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			} 
			if (con != null) {
				try {
					con.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
		}
		return list;
	}


	@Override
	public SNSVO checkSNS(String artistID, Timestamp snsDate) {
		SNSVO snsVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		try {
			
			con = ds.getConnection();
			pstmt = con.prepareStatement(CHECK_SNS);
			
			pstmt.setString(1, artistID);
			pstmt.setTimestamp(2, snsDate);
			
			rs = pstmt.executeQuery();
			
			while (rs.next()) {
				snsVO = new SNSVO();
				snsVO.setSnsID(rs.getString("snsID"));
				snsVO.setArtistID(rs.getString("artistID"));
				snsVO.setSnsDate(rs.getTimestamp("snsDate"));
			}
			
			
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured." + se.getMessage());
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			} 
			if (con != null) {
				try {
					con.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
		}
		return snsVO;
	}


	@Override
	public List<SNSVO> getSNSList(String boardID) {
//		GET_SNS_BY_BOARD
		List<SNSVO> list = new ArrayList<SNSVO>();
		SNSVO snsVO = null;
		
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		try {
			
			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_SNS_BY_BOARD);
			pstmt.setString(1, boardID);
			
			rs = pstmt.executeQuery();
			
			while (rs.next()) {
				snsVO = new SNSVO();
				snsVO.setSnsID(rs.getString("snsID"));
				snsVO.setArtistID(rs.getString("artistID"));
				snsVO.setSnsContent(rs.getString("snsContent"));
				snsVO.setSnsDate(rs.getTimestamp("snsDate"));
//				snsVO.setSnsPhoto(rs.getBytes("snsPhoto"));
				list.add(snsVO);
			}
			
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured." + se.getMessage());
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			} 
			if (con != null) {
				try {
					con.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
		}
		return list;
	}


	@Override
	public byte[] getImage(String snsID) {
		byte[] productImage = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_IMAGE);
			pstmt.setString(1, snsID);
			rs = pstmt.executeQuery();
			if(rs.next()) {
				productImage = rs.getBytes(1);
			}
		}catch(SQLException se) {
			throw new RuntimeException("A database error occured. " + se.getMessage());
		}finally {
			if(rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if(pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if(con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
		return productImage;
	}
}
