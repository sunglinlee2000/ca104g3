package com.android.sns.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.android.sns.model.SNSDAO;
import com.android.sns.model.SNSDAO_interface;
import com.android.sns.model.SNSVO;
import com.android.util.ImageUtil;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

public class AndroidSNSServlet extends HttpServlet {
	private final static String CONTENT_TYPE = "text/html; charset=UTF-8";

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		ServletContext context = getServletContext();
		SNSDAO_interface dao = new SNSDAO();
		req.setCharacterEncoding("UTF-8");
		BufferedReader br = req.getReader();
		StringBuilder sb = new StringBuilder();
		String line ;
		while((line=br.readLine())!=null) {
			sb.append(line);
		}
		JsonObject jsonObject = new Gson().fromJson(sb.toString(), JsonObject.class);
		String action = jsonObject.get("action").getAsString();
		
		
		if("getSNSList".equals(action)) {
			String boardID = jsonObject.get("boardID").getAsString();
			List<SNSVO> list = dao.getSNSList(boardID);
			writeText(res, new Gson().toJson(list));
		}else if("getImage".equals(action)) {
			OutputStream os = res.getOutputStream();
			int imageSize = jsonObject.get("imageSize").getAsInt();
			String snsID = jsonObject.get("snsID").getAsString();
			byte[] image = dao.getImage(snsID);
			if(image != null) {
				image = ImageUtil.shrink(image, imageSize);
				res.setContentType("image/jpeg");
				res.setContentLength(image.length);
			}
			os.write(image);
		}
		
	}

	
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		SNSDAO_interface dao = new SNSDAO();
		List<SNSVO> list = dao.getAll();
		writeText(res, new Gson().toJson(list));
	}
	
	public void writeText(HttpServletResponse res, String outText) throws IOException {
		res.setContentType(CONTENT_TYPE);
		PrintWriter pw = res.getWriter();
		pw.print(outText);
		pw.close();
		System.out.println("output : " + outText);
	}

	
}
