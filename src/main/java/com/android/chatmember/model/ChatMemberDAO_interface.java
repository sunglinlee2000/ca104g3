package com.android.chatmember.model;

import java.sql.Connection;
import java.util.*;

import com.android.group.model.GroupVO;


public interface ChatMemberDAO_interface {
	public void insert(ChatMemberVO chatMemberVO);
//	public void update(ChatMemberVO chatMemberVO);
	public void delete(String chatBoxID, String memberID);
	public ChatMemberVO findByPrimaryKey(String chatBoxID, String memberID);
//	找會員擁有的聊天室	
	public List<ChatMemberVO> getAllByMemberID(String memberID);
	
//	回傳聊天室有的聊天室成員
	public List<ChatMemberVO> getAllByChatBoxID(String chatBoxID);
	
//	因為變成好友同時成為聊天室成員（一對一）
	public void insert_becomeFriending(String next_chatBoxID, String memberID, 
			String friendID, Connection con);
	
//	新增揪團同時創建群組
	public void insert_newGroup(String next_chatBoxID, GroupVO groupListVO, Connection con);
	
//	加入揪團同時加入群組
	public void insert_groupMember(ChatMemberVO chatMemberVO, Connection con);
	
//  回傳聊天室另一人的會員ID
	public String getAnotherMemberID(String chatBoxID, String memberID);

}
