package com.android.chatmember.model;

import java.sql.Connection;
import java.util.List;

import com.android.group.model.GroupVO;

public class ChatMemberService {

	private ChatMemberDAO_interface dao;

	public ChatMemberService() {
		dao = new ChatMemberDAO();
	}

	public ChatMemberVO addChatMember(String chatBoxID, String memberID) {
		ChatMemberVO chatMemberVO = new ChatMemberVO();

		chatMemberVO.setChatBoxID(chatBoxID);
		chatMemberVO.setMemberID(memberID);
		dao.insert(chatMemberVO);

		return chatMemberVO;
	}

	public void deleteChatMember(String chatBoxID, String memberID) {
		dao.delete(chatBoxID, memberID);
	}

	public ChatMemberVO getOneChatMember(String chatBoxID, String memberID) {
		return dao.findByPrimaryKey(chatBoxID, memberID);
	}
	
	public List<ChatMemberVO> getAllByMemberID(String memberID) {
		return dao.getAllByMemberID(memberID);
	}
	
	public void addChatMember_becomeFriending(String next_chatBoxID, String memberID, 
			String friendID, Connection con) {
		dao.insert_becomeFriending(next_chatBoxID, memberID, friendID, con);
	}
	
	public String getAnotherMemberID(String chatBoxID, String memberID) {
		return dao.getAnotherMemberID(chatBoxID, memberID);
	}
	
	public void addChatMember_newGroup(String next_chatBoxID, GroupVO groupListVO, Connection con) {
		dao.insert_newGroup(next_chatBoxID, groupListVO, con);
	}
	
	public ChatMemberVO addChatMember_groupMember(String chatBoxID, String memberID, Connection con) {
		ChatMemberVO chatMemberVO = new ChatMemberVO();

		chatMemberVO.setChatBoxID(chatBoxID);
		chatMemberVO.setMemberID(memberID);
		dao.insert(chatMemberVO);

		return chatMemberVO;
	}
}
