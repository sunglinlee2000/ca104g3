package com.android.chatmember.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.android.chatbox.model.ChatBoxDAO;
import com.android.chatbox.model.ChatBoxDAO_interface;
import com.android.chatmember.model.ChatMemberDAO;
import com.android.chatmember.model.ChatMemberDAO_interface;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

public class AndroidChatMemberServlet extends HttpServlet{
	private static final String CONTENT_TYPE = "text/html; charset=UTF-8";
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		res.setContentType(CONTENT_TYPE);
		ChatMemberDAO_interface dao = new ChatMemberDAO();
		BufferedReader br = req.getReader();
		StringBuilder sb = new StringBuilder();
		String line ;
		if((line=br.readLine())!=null) {
			sb.append(line);
		}
		JsonObject jsonObject = new Gson().fromJson(sb.toString(), JsonObject.class);
		String action = jsonObject.get("action").getAsString();
		
		if("action".equals("getChatboxID")) {
			String memberID = jsonObject.get("memberID").getAsString();
			
		}
		
	}
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		res.setContentType(CONTENT_TYPE);
		ChatMemberDAO_interface dao = new ChatMemberDAO();
		writeText(res, new Gson().toJson(dao.getAllByChatBoxID("CB000001")));
	}
	
	public void writeText(HttpServletResponse res, String outText) throws IOException {
		res.setContentType(CONTENT_TYPE);
		PrintWriter pw = res.getWriter();
		pw.write(outText);
		pw.close();
		System.out.println("output : " + outText);
	}
}
