package com.android.chatbox.controller;

import java.io.*;
import java.nio.ByteBuffer;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;

import org.json.JSONException;

import com.android.jedis.ChatMessage;
import com.android.jedis.JedisHandleMessage;
import com.google.gson.Gson;

import redis.clients.jedis.Jedis;

import javax.websocket.Session;
import javax.websocket.OnOpen;
import javax.websocket.OnMessage;
import javax.websocket.OnError;
import javax.websocket.OnClose;
import javax.websocket.CloseReason;

@ServerEndpoint("/AndroidEchoServer/{chatBoxID}/{memberNickName}")
public class AndroidEchoServer {

	private static final Set<Session> allSessions = Collections.synchronizedSet(new HashSet<Session>());
	private static final Map<String, Set<Session>> sessionsMap = new ConcurrentHashMap<>();
	Gson gson = new Gson();

	@OnOpen
	public void onOpen(@PathParam("chatBoxID") String chatBoxID, @PathParam("memberNickName") String memberNickName,
			Session userSession) throws IOException {
		int maxBufferSize = 500 * 1024;
		userSession.setMaxTextMessageBufferSize(maxBufferSize);
		userSession.setMaxBinaryMessageBufferSize(maxBufferSize);
		
		allSessions.add(userSession);
		sessionsMap.put(chatBoxID, allSessions);
		System.out.println(userSession.getId() + ": 已連線");
//		userSession.getBasicRemote().sendText("WebSocket 連線成功");

//		傳送歷史訊息
		List<String> historyData = JedisHandleMessage.getHistoryMsg(chatBoxID);
		
		String historyMsg = gson.toJson(historyData);
		String messageType = gson.fromJson(historyMsg, ChatMessage.class).getMessageType();
		ChatMessage cmHistory = new ChatMessage("history", chatBoxID, memberNickName, historyMsg, messageType);
		
		if (userSession != null && userSession.isOpen()) {
			if(messageType.equals("image")) {
				int imageLength = cmHistory.getMessageContent().getBytes().length;
				System.out.println("image Length = " + imageLength);
				userSession.getAsyncRemote().sendBinary(ByteBuffer.wrap(historyMsg.getBytes()));
			}else {
				userSession.getAsyncRemote().sendText(gson.toJson(cmHistory));
			}
		}
	}

	@OnMessage
	public void onMessage(@PathParam("chatBoxID") String chatBoxID, Session userSession, String message)
			throws JSONException {
		ChatMessage chatMessage = gson.fromJson(message, ChatMessage.class);
		String messageType = chatMessage.getMessageType();
		System.out.println("messageType = " + messageType);
		
		Set<Session> receiverSessions = sessionsMap.get(chatBoxID);
		System.out.println("set size = " + receiverSessions.size());
		for (Session receiverSession : receiverSessions) {
			if (receiverSession != null && receiverSession.isOpen()) {
				if(messageType.equals("image")) {
					int imageLength = chatMessage.getMessageContent().length();
					System.out.println("image length = " + imageLength);
					receiverSession.getAsyncRemote().sendBinary(ByteBuffer.wrap(message.getBytes()));
				}else {
					receiverSession.getAsyncRemote().sendText(message);
				}
			}
		}
		JedisHandleMessage.saveChatMessage(chatBoxID, message);
		System.out.println("Message received: " + message);
	}
	
	@OnMessage
	public void onMessage(@PathParam("chatBoxID") String chatBoxID, Session userSession, ByteBuffer bytes)
			throws JSONException {
		String message = new String(bytes.array());
		System.out.println("ByteBuffer Message received: " + message);
	}
	
	@OnError
	public void onError(Session userSession, Throwable e) {}

	@OnClose
	public void onClose(Session userSession, CloseReason reason) {
		Set<String> memberNickNames = sessionsMap.keySet();
		for (String memberNickName : memberNickNames) {
			if (sessionsMap.get(memberNickName).equals(userSession)) {
				sessionsMap.remove(memberNickName);
				break;
			}
		}
		System.out.println(userSession.getId() + ": Disconnected: " 
		+ Integer.toString(reason.getCloseCode().getCode()));
	}

}