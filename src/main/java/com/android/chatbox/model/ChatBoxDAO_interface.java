package com.android.chatbox.model;

import java.util.*;

import com.android.group.model.GroupVO;

public interface ChatBoxDAO_interface {
	public void insert(ChatBoxVO chatBoxVO);
	public void update(ChatBoxVO chatBoxVO);
//	public void delete(String chatboxid);
	public ChatBoxVO findByPrimaryKey(String chatBoxID);
	public List<ChatBoxVO> getAll();
	
//	變成朋友的創建聊天室
	public void addChatBox_becomeFriending(String memberID, String friendID, String chatBoxStatus, java.sql.Connection con);
	
//	新增揪團的創建聊天室
	public String addChatBox_newGroup(GroupVO grouptVO, String chatBoxStatus, java.sql.Connection con);
	
// 	取得聊天室名稱
	public String getChatBoxName(String chatBoxID, String memberID);
	
	public ChatBoxVO getChatBoxID(String memberID, String friendID);
	
}


