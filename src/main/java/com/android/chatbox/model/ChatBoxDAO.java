package com.android.chatbox.model;

import java.util.*;
import java.sql.*;
import javax.naming.*;
import javax.sql.*;

import com.android.chatmember.model.ChatMemberService;
import com.android.group.model.GroupVO;
import com.android.member.model.MemberService;

public class ChatBoxDAO implements ChatBoxDAO_interface {
	private static DataSource ds = null;
	static {
		try {
			Context ctx = new InitialContext();
			ds = (DataSource) ctx.lookup("java:comp/env/jdbc/CA104G3DB");
		} catch (NamingException e) {
			e.printStackTrace();
		}
	}

	private static final String INSERT_STMT = "INSERT INTO chatbox (chatBoxID, chatBoxName, chatBoxStatus) VALUES ('CB'||LPAD(to_char(chatBox_seq.NEXTVAL), 6, '0'), ?, ?)";
	private static final String GET_ALL_STMT = "SELECT chatBoxID, chatBoxName, chatBoxStatus FROM chatbox";
	private static final String GET_ONE_STMT = "SELECT chatBoxID, chatBoxName, chatBoxStatus FROM chatbox WHERE chatBoxID = ?";
//	private static final String DELETE_STMT = "DELETE FROM chatbox WHERE chatBoxID = ?";
	
	private static final String UPDATE = "UPDATE chatbox set chatBoxName = ? WHERE chatBoxID = ?";
	
	private static final String GET_CHATBOXID ="SELECT CHATMEMBER.CHATBOXID , CHATBOX.CHATBOXNAME,CHATBOX.CHATBOXSTATUS FROM CHATMEMBER JOIN CHATBOX ON CHATMEMBER.CHATBOXID = CHATBOX.CHATBOXID WHERE MEMBERID in(?, ?) AND CHATBOX.CHATBOXSTATUS = 'CHATBOX_SINGLE' GROUP BY CHATMEMBER.CHATBOXID,CHATBOX.CHATBOXSTATUS, CHATBOX.CHATBOXNAME";
	
	private static final String SINGLE = "CHATBOX_SINGLE";
	private static final String GROUP = "CHATBOX_GROUP";

	@Override
	public void insert(ChatBoxVO chatBoxVO) {
		Connection con = null;
		PreparedStatement pstmt = null;

		try {
			con = ds.getConnection();
			pstmt = con.prepareStatement(INSERT_STMT);

			pstmt.setString(1, chatBoxVO.getChatBoxName());
			pstmt.setString(2, chatBoxVO.getChatBoxStatus());

			pstmt.executeUpdate();
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. " + se.getMessage());
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}

	@Override
	public void update(ChatBoxVO chatBoxVO) {
		Connection con = null;
		PreparedStatement pstmt = null;

		try {
			con = ds.getConnection();
			pstmt = con.prepareStatement(UPDATE);

			pstmt.setString(1, chatBoxVO.getChatBoxName());
			pstmt.setString(2, chatBoxVO.getChatBoxID());

			pstmt.executeUpdate();
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. " + se.getMessage());
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}

//	@Override
//	public void delete(String chatBoxID) {
//		// TODO Auto-generated method stub
//
//	}

	@Override
	public ChatBoxVO findByPrimaryKey(String chatBoxID) {
		ChatBoxVO chatBoxVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ONE_STMT);

			pstmt.setString(1, chatBoxID);

			rs = pstmt.executeQuery();

			while (rs.next()) {
				chatBoxVO = new ChatBoxVO();
				chatBoxVO.setChatBoxID(rs.getString("chatBoxID"));
				chatBoxVO.setChatBoxName(rs.getString("chatBoxName"));
				chatBoxVO.setChatBoxStatus(rs.getString("chatBoxStatus"));
			}

		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. " + se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return chatBoxVO;
	}

	@Override
	public List<ChatBoxVO> getAll() {
		List<ChatBoxVO> list = new ArrayList<ChatBoxVO>();
		ChatBoxVO chatBoxVO = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ALL_STMT);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				chatBoxVO = new ChatBoxVO();
				chatBoxVO.setChatBoxID(rs.getString("chatBoxID"));
				chatBoxVO.setChatBoxName(rs.getString("chatBoxName"));
				chatBoxVO.setChatBoxStatus(rs.getString("chatBoxStatus"));
				list.add(chatBoxVO); // Store the row in the list
			}

			// Handle any driver errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. " + se.getMessage());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return list;
	}

	@Override
	public void addChatBox_becomeFriending(String memberID, String friendID, String chatBoxStatus, Connection con) {
		PreparedStatement pstmt = null;

		try {

//			先新增聊天室
			String cols[] = { "chatBoxID" };
			pstmt = con.prepareStatement(INSERT_STMT, cols);

			pstmt.setNull(1, java.sql.Types.VARCHAR);
			pstmt.setString(2, chatBoxStatus);

			pstmt.executeUpdate();

//			取得對應的自增主鍵
			String next_chatBoxID = null;
			ResultSet rs = pstmt.getGeneratedKeys();
			if (rs.next()) {
				next_chatBoxID = rs.getString(1);
				System.out.println("自增主鍵值= " + next_chatBoxID + "（剛新增成功的聊天室編號）");
			} else {
				System.out.println("未取得自增主鍵值");
			}
			rs.close();

//			再同時聊天室成員
			ChatMemberService chatMemberSvc = new ChatMemberService();
			chatMemberSvc.addChatMember_becomeFriending(next_chatBoxID, memberID, friendID, con);

		} catch (SQLException se) {
			if (con != null) {
				try {
					// 3●設定於當有exception發生時之catch區塊內
					System.err.print("Transaction is being ");
					System.err.println("rolled back由chatBox");
					con.rollback();
				} catch (SQLException excep) {
					throw new RuntimeException("rollback error occured. " + excep.getMessage());
				}
			}
			throw new RuntimeException("A database error occured. " + se.getMessage());
			// Clean up Connection resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}

		}

	}

	@Override
	public String getChatBoxName(String chatBoxID, String memberID) {
		ChatBoxVO chatBoxVO = findByPrimaryKey(chatBoxID);
		String chatBoxStatus = chatBoxVO.getChatBoxStatus();
		String chatBoxName = null;

		if (SINGLE.equals(chatBoxStatus)) {
			ChatMemberService chatMemberSvc = new ChatMemberService();
			String anotherMemberID = chatMemberSvc.getAnotherMemberID(chatBoxID, memberID);

			MemberService memberSvc = new MemberService();
			chatBoxName = memberSvc.getOneMember(anotherMemberID).getMemberNickName();

		} else {
			chatBoxName = chatBoxVO.getChatBoxName();
		}

		return chatBoxName;

	}

	@Override
	public String addChatBox_newGroup(GroupVO groupVO, String chatBoxStatus, Connection con) {
		PreparedStatement pstmt = null;
//		取得對應的自增主鍵
		String next_chatBoxID = null;

		try {

//			先新增聊天室
			String cols[] = { "chatBoxID" };
			pstmt = con.prepareStatement(INSERT_STMT, cols);

			pstmt.setString(1, groupVO.getGroupTitle());
			pstmt.setString(2, chatBoxStatus);

			pstmt.executeUpdate();


			ResultSet rs = pstmt.getGeneratedKeys();
			if (rs.next()) {
				next_chatBoxID = rs.getString(1);
				System.out.println("自增主鍵值= " + next_chatBoxID + "（剛新增成功的聊天室編號）");
			} else {
				System.out.println("未取得自增主鍵值");
			}
			rs.close();

//			再同時聊天室成員
			ChatMemberService chatMemberSvc = new ChatMemberService();
			chatMemberSvc.addChatMember_newGroup(next_chatBoxID, groupVO, con);
			

		} catch (SQLException se) {
			if (con != null) {
				try {
					// 3●設定於當有exception發生時之catch區塊內
					System.err.print("Transaction is being ");
					System.err.println("rolled back由chatBox");
					con.rollback();
				} catch (SQLException excep) {
					throw new RuntimeException("rollback error occured. " + excep.getMessage());
				}
			}
			throw new RuntimeException("A database error occured. " + se.getMessage());
			// Clean up Connection resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
		}
		return next_chatBoxID;
	}

	@Override
	public ChatBoxVO getChatBoxID(String memberID, String friendID) {
		ChatBoxVO chatBoxVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_CHATBOXID);
			pstmt.setString(1, memberID);
			pstmt.setString(2, friendID);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				chatBoxVO = new ChatBoxVO();
				chatBoxVO.setChatBoxID(rs.getString("chatBoxID"));
				chatBoxVO.setChatBoxName(rs.getString("chatBoxName"));
				chatBoxVO.setChatBoxStatus(rs.getString("chatBoxStatus"));
			}

		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. " + se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return chatBoxVO;
	}

}
