package com.android.groupmember.model;

import java.util.*;
import java.sql.*;
import javax.naming.*;
import javax.sql.*;

public class GroupMemberDAO implements GroupMemberDAO_interface {
	private static DataSource ds = null;
	static {
		try {
			Context ctx = new InitialContext();
			ds = (DataSource) ctx.lookup("java:comp/env/jdbc/CA104G3DB");
		} catch (NamingException e) {
			e.printStackTrace();
		}
	}

	private static final String INSERT_STMT = "INSERT INTO groupMember (groupID, memberID, groupScore, groupCoinGained) VALUES (?, ?, null, null)";
	private static final String GET_ALL_STMT = "SELECT groupID, memberID, groupScore, groupCoinGained FROM groupMember";
	private static final String GET_ONE_STMT = "SELECT groupID, memberID, groupScore, groupCoinGained FROM groupMember WHERE groupID = ? and memberID = ?";
	private static final String DELETE_STMT = "DELETE FROM groupMember WHERE groupID = ? and memberID = ?";
	private static final String UPDATE = "UPDATE groupMember set groupScore = ?, groupCoinGained = ? WHERE groupID = ? and memberID = ?";
	private static final String GET_GROUPMEMBERLIST = "SELECT * FROM GROUPMEMBER WHERE GROUPID = ?";

	@Override
	public void insert(GroupMemberVO groupMemberVO) {
		Connection con = null;
		PreparedStatement pstmt = null;

		try {
			con = ds.getConnection();
			pstmt = con.prepareStatement(INSERT_STMT);

			pstmt.setString(1, groupMemberVO.getGroupID());
			pstmt.setString(2, groupMemberVO.getMemberID());
//			pstmt.setInt(3, groupMemberVO.getGroupScore());
//			pstmt.setInt(4, groupMemberVO.getGroupCoinGained());

			pstmt.executeUpdate();
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. " + se.getMessage());
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}

	@Override
	public void update(GroupMemberVO groupMemberVO) {
		Connection con = null;
		PreparedStatement pstmt = null;

		try {
			con = ds.getConnection();
			pstmt = con.prepareStatement(UPDATE);

			pstmt.setInt(1, groupMemberVO.getGroupScore());
			pstmt.setInt(2, groupMemberVO.getGroupCoinGained());
			pstmt.setString(3, groupMemberVO.getGroupID());
			pstmt.setString(4, groupMemberVO.getMemberID());

			pstmt.executeUpdate();
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. " + se.getMessage());
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}

	@Override
	public void delete(String groupID, String memberID) {
		Connection con = null;
		PreparedStatement pstmt = null;

		try {
			con = ds.getConnection();
			pstmt = con.prepareStatement(DELETE_STMT);

			pstmt.setString(1, groupID);
			pstmt.setString(2, memberID);

			pstmt.executeUpdate();
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. " + se.getMessage());
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}

	@Override
	public GroupMemberVO findByPrimaryKey(String groupID, String memberID) {
		GroupMemberVO groupMemberVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ONE_STMT);

			pstmt.setString(1, groupID);
			pstmt.setString(2, memberID);

			rs = pstmt.executeQuery();

			while (rs.next()) {
				groupMemberVO = new GroupMemberVO();
				groupMemberVO.setGroupID(rs.getString("groupID"));
				groupMemberVO.setMemberID(rs.getString("memberID"));
				groupMemberVO.setGroupScore(rs.getInt("groupScore"));
				groupMemberVO.setGroupCoinGained(rs.getInt("groupCoinGained"));
			}
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. " + se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return groupMemberVO;
	}

	@Override
	public List<GroupMemberVO> getAll() {
		List<GroupMemberVO> list = new ArrayList<GroupMemberVO>();
		GroupMemberVO groupMemberVO = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ALL_STMT);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				groupMemberVO = new GroupMemberVO();
				groupMemberVO.setGroupID(rs.getString("groupID"));
				groupMemberVO.setMemberID(rs.getString("memberID"));
				groupMemberVO.setGroupScore(rs.getInt("groupScore"));
				groupMemberVO.setGroupCoinGained(rs.getInt("groupCoinGained"));
				list.add(groupMemberVO);
			}

			// Handle any driver errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. " + se.getMessage());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return list;
	}

	@Override
	public void insertInitiator(GroupMemberVO groupMemberVO, Connection con) {

		PreparedStatement pstmt = null;

		try {

			pstmt = con.prepareStatement(INSERT_STMT);

			pstmt.setString(1, groupMemberVO.getGroupID());
			pstmt.setString(2, groupMemberVO.getMemberID());
//			pstmt.setInt(3, groupMemberVO.getGroupScore());
//			pstmt.setInt(4, groupMemberVO.getGroupCoinGained());

			pstmt.executeUpdate();
		} catch (SQLException se) {
			if (con != null) {
				try {
					// 3●設定於當有exception發生時之catch區塊內
					System.err.print("Transaction is being ");
					System.err.println("rolled back由groupMember");
					con.rollback();
				} catch (SQLException excep) {
					throw new RuntimeException("rollback error occured. " + excep.getMessage());
				}
			}
			throw new RuntimeException("A database error occured. " + se.getMessage());
			// Clean up Connection resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}

		}

	}

	@Override
	public List<GroupMemberVO> getOneGroupList(String groupID) {
		List<GroupMemberVO> list = new ArrayList<GroupMemberVO>();
		GroupMemberVO groupMemberVO = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_GROUPMEMBERLIST);
			pstmt.setString(1, groupID);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				groupMemberVO = new GroupMemberVO();
				groupMemberVO.setGroupID(rs.getString("groupID"));
				groupMemberVO.setMemberID(rs.getString("memberID"));
				groupMemberVO.setGroupScore(rs.getInt("groupScore"));
				groupMemberVO.setGroupCoinGained(rs.getInt("groupCoinGained"));
				list.add(groupMemberVO);
			}

			// Handle any driver errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. " + se.getMessage());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return list;
	}

}
