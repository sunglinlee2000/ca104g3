package com.android.groupmember.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.android.groupmember.model.GroupMemberDAO;
import com.android.groupmember.model.GroupMemberDAO_interface;
import com.android.groupmember.model.GroupMemberVO;
import com.android.member.model.MemberDAO;
import com.android.member.model.MemberDAO_interface;
import com.android.member.model.MemberVO;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

public class AndroidGroupMemberServlet extends HttpServlet {

	private static final String CONTENT_TYPE = "text/html; charset=UTF-8";

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		ServletContext context = getServletContext();
		req.setCharacterEncoding("UTF-8");
		GroupMemberDAO_interface groupMemberDAO= new GroupMemberDAO();
		MemberDAO_interface memberDAO = new MemberDAO();
		Gson gson = new Gson();
		BufferedReader br = req.getReader();
		StringBuilder sb = new StringBuilder();
		String line ;
		while((line = br.readLine())!=null) {
			sb.append(line);
		}
		System.out.println("input : " + sb);
		JsonObject jsonObject = gson.fromJson(sb.toString(), JsonObject.class);
		
		String action = jsonObject.get("action").getAsString();
		
		if("checkMemberInGroup".equals(action)) {
			String memberID = jsonObject.get("memberID").getAsString();
			String groupID = jsonObject.get("groupID").getAsString();
			if(groupMemberDAO.findByPrimaryKey(groupID, memberID)!=null) {
				writeText(res, "true");
			}
				
		}else if("checkIn".equals(action)) {
			String memberID = jsonObject.get("memberID").getAsString();
			String groupID = jsonObject.get("groupID").getAsString();
			String groupAuthorID = jsonObject.get("groupAuthorID").getAsString();
			
			GroupMemberVO groupMemberVO = new GroupMemberVO();
			groupMemberVO.setGroupCoinGained(40);
			groupMemberVO.setMemberID(memberID);
			groupMemberVO.setGroupID(groupID);
			groupMemberVO.setGroupScore(1);
			groupMemberDAO.update(groupMemberVO);
			
			MemberVO groupAuthor = memberDAO.findByPrimaryKey(groupAuthorID);
			groupAuthor.setMemberCoin(groupAuthor.getMemberCoin()+40);
			memberDAO.updateCoin(groupAuthor);
			
			
			MemberVO memberVO = memberDAO.findByPrimaryKey(memberID);
			memberVO.setMemberCoin(memberVO.getMemberCoin()+40);
			memberDAO.update(memberVO);
			
			
		}else if("getGroupMemberList".equals(action)) {
			String groupID = jsonObject.get("groupID").getAsString();
			List<GroupMemberVO> list = groupMemberDAO.getOneGroupList(groupID);
			writeText(res, new Gson().toJson(list));
			
		}else if("isAlreadyIn".equals(action)) {
			String memberID = jsonObject.get("memberID").getAsString();
			String groupID = jsonObject.get("groupID").getAsString();
			GroupMemberVO groupMemberVO = groupMemberDAO.findByPrimaryKey(groupID, memberID);
			if(groupMemberVO.getGroupScore()==1) {
				writeText(res, "true");
			}
		}
		
		
		
		
		
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		GroupMemberDAO_interface groupMemberDAO = new GroupMemberDAO();
		List<GroupMemberVO> groupMemberList = groupMemberDAO.getAll();
		writeText(res, new Gson().toJson(groupMemberList));
	}

	public void writeText(HttpServletResponse res, String outText) throws IOException {
		res.setContentType(CONTENT_TYPE);
		PrintWriter out = res.getWriter();
		out.print(outText);
		out.close();
		System.out.println("output: " + outText);
	}

}
