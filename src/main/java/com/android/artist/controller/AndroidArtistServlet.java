package com.android.artist.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.android.artist.model.ArtistDAO;
import com.android.artist.model.ArtistDAO_interface;
import com.android.artist.model.ArtistVO;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

public class AndroidArtistServlet extends HttpServlet{
	private static final String CONTENT_TYPE = "text/html; charst=UTF-8";
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		ServletContext context = getServletContext();
		ArtistDAO_interface dao = new ArtistDAO();
		res.setCharacterEncoding("UTF-8");
		BufferedReader br = req.getReader();
		StringBuilder sb = new StringBuilder();
		String line ;
		while((line=br.readLine())!=null) {
			sb.append(line);
		}
		JsonObject jsonObject = new Gson().fromJson(sb.toString(), JsonObject.class);
		String action = jsonObject.get("action").getAsString();
		
		if("getArtistName".equals(action)) {
			String snsID = jsonObject.get("snsID").getAsString();
			String artistName = dao.getArtistName(snsID);
			writeText(res, artistName);
		}
		
		
	}
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		ArtistDAO_interface dao = new ArtistDAO();
		List<ArtistVO> list = dao.getAll();
		writeText(res, new Gson().toJson(list));
	}
	
	
	public void writeText(HttpServletResponse res, String outText) throws IOException {
		res.setContentType(CONTENT_TYPE);
		PrintWriter pw = res.getWriter();
		pw.write(outText);
		pw.close();
		System.out.println("output: " + outText);
	}

	
	
	

}
