package com.android.artist.model;

import java.sql.*;
import java.util.*;

import javax.naming.*;
import javax.sql.DataSource;

public class ArtistDAO implements ArtistDAO_interface{

	private static DataSource ds = null;
	static {
		try {
			Context ctx = new InitialContext();
			ds = (DataSource) ctx.lookup("java:comp/env/jdbc/CA104G3DB");
		} catch (NamingException e) {
			e.printStackTrace();
		}
	}

	private static final String INSERT_STMT = "INSERT INTO artist (artistID,boardID,artistName)"
			+ "VALUES ('ART'||LPAD(TO_CHAR(ARTIST_SEQ.NEXTVAL),'6','0'),?,?)";
	private static final String UPDATE = "UPDATE artist set boardID=?, artistName=? WHERE artistID = ? ";
	private static final String DELETE = "DELETE FROM artist WHERE artistID = ?";
	private static final String GET_ONE_STMT = "SELECT * FROM artist WHERE artistID = ?";
	private static final String GET_ALL_STMT = "SELECT * FROM artist";
	private static final String GET_ARTISTS_STMT = "SELECT * FROM artist WHERE boardID = ?";
	
	/**********************************************/
	private static final String GET_ARTISTNAME = "SELECT ARTISTNAME FROM ARTIST WHERE ARTISTID IN (SELECT ARTISTID FROM SNS WHERE SNSID = ?)";
	/**********************************************/
	
	

	@Override
	public void insert(ArtistVO artistVO) {
		Connection con = null;
		PreparedStatement pstmt = null;
		
		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(INSERT_STMT);

			pstmt.setString(1, artistVO.getBoardID());
			pstmt.setString(2, artistVO.getArtistName());

			pstmt.executeUpdate();

		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
	}

	@Override
	public void update(ArtistVO artistVO) {
		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(UPDATE);
			
			pstmt.setString(1, artistVO.getBoardID());
			pstmt.setString(2, artistVO.getArtistName());
			pstmt.setString(3, artistVO.getArtistID());

			pstmt.executeUpdate();

		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
	}

	@Override
	public void delete(String artistID) {

		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(DELETE);
			
			pstmt.setString(1,artistID);

			pstmt.executeUpdate();

			
		} catch (SQLException se) {
			if (con != null) {
				try {
					con.rollback();
				} catch (SQLException excep) {
					throw new RuntimeException("rollback error occured. "
							+ excep.getMessage());
				}
			}
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
	}

	@Override
	public ArtistVO findByPrimaryKey(String artistID) {
		ArtistVO artistVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ONE_STMT);

			pstmt.setString(1, artistID);

			rs = pstmt.executeQuery();

			while (rs.next()) {
				// empVo �]�٬� Domain objects
				artistVO = new ArtistVO();
				artistVO.setArtistID(rs.getString("artistID"));
				artistVO.setBoardID(rs.getString("boardID"));
				artistVO.setArtistName(rs.getString("artistName"));

			}

		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return artistVO;
	}

	@Override
	public List<ArtistVO> getArtistByBoardID(String boardID) {
		List<ArtistVO> list = new ArrayList<ArtistVO>();
		ArtistVO artistVO = null;
		
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		try {
			
			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ARTISTS_STMT);
			pstmt.setString(1, boardID);
			rs = pstmt.executeQuery();
	
			while (rs.next()) {
				artistVO = new ArtistVO();
				artistVO.setArtistID(rs.getString("artistID"));
				artistVO.setBoardID(rs.getString("boardID"));
				artistVO.setArtistName(rs.getString("artistName"));
				list.add(artistVO); // Store the row in the vector
			}
	
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		
		return list;
	}
	
	@Override
	public List<ArtistVO> getAll() {
		List<ArtistVO> list = new ArrayList<ArtistVO>();
		ArtistVO artistVO = null;
		
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		try {
			
			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ALL_STMT);
			rs = pstmt.executeQuery();
			
			while (rs.next()) {
				artistVO = new ArtistVO();
				artistVO.setArtistID(rs.getString("artistID"));
				artistVO.setBoardID(rs.getString("boardID"));
				artistVO.setArtistName(rs.getString("artistName"));

				list.add(artistVO);
			}
			
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured." + se.getMessage());
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			} 
			if (con != null) {
				try {
					con.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
		}
		
		return list;
	}

	@Override
	public String getArtistName(String snsID) {
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String artistName = null;
		try {
			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ARTISTNAME);
			pstmt.setString(1, snsID);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				artistName = rs.getString("ARTISTNAME");
			}
	
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return artistName;
	}

	
}
