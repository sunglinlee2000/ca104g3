package com.android.group.model;

import java.sql.Connection;
import java.util.*;

public interface GroupDAO_interface {
	public void insert(GroupVO groupVO);
	public void update(GroupVO groupVO);
	public void update_noLonger(GroupVO groupVO, Connection con);
//	public void delete(String groupid);
	public GroupVO findByPrimaryKey(String groupID);
	public List<GroupVO> getAllByBoardID(String boardID);
	public List<GroupVO> getAllByBoardID(Map<String, String[]> map);
	public List<GroupVO> getAllByMemberID(String memberID);
	public List<GroupVO> getListByBoardID(String boardID);
	public List<GroupVO> getMyList(String memberID);
	

}
