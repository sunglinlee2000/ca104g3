package com.android.group.model;

import java.util.*;
import java.sql.*;
import javax.naming.*;
import javax.sql.*;

public class GroupDAO implements GroupDAO_interface {
	private static DataSource ds = null;
	static {
		try {
			Context ctx = new InitialContext();
			ds = (DataSource) ctx.lookup("java:comp/env/jdbc/CA104G3DB");
		} catch (NamingException e) {
			e.printStackTrace();
		}
	}

	private static final String INSERT_STMT = "INSERT INTO groupList (groupID, memberID, boardID, groupTitle, groupLocation, latitude, longitude, "
			+ "groupStartDate, groupMeetDate, groupContent, groupStatus) "
			+ "VALUES ('G'||LPAD(to_char(groupList_seq.NEXTVAL), 6, '0'), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
	private static final String GET_ALL_STMT = "SELECT groupID, memberID, boardID, groupTitle, groupLocation, latitude, longitude, "
			+ "groupStartDate, groupMeetDate, groupContent, groupStatus, chatBoxID FROM groupList  WHERE boardID = ? "
			+ " and groupStatus IN ('GROUP_GROUPING', 'GROUP_GROUPED', 'GROUP_FINISHED') ORDER BY groupID DESC";
	private static final String GET_ONE_STMT = "SELECT groupID, memberID, boardID, groupTitle, groupLocation, latitude, longitude, "
			+ "groupStartDate, groupMeetDate, groupContent, groupStatus, chatBoxID FROM groupList WHERE groupID = ?";
	private static final String UPDATE = "UPDATE groupList set groupTitle = ?, groupLocation = ?, latitude = ?, "
			+ "longitude = ?, groupMeetDate = ?, groupContent = ?, groupStatus = ? WHERE groupID = ?";
	private static final String GET_ALL_ByMemberID_STMT = "SELECT groupID, memberID, boardID, groupTitle, groupLocation, latitude, longitude, "
			+ "groupStartDate, groupMeetDate, groupContent, groupStatus, chatBoxID FROM groupList  WHERE memberID = ? "
			+ " and groupStatus IN ('GROUP_GROUPING', 'GROUP_GROUPED', 'GROUP_FINISHED') ORDER BY groupID DESC";
	private static final String UPDATE_CHATBOXID = "UPDATE groupList set chatBoxID = ? WHERE groupID = ?";
	private static final String UPDATE_GROUPSTATUS = "UPDATE groupList set groupStatus = ? WHERE groupID = ?";

	private static final String GROUPING = "GROUP_GROUPING";
	private static final String GROUPED = "GROUP_GROUPED";
	private static final String FAILED = "GROUP_FAILED";
	private static final String FINISHED = "GROUP_FINISHED";
	/*******************************************************************************************/
	private static final String GET_ONE_LIST_STMT =
			"SELECT * FROM GROUPLIST WHERE BOARDID = ?";
	private static final String GET_MY_LIST_STMT =
	"SELECT * FROM GROUPLIST WHERE GROUPID IN (SELECT GROUPID FROM GROUPMEMBER WHERE MEMBERID = ?)";
	/*******************************************************************************************/

	@Override
	public void insert(GroupVO groupVO) {
//		Connection con = null;
//		PreparedStatement pstmt = null;
//		PreparedStatement pstmt2 = null;
//
//		try {
//			con = ds.getConnection();
//			con.setAutoCommit(false);
//
////			先新增揪團
//			String cols[] = { "groupID" };
//			pstmt = con.prepareStatement(INSERT_STMT, cols);
//			String memberID = groupVO.getMemberID();
//
//			pstmt.setString(1, memberID);
//			pstmt.setString(2, groupVO.getBoardID());
//			pstmt.setString(3, groupVO.getGroupTitle());
//			pstmt.setString(4, groupVO.getGroupLocation());
//			pstmt.setDouble(5, groupVO.getLatitude());
//			pstmt.setDouble(6, groupVO.getLongitude());
//			pstmt.setTimestamp(7, new Timestamp(System.currentTimeMillis()));
//			pstmt.setTimestamp(8, groupVO.getGroupMeetDate());
//			pstmt.setString(9, groupVO.getGroupContent());
//			pstmt.setString(10, GROUPING);
//
//			pstmt.executeUpdate();
//
////			取得對應的自增主鍵
//			String next_groupID = null;
//			ResultSet rs = pstmt.getGeneratedKeys();
//			if (rs.next()) {
//				next_groupID = rs.getString(1);
//				System.out.println("自增主鍵值= " + next_groupID + "（剛新增成功的揪團編號）");
//			} else {
//				System.out.println("未取得自增主鍵值");
//			}
//			rs.close();
//
////			再同時新增主揪
//			GroupMemberService groupMemberSvc = new GroupMemberService();
//			groupMemberSvc.addGroupInitiator(next_groupID, memberID, con);
//			
////			再創建群組聊天室
//			ChatBoxService chatBoxSvc = new ChatBoxService();
//			String chatBoxID = chatBoxSvc.addChatBox_newGroup(groupVO, "CHATBOX_GROUP", con);
//			
//			pstmt2 = con.prepareStatement(UPDATE_CHATBOXID);
//			pstmt2.setString(1, chatBoxID);
//			pstmt2.setString(2, next_groupID);
//			pstmt2.executeUpdate();
//
////			2●設定於 pstm.executeUpdate()之後
//			con.commit();
//			con.setAutoCommit(true);
//			System.out.println(memberID + "會員創建揪團且為主揪");
//
//		} catch (SQLException se) {
//			if (con != null) {
//				try {
//					// 3●設定於當有exception發生時之catch區塊內
//					System.err.print("Transaction is being ");
//					System.err.println("rolled back-由-groupList");
//					con.rollback();
//				} catch (SQLException excep) {
//					throw new RuntimeException("rollback error occured. " + excep.getMessage());
//				}
//			}
//			throw new RuntimeException("A database error occured. " + se.getMessage());
//		} finally {
//			if (pstmt != null) {
//				try {
//					pstmt.close();
//				} catch (SQLException se) {
//					se.printStackTrace(System.err);
//				}
//			}
//			if (con != null) {
//				try {
//					con.close();
//				} catch (Exception e) {
//					e.printStackTrace(System.err);
//				}
//			}
//		}

	}

	@Override
	public void update(GroupVO groupVO) {
		Connection con = null;
		PreparedStatement pstmt = null;

		try {
			con = ds.getConnection();
			pstmt = con.prepareStatement(UPDATE);

//			pstmt.setString(1, "M000001");
//			pstmt.setString(2, "B000001");
			pstmt.setString(1, groupVO.getGroupTitle());
			pstmt.setString(2, groupVO.getGroupLocation());
			pstmt.setDouble(3, groupVO.getLatitude());
			pstmt.setDouble(4, groupVO.getLongitude());
			pstmt.setTimestamp(5, groupVO.getGroupMeetDate());
			pstmt.setString(6, groupVO.getGroupContent());
			pstmt.setString(7, GROUPING);
			pstmt.setString(8, groupVO.getGroupID());

			pstmt.executeUpdate();
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. " + se.getMessage());
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}
	
	@Override
	public void update_noLonger(GroupVO groupVO, Connection con) {
		PreparedStatement pstmt = null;
		
		try {

			pstmt = con.prepareStatement(UPDATE_GROUPSTATUS);
			pstmt.setString(1, FAILED);
			pstmt.setString(2, groupVO.getGroupID());
			pstmt.executeUpdate();

		} catch (SQLException se) {
			if (con != null) {
				try {
					// 3●設定於當有exception發生時之catch區塊內
					System.err.print("Transaction is being ");
					System.err.println("rolled back由groupList");
					con.rollback();
				} catch (SQLException excep) {
					throw new RuntimeException("rollback error occured. " + excep.getMessage());
				}
			}
			throw new RuntimeException("A database error occured. " + se.getMessage());
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
		}
		
		
	}

	@Override
	public GroupVO findByPrimaryKey(String groupID) {
		GroupVO groupVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ONE_STMT);

			pstmt.setString(1, groupID);

			rs = pstmt.executeQuery();

			while (rs.next()) {
				groupVO = new GroupVO();
				groupVO.setGroupID(rs.getString("groupID"));
				groupVO.setMemberID(rs.getString("memberID"));
				groupVO.setBoardID(rs.getString("boardID"));
				groupVO.setGroupTitle(rs.getString("groupTitle"));
				groupVO.setGroupLocation(rs.getString("groupLocation"));
				groupVO.setLatitude(rs.getDouble("latitude"));
				groupVO.setLongitude(rs.getDouble("longitude"));
				groupVO.setGroupStartDate(rs.getTimestamp("groupStartDate"));
				groupVO.setGroupMeetDate(rs.getTimestamp("groupMeetDate"));
				groupVO.setGroupContent(rs.getString("groupcontent"));
				groupVO.setGroupStatus(rs.getString("groupstatus"));
				groupVO.setChatBoxID(rs.getString("chatBoxID"));
			}

		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. " + se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return groupVO;

	}

	@Override
	public List<GroupVO> getAllByBoardID(String boardID) {
		List<GroupVO> list = new ArrayList<GroupVO>();
		GroupVO groupVO = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ALL_STMT);

			pstmt.setString(1, boardID);

			rs = pstmt.executeQuery();

			while (rs.next()) {
				groupVO = new GroupVO();
				groupVO.setGroupID(rs.getString("groupID"));
				groupVO.setMemberID(rs.getString("memberID"));
				groupVO.setBoardID(rs.getString("boardID"));
				groupVO.setGroupTitle(rs.getString("groupTitle"));
				groupVO.setGroupLocation(rs.getString("groupLocation"));
				groupVO.setLatitude(rs.getDouble("latitude"));
				groupVO.setLongitude(rs.getDouble("longitude"));
				groupVO.setGroupStartDate(rs.getTimestamp("groupStartDate"));
				groupVO.setGroupMeetDate(rs.getTimestamp("groupMeetDate"));
				groupVO.setGroupContent(rs.getString("groupcontent"));
				groupVO.setGroupStatus(rs.getString("groupstatus"));
				groupVO.setChatBoxID(rs.getString("chatBoxID"));
				list.add(groupVO);
			}

			// Handle any driver errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. " + se.getMessage());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return list;
	}

	@Override
	public List<GroupVO> getAllByBoardID(Map<String, String[]> map) {
		List<GroupVO> list = new ArrayList<GroupVO>();
		GroupVO groupVO = null;
//
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
//
//		try {
//
//			con = ds.getConnection();
//			String finalSQL = "select * from groupList " + jdbcUtil_GroupByQuery.get_WhereCondition(map)
//					+ " and groupStatus IN ('GROUP_GROUPING', 'GROUP_GROUPED', 'GROUP_FINISHED') order by groupID DESC";
//			pstmt = con.prepareStatement(finalSQL);
//			System.out.println("●●finalSQL(by DAO) = " + finalSQL);
//			rs = pstmt.executeQuery();
//
//			while (rs.next()) {
//				groupVO = new GroupVO();
//				groupVO.setGroupID(rs.getString("groupID"));
//				groupVO.setMemberID(rs.getString("memberID"));
//				groupVO.setBoardID(rs.getString("boardID"));
//				groupVO.setGroupTitle(rs.getString("groupTitle"));
//				groupVO.setGroupLocation(rs.getString("groupLocation"));
//				groupVO.setLatitude(rs.getDouble("latitude"));
//				groupVO.setLongitude(rs.getDouble("longitude"));
//				groupVO.setGroupStartDate(rs.getTimestamp("groupStartDate"));
//				groupVO.setGroupMeetDate(rs.getTimestamp("groupMeetDate"));
//				groupVO.setGroupContent(rs.getString("groupcontent"));
//				groupVO.setGroupStatus(rs.getString("groupstatus"));
//				groupVO.setChatBoxID(rs.getString("chatBoxID"));
//				list.add(groupVO);
//			}
//
//			// Handle any driver errors
//		} catch (SQLException se) {
//			throw new RuntimeException("A database error occured. " + se.getMessage());
//		} finally {
//			if (rs != null) {
//				try {
//					rs.close();
//				} catch (SQLException se) {
//					se.printStackTrace(System.err);
//				}
//			}
//			if (pstmt != null) {
//				try {
//					pstmt.close();
//				} catch (SQLException se) {
//					se.printStackTrace(System.err);
//				}
//			}
//			if (con != null) {
//				try {
//					con.close();
//				} catch (Exception e) {
//					e.printStackTrace(System.err);
//				}
//			}
//		}
		return list;
	}

	@Override
	public List<GroupVO> getAllByMemberID(String memberID) {
		List<GroupVO> list = new ArrayList<GroupVO>();
		GroupVO groupVO = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ALL_ByMemberID_STMT);

			pstmt.setString(1, memberID);

			rs = pstmt.executeQuery();

			while (rs.next()) {
				groupVO = new GroupVO();
				groupVO.setGroupID(rs.getString("groupID"));
				groupVO.setMemberID(rs.getString("memberID"));
				groupVO.setBoardID(rs.getString("boardID"));
				groupVO.setGroupTitle(rs.getString("groupTitle"));
				groupVO.setGroupLocation(rs.getString("groupLocation"));
				groupVO.setLatitude(rs.getDouble("latitude"));
				groupVO.setLongitude(rs.getDouble("longitude"));
				groupVO.setGroupStartDate(rs.getTimestamp("groupStartDate"));
				groupVO.setGroupMeetDate(rs.getTimestamp("groupMeetDate"));
				groupVO.setGroupContent(rs.getString("groupcontent"));
				groupVO.setGroupStatus(rs.getString("groupstatus"));
				groupVO.setChatBoxID(rs.getString("chatBoxID"));
				list.add(groupVO);
			}

			// Handle any driver errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. " + se.getMessage());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return list;

	}

	@Override
	public List<GroupVO> getListByBoardID(String boardID) {
		List<GroupVO> list = new ArrayList<GroupVO>();
		GroupVO groupVO = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ONE_LIST_STMT);

			pstmt.setString(1, boardID);

			rs = pstmt.executeQuery();

			while (rs.next()) {
				groupVO = new GroupVO();
				groupVO.setGroupID(rs.getString("groupID"));
				groupVO.setMemberID(rs.getString("memberID"));
				groupVO.setBoardID(rs.getString("boardID"));
				groupVO.setGroupTitle(rs.getString("groupTitle"));
				groupVO.setGroupLocation(rs.getString("groupLocation"));
				groupVO.setLatitude(rs.getDouble("latitude"));
				groupVO.setLongitude(rs.getDouble("longitude"));
				groupVO.setGroupStartDate(rs.getTimestamp("groupStartDate"));
				groupVO.setGroupMeetDate(rs.getTimestamp("groupMeetDate"));
				groupVO.setGroupContent(rs.getString("groupcontent"));
				groupVO.setGroupStatus(rs.getString("groupstatus"));
				groupVO.setChatBoxID(rs.getString("chatBoxID"));
				list.add(groupVO);
			}

			// Handle any driver errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. " + se.getMessage());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return list;
	}

	@Override
	public List<GroupVO> getMyList(String memberID) {
		List<GroupVO> list = new ArrayList<GroupVO>();
		GroupVO groupVO = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_MY_LIST_STMT);

			pstmt.setString(1, memberID);

			rs = pstmt.executeQuery();

			while (rs.next()) {
				groupVO = new GroupVO();
				groupVO.setGroupID(rs.getString("groupID"));
				groupVO.setMemberID(rs.getString("memberID"));
				groupVO.setBoardID(rs.getString("boardID"));
				groupVO.setGroupTitle(rs.getString("groupTitle"));
				groupVO.setGroupLocation(rs.getString("groupLocation"));
				groupVO.setLatitude(rs.getDouble("latitude"));
				groupVO.setLongitude(rs.getDouble("longitude"));
				groupVO.setGroupStartDate(rs.getTimestamp("groupStartDate"));
				groupVO.setGroupMeetDate(rs.getTimestamp("groupMeetDate"));
				groupVO.setGroupContent(rs.getString("groupcontent"));
				groupVO.setGroupStatus(rs.getString("groupstatus"));
				groupVO.setChatBoxID(rs.getString("chatBoxID"));
				list.add(groupVO);
			}

			// Handle any driver errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. " + se.getMessage());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return list;
	}



}
