package com.android.group.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.android.group.model.GroupDAO;
import com.android.group.model.GroupDAO_interface;
import com.android.group.model.GroupVO;
import com.android.issue.model.IssueVO;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

public class AndroidGroupServlet extends HttpServlet {
	private final static String CONTENT_TYPE = "text/html; charset=UTF-8";

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		ServletContext context = getServletContext();
		req.setCharacterEncoding("UTF-8");
		GroupDAO_interface groupI = new GroupDAO();
		Gson gson = new Gson();
		BufferedReader br = req.getReader();
		StringBuilder jsonIn = new StringBuilder();

		String line = null;
		while ((line = br.readLine()) != null) {
			jsonIn.append(line);
		}

		System.out.println("input: " + jsonIn);
		JsonObject jsonObject = gson.fromJson(jsonIn.toString(), JsonObject.class);
		String action = jsonObject.get("action").getAsString();
		if ("getGroupList".equals(action)) {
			String boardID = jsonObject.get("boardID").getAsString();
			List<GroupVO> groupList = groupI.getListByBoardID(boardID);
			writeText(res, gson.toJson(groupList));
		} else if("getMyGroupList".equals(action)){
			String memberID = jsonObject.get("memberID").getAsString();
			List<GroupVO> groupList = groupI.getMyList(memberID);
			writeText(res, gson.toJson(groupList));
		}else {
			writeText(res, "");
		}

	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		GroupDAO_interface groupI = new GroupDAO();
		List<GroupVO> groupList = groupI.getAllByBoardID("B000001");
		writeText(res, new Gson().toJson(groupList));
	}

	private void writeText(HttpServletResponse res, String outText) throws IOException {
		res.setContentType(CONTENT_TYPE);
		PrintWriter out = res.getWriter();
		out.print(outText);
		out.close();
		System.out.println("outText: " + outText);
	}

}
