package com.android.friend.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.android.friend.model.FriendDAO;
import com.android.friend.model.FriendDAO_interface;
import com.android.friend.model.FriendVO;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

public class AndroidFriendServlet extends HttpServlet{
	private static final String CONTENT_TYPE = "text/html; charset=UTF-8";

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		res.setContentType(CONTENT_TYPE);
		ServletContext context = getServletContext();
		FriendDAO_interface dao = new FriendDAO(); 
		BufferedReader br = req.getReader();
		StringBuilder sb = new StringBuilder();
		String line ;
		
		while((line=br.readLine())!=null) 
			sb.append(line);
		
		System.out.println("input : " + sb.toString());
		
		JsonObject jsonObject = new Gson().fromJson(sb.toString(), JsonObject.class);
		String action = jsonObject.get("action").getAsString();
		
		if("getFriendList".equals(action)) {
			String thirdPartyID = jsonObject.get("thirdPartyID").getAsString();
			List<FriendVO> list = dao.getMyFriendList(thirdPartyID);
			writeText(res, new Gson().toJson(list));
		}
	}
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		FriendDAO_interface dao = new FriendDAO();
		List<FriendVO> list = dao.getAll();
		writeText(res, new Gson().toJson(list));
	}	
	
	public void writeText(HttpServletResponse res, String outText) throws IOException {
		res.setContentType(CONTENT_TYPE);
		PrintWriter pw = res.getWriter();
		pw.print(outText);
		pw.close();
		System.out.println("outText : " + outText);
	}
	

}
