package com.android.friend.model;

import java.util.*;
import java.sql.*;
import javax.naming.*;
import javax.sql.*;

import com.android.chatbox.model.ChatBoxService;

public class FriendDAO implements FriendDAO_interface {
	private static DataSource ds = null;
	static {
		try {
			Context ctx = new InitialContext();
			ds = (DataSource) ctx.lookup("java:comp/env/jdbc/CA104G3DB");
		} catch (NamingException e) {
			e.printStackTrace();
		}
	}

	private static final String INSERT_STMT = "INSERT INTO friend (memberID, friendID, friendStatus) VALUES (?, ?, ?)";
	private static final String GET_ALL_STMT = "SELECT memberID, friendID, friendStatus FROM friend";
	private static final String GET_ONE_STMT = "SELECT memberID, friendID, friendStatus FROM friend WHERE memberID = ? and friendID = ?";
	private static final String DELETE_STMT = "DELETE FROM friend WHERE memberID = ? and friendID = ?";
	private static final String UPDATE = "UPDATE friend set friendStatus = ? where memberID = ? and friendID = ?";
	private static final String GET_FRIENDs_ByMemberID_STMT = "SELECT * FROM friend WHERE memberID = ? and friendStatus = 'FRIEND_FRIENDING' "
			+ "order by friendID";
	
	private static final String APPLYING = "FRIEND_APPLYING";
	private static final String NOT_YET = "ANSWER_NOT_YET";
	private static final String FRIENDING = "FRIEND_FRIENDING";
	private static final String REJECTED = "FRIEND_REJECTED";
	private static final String BLACKLIST = "FRIEND_BLACKLIST";
	
	private static final String GET_MY_FRIENDLIST = "SELECT * FROM FRIEND WHERE MEMBERID IN (SELECT MEMBERID FROM MEMBERLIST WHERE Thirdpartyid = ?)";


	@Override
	public void insert(FriendVO friendVO) {
		Connection con = null;
		PreparedStatement pstmt = null;

		try {
			con = ds.getConnection();
			pstmt = con.prepareStatement(INSERT_STMT);

			pstmt.setString(1, friendVO.getMemberID());
			pstmt.setString(2, friendVO.getFriendID());
			pstmt.setString(3, friendVO.getFriendStatus());

			pstmt.executeUpdate();
		}  catch (SQLException se) {
			throw new RuntimeException("A database error occured. " + se.getMessage());
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
	}

//	@OverrIDe
//	public voID update(FriendVO friendVO) {
//		// TODO Auto-generated method stub
//		
//	}

	@Override
	public void delete(String memberID, String friendID) {
		Connection con = null;
		PreparedStatement pstmt = null;

		try {
			con = ds.getConnection();
			pstmt = con.prepareStatement(DELETE_STMT);

			pstmt.setString(1, memberID);
			pstmt.setString(2, friendID);

			pstmt.executeUpdate();
		}  catch (SQLException se) {
			throw new RuntimeException("A database error occured. " + se.getMessage());
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}

	@Override
	public FriendVO findByPrimaryKey(String memberID, String friendID) {
		FriendVO friendVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ONE_STMT);

			pstmt.setString(1, memberID);
			pstmt.setString(2, friendID);

			rs = pstmt.executeQuery();

			while (rs.next()) {
				friendVO = new FriendVO();
				friendVO.setMemberID(rs.getString("memberID"));
				friendVO.setFriendID(rs.getString("friendID"));
				friendVO.setFriendStatus(rs.getString("friendStatus"));
			}

		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. " + se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return friendVO;

	}

	@Override
	public List<FriendVO> getAll() {
		List<FriendVO> list = new ArrayList<FriendVO>();
		FriendVO friendVO = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ALL_STMT);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				friendVO = new FriendVO();
				friendVO.setMemberID(rs.getString("memberID"));
				friendVO.setFriendID(rs.getString("friendID"));
				list.add(friendVO); // Store the row in the list
			}

			// Handle any driver errors
		}  catch (SQLException se) {
			throw new RuntimeException("A database error occured. " + se.getMessage());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return list;

	}

	@Override
	public void update(FriendVO friendVO) {
		Connection con = null;
		PreparedStatement pstmt = null;

		try {
			con = ds.getConnection();
			pstmt = con.prepareStatement(UPDATE);
			
			pstmt.setString(1, friendVO.getFriendStatus());
			pstmt.setString(2, friendVO.getMemberID());
			pstmt.setString(3, friendVO.getFriendID());
			
			pstmt.executeUpdate();
		}  catch (SQLException se) {
			throw new RuntimeException("A database error occured. " + se.getMessage());
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		
	}

	@Override
	public Set<FriendVO> getFriendsByMemberID(String memberID) {
		Set<FriendVO> set = new LinkedHashSet<FriendVO>();
		FriendVO friendVO = null;
		
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_FRIENDs_ByMemberID_STMT);
			pstmt.setString(1, memberID);
			rs = pstmt.executeQuery();
			
			while (rs.next()) {
				friendVO = new FriendVO();
				friendVO.setMemberID(rs.getString("memberID"));
				friendVO.setFriendID(rs.getString("friendID"));
				friendVO.setFriendStatus(rs.getString("friendStatus"));
				set.add(friendVO);
			}
		}  catch (SQLException se) {
			throw new RuntimeException("A database error occured. " + se.getMessage());
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return set;
	}

	@Override
	public void becomeFriending(String memberID, String friendID, String friendStatus) {
		Connection con = null;
		PreparedStatement pstmt1 = null;
		PreparedStatement pstmt2 = null;

		try {
			con = ds.getConnection();
			con.setAutoCommit(false);
			
//			雙方先變成朋友
			pstmt1 = con.prepareStatement(UPDATE);
			pstmt1.setString(1, friendStatus);
			pstmt1.setString(2, memberID);
			pstmt1.setString(3, friendID);
			pstmt1.executeUpdate();
			
			pstmt2 = con.prepareStatement(UPDATE);
			pstmt2.setString(1, friendStatus);
			pstmt2.setString(2, friendID);
			pstmt2.setString(3, memberID);
			pstmt2.executeUpdate();
			
//			再同時新增聊天室
			ChatBoxService chatBoxSvc = new ChatBoxService();
			chatBoxSvc.addChatBox_becomeFriending(memberID, friendID, "CHATBOX_SINGLE", con);
			
//			2●設定於 pstm.executeUpdate()之後
			con.commit();
			con.setAutoCommit(true);
			System.out.println("已變成好友、並創立聊天室");
			

		}  catch (SQLException se) {
			if (con != null) {
				try {
					// 3●設定於當有exception發生時之catch區塊內
					System.err.print("Transaction is being ");
					System.err.println("rolled back-由-friend");
					con.rollback();
				} catch (SQLException excep) {
					throw new RuntimeException("rollback error occured. "
							+ excep.getMessage());
				}
			}
			throw new RuntimeException("A database error occured. " + se.getMessage());
		} finally {
			if (pstmt1 != null) {
				try {
					pstmt1.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt2 != null) {
				try {
					pstmt2.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
	}

	@Override
	public List<FriendVO> getMyFriendList(String thirdPartyID) {
		// TODO Auto-generated method stub GET_MY_FRIENDLIST
		List<FriendVO> list = new ArrayList<FriendVO>();
		FriendVO friendVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_MY_FRIENDLIST);
			pstmt.setString(1, thirdPartyID);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				friendVO = new FriendVO();
				friendVO.setMemberID(rs.getString("memberID"));
				friendVO.setFriendID(rs.getString("friendID"));
				list.add(friendVO); // Store the row in the list
			}

			// Handle any driver errors
		}  catch (SQLException se) {
			throw new RuntimeException("A database error occured. " + se.getMessage());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return list;
	}

}
