package com.android.issuelike.model;

import java.util.List;

public interface IssueLikeDAO_interface {
	
	public void insert(IssueLikeVO issueLikeVO);
	public void delete(String issueID, String memberID);
	public IssueLikeVO findByPrimaryKey(String issueID, String memberID);
	public void deleteByIssueID(String issueID);	
	public boolean isLike(String memberID, String issueID);
}
