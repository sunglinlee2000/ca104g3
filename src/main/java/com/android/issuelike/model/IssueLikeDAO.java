package com.android.issuelike.model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

public class IssueLikeDAO implements IssueLikeDAO_interface{
	private static DataSource ds = null;
	static {
		try {
			Context ctx = new InitialContext();
			ds = (DataSource) ctx.lookup("java:comp/env/jdbc/CA104G3DB");
		} catch (NamingException e) {
			e.printStackTrace();
		}
	}
	private static final String INSERT_STMT =
			"INSERT INTO issueLike "
			+ "(issueID,memberID)"
			+ " VALUES "
			+ "(?,?)";
		private static final String DELETE = 
			"DELETE FROM issueLike where issueID = ? and memberID = ?";
		private static final String GET_ONE_STMT = 
			"SELECT issueID,memberID FROM issuelike where issueID = ? and memberID = ?";
		
		private static final String DELETEBYISSUEID = 
			"DELETE FROM issueLike where issueID = ?";
		private static final String CHECKISLIKED =
			"SELECT * FROM ISSUELIKE WHERE ISSUEID = ? AND MEMBERID = ?";

	
	@Override
	public void insert(IssueLikeVO issueLikeVO) {
		Connection con = null;
		PreparedStatement pstmt = null;
		
		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(INSERT_STMT);

			pstmt.setString(1, issueLikeVO.getIssueID());
			pstmt.setString(2, issueLikeVO.getMemberID());
			
			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		
	}	
	
	
	@Override
	public void delete(String issueID, String memberID) {
		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(DELETE);

			pstmt.setString(1, issueID);
			pstmt.setString(2, memberID);
			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

		
	}

	
	@Override
	public IssueLikeVO findByPrimaryKey(String issueID, String memberID) {
		IssueLikeVO issueLikeVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ONE_STMT);

			pstmt.setString(1, issueID);

			rs = pstmt.executeQuery();

			while (rs.next()) {
				// empVo �]�٬� Domain objects
				
				issueLikeVO = new IssueLikeVO();
				issueLikeVO.setIssueID(rs.getString("issueID"));
				issueLikeVO.setMemberID(rs.getString("memberID"));
				
			}

			// Handle any driver errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return issueLikeVO;
	}
	
	@Override
	public void deleteByIssueID(String issueID) {
		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(DELETEBYISSUEID);

			pstmt.setString(1, issueID);
			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

		
	}


	@Override
	public boolean isLike(String memberID, String issueID) {
		Connection con = null;
		PreparedStatement pstmt = null;
		boolean isLike = false;
		try {
			con = ds.getConnection();
			pstmt = con.prepareStatement(CHECKISLIKED);
			pstmt.setString(1, issueID);
			pstmt.setString(2, memberID);
			ResultSet rs = pstmt.executeQuery();
			if(rs.next()) {
				isLike = true;
			}
			return isLike;
		}catch(SQLException e) {
			System.out.println(e.getMessage());
		}finally {
			try {
				if (pstmt != null) {
					pstmt.close();
				}
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return isLike;
	}
}
