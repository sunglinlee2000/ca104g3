package com.android.issuelike.model;

public class IssueLikeVO implements java.io.Serializable{
	private String issueID;
	private String memberID;
	
	public String getIssueID() {
		return issueID;
	}
	public void setIssueID(String issueID) {
		this.issueID = issueID;
	}
	public String getMemberID() {
		return memberID;
	}
	public void setMemberID(String memberID) {
		this.memberID = memberID;
	}
		
}
