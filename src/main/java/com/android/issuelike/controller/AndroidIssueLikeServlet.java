package com.android.issuelike.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.android.issuelike.model.IssueLikeDAO;
import com.android.issuelike.model.IssueLikeDAO_interface;
import com.android.issuelike.model.IssueLikeVO;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

public class AndroidIssueLikeServlet extends HttpServlet{
	private final static String CONTENT_TYPE = "text/html; charset=UTF-8";

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		ServletContext context = getServletContext();
		req.setCharacterEncoding("UTF-8");
		IssueLikeDAO_interface issueLikeI = new IssueLikeDAO();
		Gson gson = new Gson();
		BufferedReader br = req.getReader();
		StringBuilder jsonIn = new StringBuilder();
		String line = null;
		while((line=br.readLine())!=null) {
			jsonIn.append(new String(line.getBytes(), "UTF-8"));
		}
		
		JsonObject jsonObject = gson.fromJson(jsonIn.toString(), JsonObject.class);
		String action = jsonObject.get("action").getAsString();
		System.out.println(action);
		
		
		if("checkLikeExist".equals(action)) {
			String memberID = jsonObject.get("memberID").getAsString();
			String issueID = jsonObject.get("issueID").getAsString();
			writeText(res, String.valueOf(issueLikeI.isLike(memberID, issueID)));
		}else if("add".equals(action)) {
			String memberID = jsonObject.get("memberID").getAsString();
			String issueID = jsonObject.get("issueID").getAsString();
			IssueLikeVO issueLikeVO = new IssueLikeVO();
			issueLikeVO.setIssueID(issueID);
			issueLikeVO.setMemberID(memberID);
			issueLikeI.insert(issueLikeVO);
			writeText(res, "Add one data in IssueLike");
		}else if("delete".equals(action)){
			String memberID = jsonObject.get("memberID").getAsString();
			String issueID = jsonObject.get("issueID").getAsString();
			issueLikeI.delete(issueID, memberID);
			writeText(res, "Delete one data in IssueLike");
		}else {
			writeText(res, "");
		}
		
	}
	
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		IssueLikeDAO_interface issueLikeI = new IssueLikeDAO();
		boolean value = issueLikeI.isLike("A", "B");
		writeText(res, new Gson().toJson(value));
		
		
	}

	private void writeText(HttpServletResponse res, String outText) throws IOException {
		res.setContentType(CONTENT_TYPE);
		PrintWriter out = res.getWriter();
		out.print(outText);
		out.close();
		System.out.println("outText: " + outText);
	}

	
	
}
