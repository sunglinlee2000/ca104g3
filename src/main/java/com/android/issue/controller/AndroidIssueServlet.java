package com.android.issue.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.android.board.model.BoardDAO;
import com.android.board.model.BoardDAO_interface;
import com.android.issue.model.IssueDAO;
import com.android.issue.model.IssueDAO_interface;
import com.android.issue.model.IssueVO;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

public class AndroidIssueServlet extends HttpServlet {
	private final static String CONTENT_TYPE = "text/html; charset=UTF-8";

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		req.setCharacterEncoding("UTF-8");
		ServletContext context = getServletContext();
		IssueDAO_interface issueI = new IssueDAO();
		Gson gson = new Gson();
		BufferedReader br = req.getReader();
		StringBuilder jsonIn = new StringBuilder();

		String line = null;
		while ((line = br.readLine()) != null) {
			jsonIn.append(line);
		}

		System.out.println("input: " + jsonIn);
		JsonObject jsonObject = gson.fromJson(jsonIn.toString(), JsonObject.class);
		String action = jsonObject.get("action").getAsString();
		
		if ("getIssueList".equals(action)) {
			String boardID = jsonObject.get("boardID").getAsString();
			List<IssueVO> issueList = issueI.getListByBoardID(boardID);
			writeText(res, gson.toJson(issueList));
		}else if("getMyIssueList".equals(action)){
			String memberID = jsonObject.get("memberID").getAsString();
			List<IssueVO> issueList = issueI.getMyList(memberID);
			writeText(res, gson.toJson(issueList));
		}else if("deleteMyIssue".equals(action)){
			
		}else if("addLikeCount".equals(action)){
			String issueID = jsonObject.get("issueID").getAsString();
			writeText(res, gson.toJson(issueI.addLikeCount(issueID)));
		}else if("removeLikeCount".equals(action)){
			String issueID = jsonObject.get("issueID").getAsString();
			writeText(res, gson.toJson(issueI.removeLikeCount(issueID)));
		}else if("getIssueLikeCount".equals(action)){
			String issueID = jsonObject.get("issueID").getAsString();
			writeText(res, gson.toJson(issueI.findByPrimaryKey(issueID).getIssueLikeCount()));
		}else {
			writeText(res, "");
		}
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		IssueDAO_interface issueI = new IssueDAO();
		List<IssueVO> issueList = issueI.getAll();
		writeText(res, new Gson().toJson(issueList));
	}

	private void writeText(HttpServletResponse res, String outText) throws IOException {
		res.setContentType(CONTENT_TYPE);
		PrintWriter out = res.getWriter();
		out.print(outText);
		out.close();
		System.out.println("outText: " + outText);
	}

}
