package com.android.issue.model;

import java.util.List;

public interface IssueDAO_interface {
	
	public void insert(IssueVO issueVO);
    public void update(IssueVO issueVO);
    public void delete(String issueid);
    public IssueVO findByPrimaryKey(String issueid);
    public List<IssueVO> getAll();
    public List<IssueVO> getListByBoardID(String boardID);
    public List<IssueVO> getMyList(String memberID);
    public boolean addLikeCount(String issueID);
    public boolean removeLikeCount(String issueID);

}
