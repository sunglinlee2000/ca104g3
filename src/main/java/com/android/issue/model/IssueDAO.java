package com.android.issue.model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import com.android.issuelike.model.IssueLikeVO;

public class IssueDAO implements IssueDAO_interface{
	private static DataSource ds = null;
	static {
		try {
			Context ctx = new InitialContext();
			ds = (DataSource) ctx.lookup("java:comp/env/jdbc/CA104G3DB");
		} catch (NamingException e) {
			e.printStackTrace();
		}
	}
	//****************************SQL******************************
	private static final String INSERT_STMT =
			"INSERT INTO issue "
			+ "(issueID,boardID,memberID,issueTitle,issueContentPhoto,issueDate,issueCoinGained,issueLikeCount)"
			+ " VALUES "
			+ "('I'||LPAD(to_char(ISSUE_seq.NEXTVAL), 6, '0'),?,?,?,?,?,?,?)";
		private static final String GET_ALL_STMT = 
			"SELECT issueID,boardID,memberID,issueTitle,issueContentPhoto,issueDate,issueCoinGained,issueLikeCount FROM issue order by issueID DESC";
		private static final String GET_ONE_STMT = 
			"SELECT issueID,boardID,memberID,issueTitle,issueContentPhoto,issueDate,issueCoinGained,issueLikeCount FROM issue where issueID = ?";
		private static final String DELETE = 
			"DELETE FROM issue where issueID = ?";
		private static final String UPDATE = 
			"UPDATE issue set memberID=?, issueTitle=?, issueContentPhoto=?,issueDate=?,issueLikeCount=?  where issueID = ?";
		private static final String GET_ONE_LIST_STMT =
			"SELECT * FROM ISSUE WHERE BOARDID = ?";
		private static final String GET_MY_LIST_STMT =
			"SELECT * FROM ISSUE WHERE MEMBERID = ?";
		private static final String ADD_LIKECOUNT =
			"UPDATE ISSUE SET ISSUELIKECOUNT = (SELECT ISSUELIKECOUNT FROM ISSUE WHERE ISSUEID = ?)+1 WHERE ISSUEID = ?";
		private static final String REMOVE_LIKECOUNT =
			"UPDATE ISSUE SET ISSUELIKECOUNT = (SELECT ISSUELIKECOUNT FROM ISSUE WHERE ISSUEID = ?)-1 WHERE ISSUEID = ?";
	//*************************************************************	
		@Override
		public void insert(IssueVO issueVO) {
			Connection con = null;
			PreparedStatement pstmt = null;
			
			try {
				
				con = ds.getConnection();
				pstmt = con.prepareStatement(INSERT_STMT);

				pstmt.setString(1, issueVO.getBoardID());
				pstmt.setString(2, issueVO.getMemberID());
				pstmt.setString(3, issueVO.getIssueTitle());
				pstmt.setString(4, issueVO.getIssueContentPhoto());
				pstmt.setTimestamp(5, issueVO.getIssueDate());
				pstmt.setInt(6, issueVO.getIssueCoinGained());
				pstmt.setInt(7, issueVO.getIssueLikeCount());
				pstmt.executeUpdate();

				// Handle any driver errors
			} catch (SQLException se) {
				throw new RuntimeException("A database error occured. "
						+ se.getMessage());
				// Clean up JDBC resources
			} finally {
				if (pstmt != null) {
					try {
						pstmt.close();
					} catch (SQLException se) {
						se.printStackTrace(System.err);
					}
				}
				if (con != null) {
					try {
						con.close();
					} catch (Exception e) {
						e.printStackTrace(System.err);
					}
				}
			}

		}

		@Override
		public void update(IssueVO issueVO) {
			Connection con = null;
			PreparedStatement pstmt = null;

			try {

				con = ds.getConnection();
				pstmt = con.prepareStatement(UPDATE);
				
				pstmt.setString(1, issueVO.getMemberID());
				pstmt.setString(2, issueVO.getIssueTitle());
				pstmt.setString(3, issueVO.getIssueContentPhoto());					
				pstmt.setTimestamp(4, issueVO.getIssueDate());
				pstmt.setInt(5, issueVO.getIssueLikeCount());
				pstmt.setString(6, issueVO.getIssueID());
				
				pstmt.executeUpdate();

				// Handle any driver errors
			} catch (SQLException se) {
				throw new RuntimeException("A database error occured. "
						+ se.getMessage());
				// Clean up JDBC resources
			} finally {
				if (pstmt != null) {
					try {
						pstmt.close();
					} catch (SQLException se) {
						se.printStackTrace(System.err);
					}
				}
				if (con != null) {
					try {
						con.close();
					} catch (Exception e) {
						e.printStackTrace(System.err);
					}
				}
			}

			
		}

		@Override
		public void delete(String issueID) {
			
			Connection con = null;
			PreparedStatement pstmt = null;

			try {

		
				con = ds.getConnection();
				pstmt = con.prepareStatement(DELETE);

				pstmt.setString(1, issueID);
				pstmt.executeUpdate();

				// Handle any driver errors
			} catch (SQLException se) {
				throw new RuntimeException("A database error occured. "
						+ se.getMessage());
				// Clean up JDBC resources
			} finally {
				if (pstmt != null) {
					try {
						pstmt.close();
					} catch (SQLException se) {
						se.printStackTrace(System.err);
					}
				}
				if (con != null) {
					try {
						con.close();
					} catch (Exception e) {
						e.printStackTrace(System.err);
					}
				}
			}

			
		}

		@Override
		public IssueVO findByPrimaryKey(String issueID) {
			IssueVO issueVO = null;
			Connection con = null;
			PreparedStatement pstmt = null;
			ResultSet rs = null;

			try {

				con = ds.getConnection();
				pstmt = con.prepareStatement(GET_ONE_STMT);

				pstmt.setString(1, issueID);

				rs = pstmt.executeQuery();

				while (rs.next()) {
					
					
					issueVO = new IssueVO();
					issueVO.setIssueID(rs.getString("issueID"));
					issueVO.setBoardID(rs.getString("boardID"));
					issueVO.setMemberID(rs.getString("memberID"));
					issueVO.setIssueTitle(rs.getString("issueTitle"));
					issueVO.setIssueContentPhoto(rs.getString("issueContentPhoto"));
					issueVO.setIssueDate(rs.getTimestamp("issueDate"));
					issueVO.setIssueCoinGained(rs.getInt("issueCoinGained"));
					issueVO.setIssueLikeCount(rs.getInt("issueLikeCount"));
				}

				// Handle any driver errors
			} catch (SQLException se) {
				throw new RuntimeException("A database error occured. "
						+ se.getMessage());
				// Clean up JDBC resources
			} finally {
				if (pstmt != null) {
					try {
						pstmt.close();
					} catch (SQLException se) {
						se.printStackTrace(System.err);
					}
				}
				if (con != null) {
					try {
						con.close();
					} catch (Exception e) {
						e.printStackTrace(System.err);
					}
				}
			}
			return issueVO;
		}

		@Override
		public List<IssueVO> getAll() {
			List<IssueVO> list = new ArrayList<IssueVO>();
			IssueVO issueVO = null;
			
			Connection con = null;
			PreparedStatement pstmt = null;
			ResultSet rs = null;
			
			try {

				con = ds.getConnection();
				pstmt = con.prepareStatement(GET_ALL_STMT);
				rs = pstmt.executeQuery();

				while (rs.next()) {
					
					issueVO = new IssueVO();				
					issueVO.setIssueID(rs.getString("issueID"));
					issueVO.setBoardID(rs.getString("boardID"));
					issueVO.setMemberID(rs.getString("memberID"));
					issueVO.setIssueTitle(rs.getString("issueTitle"));
					issueVO.setIssueContentPhoto(rs.getString("issueContentPhoto"));
					issueVO.setIssueDate(rs.getTimestamp("issueDate"));
					issueVO.setIssueCoinGained(rs.getInt("issueCoinGained"));
					issueVO.setIssueLikeCount(rs.getInt("issueLikeCount"));
					
					list.add(issueVO); // Store the row in the list
				}

				// Handle any driver errors
			} catch (SQLException se) {
				throw new RuntimeException("A database error occured. "
						+ se.getMessage());
				// Clean up JDBC resources
			} finally {
				if (pstmt != null) {
					try {
						pstmt.close();
					} catch (SQLException se) {
						se.printStackTrace(System.err);
					}
				}
				if (con != null) {
					try {
						con.close();
					} catch (Exception e) {
						e.printStackTrace(System.err);
					}
				}
			}
			return list;
		}

		@Override
		public List<IssueVO> getListByBoardID(String boardID) {
			List<IssueVO> list = new ArrayList<IssueVO>();
			IssueVO issueVO = null;
			
			Connection con = null;
			PreparedStatement pstmt = null;
			ResultSet rs = null;
			
			try {

				con = ds.getConnection();
				pstmt = con.prepareStatement(GET_ONE_LIST_STMT);
				pstmt.setString(1, boardID);
				rs = pstmt.executeQuery();

				while (rs.next()) {
					
					issueVO = new IssueVO();				
					issueVO.setIssueID(rs.getString("issueID"));
					issueVO.setBoardID(rs.getString("boardID"));
					issueVO.setMemberID(rs.getString("memberID"));
					issueVO.setIssueTitle(rs.getString("issueTitle"));
					issueVO.setIssueContentPhoto(rs.getString("issueContentPhoto"));
					issueVO.setIssueDate(rs.getTimestamp("issueDate"));
					issueVO.setIssueCoinGained(rs.getInt("issueCoinGained"));
					issueVO.setIssueLikeCount(rs.getInt("issueLikeCount"));
					list.add(issueVO); // Store the row in the list
				}

				// Handle any driver errors
			} catch (SQLException se) {
				throw new RuntimeException("A database error occured. "
						+ se.getMessage());
				// Clean up JDBC resources
			} finally {
				if (pstmt != null) {
					try {
						pstmt.close();
					} catch (SQLException se) {
						se.printStackTrace(System.err);
					}
				}
				if (con != null) {
					try {
						con.close();
					} catch (Exception e) {
						e.printStackTrace(System.err);
					}
				}
			}
			return list;
		}

		@Override
		public List<IssueVO> getMyList(String MemberID) {
			List<IssueVO> list = new ArrayList<IssueVO>();
			IssueVO issueVO = null;
			
			Connection con = null;
			PreparedStatement pstmt = null;
			ResultSet rs = null;
			
			try {

				con = ds.getConnection();
				pstmt = con.prepareStatement(GET_MY_LIST_STMT);
				pstmt.setString(1, MemberID);
				rs = pstmt.executeQuery();

				while (rs.next()) {
					
					issueVO = new IssueVO();				
					issueVO.setIssueID(rs.getString("issueID"));
					issueVO.setBoardID(rs.getString("boardID"));
					issueVO.setMemberID(rs.getString("memberID"));
					issueVO.setIssueTitle(rs.getString("issueTitle"));
					issueVO.setIssueContentPhoto(rs.getString("issueContentPhoto"));
					issueVO.setIssueDate(rs.getTimestamp("issueDate"));
					issueVO.setIssueCoinGained(rs.getInt("issueCoinGained"));
					issueVO.setIssueLikeCount(rs.getInt("issueLikeCount"));
					list.add(issueVO); // Store the row in the list
				}

				// Handle any driver errors
			} catch (SQLException se) {
				throw new RuntimeException("A database error occured. "
						+ se.getMessage());
				// Clean up JDBC resources
			} finally {
				if (pstmt != null) {
					try {
						pstmt.close();
					} catch (SQLException se) {
						se.printStackTrace(System.err);
					}
				}
				if (con != null) {
					try {
						con.close();
					} catch (Exception e) {
						e.printStackTrace(System.err);
					}
				}
			}
			return list;
		}

		@Override
		public boolean addLikeCount(String issueID) {
			Connection con = null;
			PreparedStatement pstmt = null;
			boolean isAdd = false;
			ResultSet rs = null;
			try {
				con =ds.getConnection();
				pstmt = con.prepareStatement(ADD_LIKECOUNT);
				if(issueID!=null) {
					pstmt.setString(1, issueID);
					pstmt.setString(2, issueID);
				}
				int i = pstmt.executeUpdate();
				isAdd = i>0;
				
			}catch(SQLException e) {
				throw new RuntimeException("A database error occured.");
			}finally {
				if (pstmt != null) {
					try {
						pstmt.close();
					} catch (SQLException se) {
						se.printStackTrace(System.err);
					}
				}
				if (con != null) {
					try {
						con.close();
					} catch (Exception e) {
						e.printStackTrace(System.err);
					}
				}
			}
			return isAdd;
		}

		@Override
		public boolean removeLikeCount(String issueID) {
			Connection con = null;
			PreparedStatement pstmt = null;
			boolean isRemove = false;
			ResultSet rs = null;
			try {
				con =ds.getConnection();
				pstmt = con.prepareStatement(REMOVE_LIKECOUNT);
				if(issueID!=null) {
					pstmt.setString(1, issueID);
					pstmt.setString(2, issueID);
				}
				int i = pstmt.executeUpdate();
				isRemove = i>0;
				
			}catch(SQLException e) {
				throw new RuntimeException("A database error occured.");
			}finally {
				if (pstmt != null) {
					try {
						pstmt.close();
					} catch (SQLException se) {
						se.printStackTrace(System.err);
					}
				}
				if (con != null) {
					try {
						con.close();
					} catch (Exception e) {
						e.printStackTrace(System.err);
					}
				}
			}
			return isRemove;
			
		}
}
