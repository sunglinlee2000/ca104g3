package com.android.comment.model;

import java.util.List;

public interface CommentDAO_interface {
	public void insert(CommentVO commentListVO);
    public void update(CommentVO commentListVO);
    public void delete(String commentListVO);
    public CommentVO findByPrimaryKey(String commentID);
    public List<CommentVO> getAll();
    public void deleteByIssueID(String issueID);
    public List<CommentVO> getCommentList(String issueID);
}
