package com.android.comment.model;

import java.sql.Timestamp;

public class CommentVO {
	private String commentID;
	private String memberID;
	private String issueID;
	private String gifID;
	private String commentContent;
	private Timestamp commentDate;
	
	public String getCommentID() {
		return commentID;
	}
	public void setCommentID(String commentID) {
		this.commentID = commentID;
	}
	public String getMemberID() {
		return memberID;
	}
	public void setMemberID(String memberID) {
		this.memberID = memberID;
	}
	public String getIssueID() {
		return issueID;
	}
	public void setIssueID(String issueID) {
		this.issueID = issueID;
	}
	public String getGifID() {
		return gifID;
	}
	public void setGifID(String gifID) {
		this.gifID = gifID;
	}
	public String getCommentContent() {
		return commentContent;
	}
	public void setCommentContent(String commentContent) {
		this.commentContent = commentContent;
	}
	public Timestamp getCommentDate() {
		return commentDate;
	}
	public void setCommentDate(Timestamp commentDate) {
		this.commentDate = commentDate;
	}
	
	
}
