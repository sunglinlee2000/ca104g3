package com.android.comment.model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;


public class CommentDAO implements CommentDAO_interface {
	private static DataSource ds = null;
	static {
		try {
			Context ctx = new InitialContext();
			ds = (DataSource) ctx.lookup("java:comp/env/jdbc/CA104G3DB");
		} catch (NamingException e) {
			e.printStackTrace();
		}
	}
		
	private static final String INSERT_STMT =
			"INSERT INTO commentList "
			+ "(commentID,memberID,issueID,gifID,commentContent,commentDate)"
			+ " VALUES "
			+ "('C'||LPAD(to_char(COMMENTLIST_seq.NEXTVAL), 6, '0'),?,?,?,?,?)";
		private static final String GET_ALL_STMT = 
			"SELECT commentID,memberID,issueID,gifID,commentContent,commentDate FROM commentList order by commentID DESC";
		private static final String GET_ONE_STMT = 
			"SELECT commentID,memberID,issueID,gifID,commentContent,commentDate FROM commentList where commentID = ?";
		private static final String DELETE = 
			"DELETE FROM commentList where commentID = ?";
		private static final String UPDATE = 
			"UPDATE commentList set gifID=?, commentContent=? where commentID = ?";
		private static final String DELETEBYISSUEID = 
			"DELETE FROM commentList where issueID = ?";
		private static final String GET_COMMENT_LIST = 
			"SELECT * FROM COMMENTLIST WHERE ISSUEID = ?";
	@Override
	public void insert(CommentVO commentListVO) {
		Connection con = null;
		PreparedStatement pstmt = null;
		
		try {
			con = ds.getConnection();
			pstmt = con.prepareStatement(INSERT_STMT);
			
			pstmt.setString(1, commentListVO.getMemberID());
			pstmt.setString(2, commentListVO.getIssueID());
			pstmt.setString(3, commentListVO.getGifID());
			pstmt.setString(4, commentListVO.getCommentContent());
			pstmt.setTimestamp(5, commentListVO.getCommentDate());
			
			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}

	@Override
	public void update(CommentVO commentListVO) {
		Connection con = null;
		PreparedStatement pstmt = null;		
		
		try {
			
			con = ds.getConnection();
			pstmt = con.prepareStatement(UPDATE);
			
			pstmt.setString(1, commentListVO.getGifID());
			pstmt.setString(2, commentListVO.getCommentContent());	
			pstmt.setString(3, commentListVO.getCommentID());
			
			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

		
	}
	@Override
	public void delete(String commentID) {
		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(DELETE);
			
			pstmt.setString(1, commentID);
			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

		
	}

	@Override
	public CommentVO findByPrimaryKey(String commentID) {
		CommentVO commentListVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ONE_STMT);

			pstmt.setString(1, commentID);

			rs = pstmt.executeQuery();

			while (rs.next()) {

		
				commentListVO = new CommentVO();
				commentListVO.setCommentID(rs.getString("commentID"));
				commentListVO.setMemberID(rs.getString("memberID"));
				commentListVO.setIssueID(rs.getString("issueID"));
				commentListVO.setGifID(rs.getString("gifID"));
				commentListVO.setCommentContent(rs.getString("commentContent"));
				commentListVO.setCommentDate(rs.getTimestamp("commentDate"));
			
			}

			// Handle any driver errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return commentListVO;
	}

	@Override
	public List<CommentVO> getAll() {
		List<CommentVO> list = new ArrayList<CommentVO>();
		CommentVO commentListVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ALL_STMT);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				// empVO �]�٬� Domain objects
				
				commentListVO = new CommentVO();				
				commentListVO.setCommentID(rs.getString("commentID"));
				commentListVO.setMemberID(rs.getString("memberID"));
				commentListVO.setIssueID(rs.getString("issueID"));
				commentListVO.setGifID(rs.getString("gifID"));
				commentListVO.setCommentContent(rs.getString("commentContent"));
				commentListVO.setCommentDate(rs.getTimestamp("commentDate"));
				
				
				list.add(commentListVO); // Store the row in the list
			}

			// Handle any driver errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return list;
	}
	
	@Override
	public void deleteByIssueID(String issueID) {
		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(DELETEBYISSUEID);

			pstmt.setString(1, issueID);
			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
	
	}

	@Override
	public List<CommentVO> getCommentList(String issueID) {

		List<CommentVO> list = new ArrayList<CommentVO>();
		CommentVO commentVO = null;
		
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_COMMENT_LIST);
			pstmt.setString(1, issueID);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				
				commentVO = new CommentVO();				
				commentVO.setCommentID(rs.getString("commentID"));
				commentVO.setMemberID(rs.getString("memberID"));
				commentVO.setIssueID(rs.getString("issueID"));
				commentVO.setGifID(rs.getString("gifID"));
				commentVO.setCommentContent(rs.getString("commentContent"));
				commentVO.setCommentDate(rs.getTimestamp("commentDate"));
				
				
				list.add(commentVO); // Store the row in the list
			}

			// Handle any driver errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return list;
		
	}
}
