package com.android.comment.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.android.comment.model.CommentDAO;
import com.android.comment.model.CommentDAO_interface;
import com.android.comment.model.CommentVO;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

public class AndroidCommentServlet extends HttpServlet{
	private final static String CONTENT_TYPE = "text/html; charset=UTF-8";

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		req.setCharacterEncoding("UTF-8");
		ServletContext context = getServletContext();
		CommentDAO_interface commentI = new CommentDAO();
		Gson gson = new Gson();
		BufferedReader br = req.getReader();
		StringBuilder jsonIn = new StringBuilder();
		
		String line = null;
		while((line = br.readLine()) != null) {
			jsonIn.append(line);
		}
		
		System.out.println("input: " + jsonIn);
		JsonObject jsonObject = gson.fromJson(jsonIn.toString(), JsonObject.class);
		String action = jsonObject.get("action").getAsString();
		
		if("getCommentList".equals(action)) {
			String issueID = jsonObject.get("issueID").getAsString();
			List<CommentVO> commentList = commentI.getCommentList(issueID);
			writeText(res, gson.toJson(commentList));
		}else {
			writeText(res, "");
		}
		
	}
	
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		CommentDAO_interface commentI = new CommentDAO();
		List<CommentVO> commentList = commentI.getAll();
		writeText(res, new Gson().toJson(commentList));
	}

	
	private void writeText(HttpServletResponse res, String outText) throws IOException{
		res.setContentType(CONTENT_TYPE);
		PrintWriter out = res.getWriter();
		out.print(outText);
		out.close();
		System.out.println("outText: " + outText);
	}
	
	
	
	

}
