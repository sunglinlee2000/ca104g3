package com.android.order.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.android.order.model.OrderDAO;
import com.android.order.model.OrderDAO_interface;
import com.android.order.model.OrderVO;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

public class AndroidOrderServlet extends HttpServlet{
	private static final String CONTENT_TYPE = "text/html; charset=UTF-8";

	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		ServletContext context = getServletContext();
		req.setCharacterEncoding("UTF-8");
		OrderDAO_interface dao = new OrderDAO();
		Gson gson = new Gson();
		BufferedReader br = req.getReader();
		StringBuilder jsonIn = new StringBuilder();
		String line = null;
		while((line = br.readLine())!=null) {
			jsonIn.append(new String(line.getBytes(),"UTF-8"));
		}
		
		JsonObject jsonObject = gson.fromJson(jsonIn.toString(), JsonObject.class);
		String action = jsonObject.get("action").getAsString();
		
		if("getAll".equals(action)) {
			List<OrderVO> list = dao.getAll();
			writeText(res, gson.toJson(list));
		}else if("addOrder".equals(action)) {
			String strIn = jsonObject.get("orderList").getAsString();
//			List<OrderVO> orderList = new Gson().fromJson(strIn, List.class);
//			System.out.println(orderList.toString());
			Type typeToken = new TypeToken<List<OrderVO>>() {}.getType();
			List<OrderVO> orderList = gson.fromJson(strIn, typeToken);
			System.out.println("XXX");
			System.out.println(orderList.isEmpty());
			System.out.println(orderList.size());
			for(int i = 0; i<orderList.size(); i++) {
				OrderVO order = (OrderVO)orderList.get(i);
				System.out.println(order.getBuyerAddress());
				System.out.println(order.getMemberID());
				dao.insert(order);
				writeText(res, order.getOrderID() + "Insert OK");
			}
		}else if("getMyOrderList".equals(action)) {
			String memberID = jsonObject.get("memberID").getAsString();
			List<OrderVO> myList = dao.findBySelfMemberID(memberID);
			writeText(res, gson.toJson(myList));
		}else if("receiveProduct".equals(action)) {
			String orderID = jsonObject.get("orderID").getAsString();
			writeText(res, String.valueOf(dao.updateStatus(orderID)));
		}
		
		
	
	}
	
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		OrderDAO_interface dao = new OrderDAO();
		List<OrderVO> list = dao.getAll();
		writeText(res, new Gson().toJson(list));
	
	}
	
	public void writeText(HttpServletResponse res, String outText) throws IOException {
		res.setContentType(CONTENT_TYPE);
		PrintWriter out = res.getWriter();
		out.print(outText);
		out.close();
		System.out.println("outText: " + outText);
		
	}

	
	
	
	
	
}
