package com.android.order.model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

public class OrderDAO implements OrderDAO_interface {
	
	private static DataSource ds = null;
	static {
		try {
			Context ctx = new InitialContext();
			ds = (DataSource) ctx.lookup("java:comp/env/jdbc/CA104G3DB");
		} catch (NamingException e) {
			e.printStackTrace();
		}
	}
	
	private static final String INSERT_STMT =
			"insert into ORDERLIST(ORDERID,MEMBERID,PRODUCTID,BUYAMOUNT,ORDERPRICE,ORDERDATE,MEMBERCREDITCARDNUMBER,TRANSACTIONMETHOD, BUYERADDRESS, ORDERSTATUS,buyerName,buyerPhone)"
			+"values(to_char(sysdate,'yyyymmdd')||'-'||LPAD(to_char(ORDERLIST_seq.NEXTVAL), 6, '0'),?,?,?,?,(CURRENT_TIMESTAMP),?,?,?,?,?,?)";
	private static final String UPDATE =
			"UPDATE orderlist set memberid=?,productid=?,buyamount=?,orderprice=?,orderdate=?,buyerscore=?,vendorscore=?,membercreditcardnumber=?,transactionmethod=?,discount=?,coinused=?,buyeraddress=?,orderstatus=?, buyerName=?,buyerPhone=? where orderid=?";
	private static final String DELETE = 
			"DELETE FROM orderlist where orderid = ?";
	private static final String GET_ONE_STMT = 
			"select * from orderlist where orderid=? ";
	private static final String GET_ALL_STMT =
			"select * from orderlist order by orderid DESC";
	private static final String GET_ALL_FOR_MEMBERID =
			"select * from orderlist left join product on orderlist.productid = product.productid where product.memberid =?";
	private static final String GET_SELF_FOR_MEMBERID =
			"select * from orderlist  where memberid =?";
	private static final String ALTER_STATUS = 
			"UPDATE ORDERLIST SET ORDERSTATUS = 'GOT' WHERE ORDERID = ?";
	
	
	
	
	
	@Override
	public void insert(OrderVO OrderListVO) {
		Connection con = null;
		PreparedStatement pstmt = null;
		try {
			con = ds.getConnection();
			pstmt = con.prepareStatement(INSERT_STMT);
			
			pstmt.setString(1, OrderListVO.getMemberID());
			pstmt.setString(2, OrderListVO.getOrderProductID());
			pstmt.setInt(3, OrderListVO.getBuyAmount());
			pstmt.setInt(4, OrderListVO.getOrderPrice());
//			pstmt.setTimestamp(5, OrderListVO.getOrderDate());
//			pstmt.setInt(5, OrderListVO.getBuyerScore());
//			pstmt.setInt(6, OrderListVO.getVendorScore());
			pstmt.setString(5, OrderListVO.getMemberCreditcardNumber());
			pstmt.setString(6, OrderListVO.getTransactionMethod());
//			pstmt.setInt(9, OrderListVO.getDiscount());
//			pstmt.setInt(10, OrderListVO.getCoinused());
			pstmt.setString(7, OrderListVO.getBuyerAddress());
			pstmt.setString(8, OrderListVO.getOrderStatus());
			pstmt.setString(9, OrderListVO.getBuyerName());
			pstmt.setString(10, OrderListVO.getBuyerPhone());
			pstmt.executeUpdate();
			
		}catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		
	}

	@Override
	public void update(OrderVO orderListVO) {
		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(UPDATE);

			pstmt.setString(1, orderListVO.getMemberID());
			pstmt.setString(2, orderListVO.getOrderProductID());
			pstmt.setInt(3, orderListVO.getBuyAmount());
			pstmt.setInt(4, orderListVO.getOrderPrice());
			pstmt.setTimestamp(5, orderListVO.getOrderDate());
			pstmt.setInt(6, orderListVO.getBuyerScore());
			pstmt.setInt(7, orderListVO.getVendorScore());
			pstmt.setString(8, orderListVO.getMemberCreditcardNumber());
			pstmt.setString(9, orderListVO.getTransactionMethod());
			pstmt.setInt(10, orderListVO.getDiscount());
			pstmt.setInt(11, orderListVO.getCoinused());
			pstmt.setString(12, orderListVO.getBuyerAddress());
			pstmt.setString(13, orderListVO.getOrderStatus());
			pstmt.setString(14, orderListVO.getBuyerName());
			pstmt.setString(15, orderListVO.getBuyerPhone());
			pstmt.setString(16, orderListVO.getOrderID());
			

			pstmt.executeUpdate();

			// Handle any driver errors
		}catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		
	}

	@Override
	public void delete(String orderid) {
		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(DELETE);

			pstmt.setString(1, orderid);

			pstmt.executeUpdate();

			// Handle any driver errors
		}catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		
	}

	@Override
	public OrderVO findByPrimaryKey(String orderID) {
		OrderVO orderListVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ONE_STMT);

			pstmt.setString(1, orderID);

			rs = pstmt.executeQuery();

			while (rs.next()) {
				orderListVO=new OrderVO();
				orderListVO.setOrderID(rs.getString("orderid"));
				orderListVO.setMemberID(rs.getString("memberid"));
				orderListVO.setOrderProductID(rs.getString("productid"));
				orderListVO.setBuyAmount(rs.getInt("buyamount"));
				orderListVO.setOrderPrice(rs.getInt("orderprice"));
				orderListVO.setOrderDate(rs.getTimestamp("orderdate"));
				orderListVO.setBuyerScore(rs.getInt("buyerscore"));
				orderListVO.setVendorScore(rs.getInt("vendorscore"));
				orderListVO.setMemberCreditcardNumber(rs.getString("membercreditcardnumber"));
				orderListVO.setTransactionMethod(rs.getString("transactionmethod"));
				orderListVO.setDiscount(rs.getInt("discount"));
				orderListVO.setCoinused(rs.getInt("coinused"));
				orderListVO.setBuyerAddress(rs.getString("buyeraddress"));
				orderListVO.setOrderStatus(rs.getString("orderstatus"));
				orderListVO.setBuyerName(rs.getString("buyerName"));
				orderListVO.setBuyerPhone(rs.getString("buyerPhone"));
			}

			// Handle any driver errors
		}catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return orderListVO;
	}

	@Override
	public List<OrderVO> getAll() {
		List<OrderVO> list = new ArrayList<OrderVO>();
		OrderVO OrderListVO=null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ALL_STMT);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				OrderListVO = new OrderVO();
				OrderListVO.setOrderID(rs.getString("orderid"));
				OrderListVO.setMemberID(rs.getString("memberid"));
				OrderListVO.setOrderProductID(rs.getString("productid"));
				OrderListVO.setBuyAmount(rs.getInt("buyamount"));
				OrderListVO.setOrderPrice(rs.getInt("orderprice"));
				OrderListVO.setOrderDate(rs.getTimestamp("orderdate"));
				OrderListVO.setBuyerScore(rs.getInt("buyerscore"));
				OrderListVO.setVendorScore(rs.getInt("vendorscore"));
				OrderListVO.setMemberCreditcardNumber(rs.getString("membercreditcardnumber"));
				OrderListVO.setTransactionMethod(rs.getString("transactionmethod"));
				OrderListVO.setDiscount(rs.getInt("discount"));
				OrderListVO.setCoinused(rs.getInt("coinused"));
				OrderListVO.setBuyerAddress(rs.getString("buyeraddress"));
				OrderListVO.setOrderStatus(rs.getString("orderstatus"));
				OrderListVO.setBuyerName(rs.getString("buyerName"));
				OrderListVO.setBuyerPhone(rs.getString("buyerPhone"));
				list.add(OrderListVO); // Store the row in the list
			}

			// Handle any driver errors
		}  catch (SQLException se) {
//			throw new RuntimeException("A database error occured. "
//					+ se.getMessage());
			se.printStackTrace();
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return list;
	}

	@Override
	public List<OrderVO> getAll(String memberID) {
		List<OrderVO> list = new ArrayList<OrderVO>();
		OrderVO OrderListVO=null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ALL_FOR_MEMBERID);
			pstmt.setString(1, memberID);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				OrderListVO = new OrderVO();
				OrderListVO.setOrderID(rs.getString("orderid"));
				OrderListVO.setMemberID(rs.getString("memberid"));
				OrderListVO.setOrderProductID(rs.getString("productid"));
				OrderListVO.setBuyAmount(rs.getInt("buyamount"));
				OrderListVO.setOrderPrice(rs.getInt("orderprice"));
				OrderListVO.setOrderDate(rs.getTimestamp("orderdate"));
				OrderListVO.setBuyerScore(rs.getInt("buyerscore"));
				OrderListVO.setVendorScore(rs.getInt("vendorscore"));
				OrderListVO.setMemberCreditcardNumber(rs.getString("membercreditcardnumber"));
				OrderListVO.setTransactionMethod(rs.getString("transactionmethod"));
				OrderListVO.setDiscount(rs.getInt("discount"));
				OrderListVO.setCoinused(rs.getInt("coinused"));
				OrderListVO.setBuyerAddress(rs.getString("buyeraddress"));
				OrderListVO.setOrderStatus(rs.getString("orderstatus"));
				OrderListVO.setBuyerName(rs.getString("buyerName"));
				OrderListVO.setBuyerPhone(rs.getString("buyerPhone"));
				list.add(OrderListVO); 
			}

			// Handle any driver errors
		}  catch (SQLException se) {
//			throw new RuntimeException("A database error occured. "
//					+ se.getMessage());
			se.printStackTrace();
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return list;
	}

	@Override
	public List<OrderVO> findBySelfMemberID(String memberID) {
		List<OrderVO> list = new ArrayList<OrderVO>();
		OrderVO OrderListVO=null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_SELF_FOR_MEMBERID);
			pstmt.setString(1, memberID);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				OrderListVO = new OrderVO();
				OrderListVO.setOrderID(rs.getString("orderid"));
				OrderListVO.setMemberID(rs.getString("memberid"));
				OrderListVO.setOrderProductID(rs.getString("productid"));
				OrderListVO.setBuyAmount(rs.getInt("buyamount"));
				OrderListVO.setOrderPrice(rs.getInt("orderprice"));
				OrderListVO.setOrderDate(rs.getTimestamp("orderdate"));
				OrderListVO.setBuyerScore(rs.getInt("buyerscore"));
				OrderListVO.setVendorScore(rs.getInt("vendorscore"));
				OrderListVO.setMemberCreditcardNumber(rs.getString("membercreditcardnumber"));
				OrderListVO.setTransactionMethod(rs.getString("transactionmethod"));
				OrderListVO.setDiscount(rs.getInt("discount"));
				OrderListVO.setCoinused(rs.getInt("coinused"));
				OrderListVO.setBuyerAddress(rs.getString("buyeraddress"));
				OrderListVO.setOrderStatus(rs.getString("orderstatus"));
				OrderListVO.setBuyerName(rs.getString("buyerName"));
				OrderListVO.setBuyerPhone(rs.getString("buyerPhone"));
				list.add(OrderListVO); 
			}

			// Handle any driver errors
		}  catch (SQLException se) {
//			throw new RuntimeException("A database error occured. "
//					+ se.getMessage());
			se.printStackTrace();
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return list;
	}

	@Override
	public boolean updateStatus(String orderID) {
		// TODO Auto-generated method stub ALTER_STATUS
		
		Connection con = null;
		PreparedStatement pstmt = null;
		boolean result = false;
		try {
			con = ds.getConnection();
			pstmt = con.prepareStatement(ALTER_STATUS);
			pstmt.setString(1, orderID);
			int i = pstmt.executeUpdate();
			if(i==1)
				result = true;
		}catch(SQLException se) {
			se.printStackTrace();
		}finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return result;
	}


}
