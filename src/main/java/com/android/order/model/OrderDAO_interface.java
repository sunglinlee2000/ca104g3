package com.android.order.model;


import java.util.List;


public interface OrderDAO_interface {
	public void insert(OrderVO orderListVO);
    public void update(OrderVO orderListVO);
    public void delete(String orderID);
    public OrderVO findByPrimaryKey(String orderID);
    public List<OrderVO> getAll();
    public List<OrderVO> getAll(String memberID);
    public List<OrderVO> findBySelfMemberID(String memberID);
    public boolean updateStatus(String orderID);
}
