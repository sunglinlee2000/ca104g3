package com.android.order.model;


import java.sql.Date;
import java.sql.Timestamp;

public class OrderVO implements java.io.Serializable{
	private	String		orderID;
	private	String		memberID;
	private	String		orderProductID;
	private	Integer		buyAmount;
	private	Integer		orderPrice;
	private	Timestamp	orderDate;
	private Integer		buyerScore;
	private	Integer		vendorScore;
	private	String		memberCreditcardNumber;
	private	String		transactionMethod;
	private Integer		discount;
	private Integer		coinused;
	private String		buyerAddress;
	private String		orderStatus;
	private String 		buyerName;
	private String 		buyerPhone;
	
	public String getOrderID() {
		return orderID;
	}
	public void setOrderID(String orderID) {
		this.orderID = orderID;
	}
	public String getMemberID() {
		return memberID;
	}
	public void setMemberID(String memberID) {
		this.memberID = memberID;
	}
	public String getOrderProductID() {
		return orderProductID;
	}
	public void setOrderProductID(String orderProductID) {
		this.orderProductID = orderProductID;
	}
	public Integer getBuyAmount() {
		return buyAmount;
	}
	public void setBuyAmount(Integer buyAmount) {
		this.buyAmount = buyAmount;
	}
	public Integer getOrderPrice() {
		return orderPrice;
	}
	public void setOrderPrice(Integer orderPrice) {
		this.orderPrice = orderPrice;
	}
	public Timestamp getOrderDate() {
		return orderDate;
	}
	public void setOrderDate(Timestamp orderDate) {
		this.orderDate = orderDate;
	}
	public Integer getBuyerScore() {
		return buyerScore;
	}
	public void setBuyerScore(Integer buyerScore) {
		this.buyerScore = buyerScore;
	}
	public Integer getVendorScore() {
		return vendorScore;
	}
	public void setVendorScore(Integer vendorScore) {
		this.vendorScore = vendorScore;
	}
	public String getMemberCreditcardNumber() {
		return memberCreditcardNumber;
	}
	public void setMemberCreditcardNumber(String memberCreditcardNumber) {
		this.memberCreditcardNumber = memberCreditcardNumber;
	}
	public String getTransactionMethod() {
		return transactionMethod;
	}
	public void setTransactionMethod(String transactionMethod) {
		this.transactionMethod = transactionMethod;
	}
	public Integer getDiscount() {
		return discount;
	}
	public void setDiscount(Integer discount) {
		this.discount = discount;
	}
	public Integer getCoinused() {
		return coinused;
	}
	public void setCoinused(Integer coinused) {
		this.coinused = coinused;
	}
	public String getBuyerAddress() {
		return buyerAddress;
	}
	public void setBuyerAddress(String buyerAddress) {
		this.buyerAddress = buyerAddress;
	}
	public String getOrderStatus() {
		return orderStatus;
	}
	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}
	public String getBuyerName() {
		return buyerName;
	}
	public void setBuyerName(String buyerName) {
		this.buyerName = buyerName;
	}
	public String getBuyerPhone() {
		return buyerPhone;
	}
	public void setBuyerPhone(String buyerPhone) {
		this.buyerPhone = buyerPhone;
	}
	
	
	
	
	
	
	
}
