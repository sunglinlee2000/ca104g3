package com.android.member.controller;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.android.member.model.MemberDAO;
import com.android.member.model.MemberDAO_interface;
import com.android.member.model.MemberVO;
import com.android.util.ImageUtil;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.mysql.fabric.xmlrpc.base.Member;

public class AndroidMemberServlet extends HttpServlet {
	private final static String CONTENT_TYPE = "text/html; charset=UTF-8";

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		ServletContext context = getServletContext();
//		"先後再"  不然可能會有亂碼
		req.setCharacterEncoding("UTF-8");
		MemberDAO_interface memberI = new MemberDAO();
		Gson gson = new Gson();
		BufferedReader br = req.getReader();
		StringBuilder jsonIn = new StringBuilder();
		String line = null;
		while ((line = br.readLine()) != null) {
			jsonIn.append(new String(line.getBytes(), "UTF-8"));
		}

		JsonObject jsonObject = gson.fromJson(jsonIn.toString(), JsonObject.class);
		String action = jsonObject.get("action").getAsString();
//		String memberID = jsonObject.get("memberID").getAsString();
		
		if ("getAll".equals(action)) {
			List<MemberVO> memberList = memberI.getAll();
			writeText(res, gson.toJson(memberList));
		} else if ("findByPrimayKey".equals(action)) {
			String memberID = jsonObject.get("memberID").getAsString();
			MemberVO member = memberI.findByPrimaryKey(memberID);
			writeText(res, gson.toJson(member));
		} else if("writePhotoIntoDB".equals(action)){
			
		} else if("getMemberPhoto".equals(action)) {
			OutputStream os = res.getOutputStream();
			int imageSize = jsonObject.get("imageSize").getAsInt();
			String memberID = jsonObject.get("memberID").getAsString();
			byte[] image = memberI.getMemberPhoto(memberID);
			if (image != null) {
				image = ImageUtil.shrink(image, imageSize);
				res.setContentType("image/jpeg");
				res.setContentLength(image.length);
			}
			os.write(image);
		} else if ("getMemberNickName".equals(action)) {
			String memberID = jsonObject.get("memberID").getAsString();
			String memberNickName = memberI.getMemberNickName(memberID);
			if (memberNickName != null) {
				writeText(res, memberNickName);
			}
		} else if ("isMember".equals(action)) {
			System.out.println(jsonObject.get("thirdPartyID").getAsString());
			String thirdPartyID = jsonObject.get("thirdPartyID").getAsString().replaceAll("\\\\", "");
			writeText(res, String.valueOf(memberI.isMember(thirdPartyID)));
		} else if ("add".equals(action)) {
			System.out.println(jsonObject.get("member").getAsString().replaceAll("\\\\", ""));
			System.out.println(new String(jsonObject.get("member").getAsString().replaceAll("\\\\", "").getBytes(), "UTF-8"));
			MemberVO memberVO = gson.fromJson(jsonObject.get("member").getAsString().replaceAll("\\\\", ""),MemberVO.class);
			writeText(res, String.valueOf(memberI.add(memberVO)));
		} else if("getMeByThirdPartyID".equals(action)){
			String thirdPartyID = jsonObject.get("thirdPartyID").getAsString();
			MemberVO member = memberI.getMeByThirdPartyID(thirdPartyID);
			writeText(res, gson.toJson(member));
		} else if("simpleUpdate".equals(action)){
			System.out.println(jsonObject.get("member").getAsString().replaceAll("\\\\", ""));
			MemberVO member = gson.fromJson(jsonObject.get("member").getAsString().replaceAll("\\\\", ""), MemberVO.class);
			writeText(res, String.valueOf(memberI.simpleUpdate(member)));
		} else if("setImage".equals(action)){
			byte[] bt = jsonObject.get("inputStream").getAsString().getBytes("UTF-8");
			String memberID = jsonObject.get("memberID").getAsString();
			MemberVO memberVO = memberI.findByPrimaryKey(memberID);
			memberVO.setMemberPhoto(bt);
		}else {
			writeText(res, "");
		}

	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		MemberDAO_interface memberI = new MemberDAO();
		List<MemberVO> memberList = memberI.getAll();
		writeText(res, new Gson().toJson(memberList));
	}

	private void writeText(HttpServletResponse res, String outText) throws IOException {
		res.setContentType(CONTENT_TYPE);
		PrintWriter out = res.getWriter();
		out.print(outText);
		out.close();
		System.out.println("outText: " + outText);
	}
	
	

}
