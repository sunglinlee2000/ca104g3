package com.android.member.model;

import java.sql.*;
import java.util.*;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

public class MemberDAO implements MemberDAO_interface {

	private static DataSource ds = null;
	static {
		try {
			Context ctx = new InitialContext();
			ds = (DataSource) ctx.lookup("java:comp/env/jdbc/CA104G3DB");
		} catch (NamingException e) {
			e.printStackTrace();
		}
	}
//	**************************************SQL**************************************//	
	private static final String ADD_STMT = "INSERT INTO MEMBERLIST (memberID, memberName, memberNickName, memberEmail, thirdPartyID, MEMBERSTATUS) "
			+ "VALUES('M'||LPAD(TO_CHAR(mem_seq.NEXTVAL),6,0), ?, ?, ?, ?, 'MEMBER_NORMAL')";
	private static final String INSERT_STMT = "INSERT INTO memberlist (memberID,memberEmail,thirdPartyID,memberName,"
			+ "memberNickName,memberSex,memberBirthday,memberPhoto,memberCoin,"
			+ "memberPhone,memberBankAccount,memberAddress,memberJoinDate,"
			+ "memberStatus) VALUES ('M'||LPAD(TO_CHAR(mem_seq.NEXTVAL),6,0),?,?,\r\n"
			+ "?,?,?,?,?,?,?,?,?,(CURRENT_TIMESTAMP),?)";
	private static final String UPDATTE_STATUS = "UPDATE memberlist SET memberStatus=? Where memberID=?";
	private static final String CHECK_MEM = "SELECT memberID,thirdPartyID FROM memberlist WHERE thirdPartyID = ?";
	private static final String UPDATE_COIN = "UPDATE memberlist set memberCoin=? WHERE memberID = ? ";
	private static final String GET_ALL_STMT = "SELECT * FROM memberlist ORDER BY memberID";
	private static final String GET_ONE_STMT = "SELECT * FROM memberlist WHERE memberID = ?";
	private static final String UPDATE = "UPDATE memberlist set memberEmail=?, thirdPartyID=?, memberName=?,"
			+ "memberNickName=?, memberSex=?, memberBirthday=?, memberPhoto=?, memberCoin=?, "
			+ "memberPhone=?, memberBankAccount=?, memberAddress=?, memberJoinDate=?, memberStatus=? WHERE memberID=?";
	private static final String SIMPLE_UPDATE = "UPDATE MEMBERLIST SET MEMBEREMAIL = ?, MEMBERNAME = ?, MEMBERNICKNAME = ?, MEMBERSEX = ?, MEMBERPHONE = ?"
			+ ",MEMBERBANKACCOUNT = ?, MEMBERADDRESS = ? WHERE THIRDPARTYID = ?";
	private static final String GET_MEMBER_PHOTO = "SELECT MEMBERPHOTO FROM MEMBERLIST WHERE MEMBERID = ?";
	private static final String GET_MEMBERNICKNAME = "SELECT MEMBERNICKNAME FROM MEMBERLIST WHERE MEMBERID = ?";
	private static final String CHECK_MEMBER_EXIST = "SELECT THIRDPARTYID FROM MEMBERLIST WHERE THIRDPARTYID = ?";
	private static final String GET_ME_BY_THIRDPARTYID = "SELECT * FROM MEMBERLIST WHERE THIRDPARTYID = ?";
	private static final String GET_COIN = "SELECT memberID,memberCoin FROM memberlist WHERE memberID = ? ";

//	********************************************************************************//		
	@Override
	public void insert(MemberVO memberVO) {

		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(INSERT_STMT);

			pstmt.setString(1, memberVO.getMemberEmail());
			pstmt.setString(2, memberVO.getThirdPartyID());
			pstmt.setString(3, memberVO.getMemberName());
			pstmt.setString(4, memberVO.getMemberNickName());
			pstmt.setString(5, memberVO.getMemberSex());
			pstmt.setTimestamp(6, memberVO.getMemberBirthday());
			pstmt.setBytes(7, memberVO.getMemberPhoto());
			pstmt.setInt(8, memberVO.getMemberCoin());
			pstmt.setString(9, memberVO.getMemberPhone());
			pstmt.setString(10, memberVO.getMemberBankAccount());
			pstmt.setString(11, memberVO.getMemberAddress());
//				pstmt.setTimestamp(13, memberVO.getMemberJoinDate());
			pstmt.setString(12, memberVO.getMemberStatus());

			pstmt.executeUpdate();

		} catch (SQLException se) {
			throw new RuntimeException("A database error occured." + se.getMessage());

		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
		}
	}

	@Override
	public void update(MemberVO memberVO) {

		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(UPDATE);

			pstmt.setString(1, memberVO.getMemberEmail());
			pstmt.setString(2, memberVO.getThirdPartyID());
			pstmt.setString(3, memberVO.getMemberName());
			pstmt.setString(4, memberVO.getMemberNickName());
			pstmt.setString(5, memberVO.getMemberSex());
			pstmt.setTimestamp(6, memberVO.getMemberBirthday());
			pstmt.setBytes(7, memberVO.getMemberPhoto());
			pstmt.setInt(8, memberVO.getMemberCoin());
			pstmt.setString(9, memberVO.getMemberPhone());
			pstmt.setString(10, memberVO.getMemberBankAccount());
			pstmt.setString(11, memberVO.getMemberAddress());
			pstmt.setTimestamp(12, memberVO.getMemberJoinDate());
			pstmt.setString(13, memberVO.getMemberStatus());
			pstmt.setString(14, memberVO.getMemberID());

			pstmt.executeUpdate();

		} catch (SQLException se) {
			throw new RuntimeException("A database error occured." + se.getMessage());
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
	}

	@Override
	public MemberVO findByPrimaryKey(String memberID) {

		MemberVO memberVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ONE_STMT);

			pstmt.setString(1, memberID);

			rs = pstmt.executeQuery();

			while (rs.next()) {
				memberVO = new MemberVO();
				memberVO.setMemberID(rs.getString("memberID"));
				memberVO.setMemberEmail(rs.getString("memberEmail"));
				memberVO.setThirdPartyID(rs.getString("thirdPartyID"));
				memberVO.setMemberName(rs.getString("memberName"));
				memberVO.setMemberNickName(rs.getString("memberNickName"));
				memberVO.setMemberSex(rs.getString("memberSex"));
				memberVO.setMemberBirthday(rs.getTimestamp("memberBirthday"));
//					memberVO.setMemberPhoto(rs.getBytes("memberPhoto"));
				memberVO.setMemberCoin(rs.getInt("memberCoin"));
				memberVO.setMemberPhone(rs.getString("memberPhone"));
				memberVO.setMemberBankAccount(rs.getString("memberBankAccount"));
				memberVO.setMemberAddress(rs.getString("memberAddress"));
				memberVO.setMemberJoinDate(rs.getTimestamp("memberJoinDate"));
				memberVO.setMemberStatus(rs.getString("memberStatus"));

			}

		} catch (SQLException se) {
			throw new RuntimeException("A database error occured." + se.getMessage());
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
		}

		return memberVO;
	}

	@Override
	public List<MemberVO> getAll() {
		List<MemberVO> list = new ArrayList<MemberVO>();
		MemberVO memberVO = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ALL_STMT);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				memberVO = new MemberVO();
				memberVO.setMemberID(rs.getString("memberID"));
				memberVO.setMemberEmail(rs.getString("memberEmail"));
				memberVO.setThirdPartyID(rs.getString("thirdPartyID"));
				memberVO.setMemberName(rs.getString("memberName"));
				memberVO.setMemberNickName(rs.getString("memberNickName"));
				memberVO.setMemberSex(rs.getString("memberSex"));
				memberVO.setMemberBirthday(rs.getTimestamp("memberBirthday"));
//					memberVO.setMemberPhoto(rs.getBytes("memberPhoto"));
				memberVO.setMemberCoin(rs.getInt("memberCoin"));
				memberVO.setMemberPhone(rs.getString("memberPhone"));
				memberVO.setMemberBankAccount(rs.getString("memberBankAccount"));
				memberVO.setMemberAddress(rs.getString("memberAddress"));
				memberVO.setMemberJoinDate(rs.getTimestamp("memberJoinDate"));
				memberVO.setMemberStatus(rs.getString("memberStatus"));
				list.add(memberVO);
			}

		} catch (SQLException se) {
			throw new RuntimeException("A database error occured." + se.getMessage());
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
		}

		return list;
	}

	@Override
	public byte[] getMemberPhoto(String memberID) {
		byte[] memberPhoto = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_MEMBER_PHOTO);
			pstmt.setString(1, memberID);
			rs = pstmt.executeQuery();
			if (rs.next()) {

				memberPhoto = rs.getBytes(1);

			}

		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. " + se.getMessage());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

		return memberPhoto;
	}

	@Override
	public String getMemberNickName(String memberID) {
		String memberNickName = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_MEMBERNICKNAME);
			pstmt.setString(1, memberID);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				memberNickName = rs.getString(1);
			}

		} catch (SQLException se) {
			throw new RuntimeException("A database error occured." + se.getMessage());
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
		}

		return memberNickName;
	}

	@Override
	public boolean add(MemberVO memberVO) {
		Connection con = null;
		PreparedStatement pstmt = null;
		boolean isAdded = false;
		try {
			con = ds.getConnection();
			pstmt = con.prepareStatement(ADD_STMT);

			pstmt.setString(1, memberVO.getMemberName());
			pstmt.setString(2, memberVO.getMemberNickName());
			pstmt.setString(3, memberVO.getMemberEmail());
			pstmt.setString(4, memberVO.getThirdPartyID());
			int count = pstmt.executeUpdate();
			isAdded = count == 1;
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured." + se.getMessage());

		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
		}
		return isAdded;
	}

	@Override
	public boolean isMember(String thirdPartyID) {
		Connection con = null;
		PreparedStatement pstmt = null;
		boolean isMember = false;
		try {
			con = ds.getConnection();
			pstmt = con.prepareStatement(CHECK_MEMBER_EXIST);
			pstmt.setString(1, thirdPartyID);
			ResultSet rs = pstmt.executeQuery();
			isMember = rs.next();
			return isMember;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			try {
				if (pstmt != null) {
					pstmt.close();
				}
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return isMember;
	}

	@Override
	public MemberVO getMeByThirdPartyID(String thirdPartyID) {
		MemberVO memberVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ME_BY_THIRDPARTYID);
			pstmt.setString(1, thirdPartyID);

			rs = pstmt.executeQuery();

			while (rs.next()) {
				memberVO = new MemberVO();
				memberVO.setMemberID(rs.getString("memberID"));
				memberVO.setMemberEmail(rs.getString("memberEmail"));
				memberVO.setThirdPartyID(rs.getString("thirdPartyID"));
				memberVO.setMemberName(rs.getString("memberName"));
				memberVO.setMemberNickName(rs.getString("memberNickName"));
				memberVO.setMemberSex(rs.getString("memberSex"));
				memberVO.setMemberBirthday(rs.getTimestamp("memberBirthday"));
				memberVO.setMemberCoin(rs.getInt("memberCoin"));
				memberVO.setMemberPhone(rs.getString("memberPhone"));
				memberVO.setMemberBankAccount(rs.getString("memberBankAccount"));
				memberVO.setMemberAddress(rs.getString("memberAddress"));
				memberVO.setMemberJoinDate(rs.getTimestamp("memberJoinDate"));
				memberVO.setMemberStatus(rs.getString("memberStatus"));

			}

		} catch (SQLException se) {
			throw new RuntimeException("A database error occured." + se.getMessage());
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
		}

		return memberVO;
	}

	public boolean simpleUpdate(MemberVO memberVO) {

		Connection con = null;
		PreparedStatement pstmt = null;
		boolean isUpdated = false;
		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(SIMPLE_UPDATE);

			if (memberVO.getMemberSex().isEmpty()) {
				pstmt.setString(4, "N");
			} else {
				pstmt.setString(4, memberVO.getMemberSex());
			}
			if (memberVO.getMemberAddress().isEmpty()) {
				pstmt.setString(7, "N");
				System.out.println("SSSSSSSSSSSSSS");
			} else {
				pstmt.setString(7, memberVO.getMemberAddress());
			}
			if (memberVO.getMemberBankAccount().isEmpty()) {
				pstmt.setString(6, "N");
			} else {
				pstmt.setString(6, memberVO.getMemberBankAccount());
			}
			if (memberVO.getMemberPhone().isEmpty()) {
				pstmt.setString(5, "N");
			} else {
				pstmt.setString(5, memberVO.getMemberPhone());
			}

			pstmt.setString(1, memberVO.getMemberEmail());
			pstmt.setString(2, memberVO.getMemberName());
			pstmt.setString(3, memberVO.getMemberNickName());
			pstmt.setString(8, memberVO.getThirdPartyID());
			int count = pstmt.executeUpdate();
			isUpdated = count > 0 ? true : false;

			System.out.println("isUpdated = " + isUpdated);
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured." + se.getMessage());
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return isUpdated;
	}

	@Override
	public void updateCoin(MemberVO memberVO) {
		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(UPDATE_COIN);

			pstmt.setInt(1, memberVO.getMemberCoin());
			pstmt.setString(2, memberVO.getMemberID());

			pstmt.executeUpdate();

		} catch (SQLException se) {
			throw new RuntimeException("A database error occured." + se.getMessage());
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
	}

	@Override
	public void updateStatus(MemberVO memberVO) {
		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(UPDATTE_STATUS);

			pstmt.setString(1, memberVO.getMemberStatus());
			pstmt.setString(2, memberVO.getMemberID());

			pstmt.executeUpdate();

		} catch (SQLException se) {
			throw new RuntimeException("A database error occured." + se.getMessage());
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

	}

	@Override
	public MemberVO getCoin(String memberID) {
		MemberVO memberVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_COIN);

			pstmt.setString(1, memberID);

			rs = pstmt.executeQuery();

			while (rs.next()) {
				memberVO = new MemberVO();
				memberVO.setMemberID(rs.getString("memberID"));
				memberVO.setMemberCoin(rs.getInt("memberCoin"));

			}

		} catch (SQLException se) {
			throw new RuntimeException("A database error occured." + se.getMessage());
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
		}

		return memberVO;
	}

	@Override
	public MemberVO findByTPID(String thirdPartyID) {
		MemberVO memberVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(CHECK_MEM);

			pstmt.setString(1, thirdPartyID);

			rs = pstmt.executeQuery();

			while (rs.next()) {
				memberVO = new MemberVO();
				memberVO.setMemberID(rs.getString("memberID"));
				memberVO.setThirdPartyID(rs.getString("thirdPartyID"));

			}

		} catch (SQLException se) {
			throw new RuntimeException("A database error occured." + se.getMessage());
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
		}

		return memberVO;
	}

}
