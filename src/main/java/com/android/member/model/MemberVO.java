package com.android.member.model;
import java.sql.Timestamp;

public class MemberVO implements java.io.Serializable{
	private String memberID;
	private String memberEmail;
	private String thirdPartyID;
	private String memberName;
	private String memberNickName;
	private String memberSex;
	private Timestamp memberBirthday;
	private byte[] memberPhoto;
	private Integer memberCoin;
	private String memberPhone;
	private String memberBankAccount;
	private String memberAddress;
	private Timestamp memberJoinDate;
	private String memberStatus;
	private String memberPhotoURL;
	
	public String getMemberID() {
		return memberID;
	}
	public void setMemberID(String memberID) {
		this.memberID = memberID;
	}
	public String getMemberEmail() {
		return memberEmail;
	}
	public void setMemberEmail(String memberEmail) {
		this.memberEmail = memberEmail;
	}
	public String getThirdPartyID() {
		return thirdPartyID;
	}
	public void setThirdPartyID(String thirdPartyID) {
		this.thirdPartyID = thirdPartyID;
	}
	public String getMemberName() {
		return memberName;
	}
	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}
	public String getMemberNickName() {
		return memberNickName;
	}
	public void setMemberNickName(String memberNickName) {
		this.memberNickName = memberNickName;
	}
	public String getMemberSex() {
		return memberSex;
	}
	public void setMemberSex(String memberSex) {
		this.memberSex = memberSex;
	}
	public Timestamp getMemberBirthday() {
		return memberBirthday;
	}
	public void setMemberBirthday(Timestamp memberBirthday) {
		this.memberBirthday = memberBirthday;
	}
	public byte[] getMemberPhoto() {
		return memberPhoto;
	}
	public void setMemberPhoto(byte[] memberPhoto) {
		this.memberPhoto = memberPhoto;
	}
	public Integer getMemberCoin() {
		return memberCoin;
	}
	public void setMemberCoin(Integer memberCoin) {
		this.memberCoin = memberCoin;
	}
	public String getMemberPhone() {
		return memberPhone;
	}
	public void setMemberPhone(String memberPhone) {
		this.memberPhone = memberPhone;
	}
	public String getMemberBankAccount() {
		return memberBankAccount;
	}
	public void setMemberBankAccount(String memberBankAccount) {
		this.memberBankAccount = memberBankAccount;
	}
	public String getMemberAddress() {
		return memberAddress;
	}
	public void setMemberAddress(String memberAddress) {
		this.memberAddress = memberAddress;
	}
	public Timestamp getMemberJoinDate() {
		return memberJoinDate;
	}
	public void setMemberJoinDate(Timestamp memberJoinDate) {
		this.memberJoinDate = memberJoinDate;
	}
	public String getMemberStatus() {
		return memberStatus;
	}
	public void setMemberStatus(String memberStatus) {
		this.memberStatus = memberStatus;
	}
	public String getMemberPhotoURL() {
		return memberPhotoURL;
	}
	public void setMemberPhotoURL(String memberPhotoURL) {
		this.memberPhotoURL = memberPhotoURL;
	}
	
	
}
