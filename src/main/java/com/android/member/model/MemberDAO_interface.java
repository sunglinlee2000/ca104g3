package com.android.member.model;
import java.util.List;

public interface MemberDAO_interface {
		public void insert(MemberVO memberVO);
		public boolean add(MemberVO memberVO);
		public void update(MemberVO memberVO);
		public MemberVO findByPrimaryKey(String memberID);
		public List<MemberVO> getAll();
		public byte[] getMemberPhoto(String memberID);
		public String getMemberNickName(String memberID);
		public boolean isMember(String thirdPartyID);
		public MemberVO getMeByThirdPartyID(String thirdPartyID);
		public boolean simpleUpdate(MemberVO memberVO);
		
		public void updateCoin(MemberVO memberVO);
		public void updateStatus(MemberVO memberVO);
		public MemberVO getCoin(String memberID);		
		public MemberVO findByTPID(String thirdPartyID);
		
		
}
