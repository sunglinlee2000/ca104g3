package com.android.member.model;

import java.sql.Timestamp;
import java.util.List;

public class MemberService {
	
	private MemberDAO_interface dao;
	
	public MemberService() {
		dao = new MemberDAO();
	}
	
	public MemberVO addMember(String memberEmail,String thirdPartyID,
			String memberName,String memberNickName,String memberSex,Timestamp memberBirthday, String memberPhotoURL,
			String memberPhone,String memberBankAccount,
			String memberAddress) {
		MemberVO memberVO = new MemberVO();
		
		memberVO.setMemberEmail(memberEmail);
		memberVO.setThirdPartyID(thirdPartyID);
		memberVO.setMemberName(memberName);
		memberVO.setMemberNickName(memberNickName);
		memberVO.setMemberSex(memberSex);
		memberVO.setMemberBirthday(memberBirthday);
		memberVO.setMemberPhone(memberPhone);
		memberVO.setMemberBankAccount(memberBankAccount);
		memberVO.setMemberAddress(memberAddress);
		memberVO.setMemberPhotoURL(memberPhotoURL);
		dao.insert(memberVO);
				
		return memberVO;
	}
	
	public MemberVO updateMember(String memberID,String memberEmail,
			String memberName,String memberNickName,String memberSex,Timestamp memberBirthday,
			byte[] memberPhoto,String memberPhone,String memberBankAccount,
			String memberAddress) {
		MemberVO memberVO = new MemberVO();
		
		memberVO.setMemberID(memberID);
		memberVO.setMemberEmail(memberEmail);
		memberVO.setMemberName(memberName);
		memberVO.setMemberNickName(memberNickName);
		memberVO.setMemberSex(memberSex);
		memberVO.setMemberBirthday(memberBirthday);
		memberVO.setMemberPhoto(memberPhoto);
		memberVO.setMemberPhone(memberPhone);
		memberVO.setMemberBankAccount(memberBankAccount);
		memberVO.setMemberAddress(memberAddress);

		dao.update(memberVO);
		
		return memberVO;
	}
	
	
	public MemberVO updateMemberCoin(Integer memberCoin,String memberID) {
		MemberVO memberVO = new MemberVO();
		
		memberVO.setMemberID(memberID);
		memberVO.setMemberCoin(memberCoin);

		dao.updateCoin(memberVO);		
		return memberVO;
	}
	
	public MemberVO updateMemberStatus(String memberID,String memberStatus) {
		MemberVO memberVO = new MemberVO();
		System.out.println(memberStatus);
		memberVO.setMemberID(memberID);
		memberVO.setMemberStatus(memberStatus);

		dao.updateStatus(memberVO);		
		return memberVO;
	}
	
	public MemberVO getOneMember(String memberID) {
		return dao.findByPrimaryKey(memberID);
	}
	
	public MemberVO getCoin(String memberID) {
		return dao.getCoin(memberID);
	}
	
	public MemberVO findMem(String thirdPartyID) {
		return dao.findByTPID(thirdPartyID);
	}
	
	public List<MemberVO> getAll() {
		return dao.getAll();
	}
}
