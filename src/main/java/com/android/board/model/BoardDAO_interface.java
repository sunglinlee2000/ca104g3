package com.android.board.model;
import java.util.List;

public interface BoardDAO_interface {
	public void insert(BoardVO boardVO);
	public void update(BoardVO boardVO);
	public void delete(String boardID);
	public BoardVO findByPrimaryKey(String boardID);
	public List<BoardVO> getAll();
	public byte[] getImage(String boardID);
	public List<BoardVO> getBoardList();
}
