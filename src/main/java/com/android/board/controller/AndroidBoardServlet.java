package com.android.board.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.android.board.model.BoardDAO;
import com.android.board.model.BoardDAO_interface;
import com.android.board.model.BoardVO;
import com.android.util.ImageUtil;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

public class AndroidBoardServlet extends HttpServlet{
	private final static String CONTENT_TYPE = "text/html; charset=UTF-8";

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		ServletContext context = getServletContext();
		BoardDAO_interface daoI = new BoardDAO();
		Gson gson = new Gson();
		BufferedReader br = req.getReader();
		StringBuilder jsonIn = new StringBuilder();
		
		String line = null;
		while((line = br.readLine())!=null) {
			jsonIn.append(line);
		}
		
		System.out.println("input: " + jsonIn);
		JsonObject jsonObject = gson.fromJson(jsonIn.toString(), JsonObject.class);
		String action = jsonObject.get("action").getAsString();
		
		if("getBoardList".equals(action)) {
			List<BoardVO> boardList = daoI.getBoardList();
			writeText(res, gson.toJson(boardList));
		} else if ("getImage".equals(action)) {
			OutputStream os = res.getOutputStream();
			String boardID = jsonObject.get("boardID").getAsString();
			int imageSize = jsonObject.get("imageSize").getAsInt();
			byte[] image = daoI.getImage(boardID);
			if(image != null) {
				image = ImageUtil.shrink(image, imageSize);
				res.setContentType("image/jpeg");
				res.setContentLength(image.length);
			}
			os.write(image);
		}else {
			writeText(res, "");
		}
		
		
		
		
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		// TODO Auto-generated method stub
		BoardDAO_interface board_i = new BoardDAO();
		List<BoardVO> boardList = board_i.getAll();
		
		writeText(res, new Gson().toJson(boardList));
	}
	
	private void writeText(HttpServletResponse res, String outText) throws IOException{
		res.setContentType(CONTENT_TYPE);
		PrintWriter out = res.getWriter();
		out.print(outText);
		out.close();
		System.out.println("outText: " + outText);
	}
	
	
	
	
	
}
