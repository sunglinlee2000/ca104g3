package com.android.adfund.model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.*;

public class ADFundDAO implements ADFundDAO_interface{

//	連線設定
	private static DataSource ds = null;
	static {
		try {
			Context ctx = new InitialContext();
			ds = (DataSource) ctx.lookup("java:comp/env/jdbc/CA104G3DB");
		} catch (NamingException e) {
			e.printStackTrace();
		}
	}
	
//	********************************SQl SETTING**********************************  //
	private static final String GET_ALL_STMT = "SELECT * FROM ADFUND";
	private static final String GET_ADFUNDPASS_LIST = "SELECT ADFUNDID FROM ADFUND WHERE ADFUNDSTATUS = 'AD_PASSED'";
	private static final String INSERT_STMT = "INSERT INTO ADFUND VALUES "
			+ "('AD'||(LPAD(TO_CHAR(ADFUND_SEQ.NEXTVAL),6,'0')),?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	private static final String GET_ONE_STMT = "SELECT * FROM ADFUND WHERE ADFUNDID = ? ";
	private static final String UPDATE_BY_MEM = "UPDATE ADFUND SET "
			+ "ADFUNDTITLE=?, ADFUNDTEXT=?, ADFUNDHOMEPHOTO=?, ADFUNDBOARDPHOTO=?, ADFUNDCOINTARGET=? WHERE ADFUNDID = ?";
	private static final String UPDATE_BY_AD = "UPDATE ADFUND SET "
			+ "ADFUNDSTATUS =? WHERE ADFUNDID = ?";
	private static final String UPDATE_COINNOW = "UPDATE ADFUND SET "
			+ "ADFUNDCOINNOW =? WHERE ADFUNDID = ?";
	private static final String DELETE = "DELETE FROM ADFUND WHERE ADFUNDID = ?";
	private static final String FIND_ADFUNDHOMEPHOTO_BY_ADFUNDID = "SELECT ADFUNDHOMEPHOTO FROM ADFUND WHERE ADFUNDID = ?";
//	*******************************************************************************	//
	@Override
	public void insert(ADFundVO adfundVO) {

		Connection con = null;
		PreparedStatement pstmt = null;

		try {
		
			con = ds.getConnection();
			pstmt = con.prepareStatement(INSERT_STMT);

			pstmt.setString(1, adfundVO.getMemberID());
			pstmt.setString(2, adfundVO.getArtistID());
			pstmt.setString(3, adfundVO.getAdministratorID());
			pstmt.setTimestamp(4, adfundVO.getAdfundStartDate());
			pstmt.setTimestamp(5, adfundVO.getAdfundEndDate());
			pstmt.setTimestamp(6, adfundVO.getAdfundDisplayDate());
			pstmt.setString(7, adfundVO.getAdfundTitle());
			pstmt.setString(8, adfundVO.getAdfundType());
			pstmt.setString(9, adfundVO.getAdfundText());
			pstmt.setBytes(10, adfundVO.getAdfundHomePhoto());
			pstmt.setBytes(11, adfundVO.getAdfundBoardPhoto());
			pstmt.setInt(12, adfundVO.getAdfundCoinTarget());
			pstmt.setInt(13, adfundVO.getAdfundCoinNow());
			pstmt.setTimestamp(14, adfundVO.getAdfundSubmitDate());
			pstmt.setString(15, adfundVO.getAdfundStatus());

			pstmt.executeUpdate();

		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. " + se.getMessage());

		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {

				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}

		}
	}

	@Override
	public void updateByMem(ADFundVO adfundVO) {

		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(UPDATE_BY_MEM);

			pstmt.setString(1, adfundVO.getAdfundTitle());
			pstmt.setString(2, adfundVO.getAdfundText());
			pstmt.setBytes(3, adfundVO.getAdfundHomePhoto());
			pstmt.setBytes(4, adfundVO.getAdfundBoardPhoto());
			pstmt.setInt(5, adfundVO.getAdfundCoinTarget());
			pstmt.setString(6, adfundVO.getAdfundID());

			pstmt.executeUpdate();
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. " + se.getMessage());
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
					e.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace(System.err);
				}

			}
		}

	}
	
	
	@Override
	public void updateByAd(ADFundVO adfundVO) {
		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(UPDATE_BY_AD);

			pstmt.setString(1, adfundVO.getAdfundStatus());
			pstmt.setString(2, adfundVO.getAdfundID());


			pstmt.executeUpdate();
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. " + se.getMessage());
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
					e.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace(System.err);
				}
			}
		}		
	}
	

	@Override
	public ADFundVO findByPrimaryKey(String adfundid) {

		ADFundVO adfVO = new ADFundVO();
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ONE_STMT);

			pstmt.setString(1, adfundid);

			rs = pstmt.executeQuery();
			
			while (rs.next()) {
				adfVO = new ADFundVO();
				adfVO.setAdfundID(rs.getString("adfundid"));
				adfVO.setMemberID(rs.getString("memberid"));
				adfVO.setArtistID(rs.getString("artistid"));
				adfVO.setAdministratorID(rs.getString("administratorid"));
				adfVO.setAdfundStartDate(rs.getTimestamp("adfundstartdate"));
				adfVO.setAdfundEndDate(rs.getTimestamp("adfundenddate"));
				adfVO.setAdfundDisplayDate(rs.getTimestamp("adfunddisplaydate"));
				adfVO.setAdfundTitle(rs.getString("adfundtitle"));
				adfVO.setAdfundType(rs.getString("adfundtype"));
				adfVO.setAdfundText(rs.getString("adfundtext"));
				adfVO.setAdfundHomePhoto(rs.getBytes("adfundhomephoto"));
				adfVO.setAdfundBoardPhoto(rs.getBytes("adfundboardphoto"));
				adfVO.setAdfundCoinTarget(rs.getInt("adfundcointarget"));
				adfVO.setAdfundCoinNow(rs.getInt("adfundcoinnow"));
				adfVO.setAdfundSubmitDate(rs.getTimestamp("adfundsubmitdate"));
				adfVO.setAdfundStatus(rs.getString("adfundstatus"));
			}
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. " + se.getMessage());

		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return adfVO;
	}

	@Override
	public List<ADFundVO> getAll() {
		List<ADFundVO> adlist = new ArrayList<ADFundVO>();
		ADFundVO adfVO = null;
		
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs =null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ALL_STMT);
			rs = pstmt.executeQuery();
			
			while (rs.next()) {
				adfVO = new ADFundVO();
				adfVO.setAdfundID(rs.getString("adfundid"));
//				adfVO.setMemberID(rs.getString("memberid"));
//				adfVO.setArtistID(rs.getString("artistid"));
//				adfVO.setAdministratorID(rs.getString("administratorid"));
//				adfVO.setAdfundStartDate(rs.getTimestamp("adfundstartdate"));
//				adfVO.setAdfundEndDate(rs.getTimestamp("adfundenddate"));
//				adfVO.setAdfundDisplayDate(rs.getTimestamp("adfunddisplaydate"));
//				adfVO.setAdfundTitle(rs.getString("adfundtitle"));
//				adfVO.setAdfundType(rs.getString("adfundtype"));
//				adfVO.setAdfundText(rs.getString("adfundtext"));
//				adfVO.setAdfundHomePhoto(rs.getBytes("adfundhomephoto"));
//				adfVO.setAdfundBoardPhoto(rs.getBytes("adfundboardphoto"));
//				adfVO.setAdfundCoinTarget(rs.getInt("adfundcointarget"));
//				adfVO.setAdfundCoinNow(rs.getInt("adfundcoinnow"));
//				adfVO.setAdfundSubmitDate(rs.getTimestamp("adfundsubmitdate"));
//				adfVO.setAdfundStatus(rs.getString("adfundstatus"));
				adlist.add(adfVO);
				
			}
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. " + se.getMessage());
			
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

		return adlist;
	}
	
	

	@Override
	public void delete(String adfundID) {
		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(DELETE);

			pstmt.setString(1, adfundID);

			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		
	}

	@Override
	public void updateCoinNow(ADFundVO adfundVO) {
		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(UPDATE_COINNOW);

			pstmt.setInt(1, adfundVO.getAdfundCoinNow());
			pstmt.setString(2, adfundVO.getAdfundID());


			pstmt.executeUpdate();
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. " + se.getMessage());
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
					e.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace(System.err);
				}
			}
		}		
		
	}

	@Override
	public byte[] getImage(String adfundID) {
		byte[] adfundhomephoto = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		try {
			con = ds.getConnection();
			pstmt = con.prepareStatement(FIND_ADFUNDHOMEPHOTO_BY_ADFUNDID);
			pstmt.setString(1, adfundID);
			rs = pstmt.executeQuery();
			System.out.println("AdfundHomePhoto GET!");
			if(rs.next()) {
				adfundhomephoto = rs.getBytes(1);
			}
			
		}catch(SQLException se) {
			throw new RuntimeException("A database error occured. " + se.getMessage());
		}
		
		return adfundhomephoto;
	}

	@Override
	public List<ADFundVO> getADFundList() {
		List<ADFundVO> adFundList = new ArrayList<ADFundVO>();
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs =null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ADFUNDPASS_LIST);
			rs = pstmt.executeQuery();
			
			while (rs.next()) {
				ADFundVO adfundVO = new ADFundVO();
				adfundVO.setAdfundID(rs.getString("adfundid"));
				adFundList.add(adfundVO);
			}
			
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. " + se.getMessage());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

		return adFundList;
	}
	
	
}
