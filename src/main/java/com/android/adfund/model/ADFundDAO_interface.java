package com.android.adfund.model;

import java.util.*;

public interface ADFundDAO_interface {
	public void insert(ADFundVO adfundVO);
	public void updateByMem(ADFundVO adfundVO);
	public void updateByAd(ADFundVO adfundVO);
	public void updateCoinNow(ADFundVO adfundVO);
	public void delete(String adfundID);
	public ADFundVO  findByPrimaryKey(String adfundID);
	public List<ADFundVO> getAll();
	public List<ADFundVO> getADFundList();
	public byte[] getImage(String adfundID);
	

}
