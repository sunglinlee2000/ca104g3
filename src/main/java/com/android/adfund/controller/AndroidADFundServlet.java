package com.android.adfund.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.android.adfund.model.ADFundDAO;
import com.android.adfund.model.ADFundDAO_interface;
import com.android.adfund.model.ADFundVO;
import com.android.util.ImageUtil;
import com.google.gson.Gson;
import com.google.gson.JsonObject;


public class AndroidADFundServlet extends HttpServlet {
	private final static String CONTENT_TYPE = "text/html; charset=UTF-8";
	private static final long serialVersionUID = 1L;
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		
		ServletContext context = getServletContext();
		ADFundDAO_interface adfundI = new ADFundDAO();
		Gson gson = new Gson();
		BufferedReader br = req.getReader();
		StringBuilder jsonIn = new StringBuilder();
		
		String line = null;
		while((line = br.readLine()) != null) {
			jsonIn.append(line);
		}
		
		System.out.println("input: " + jsonIn);
		JsonObject jsonObject = gson.fromJson(jsonIn.toString(),JsonObject.class);
		String action = jsonObject.get("action").getAsString();
		
		if("getADFundList".equals(action)) {
			List<ADFundVO> adfundList = adfundI.getADFundList();
			writeText(res, gson.toJson(adfundList));
		}else if("getImage".equals(action)) {
			OutputStream os =res.getOutputStream();
			String adfundID = jsonObject.get("adfundID").getAsString();
			int imageSize = jsonObject.get("imageSize").getAsInt();
			byte[] image = adfundI.getImage(adfundID);
			if(image != null) {
				image = ImageUtil.shrink(image, imageSize);
				res.setContentType("image/jpeg");
				res.setContentLength(image.length);
			}
			os.write(image);
		}
	}
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		ADFundDAO_interface adfundI = new ADFundDAO();
		List<ADFundVO> adfundList = adfundI.getAll();
		writeText(res, new Gson().toJson(adfundList));
	}
	
	private void writeText(HttpServletResponse res, String outText) throws IOException{
		res.setContentType(CONTENT_TYPE);
		PrintWriter out = res.getWriter();
		out.print(outText);
		out.close();
		System.out.println("outText: " + outText);
	}

}
