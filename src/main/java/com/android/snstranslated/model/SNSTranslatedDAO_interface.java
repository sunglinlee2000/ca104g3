package com.android.snstranslated.model;

import java.util.List;


public interface SNSTranslatedDAO_interface {
	public void insert(SNSTranslatedVO snsTranslatedVO);
	public void update(SNSTranslatedVO snsTranslatedVO);
	public void delete(String snsTranslatedID);
	public SNSTranslatedVO findByPrimaryKey(String snsTranslatedID);
	public SNSTranslatedVO getTrans(String snsID,String memberID,String snsTranslatedContent);
	public List<SNSTranslatedVO> getAll(String snsID);
}
