package com.android.snstranslated.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;


public class SNSTranslatedDAO implements SNSTranslatedDAO_interface{
	private static DataSource ds = null;
	static {
		try {
			Context ctx = new InitialContext();
			ds = (DataSource) ctx.lookup("java:comp/env/jdbc/CA104G3DB");
		} catch (NamingException e) {
			e.printStackTrace();
		}
	}
	private static final String INSERT_STMT = "INSERT INTO SNSTranslated VALUES('SNST'||LPAD(TO_CHAR(SNS_SEQ.NEXTVAL),6,0),?,?,(CURRENT_TIMESTAMP),0,?)";
	private static final String UPDATE = "UPDATE SNSTranslated SET snsTranslatedContent=? WHERE snsTranslatedID=?";
	private static final String DELETE = "DELETE FROM SNSTranslated WHERE snsTranslatedID=?";
	private static final String GET_ONE_STMT = "SELECT * FROM SNSTranslated WHERE snsTranslatedID=?";
	private static final String GET_ALL = "SELECT * FROM SNSTranslated WHERE snsID=? Order by snsTranslatedDate";
	private static final String GET_TRANS = "SELECT * FROM SNSTranslated WHERE snsID=? AND memberID=? AND snsTranslatedContent Like ?";
	
	/***************************************************/
//	private static final String GET_ONE = "SELECT * FROM SNSTRANSLATED WHERE SNS"
	/***************************************************/
	
	@Override
	public void insert(SNSTranslatedVO snsTranslatedVO) {
		Connection con = null; 
		PreparedStatement pstmt = null;
		
		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(INSERT_STMT);

			pstmt.setString(1, snsTranslatedVO.getSnsID());
			pstmt.setString(2, snsTranslatedVO.getMemberID());
			pstmt.setString(3, snsTranslatedVO.getSnsTranslatedContent());

			pstmt.executeUpdate();

		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}		
	}

	@Override
	public void update(SNSTranslatedVO snsTranslatedVO) {
		Connection con = null;
		PreparedStatement pstmt = null;
		System.out.println("進入修改翻譯");
		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(UPDATE);
			System.out.println("content"+snsTranslatedVO.getSnsTranslatedContent());
			System.out.println("id"+snsTranslatedVO.getSnsTranslatedID());
			pstmt.setString(1, snsTranslatedVO.getSnsTranslatedContent());
			pstmt.setString(2, snsTranslatedVO.getSnsTranslatedID());

			pstmt.executeUpdate();

		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}		
	}

	@Override
	public void delete(String snsTranslatedID) {
		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(DELETE);
			
			pstmt.setString(1, snsTranslatedID);

			pstmt.executeUpdate();

			
		} catch (SQLException se) {
			if (con != null) {
				try {
					con.rollback();
				} catch (SQLException excep) {
					throw new RuntimeException("rollback error occured. "
							+ excep.getMessage());
				}
			}
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}		
	}

	@Override
	public SNSTranslatedVO findByPrimaryKey(String snsTranslatedID) {
		SNSTranslatedVO snsTranslatedVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ONE_STMT);

			pstmt.setString(1, snsTranslatedID);

			rs = pstmt.executeQuery();

			while (rs.next()) {
				snsTranslatedVO = new SNSTranslatedVO();
				snsTranslatedVO.setSnsTranslatedID(rs.getString("snsTranslatedID"));
				snsTranslatedVO.setSnsID(rs.getString("snsID"));
				snsTranslatedVO.setMemberID(rs.getString("memberID"));
				snsTranslatedVO.setSnsTranslatedContent(rs.getString("snsTranslatedContent"));

			}

		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return snsTranslatedVO;
	}

	@Override
	public List<SNSTranslatedVO> getAll(String snsID) {
		List<SNSTranslatedVO> list = new ArrayList<SNSTranslatedVO>();
		SNSTranslatedVO snsTranslatedVO = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ALL);
			pstmt.setString(1, snsID);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				snsTranslatedVO = new SNSTranslatedVO();
				snsTranslatedVO.setSnsTranslatedID(rs.getString("snsTranslatedID"));
				snsTranslatedVO.setSnsID(rs.getString("snsID"));
				snsTranslatedVO.setMemberID(rs.getString("memberID"));
				snsTranslatedVO.setSnsTranslatedContent(rs.getString("snsTranslatedContent"));
				snsTranslatedVO.setSnsTranslatedDate(rs.getTimestamp("snsTranslatedDate"));

				list.add(snsTranslatedVO); // Store the row in the list
			}

		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return list;
	}

	@Override
	public SNSTranslatedVO getTrans(String snsID, String memberID, String snsTranslatedContent) {
		SNSTranslatedVO snsTranslatedVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_TRANS);

			pstmt.setString(1, snsID);
			pstmt.setString(2, memberID);
			pstmt.setString(3, snsTranslatedContent);

			rs = pstmt.executeQuery();

			while (rs.next()) {
				snsTranslatedVO = new SNSTranslatedVO();
				snsTranslatedVO.setSnsTranslatedID(rs.getString("snsTranslatedID"));
				snsTranslatedVO.setSnsID(rs.getString("snsID"));
				snsTranslatedVO.setMemberID(rs.getString("memberID"));
				snsTranslatedVO.setSnsTranslatedContent(rs.getString("snsTranslatedContent"));
				snsTranslatedVO.setSnsTranslatedDate(rs.getTimestamp("snsTranslatedDate"));

			}

		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return snsTranslatedVO;	
	}
}
