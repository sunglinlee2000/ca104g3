package com.android.snstranslated.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.android.snstranslated.model.SNSTranslatedVO;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.android.snstranslated.model.SNSTranslatedDAO;
import com.android.snstranslated.model.SNSTranslatedDAO_interface;

public class AndroidSNSTranslatedServlet extends HttpServlet{
	private final static String CONTENT_TYPE = "text/html; charset=UTF-8";
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		ServletContext context = getServletContext();
		req.setCharacterEncoding("UTF-8");
		SNSTranslatedDAO_interface dao = new SNSTranslatedDAO();
		BufferedReader br = req.getReader();
		StringBuilder sb = new StringBuilder();
		String line ;
		while((line=br.readLine())!=null) {
			sb.append(line);
		}
		System.out.println("input: " + sb);
		Gson gson = new Gson();
		JsonObject jsonObject = new Gson().fromJson(sb.toString(), JsonObject.class);
		String action = jsonObject.get("action").getAsString();
		
		if("getOneSNSTranslated".equals(action)) {
			String snsID = jsonObject.get("snsID").getAsString();
			List<SNSTranslatedVO> list = dao.getAll(snsID);
			writeText(res, new Gson().toJson(list));
		}
		
		
	}

	
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		SNSTranslatedDAO_interface dao = new SNSTranslatedDAO();
		SNSTranslatedVO snsTranslated = dao.findByPrimaryKey("ST000001");
		writeText(res, new Gson().toJson(snsTranslated));
	}
	
	public void writeText(HttpServletResponse res, String outText) throws IOException {
		res.setContentType(CONTENT_TYPE);
		PrintWriter pw = res.getWriter();
		pw.print(outText);
		pw.close();
		System.out.println("output : " + outText);
	}

}
