package com.android.product.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.android.product.model.ProductDAO;
import com.android.product.model.ProductDAO_interface;
import com.android.product.model.ProductVO;
import com.android.util.ImageUtil;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

public class AndroidProductServlet extends HttpServlet {
	private final static String CONTENT_TYPE = "text/html; charset=UTF-8";

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		req.setCharacterEncoding("UTF-8");
		ServletContext context = getServletContext();
		ProductDAO_interface dao = new ProductDAO();
		Gson gson = new Gson();
		BufferedReader br = req.getReader();
		StringBuilder jsonIn = new StringBuilder();
		String line = null;
		while((line = br.readLine())!=null) {
			jsonIn.append(line);
		}
		
		System.out.println("input : " + jsonIn);
		JsonObject jsonObject = gson.fromJson(jsonIn.toString(), JsonObject.class);
		String action = jsonObject.get("action").getAsString();
		
		if("getProductList".equals(action)) {
			List<ProductVO> productList = dao.getAll();
			writeText(res, gson.toJson(productList));
		}else if("getSearchList".equals(action)) {
			String word = jsonObject.get("word").getAsString();
			List<ProductVO> productList = dao.getSearchList(word);
			writeText(res, gson.toJson(productList));
		}else if("getRelatedList".equals(action)) {
			String artistID = jsonObject.get("artistID").getAsString();
			List<ProductVO> productList = dao.getRelatedList(artistID);
			writeText(res, gson.toJson(productList));
		}else if("getImage".equals(action)) {
			OutputStream os = res.getOutputStream();
			int imageSize = jsonObject.get("imageSize").getAsInt();
			String productID = jsonObject.get("productID").getAsString();
			byte[] image = dao.getProductImage(productID);
			if(image != null) {
				image = ImageUtil.shrink(image, imageSize);
				res.setContentType("image/jpeg");
				res.setContentLength(image.length);
			}
			os.write(image);
		}else if("getProductName".equals(action)) {
			String productID = jsonObject.get("productID").getAsString();
			ProductVO product = dao.findByPrimaryKey(productID);
			writeText(res, product.getProductName());
		}
		
	
	
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		ProductDAO_interface productI = new ProductDAO();
		List<ProductVO> productList = productI.getAll();
		writeText(res, new Gson().toJson(productList));
	}

	public void writeText(HttpServletResponse res, String outText) throws IOException {
		res.setContentType(CONTENT_TYPE);
		PrintWriter out = res.getWriter();
		out.print(outText);
		out.close();
		System.out.println("outText: " + outText);
	}

}
