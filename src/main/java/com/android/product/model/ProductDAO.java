package com.android.product.model;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.*;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

public class ProductDAO implements ProductDAO_interface{
	
	private static DataSource ds = null;
	static {
		try {
			Context ctx = new InitialContext();
			ds = (DataSource) ctx.lookup("java:comp/env/jdbc/CA104G3DB");
		} catch (NamingException e) {
			e.printStackTrace();
		}
	}
	
	private static final String INSERT_STMT = 
			"INSERT INTO product(productid,memberid,artistid,productphoto,productamount,productprice,productinfo,productname,productstatus) "
			+ "VALUES ('P'||LPAD(to_char(PRODUCT_SEQ.NEXTVAL), 6, '0'),?,?,?,?,?,?,?,?)";
	private static final String UPDATE = 
			"UPDATE product SET memberid=?, artistid=?, productphoto=?, productamount=?, productprice=?, productinfo=?,productname=?,productstatus=? where productid = ?";
	private static final String GET_ONE_STMT = 
			"SELECT * FROM product where productid = ?";
	private static final String GET_ALL_STMT = 
			"SELECT * FROM product order by productid desc";
	private static final String GET_ALL_FOR_MEMBERID=
			"SELECT * FROM product  where memberID=? order by productid desc";
	private static final String GET_PRODUCT_IMAGE =
			"SELECT PRODUCTPHOTO FROM PRODUCT WHERE PRODUCTID = ?";
	private static final String GET_SEARCHLIST = 
			"SELECT * FROM PRODUCT WHERE LOWER(PRODUCTNAME) LIKE LOWER(?)";
	private static final String GET_RELATEDLIST = 
			"SELECT * FROM PRODUCT WHERE ARTISTID = ?";
		
	@Override
	public void insert(ProductVO productVO) {
		Connection con = null;
		PreparedStatement pstmt = null;
		try {
			
			con = ds.getConnection();
			pstmt = con.prepareStatement(INSERT_STMT);
			
			pstmt.setString(1, productVO.getMemberID());
			pstmt.setString(2,productVO.getArtistID());
			pstmt.setBytes(3,productVO.getProductPhoto());
			pstmt.setInt(4, productVO.getProductAmount());
			pstmt.setInt(5, productVO.getProductPrice());
			pstmt.setString(6,productVO.getProductInfo());
			pstmt.setString(7, productVO.getProductName());
			pstmt.setString(8,productVO.getProductStatus());
			pstmt.executeUpdate();
			
		
		}catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
	}

	@Override
	public void update(ProductVO productVO) {
		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(UPDATE);

			pstmt.setString(1, productVO.getMemberID());
			pstmt.setString(2,productVO.getArtistID());
			pstmt.setBytes(3,productVO.getProductPhoto());
			pstmt.setInt(4, productVO.getProductAmount());
			pstmt.setInt(5, productVO.getProductPrice());
			pstmt.setString(6,productVO.getProductInfo());
			pstmt.setString(7, productVO.getProductName());
			pstmt.setString(8,productVO.getProductStatus());
			pstmt.setString(9, productVO.getProductID());
		
			
			pstmt.executeUpdate();

			
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		
	}

	

	@Override
	public ProductVO findByPrimaryKey(String productID) {
		ProductVO productVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ONE_STMT);

			pstmt.setString(1, productID);

			rs = pstmt.executeQuery();

			while (rs.next()) {
				
				productVO = new ProductVO();
				productVO.setProductID(rs.getString("productid"));
				productVO.setMemberID(rs.getString("memberid"));
				productVO.setArtistID(rs.getString("artistid"));
				productVO.setProductPhoto(rs.getBytes("productphoto"));
				productVO.setProductAmount(rs.getInt("productamount"));
				productVO.setProductPrice(rs.getInt("productprice"));
				productVO.setProductInfo(rs.getString("productinfo"));
				productVO.setProductName(rs.getString("productname"));
				productVO.setProductStatus(rs.getString("productstatus"));
				
				
			}

			
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return productVO;
	}

	@Override
	public List<ProductVO> getAll() {
		List<ProductVO> list = new ArrayList<ProductVO>();
		ProductVO productVO = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ALL_STMT);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				
				productVO = new ProductVO();
				productVO.setProductID(rs.getString("productid"));
				productVO.setMemberID(rs.getString("memberid"));
				productVO.setArtistID(rs.getString("artistid"));
//				productVO.setProductPhoto(rs.getBytes("productphoto"));
				productVO.setProductAmount(rs.getInt("productamount"));
				productVO.setProductPrice(rs.getInt("productprice"));
				productVO.setProductInfo(rs.getString("productinfo"));
				productVO.setProductName(rs.getString("productname"));
				productVO.setProductStatus(rs.getString("productstatus"));
				
				list.add(productVO); // Store the row in the list
			}

			
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return list;
	}

	public static byte[] getPictureByteArray(String path) throws IOException {
		File file = new File(path);
		FileInputStream fis = new FileInputStream(file);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		byte[] buffer = new byte[8192];
		int i;
		while ((i = fis.read(buffer)) != -1) {
			baos.write(buffer, 0, i);
		}
		baos.close();
		fis.close();

		return baos.toByteArray();
	}

//	@Override
//	public List<ProductVO> getAll(Map<String, String[]> map) {
//		List<ProductVO> list = new ArrayList<ProductVO>();
//		ProductVO productVO = null;
//	
//		Connection con = null;
//		PreparedStatement pstmt = null;
//		ResultSet rs = null;
//	
//		try {
//			
//			con = ds.getConnection();
//			String finalSQL = "select * from product "
////		          + jdbcUtil_CompositeQuery_Product.get_WhereCondition(map)
//		          + "order by productID desc";
//			pstmt = con.prepareStatement(finalSQL);
//			System.out.println("●●finalSQL(by DAO) = "+finalSQL);
//			rs = pstmt.executeQuery();
//	
//			while (rs.next()) {
//				
//				productVO = new ProductVO();
//				productVO.setProductID(rs.getString("productID"));
//				productVO.setMemberID(rs.getString("memberID"));
//				productVO.setArtistID(rs.getString("artistID"));
//				productVO.setProductPhoto(rs.getBytes("productPhoto"));
//				productVO.setProductAmount(rs.getInt("productAmount"));
//				productVO.setProductPrice(rs.getInt("productPrice"));
//				productVO.setProductInfo(rs.getString("productInfo"));
//				productVO.setProductName(rs.getString("productName"));
//				productVO.setProductStatus(rs.getString("productStatus"));
//				list.add(productVO); // Store the row in the list
//			}
//	
//			// Handle any SQL errors
//		} catch (SQLException se) {
//			throw new RuntimeException("A database error occured. "
//					+ se.getMessage());
//		} finally {
//			if (rs != null) {
//				try {
//					rs.close();
//				} catch (SQLException se) {
//					se.printStackTrace(System.err);
//				}
//			}
//			if (pstmt != null) {
//				try {
//					pstmt.close();
//				} catch (SQLException se) {
//					se.printStackTrace(System.err);
//				}
//			}
//			if (con != null) {
//				try {
//					con.close();
//				} catch (Exception e) {
//					e.printStackTrace(System.err);
//				}
//			}
//		}
//		return list;
//	}

	@Override
	public List<ProductVO> getAll(String memberID) {
		List<ProductVO> list = new ArrayList<ProductVO>();
		ProductVO productVO = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ALL_FOR_MEMBERID);
			pstmt.setString(1, memberID);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				
				productVO = new ProductVO();
				productVO.setProductID(rs.getString("productid"));
				productVO.setMemberID(rs.getString("memberid"));
				productVO.setArtistID(rs.getString("artistid"));
				productVO.setProductAmount(rs.getInt("productamount"));
				productVO.setProductPrice(rs.getInt("productprice"));
				productVO.setProductInfo(rs.getString("productinfo"));
				productVO.setProductName(rs.getString("productname"));
				productVO.setProductStatus(rs.getString("productstatus"));
				
				list.add(productVO); // Store the row in the list
			}

			
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return list;
	}

	@Override
	public List<ProductVO> getAll(Map<String, String[]> map) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public byte[] getProductImage(String productID) {
		byte[] productImage = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_PRODUCT_IMAGE);
			pstmt.setString(1, productID);
			rs = pstmt.executeQuery();
			if(rs.next()) {
				productImage = rs.getBytes(1);
			}
		}catch(SQLException se) {
			throw new RuntimeException("A database error occured. " + se.getMessage());
		}finally {
			if(rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if(pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if(con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
		return productImage;
	}

	@Override
	public List<ProductVO> getSearchList(String word){
		
		List<ProductVO> list = new ArrayList<ProductVO>();
		ProductVO productVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_SEARCHLIST);
			
			pstmt.setString(1, "%"+word+"%");
			
			rs = pstmt.executeQuery();

			while (rs.next()) {
				productVO = new ProductVO();
				productVO.setProductID(rs.getString("productid"));
				productVO.setMemberID(rs.getString("memberid"));
				productVO.setArtistID(rs.getString("artistid"));
				productVO.setProductAmount(rs.getInt("productamount"));
				productVO.setProductPrice(rs.getInt("productprice"));
				productVO.setProductInfo(rs.getString("productinfo"));
				productVO.setProductName(rs.getString("productname"));
				productVO.setProductStatus(rs.getString("productstatus"));
				list.add(productVO); // Store the row in the list
			}
			
			
			

			
			
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return list;
	
	}

	@Override
	public List<ProductVO> getRelatedList(String artistID) {
		List<ProductVO> list = new ArrayList<ProductVO>();
		ProductVO productVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_RELATEDLIST);
			
			pstmt.setString(1, artistID);
			
			rs = pstmt.executeQuery();

			while (rs.next()) {
				productVO = new ProductVO();
				productVO.setProductID(rs.getString("productid"));
				productVO.setMemberID(rs.getString("memberid"));
				productVO.setArtistID(rs.getString("artistid"));
				productVO.setProductAmount(rs.getInt("productamount"));
				productVO.setProductPrice(rs.getInt("productprice"));
				productVO.setProductInfo(rs.getString("productinfo"));
				productVO.setProductName(rs.getString("productname"));
				productVO.setProductStatus(rs.getString("productstatus"));
				list.add(productVO); // Store the row in the list
			}
				
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return list;
	}


}
