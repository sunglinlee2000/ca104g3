package com.android.product.model;
import java.util.*;



public interface ProductDAO_interface {
	public void insert(ProductVO productVO);
    public void update(ProductVO productVO);
    public ProductVO findByPrimaryKey(String productID);
    public List<ProductVO> getAll(String memberID);
    public List<ProductVO> getAll();
    public List<ProductVO> getSearchList(String word);
    public List<ProductVO> getRelatedList(String artistID);
    
    //萬用複合查詢(傳入參數型態Map)(回傳 List)
    public List<ProductVO> getAll(Map<String,String[]> map);
    public byte[] getProductImage(String productID);
}
