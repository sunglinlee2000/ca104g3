package com.android.product.model;

import java.util.Arrays;

public class ProductVO implements java.io.Serializable {
	private String  productID;
	private String	memberID;
	private String  artistID;
	private byte[]	productPhoto;
	private Integer	productAmount;
	private Integer productPrice;
	private String	productInfo;
	private String	productName;
	private String 	productStatus;
	private Integer quantity;
	

	public ProductVO() {
		super();
	}

	//購物車使用
	public ProductVO(String productID, String memberID, Integer productPrice, String productName, Integer quantity) {
		super();
		this.productID = productID;
		this.memberID = memberID;
		this.productPrice = productPrice;
		this.productName = productName;
		this.quantity = quantity;
	}


	public String getProductID() {
		return productID;
	}


	public void setProductID(String productID) {
		this.productID = productID;
	}


	public String getMemberID() {
		return memberID;
	}


	public void setMemberID(String memberID) {
		this.memberID = memberID;
	}


	public String getArtistID() {
		return artistID;
	}


	public void setArtistID(String artistID) {
		this.artistID = artistID;
	}


	public byte[] getProductPhoto() {
		return productPhoto;
	}


	public void setProductPhoto(byte[] productPhoto) {
		this.productPhoto = productPhoto;
	}


	public Integer getProductAmount() {
		return productAmount;
	}


	public void setProductAmount(Integer productAmount) {
		this.productAmount = productAmount;
	}


	public Integer getProductPrice() {
		return productPrice;
	}


	public void setProductPrice(Integer productPrice) {
		this.productPrice = productPrice;
	}


	public String getProductInfo() {
		return productInfo;
	}


	public void setProductInfo(String productInfo) {
		this.productInfo = productInfo;
	}


	public String getProductName() {
		return productName;
	}


	public void setProductName(String productName) {
		this.productName = productName;
	}


	public String getProductStatus() {
		return productStatus;
	}


	public void setProductStatus(String productStatus) {
		this.productStatus = productStatus;
	}


	public Integer getQuantity() {
		return quantity;
	}


	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	@Override
	public String toString() {
		return "ProductVO [productID=" + productID + ", memberID=" + memberID + ", artistID=" + artistID
				+ ", productPhoto=" + Arrays.toString(productPhoto) + ", productAmount=" + productAmount
				+ ", productPrice=" + productPrice + ", productInfo=" + productInfo + ", productName=" + productName
				+ ", productStatus=" + productStatus + ", quantity=" + quantity + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((artistID == null) ? 0 : artistID.hashCode());
		result = prime * result + ((memberID == null) ? 0 : memberID.hashCode());
		result = prime * result + ((productAmount == null) ? 0 : productAmount.hashCode());
		result = prime * result + ((productID == null) ? 0 : productID.hashCode());
		result = prime * result + ((productInfo == null) ? 0 : productInfo.hashCode());
		result = prime * result + ((productName == null) ? 0 : productName.hashCode());
		result = prime * result + Arrays.hashCode(productPhoto);
		result = prime * result + ((productPrice == null) ? 0 : productPrice.hashCode());
		result = prime * result + ((productStatus == null) ? 0 : productStatus.hashCode());
		result = prime * result + ((quantity == null) ? 0 : quantity.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProductVO other = (ProductVO) obj;
		if (artistID == null) {
			if (other.artistID != null)
				return false;
		} else if (!artistID.equals(other.artistID))
			return false;
		if (memberID == null) {
			if (other.memberID != null)
				return false;
		} else if (!memberID.equals(other.memberID))
			return false;
		if (productAmount == null) {
			if (other.productAmount != null)
				return false;
		} else if (!productAmount.equals(other.productAmount))
			return false;
		if (productID == null) {
			if (other.productID != null)
				return false;
		} else if (!productID.equals(other.productID))
			return false;
		if (productInfo == null) {
			if (other.productInfo != null)
				return false;
		} else if (!productInfo.equals(other.productInfo))
			return false;
		if (productName == null) {
			if (other.productName != null)
				return false;
		} else if (!productName.equals(other.productName))
			return false;
		if (!Arrays.equals(productPhoto, other.productPhoto))
			return false;
		if (productPrice == null) {
			if (other.productPrice != null)
				return false;
		} else if (!productPrice.equals(other.productPrice))
			return false;
		if (productStatus == null) {
			if (other.productStatus != null)
				return false;
		} else if (!productStatus.equals(other.productStatus))
			return false;
		if (quantity == null) {
			if (other.quantity != null)
				return false;
		} else if (!quantity.equals(other.quantity))
			return false;
		return true;
	}
	
	
	
}
