package com.issueShare.model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

import com.issue.model.IssueVO;

public class IssueShareJDBCDAO implements IssueShareDAO_interface{
	String driver = "oracle.jdbc.driver.OracleDriver";
	String url = "jdbc:oracle:thin:@localhost:1521:XE";
	String userid = "CA104G3";
	String passwd = "123456";
	
	private static final String INSERT_STMT =
			"INSERT INTO issueshare "
			+ "(issueID,memberID,issueShareDate)"
			+ " VALUES "
			+ "(?,?,?)";
		private static final String DELETE = 
			"DELETE FROM issueshare where issueID = ? and memberID = ?";
		private static final String GET_ONE_STMT = 
			"SELECT issueID,memberID,issueShareDate FROM issueshare where issueID = ? and memberID= ?";
		
		private static final String DELETEBYISSUEID = 
				"DELETE FROM issueShare where issueID = ?";
		
		@Override
		public void insert(IssueShareVO issueShareVO) {
			Connection con = null;
			PreparedStatement pstmt = null;
			
			try {

				Class.forName(driver);
				con = DriverManager.getConnection(url, userid, passwd);
				pstmt = con.prepareStatement(INSERT_STMT);

				pstmt.setString(1, issueShareVO.getIssueID());
				pstmt.setString(2, issueShareVO.getMemberID());
				pstmt.setTimestamp(3, issueShareVO.getIssueShareDate());
				
				pstmt.executeUpdate();

				// Handle any driver errors
			} catch (ClassNotFoundException e) {
				throw new RuntimeException("Couldn't load database driver. "
						+ e.getMessage());
				// Handle any SQL errors
			} catch (SQLException se) {
				throw new RuntimeException("A database error occured. "
						+ se.getMessage());
				// Clean up JDBC resources
			} finally {
				if (pstmt != null) {
					try {
						pstmt.close();
					} catch (SQLException se) {
						se.printStackTrace(System.err);
					}
				}
				if (con != null) {
					try {
						con.close();
					} catch (Exception e) {
						e.printStackTrace(System.err);
					}
				}
			}
			
		}
		@Override
		public void delete(String issueID,String memberID) {
			Connection con = null;
			PreparedStatement pstmt = null;

			try {

				Class.forName(driver);
				con = DriverManager.getConnection(url, userid, passwd);
				pstmt = con.prepareStatement(DELETE);

				pstmt.setString(1, issueID);
				pstmt.setString(2, memberID);
				pstmt.executeUpdate();

				// Handle any driver errors
			} catch (ClassNotFoundException e) {
				throw new RuntimeException("Couldn't load database driver. "
						+ e.getMessage());
				// Handle any SQL errors
			} catch (SQLException se) {
				throw new RuntimeException("A database error occured. "
						+ se.getMessage());
				// Clean up JDBC resources
			} finally {
				if (pstmt != null) {
					try {
						pstmt.close();
					} catch (SQLException se) {
						se.printStackTrace(System.err);
					}
				}
				if (con != null) {
					try {
						con.close();
					} catch (Exception e) {
						e.printStackTrace(System.err);
					}
				}
			}

			
		}
		@Override
		public IssueShareVO findByPrimaryKey(String issueID,String memberID) {
			IssueShareVO issueShareVO = null;
			Connection con = null;
			PreparedStatement pstmt = null;
			ResultSet rs = null;

			try {

				Class.forName(driver);
				con = DriverManager.getConnection(url, userid, passwd);
				pstmt = con.prepareStatement(GET_ONE_STMT);

				pstmt.setString(1, issueID);

				rs = pstmt.executeQuery();

				while (rs.next()) {
					// empVo �]�٬� Domain objects
					
					issueShareVO = new IssueShareVO();
					issueShareVO.setIssueID(rs.getString("issueID"));
					issueShareVO.setMemberID(rs.getString("memberID"));
					issueShareVO.setIssueShareDate(rs.getTimestamp("issueShareDate"));
				}

				// Handle any driver errors
			} catch (ClassNotFoundException e) {
				throw new RuntimeException("Couldn't load database driver. "
						+ e.getMessage());
				// Handle any SQL errors
			} catch (SQLException se) {
				throw new RuntimeException("A database error occured. "
						+ se.getMessage());
				// Clean up JDBC resources
			} finally {
				if (rs != null) {
					try {
						rs.close();
					} catch (SQLException se) {
						se.printStackTrace(System.err);
					}
				}
				if (pstmt != null) {
					try {
						pstmt.close();
					} catch (SQLException se) {
						se.printStackTrace(System.err);
					}
				}
				if (con != null) {
					try {
						con.close();
					} catch (Exception e) {
						e.printStackTrace(System.err);
					}
				}
			}
			return issueShareVO;
		}
		
		@Override
		public void deleteByIssueID(String issueID) {
			Connection con = null;
			PreparedStatement pstmt = null;

			try {

				Class.forName(driver);
				con = DriverManager.getConnection(url, userid, passwd);
				pstmt = con.prepareStatement(DELETEBYISSUEID);

				pstmt.setString(1, issueID);
				pstmt.executeUpdate();

				// Handle any driver errors
			} catch (ClassNotFoundException e) {
				throw new RuntimeException("Couldn't load database driver. "
						+ e.getMessage());
				// Handle any SQL errors
			} catch (SQLException se) {
				throw new RuntimeException("A database error occured. "
						+ se.getMessage());
				// Clean up JDBC resources
			} finally {
				if (pstmt != null) {
					try {
						pstmt.close();
					} catch (SQLException se) {
						se.printStackTrace(System.err);
					}
				}
				if (con != null) {
					try {
						con.close();
					} catch (Exception e) {
						e.printStackTrace(System.err);
					}
				}
			}

			
		}
		
		
		public static void main(String[] args) {
			IssueShareJDBCDAO dao = new IssueShareJDBCDAO();
			
			// 新增OK
//			IssueShareVO issueShareVO1 = new IssueShareVO();
//			issueShareVO1.setIssueID("I000003");
//			issueShareVO1.setMemberID("M000006");
//			issueShareVO1.setIssueShareDate(java.sql.Timestamp.valueOf(ZonedDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"))));
//			dao.insert(issueShareVO1);
//			System.out.println("OK");
			
//			// 刪除OK
//			dao.delete("I000003","M000006");
//			System.out.println("OK");
			
			//刪除deleteByIssueID OK
//			dao.deleteByIssueID("I000001");
			
		}
	
	

}
