package com.issueShare.model;

import com.issueShare.model.IssueShareVO;

public interface IssueShareDAO_interface {
	
	public void insert(IssueShareVO issueSsharVO);
	public void delete(String issueID,String memberID);
	public IssueShareVO findByPrimaryKey(String issueID,String memberID);
	public void deleteByIssueID(String issueID);

}
