package com.issueShare.model;

import java.sql.Timestamp;

public class IssueShareVO {
	private String issueID;
	private String memberID;
	private Timestamp issueShareDate;
	
	public String getIssueID() {
		return issueID;
	}
	public void setIssueID(String issueID) {
		this.issueID = issueID;
	}
	public String getMemberID() {
		return memberID;
	}
	public void setMemberID(String memberID) {
		this.memberID = memberID;
	}
	public Timestamp getIssueShareDate() {
		return issueShareDate;
	}
	public void setIssueShareDate(Timestamp issueShareDate) {
		this.issueShareDate = issueShareDate;
	}		
	
}
