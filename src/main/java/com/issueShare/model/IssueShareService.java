package com.issueShare.model;

import java.sql.Timestamp;

public class IssueShareService {
	
	private IssueShareDAO_interface issueShareDAO;
	
	public IssueShareService() {
		issueShareDAO = new IssueShareDAO();
	}
	
	public IssueShareVO addIssueShare(String issueID,String memberID,Timestamp issueShareDate) {
		
		IssueShareVO issueShareVO = new IssueShareVO();
		
		issueShareVO.setIssueID(issueID);
		issueShareVO.setMemberID(memberID);
		issueShareVO.setIssueShareDate(issueShareDate);
		
		return issueShareVO;		
	}
	
	public void deleteIssueShare(String issueID,String memberID) {
		issueShareDAO.delete(issueID, memberID);
	}
	
	public void deleteByIssueID(String issueID) {
		issueShareDAO.deleteByIssueID(issueID);
	}
}
