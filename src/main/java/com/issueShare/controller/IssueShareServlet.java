package com.issueShare.controller;

import java.io.*;
import java.sql.Timestamp;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

import javax.servlet.*;
import javax.servlet.http.*;

import com.issueShare.model.*;

public class IssueShareServlet extends HttpServlet {

	public void doGet(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		doPost(req, res);
	}

	public void doPost(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {

		req.setCharacterEncoding("UTF-8");
		String action = req.getParameter("action");
		

        if ("insert".equals(action)) { // 來自addEmp.jsp的請求  
			
			List<String> errorMsgs = new LinkedList<String>();
			// Store this set in the request scope, in case we need to
			// send the ErrorPage view.
			req.setAttribute("errorMsgs", errorMsgs);

			try {
				/***********************1.接收請求參數 - 輸入格式的錯誤處理*************************/
				
				String issueID = req.getParameter("issueID");
				String memberID = req.getParameter("memberID");
				String issueShareDate = req.getParameter("issueShareDate");
				

				IssueShareVO issueShareVO = new IssueShareVO();				
				issueShareVO.setIssueID(issueID);
				issueShareVO.setMemberID("M000001");
				Timestamp time = new Timestamp(System.currentTimeMillis());
				issueShareVO.setIssueShareDate(java.sql.Timestamp.valueOf(ZonedDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"))));
				
				// Send the use back to the form, if there were errors
				if (!errorMsgs.isEmpty()) {
					req.setAttribute("issueShareVO", issueShareVO); // 含有輸入格式錯誤的empVO物件,也存入req
					RequestDispatcher failureView = req
							.getRequestDispatcher("/front-end/issueShare/addIssueShare.jsp");
					failureView.forward(req, res);
					return;
				}
				
				/***************************2.開始新增資料***************************************/
				IssueShareService issueShareSvc = new IssueShareService();
				issueShareVO = issueShareSvc.addIssueShare(issueID, "M000001",time);
				
				/***************************3.新增完成,準備轉交(Send the Success view)***********/
				String url = "/front-end/issueShare/listAllEmp.jsp";
				RequestDispatcher successView = req.getRequestDispatcher(url); // 新增成功後轉交listAllEmp.jsp
				successView.forward(req, res);				
				
				/***************************其他可能的錯誤處理**********************************/
			} catch (Exception e) {
				errorMsgs.add(e.getMessage());
				RequestDispatcher failureView = req
						.getRequestDispatcher("/front-end/issueShare/addIssueShare.jsp");
				failureView.forward(req, res);
			}
		}
		
		
		if ("delete".equals(action)) { // 來自listAllEmp.jsp

			List<String> errorMsgs = new LinkedList<String>();
			// Store this set in the request scope, in case we need to
			// send the ErrorPage view.
			req.setAttribute("errorMsgs", errorMsgs);
	
			try {
				/***************************1.接收請求參數***************************************/
				String issueID = new String(req.getParameter("issueID"));
				String memberID = new String(req.getParameter("memberID"));
				
				/***************************2.開始刪除資料***************************************/
				IssueShareService issueShareSvc = new IssueShareService();
				issueShareSvc.deleteIssueShare(issueID,memberID);
				
				/***************************3.刪除完成,準備轉交(Send the Success view)***********/								
				String url = "/front-end/issueShare/listAllIssueShare.jsp";
				RequestDispatcher successView = req.getRequestDispatcher(url);// 刪除成功後,轉交回送出刪除的來源網頁
				successView.forward(req, res);
				
				/***************************其他可能的錯誤處理**********************************/
			} catch (Exception e) {
				errorMsgs.add("刪除資料失敗:"+e.getMessage());
				RequestDispatcher failureView = req
						.getRequestDispatcher("/front-end/issueShare/listAllEmp.jsp");
				failureView.forward(req, res);
			}
		}
	}
}
