package com.adfundRecord.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Timestamp;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;

import com.adfund.model.*;
import com.adfundRecord.model.*;
import com.member.model.*;


@MultipartConfig(fileSizeThreshold = 1024*1024, maxFileSize = 5*1024*1024, maxRequestSize = 5*5*1024)
public class ADFundRecordServlet extends HttpServlet{
	
	public void doGet(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		doPost(req, res);
	}

	public void doPost(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {

		req.setCharacterEncoding("UTF-8");
		String action = req.getParameter("action");
		System.out.println("adrcs:" + action);
		
		
		
//        if ("insert".equals(action)) { 
//        	
//        	System.out.println(3);
//        	
//			List<String> errorMsgs = new LinkedList<String>();
//			// Store this set in the request scope, in case we need to
//			// send the ErrorPage view.
//			req.setAttribute("errorMsgs", errorMsgs);
//
//			try {
//				/***********************1.接收請求參數 - 輸入格式的錯誤處理*************************/
//				  
//			    //會員欲使用的代幣量
//			    String adfundID = req.getParameter("adfundID");
//			    System.out.println(adfundID);
//
//				String memberID = req.getParameter("memberID");
//				System.out.println(memberID);
//              
//				Integer adfundCoinUsed = new Integer(req.getParameter("adfundCoinUsed"));
//				
//				int adfundCoinUsedint = adfundCoinUsed;
//				
//				Timestamp adfundJoinDate = new Timestamp(System.currentTimeMillis());
//				
//				Long adfundJoinDatelong = System.currentTimeMillis();
//                
//				ADFundRecordVO adfundRecordVO = new ADFundRecordVO();
//				
//								
//                //增加募資現有代幣量								
//				ADFundService adfundSvc = new ADFundService();
//				
//				ADFundVO adfundVO = adfundSvc.getOneADFund(adfundID); //先取得這個該募資的VO
//				
//				Integer adfundCoinNow = adfundVO.getAdfundCoinNow() + adfundCoinUsed; //再取得這個該募資的VO裡的代幣現有量並加上新得到的代幣
//				System.out.println(adfundCoinNow);
//				
//				Long adfundEndDatelong = adfundVO.getAdfundEndDate().getTime();
//				
//				String adfundStatus = adfundVO.getAdfundStatus();//狀態為"募資中"改為"未提交已審核"
//					
//
//				
//				
//				//減少會員持有的代幣量
//				MemberService memberSvc = new MemberService();
//				
//				MemberVO memberVO = memberSvc.getOneMember(memberID);
//				
//				Integer memberCoin = memberVO.getMemberCoin() - adfundCoinUsed;
//				
//				System.out.println(memberCoin);
//				
//				
//				if(memberVO.getMemberCoin() < adfundCoinUsed) {
//					errorMsgs.add("錢不夠 你無法參與募資喔");
//				}
//				
//				if(adfundCoinUsedint <= 0) {
//					errorMsgs.add("請輸入正整數");
//				}
//				
//				if(adfundJoinDatelong > adfundEndDatelong) {
//					errorMsgs.add("時間已過無法參與");
//				}								
//				
//				
//				
//				if (!errorMsgs.isEmpty()) {
//					req.setAttribute("adfundRecordVO", adfundRecordVO); // 含有輸入格式錯誤的empVO物件,也存入req
//					RequestDispatcher failureView = req
//							.getRequestDispatcher("/front-end/adfund/adfund_solo.jsp");
//					failureView.forward(req, res);
//					return; //程式中斷
//				}
//
//				
//				//------------------
//				
//				//會員欲使用的代幣量
//				adfundRecordVO.setAdfundID(adfundID);
//				adfundRecordVO.setMemberID(memberID);
//				adfundRecordVO.setAdfundCoinUsed(adfundCoinUsed);
//				adfundRecordVO.setAdfundJoinDate(adfundJoinDate);
//				
//								
//                //增加募資現有代幣量													
//				adfundVO.setAdfundCoinNow(adfundCoinNow);
//				adfundVO.setAdfundID(adfundID);
//				
//				//如果募資達成滿足條件就修改狀態
//				if(adfundJoinDatelong < adfundEndDatelong & adfundCoinNow >= 3000) {					
//					 adfundVO.setAdfundStatus("AD_SUBMITTED");
//				}
//				
//				
//				//減少會員持有的代幣量				
//				memberVO.setMemberID(memberID);
//				memberVO.setMemberCoin(memberCoin);
//								
//
//
//				// Send the use back to the form, if there were errors
//				if (!errorMsgs.isEmpty()) {
//					req.setAttribute("adfundRecordVO", adfundRecordVO); // 含有輸入格式錯誤的empVO物件,也存入req
//					RequestDispatcher failureView = req
//							.getRequestDispatcher("/front-end/adfund/adfund_list.jsp");
//					failureView.forward(req, res);
//					return;
//				}
//				
//				/***************************2.開始新增資料***************************************/
//				//會員欲使用的代幣量
//				ADFundRecordService adfundRecordSvc = new ADFundRecordService();
//				adfundRecordVO = adfundRecordSvc.addADFundRecord(adfundID, memberID, adfundCoinUsed, adfundJoinDate);
//												
//				//增加募資現有代幣量
//				adfundVO = adfundSvc.updateADFundCoinNow(adfundID, adfundCoinNow);
//				adfundVO = adfundSvc.updateADFundByAd(adfundID, "AD_SUBMITTED");
//								
//				//減少會員持有的代幣量
//				memberVO = memberSvc.updateMemberCoin(memberCoin, memberID);				
//				
//				/***************************3.新增完成,準備轉交(Send the Success view)***********/
////				String url = "/front-end/adfund/adfund_solo.jsp";
////				RequestDispatcher successView = req.getRequestDispatcher(url); // 新增成功後轉交listAllEmp.jsp
////				successView.forward(req, res);	
//				
//				res.sendRedirect(req.getContextPath() + "/adfundTest/adfund.do?action=getOne_For_Display&adfundID="+adfundID);
//				
//				/***************************其他可能的錯誤處理**********************************/
//			} 
//			catch (Exception e) {
//				errorMsgs.add(e.getMessage());
//				RequestDispatcher failureView = req
//						.getRequestDispatcher("/front-end/adfund/adfund_solo.jsp");
//				failureView.forward(req, res);
//			}
//		}
		
		
		
		//ajax寫法
        if ("insert".equals(action)) { 
        	
        	System.out.println(3);
        	
			List<String> errorMsgs = new LinkedList<String>();
			// Store this set in the request scope, in case we need to
			// send the ErrorPage view.
			req.setAttribute("errorMsgs", errorMsgs);


				  
			    //會員欲使用的代幣量
			    String adfundID = req.getParameter("adfundID");

				String memberID = req.getParameter("memberID");
              
				Integer adfundCoinUsed = new Integer(req.getParameter("adfundCoinUsed"));
				
				int adfundCoinUsedint = adfundCoinUsed;
				
				Timestamp adfundJoinDate = new Timestamp(System.currentTimeMillis());
				
				Long adfundJoinDatelong = System.currentTimeMillis();
                
				ADFundRecordVO adfundRecordVO = new ADFundRecordVO();
				
								
                //增加募資現有代幣量								
				ADFundService adfundSvc = new ADFundService();
				
				ADFundVO adfundVO = adfundSvc.getOneADFund(adfundID); //先取得這個該募資的VO
				
				Integer adfundCoinNow = adfundVO.getAdfundCoinNow() + adfundCoinUsed; //再取得這個該募資的VO裡的代幣現有量並加上新得到的代幣
				System.out.println(adfundCoinNow);
				
				Long adfundEndDatelong = adfundVO.getAdfundEndDate().getTime();
				
				String adfundStatus = adfundVO.getAdfundStatus();//狀態為"募資中"改為"未提交已審核"
					

				
				
				//減少會員持有的代幣量
				MemberService memberSvc = new MemberService();
				
				MemberVO memberVO = memberSvc.getOneMember(memberID);
				
				Integer memberCoin = memberVO.getMemberCoin() - adfundCoinUsed;
				
				System.out.println(memberCoin);
				
				
				if(memberVO.getMemberCoin() < adfundCoinUsed) {
					errorMsgs.add("錢不夠 你無法參與募資喔");
				}
				
				if(adfundCoinUsedint <= 0) {
					errorMsgs.add("請輸入正整數");
				}
				
				if(adfundJoinDatelong > adfundEndDatelong) {
					errorMsgs.add("時間已過無法參與");
				}								
				
				
				
				if (!errorMsgs.isEmpty()) {
					req.setAttribute("adfundRecordVO", adfundRecordVO); // 含有輸入格式錯誤的empVO物件,也存入req
					RequestDispatcher failureView = req
							.getRequestDispatcher("/front-end/adfund/adfund_solo.jsp");
					failureView.forward(req, res);
					return; //程式中斷
				}

				
				//------------------
				
				//會員欲使用的代幣量
				adfundRecordVO.setAdfundID(adfundID);
				adfundRecordVO.setMemberID(memberID);
				adfundRecordVO.setAdfundCoinUsed(adfundCoinUsed);
				adfundRecordVO.setAdfundJoinDate(adfundJoinDate);
				
								
                //增加募資現有代幣量													
				adfundVO.setAdfundCoinNow(adfundCoinNow);
				adfundVO.setAdfundID(adfundID);
								
				
				//減少會員持有的代幣量				
				memberVO.setMemberID(memberID);
				memberVO.setMemberCoin(memberCoin);
								


				// Send the use back to the form, if there were errors
				if (!errorMsgs.isEmpty()) {
					req.setAttribute("adfundRecordVO", adfundRecordVO); // 含有輸入格式錯誤的empVO物件,也存入req
					RequestDispatcher failureView = req
							.getRequestDispatcher("/front-end/adfund/adfund_list.jsp");
					failureView.forward(req, res);
					return;
				}
				
				/***************************2.開始新增資料***************************************/
				//會員欲使用的代幣量
				ADFundRecordService adfundRecordSvc = new ADFundRecordService();
				adfundRecordVO = adfundRecordSvc.addADFundRecord(adfundID, memberID, adfundCoinUsed, adfundJoinDate);
												
				//增加募資現有代幣量
				adfundVO = adfundSvc.updateADFundCoinNow(adfundID, adfundCoinNow);
				
				//如果募資達成滿足條件就修改狀態
				if(adfundJoinDatelong < adfundEndDatelong & adfundCoinNow >= 3000) {					
					 adfundVO.setAdfundStatus("AD_SUBMITTED");
					 adfundVO = adfundSvc.updateADFundByAd(adfundID, "AD_SUBMITTED");
				}
				
								
				//減少會員持有的代幣量
				memberVO = memberSvc.updateMemberCoin(memberCoin, memberID);			
				
				
				System.out.println("募資明細新增完成");
				
				
				JSONObject obj = new JSONObject(); 
				
				try { 
					obj.put("adfundCoinNow", adfundCoinNow); 
				} catch (JSONException e) { 
					e.printStackTrace(); 
				} 
				
				res.setContentType("text/plain"); 
				res.setCharacterEncoding("UTF-8"); 
				PrintWriter out = res.getWriter(); 
				out.write(obj.toString()); 
				out.flush(); 
				out.close();
				

		}
        
        
        
        //退代幣
        if ("returnCoin".equals(action)) { 
        	
        	
			List<String> errorMsgs = new LinkedList<String>();
			// Store this set in the request scope, in case we need to
			// send the ErrorPage view.
			req.setAttribute("errorMsgs", errorMsgs);

			try {
				
			String adfundID = req.getParameter("adfundID");
			
			System.out.println(adfundID);
			
			
			ADFundRecordService ADRecSvc = new ADFundRecordService();
			List<ADFundRecordVO> ADRecList = ADRecSvc.getAllADFundRecord(adfundID);
			
			MemberService memSvc = new MemberService();
			
			
			for(ADFundRecordVO ADRecList2 : ADRecList) {
				//退代幣給每個有參加的會員
				System.out.println(ADRecList2.getMemberID());
                MemberVO memberVO = memSvc.getOneMember(ADRecList2.getMemberID());				
				Integer memberCoin = memberVO.getMemberCoin() + ADRecList2.getAdfundCoinUsed();
				
				memberVO.setMemberID(ADRecList2.getMemberID());
				memberVO.setMemberCoin(memberCoin);
				
				memberVO = memSvc.updateMemberCoin(memberCoin, ADRecList2.getMemberID());	
				
				
				//將募資參與明細之每個參與者的代幣歸零
				System.out.println(ADRecList2.getAdfundRecordID());
				ADFundRecordVO adfundRecordVO = ADRecSvc.getOneADFundRecord(ADRecList2.getAdfundRecordID());
				Integer adfundCoinUsed = adfundRecordVO.getAdfundCoinUsed() - adfundRecordVO.getAdfundCoinUsed();
				
				adfundRecordVO.setAdfundRecordID(ADRecList2.getAdfundRecordID());
				adfundRecordVO.setAdfundCoinUsed(adfundCoinUsed);
				
				adfundRecordVO = ADRecSvc.updateADRecCoin(adfundCoinUsed, ADRecList2.getAdfundRecordID());
				
			}
			
			res.sendRedirect(req.getContextPath() + "/back-end/adfund/adfund_back_list.jsp");
				
			} 
			catch (Exception e) {
				errorMsgs.add(e.getMessage());
				RequestDispatcher failureView = req
						.getRequestDispatcher("/back-end/adfund/adfund_back_list.jsp");
				failureView.forward(req, res);
			}
		
	}	
	}
}
