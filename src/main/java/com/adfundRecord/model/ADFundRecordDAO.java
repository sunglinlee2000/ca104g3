package com.adfundRecord.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

public class ADFundRecordDAO implements ADFundRecordDAO_interface {

	private static DataSource ds = null;
	static {
		try {
			Context ctx = new InitialContext();
			ds = (DataSource) ctx.lookup("java:comp/env/jdbc/CA104G3DB");
		} catch (NamingException e) {
			e.printStackTrace();
		}
	}
			
	private static final String INSERT_STMT = "INSERT INTO ADFUNDRECORD VALUES " + "('ADFR'||(LPAD(TO_CHAR(ADFUND_SEQ.NEXTVAL),6,'0')), ?, ?, ?, ?)";
	private static final String GET_ALL_STMT = "SELECT * FROM ADFUNDRECORD WHERE ADFUNDID = ?";
	private static final String GET_ONE_STMT = "SELECT * FROM ADFUNDRECORD WHERE ADFUNDRECORDID = ?";
	private static final String UPDATE_COIN = "UPDATE adfundrecord set adfundCoinUsed=? WHERE adfundRecordID = ? ";

	
	@Override
	public void insert(ADFundRecordVO adfundRecVO) {

		Connection con = null;
		PreparedStatement pstmt = null;

		try {


			con = ds.getConnection();
			pstmt = con.prepareStatement(INSERT_STMT);
		       
			pstmt.setString(1, adfundRecVO.getAdfundID());
			pstmt.setString(2, adfundRecVO.getMemberID());
			pstmt.setInt(3, adfundRecVO.getAdfundCoinUsed());
			pstmt.setTimestamp(4, adfundRecVO.getAdfundJoinDate());

			pstmt.executeUpdate();

		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. " + se.getMessage());

		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}

			if (con != null) {
				try {
					con.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
		}
	}

	@Override
	public ADFundRecordVO findByPrimaryKey(String adfundrecordid) {

		ADFundRecordVO adfrVO = new ADFundRecordVO();
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {


			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ONE_STMT);
			pstmt.setString(1, adfundrecordid);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				adfrVO = new ADFundRecordVO();
				adfrVO.setAdfundRecordID(rs.getString("adfundrecordid"));
				adfrVO.setAdfundID(rs.getString("adfundid"));
				adfrVO.setMemberID(rs.getString("memberid"));
				adfrVO.setAdfundCoinUsed(rs.getInt("adfundcoinused"));
				adfrVO.setAdfundJoinDate(rs.getTimestamp("adfundjoindate"));
			}

		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. " + se.getMessage());

		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return adfrVO;
	}

	@Override
	public List<ADFundRecordVO> getAll(String adfundID) {
		List<ADFundRecordVO> adfrlist= new ArrayList<ADFundRecordVO>();
		ADFundRecordVO adfrVO = null;
		
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ALL_STMT);
			pstmt.setString(1, adfundID);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				adfrVO = new ADFundRecordVO();
				adfrVO.setAdfundRecordID(rs.getString("adfundrecordid"));
				adfrVO.setAdfundID(adfundID);
				adfrVO.setMemberID(rs.getString("memberid"));
				adfrVO.setAdfundCoinUsed(rs.getInt("adfundcoinused"));
				adfrVO.setAdfundJoinDate(rs.getTimestamp("adfundjoindate"));
				adfrlist.add(adfrVO);
			}

		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. " + se.getMessage());

		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return adfrlist;
	}

	@Override
	public void updateCoin(ADFundRecordVO adfundRecordVO) {
		Connection con = null;
		PreparedStatement pstmt = null;
		
		try {
			
			con =ds.getConnection();
			pstmt = con.prepareStatement(UPDATE_COIN);
			
			
			pstmt.setInt(1, adfundRecordVO.getAdfundCoinUsed());
			pstmt.setString(2, adfundRecordVO.getAdfundRecordID());
			
			pstmt.executeUpdate();	
			
			
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured." + se.getMessage());
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		
	}
	
}
