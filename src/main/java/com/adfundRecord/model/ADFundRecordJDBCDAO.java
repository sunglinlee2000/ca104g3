//package com.adfundRecord.model;
//
//import java.sql.*;
//import java.util.*;
//
//public class ADFundRecordJDBCDAO implements ADFundRecordDAO_interface {
//
//	String driver = "oracle.jdbc.driver.OracleDriver";
//	String url = "jdbc:oracle:thin:@localhost:1521:XE";
//	String userid = "CA104G3";
//	String passwd = "123456";
//
//	private static final String INSERT_STMT = "INSERT INTO ADFUNDRECORD VALUES " + "('ADFR'||(LPAD(TO_CHAR(ADFUND_SEQ.NEXTVAL),6,'0')), ?, ?, ?, ?)";
//	private static final String GET_ALL_STMT = "SELECT * FROM ADFUNDRECORD";
//	private static final String GET_ONE_STMT = "SELECT * FROM ADFUNDRECORD WHERE ADFUNDRECORDID = ?";
//
//	@Override
//	public void insert(ADFundRecordVO adfundRecVO) {
//
//		Connection con = null;
//		PreparedStatement pstmt = null;
//
//		try {
//
//			Class.forName(driver);
//			con = DriverManager.getConnection(url, userid, passwd);
//			pstmt = con.prepareStatement(INSERT_STMT);
//		       
//			pstmt.setString(1, adfundRecVO.getAdfundID());
//			pstmt.setString(2, adfundRecVO.getMemberID());
//			pstmt.setInt(3, adfundRecVO.getAdfundCoinUsed());
//			pstmt.setTimestamp(4, adfundRecVO.getAdfundJoinDate());
//
//			pstmt.executeUpdate();
//
//		} catch (ClassNotFoundException e) {
//			throw new RuntimeException("Couldn't load database driver. " + e.getMessage());
//
//		} catch (SQLException se) {
//			throw new RuntimeException("A database error occured. " + se.getMessage());
//
//		} finally {
//			if (pstmt != null) {
//				try {
//					pstmt.close();
//				} catch (SQLException se) {
//					se.printStackTrace(System.err);
//				}
//			}
//
//			if (con != null) {
//				try {
//					con.close();
//				} catch (SQLException se) {
//					se.printStackTrace(System.err);
//				}
//			}
//		}
//	}
//
//	@Override
//	public ADFundRecordVO findByPrimaryKey(String adfundrecordid) {
//
//		ADFundRecordVO adfrVO = new ADFundRecordVO();
//		Connection con = null;
//		PreparedStatement pstmt = null;
//		ResultSet rs = null;
//
//		try {
//
//			Class.forName(driver);
//			con = DriverManager.getConnection(url, userid, passwd);
//			pstmt = con.prepareStatement(GET_ONE_STMT);
//
//			pstmt.setString(1, adfundrecordid);
//
//
//			rs = pstmt.executeQuery();
//
//			while (rs.next()) {
//				adfrVO = new ADFundRecordVO();
//				adfrVO.setAdfundID(rs.getString("adfundrecordid"));
//				adfrVO.setAdfundID(rs.getString("adfundid"));
//				adfrVO.setMemberID(rs.getString("memberid"));
//				adfrVO.setAdfundCoinUsed(rs.getInt("adfundcoinused"));
//				adfrVO.setAdfundJoinDate(rs.getTimestamp("adfundjoindate"));
//			}
//
//		} catch (ClassNotFoundException e) {
//			throw new RuntimeException("Couldn't load database driver. " + e.getMessage());
//
//		} catch (SQLException se) {
//			throw new RuntimeException("A database error occured. " + se.getMessage());
//
//		} finally {
//			if (rs != null) {
//				try {
//					rs.close();
//				} catch (SQLException se) {
//					se.printStackTrace(System.err);
//				}
//			}
//			if (pstmt != null) {
//				try {
//					pstmt.close();
//				} catch (SQLException se) {
//					se.printStackTrace(System.err);
//				}
//			}
//			if (con != null) {
//				try {
//					con.close();
//				} catch (Exception e) {
//					e.printStackTrace(System.err);
//				}
//			}
//		}
//		return adfrVO;
//	}
//
//	@Override
//	public List<ADFundRecordVO> getAll() {
//		List<ADFundRecordVO> adfrlist= new ArrayList<ADFundRecordVO>();
//		ADFundRecordVO adfrVO = null;
//		
//		Connection con = null;
//		PreparedStatement pstmt = null;
//		ResultSet rs = null;
//
//		try {
//
//			Class.forName(driver);
//			con = DriverManager.getConnection(url, userid, passwd);
//			pstmt = con.prepareStatement(GET_ALL_STMT);
//
//			rs = pstmt.executeQuery();
//
//			while (rs.next()) {
//				adfrVO = new ADFundRecordVO();
//				adfrVO.setAdfundID(rs.getString("adfundrecordid"));
//				adfrVO.setAdfundID(rs.getString("adfundid"));
//				adfrVO.setMemberID(rs.getString("memberid"));
//				adfrVO.setAdfundCoinUsed(rs.getInt("adfundcoinused"));
//				adfrVO.setAdfundJoinDate(rs.getTimestamp("adfundjoindate"));
//				adfrlist.add(adfrVO);
//			}
//
//		} catch (ClassNotFoundException e) {
//			throw new RuntimeException("Couldn't load database driver. " + e.getMessage());
//
//		} catch (SQLException se) {
//			throw new RuntimeException("A database error occured. " + se.getMessage());
//
//		} finally {
//			if (rs != null) {
//				try {
//					rs.close();
//				} catch (SQLException se) {
//					se.printStackTrace(System.err);
//				}
//			}
//			if (pstmt != null) {
//				try {
//					pstmt.close();
//				} catch (SQLException se) {
//					se.printStackTrace(System.err);
//				}
//			}
//			if (con != null) {
//				try {
//					con.close();
//				} catch (Exception e) {
//					e.printStackTrace(System.err);
//				}
//			}
//		}
//		return adfrlist;
//	}
//
//}
