package com.adfundRecord.model;

import java.sql.*;

public class ADFundRecordVO implements java.io.Serializable{
	private String adfundRecordID;
	private String adfundID;
	private String memberID;
	private Integer adfundCoinUsed;
	private Timestamp adfundJoinDate;
	
	public String getAdfundRecordID() {
		return adfundRecordID;
	}
	public void setAdfundRecordID(String adfundRecordID) {
		this.adfundRecordID = adfundRecordID;
	}
	public String getAdfundID() {
		return adfundID;
	}
	public void setAdfundID(String adfundID) {
		this.adfundID = adfundID;
	}
	public String getMemberID() {
		return memberID;
	}
	public void setMemberID(String memberID) {
		this.memberID = memberID;
	}
	public Integer getAdfundCoinUsed() {
		return adfundCoinUsed;
	}
	public void setAdfundCoinUsed(Integer adfundCoinUsed) {
		this.adfundCoinUsed = adfundCoinUsed;
	}
	public Timestamp getAdfundJoinDate() {
		return adfundJoinDate;
	}
	public void setAdfundJoinDate(Timestamp adfundJoinDate) {
		this.adfundJoinDate = adfundJoinDate;
	}
	
	
}
