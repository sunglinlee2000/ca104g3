package com.adfundRecord.model;

import java.util.List;

import com.member.model.MemberVO;


public interface ADFundRecordDAO_interface {
	public void insert(ADFundRecordVO adfundRecVO);
	public ADFundRecordVO  findByPrimaryKey(String adfundRecordID);
	public List<ADFundRecordVO> getAll(String adfundID);
	public void updateCoin(ADFundRecordVO adfundRecordVO); 

}
