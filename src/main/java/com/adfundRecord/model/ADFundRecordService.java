package com.adfundRecord.model;

import java.sql.Timestamp;
import java.util.List;

import com.member.model.MemberVO;

public class ADFundRecordService {
	
	private ADFundRecordDAO_interface dao;
	
	public ADFundRecordService() {
		dao = new ADFundRecordDAO();
	}
	
	public ADFundRecordVO addADFundRecord(String adfundID, String memberID, Integer adfundCoinUsed, Timestamp adfundJoinDate) {

		ADFundRecordVO adfundRecordVO = new ADFundRecordVO();
		
		adfundRecordVO.setAdfundID(adfundID);
		adfundRecordVO.setMemberID(memberID);
		adfundRecordVO.setAdfundCoinUsed(adfundCoinUsed);
		adfundRecordVO.setAdfundJoinDate(adfundJoinDate);
        dao.insert(adfundRecordVO);	
        
        return adfundRecordVO;
	}
	
	public ADFundRecordVO getOneADFundRecord(String adfundRecordID) {
		return dao.findByPrimaryKey(adfundRecordID);
	}
	
	public List<ADFundRecordVO> getAllADFundRecord(String adfundID){
		return dao.getAll(adfundID);
	}
	
	
	public ADFundRecordVO updateADRecCoin(Integer adfundCoinUsed,String adfundRecordID) {
		ADFundRecordVO adfundRecordVO = new ADFundRecordVO();
		
		adfundRecordVO.setAdfundRecordID(adfundRecordID);
		adfundRecordVO.setAdfundCoinUsed(adfundCoinUsed);

		dao.updateCoin(adfundRecordVO);		
		return adfundRecordVO;
	}

}
