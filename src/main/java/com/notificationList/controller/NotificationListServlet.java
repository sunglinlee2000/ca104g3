package com.notificationList.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;

import com.notificationList.model.*;



@MultipartConfig(fileSizeThreshold = 1024*1024, maxFileSize = 5*1024*1024, maxRequestSize = 5*5*1024)
public class NotificationListServlet extends HttpServlet{
		
		public void doGet(HttpServletRequest req, HttpServletResponse res)
				throws ServletException, IOException {
			doPost(req, res);
		}

		public void doPost(HttpServletRequest req, HttpServletResponse res)
				throws ServletException, IOException {

			req.setCharacterEncoding("UTF-8");
			String action = req.getParameter("action");
			System.out.println("noti:" + action);
			
			if ("isClick".equals(action)) { 
				
				
				String memberID = req.getParameter("memberID");				
				System.out.println("memberID="+memberID);
				
				NotificationListService notiSvc = new NotificationListService();
				List<NotificationListVO> noList = notiSvc.getAllNoList(memberID);
				
				for(NotificationListVO noList1 : noList) {
					
					if("FALSE".equals(noList1.getNotificationclick())){
						NotificationListVO notiVO = notiSvc.updateNotiClick(noList1.getNotificationid(), "TRUE");
						System.out.println("通知改成已讀OK");
					}
					
				}

								
				JSONObject obj = new JSONObject(); 
				
				res.setContentType("text/plain"); 
				res.setCharacterEncoding("UTF-8"); 
				PrintWriter out = res.getWriter(); 
				out.write(obj.toString()); 
				out.flush(); 
				out.close();
			
			}
			
			
			if ("isRead".equals(action)) { 
				
				
				String memberID = req.getParameter("memberID");				
				System.out.println("memberID="+memberID);
				
				Boolean notiStatus = false;				
				JSONObject obj = new JSONObject();
				NotificationListService notiSvc = new NotificationListService();
				List<NotificationListVO> noList = notiSvc.getAllNoList(memberID);
				
				int falseCount = 0;
				
				for(NotificationListVO noList1 : noList) {
					
					if("FALSE".equals(noList1.getNotificationclick())){
						falseCount++;
						try { 
							obj.put("notiStatus", notiStatus);
							obj.put("falseCount", falseCount);
						} catch (JSONException e) { 
							e.printStackTrace(); 
						}  
					}
					
				}
				System.out.println("falseCount=" + falseCount);
		
				res.setContentType("text/plain"); 
				res.setCharacterEncoding("UTF-8"); 
				PrintWriter out = res.getWriter(); 
				out.write(obj.toString()); 
				out.flush(); 
				out.close();
			
			}
			
		}

}
