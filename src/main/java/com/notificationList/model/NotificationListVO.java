package com.notificationList.model;

import java.sql.Date;
import java.sql.Timestamp;

public class NotificationListVO {
	private String notificationid;
	private String notificationtypeid;
	private String memberid;
	private String notificationcontent;
	private Timestamp notificationdate;
	private String notificationtarget;
	private String notificationclick;
	private String notificationpath;
	
	public String getNotificationid() {
		return notificationid;
	}
	public void setNotificationid(String notificationid) {
		this.notificationid = notificationid;
	}
	public String getNotificationtypeid() {
		return notificationtypeid;
	}
	public void setNotificationtypeid(String notificationtypeid) {
		this.notificationtypeid = notificationtypeid;
	}
	public String getMemberid() {
		return memberid;
	}
	public void setMemberid(String memberid) {
		this.memberid = memberid;
	}
	public String getNotificationcontent() {
		return notificationcontent;
	}
	public void setNotificationcontent(String notificationcontent) {
		this.notificationcontent = notificationcontent;
	}
	public Timestamp getNotificationdate() {
		return notificationdate;
	}
	public void setNotificationdate(Timestamp notificationdate) {
		this.notificationdate = notificationdate;
	}
	public String getNotificationtarget() {
		return notificationtarget;
	}
	public void setNotificationtarget(String notificationtarget) {
		this.notificationtarget = notificationtarget;
	}
	public String getNotificationclick() {
		return notificationclick;
	}
	public void setNotificationclick(String notificationclick) {
		this.notificationclick = notificationclick;
	}
	public String getNotificationpath() {
		return notificationpath;
	}
	public void setNotificationpath(String notificationpath) {
		this.notificationpath = notificationpath;
	}

	
	
	
	
}
