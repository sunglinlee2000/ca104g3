package com.notificationList.model;

import java.util.*;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import java.sql.*;

public class NotificationListDAO implements NotificationListDAO_interface{
	
	private static DataSource ds = null;
	static {
		try {
			Context ctx = new InitialContext();
			ds = (DataSource) ctx.lookup("java:comp/env/jdbc/CA104G3DB");
		} catch (NamingException e) {
			e.printStackTrace();
		}
	}
	
	private static final String INSERT_STMT=
		"INSERT INTO NOTIFICATIONLIST (NOTIFICATIONID, NOTIFICATIONTYPEID, MEMBERID, NOTIFICATIONCONTENT, NOTIFICATIONDATE, NOTIFICATIONTARGET, NOTIFICATIONCLICK, NOTIFICATIONPATH) VALUES('N'||TO_CHAR(LPAD(NOTIFICATIONLIST_SEQ.NEXTVAL,6,'0')), ? , ? , ?, ?, ?, ?, ?)";
	private static final String DELETE_STMT=
		"DELETE FROM NOTIFICATIONLIST WHERE NOTIFICATIONID = ?";
	private static final String GET_ONE_STMT=
		"SELECT * FROM NOTIFICATIONLIST WHERE NOTIFICATIONID = ?";
	private static final String GET_ALL_STMT=
		"SELECT * FROM NOTIFICATIONLIST WHERE MEMBERID = ? ORDER BY NOTIFICATIONID DESC";
	private static final String UPDATE_NOTI_CLICK=
		"UPDATE NOTIFICATIONLIST SET NOTIFICATIONCLICK =? WHERE NOTIFICATIONID = ?";
	
	@Override
	public void insert(NotificationListVO notificationlistVO) {
		// TODO Auto-generated method stub
		Connection con = null;
		PreparedStatement pstmt = null;
		
		
		try {
			
			con = ds.getConnection();
			pstmt = con.prepareStatement(INSERT_STMT);
			
			pstmt.setString(1, notificationlistVO.getNotificationtypeid());
			pstmt.setString(2, notificationlistVO.getMemberid());
			pstmt.setString(3, notificationlistVO.getNotificationcontent());
			pstmt.setTimestamp(4, notificationlistVO.getNotificationdate());
			pstmt.setString(5, notificationlistVO.getNotificationtarget());
			pstmt.setString(6, notificationlistVO.getNotificationclick());
			pstmt.setString(7, notificationlistVO.getNotificationpath());
			pstmt.executeUpdate();
			
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		
	}
	
	
	@Override
	public void delete(String notificationlistid) {
		// TODO Auto-generated method stub
		Connection con = null;
		PreparedStatement pstmt = null;
		
		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(DELETE_STMT);

			pstmt.setString(1, notificationlistid);

			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
	}
	@Override
	public NotificationListVO findByPrimaryKey(String notificationid) {
		// TODO Auto-generated method stub
		
		NotificationListVO notificationVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ONE_STMT);

			pstmt.setString(1, notificationid);

			rs = pstmt.executeQuery();

			while (rs.next()) {
				// empVo �]�٬� Domain objects
				notificationVO = new NotificationListVO();
				notificationVO.setMemberid(rs.getString("MEMBERID"));
				notificationVO.setNotificationcontent(rs.getString("NOTIFICATIONCONTENT"));
				notificationVO.setNotificationdate(rs.getTimestamp("NOTIFICATIONDATE"));
				notificationVO.setNotificationid(rs.getString("NOTIFICATIONID"));
				notificationVO.setNotificationtypeid(rs.getString("NOTIFICATIONTYPEID"));
				notificationVO.setNotificationtarget(rs.getString("NOTIFICATIONTARGET"));
				notificationVO.setNotificationclick(rs.getString("NOTIFICATIONCLICK"));
				notificationVO.setNotificationpath(rs.getString("NOTIFICATIONPATH"));
				
			}

			// Handle any driver errors
		}  catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		
		
		
		
		return notificationVO;
	}
	@Override
	public List<NotificationListVO> getAll(String memberid) {
		// TODO Auto-generated method stub
		
		List<NotificationListVO> list = new ArrayList<NotificationListVO>();
		NotificationListVO notificationVO = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		
		
		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ALL_STMT);
			pstmt.setString(1, memberid);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				notificationVO = new NotificationListVO();
				notificationVO.setMemberid(rs.getString("memberid"));
				notificationVO.setNotificationcontent(rs.getString("notificationcontent"));
				notificationVO.setNotificationid(rs.getString("notificationid"));
				notificationVO.setNotificationtypeid(rs.getString("notificationtypeid"));
				notificationVO.setNotificationdate(rs.getTimestamp("notificationdate"));
				notificationVO.setNotificationtarget(rs.getString("notificationtarget"));
				notificationVO.setNotificationclick(rs.getString("NOTIFICATIONCLICK"));
				notificationVO.setNotificationpath(rs.getString("NOTIFICATIONPATH"));
				list.add(notificationVO); // Store the row in the list
			}

			// Handle any driver errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return list;
	}


	@Override
	public void updateNotiClick(NotificationListVO notificationlistVO) {
		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(UPDATE_NOTI_CLICK);

			pstmt.setString(1, notificationlistVO.getNotificationclick());
			pstmt.setString(2, notificationlistVO.getNotificationid());


			pstmt.executeUpdate();
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. " + se.getMessage());
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException e) {
					e.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace(System.err);
				}
			}
		}		
		
	}
	
	
	
}
