package com.notificationList.model;

import java.util.List;

public interface NotificationListDAO_interface {
	public void insert(NotificationListVO notificationlistVO);
	public void updateNotiClick(NotificationListVO notificationlistVO);
	public void delete(String notificationlistid);
	public NotificationListVO findByPrimaryKey(String notificationid);
	public List<NotificationListVO> getAll(String memberID);
	
	
	
}
