package com.notificationList.model;

import java.sql.Timestamp;
import java.util.List;

import com.adfund.model.ADFundVO;

public class NotificationListService {
	
	private NotificationListDAO_interface dao;
	
	public NotificationListService() {
		dao = new NotificationListDAO();
	}

	public NotificationListVO addNoList(String notificationtypeid, String memberid, String notificationcontent, Timestamp notificationdate, String notificationtarget, String notificationclick, String notificationpath) {
		
		NotificationListVO noVO = new NotificationListVO();
		
		noVO.setNotificationtypeid(notificationtypeid);
		noVO.setMemberid(memberid);
		noVO.setNotificationcontent(notificationcontent);
		noVO.setNotificationdate(notificationdate);
		noVO.setNotificationtarget(notificationtarget);
		noVO.setNotificationclick(notificationclick);
		noVO.setNotificationpath(notificationpath);
		dao.insert(noVO);
		
		return noVO;
	}
	
	
	public void deleteNoList(String notificationid) {
		dao.delete(notificationid);
	}
	
	public NotificationListVO getOneNoList(String notificationid) {
		return dao.findByPrimaryKey(notificationid);		
	}
	
	public List<NotificationListVO> getAllNoList(String memberid){
		return dao.getAll(memberid);
	} 
	
    public NotificationListVO updateNotiClick(String notificationid, String notificationclick) {
		
    	NotificationListVO notiVO = new NotificationListVO();
    	
    	notiVO.setNotificationid(notificationid);
    	notiVO.setNotificationclick(notificationclick);
    	
    	dao.updateNotiClick(notiVO);
		
		return notiVO;	
	}
	
}
