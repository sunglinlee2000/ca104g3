//package com.notificationList.model;
//
//import java.util.*;
//import java.sql.*;
//
//public class NotificationListJDBCDAO implements NotificationListDAO_interface{
//	String driver = "oracle.jdbc.driver.OracleDriver";
//	String url = "jdbc:oracle:thin:@localhost:1521:XE";
//	String userid = "ca104";
//	String passwd = "123456";
//	
//	private static final String INSERT_STMT=
//		"INSERT INTO NOTIFICATIONLIST (NOTIFICATIONID, NOTIFICATIONTYPEID, MEMBERID, NOTIFICATIONCONTENT, NOTIFICATIONDATE) VALUES('N'||TO_CHAR(LPAD(NOTIFICATIONLIST_SEQ.NEXTVAL,6,'0')), ? , ? , ?, ?)";
//	private static final String DELETE_STMT=
//		"DELETE FROM NOTIFICATIONLIST WHERE NOTIFICATIONID = ?";
//	private static final String GET_ONE_STMT=
//		"SELECT * FROM NOTIFICATIONLIST WHERE NOTIFICATIONID = ?";
//	private static final String GET_ALL_STMT=
//		"SELECT * FROM NOTIFICATIONLIST";
//	
//	@Override
//	public void insert(NotificationListVO notificationlistVO) {
//		// TODO Auto-generated method stub
//		Connection con = null;
//		PreparedStatement pstmt = null;
//		
//		
//		try {
//			
//			Class.forName(driver);
//			con = DriverManager.getConnection(url, userid, passwd);
//			pstmt = con.prepareStatement(INSERT_STMT);
//			
//			pstmt.setString(1, notificationlistVO.getNotificationtypeid());
//			pstmt.setString(2, notificationlistVO.getMemberid());
//			pstmt.setString(3, notificationlistVO.getNotificationcontent());
//			pstmt.setTimestamp(4, notificationlistVO.getNotificationdate());
//			pstmt.executeUpdate();
//			
//		} catch (ClassNotFoundException e) {
//			throw new RuntimeException("Couldn't load database driver. "
//					+ e.getMessage());
//			// Handle any SQL errors
//		} catch (SQLException se) {
//			throw new RuntimeException("A database error occured. "
//					+ se.getMessage());
//			// Clean up JDBC resources
//		} finally {
//			if (pstmt != null) {
//				try {
//					pstmt.close();
//				} catch (SQLException se) {
//					se.printStackTrace(System.err);
//				}
//			}
//			if (con != null) {
//				try {
//					con.close();
//				} catch (Exception e) {
//					e.printStackTrace(System.err);
//				}
//			}
//		}
//		
//	}
//	
//	
//	@Override
//	public void delete(String notificationlistid) {
//		// TODO Auto-generated method stub
//		Connection con = null;
//		PreparedStatement pstmt = null;
//		
//		try {
//
//			Class.forName(driver);
//			con = DriverManager.getConnection(url, userid, passwd);
//			pstmt = con.prepareStatement(DELETE_STMT);
//
//			pstmt.setString(1, notificationlistid);
//
//			pstmt.executeUpdate();
//
//			// Handle any driver errors
//		} catch (ClassNotFoundException e) {
//			throw new RuntimeException("Couldn't load database driver. "
//					+ e.getMessage());
//			// Handle any SQL errors
//		} catch (SQLException se) {
//			throw new RuntimeException("A database error occured. "
//					+ se.getMessage());
//			// Clean up JDBC resources
//		} finally {
//			if (pstmt != null) {
//				try {
//					pstmt.close();
//				} catch (SQLException se) {
//					se.printStackTrace(System.err);
//				}
//			}
//			if (con != null) {
//				try {
//					con.close();
//				} catch (Exception e) {
//					e.printStackTrace(System.err);
//				}
//			}
//		}
//	}
//	@Override
//	public NotificationListVO findByPrimaryKey(String notificationid) {
//		// TODO Auto-generated method stub
//		
//		NotificationListVO notificationVO = null;
//		Connection con = null;
//		PreparedStatement pstmt = null;
//		ResultSet rs = null;
//		
//		try {
//
//			Class.forName(driver);
//			con = DriverManager.getConnection(url, userid, passwd);
//			pstmt = con.prepareStatement(GET_ONE_STMT);
//
//			pstmt.setString(1, notificationid);
//
//			rs = pstmt.executeQuery();
//
//			while (rs.next()) {
//				// empVo �]�٬� Domain objects
//				notificationVO = new NotificationListVO();
//				notificationVO.setMemberid(rs.getString("MEMBERID"));
//				notificationVO.setNotificationcontent(rs.getString("NOTIFICATIONCONTENT"));
//				notificationVO.setNotificationdate(rs.getTimestamp("NOTIFICATIONDATE"));
//				notificationVO.setNotificationid(rs.getString("NOTIFICATIONID"));
//				notificationVO.setNotificationtypeid(rs.getString("NOTIFICATIONTYPEID"));
//				
//			}
//
//			// Handle any driver errors
//		} catch (ClassNotFoundException e) {
//			throw new RuntimeException("Couldn't load database driver. "
//					+ e.getMessage());
//			// Handle any SQL errors
//		} catch (SQLException se) {
//			throw new RuntimeException("A database error occured. "
//					+ se.getMessage());
//			// Clean up JDBC resources
//		} finally {
//			if (rs != null) {
//				try {
//					rs.close();
//				} catch (SQLException se) {
//					se.printStackTrace(System.err);
//				}
//			}
//			if (pstmt != null) {
//				try {
//					pstmt.close();
//				} catch (SQLException se) {
//					se.printStackTrace(System.err);
//				}
//			}
//			if (con != null) {
//				try {
//					con.close();
//				} catch (Exception e) {
//					e.printStackTrace(System.err);
//				}
//			}
//		}
//		
//		
//		
//		
//		return notificationVO;
//	}
//	@Override
//	public List<NotificationListVO> getAll() {
//		// TODO Auto-generated method stub
//		
//		List<NotificationListVO> list = new ArrayList<NotificationListVO>();
//		NotificationListVO notificationVO = null;
//
//		Connection con = null;
//		PreparedStatement pstmt = null;
//		ResultSet rs = null;
//		
//		
//		
//		try {
//
//			Class.forName(driver);
//			con = DriverManager.getConnection(url, userid, passwd);
//			pstmt = con.prepareStatement(GET_ALL_STMT);
//			rs = pstmt.executeQuery();
//
//			while (rs.next()) {
//				notificationVO = new NotificationListVO();
//				notificationVO.setMemberid(rs.getString("memberid"));
//				notificationVO.setNotificationcontent(rs.getString("notificationcontent"));
//				notificationVO.setNotificationid(rs.getString("notificationid"));
//				notificationVO.setNotificationtypeid(rs.getString("Notificationtypeid"));
//				notificationVO.setNotificationdate(rs.getTimestamp("notificationdate"));
//				
//				list.add(notificationVO); // Store the row in the list
//			}
//
//			// Handle any driver errors
//		} catch (ClassNotFoundException e) {
//			throw new RuntimeException("Couldn't load database driver. "
//					+ e.getMessage());
//			// Handle any SQL errors
//		} catch (SQLException se) {
//			throw new RuntimeException("A database error occured. "
//					+ se.getMessage());
//		} finally {
//			if (rs != null) {
//				try {
//					rs.close();
//				} catch (SQLException se) {
//					se.printStackTrace(System.err);
//				}
//			}
//			if (pstmt != null) {
//				try {
//					pstmt.close();
//				} catch (SQLException se) {
//					se.printStackTrace(System.err);
//				}
//			}
//			if (con != null) {
//				try {
//					con.close();
//				} catch (Exception e) {
//					e.printStackTrace(System.err);
//				}
//			}
//		}
//		return list;
//	}
//	
//	
//	
//}
