package com.SNS.model;

import java.sql.Timestamp;
import java.util.List;

public class SNSService {

	private SNSDAO_interface dao;
	
	public SNSService() {
		dao = new SNSDAO();
	}
	
	public SNSVO addSNS(SNSVO snsvo) {
		SNSVO snsVO = new SNSVO();
		snsVO.setArtistID(snsvo.getArtistID());
		snsVO.setSnsPhotoURL(snsvo.getSnsPhotoURL());
		snsVO.setSnsContent(snsvo.getSnsContent());
		snsVO.setTime(snsvo.getTime());
		dao.insert(snsVO);
		
		return snsVO;
	}
	
	public SNSVO getOneSNS(String snsID) {
		return dao.findByPrimaryKey(snsID);
	}
	
	public SNSVO checkSNS(String artistID,Timestamp snsDate) {
		return dao.checkSNS(artistID, snsDate);
	}
	
	public List<SNSVO> getAll(){
		return dao.getAll();
	}
	
	public List<SNSVO> getOne(String artistID){
		return dao.getOne(artistID);
	}
	
	
	
	
}
