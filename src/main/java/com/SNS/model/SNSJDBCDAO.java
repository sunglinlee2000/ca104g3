package com.SNS.model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;

public class SNSJDBCDAO implements SNSDAO_interface{
	String driver = "oracle.jdbc.driver.OracleDriver";
	String url = "jdbc:oracle:thin:@localhost:1521:XE";
	String userid = "CA104G3";
	String passwd = "123456";
	
	private static final String INSERT_STMT = "INSERT INTO SNS VALUES"
			+ "('SNS'||LPAD(TO_CHAR(SNS_SEQ.NEXTVAL),6,0),?,?,?,?)";
	private static final String GET_ONE_STMT = "SELECT * FROM SNS WHERE SnsID=?";
	private static final String GET_ALL_STMT = "SELECT * FROM SNS ORDER BY SnsID";
	
	
	

	@Override
	public void insert(SNSVO snsVO) {
		Connection con = null;
		PreparedStatement pstmt = null;
		
		try {
			
			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(INSERT_STMT);
			

			pstmt.setString(1, snsVO.getArtistID());
			pstmt.setString(2, snsVO.getSnsContent());
			pstmt.setTimestamp(3, snsVO.getSnsDate());
			pstmt.setBytes(4, snsVO.getSnsPhoto());
	
			
			pstmt.executeUpdate();			
			
			
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver." + e.getMessage());
		
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured." + se.getMessage());
		
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if(con != null) {
				try {
					con.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
		}
	}


	@Override
	public SNSVO findByPrimaryKey(String snsID) {
		SNSVO snsVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		try {
			
			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(GET_ONE_STMT);
			
			pstmt.setString(1, snsID);
			
			rs = pstmt.executeQuery();
			
			while (rs.next()) {
				snsVO = new SNSVO();
				snsVO.setSnsID(rs.getString("snsID"));
				snsVO.setArtistID(rs.getString("artistID"));
				snsVO.setSnsContent(rs.getString("snsContent"));
				snsVO.setSnsDate(rs.getTimestamp("snsDate"));
				snsVO.setSnsPhoto(rs.getBytes("snsPhoto"));
			}
			
			
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver."+ e.getMessage());
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured." + se.getMessage());
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			} 
			if (con != null) {
				try {
					con.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
		}
		return snsVO;
	}

	@Override
	public List<SNSVO> getAll() {
		List<SNSVO> list = new ArrayList<SNSVO>();
		SNSVO snsVO = null;
		
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		try {
			
			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(GET_ALL_STMT);
			rs = pstmt.executeQuery();
			
			while (rs.next()) {
				snsVO = new SNSVO();
				snsVO.setSnsID(rs.getString("snsID"));
				snsVO.setArtistID(rs.getString("artistID"));
				snsVO.setSnsContent(rs.getString("snsContent"));
				snsVO.setSnsDate(rs.getTimestamp("snsDate"));
				snsVO.setSnsPhoto(rs.getBytes("snsPhoto"));
				list.add(snsVO);
			}
			
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver."+ e.getMessage());
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured." + se.getMessage());
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			} 
			if (con != null) {
				try {
					con.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
		}
		return list;
	}


	@Override
	public List<SNSVO> getOne(String artistID) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public SNSVO checkSNS(String artistID, Timestamp snsDate) {
		// TODO Auto-generated method stub
		return null;
	}

}
