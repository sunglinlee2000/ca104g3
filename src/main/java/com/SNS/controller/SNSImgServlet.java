package com.SNS.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.SNS.model.SNSService;
import com.SNS.model.SNSVO;

public class SNSImgServlet extends HttpServlet{
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		req.setCharacterEncoding("UTF-8");
		String snsID = req.getParameter("snsID");
		SNSService snsSvc = new SNSService();
		SNSVO snsVO = snsSvc.getOneSNS(snsID);
		
		byte[] photo = null;
		ServletOutputStream out = res.getOutputStream();
		photo = snsVO.getSnsPhoto();
		
		res.setContentType("image/jpeg");
		res.setContentLength(photo.length);
		out.write(photo);
		out.flush();
		out.close();
		
		
	}
}
