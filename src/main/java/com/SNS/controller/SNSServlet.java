package com.SNS.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.artist.model.ArtistService;
import com.artist.model.ArtistVO;

public class SNSServlet extends HttpServlet{
	private static final long serialVersionUID = 1L;

	public void doGet(HttpServletRequest req,HttpServletResponse res) 
			throws ServletException,IOException{
		doPost(req,res);
	}
	public void doPost(HttpServletRequest req,HttpServletResponse res) 
				throws ServletException,IOException {
		req.setCharacterEncoding("UTF-8");
		res.setContentType("text/html; charset=UTF-8");
		String action = req.getParameter("action");

		
	if("showOne".equals(action)) {
		
		try {
			/***************************1.接收請求參數 - 輸入格式的錯誤處理**********************/
			String str = req.getParameter("boardID");
			
			String boardID = null;
			try {
				boardID = new String(str);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			/***************************2.開始查詢資料*****************************************/
			ArtistService artistSvc = new ArtistService();
			List<ArtistVO> list =  artistSvc.getArtistByBoard(boardID);
			ArtistVO artistVO = list.get(0);
			
			/***************************3.查詢完成,準備轉交(Send the Success view)*************/
			req.setAttribute("artistVO", artistVO); // 資料庫取出的empVO物件,存入req
			req.setAttribute("boardID", boardID);
			String url = "/front-end/SNS/listOneSNS.jsp";
			RequestDispatcher successView = req.getRequestDispatcher(url); // 成功轉交 listOneEmp.jsp
			successView.forward(req, res);

			/***************************其他可能的錯誤處理*************************************/
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
			
		
	
	}
}
