package com.SNS.controller;

import java.net.*;
import java.io.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Test {

	public static void main(String[] args) throws Exception {
		
		File myDir = new File("myDir");
		if (!myDir.exists()) myDir.mkdir();
		File myFile = new File(myDir, "myFile.txt");

		try {
			String httpsURL = "https://www.instagram.com/twicetagram/?hl=zh-tw"; //臺灣觀光年曆
			// 以URL物件建立URLConnection物件
			URLConnection urlConn = HttpsUtil.getURLConnection(httpsURL);
			// 以下模擬瀏覽器的user-agent請求標頭(Servlet講義p79-範例HeaderSnoop.java ; 或講義p185-範例EL10.jsp)
			urlConn.setRequestProperty("user-agent", "Mozilla/5.0 (Windows NT 6.1; WOW64; Trident/7.0; rv:11.0) like Gecko");
			// 以URLConnection物件取得輸入資料流
			InputStream ins = urlConn.getInputStream();
			// 建立URL資料流
			BufferedReader br = new BufferedReader(new InputStreamReader(ins, "utf-8"));
			StringBuilder sb = new StringBuilder();
			String data;
			while ((data = br.readLine()) != null) {
				sb.append(data + "\r\n");
			}

			String myResultString = sb.toString();
			PrintWriter pr = new PrintWriter(new BufferedWriter(new FileWriter(myFile)));
			pr.println(myResultString);
			System.out.println(myResultString);
			pr.close();

			Pattern myPattern = Pattern.compile("http[s]?:[a-zA-Z0-9\\./_=?-]*.jpg");
			Matcher myMatcher = myPattern.matcher(myResultString);
			int count = 0;
			while (myMatcher.find()) {
				String myToken = myResultString.substring(myMatcher.start(), myMatcher.end());
				System.out.println("myTokenURL=" + myToken);
				URL myTokenURL = new URL(myToken);
				try {
					InputStream in = myTokenURL.openConnection().getInputStream();
					count++;
					OutputStream out = new FileOutputStream(new File(myDir, count + ".jpg"));
					byte buffer[] = new byte[4096];
					int bytes_read;
					while ((bytes_read = in.read(buffer)) != -1) {
						out.write(buffer, 0, bytes_read);
					}
					in.close();
					out.close();
				} catch (Exception ignored) { }
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
