package com.SNS.controller;
import java.util.Random;

import org.json.*;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

public class TestIG2 {

	@SuppressWarnings("deprecation")
	public static void main(String args[]) throws InterruptedException{ 
		
		WebDriver driver = null;
        long waitLoadBaseTime = 500;
        int waitLoadRandomTime = 500;
        Random random = new Random(System.currentTimeMillis());
		
		System.setProperty("webdriver.chrome.driver","Webcontent/WEB-INF/lib/chromedriver.exe");
		
		driver = new ChromeDriver(DesiredCapabilities.chrome());
		JavascriptExecutor js = ((JavascriptExecutor) driver);
		driver.get("https://www.instagram.com/twicetagram/?hl=zh-tw");
		
		String string = driver.getTitle();
		System.out.println(string);

        Thread.sleep(waitLoadBaseTime+random.nextInt(waitLoadRandomTime));

        int pages=3;
        for(int i=0; i<pages; i++) {
            driver.manage().window().maximize();
            js.executeScript("window.scrollTo(0, document.body.scrollHeight)");

            Thread.sleep(waitLoadBaseTime+random.nextInt(waitLoadRandomTime));
        }
		

		JSONObject j;
		String tmp = null;

//		try {
			String data = driver.getPageSource();
//			System.out.println(data);
				if (data.contains("<script type=\"text/javascript\">window._sharedData")) {
					tmp = data.substring(data.indexOf("<script type=\"text/javascript\">window._sharedData")+51 , data.indexOf(";</script>"));
//					System.out.println(data.substring(data.indexOf("<script type=\"text/javascript\">window._sharedData")+51 , data.indexOf(";</script>")));
				}

 
			try {
				j = new JSONObject(tmp);
		
			for(int i = 0 ; i <= 11 ; i++) {
			Object photoURL = j.getJSONObject("entry_data").getJSONArray("ProfilePage").getJSONObject(0).getJSONObject("graphql").getJSONObject("user").getJSONObject("edge_owner_to_timeline_media").getJSONArray("edges").getJSONObject(i).getJSONObject("node").get("display_url");
			Object content = j.getJSONObject("entry_data").getJSONArray("ProfilePage").getJSONObject(0).getJSONObject("graphql").getJSONObject("user").getJSONObject("edge_owner_to_timeline_media").getJSONArray("edges").getJSONObject(i).getJSONObject("node").getJSONObject("edge_media_to_caption").getJSONArray("edges").getJSONObject(0).getJSONObject("node").get("text");
			Object time = j.getJSONObject("entry_data").getJSONArray("ProfilePage").getJSONObject(0).getJSONObject("graphql").getJSONObject("user").getJSONObject("edge_owner_to_timeline_media").getJSONArray("edges").getJSONObject(i).getJSONObject("node").get("taken_at_timestamp");
			System.out.println(i);
			System.out.println(photoURL);
			System.out.println(content);
			System.out.println(time);
			System.out.println("=================================");
			}
			} catch (JSONException e) {
				e.printStackTrace();
			}
	}		
		 
}
