package com.member.model;
import java.util.List;

public interface MemberDAO_interface {
		public void insert(MemberVO memberVO);
		public void update(MemberVO memberVO);
		public void updateCoin(MemberVO memberVO);
		public void updateWithoutPhoto(MemberVO memberVO);
		public void updateStatus(MemberVO memberVO);
		public MemberVO getCoin(String memberID);		
		public MemberVO findByPrimaryKey(String memberID);
		public MemberVO findByTPID(String thirdPartyID);
		public List<MemberVO> getAll();
}
