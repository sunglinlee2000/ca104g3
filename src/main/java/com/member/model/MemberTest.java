package com.member.model;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;

public class MemberTest {

	public static void main(String[] args) {
		MemberJDBCDAO memberJDBCDAO = new MemberJDBCDAO();
//		1.------------------------
		MemberVO memberVO = new MemberVO();
//		memberVO.setMemberEmail("aaa@gmail.com");
//		memberVO.setThirdPartyID("123456789");
//		memberVO.setMemberName("會員A");
//		memberVO.setMemberNickName("DAVID");
//		memberVO.setMemberSex("男");
//		memberVO.setMemberBirthday(java.sql.Timestamp.valueOf("1990-01-01 10:10:10"));
//		byte[] pic = null;
//		try {
//			pic = getPictureByteArray("WebContent/front-end/res/img/DAVID.png");
//		}catch (IOException e) {
//			e.printStackTrace(System.err);
//		}
//		memberVO.setMemberPhoto(pic);
//		memberVO.setMemberPhone("0912-345-678");
//		memberVO.setMemberBankAccount("700-12345-678987");
//		memberVO.setMemberAddress("桃園中壢");
//		memberVO.setMemberStatus("MEMBER_NORMAL");
//		memberJDBCDAO.insert(memberVO);
//		
//		System.out.println("OK");
//		
//		2.------------------------
//		MemberVO memberVO2 = new MemberVO();
//		memberVO2.setMemberID("M000011");
//		memberVO2.setMemberEmail("aaaaaaaa@gmail.com");
//		memberVO2.setThirdPartyID((long) 123456789);
//		memberVO2.setMemberName("會員A");
//		memberVO2.setMemberNickName("DAVID");
//		memberVO2.setMemberSex("男");
//		memberVO2.setMemberBirthday(java.sql.Timestamp.valueOf("1990-01-01 10:10:10"));
//		byte[] pic2 = null;
//		try {
//			pic2 = getPictureByteArray("WebContent/front-end/res/img/DAVID.png");
//		}catch (IOException e) {
//			e.printStackTrace(System.err);
//		}
//		memberVO2.setMemberPhoto(pic2);
//		memberVO2.setMemberCoin(0);
//		memberVO2.setMemberPhone("0912-345-678");
//		memberVO2.setMemberBankAccount("700-12345-678987");
//		memberVO2.setMemberAddress("桃園中壢");
//		memberVO2.setMemberStatus("MEMBER_BUYER");
//		memberJDBCDAO.update(memberVO2);
//		
//		System.out.println("OK");
//
//		3.------------------------
//		MemberVO memberVO3 = memberJDBCDAO.findByPrimaryKey("M000011");
//		System.out.println(memberVO3.getMemberID());
//		System.out.println(memberVO3.getMemberEmail());
//		System.out.println(memberVO3.getThirdPartyID());
//		System.out.println(memberVO3.getMemberName());
//		System.out.println(memberVO3.getMemberNickName());
//		System.out.println(memberVO3.getMemberSex());
//		System.out.println(memberVO3.getMemberBirthday());
//		System.out.println(memberVO3.getMemberPhoto());
//		System.out.println(memberVO3.getMemberCoin());
//		System.out.println(memberVO3.getMemberPhone());
//		System.out.println(memberVO3.getMemberBankAccount());
//		System.out.println(memberVO3.getMemberAddress());
//		System.out.println(memberVO3.getMemberJoinDate());
//		System.out.println(memberVO3.getMemberStatus());
//		System.out.println("==================================");
//		System.out.println("OK");

//		MemberVO memberVO4 = memberJDBCDAO.getCoin("M000005");
//		System.out.println(memberVO4.getMemberID());
//		System.out.println(memberVO4.getMemberCoin());
//		System.out.println("OK");
		
//		4.------------------------
//		List<MemberVO> list = memberJDBCDAO.getAll();
//		for (MemberVO memberVO4 : list) {
//			System.out.println(memberVO4.getMemberID());
//			System.out.println(memberVO4.getMemberEmail());
//			System.out.println(memberVO4.getThirdPartyID());
//			System.out.println(memberVO4.getMemberName());
//			System.out.println(memberVO4.getMemberNickName());
//			System.out.println(memberVO4.getMemberSex());
//			System.out.println(memberVO4.getMemberBirthday());
//			System.out.println(memberVO4.getMemberPhoto());
//			System.out.println(memberVO4.getMemberCoin());
//			System.out.println(memberVO4.getMemberPhone());
//			System.out.println(memberVO4.getMemberBankAccount());
//			System.out.println(memberVO4.getMemberAddress());
//			System.out.println(memberVO4.getMemberStatus());
//			System.out.println("==================================");
//		}
//		System.out.println("OK");
		
		for (int i = 1 ; i <= 10 ; i++) {
			byte[] pic1 = null;

			memberVO.setMemberID(("M00000"+i));
			try {
				pic1  = getPictureByteArray("WebContent/front-end/res/img/member/"+i+".jpg");
			} catch(IOException e ) {
				e.printStackTrace(System.err);
			}
			memberVO.setMemberPhoto(pic1);
			memberJDBCDAO.update(memberVO);
			System.out.println("OK");
	}
	}

	public static byte[] getPictureByteArray(String path) throws IOException {
		File file = new File(path);
		FileInputStream fis = new FileInputStream(file);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		byte[] buffer = new byte[8192];
		int i;
		while ((i = fis.read(buffer)) != -1) {
			baos.write(buffer, 0, i);
		}
		baos.close();
		fis.close();

		return baos.toByteArray();
	}
}
