package com.member.model;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.*;
import java.util.*;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

public class MemberDAO implements MemberDAO_interface{

	private static DataSource ds = null;
	static {
		try {
			Context ctx = new InitialContext();
			ds = (DataSource) ctx.lookup("java:comp/env/jdbc/CA104G3DB");
		} catch (NamingException e) {
			e.printStackTrace();
		}
	}
	
	private static final String INSERT_STMT =
			"INSERT INTO memberlist (memberID,memberEmail,thirdPartyID,memberName,"
			+ "memberNickName,memberSex,memberBirthday,memberPhoto,memberCoin,"
			+ "memberPhone,memberBankAccount,memberAddress,memberJoinDate,"
			+ "memberStatus) VALUES ('M'||LPAD(TO_CHAR(mem_seq.NEXTVAL),6,0),?,?,\r\n" 
			+ "?,?,?,?,?,5000,?,?,?,(CURRENT_TIMESTAMP),'MEMBER_NORMAL')";
		private static final String GET_ALL_STMT =
			"SELECT * FROM memberlist ORDER BY memberID";
		private static final String GET_ONE_STMT =
			"SELECT * FROM memberlist WHERE memberID = ?";
		private static final String UPDATE =
			"UPDATE memberlist set memberEmail=?, memberName=?,"
			+ "memberNickName=?, memberSex=?, memberBirthday=?, memberPhoto=?, "
			+ "memberPhone=?, memberBankAccount=?, memberAddress=? WHERE memberID=?";
		private static final String UPDATE_WITHOUT_PHOTO =
				"UPDATE memberlist set memberEmail=?, memberName=?,"
				+ "memberNickName=?, memberSex=?, memberBirthday=?,"
				+ "memberPhone=?, memberBankAccount=?, memberAddress=? WHERE memberID=?";
		private static final String GET_COIN = "SELECT memberID,memberCoin FROM memberlist WHERE memberID = ? ";
		private static final String CHECK_MEM = "SELECT memberID,thirdPartyID FROM memberlist WHERE thirdPartyID = ?"; 
		private static final String UPDATE_COIN = "UPDATE memberlist set memberCoin=? WHERE memberID = ? ";
		private static final String UPDATTE_STATUS = "UPDATE memberlist SET memberStatus=? Where memberID=?";
	 
		
		@Override
		public void insert(MemberVO memberVO) {
			Connection con = null;
			PreparedStatement pstmt = null;
			byte[] photo = null;
			try {
				URL url = new URL(memberVO.getMemberPhotoURL());
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				InputStream is = null;
				
				try {
					is= url.openStream();
					byte[] byteChunk = new byte[8192];
					int n ;
					while((n = is.read(byteChunk))>0) {
						baos.write(byteChunk,0,n);
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
				
				photo = baos.toByteArray();
			} catch (MalformedURLException e1) {
				e1.printStackTrace();
			}

			try {
				con = ds.getConnection();
				pstmt = con.prepareStatement(INSERT_STMT);
				
				Blob blob = null;
				try {
					blob = con.createBlob();
					blob.setBytes(1, photo);
				} catch (SQLException e) {
					e.printStackTrace();
				}
				
				pstmt.setString(1, memberVO.getMemberEmail());
				pstmt.setString(2, memberVO.getThirdPartyID());
				pstmt.setString(3, memberVO.getMemberName());
				pstmt.setString(4, memberVO.getMemberNickName());
				pstmt.setString(5, memberVO.getMemberSex());
				pstmt.setTimestamp(6, memberVO.getMemberBirthday());
				pstmt.setBlob(7, blob);
				pstmt.setString(8, memberVO.getMemberPhone());
				pstmt.setString(9, memberVO.getMemberBankAccount());
				pstmt.setString(10, memberVO.getMemberAddress());
				
				pstmt.executeUpdate();			
				
			} catch (SQLException se) {
				throw new RuntimeException("A database error occured." + se.getMessage());
			} finally {
				if (pstmt != null) {
					try {
						pstmt.close();
					} catch (SQLException se) {
						se.printStackTrace(System.err);
					}
				}
				if(con != null) {
					try {
						con.close();
					} catch (SQLException se) {
						se.printStackTrace(System.err);
					}
				}
			}
		}

		@Override
		public void update(MemberVO memberVO) {
			
			Connection con = null;
			PreparedStatement pstmt = null;
			
			try {
				
				con =ds.getConnection();
				pstmt = con.prepareStatement(UPDATE);
				
				
				pstmt.setString(1, memberVO.getMemberEmail());
				pstmt.setString(2, memberVO.getMemberName());
				pstmt.setString(3, memberVO.getMemberNickName());
				pstmt.setString(4, memberVO.getMemberSex());
				pstmt.setTimestamp(5, memberVO.getMemberBirthday());
				pstmt.setBytes(6, memberVO.getMemberPhoto());
				pstmt.setString(7, memberVO.getMemberPhone());
				pstmt.setString(8, memberVO.getMemberBankAccount());
				pstmt.setString(9, memberVO.getMemberAddress());
				pstmt.setString(10, memberVO.getMemberID());
				
				pstmt.executeUpdate();	
				
				
			} catch (SQLException se) {
				throw new RuntimeException("A database error occured." + se.getMessage());
			} finally {
				if (pstmt != null) {
					try {
						pstmt.close();
					} catch (SQLException se) {
						se.printStackTrace(System.err);
					}
				}
				if (con != null) {
					try {
						con.close();
					} catch (Exception e) {
						e.printStackTrace(System.err);
					}
				}
			}
		}


		@Override
		public MemberVO findByPrimaryKey(String memberID) {
			
			MemberVO memberVO = null;
			Connection con = null;
			PreparedStatement pstmt = null;
			ResultSet rs = null;
			
			try {
				
				con = ds.getConnection();
				pstmt = con.prepareStatement(GET_ONE_STMT);
				
				pstmt.setString(1, memberID);
				
				rs = pstmt.executeQuery();
				
				while (rs.next()) {
					memberVO = new MemberVO();
					memberVO.setMemberID(rs.getString("memberID"));
					memberVO.setMemberEmail(rs.getString("memberEmail"));
					memberVO.setThirdPartyID(rs.getString("thirdPartyID"));
					memberVO.setMemberName(rs.getString("memberName"));
					memberVO.setMemberNickName(rs.getString("memberNickName"));
					memberVO.setMemberSex(rs.getString("memberSex"));
					memberVO.setMemberBirthday(rs.getTimestamp("memberBirthday"));
					memberVO.setMemberPhoto(rs.getBytes("memberPhoto"));
					memberVO.setMemberCoin(rs.getInt("memberCoin"));
					memberVO.setMemberPhone(rs.getString("memberPhone"));
					memberVO.setMemberBankAccount(rs.getString("memberBankAccount"));
					memberVO.setMemberAddress(rs.getString("memberAddress"));
					memberVO.setMemberJoinDate(rs.getTimestamp("memberJoinDate"));
					String status = rs.getString("memberStatus");
					switch(status) {
						case "MEMBER_NORMAL" :
							status = "一般會員";
							break;
						case "MEMBER_BUYER" :
							status = "買家";
							break;
						case "MEMBER_SELLER" :
							status = "賣家";
							break;
						case "MEMBER_LOCKED" :
							status = "帳號凍結";
							break;							
					}
					memberVO.setMemberStatus(status);
				}
				
				
			} catch (SQLException se) {
				throw new RuntimeException("A database error occured." + se.getMessage());
			} finally {
				if (pstmt != null) {
					try {
						pstmt.close();
					} catch (SQLException se) {
						se.printStackTrace(System.err);
					}
				} 
				if (con != null) {
					try {
						con.close();
					} catch (SQLException se) {
						se.printStackTrace(System.err);
					}
				}
			}
			
			return memberVO;
		}

		@Override
		public List<MemberVO> getAll() {
			List<MemberVO> list = new ArrayList<MemberVO>();
			MemberVO memberVO = null;
			
			Connection con = null;
			PreparedStatement pstmt = null;
			ResultSet rs = null;
			
			try {
				
				con = ds.getConnection();
				pstmt = con.prepareStatement(GET_ALL_STMT);
				rs = pstmt.executeQuery();
				
				while (rs.next()) {
					memberVO = new MemberVO();
					memberVO.setMemberID(rs.getString("memberID"));
					memberVO.setMemberEmail(rs.getString("memberEmail"));
					memberVO.setThirdPartyID(rs.getString("thirdPartyID"));
					memberVO.setMemberName(rs.getString("memberName"));
					memberVO.setMemberNickName(rs.getString("memberNickName"));
					memberVO.setMemberSex(rs.getString("memberSex"));
					memberVO.setMemberBirthday(rs.getTimestamp("memberBirthday"));
					memberVO.setMemberPhoto(rs.getBytes("memberPhoto"));
					memberVO.setMemberCoin(rs.getInt("memberCoin"));
					memberVO.setMemberPhone(rs.getString("memberPhone"));
					memberVO.setMemberBankAccount(rs.getString("memberBankAccount"));
					memberVO.setMemberAddress(rs.getString("memberAddress"));
					memberVO.setMemberJoinDate(rs.getTimestamp("memberJoinDate"));
					memberVO.setMemberStatus(rs.getString("memberStatus"));
					list.add(memberVO);
				}
				
			} catch (SQLException se) {
				throw new RuntimeException("A database error occured." + se.getMessage());
			} finally {
				if (pstmt != null) {
					try {
						pstmt.close();
					} catch (SQLException se) {
						se.printStackTrace(System.err);
					}
				} 
				if (con != null) {
					try {
						con.close();
					} catch (SQLException se) {
						se.printStackTrace(System.err);
					}
				}
			}
			
			return list;
		}
		
		@Override
		public MemberVO getCoin(String memberID) {
			MemberVO memberVO = null;
			Connection con = null;
			PreparedStatement pstmt = null;
			ResultSet rs = null;
			
			try {
				
				con = ds.getConnection();
				pstmt = con.prepareStatement(GET_COIN);
				
				pstmt.setString(1, memberID);
				
				rs = pstmt.executeQuery();
				
				while (rs.next()) {
					memberVO = new MemberVO();
					memberVO.setMemberID(rs.getString("memberID"));
					memberVO.setMemberCoin(rs.getInt("memberCoin"));

					
				}
				
				
			} catch (SQLException se) {
				throw new RuntimeException("A database error occured." + se.getMessage());
			} finally {
				if (pstmt != null) {
					try {
						pstmt.close();
					} catch (SQLException se) {
						se.printStackTrace(System.err);
					}
				} 
				if (con != null) {
					try {
						con.close();
					} catch (SQLException se) {
						se.printStackTrace(System.err);
					}
				}
			}
			
			return memberVO;
		}

		@Override
		public void updateCoin(MemberVO memberVO) {
			Connection con = null;
			PreparedStatement pstmt = null;
			
			try {
				
				con =ds.getConnection();
				pstmt = con.prepareStatement(UPDATE_COIN);
				
				
				pstmt.setInt(1, memberVO.getMemberCoin());
				pstmt.setString(2, memberVO.getMemberID());
				
				pstmt.executeUpdate();	
				
				
			} catch (SQLException se) {
				throw new RuntimeException("A database error occured." + se.getMessage());
			} finally {
				if (pstmt != null) {
					try {
						pstmt.close();
					} catch (SQLException se) {
						se.printStackTrace(System.err);
					}
				}
				if (con != null) {
					try {
						con.close();
					} catch (Exception e) {
						e.printStackTrace(System.err);
					}
				}
			}
		}

		@Override
		public MemberVO findByTPID(String thirdPartyID) {
			MemberVO memberVO = null;
			Connection con = null;
			PreparedStatement pstmt = null;
			ResultSet rs = null;
			
			try {
				
				con = ds.getConnection();
				pstmt = con.prepareStatement(CHECK_MEM);
				
				pstmt.setString(1, thirdPartyID);
				
				rs = pstmt.executeQuery();
				
				while (rs.next()) {
					memberVO = new MemberVO();
					memberVO.setMemberID(rs.getString("memberID"));
					memberVO.setThirdPartyID(rs.getString("thirdPartyID"));
					
				}
				
				
			} catch (SQLException se) {
				throw new RuntimeException("A database error occured." + se.getMessage());
			} finally {
				if (pstmt != null) {
					try {
						pstmt.close();
					} catch (SQLException se) {
						se.printStackTrace(System.err);
					}
				} 
				if (con != null) {
					try {
						con.close();
					} catch (SQLException se) {
						se.printStackTrace(System.err);
					}
				}
			}
			
			return memberVO;
		}
		
		@Override
		public void updateStatus(MemberVO memberVO) {
			Connection con = null;
			PreparedStatement pstmt = null;
			
			try {
				
				con =ds.getConnection();
				pstmt = con.prepareStatement(UPDATTE_STATUS);
				
				
				pstmt.setString(1, memberVO.getMemberStatus());
				pstmt.setString(2, memberVO.getMemberID());
				
				pstmt.executeUpdate();	
				
				
			} catch (SQLException se) {
				throw new RuntimeException("A database error occured." + se.getMessage());
			} finally {
				if (pstmt != null) {
					try {
						pstmt.close();
					} catch (SQLException se) {
						se.printStackTrace(System.err);
					}
				}
				if (con != null) {
					try {
						con.close();
					} catch (Exception e) {
						e.printStackTrace(System.err);
					}
				}
			}			
		}
		
		public static byte[] getPictureByteArray(String path) throws IOException {
			File file = new File(path);
			FileInputStream fis = new FileInputStream(file);
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			byte[] buffer = new byte[8192];
			int i;
			while ((i = fis.read(buffer)) != -1) {
				baos.write(buffer, 0, i);
			}
			baos.close();
			fis.close();

			return baos.toByteArray();
		}

		@Override
		public void updateWithoutPhoto(MemberVO memberVO) {
			Connection con = null;
			PreparedStatement pstmt = null;
			
			try {
				
				con =ds.getConnection();
				pstmt = con.prepareStatement(UPDATE_WITHOUT_PHOTO);
				
				
				pstmt.setString(1, memberVO.getMemberEmail());
				pstmt.setString(2, memberVO.getMemberName());
				pstmt.setString(3, memberVO.getMemberNickName());
				pstmt.setString(4, memberVO.getMemberSex());
				pstmt.setTimestamp(5, memberVO.getMemberBirthday());
				pstmt.setString(6, memberVO.getMemberPhone());
				pstmt.setString(7, memberVO.getMemberBankAccount());
				pstmt.setString(8, memberVO.getMemberAddress());
				pstmt.setString(9, memberVO.getMemberID());
				
				pstmt.executeUpdate();	
				
				
			} catch (SQLException se) {
				throw new RuntimeException("A database error occured." + se.getMessage());
			} finally {
				if (pstmt != null) {
					try {
						pstmt.close();
					} catch (SQLException se) {
						se.printStackTrace(System.err);
					}
				}
				if (con != null) {
					try {
						con.close();
					} catch (Exception e) {
						e.printStackTrace(System.err);
					}
				}
			}
			
		}


}
