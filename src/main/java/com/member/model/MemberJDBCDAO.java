package com.member.model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class MemberJDBCDAO implements MemberDAO_interface{
	String driver = "oracle.jdbc.driver.OracleDriver";
	String url = "jdbc:oracle:thin:@localhost:1521:XE";
	String userid = "CA104G3";
	String passwd = "123456";
	
	private static final String INSERT_STMT =
		"INSERT INTO memberlist (memberID,memberEmail,thirdPartyID,memberName,"
		+ "memberNickName,memberSex,memberBirthday,memberPhoto,memberCoin,"
		+ "memberPhone,memberBankAccount,memberAddress,memberJoinDate,"
		+ "memberStatus) VALUES ('M'||LPAD(TO_CHAR(mem_seq.NEXTVAL),6,0),?,?,\r\n" 
		+ "?,?,?,?,?,0,?,?,?,(CURRENT_TIMESTAMP),?)";
	private static final String GET_ALL_STMT =
		"SELECT * FROM memberlist ORDER BY memberID";
	private static final String GET_ONE_STMT =
		"SELECT * FROM memberlist WHERE memberID = ?";
//	private static final String UPDATE =
//			"UPDATE memberlist set memberEmail=?, memberName=?,"
//			+ "memberNickName=?, memberSex=?, memberBirthday=?, memberPhoto=?, "
//			+ "memberPhone=?, memberBankAccount=?, memberAddress=? WHERE memberID=?";
	private static final String UPDATE = "UPDATE memberlist SET memberPhoto=? WHERE memberID=?";
	private static final String GET_COIN = "SELECT memberID,memberCoin FROM memberlist WHERE memberID = ? ";
	private static final String CHECK_MEM = "SELECT memberID,thirdPartyID FROM memberlist WHERE thirdPartyID = ?"; 
	private static final String UPDATE_COIN = "UPDATE memberlist set memberCoin=? WHERE memberID = ? ";
	
	

	@Override
	public void insert(MemberVO memberVO) {
		
		Connection con = null;
		PreparedStatement pstmt = null;
		
		try {
			
			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(INSERT_STMT);
			
			pstmt.setString(1, memberVO.getMemberEmail());
			pstmt.setString(2, memberVO.getThirdPartyID());
			pstmt.setString(3, memberVO.getMemberName());
			pstmt.setString(4, memberVO.getMemberNickName());
			pstmt.setString(5, memberVO.getMemberSex());
			pstmt.setTimestamp(6, memberVO.getMemberBirthday());
			pstmt.setBytes(7, memberVO.getMemberPhoto());
			pstmt.setString(8, memberVO.getMemberPhone());
			pstmt.setString(9, memberVO.getMemberBankAccount());
			pstmt.setString(10, memberVO.getMemberAddress());
			pstmt.setString(11, memberVO.getMemberStatus());
			
			pstmt.executeUpdate();			
			
			
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver." + e.getMessage());
		
		} catch (SQLException se) {
			se.printStackTrace(System.err);
			throw new RuntimeException("A database error occured." + se.getMessage());
			
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if(con != null) {
				try {
					con.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
		}
	}

	@Override
	public void update(MemberVO memberVO) {
		
		Connection con = null;
		PreparedStatement pstmt = null;
		
		try {
			
			Class.forName(driver);
			con =DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(UPDATE);
			
//			pstmt.setString(1, memberVO.getMemberEmail());
//			pstmt.setString(2, memberVO.getMemberName());
//			pstmt.setString(3, memberVO.getMemberNickName());
//			pstmt.setString(4, memberVO.getMemberSex());
//			pstmt.setTimestamp(5, memberVO.getMemberBirthday());
//			pstmt.setBytes(6, memberVO.getMemberPhoto());
//			pstmt.setString(7, memberVO.getMemberPhone());
//			pstmt.setString(8, memberVO.getMemberBankAccount());
//			pstmt.setString(9, memberVO.getMemberAddress());
//			pstmt.setString(10, memberVO.getMemberID());
			
			
			pstmt.setBytes(1, memberVO.getMemberPhoto());
			pstmt.setString(2, memberVO.getMemberID());
			
			
			pstmt.executeUpdate();	
			
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver." + e.getMessage());
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured." + se.getMessage());
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
	}


	@Override
	public MemberVO findByPrimaryKey(String memberID) {
		
		MemberVO memberVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		try {
			
			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(GET_ONE_STMT);
			
			pstmt.setString(1, memberID);
			
			rs = pstmt.executeQuery();
			
			while (rs.next()) {
				memberVO = new MemberVO();
				memberVO.setMemberID(rs.getString("memberID"));
				memberVO.setMemberEmail(rs.getString("memberEmail"));
				memberVO.setThirdPartyID(rs.getString("thirdPartyID"));
				memberVO.setMemberName(rs.getString("memberName"));
				memberVO.setMemberNickName(rs.getString("memberNickName"));
				memberVO.setMemberSex(rs.getString("memberSex"));
				memberVO.setMemberBirthday(rs.getTimestamp("memberBirthday"));
				memberVO.setMemberPhoto(rs.getBytes("memberPhoto"));
				memberVO.setMemberCoin(rs.getInt("memberCoin"));
				memberVO.setMemberPhone(rs.getString("memberPhone"));
				memberVO.setMemberBankAccount(rs.getString("memberBankAccount"));
				memberVO.setMemberAddress(rs.getString("memberAddress"));
				memberVO.setMemberJoinDate(rs.getTimestamp("memberJoinDate"));
				memberVO.setMemberStatus(rs.getString("memberStatus"));
				
			}
			
			
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver."+ e.getMessage());
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured." + se.getMessage());
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			} 
			if (con != null) {
				try {
					con.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
		}
		
		return memberVO;
	}

	@Override
	public List<MemberVO> getAll() {
		List<MemberVO> list = new ArrayList<MemberVO>();
		MemberVO memberVO = null;
		
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		try {
			
			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(GET_ALL_STMT);
			rs = pstmt.executeQuery();
			
			while (rs.next()) {
				memberVO = new MemberVO();
				memberVO.setMemberID(rs.getString("memberID"));
				memberVO.setMemberEmail(rs.getString("memberEmail"));
				memberVO.setThirdPartyID(rs.getString("thirdPartyID"));
				memberVO.setMemberName(rs.getString("memberName"));
				memberVO.setMemberNickName(rs.getString("memberNickName"));
				memberVO.setMemberSex(rs.getString("memberSex"));
				memberVO.setMemberBirthday(rs.getTimestamp("memberBirthday"));
				memberVO.setMemberPhoto(rs.getBytes("memberPhoto"));
				memberVO.setMemberCoin(rs.getInt("memberCoin"));
				memberVO.setMemberPhone(rs.getString("memberPhone"));
				memberVO.setMemberBankAccount(rs.getString("memberBankAccount"));
				memberVO.setMemberAddress(rs.getString("memberAddress"));
				memberVO.setMemberJoinDate(rs.getTimestamp("memberJoinDate"));
				memberVO.setMemberStatus(rs.getString("memberStatus"));
				list.add(memberVO);
			}
			
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver."+ e.getMessage());
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured." + se.getMessage());
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			} 
			if (con != null) {
				try {
					con.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
		}
		
		return list;
	}

	@Override
	public MemberVO getCoin(String memberID) {
		MemberVO memberVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		try {
			
			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(GET_COIN);
			
			pstmt.setString(1, memberID);
			
			rs = pstmt.executeQuery();
			
			while (rs.next()) {
				memberVO = new MemberVO();
				memberVO.setMemberID(rs.getString("memberID"));
				memberVO.setMemberCoin(rs.getInt("memberCoin"));
				
			}
			
			
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver."+ e.getMessage());
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured." + se.getMessage());
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			} 
			if (con != null) {
				try {
					con.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
		}
		
		return memberVO;
	}

	@Override
	public MemberVO findByTPID(String thirdPartyID) {
		MemberVO memberVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		try {
			
			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(CHECK_MEM);
			
			pstmt.setString(1, thirdPartyID);
			
			rs = pstmt.executeQuery();
			
			while (rs.next()) {
				memberVO = new MemberVO();
				memberVO.setMemberID(rs.getString("memberID"));
				memberVO.setThirdPartyID(rs.getString("thirdPartyID"));
				
			}
			
			
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver."+ e.getMessage());
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured." + se.getMessage());
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			} 
			if (con != null) {
				try {
					con.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
		}
		
		return memberVO;
	}

	@Override
	public void updateCoin(MemberVO memberVO) {
		Connection con = null;
		PreparedStatement pstmt = null;
		
		try {
			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(UPDATE_COIN);
			
			
			pstmt.setInt(1, memberVO.getMemberCoin());
			pstmt.setString(2, memberVO.getMemberID());
			
			pstmt.executeUpdate();	
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver."+ e.getMessage());			
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured." + se.getMessage());
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
	}

	@Override
	public void updateStatus(MemberVO memberVO) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updateWithoutPhoto(MemberVO memberVO) {
		// TODO Auto-generated method stub
		
	}

}
