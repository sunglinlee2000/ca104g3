package com.member.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.member.model.*;

public class MemberImgServlet extends HttpServlet{
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		req.setCharacterEncoding("UTF-8");
		String memberID = req.getParameter("memberID");
		MemberService memberSvc = new MemberService();
		MemberVO memberVO = memberSvc.getOneMember(memberID);
		
		byte[] photo = null;
		ServletOutputStream out = res.getOutputStream();
		photo = memberVO.getMemberPhoto();
		
		res.setContentType("image/jpeg");
		res.setContentLength(photo.length);
		out.write(photo);
		out.flush();
		out.close();
		
		
	}
}
