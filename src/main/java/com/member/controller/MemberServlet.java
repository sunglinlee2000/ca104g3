package com.member.controller;

import java.io.*;
import java.util.*;

import javax.servlet.*;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.*;

import com.member.model.*;

@MultipartConfig(fileSizeThreshold=1024*1024, maxFileSize=10*1024*1024, maxRequestSize=5*10*1024*1024)
public class MemberServlet extends HttpServlet{
	private static final long serialVersionUID = 1L;

	public void doGet(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		doPost(req, res);
	}

	public void doPost(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {

		req.setCharacterEncoding("UTF-8");
		String action = req.getParameter("action");
		
		
		if ("getOne_For_Display".equals(action)) { // 來自select_page.jsp的請求

			List<String> errorMsgs = new LinkedList<String>();
			req.setAttribute("errorMsgs", errorMsgs);

			try {
				/***************************1.接收請求參數 - 輸入格式的錯誤處理**********************/
				String str = req.getParameter("memberID");
				if (str == null || (str.trim()).length() == 0) {
					errorMsgs.add("請輸入會員編號");
				}
				// Send the use back to the form, if there were errors
				if (!errorMsgs.isEmpty()) {
					RequestDispatcher failureView = req
							.getRequestDispatcher("/front-end/member/select_page.jsp");
					failureView.forward(req, res);
					return;//程式中斷
				}
				
				String memberID = null;
				try {
					memberID = new String(str);
				} catch (Exception e) {
					errorMsgs.add("會員編號格式不正確");
				}
				// Send the use back to the form, if there were errors
				if (!errorMsgs.isEmpty()) {
					RequestDispatcher failureView = req
							.getRequestDispatcher("/front-end/member/select_page.jsp");
					failureView.forward(req, res);
					return;//程式中斷
				}
				
				/***************************2.開始查詢資料*****************************************/
				MemberService memSvc = new MemberService();
				MemberVO memberVO = memSvc.getOneMember(memberID);
				if (memberVO == null) {
					errorMsgs.add("查無資料");
				}
				// Send the use back to the form, if there were errors
				if (!errorMsgs.isEmpty()) {
					RequestDispatcher failureView = req
							.getRequestDispatcher("/front-end/member/select_page.jsp");
					failureView.forward(req, res);
					return;//程式中斷
				}
				
				/***************************3.查詢完成,準備轉交(Send the Success view)*************/
				req.setAttribute("memberVO", memberVO); // 資料庫取出的empVO物件,存入req
				String url = "/front-end/member/listOneMember.jsp";
				RequestDispatcher successView = req.getRequestDispatcher(url); // 成功轉交 listOneEmp.jsp
				successView.forward(req, res);

				/***************************其他可能的錯誤處理*************************************/
			} catch (Exception e) {
				errorMsgs.add("無法取得資料:" + e.getMessage());
				RequestDispatcher failureView = req
						.getRequestDispatcher("/front-end/member/select_page.jsp");
				failureView.forward(req, res);
			}
		
		}
		
		if ("getOne_For_Update".equals(action)) { // 來自listAllEmp.jsp的請求

			List<String> errorMsgs = new LinkedList<String>();
			req.setAttribute("errorMsgs", errorMsgs);
			
			try {
				/***************************1.接收請求參數****************************************/
				String memberID = new String(req.getParameter("memberID"));
				
				/***************************2.開始查詢資料****************************************/
				MemberService memberSvc = new MemberService();
				MemberVO memberVO = memberSvc.getOneMember(memberID);
								
				/***************************3.查詢完成,準備轉交(Send the Success view)************/
				req.setAttribute("memberVO", memberVO);         // 資料庫取出的empVO物件,存入req
				String url = "/front-end/member/update_member_input.jsp";
				RequestDispatcher successView = req.getRequestDispatcher(url);// 成功轉交 update_emp_input.jsp
				successView.forward(req, res);

				/***************************其他可能的錯誤處理**********************************/
			} catch (Exception e) {
				errorMsgs.add("無法取得要修改的資料:" + e.getMessage());
				RequestDispatcher failureView = req
						.getRequestDispatcher("/front-end/member/listAllMember.jsp");
				failureView.forward(req, res);
			}
		}
		
		
if ("update".equals(action)) {
			
	Map<String,String> errorMsgs = new LinkedHashMap<String,String>();
	req.setAttribute("errorMsgs", errorMsgs);
		
			try {
				/***************************1.接收請求參數 - 輸入格式的錯誤處理**********************/
				String memberID=req.getParameter("memberID");
								
				String memberEmail = req.getParameter("memberEmail");
				if(memberEmail == null || memberEmail.trim().length()== 0) {
					errorMsgs.put("memberEmail","內容請勿空白");
				}
				
				
				String memberName = req.getParameter("memberName");
				if(memberName == null || memberName.trim().length()== 0) {
					errorMsgs.put("memberName","內容請勿空白");
				}
				
				String memberNickName = req.getParameter("memberNickName");
				if(memberNickName == null || memberNickName.trim().length()== 0) {
					errorMsgs.put("memberNickName","內容請勿空白");
				}
				
				String memberSex = req.getParameter("memberSex");
				if(memberSex == null || memberSex.trim().length()== 0) {
					errorMsgs.put("memberSex","內容請勿空白");
				}
				
				
				java.sql.Timestamp memberBirthday = null;
				try {
					memberBirthday = java.sql.Timestamp.valueOf(req.getParameter("memberBirthday").trim());
				} catch (IllegalArgumentException e) {
					memberBirthday=new java.sql.Timestamp(System.currentTimeMillis());
					errorMsgs.put("memberBirthday","請輸入日期!");
				}
								
				Part part = req.getPart("memberPhoto");
				InputStream is = part.getInputStream();
				byte[] memberPhoto = new byte[is.available()];
				is.read(memberPhoto);
				is.close();
				int p = memberPhoto.length;
				
				String memberPhone = req.getParameter("memberPhone");				
				
				String memberBankAccount = req.getParameter("memberBankAccount");				
				
				StringBuffer address = new StringBuffer();
				String county = req.getParameter("county");
				if (county == null || county.trim().length() == 0) {
				} else {
					address.append(county);
				}
				
				String district = req.getParameter("district");
				if (district == null || district.trim().length() == 0) {
				} else {
					address.append(district);
				}		
				String memberAddress = req.getParameter("memberAddress");
				if (memberAddress == null || memberAddress.trim().length() == 0) {
				} else {
					address.append(memberAddress);
				}
				
				String memberaddress = new String(address);
				
				if(county.trim().length() == 0 || district.trim().length() == 0 || memberAddress.trim().length() == 0) {
					MemberService memberSvc = new MemberService();
					MemberVO memberVO = memberSvc.getOneMember(memberID);
					memberaddress = new String(memberVO.getMemberAddress());
					System.out.println("地址="+memberaddress);
				}

				


				MemberVO memberVO = new MemberVO();
				memberVO.setMemberID(memberID);
				memberVO.setMemberEmail(memberEmail);
				memberVO.setMemberName(memberName);
				memberVO.setMemberNickName(memberNickName);
				memberVO.setMemberSex(memberSex);
				memberVO.setMemberBirthday(memberBirthday);
				memberVO.setMemberPhone(memberPhone);
				memberVO.setMemberBankAccount(memberBankAccount);
				memberVO.setMemberAddress(memberaddress);



				if (!errorMsgs.isEmpty()) {
					req.setAttribute("memberVO", memberVO); // 含有輸入格式錯誤的empVO物件,也存入req
					RequestDispatcher failureView = req
							.getRequestDispatcher("/front-end/member/update_member_input.jsp");
					failureView.forward(req, res);
					return; //程式中斷
				}
				
				/***************************2.開始修改資料*****************************************/
				MemberService memberSvc = new MemberService();
				
				if(p==0) {
					memberVO = memberSvc.updateWithoutPhotoMember(memberID, memberEmail, memberName, memberNickName, memberSex, memberBirthday, memberPhone, memberBankAccount, memberaddress);
				}else {
					memberVO.setMemberPhoto(memberPhoto);
					memberVO = memberSvc.updateMember(memberID,memberEmail,memberName, memberNickName, memberSex, memberBirthday, memberPhoto, memberPhone, memberBankAccount, memberaddress);
				}
				/***************************3.修改完成,準備轉交(Send the Success view)*************/
				HttpSession session = req.getSession();
				MemberVO memberVO1 = memberSvc.getOneMember(memberID);
				session.setAttribute("memberVO", memberVO1);
				
				
//				req.setAttribute("memberVO", memberVO); // 資料庫update成功後,正確的的empVO物件,存入req
				String url = "/front-end/member/listOneMember.jsp";
				RequestDispatcher successView = req.getRequestDispatcher(url); // 修改成功後,轉交listOneEmp.jsp
				successView.forward(req, res);
				/***************************其他可能的錯誤處理*************************************/
			} catch (Exception e) {
				errorMsgs.put("Exception","修改資料失敗:"+e.getMessage());
				RequestDispatcher failureView = req
						.getRequestDispatcher("/front-end/member/update_member_input.jsp");
				failureView.forward(req, res);
			}
		}

if ("insert".equals(action)) { 
	
	Map<String,String> errorMsgs = new LinkedHashMap<String,String>();
	req.setAttribute("errorMsgs", errorMsgs);
	MemberVO memberVO = new MemberVO();
	try {
		/***********************1.接收請求參數 - 輸入格式的錯誤處理*************************/
		String memberEmail = req.getParameter("memberEmail");
		if(memberEmail == null || memberEmail.trim().length()== 0) {
			errorMsgs.put("memberEmail","內容請勿空白");
		}
		
		String thirdPartyID =req.getParameter("thirdPartyID");
		
		String memberName = req.getParameter("memberName");
		if(memberName == null || memberName.trim().length()== 0) {
			errorMsgs.put("memberName","內容請勿空白");
		}
		
		String memberNickName = req.getParameter("memberNickName");
		if(memberNickName == null || memberNickName.trim().length()== 0) {
			errorMsgs.put("memberNickName","內容請勿空白");
		}
		
		String memberSex = req.getParameter("memberSex");
		
		
		java.sql.Timestamp memberBirthday = null;
		try {
			memberBirthday = java.sql.Timestamp.valueOf(req.getParameter("memberBirthday").trim());
		} catch (IllegalArgumentException e) {
			memberBirthday=new java.sql.Timestamp(System.currentTimeMillis());
			errorMsgs.put("memberBirthday","請輸入日期!");
		}
		
		String memberPhotoURL = req.getParameter("memberPhotoURL");

		
		String memberPhone = req.getParameter("memberPhone");				
		if (memberPhone == null || memberPhone.trim().length() == 0) {
			errorMsgs.put("memberPhone","會員手機: 請勿空白");
		}

		String memberBankAccount = req.getParameter("memberBankAccount");				
		if (memberBankAccount == null || memberBankAccount.trim().length() == 0) {
			errorMsgs.put("memberBankAccount","會員銀行帳號: 請勿空白");
		}

		StringBuffer address = new StringBuffer();

		String county = req.getParameter("county");
		if (county == null || county.trim().length() == 0) {
			errorMsgs.put("county","內容請勿空白");
		} else {
			address.append(county);
		}
		
		String district = req.getParameter("district");
		if (district == null || district.trim().length() == 0) {
			errorMsgs.put("district","內容請勿空白");
		} else {
			address.append(district);
		}		
		String memberAddress = req.getParameter("memberAddress");
		if (memberAddress == null || memberAddress.trim().length() == 0) {
			errorMsgs.put("memberAddress","內容請勿空白");
		} else {
			address.append(memberAddress);
		}
		
		String memberaddress = new String(address);
		System.out.println(memberaddress);
		
		memberVO.setMemberEmail(memberEmail);
		memberVO.setThirdPartyID(thirdPartyID);
		memberVO.setMemberName(memberName);
		memberVO.setMemberNickName(memberNickName);
		memberVO.setMemberSex(memberSex);
		memberVO.setMemberBirthday(memberBirthday);
		memberVO.setMemberPhotoURL(memberPhotoURL);
		memberVO.setMemberPhone(memberPhone);
		memberVO.setMemberBankAccount(memberBankAccount);
		memberVO.setMemberAddress(memberaddress);
		
		// Send the use back to the form, if there were errors
		if (!errorMsgs.isEmpty()) {
			req.setAttribute("memberVO", memberVO); // 含有輸入格式錯誤的empVO物件,也存入req
			RequestDispatcher failureView = req
					.getRequestDispatcher("/front-end/member/addMember.jsp");
			failureView.forward(req, res);
			return;
		}
		
		/***************************2.開始新增資料***************************************/
		MemberService memberSvc = new MemberService();
		memberVO = memberSvc.addMember(memberEmail, thirdPartyID, memberName, memberNickName, memberSex, memberBirthday,memberPhotoURL,   memberPhone, memberBankAccount,  memberaddress);
		/***************************3.新增完成,準備轉交(Send the Success view)***********/
		MemberVO memberVO1 = memberSvc.findMem(thirdPartyID);
		MemberVO memberVO2 = memberSvc.getOneMember(memberVO1.getMemberID());
		HttpSession session = req.getSession();
		session.setAttribute("memberVO", memberVO2);
		System.out.println(memberVO2.getMemberID());
		
       try {                                                        
           String location = (String) session.getAttribute("location");
           if (location != null) {
             session.removeAttribute("location");   //*工作2: 看看有無來源網頁 (-->如有來源網頁:則重導至來源網頁)
             res.sendRedirect(location);            
             return;
           }
         }catch (Exception ignored) { }		
		
		String url = "/front-end/member/listOneMember.jsp";
		RequestDispatcher successView = req.getRequestDispatcher(url); // 新增成功後轉交listAllEmp.jsp
		successView.forward(req, res);				
		/***************************其他可能的錯誤處理**********************************/
		} catch (Exception e) {
			e.printStackTrace(System.err);
			errorMsgs.put("exception",e.getMessage());
			RequestDispatcher failureView = req
					.getRequestDispatcher("/front-end/member/addMember.jsp");
			failureView.forward(req, res);
		}
	
}
	
if ("getCoin".equals(action)) { // 來自select_page.jsp的請求

	List<String> errorMsgs = new LinkedList<String>();
	req.setAttribute("errorMsgs", errorMsgs);

	try {
		/***************************1.接收請求參數 - 輸入格式的錯誤處理**********************/
		String str = req.getParameter("memberID");
		if (str == null || (str.trim()).length() == 0) {
			errorMsgs.add("請輸入會員編號");
		}
		// Send the use back to the form, if there were errors
		if (!errorMsgs.isEmpty()) {
			RequestDispatcher failureView = req
					.getRequestDispatcher("/front-end/member/select_page.jsp");
			failureView.forward(req, res);
			return;//程式中斷
		}
		
		String memberID = null;
		try {
			memberID = new String(str);
		} catch (Exception e) {
			errorMsgs.add("會員編號格式不正確");
		}
		// Send the use back to the form, if there were errors
		if (!errorMsgs.isEmpty()) {
			RequestDispatcher failureView = req
					.getRequestDispatcher("/front-end/member/select_page.jsp");
			failureView.forward(req, res);
			return;//程式中斷
		}
		
		/***************************2.開始查詢資料*****************************************/
		MemberService memSvc = new MemberService();
		MemberVO memberVO = memSvc.getCoin(memberID);
		if (memberVO == null) {
			errorMsgs.add("查無資料");
		}
		// Send the use back to the form, if there were errors
		if (!errorMsgs.isEmpty()) {
			RequestDispatcher failureView = req
					.getRequestDispatcher("/front-end/member/select_page.jsp");
			failureView.forward(req, res);
			return;//程式中斷
		}
		
		/***************************3.查詢完成,準備轉交(Send the Success view)*************/
		req.setAttribute("memberVO", memberVO); // 資料庫取出的empVO物件,存入req
		String url = "/front-end/member/listOneMember.jsp";
		RequestDispatcher successView = req.getRequestDispatcher(url); // 成功轉交 listOneEmp.jsp
		successView.forward(req, res);

		/***************************其他可能的錯誤處理*************************************/
	} catch (Exception e) {
		errorMsgs.add("無法取得資料:" + e.getMessage());
		RequestDispatcher failureView = req
				.getRequestDispatcher("/front-end/member/select_page.jsp");
		failureView.forward(req, res);
	}

}


if ("check_Member".equals(action)) { // 來自select_page.jsp的請求
	List<String> errorMsgs = new LinkedList<String>();
	req.setAttribute("errorMsgs", errorMsgs);

	try {
		/***************************1.接收請求參數 - 輸入格式的錯誤處理**********************/
		System.out.println(req.getParameter("thirdPartyID"));
		System.out.println(req.getParameter("memberName"));
		System.out.println(req.getParameter("memberEmail"));
		System.out.println(req.getParameter("memberPhotoURL"));
		String thirdPartyID = req.getParameter("thirdPartyID");
		String memberEmail = req.getParameter("memberEmail");
		String memberName = req.getParameter("memberName");
		

			
		MemberVO memberVO = new MemberVO();
		memberVO.setMemberEmail(memberEmail);
		memberVO.setThirdPartyID(thirdPartyID);
		memberVO.setMemberName(memberName);
		memberVO.setMemberPhotoURL(req.getParameter("memberPhotoURL"));
		/***************************2.開始查詢資料*****************************************/
		MemberService memSvc = new MemberService();
		MemberVO memberVO1 = memSvc.findMem(thirdPartyID);
		
		if (memberVO1 == null) {
			req.setAttribute("memberVO", memberVO);
			RequestDispatcher failureView = req
			.getRequestDispatcher("/front-end/member/addMember.jsp");
			failureView.forward(req, res);
			return;//程式中斷
			
		} else {
			MemberVO memberVO2 = memSvc.getOneMember(memberVO1.getMemberID());
			HttpSession session = req.getSession();
			session.setAttribute("memberVO", memberVO2);
			System.out.println(memberVO1.getMemberID());
			
	       try {                                                        
	           String location = (String) session.getAttribute("location");
	           if (location != null) {
	             session.removeAttribute("location");   //*工作2: 看看有無來源網頁 (-->如有來源網頁:則重導至來源網頁)
	             res.sendRedirect(location);            
	             return;
	           }
	         }catch (Exception ignored) { }			
	       
			res.sendRedirect(req.getContextPath()+"/front-end/index.jsp");
		}
		
		
		} catch(Exception e) {
			e.printStackTrace();
		}
}
		

	if ("logOut".equals(action)) { 

		HttpSession session = req.getSession();
		if(session != null) {
			session.removeAttribute("memberVO");
			res.sendRedirect(req.getContextPath()+"/front-end/login.jsp");
			return;
		}
	}
	
	if ("checkPhone" .equals(action)) {
		
		String memberPhone = req.getParameter("memberPhone");
		System.out.println(memberPhone);
		
		int num = (int)(Math.random()*999998+1);
		System.out.println(num);
		
		HttpSession sessionPhone = req.getSession();
		sessionPhone.setAttribute("memberPhone", memberPhone);
		sessionPhone.setAttribute("checkNum", num);
		sessionPhone.setMaxInactiveInterval(300);
		
	 	Send se = new Send();

	 	String[] tel ={memberPhone};
	 	String message = "本訊息由FanPool系統自動發送，請勿回覆，您的驗證碼為:"+String.valueOf(num)+"，請在五分鐘內填寫，超過時間驗證碼將失效，謝謝您的申請!";
//		String message = "This message is send by FanPool's system , please do not reply , below is your Check Number:"+String.valueOf(num)+",Please insert your check number in 5 minutes,Thank you!";
	 	se.sendMessage(tel , message);
	 	
	 	

		req.setAttribute("numPhone", num);
		RequestDispatcher failureView = req
		.getRequestDispatcher("/front-end/member/listOneMember.jsp");
		failureView.forward(req, res);
		return;

	}
	
	if ("confirmNum" .equals(action)) {
		
		String checkNumInput = req.getParameter("checkNum");
		System.out.println(checkNumInput);
		
		
		HttpSession sessionPhone = req.getSession();
		int checkNumSession = (Integer)sessionPhone.getAttribute("checkNum");
		String checkNum = String.valueOf(checkNumSession);
		System.out.println(checkNum);
		
		if(checkNumInput.equals(checkNum)) {
			try {
				/***************************1.接收請求參數 - 輸入格式的錯誤處理**********************/
				String memberID=req.getParameter("memberID");
				String memberStatus="MEMBER_BUYER";

				MemberVO memberVOUpdateStatus = new MemberVO();
				memberVOUpdateStatus.setMemberID(memberID);
				memberVOUpdateStatus.setMemberStatus(memberStatus);


				
				/***************************2.開始修改資料*****************************************/
				MemberService memberSvc = new MemberService();
				memberVOUpdateStatus = memberSvc.updateMemberStatus(memberID,memberStatus);
				
				HttpSession session = req.getSession();
				MemberVO memberVO = memberSvc.getOneMember(memberID);
				session.setAttribute("memberVO", memberVO);
				
				
				/***************************3.修改完成,準備轉交(Send the Success view)*************/
				Boolean fail = false;
				req.setAttribute("Phonefail", fail);
				RequestDispatcher failureView = req
				.getRequestDispatcher("/front-end/member/listOneMember.jsp");
				failureView.forward(req, res);
				return;//程式中斷
				} catch (Exception e) {
					e.printStackTrace();
				}
			} else {
				Boolean fail = true;
				req.setAttribute("Phonefail", fail);
				RequestDispatcher failureView = req
				.getRequestDispatcher("/front-end/member/listOneMember.jsp");
				failureView.forward(req, res);
				return;//程式中斷
//				res.sendRedirect(req.getContextPath()+"/front-end/member/listOneMember.jsp");
			}
		}	
	

	if ("checkAccount" .equals(action)) {
		String memberID = req.getParameter("memberID");
		System.out.println(memberID);
		String memberEmail = req.getParameter("memberEmail");
		System.out.println(memberEmail);
		String memberBankAccount = req.getParameter("memberBankAccount");
		System.out.println(memberBankAccount);
		
		int num = (int)(Math.random()*999998+1);
		System.out.println(num);
		
		HttpSession sessionBankAccount = req.getSession();
		sessionBankAccount.setAttribute("memberBankAccount", memberBankAccount);
		sessionBankAccount.setAttribute("checkNum", num);
		sessionBankAccount.setMaxInactiveInterval(300);
		
		HttpSession session = req.getSession();
		MemberVO memberVO = (MemberVO)session.getAttribute("memberVO");
		String memberName = memberVO.getMemberName();
		
		String messageText="親愛的"+memberName+"會員您好，感謝您申請FanPool賣家驗證，以下為您的銀行帳號，"+memberBankAccount+"，如確認無誤，請於五分鐘內將下方驗證碼填入網站，若過五分鐘，驗證碼將會失效，謝謝您的配合!! 您的驗證碼為:"+num;
		MailService mailService = new MailService();
		mailService.sendMail(memberEmail, "FanPool銀行帳號驗證", messageText);

	 	
	 	

		req.setAttribute("num", num);
		RequestDispatcher failureView = req
		.getRequestDispatcher("/front-end/member/listOneMember.jsp");
		failureView.forward(req, res);
		return;//程式中斷
//		res.sendRedirect(req.getContextPath()+"/front-end/member/listOneMember.jsp");

	}
	
	if ("confirmNumBank" .equals(action)) {
		
		String checkNumInput = req.getParameter("checkNum");
		System.out.println(checkNumInput);
		
		
		HttpSession sessionBankAccount = req.getSession();
		int checkNumSession = (Integer)sessionBankAccount.getAttribute("checkNum");
		String checkNum = String.valueOf(checkNumSession);
		System.out.println(checkNum);
		
		if(checkNumInput.equals(checkNum)) {
			try {
				/***************************1.接收請求參數 - 輸入格式的錯誤處理**********************/
				String memberID=req.getParameter("memberID");
				String memberStatus="MEMBER_SELLER";
				MemberVO memberVOUpdateStatus = new MemberVO();
				memberVOUpdateStatus.setMemberID(memberID);
				memberVOUpdateStatus.setMemberStatus(memberStatus);

				/***************************2.開始修改資料*****************************************/
				MemberService memberSvc = new MemberService();
				memberVOUpdateStatus = memberSvc.updateMemberStatus(memberID,memberStatus);
				
				HttpSession session = req.getSession();
				MemberVO memberVO = memberSvc.getOneMember(memberID);
				session.setAttribute("memberVO", memberVO);
				
				/***************************3.修改完成,準備轉交(Send the Success view)*************/
				Boolean fail = false;
				req.setAttribute("Bankfail", fail);
				RequestDispatcher failureView = req
				.getRequestDispatcher("/front-end/member/listOneMember.jsp");
				failureView.forward(req, res);
				return;//程式中斷
				} catch (Exception e) {
					e.printStackTrace();
				}
			} else {
				Boolean fail = true;
				req.setAttribute("Bankfail", fail);
				RequestDispatcher failureView = req
				.getRequestDispatcher("/front-end/member/listOneMember.jsp");
				failureView.forward(req, res);
				return;//程式中斷
//				res.sendRedirect(req.getContextPath()+"/front-end/member/listOneMember.jsp");
			}
		}		
	}	


	
	public static InputStream getPictureStream(String path) throws IOException {
		File file = new File(path);
		FileInputStream fis = new FileInputStream(file);
		return fis;
	}
	
	public void sendMessage(String[] tel , String message)  {

		  try {
		      String server  = "203.66.172.131"; //Socket to Air Gateway IP
		      int port	     = 8000;            //Socket to Air Gateway Port

		      String user    = "85559671"; //帳號
		      String passwd  = "2irioiai"; //密碼
		      String messageBig5 = new String(message.getBytes(),"Big5"); //簡訊內容

		      //----建立連線 and 檢查帳號密碼是否錯誤
		      sock2air mysms = new sock2air();
		      int ret_code = mysms.create_conn(server,port,user,passwd) ;
		      if( ret_code == 0 ) {
		           System.out.println("帳號密碼Login OK!");
		      } else {
		      	   System.out.println("帳號密碼Login Fail!");
		           System.out.println("ret_code="+ret_code + ",ret_content=" + mysms.get_message());
		           //結束連線
		           mysms.close_conn();
		           return ;
		      }

		      //傳送文字簡訊
		      //如需同時傳送多筆簡訊，請多次呼叫send_message()即可。
		    for(int i=0 ; i<tel.length ; i++){  
		      ret_code=mysms.send_message(tel[i],messageBig5);
		      if( ret_code == 0 ) {
		           System.out.println("簡訊已送到簡訊中心!");
		           System.out.println("MessageID="+mysms.get_message()); //取得MessageID
		      } else {
		           System.out.println("簡訊傳送發生錯誤!");
		           System.out.print("ret_code="+ret_code+",");
		           System.out.println("ret_content="+mysms.get_message());//取得錯誤的訊息
		           //結束連線
		           mysms.close_conn();
		           return ;
		      }
		    }

		      //結束連線
		      mysms.close_conn();

		  	}catch (Exception e)  {

		      System.out.println("I/O Exception : " + e);
		  }
	}
}//end of class}

	

