package com.groupMember.controller;

import java.io.*;
import java.util.*;
import java.sql.*;

import javax.servlet.*;
import javax.servlet.http.*;

import com.groupList.model.*;
import com.groupMember.model.*;

public class GroupMemberServlet extends HttpServlet {

	public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		doPost(req, res);
	}

	public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {

		req.setCharacterEncoding("UTF-8");
		String action = req.getParameter("action");
		res.setHeader("Cache-Control", "no-store");
		res.setHeader("Pragma", "no-cache");
		res.setDateHeader("Expires", 0);

		if ("groupMember_Application".equals(action)) {

			List<String> errorMsgs = new LinkedList<String>();
			req.setAttribute("errorMsgs", errorMsgs);

			try {
//				1.錯誤處理
				String groupID = req.getParameter("groupID");
				String memberID = req.getParameter("memberID");
				
				GroupMemberService groupMemberSvc = new GroupMemberService();
				GroupMemberVO groupMemberVO = groupMemberSvc.getOneGroupMember(groupID, memberID);
				if (groupMemberVO != null) {
					errorMsgs.add("您已經是本揪團的成員了唷！");
				}
				
				if (!errorMsgs.isEmpty()) {
					GroupListService groupListSvc = new GroupListService();
					GroupListVO groupListVO = groupListSvc.getOneGroup(groupID);
					req.setAttribute("groupListVO", groupListVO);
					RequestDispatcher failureView = req.getRequestDispatcher("/front-end/groupList/listOneGroup.jsp");
					failureView.forward(req, res);
					return;
				}

				/*************************** 2.開始新增資料 ***************************************/
				
				groupMemberVO = groupMemberSvc.addGroupMember(groupID, memberID);

				/*************************** 3.新增完成,準備轉交(Send the Success view) ***********/
				GroupListService groupListSvc = new GroupListService();
				GroupListVO groupListVO = groupListSvc.getOneGroup(groupID);
				req.setAttribute("groupListVO", groupListVO);
				String url = "/front-end/groupList/listOneGroup.jsp";
				RequestDispatcher successView = req.getRequestDispatcher(url);
				successView.forward(req, res);

				/*************************** 其他可能的錯誤處理 **********************************/
			} catch (Exception e) {
				errorMsgs.add(e.getMessage());
				RequestDispatcher failureView = req.getRequestDispatcher("/front-end/groupList/listOneGroup.jsp");
				failureView.forward(req, res);
			}
		}
	}

}
