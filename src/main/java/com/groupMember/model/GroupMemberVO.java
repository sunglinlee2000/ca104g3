package com.groupMember.model;

public class GroupMemberVO implements java.io.Serializable {
	private String groupID;
	private String memberID;
	private Integer groupScore;
	private Integer groupCoinGained;

	public String getGroupID() {
		return groupID;
	}

	public void setGroupID(String groupID) {
		this.groupID = groupID;
	}

	public String getMemberID() {
		return memberID;
	}

	public void setMemberID(String memberID) {
		this.memberID = memberID;
	}

	public Integer getGroupScore() {
		return groupScore;
	}

	public void setGroupScore(Integer groupScore) {
		this.groupScore = groupScore;
	}

	public Integer getGroupCoinGained() {
		return groupCoinGained;
	}

	public void setGroupCoinGained(Integer groupCoinGained) {
		this.groupCoinGained = groupCoinGained;
	}

}
