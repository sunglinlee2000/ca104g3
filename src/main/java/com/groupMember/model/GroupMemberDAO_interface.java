package com.groupMember.model;

import java.util.*;

public interface GroupMemberDAO_interface {
	public void insert(GroupMemberVO groupMemberVO);
	public void update(GroupMemberVO groupMemberVO);
	public void delete(String groupID, String memberID);
	public GroupMemberVO findByPrimaryKey(String groupID, String memberID);
	public List<GroupMemberVO> getAll();
	
//	新增主揪的insert
	public void insertInitiator(GroupMemberVO groupMemberVO, java.sql.Connection con);
	


}
