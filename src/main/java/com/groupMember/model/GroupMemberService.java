package com.groupMember.model;

import java.sql.Connection;
import java.util.List;

public class GroupMemberService {

	private GroupMemberDAO_interface dao;
	
	public GroupMemberService() {
		dao = new GroupMemberDAO();
	}
	
	public GroupMemberVO addGroupMember(String groupID, String memberID) {
		
		GroupMemberVO groupMemberVO = new GroupMemberVO();
		
		groupMemberVO.setGroupID(groupID);
		groupMemberVO.setMemberID(memberID);
//		groupMemberVO.setGroupScore(groupScore);
//		groupMemberVO.setGroupCoinGained(groupCoinGained);
		dao.insert(groupMemberVO);
		
		return groupMemberVO;
	}
	
	public GroupMemberVO addGroupInitiator(String groupID, String memberID, Connection con) {
		
		GroupMemberVO groupMemberVO = new GroupMemberVO();
		
		groupMemberVO.setGroupID(groupID);
		groupMemberVO.setMemberID(memberID);
//		groupMemberVO.setGroupScore(groupScore);
//		groupMemberVO.setGroupCoinGained(groupCoinGained);
		dao.insertInitiator(groupMemberVO, con);
		
		return groupMemberVO;
	}
	
	public GroupMemberVO updateGroupMember(String groupID, String memberID, Integer groupScore, 
			Integer groupCoinGained) {
		
		GroupMemberVO groupMemberVO = new GroupMemberVO();
		
		groupMemberVO.setGroupID(groupID);
		groupMemberVO.setMemberID(memberID);
		groupMemberVO.setGroupScore(groupScore);
		groupMemberVO.setGroupCoinGained(groupCoinGained);
		dao.update(groupMemberVO);
		
		return groupMemberVO;
	}
	
	public void deleteGroupMember(String groupID, String memberID) {
		dao.delete(groupID, memberID);
	}
	
	public GroupMemberVO getOneGroupMember(String groupID, String memberID) {
		return dao.findByPrimaryKey(groupID, memberID);
	}
	
	public List<GroupMemberVO> getAll() {
		return dao.getAll();
	}
	
}
