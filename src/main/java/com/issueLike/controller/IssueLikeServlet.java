package com.issueLike.controller;

import java.io.*;
import java.sql.Timestamp;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

import javax.servlet.*;
import javax.servlet.http.*;

import org.json.JSONException;
import org.json.JSONObject;

import com.issue.model.IssueService;
import com.issue.model.IssueVO;
import com.issueLike.model.*;

public class IssueLikeServlet extends HttpServlet {

	public void doGet(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		doPost(req, res);
	}

	public void doPost(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {

		req.setCharacterEncoding("UTF-8");
		String action = req.getParameter("action");
		
		HttpSession session = req.getSession();//取得session資料

		
//        if ("insert".equals(action)) { // 來自addEmp.jsp的請求  
//			
//			List<String> errorMsgs = new LinkedList<String>();
//			// Store this set in the request scope, in case we need to
//			// send the ErrorPage view.
//			req.setAttribute("errorMsgs", errorMsgs);
//			IssueLikeVO issueLikeVO = null;
//			IssueVO issueVO = null;
//
//			try {
//				/***********************1.接收請求參數 - 輸入格式的錯誤處理*************************/
//				
//				String issueID = req.getParameter("issueID");
//				System.out.println(issueID);
//				String memberID = req.getParameter("memberID");
//				System.out.println(memberID);
//				
//				IssueService a = new IssueService();
//				issueVO = a.getOneIssue(issueID);
//				
//				issueLikeVO = new IssueLikeVO();				
//				issueLikeVO.setIssueID(issueID);
//				issueLikeVO.setMemberID(memberID);
//				
//				
//				/***************************2.開始新增資料***************************************/
//				IssueLikeService issueLikeService = new IssueLikeService();
//				issueLikeVO = issueLikeService.addIssueLike(issueID, memberID);
//				
//				/***************************3.新增完成,準備轉交(Send the Success view)***********/
//				req.setAttribute("issueVO", issueVO);
//				String url = "/front-end/issue/issue_detailed.jsp";
//				RequestDispatcher successView = req.getRequestDispatcher(url); // 新增成功後轉交listAllEmp.jsp
//				successView.forward(req, res);				
//				
//				/***************************其他可能的錯誤處理**********************************/
//			} catch (Exception e) {
//				req.setAttribute("issueVO", issueVO);
//				errorMsgs.add(e.getMessage());
//				RequestDispatcher failureView = req
//						.getRequestDispatcher("/front-end/issue/issue_detailed.jsp");
//				failureView.forward(req, res);
//			}
//		}
		
//		
//		if ("delete".equals(action)) { // 來自listAllEmp.jsp
//
//			List<String> errorMsgs = new LinkedList<String>();
//			// Store this set in the request scope, in case we need to
//			// send the ErrorPage view.
//			req.setAttribute("errorMsgs", errorMsgs);
//			IssueLikeVO issueLikeVO = null;
//			IssueVO issueVO = null;
//	
//			try {
//				/***************************1.接收請求參數***************************************/
//				System.out.println("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");
//				String issueID = new String(req.getParameter("issueID"));
//				String memberID = new String(req.getParameter("memberID"));
//				
//				IssueService a = new IssueService();
//				issueVO = a.getOneIssue(issueID);
//				
//				issueLikeVO = new IssueLikeVO();				
//				issueLikeVO.setIssueID(issueID);
//				issueLikeVO.setMemberID(memberID);
//				
//				/***************************2.開始刪除資料***************************************/
//				IssueLikeService issueLikeService = new IssueLikeService();
//				issueLikeService.deleteIssueLike(issueID,memberID);
//				
//				/***************************3.刪除完成,準備轉交(Send the Success view)***********/	
//				req.setAttribute("issueVO", issueVO);
//				String url = "/front-end/issue/issue_detailed.jsp";
//				RequestDispatcher successView = req.getRequestDispatcher(url);// 刪除成功後,轉交回送出刪除的來源網頁
//				successView.forward(req, res);
//				
//				/***************************其他可能的錯誤處理**********************************/
//			} catch (Exception e) {
//				req.setAttribute("issueVO", issueVO);
//				errorMsgs.add("刪除資料失敗:"+e.getMessage());
//				RequestDispatcher failureView = req
//						.getRequestDispatcher("/front-end/issueLike/listAllEmp.jsp");
//				failureView.forward(req, res);
//			}
//		}
		
//		//利用ajax新增
		if("insert".equals(action)) {
			System.out.println(action);
			String issueID = req.getParameter("issueID");
			String memberID = req.getParameter("memberID"); 
			
			JSONObject obj = new JSONObject();
			
			//新增資料			
			IssueLikeService issueLikeService = new IssueLikeService();
			if(issueLikeService.getOneIssueLike(issueID, memberID) == null) {
				issueLikeService.addIssueLike(issueID, memberID);
			}
			Integer likes = issueLikeService.getCount(issueID);
			
			try {
				obj.put("likes", likes);
			} catch (JSONException e) {
				e.printStackTrace();
			}
			
			res.setContentType("text/plain");
			res.setCharacterEncoding("UTF-8");
			PrintWriter out = res.getWriter();
			out.write(obj.toString());
			out.flush();
			out.close();
		}
//		
//		
//		//利用ajax刪除
		if("delete".equals(action)) {
			System.out.println(action);
			String issueID = req.getParameter("issueID");
			String memberID = req.getParameter("memberID"); 
			
			JSONObject obj = new JSONObject();
			
			//刪除資料
			
			IssueLikeService issueLikeService = new IssueLikeService();
			if(issueLikeService.getOneIssueLike(issueID, memberID) != null) {
				issueLikeService.deleteIssueLike(issueID, memberID);
			}
			Integer likes = issueLikeService.getCount(issueID);
			
			try {
				obj.put("likes", likes);
			} catch (JSONException e) {
				e.printStackTrace();
			}
			
			res.setContentType("text/plain");
			res.setCharacterEncoding("UTF-8");
			PrintWriter out = res.getWriter();
			out.write(obj.toString());
			out.flush();
			out.close();
		}
	}
}
