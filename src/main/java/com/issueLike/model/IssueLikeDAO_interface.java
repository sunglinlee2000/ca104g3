package com.issueLike.model;

import java.util.List;

public interface IssueLikeDAO_interface {
	
	public void insert(IssueLikeVO issueLikeVO);
	public void delete(String issueID, String memberID);
	public IssueLikeVO findByPrimaryKey(String issueID, String memberID);
	public void deleteByIssueID(String issueID);
	public Integer getCount(String issueID);//查總讚數
	

}
