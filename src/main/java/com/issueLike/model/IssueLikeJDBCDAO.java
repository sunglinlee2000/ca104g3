package com.issueLike.model;

import java.util.*;

import com.issue.model.IssueVO;

import java.sql.*;

public class IssueLikeJDBCDAO implements IssueLikeDAO_interface {
	String driver = "oracle.jdbc.driver.OracleDriver";
	String url = "jdbc:oracle:thin:@localhost:1521:XE";
	String userid = "CA104G3";
	String passwd = "123456";
	
	
	private static final String INSERT_STMT =
			"INSERT INTO issuelike "
			+ "(issueID,memberID)"
			+ " VALUES "
			+ "(?,?)";
		private static final String DELETE = 
			"DELETE FROM issuelike where issueID = ? and memberID = ?";
		private static final String GET_ONE_STMT = 
			"SELECT issueID,memberID FROM issuelike where issueID = ? and memberID = ?";
		
		private static final String DELETEBYISSUEID = 
				"DELETE FROM issueLike where issueID = ?";

	
	@Override
	public void insert(IssueLikeVO issueLikeVO) {
		Connection con = null;
		PreparedStatement pstmt = null;
		
		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(INSERT_STMT);

			pstmt.setString(1, issueLikeVO.getIssueID());
			pstmt.setString(2, issueLikeVO.getMemberID());
			
			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		
	}	
	
	
	@Override
	public void delete(String issueID, String memberID) {
		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(DELETE);

			pstmt.setString(1, issueID);
			pstmt.setString(2, memberID);
			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

		
	}

	
	@Override
	public IssueLikeVO findByPrimaryKey(String issueID, String memberID) {
		IssueLikeVO issueLikeVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(GET_ONE_STMT);

			pstmt.setString(1, issueID);
			pstmt.setString(2, memberID);
			
			rs = pstmt.executeQuery();

			while (rs.next()) {
				
				issueLikeVO = new IssueLikeVO();
				issueLikeVO.setIssueID(rs.getString("issueID"));
				issueLikeVO.setMemberID(rs.getString("memberID"));
				
			}

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return issueLikeVO;
	}
	
	@Override
	public void deleteByIssueID(String issueID) {
		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(DELETEBYISSUEID);

			pstmt.setString(1, issueID);
			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

		
	}

	
	
	public static void main(String[] args) {
		IssueLikeJDBCDAO dao = new IssueLikeJDBCDAO();
		
		// 新增WOK
//		IssueLikeVO issueLikeVO1 = new IssueLikeVO();
//		issueLikeVO1.setIssueID("I000003");
//		issueLikeVO1.setMemberID("M000006");
//		dao.insert(issueLikeVO1);
//		System.out.println("OK");
		
		// 刪除OK
//		dao.delete("I000005","M000001");
//		System.out.println("OK");
		
		//刪除deleteByIssueID OK
//		dao.deleteByIssueID("I000001");
		System.out.println(dao.findByPrimaryKey("I000001", "M000011"));
	}


	@Override
	public Integer getCount(String issueID) {
		// TODO Auto-generated method stub
		return null;
	}
	
}
