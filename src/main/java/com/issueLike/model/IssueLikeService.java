package com.issueLike.model;


public class IssueLikeService {
	
	private IssueLikeDAO_interface issueLikeDAO;
	
	public IssueLikeService() {
		issueLikeDAO = new IssueLikeDAO();
	}
	
	public IssueLikeVO addIssueLike (String issueID,String memberID) {
		
		
		IssueLikeVO issueLikeVO = new IssueLikeVO();
		
		
		issueLikeVO.setIssueID(issueID);
		issueLikeVO.setMemberID(memberID);
		
		issueLikeDAO.insert(issueLikeVO);
		
		return issueLikeVO;
	}
	
	public void deleteIssueLike(String issueID,String memberID) {
		issueLikeDAO.delete(issueID, memberID);
	}
	
	public void deleteByIssueID(String issueID) {
		issueLikeDAO.deleteByIssueID(issueID);
	}
	
	public Integer getCount(String issueID) {
		return issueLikeDAO.getCount(issueID);
	}
	
	public IssueLikeVO getOneIssueLike(String issueID, String memberID) {
		return issueLikeDAO.findByPrimaryKey(issueID, memberID);
	}
}
