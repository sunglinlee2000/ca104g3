package com.issueReport.controller;

import java.io.*;
import java.sql.Timestamp;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

import javax.servlet.*;
import javax.servlet.http.*;

import com.commentList.model.CommentListService;
import com.issue.model.IssueService;
import com.issue.model.IssueVO;
import com.issueLike.model.IssueLikeService;
import com.issueReport.model.*;
import com.issueShare.model.IssueShareService;
import com.member.model.MemberService;
import com.member.model.MemberVO;
import com.notificationList.model.NotificationListService;

public class IssueReportServlet extends HttpServlet {

	public void doGet(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		doPost(req, res);
	}

	public void doPost(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {

		req.setCharacterEncoding("UTF-8");
		String action = req.getParameter("action");
		
		HttpSession session = req.getSession();
		
		if ("getOne_For_Display".equals(action)) { // 來自select_page.jsp的請求

			List<String> errorMsgs = new LinkedList<String>();
			// Store this set in the request scope, in case we need to
			// send the ErrorPage view.
			req.setAttribute("errorMsgs", errorMsgs);

			try {
				/***************************1.接收請求參數 - 輸入格式的錯誤處理**********************/
				String issueReportID = req.getParameter("issueReportID");
//				if (issueReportID == null || (issueReportID.trim()).length() == 0) {
//					errorMsgs.add("請輸入檢舉編號");
//				}
				// Send the use back to the form, if there were errors
//				if (!errorMsgs.isEmpty()) {
//					RequestDispatcher failureView = req
//							.getRequestDispatcher("/front-end/issueReport/select_page.jsp");
//					failureView.forward(req, res);
//					return;//程式中斷
//				}
				
//				String issueReportID = null;
//				try {
//					issueReportID = new String(issueReportID);
//				} catch (Exception e) {
//					errorMsgs.add("員工編號格式不正確");
//				}
				// Send the use back to the form, if there were errors
				if (!errorMsgs.isEmpty()) {
					RequestDispatcher failureView = req
							.getRequestDispatcher("/front-end/issueReport/select_page.jsp");
					failureView.forward(req, res);
					return;//程式中斷
				}
				
				/***************************2.開始查詢資料*****************************************/
				IssueReportService issueReportSvc = new IssueReportService();
				IssueReportVO issueReportVO = issueReportSvc.getOneIssueReport(issueReportID);
				if (issueReportVO == null) {
					errorMsgs.add("查無資料");
				}
				// Send the use back to the form, if there were errors
				if (!errorMsgs.isEmpty()) {
					RequestDispatcher failureView = req
							.getRequestDispatcher("/front-end/issueReport/select_page.jsp");
					failureView.forward(req, res);
					return;//程式中斷
				}
				
				/***************************3.查詢完成,準備轉交(Send the Success view)*************/
				req.setAttribute("issueReportVO", issueReportVO); // 資料庫取出的empVO物件,存入req
				String url = "/front-end/issueReport/listOneEmp.jsp";
				RequestDispatcher successView = req.getRequestDispatcher(url); // 成功轉交 listOneEmp.jsp
				successView.forward(req, res);

				/***************************其他可能的錯誤處理*************************************/
			} catch (Exception e) {
				errorMsgs.add("無法取得資料:" + e.getMessage());
				RequestDispatcher failureView = req
						.getRequestDispatcher("/front-end/issueReport/select_page.jsp");
				failureView.forward(req, res);
			}
		}
		
		
		if ("getOne_For_Update".equals(action)) { // 來自listAllEmp.jsp的請求

			List<String> errorMsgs = new LinkedList<String>();
			// Store this set in the request scope, in case we need to
			// send the ErrorPage view.
			req.setAttribute("errorMsgs", errorMsgs);
			
			try {
				/***************************1.接收請求參數****************************************/
				String issueReportID = new String(req.getParameter("issueReportID"));
				
				/***************************2.開始查詢資料****************************************/
				IssueReportService issueReportSvc = new IssueReportService();
				IssueReportVO issueReportVO = issueReportSvc.getOneIssueReport(issueReportID);
								
				/***************************3.查詢完成,準備轉交(Send the Success view)************/
				req.setAttribute("issueReportVO", issueReportVO);         // 資料庫取出的empVO物件,存入req
				String url = "/front-end/issueReport/update_emp_input.jsp";
				RequestDispatcher successView = req.getRequestDispatcher(url);// 成功轉交 update_emp_input.jsp
				successView.forward(req, res);

				/***************************其他可能的錯誤處理**********************************/
			} catch (Exception e) {
				errorMsgs.add("無法取得要修改的資料:" + e.getMessage());
				RequestDispatcher failureView = req
						.getRequestDispatcher("/front-end/issueReport/listAllEmp.jsp");
				failureView.forward(req, res);
			}
		}
		
		
		if ("update".equals(action)) { // 來自update_emp_input.jsp的請求
			
			List<String> errorMsgs = new LinkedList<String>();
			// Store this set in the request scope, in case we need to
			// send the ErrorPage view.
			req.setAttribute("errorMsgs", errorMsgs);
		
			try {
				/***************************1.接收請求參數 - 輸入格式的錯誤處理**********************/
				
				String issueReportID = new String(req.getParameter("issueReportID").trim());																			
				String issueReportStatus = req.getParameter("issueReportStatus");
				if (issueReportStatus == null || issueReportStatus.trim().length() == 0) {
					errorMsgs.add("請勾選項目");
				} 				
			
				IssueReportVO issueReportVO = new IssueReportVO();
				issueReportVO.setIssueReportID(issueReportID);
				issueReportVO.setIssueReportStatus(issueReportStatus);

				// Send the use back to the form, if there were errors
				if (!errorMsgs.isEmpty()) {
					req.setAttribute("issueReportVO", issueReportVO); // 含有輸入格式錯誤的empVO物件,也存入req
					RequestDispatcher failureView = req
							.getRequestDispatcher("/front-end/issueReport/update_emp_input.jsp");
					failureView.forward(req, res);
					return; //程式中斷
				}
				
				/***************************2.開始修改資料*****************************************/
				IssueReportService issueReportSvc = new IssueReportService();
				issueReportVO = issueReportSvc.updateIssueReport(issueReportID, issueReportStatus);
				
				/***************************3.修改完成,準備轉交(Send the Success view)*************/
				req.setAttribute("issueReportVO", issueReportVO); // 資料庫update成功後,正確的的empVO物件,存入req
				String url = "/front-end/issueReport/listOneEmp.jsp";
				RequestDispatcher successView = req.getRequestDispatcher(url); // 修改成功後,轉交listOneEmp.jsp
				successView.forward(req, res);

				/***************************其他可能的錯誤處理*************************************/
			} catch (Exception e) {
				errorMsgs.add("修改資料失敗:"+e.getMessage());
				RequestDispatcher failureView = req
						.getRequestDispatcher("/front-end/issueReport/update_emp_input.jsp");
				failureView.forward(req, res);
			}
		}

        if ("insert".equals(action)) { // 來自addEmp.jsp的請求  
			
			List<String> errorMsgs = new LinkedList<String>();
			// Store this set in the request scope, in case we need to
			// send the ErrorPage view.
			req.setAttribute("errorMsgs", errorMsgs);

			try {
				/***********************1.接收請求參數 - 輸入格式的錯誤處理*************************/
				
				String boardID = (String)session.getAttribute("boardID");//取回專版編號，動作完回到專版話題列表
				System.out.println("11111"+boardID);
				String issueID = req.getParameter("issueID");	
				String memberID = req.getParameter("memberID");
				String administratorID = req.getParameter("administratorID");
				String issueReportContent = req.getParameter("issueReportContent");


				Timestamp issueReportDate = new java.sql.Timestamp(System.currentTimeMillis());
				String issueReportReason = req.getParameter("issueReportReason");
				IssueReportVO issueReportVO = new IssueReportVO();				
				issueReportVO.setIssueID(issueID);
				issueReportVO.setMemberID(memberID);
				issueReportVO.setAdministratorID("A000001");
				issueReportVO.setIssueReportContent(issueReportContent);	
				Timestamp time = new Timestamp(System.currentTimeMillis());
				issueReportVO.setIssueReportDate(java.sql.Timestamp.valueOf(ZonedDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"))));
				issueReportVO.setIssueReportStatus("NEW_REPORT");
				issueReportVO.setIssueReportReason(issueReportReason);
				
				
				// Send the use back to the form, if there were errors
				if (!errorMsgs.isEmpty()) {
					req.setAttribute("issueReportVO", issueReportVO); // 含有輸入格式錯誤的empVO物件,也存入req
					RequestDispatcher failureView = req
							.getRequestDispatcher("/front-end/issueReport/addEmp.jsp");
					failureView.forward(req, res);
					return;
				}
				
				/***************************2.開始新增資料***************************************/
				IssueReportService issueReportSvc = new IssueReportService();
				issueReportVO = issueReportSvc.addIssueReport(issueID,memberID,"A000001",issueReportContent, time, "NEW_REPORT",issueReportReason);
				
				/***************************3.新增完成,準備轉交(Send the Success view)***********/
				req.setAttribute("boardIDxx",boardID);//利用取到的boardID返回列表
				String url = "/front-end/issue/issue_list.jsp";
//				RequestDispatcher successView = req.getRequestDispatcher(url); // 新增成功後轉交listAllEmp.jsp
//				successView.forward(req, res);				
				res.sendRedirect(req.getContextPath() + "/issue/issue.do?action=getOne_For_Display&issueID="+issueID);
				/***************************其他可能的錯誤處理**********************************/
			} catch (Exception e) {
				errorMsgs.add(e.getMessage());
				RequestDispatcher failureView = req
						.getRequestDispatcher("/front-end/issueReport/addEmp.jsp");
				failureView.forward(req, res);


			}
		}
		
		
		if ("delete".equals(action)) { // 來自listAllEmp.jsp

			List<String> errorMsgs = new LinkedList<String>();
			// Store this set in the request scope, in case we need to
			// send the ErrorPage view.
			req.setAttribute("errorMsgs", errorMsgs);
	
			try {
				/***************************1.接收請求參數***************************************/
				String issueReportID = new String(req.getParameter("issueReportID"));
				
				/***************************2.開始刪除資料***************************************/
				IssueReportService issueReportSvc = new IssueReportService();
				
//				新增通知的宣告				
				IssueReportVO issueReportVO = issueReportSvc.getOneIssueReport(issueReportID);
				String memberID = issueReportVO.getMemberID();
//				開始新增檢舉通知	
				String notificationContent ="管理員已駁回您對該文章的檢舉";
				Timestamp now = new Timestamp(new Date().getTime());
				NotificationListService notificationListSvc = new NotificationListService();
				notificationListSvc.addNoList("NT000001", memberID, notificationContent, now, "", "FALSE", 
						"/issue/issue.do?action=getOne_For_Display&issueID=");
				
				issueReportSvc.deleteIssueReport(issueReportID);
				
				/***************************3.刪除完成,準備轉交(Send the Success view)***********/								
				String url = "/back-end/issueReport/listAllEmp.jsp";
				RequestDispatcher successView = req.getRequestDispatcher(url);// 刪除成功後,轉交回送出刪除的來源網頁
				successView.forward(req, res);
				
				/***************************其他可能的錯誤處理**********************************/
			} catch (Exception e) {
				errorMsgs.add("刪除資料失敗:"+e.getMessage());
				RequestDispatcher failureView = req
						.getRequestDispatcher("/back-end/issueReport/listAllEmp.jsp");
				failureView.forward(req, res);
			}
		}
		
		if ("deleteByIssueID".equals(action)) { 

			List<String> errorMsgs = new LinkedList<String>();
			// Store this set in the request scope, in case we need to
			// send the ErrorPage view.
			req.setAttribute("errorMsgs", errorMsgs);
	
			try {
				/***************************1.接收請求參數***************************************/
				String issueID = new String(req.getParameter("issueID"));
				String issueReportID = new String(req.getParameter("issueReportID"));
				/***************************2.開始刪除資料***************************************/
				
				IssueShareService issueShareSvc = new IssueShareService();
				issueShareSvc.deleteByIssueID(issueID);
				IssueLikeService issueLikeSvc = new IssueLikeService();
				issueLikeSvc.deleteByIssueID(issueID);
				CommentListService commentListSvc = new CommentListService();
				commentListSvc.deleteByIssueID(issueID);
				IssueReportService issueReportSvc = new IssueReportService();
				issueReportSvc.deleteByIssueID(issueReportID);
				IssueService issueSvc = new IssueService();
				issueSvc.deleteByIssueID(issueID);

////				新增通知的宣告				
//				IssueReportVO issueReportVO = issueReportSvc.getOneIssueReport(issueReportID);
//				String memberID = issueReportVO.getMemberID();
////				開始新增檢舉通知	
//				String notificationContent ="管理員已接受您的檢舉";
//				Timestamp now = new Timestamp(new Date().getTime());
//				NotificationListService notificationListSvc = new NotificationListService();
//				notificationListSvc.addNoList("NT000001", memberID, notificationContent, now, "", "FALSE", 
//						"/issue/issue.do?action=getOne_For_Display&issueID=");				
//				
////			新增通知的宣告
//				IssueVO issueVO = issueSvc.getOneIssue(issueID);
//				String memberID1 = issueVO.getMemberID();
//				System.out.println(memberID1);
////			開始新增檢舉通知	
//				String notificationContent1 ="您的文章已被管理員強制刪除，";
//				Timestamp now1 = new Timestamp(new Date().getTime());
//				NotificationListService notificationListSvc1 = new NotificationListService();
//				notificationListSvc1.addNoList("NT000001", memberID1, notificationContent1, now1, memberID1, "FALSE", 
//						"/personalPage/personalPage.do?action=toOnePersonalPage&memberID=");

				

				/***************************3.刪除完成,準備轉交(Send the Success view)***********/								
				String url = "/back-end/issueReport/listAllEmp.jsp";
				RequestDispatcher successView = req.getRequestDispatcher(url);// 刪除成功後,轉交回送出刪除的來源網頁
				successView.forward(req, res);
				
		

				
				/***************************其他可能的錯誤處理**********************************/
			} catch (Exception e) {
				errorMsgs.add("刪除資料失敗:"+"文章已經被分享過搂!!所以要先去刪小表");
				RequestDispatcher failureView = req
						.getRequestDispatcher("/back-end/issueReport/listAllEmp.jsp");
				failureView.forward(req, res);
			}
		}
	}
}
