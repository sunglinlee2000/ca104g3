package com.issueReport.model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import com.issue.model.IssueVO;

public class IssueReportJDBCDAO implements IssueReportDAO_interface {
	
	String driver = "oracle.jdbc.driver.OracleDriver";
	String url = "jdbc:oracle:thin:@localhost:1521:XE";
	String userid = "CA104G3";
	String passwd = "123456";
	
	private static final String INSERT_STMT =			
			"INSERT INTO issuereport "
			+ "(issueReportID,issueID,memberID,administratorID,issueReportContent,issueReportDate,issueReportStatus,issueReportReason)"
			+ " VALUES "
			+ "('IR'||LPAD(to_char(ISSUE_seq.NEXTVAL), 6, '0'),?,?,?,?,?,?,?)";
		private static final String GET_ALL_STMT = 
			"SELECT issueReportID,issueID,memberID,administratorID,issueReportContent,issueReportDate,issueReportStatus,issueReportReason FROM issuereport order by issueReportID";
		private static final String GET_ONE_STMT = 
			"SELECT issueReportID,issueID,memberID,administratorID,issueReportContent,issueReportDate,issueReportStatus,issueReportReason FROM issuereport where issueReportID = ?";
		private static final String DELETE = 
			"DELETE FROM issuereport where issueReportID = ?";
		private static final String UPDATE = 
			"UPDATE issuereport set issueReportStatus=? where issueReportID = ?";
		
		private static final String DELETEBYISSUEID = 
				"DELETE FROM issueReport where issueID = ?";
		
		@Override
		public void insert(IssueReportVO issueReportVO) {
			Connection con = null;
			PreparedStatement pstmt = null;
			
			try {

				Class.forName(driver);
				con = DriverManager.getConnection(url, userid, passwd);
				pstmt = con.prepareStatement(INSERT_STMT);
//issueReportID,issueID,memberID,administratorID,issueReportContent,issueReportDate,issueReportStatus,issueReportReason

				pstmt.setString(1, issueReportVO.getIssueID());
				pstmt.setString(2, issueReportVO.getMemberID());
				pstmt.setString(3, issueReportVO.getAdministratorID());
				pstmt.setString(4, issueReportVO.getIssueReportContent());
				pstmt.setTimestamp(5, issueReportVO.getIssueReportDate());
				pstmt.setString(6, issueReportVO.getIssueReportStatus());
				pstmt.setString(7, issueReportVO.getIssueReportReason());
				pstmt.executeUpdate();

				// Handle any driver errors
			} catch (ClassNotFoundException e) {
				throw new RuntimeException("Couldn't load database driver. "
						+ e.getMessage());
				// Handle any SQL errors
			} catch (SQLException se) {
				throw new RuntimeException("A database error occured. "
						+ se.getMessage());
				// Clean up JDBC resources
			} finally {
				if (pstmt != null) {
					try {
						pstmt.close();
					} catch (SQLException se) {
						se.printStackTrace(System.err);
					}
				}
				if (con != null) {
					try {
						con.close();
					} catch (Exception e) {
						e.printStackTrace(System.err);
					}
				}
			}
			
		}
		
		@Override
		public void update(IssueReportVO issueReportVO) {
//issueReportID,issueID,memberID,administratorID,issueReportContent,issueReportDate,issueReportStatus,issueReportReason
			Connection con = null;
			PreparedStatement pstmt = null;

			try {

				Class.forName(driver);
				con = DriverManager.getConnection(url, userid, passwd);
				pstmt = con.prepareStatement(UPDATE);

				pstmt.setString(1, issueReportVO.getIssueReportStatus());
				pstmt.setString(2, issueReportVO.getIssueReportID());	
				
				pstmt.executeUpdate();

				// Handle any driver errors
			} catch (ClassNotFoundException e) {
				throw new RuntimeException("Couldn't load database driver. "
						+ e.getMessage());
				// Handle any SQL errors
			} catch (SQLException se) {
				throw new RuntimeException("A database error occured. "
						+ se.getMessage());
				// Clean up JDBC resources
			} finally {
				if (pstmt != null) {
					try {
						pstmt.close();
					} catch (SQLException se) {
						se.printStackTrace(System.err);
					}
				}
				if (con != null) {
					try {
						con.close();
					} catch (Exception e) {
						e.printStackTrace(System.err);
					}
				}
			}

			
		}
		@Override
		public void delete(String issueReportID) {
//issueReportID,issueID,memberID,administratorID,issueReportContent,issueReportDate,issueReportStatus,issueReportReason
			Connection con = null;
			PreparedStatement pstmt = null;

			try {

				Class.forName(driver);
				con = DriverManager.getConnection(url, userid, passwd);
				pstmt = con.prepareStatement(DELETE);

				pstmt.setString(1, issueReportID);
				pstmt.executeUpdate();

				// Handle any driver errors
			} catch (ClassNotFoundException e) {
				throw new RuntimeException("Couldn't load database driver. "
						+ e.getMessage());
				// Handle any SQL errors
			} catch (SQLException se) {
				throw new RuntimeException("A database error occured. "
						+ se.getMessage());
				// Clean up JDBC resources
			} finally {
				if (pstmt != null) {
					try {
						pstmt.close();
					} catch (SQLException se) {
						se.printStackTrace(System.err);
					}
				}
				if (con != null) {
					try {
						con.close();
					} catch (Exception e) {
						e.printStackTrace(System.err);
					}
				}
			}

			
		}
		@Override
		public IssueReportVO findByPrimaryKey(String issueReportID) {
			IssueReportVO issueReportVO = null;
			Connection con = null;
			PreparedStatement pstmt = null;
			ResultSet rs = null;

			try {

				Class.forName(driver);
				con = DriverManager.getConnection(url, userid, passwd);
				pstmt = con.prepareStatement(GET_ONE_STMT);

				pstmt.setString(1, issueReportID);

				rs = pstmt.executeQuery();

				while (rs.next()) {
					// empVo �]�٬� Domain objects
//issueReportID,issueID,memberID,administratorID,issueReportContent,issueReportDate,issueReportStatus,issueReportReason
					
					issueReportVO = new IssueReportVO();
					issueReportVO.setIssueReportID(rs.getString("issueReportID"));
					issueReportVO.setIssueID(rs.getString("issueID"));
					issueReportVO.setMemberID(rs.getString("memberID"));
					issueReportVO.setAdministratorID(rs.getString("administratorID"));
					issueReportVO.setIssueReportContent(rs.getString("issueReportContent"));
					issueReportVO.setIssueReportDate(rs.getTimestamp("issueReportDate"));
					issueReportVO.setIssueReportStatus(rs.getString("issueReportStatus"));
					issueReportVO.setIssueReportReason(rs.getString("issueReportReason"));
				}

				// Handle any driver errors
			} catch (ClassNotFoundException e) {
				throw new RuntimeException("Couldn't load database driver. "
						+ e.getMessage());
				// Handle any SQL errors
			} catch (SQLException se) {
				throw new RuntimeException("A database error occured. "
						+ se.getMessage());
				// Clean up JDBC resources
			} finally {
				if (rs != null) {
					try {
						rs.close();
					} catch (SQLException se) {
						se.printStackTrace(System.err);
					}
				}
				if (pstmt != null) {
					try {
						pstmt.close();
					} catch (SQLException se) {
						se.printStackTrace(System.err);
					}
				}
				if (con != null) {
					try {
						con.close();
					} catch (Exception e) {
						e.printStackTrace(System.err);
					}
				}
			}
			return issueReportVO;
		}
		
		@Override
		public List<IssueReportVO> getAll() {
			List<IssueReportVO> list = new ArrayList<IssueReportVO>();
			IssueReportVO issueReportVO = null;
			
			Connection con = null;
			PreparedStatement pstmt = null;
			ResultSet rs = null;
			
			try {

				Class.forName(driver);
				con = DriverManager.getConnection(url, userid, passwd);
				pstmt = con.prepareStatement(GET_ALL_STMT);
				rs = pstmt.executeQuery();

				while (rs.next()) {
					// empVO �]�٬� Domain objects
//issueReportID,issueID,memberID,administratorID,issueReportContent,issueReportDate,issueReportStatus,issueReportReason
					
					issueReportVO = new IssueReportVO();				
					issueReportVO.setIssueReportID(rs.getString("issueReportID"));
					issueReportVO.setIssueID(rs.getString("issueID"));
					issueReportVO.setMemberID(rs.getString("memberID"));
					issueReportVO.setAdministratorID(rs.getString("administratorID"));
					issueReportVO.setIssueReportContent(rs.getString("issueReportContent"));
					issueReportVO.setIssueReportDate(rs.getTimestamp("issueReportDate"));
					issueReportVO.setIssueReportStatus(rs.getString("issueReportStatus"));
					issueReportVO.setIssueReportReason(rs.getString("issueReportReason"));
					
					list.add(issueReportVO); // Store the row in the list
				}

				// Handle any driver errors
			} catch (ClassNotFoundException e) {
				throw new RuntimeException("Couldn't load database driver. "
						+ e.getMessage());
				// Handle any SQL errors
			} catch (SQLException se) {
				throw new RuntimeException("A database error occured. "
						+ se.getMessage());
				// Clean up JDBC resources
			} finally {
				if (rs != null) {
					try {
						rs.close();
					} catch (SQLException se) {
						se.printStackTrace(System.err);
					}
				}
				if (pstmt != null) {
					try {
						pstmt.close();
					} catch (SQLException se) {
						se.printStackTrace(System.err);
					}
				}
				if (con != null) {
					try {
						con.close();
					} catch (Exception e) {
						e.printStackTrace(System.err);
					}
				}
			}
			return list;			
		}
		
		@Override
		public void deleteByIssueID(String issueID) {
//issueReportID,issueID,memberID,administratorID,issueReportContent,issueReportDate,issueReportStatus,issueReportReason
			Connection con = null;
			PreparedStatement pstmt = null;

			try {

				Class.forName(driver);
				con = DriverManager.getConnection(url, userid, passwd);
				pstmt = con.prepareStatement(DELETEBYISSUEID);

				pstmt.setString(1, issueID);
				pstmt.executeUpdate();

				// Handle any driver errors
			} catch (ClassNotFoundException e) {
				throw new RuntimeException("Couldn't load database driver. "
						+ e.getMessage());
				// Handle any SQL errors
			} catch (SQLException se) {
				throw new RuntimeException("A database error occured. "
						+ se.getMessage());
				// Clean up JDBC resources
			} finally {
				if (pstmt != null) {
					try {
						pstmt.close();
					} catch (SQLException se) {
						se.printStackTrace(System.err);
					}
				}
				if (con != null) {
					try {
						con.close();
					} catch (Exception e) {
						e.printStackTrace(System.err);
					}
				}
			}

			
		}
		
		
		public static void main(String[] args) {
			IssueReportJDBCDAO dao = new IssueReportJDBCDAO();
			
            // 新增OK
//			IssueReportVO issueReportVO1 = new IssueReportVO();
//			issueReportVO1.setIssueID("I000005");
//			issueReportVO1.setMemberID("M000005");
//			issueReportVO1.setAdministratorID("A000001");
//			issueReportVO1.setIssueReportContent(null);
//			issueReportVO1.setIssueReportDate(java.sql.Timestamp.valueOf(ZonedDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"))));
//			issueReportVO1.setIssueReportStatus("審核未通過");
//			issueReportVO1.setIssueReportReason("內容不雅");
//			dao.insert(issueReportVO1);
			
			
			// 修改OK 

//			IssueReportVO issueReportVO2 = new IssueReportVO();
//			issueReportVO2.setIssueReportID("IR000001");
//			System.out.println(".......");
//			issueReportVO2.setIssueReportStatus("審核通過");
//			System.out.println(".......");
//			dao.update(issueReportVO2);
//			System.out.println(".......");
			
			
			// 刪除OK
//			dao.delete("IR000015");
			
			//刪除deleteByIssueID OK
//			dao.deleteByIssueID("I000001");
			
			
			// 查詢OK
			
//			IssueReportVO issueReportVO3 = dao.findByPrimaryKey("IR000001");
//			System.out.print(issueReportVO3.getIssueReportID() + ",");
//			System.out.print(issueReportVO3.getIssueID() + ",");
//			System.out.print(issueReportVO3.getMemberID() + ",");
//			System.out.print(issueReportVO3.getAdministratorID() + ",");
//			System.out.print(issueReportVO3.getIssueReportContent() + ",");
//			System.out.print(issueReportVO3.getIssueReportDate() + ",");
//			System.out.print(issueReportVO3.getIssueReportStatus() + ",");
//			System.out.println(issueReportVO3.getIssueReportReason());
//			System.out.println("---------------------");
//			
			
			// 查全部OK
//			List<IssueReportVO> list = dao.getAll();
//			for(IssueReportVO aIssueReport : list) {
//			System.out.print(aIssueReport.getIssueReportID() + ",");
//			System.out.print(aIssueReport.getIssueID() + ",");
//			System.out.print(aIssueReport.getMemberID() + ",");
//			System.out.print(aIssueReport.getAdministratorID() + ",");
//			System.out.print(aIssueReport.getIssueReportContent() + ",");
//			System.out.print(aIssueReport.getIssueReportDate() + ",");
//			System.out.print(aIssueReport.getIssueReportStatus() + ",");
//			System.out.println(aIssueReport.getIssueReportReason());
//			System.out.println("---------------------");
//			}
			
		}

		@Override
		public Integer getIsReportCount() {
			// TODO Auto-generated method stub
			return null;
		}
	
}
