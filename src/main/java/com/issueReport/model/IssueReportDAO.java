package com.issueReport.model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

public class IssueReportDAO implements IssueReportDAO_interface {
	private static DataSource ds = null;
	static {
		try {
			Context ctx = new InitialContext();
			ds = (DataSource) ctx.lookup("java:comp/env/jdbc/CA104G3DB");
		} catch (NamingException e) {
			e.printStackTrace();
		}
	}
	
	private static final String INSERT_STMT =			
			"INSERT INTO issueReport "
			+ "(issueReportID,issueID,memberID,administratorID,issueReportContent,issueReportDate,issueReportStatus,issueReportReason)"
			+ " VALUES "
			+ "('IR'||LPAD(to_char(ISSUE_seq.NEXTVAL), 6, '0'),?,?,?,?,?,?,?)";
		private static final String GET_ALL_STMT = 
			"SELECT issueReportID,issueID,memberID,administratorID,issueReportContent,issueReportDate,issueReportStatus,issueReportReason FROM issueReport order by issueReportID";
		private static final String GET_ONE_STMT = 
			"SELECT issueReportID,issueID,memberID,administratorID,issueReportContent,issueReportDate,issueReportStatus,issueReportReason FROM issueReport where issueReportID = ?";
		private static final String DELETE = 
			"DELETE FROM issueReport where issueReportID = ?";
		private static final String UPDATE = 
			"UPDATE issueReport set issueReportStatus=? where issueReportID = ?";
		
		private static final String DELETEBYISSUEID = 
			"DELETE FROM issueReport where issueReportID = ?";
		private static final String GET_ISREPORT_COUNT = 
				"SELECT COUNT(*) AS COUNT FROM ISSUEREPORT WHERE issueReportStatus = 'NEW_REPORT'";

		
		@Override
		public void insert(IssueReportVO issueReportVO) {
			Connection con = null;
			PreparedStatement pstmt = null;
			
			try {

				con = ds.getConnection();
				pstmt = con.prepareStatement(INSERT_STMT);
//issueReportID,issueID,memberID,administratorID,issueReportContent,issueReportDate,issueReportStatus,issueReportReason

				pstmt.setString(1, issueReportVO.getIssueID());
				pstmt.setString(2, issueReportVO.getMemberID());
				pstmt.setString(3, issueReportVO.getAdministratorID());
				pstmt.setString(4, issueReportVO.getIssueReportContent());
				pstmt.setTimestamp(5, issueReportVO.getIssueReportDate());
				pstmt.setString(6, issueReportVO.getIssueReportStatus());
				pstmt.setString(7, issueReportVO.getIssueReportReason());
				pstmt.executeUpdate();

				// Handle any driver errors
			} catch (SQLException se) {
				throw new RuntimeException("A database error occured. "
						+ se.getMessage());
				// Clean up JDBC resources
			} finally {
				if (pstmt != null) {
					try {
						pstmt.close();
					} catch (SQLException se) {
						se.printStackTrace(System.err);
					}
				}
				if (con != null) {
					try {
						con.close();
					} catch (Exception e) {
						e.printStackTrace(System.err);
					}
				}
			}
			
		}
		
		@Override
		public void update(IssueReportVO issueReportVO) {
//issueReportID,issueID,memberID,administratorID,issueReportContent,issueReportDate,issueReportStatus,issueReportReason
			Connection con = null;
			PreparedStatement pstmt = null;

			try {

				con = ds.getConnection();
				pstmt = con.prepareStatement(UPDATE);

				pstmt.setString(1, issueReportVO.getIssueReportStatus());
				pstmt.setString(2, issueReportVO.getIssueReportID());	
				
				pstmt.executeUpdate();

				// Handle any driver errors
			} catch (SQLException se) {
				throw new RuntimeException("A database error occured. "
						+ se.getMessage());
				// Clean up JDBC resources
			} finally {
				if (pstmt != null) {
					try {
						pstmt.close();
					} catch (SQLException se) {
						se.printStackTrace(System.err);
					}
				}
				if (con != null) {
					try {
						con.close();
					} catch (Exception e) {
						e.printStackTrace(System.err);
					}
				}
			}

			
		}
		@Override
		public void delete(String issueReportID) {
//issueReportID,issueID,memberID,administratorID,issueReportContent,issueReportDate,issueReportStatus,issueReportReason
			Connection con = null;
			PreparedStatement pstmt = null;

			try {

				con = ds.getConnection();
				pstmt = con.prepareStatement(DELETE);

				pstmt.setString(1, issueReportID);
				pstmt.executeUpdate();

				// Handle any driver errors
			} catch (SQLException se) {
				throw new RuntimeException("A database error occured. "
						+ se.getMessage());
				// Clean up JDBC resources
			} finally {
				if (pstmt != null) {
					try {
						pstmt.close();
					} catch (SQLException se) {
						se.printStackTrace(System.err);
					}
				}
				if (con != null) {
					try {
						con.close();
					} catch (Exception e) {
						e.printStackTrace(System.err);
					}
				}
			}

			
		}
		@Override
		public IssueReportVO findByPrimaryKey(String issueReportID) {
			IssueReportVO issueReportVO = null;
			Connection con = null;
			PreparedStatement pstmt = null;
			ResultSet rs = null;

			try {

				con = ds.getConnection();
				pstmt = con.prepareStatement(GET_ONE_STMT);

				pstmt.setString(1, issueReportID);

				rs = pstmt.executeQuery();

				while (rs.next()) {
					// empVo �]�٬� Domain objects
//issueReportID,issueID,memberID,administratorID,issueReportContent,issueReportDate,issueReportStatus,issueReportReason
					
					issueReportVO = new IssueReportVO();
					issueReportVO.setIssueReportID(rs.getString("issueReportID"));
					issueReportVO.setIssueID(rs.getString("issueID"));
					issueReportVO.setMemberID(rs.getString("memberID"));
					issueReportVO.setAdministratorID(rs.getString("administratorID"));
					issueReportVO.setIssueReportContent(rs.getString("issueReportContent"));
					issueReportVO.setIssueReportDate(rs.getTimestamp("issueReportDate"));
					issueReportVO.setIssueReportStatus(rs.getString("issueReportStatus"));
					issueReportVO.setIssueReportReason(rs.getString("issueReportReason"));
				}

				// Handle any driver errors
			} catch (SQLException se) {
				throw new RuntimeException("A database error occured. "
						+ se.getMessage());
				// Clean up JDBC resources
			} finally {
				if (pstmt != null) {
					try {
						pstmt.close();
					} catch (SQLException se) {
						se.printStackTrace(System.err);
					}
				}
				if (con != null) {
					try {
						con.close();
					} catch (Exception e) {
						e.printStackTrace(System.err);
					}
				}
			}
			return issueReportVO;
		}
		
		@Override
		public List<IssueReportVO> getAll() {
			List<IssueReportVO> list = new ArrayList<IssueReportVO>();
			IssueReportVO issueReportVO = null;
			
			Connection con = null;
			PreparedStatement pstmt = null;
			ResultSet rs = null;
			
			try {

				con = ds.getConnection();
				pstmt = con.prepareStatement(GET_ALL_STMT);
				rs = pstmt.executeQuery();

				while (rs.next()) {
					// empVO �]�٬� Domain objects
//issueReportID,issueID,memberID,administratorID,issueReportContent,issueReportDate,issueReportStatus,issueReportReason
					
					issueReportVO = new IssueReportVO();				
					issueReportVO.setIssueReportID(rs.getString("issueReportID"));
					issueReportVO.setIssueID(rs.getString("issueID"));
					issueReportVO.setMemberID(rs.getString("memberID"));
					issueReportVO.setAdministratorID(rs.getString("administratorID"));
					issueReportVO.setIssueReportContent(rs.getString("issueReportContent"));
					issueReportVO.setIssueReportDate(rs.getTimestamp("issueReportDate"));
					issueReportVO.setIssueReportStatus(rs.getString("issueReportStatus"));
					issueReportVO.setIssueReportReason(rs.getString("issueReportReason"));
					
					list.add(issueReportVO); // Store the row in the list
				}

				// Handle any driver errors
			} catch (SQLException se) {
				throw new RuntimeException("A database error occured. "
						+ se.getMessage());
				// Clean up JDBC resources
			} finally {
				if (pstmt != null) {
					try {
						pstmt.close();
					} catch (SQLException se) {
						se.printStackTrace(System.err);
					}
				}
				if (con != null) {
					try {
						con.close();
					} catch (Exception e) {
						e.printStackTrace(System.err);
					}
				}
			}
			return list;			
		}
		
		@Override
		public void deleteByIssueID(String issueID) {
			Connection con = null;
			PreparedStatement pstmt = null;

			try {

				con = ds.getConnection();
				pstmt = con.prepareStatement(DELETEBYISSUEID);

				pstmt.setString(1, issueID);
				pstmt.executeUpdate();

				// Handle any driver errors
			} catch (SQLException se) {
				throw new RuntimeException("A database error occured. "
						+ se.getMessage());
				// Clean up JDBC resources
			} finally {
				if (pstmt != null) {
					try {
						pstmt.close();
					} catch (SQLException se) {
						se.printStackTrace(System.err);
					}
				}
				if (con != null) {
					try {
						con.close();
					} catch (Exception e) {
						e.printStackTrace(System.err);
					}
				}
			}

			
		}

		@Override
		public Integer getIsReportCount() {
			Integer count = 0;
			Connection con = null;
			PreparedStatement pstmt = null;
			ResultSet rs = null;

			try {

				con = ds.getConnection();
				pstmt = con.prepareStatement(GET_ISREPORT_COUNT);

				rs = pstmt.executeQuery();

				while (rs.next()) {
					count= rs.getInt(1);
				}
			} catch (SQLException se) {
				throw new RuntimeException("A database error occured. " + se.getMessage());

			} finally {
				if (rs != null) {
					try {
						rs.close();
					} catch (SQLException se) {
						se.printStackTrace(System.err);
					}
				}
				if (pstmt != null) {
					try {
						pstmt.close();
					} catch (SQLException se) {
						se.printStackTrace(System.err);
					}
				}
				if (con != null) {
					try {
						con.close();
					} catch (Exception e) {
						e.printStackTrace(System.err);
					}
				}
			}

			return count;
		}
}
