package com.issueReport.model;

import java.sql.Timestamp;

public class IssueReportVO {
	private String issuereportID;
	private String issueID;
	private String memberID;
	private String administratorID;
	private String issueReportContent;
	private Timestamp issueReportDate;
	private String issueReportStatus;
	private String issueReportReason;
	
	public String getIssueReportID() {
		return issuereportID;
	}
	public void setIssueReportID(String issuereportID) {
		this.issuereportID = issuereportID;
	}
	public String getIssueID() {
		return issueID;
	}
	public void setIssueID(String issueID) {
		this.issueID = issueID;
	}
	public String getMemberID() {
		return memberID;
	}
	public void setMemberID(String memberID) {
		this.memberID = memberID;
	}
	public String getAdministratorID() {
		return administratorID;
	}
	public void setAdministratorID(String administratorID) {
		this.administratorID = administratorID;
	}
	public String getIssueReportContent() {
		return issueReportContent;
	}
	public void setIssueReportContent(String issueReportContent) {
		this.issueReportContent = issueReportContent;
	}
	public Timestamp getIssueReportDate() {
		return issueReportDate;
	}
	public void setIssueReportDate(Timestamp issueReportDate) {
		this.issueReportDate = issueReportDate;
	}
	public String getIssueReportStatus() {
		return issueReportStatus;
	}
	public void setIssueReportStatus(String issueReportStatus) {
		this.issueReportStatus = issueReportStatus;
	}
	public String getIssueReportReason() {
		return issueReportReason;
	}
	public void setIssueReportReason(String issueReportReason) {
		this.issueReportReason = issueReportReason;
	}
	
	
}
