package com.issueReport.model;

import java.sql.Timestamp;
import java.util.List;

public class IssueReportService {
	
	private IssueReportDAO_interface issueReportDAO;
	
	public IssueReportService() {
		issueReportDAO = new IssueReportDAO();
	}
	
	public IssueReportVO addIssueReport(String issueID,String memberID,String administratorID,String issueReportContent,Timestamp issueReportDate,String issueReportStatus,String issueReportReason) {
		
		IssueReportVO issueReportVO = new IssueReportVO();
		
		issueReportVO.setIssueID(issueID);
		issueReportVO.setMemberID(memberID);
		issueReportVO.setAdministratorID(administratorID);
		issueReportVO.setIssueReportContent(issueReportContent);
		issueReportVO.setIssueReportDate(issueReportDate);
		issueReportVO.setIssueReportStatus(issueReportStatus);
		issueReportVO.setIssueReportReason(issueReportReason);
		
		issueReportDAO.insert(issueReportVO);
		
		return issueReportVO;
	}
	
	public IssueReportVO updateIssueReport(String issueReportID,String issueReportStatus) {
		
		IssueReportVO issueReportVO = new IssueReportVO();
		
		issueReportVO.setIssueReportID(issueReportID);
		
		issueReportVO.setIssueReportStatus(issueReportStatus);
		
		return issueReportVO;
	}
	
	public void deleteIssueReport(String issueReportID) {		
		issueReportDAO.delete(issueReportID);
	}
	
	public IssueReportVO getOneIssueReport(String issueReportID) {		
		return issueReportDAO.findByPrimaryKey(issueReportID);		
	}
	
	public List<IssueReportVO> getAll(){
		return issueReportDAO.getAll();
		
	}
	
	public void deleteByIssueID(String issueReportID) {
		issueReportDAO.deleteByIssueID(issueReportID);
	}
	
	public Integer getISRepoetCount(){
		return issueReportDAO.getIsReportCount();
	}	
}
