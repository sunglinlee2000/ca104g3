package com.issueReport.model;

import java.util.List;


public interface IssueReportDAO_interface {
	public void insert(IssueReportVO issueReportVO);
    public void update(IssueReportVO issueReportVO);
    public void delete(String issueReportID);
    public IssueReportVO findByPrimaryKey(String issueReportID);
    public List<IssueReportVO> getAll();
	public void deleteByIssueID(String issueReportID);
	//計算話題檢舉之狀態未審核的筆數
	public Integer getIsReportCount();
}
