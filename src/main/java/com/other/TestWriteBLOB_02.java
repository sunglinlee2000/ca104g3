package com.other;

import java.sql.*;

public class TestWriteBLOB_02 {
	private static final String URL = "jdbc:oracle:thin:@localhost:1521:xe";
	private static final String USER = "CA104G3";
	private static final String PASSWORD = "123456";
	private static final String GET_ALL = "SELECT * FROM ADFUND";

	
	public static void main(String[] args) {
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		ResultSetMetaData rsmd = null;
		
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
			con = DriverManager.getConnection(URL, USER, PASSWORD);			
			pstmt = con.prepareStatement(GET_ALL);
			rs = pstmt.executeQuery();
			rsmd = rs.getMetaData();
			
			int numberOfColumns = rsmd.getColumnCount();
			
			for (int i = 1; i <= numberOfColumns; i++) {
				String colName = rsmd.getColumnName(i);
				System.out.println(colName);
				String tableName = rsmd.getTableName(i);
				System.out.println(tableName);
				String colTypeName = rsmd.getColumnTypeName(i);
				System.out.println(colTypeName);
				System.out.println("....");
							
			}
			
		} catch(ClassNotFoundException ce) {
			ce.printStackTrace();
		} catch(SQLException se) {
			se.printStackTrace();
		} finally {
			if (rs != null) {
				try {
					rs.close();
				}catch(SQLException se) {
					se.printStackTrace();
				}
			}
			
			if (pstmt != null) {
				try {
					pstmt.close();
				}catch(SQLException se) {
					se.printStackTrace();
				}
			}
			
			if (con != null) {
				try {
					con.close();
				}catch(SQLException se) {
					se.printStackTrace();
				}
			}
		}
	}
}
