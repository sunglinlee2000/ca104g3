package com.other;

import java.io.*;
import java.sql.*;

public class TestWriteBLOB {
	private static final String URL = "jdbc:oracle:thin:@localhost:1521:xe";
	private static final String USER = "CA104G3";
	private static final String PASSWORD = "123456";
	private static final String SQL = "UPDATE ADFUND SET ADFUNDHOMEPHOTO = ? WHERE ADFUNDID = ?";

	public static void main(String[] args) {
		Connection con = null;
		PreparedStatement pstmt = null;

		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
			con = DriverManager.getConnection(URL, USER, PASSWORD);
			pstmt = con.prepareStatement(SQL);

			// 2. setBytes
			for(int i = 1; i <= 5; i++) {
			byte[] pic = getPictureByteArray("img/"+ i + ".jpg");
			pstmt.setBytes(1, pic);
			pstmt.setString(2, ("AD00000" + i));
			pstmt.executeUpdate();
						
			System.out.println("新增第" + i + "張圖");
			}

		} catch (ClassNotFoundException ce) {
			System.out.println(ce);
		} catch (SQLException se) {
			System.out.println(se);
		} catch (IOException ie) {
			System.out.println(ie);
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					System.out.println(se);
				}
			}

			if (con != null) {
				try {
					con.close();
				} catch (SQLException se) {
					System.out.println(se);
				}
			}
		}
	
	}
	// �ϥ�byte[]�覡
		public static byte[] getPictureByteArray(String path) throws IOException {
			
			File file = new File(path);
			FileInputStream fis = new FileInputStream(file);
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			byte[] buffer = new byte[8192];
			int i;
			while ((i = fis.read(buffer)) != -1) {
				baos.write(buffer, 0, i);
			}
			baos.close();
			fis.close();
			
			return baos.toByteArray();
		}
	
	

}
