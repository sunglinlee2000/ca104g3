package com.other;

import java.io.*;
import java.sql.*;

public class TestWriteBLOB_01 {
	private static final String URL = "jdbc:oracle:thin:@localhost:1521:xe";
	private static final String USER = "CA104G3";
	private static final String PASSWORD = "123456";
	private static final String UPDATE = "UPDATE ADFUND SET ADFUNDHOMEPHOTO = ? WHERE ADFUNDID = ?";
	private static final String GET_ALL = "SELECT ? FROM ADFUND";

	public static void main(String[] args) {

		// Test
//		File file = new File("img");
//		String path = file.getPath();
//		System.out.println(path);
//
//		String aPath = file.getAbsolutePath();
//		System.out.println(aPath);
//
//		String[] fileList = file.list();
//
//		for (String fileListName : fileList) {
//			System.out.println(fileListName);
//			System.out.println("......");
//		}
//
//		File[] filesList = file.listFiles();
//		for (File fileListsName : filesList) {
//			System.out.println(fileListsName);			
//			System.out.println(fileListsName.getName());
//			int node =  fileListsName.getName().indexOf(".");
//			System.out.println(node);
//			System.out.println(fileListsName.getName().substring(0, node));
//			System.out.println("......");
//		}

		File file = new File("img");
		Connection con = null;
		PreparedStatement pstmt = null;

		try {
			// �ק�
//			Class.forName("oracle.jdbc.driver.OracleDriver");
//			con = DriverManager.getConnection(URL, USER, PASSWORD);
//			pstmt = con.prepareStatement(UPDATE);
//
//			//setBytes
//			File[] filesList = file.listFiles();
//			for (File fileListsName : filesList) {				
//					String fileListsNameStr = fileListsName.getAbsolutePath();
//					byte[] pic = getPictureByteArray(fileListsNameStr);
//					pstmt.setBytes(1, pic);
//					
//					int node =  fileListsName.getName().indexOf(".");
//					String fName = fileListsName.getName().substring(0, node);
//					pstmt.setString(2, ("AD00000" + fName));
//					
//					pstmt.executeUpdate();
//
//					System.out.println("�s�W���\" + "��" + fName + "�i");
//				}

			
			Class.forName("oracle.jdbc.driver.OracleDriver");
			con = DriverManager.getConnection(URL, USER, PASSWORD);
			pstmt = con.prepareStatement(GET_ALL);

			pstmt.setInt(1, 1);

			pstmt.executeQuery();
			
			System.out.println("...");

		} catch (ClassNotFoundException ce) {
			System.out.println(ce);
		} catch (SQLException se) {
			System.out.println(se);
		}
//		catch (IOException ie) {
//			System.out.println(ie);
//		} 
		finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					System.out.println(se);
				}
			}

			if (con != null) {
				try {
					con.close();
				} catch (SQLException se) {
					System.out.println(se);
				}
			}
		}
	}

	public static byte[] getPictureByteArray(String path) throws IOException {

		File file = new File(path);
		FileInputStream fis = new FileInputStream(file);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		byte[] buffer = new byte[8192];
		int i;
		while ((i = fis.read(buffer)) != -1) {
			baos.write(buffer, 0, i);
		}
		baos.close();
		fis.close();

		return baos.toByteArray();
	}

}
