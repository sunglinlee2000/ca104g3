package com.SNSReport.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Timestamp;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;

import com.SNSReport.model.SNSReportService;
import com.SNSReport.model.SNSReportVO;
import com.SNSTranslated.model.SNSTranslatedService;
import com.SNSTranslated.model.SNSTranslatedVO;
import com.notificationList.model.NotificationListService;
import com.notificationList.model.NotificationListVO;

public class SNSReportServlet extends HttpServlet{
	private static final long serialVersionUID = 1L;
	public void doGet(HttpServletRequest req,HttpServletResponse res) 
			throws ServletException,IOException{
		doPost(req,res);
	}
	public void doPost(HttpServletRequest req,HttpServletResponse res) 
				throws ServletException,IOException {
		req.setCharacterEncoding("UTF-8");
		res.setContentType("text/html; charset=UTF-8");
		String action = req.getParameter("action");

		
		
		
		if ("insert".equals(action)) { 
				System.out.println("進入新增檢舉");
				String snsTranslatedID = req.getParameter("snsTranslatedID");
				System.out.println(snsTranslatedID);
				String memberID = req.getParameter("memberID");
				System.out.println(memberID);
				String snsReportReason = req.getParameter("snsReportReason");
				System.out.println(snsReportReason);
				String snsReportInfo = req.getParameter("snsTranslatedContent");
				String boardID = req.getParameter("boardID");
				
				JSONObject obj = new JSONObject();				

				SNSReportVO snsReportVO = new SNSReportVO();
				snsReportVO.setSnsTranslatedID(snsTranslatedID);
				snsReportVO.setMemberID(memberID);
				snsReportVO.setSnsReportReason(snsReportReason);

				
				/***************************2.開始新增資料***************************************/
				SNSReportService snsReportSvc = new SNSReportService();
				snsReportVO = snsReportSvc.addReport(snsTranslatedID, memberID, snsReportInfo, snsReportReason,boardID);
				
				res.setContentType("text/plain");
				res.setCharacterEncoding("UTF-8");
				PrintWriter out = res.getWriter();
				out.write(obj.toString());
				out.flush();
				out.close();
		}		
	
	
		if("updateStatus".equals(action)) {
				String snsTranslatedID = req.getParameter("snsTranslatedID");
				System.out.println("進入修改檢舉");
				String snsReportID = req.getParameter("snsReportID");
				System.out.println("snsReportID"+snsReportID);
				String snsReportStatus = req.getParameter("snsReportStatus");
				System.out.println("snsReportStatus"+snsReportStatus);
				
				String boardID = req.getParameter("boardID");
				
				String mmemberIDRepoter = req.getParameter("memberID");
				
				JSONObject obj = new JSONObject();	
				
				SNSReportVO snsReportVO = new SNSReportVO();
				snsReportVO.setSnsReportID(snsReportID);
				snsReportVO.setSnsReportStatus(snsReportStatus);
				
				SNSReportService snsReportSvc = new SNSReportService();
				SNSTranslatedService snsTranslatedSvc = new SNSTranslatedService();
				
				snsReportSvc.update(snsReportStatus, snsReportID);
				
				if(snsReportStatus.equals("REPORT_ACCEPTED")) {
					String snsTranslatedStatus = "pass";
					snsTranslatedSvc.updateSNSTranslatedStatus(snsTranslatedID, snsTranslatedStatus);
					System.out.println("文章已更改狀態");
					//===========================查詢原文會員=========================================
					SNSTranslatedVO snsTranslatedVO = snsTranslatedSvc.getOneSNSTranslated(snsTranslatedID);
					String memberID = snsTranslatedVO.getMemberID();
					
					NotificationListService NoSvc = new NotificationListService();
					NotificationListVO NoVO = new NotificationListVO();		
					
					String notificationtypeid = "NT000001";
					
					String notificationcontent = "您的翻譯被檢舉已審核通過，翻譯已被系統刪除";
					String notificationcontent1 = "您的檢舉已審核通過，翻譯已被系統刪除";
					
					Timestamp notificationdate = new java.sql.Timestamp (System.currentTimeMillis());
					
					String notificationtarget = boardID ;
					
					String notificationclick = "FALSE";
					
					String notificationpath = "/front-end/SNS/SNS.do?action=showOne&boardID=";		
					
					
					NoSvc.addNoList(notificationtypeid, memberID, notificationcontent, notificationdate, notificationtarget, notificationclick,notificationpath);
					NoSvc.addNoList(notificationtypeid, mmemberIDRepoter, notificationcontent1, notificationdate, notificationtarget, notificationclick,notificationpath);
	//=============================================================================
				}else {
					String snsTranslatedStatus = "";
					snsTranslatedSvc.updateSNSTranslatedStatus(snsTranslatedID, snsTranslatedStatus);
					System.out.println("文章已更改狀態");
				}
				

				
				res.setContentType("text/plain");
				res.setCharacterEncoding("UTF-8");
				PrintWriter out = res.getWriter();
				out.write(obj.toString());
				out.flush();
				out.close();
		}
	
	
	
	
	}
}
