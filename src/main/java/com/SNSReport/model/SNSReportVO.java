package com.SNSReport.model;
import java.sql.Timestamp;

public class SNSReportVO implements java.io.Serializable{
	private String snsReportID;
	private String snsTranslatedID;
	private String memberID;
	private String administratorID;
	private String snsReportInfo;
	private Timestamp snsReportDate;
	private String snsReportStatus;
	private String snsReportReason;
	private String boardID;
	
	public String getSnsReportID() {
		return snsReportID;
	}
	public void setSnsReportID(String snsReportID) {
		this.snsReportID = snsReportID;
	}
	public String getSnsTranslatedID() {
		return snsTranslatedID;
	}
	public void setSnsTranslatedID(String snsTranslatedID) {
		this.snsTranslatedID = snsTranslatedID;
	}
	public String getMemberID() {
		return memberID;
	}
	public void setMemberID(String memberID) {
		this.memberID = memberID;
	}
	public String getAdministratorID() {
		return administratorID;
	}
	public void setAdministratorID(String administratorID) {
		this.administratorID = administratorID;
	}
	public String getSnsReportInfo() {
		return snsReportInfo;
	}
	public void setSnsReportInfo(String snsReportInfo) {
		this.snsReportInfo = snsReportInfo;
	}
	public Timestamp getSnsReportDate() {
		return snsReportDate;
	}
	public void setSnsReportDate(Timestamp snsReportDate) {
		this.snsReportDate = snsReportDate;
	}
	public String getSnsReportStatus() {
		return snsReportStatus;
	}
	public void setSnsReportStatus(String snsReportStatus) {
		this.snsReportStatus = snsReportStatus;
	}
	public String getSnsReportReason() {
		return snsReportReason;
	}
	public void setSnsReportReason(String snsReportReason) {
		this.snsReportReason = snsReportReason;
	}
	public String getBoardID() {
		return boardID;
	}
	public void setBoardID(String boardID) {
		this.boardID = boardID;
	}

}
