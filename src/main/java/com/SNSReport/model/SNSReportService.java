package com.SNSReport.model;

import java.sql.Timestamp;
import java.util.List;



public class SNSReportService {
	
	private SNSReportDAO_interface dao;
	
	public SNSReportService() {
		dao = new SNSReportDAO();
	}
	
	public SNSReportVO addReport(String snsTranslatedID,String memberID,
			String snsReportInfo,String snsReportReason,String boardID) {
		SNSReportVO snsReportVO = new SNSReportVO();

		snsReportVO.setSnsTranslatedID(snsTranslatedID);
		snsReportVO.setMemberID(memberID);
		snsReportVO.setSnsReportInfo(snsReportInfo);
		snsReportVO.setSnsReportReason(snsReportReason);
		snsReportVO.setBoardID(boardID);
		
		dao.insert(snsReportVO);
				
		return snsReportVO;
	}
	
	public SNSReportVO update(String snsReportStatus,String snsReportID) {
		SNSReportVO snsReportVO = new SNSReportVO();
		
		snsReportVO.setSnsReportStatus(snsReportStatus);
		snsReportVO.setSnsReportID(snsReportID);


		dao.update(snsReportVO);
		
		return snsReportVO;
	}
	
	
	public SNSReportVO findReport(String snsReportID) {
		return dao.findByPrimaryKey(snsReportID);
	}
	
	public List<SNSReportVO> getAll() {
		return dao.getAll();
	}
}
