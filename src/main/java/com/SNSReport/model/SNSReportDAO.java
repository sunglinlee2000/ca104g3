package com.SNSReport.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

public class SNSReportDAO implements SNSReportDAO_interface{
	private static DataSource ds = null;
	static {
		try {
			Context ctx = new InitialContext();
			ds = (DataSource) ctx.lookup("java:comp/env/jdbc/CA104G3DB");
		} catch (NamingException e) {
			e.printStackTrace();
		}
	}
	private static final String INSERT_STMT = "INSERT INTO SNSReport VALUES('SNSR'||LPAD(TO_CHAR(snsreport_seq.NEXTVAL),6,0),?,?,'A000001',?,(CURRENT_TIMESTAMP),'REPORT_UNCHECKED',?,?)";
	private static final String UPDATE = "UPDATE SNSReport SET snsReportStatus=? WHERE snsReportID=?";
	private static final String GET_ONE_STMT = "SELECT * FROM SNSReport WHERE snsReportID=?";
	private static final String GET_ALL_STMT = "SELECT * FROM SNSReport ORDER BY snsReportID";
	
	
	@Override
	public void insert(SNSReportVO snsReportVO) {
		
		Connection con = null;
		PreparedStatement pstmt = null;
		
		try {
			
			con = ds.getConnection();
			pstmt = con.prepareStatement(INSERT_STMT);
			
			pstmt.setString(1,snsReportVO.getSnsTranslatedID());
			pstmt.setString(2,snsReportVO.getMemberID());
			pstmt.setString(3,snsReportVO.getSnsReportInfo());
			pstmt.setString(4,snsReportVO.getSnsReportReason());
			pstmt.setString(5, snsReportVO.getBoardID());
			
			pstmt.executeUpdate();			
			
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured." + se.getMessage());
		
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if(con != null) {
				try {
					con.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
		}
	}

	@Override
	public void update(SNSReportVO snsReportVO) {
		
		Connection con = null;
		PreparedStatement pstmt = null;
		
		try {
			
			con =ds.getConnection();
			pstmt = con.prepareStatement(UPDATE);
			
			
			pstmt.setString(1, snsReportVO.getSnsReportStatus());
			pstmt.setString(2, snsReportVO.getSnsReportID());
			
			pstmt.executeUpdate();	
			
			
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured." + se.getMessage());
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
	}

	@Override
	public SNSReportVO findByPrimaryKey(String snsReportID) {
		SNSReportVO snsReportVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ONE_STMT);

			pstmt.setString(1, snsReportID);

			rs = pstmt.executeQuery();

			while (rs.next()) {
				snsReportVO = new SNSReportVO();
				snsReportVO.setSnsReportID(rs.getString("snsReportID"));
				snsReportVO.setSnsTranslatedID(rs.getString("snsID"));
				snsReportVO.setMemberID(rs.getString("memberID"));
				snsReportVO.setAdministratorID(rs.getString("administratorID"));
				snsReportVO.setSnsReportInfo(rs.getString("snsReportInfo"));
				snsReportVO.setSnsReportDate(rs.getTimestamp("snsReportDate"));
				snsReportVO.setSnsReportStatus(rs.getString("snsReportStatus"));
				snsReportVO.setSnsReportReason(rs.getString("snsReportReason"));
				snsReportVO.setBoardID(rs.getString("boardID"));
			}

		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return snsReportVO;
	}

	@Override
	public List<SNSReportVO> getAll() {
		List<SNSReportVO> list = new ArrayList<SNSReportVO>();
		SNSReportVO snsReportVO = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ALL_STMT);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				snsReportVO = new SNSReportVO();
				snsReportVO.setSnsReportID(rs.getString("snsReportID"));
				snsReportVO.setSnsTranslatedID(rs.getString("snsTranslatedID"));
				snsReportVO.setMemberID(rs.getString("memberID"));
				snsReportVO.setAdministratorID(rs.getString("administratorID"));
				snsReportVO.setSnsReportInfo(rs.getString("snsReportInfo"));
				snsReportVO.setSnsReportDate(rs.getTimestamp("snsReportDate"));
				snsReportVO.setSnsReportStatus(rs.getString("snsReportStatus"));
				snsReportVO.setSnsReportReason(rs.getString("snsReportReason"));
				snsReportVO.setBoardID(rs.getString("boardID"));

				list.add(snsReportVO); // Store the row in the list
			}

		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return list;
	}

}
