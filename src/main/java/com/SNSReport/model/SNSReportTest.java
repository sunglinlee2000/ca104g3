package com.SNSReport.model;

import java.util.List;

public class SNSReportTest {
	
	public static void main(String[] args) {
		SNSReportJDBCDAO snsReportJDBCDAO = new SNSReportJDBCDAO();
//		1.------------------------
//		SNSReportVO snsReportVO = new SNSReportVO();
//		
//		snsReportVO.setSnsID("SNS000001");
//		snsReportVO.setMemberID("M000001");
//		snsReportVO.setAdministratorID("A000001");
//		snsReportVO.setSnsReportInfo("嘿嘿");
//		snsReportVO.setSnsReportDate(java.sql.Timestamp.valueOf("2018-10-22 00:00:00"));
//		snsReportVO.setSnsReportStatus("REPORT_UNCHECKED");
//		snsReportVO.setSnsReportReason("OTHER");
//		snsReportJDBCDAO.insert(snsReportVO);
//		
//		System.out.println("OK");
		
//		2.------------------------
//		SNSReportVO snsReportVO2 = new SNSReportVO();
//		
//		snsReportVO2.setSnsReportID("SNSR000003");
//		snsReportVO2.setSnsID("SNS000001");
//		snsReportVO2.setMemberID("M000001");
//		snsReportVO2.setAdministratorID("A000001");
//		snsReportVO2.setSnsReportInfo("嘿嘿嘿");
//		snsReportVO2.setSnsReportDate(java.sql.Timestamp.valueOf("2018-10-22 00:00:00"));
//		snsReportVO2.setSnsReportStatus("REPORT_UNCHECKED");
//		snsReportVO2.setSnsReportReason("OTHER");
//		snsReportJDBCDAO.update(snsReportVO2);
//		
//		System.out.println("OK");
		
//		3.---------------------
//		SNSReportVO snsReportVO3 = snsReportJDBCDAO.findByPrimaryKey("SNSR000003");
//		System.out.println(snsReportVO3.getSnsReportID());
//		System.out.println(snsReportVO3.getSnsID());
//		System.out.println(snsReportVO3.getMemberID());
//		System.out.println(snsReportVO3.getAdministratorID());
//		System.out.println(snsReportVO3.getSnsReportInfo());
//		System.out.println(snsReportVO3.getSnsReportDate());
//		System.out.println(snsReportVO3.getSnsReportStatus());
//		System.out.println(snsReportVO3.getSnsReportReason());
//		System.out.println("==================================");
//		System.out.println("OK");
		
//		4.---------------------
		List<SNSReportVO> list = snsReportJDBCDAO.getAll();
		for (SNSReportVO snsReportVO4 : list) {
			System.out.println(snsReportVO4.getSnsReportID());
//			System.out.println(snsReportVO4.getSnsID());
			System.out.println(snsReportVO4.getMemberID());
			System.out.println(snsReportVO4.getAdministratorID());
			System.out.println(snsReportVO4.getSnsReportInfo());
			System.out.println(snsReportVO4.getSnsReportDate());
			System.out.println(snsReportVO4.getSnsReportStatus());
			System.out.println(snsReportVO4.getSnsReportReason());
			System.out.println("==================================");
		}
		
		System.out.println("OK");
		
	}

}
