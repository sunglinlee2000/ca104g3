package com.SNSReport.model;
import java.util.List;

public interface SNSReportDAO_interface {
		public void insert(SNSReportVO snsReportVO);
		public void update(SNSReportVO snsReportVO);
		public SNSReportVO findByPrimaryKey(String snsReportID);
		public List<SNSReportVO> getAll();
}
