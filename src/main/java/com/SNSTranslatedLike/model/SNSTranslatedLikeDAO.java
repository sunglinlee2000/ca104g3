package com.SNSTranslatedLike.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

public class SNSTranslatedLikeDAO implements SNSTranslatedLikeDAO_interface{
	private static DataSource ds = null;
	static {
		try {
			Context ctx = new InitialContext();
			ds = (DataSource) ctx.lookup("java:comp/env/jdbc/CA104G3DB");
		} catch (NamingException e) {
			e.printStackTrace();
		}
	}
	private static final String INSERT_STMT = "INSERT INTO SNSTranslatedLike(snsTranslatedID,memberID)VALUES"
			+ "(?,?)";
	private static final String DELETE = "DELETE FROM SNSTranslatedLike WHERE snsTranslatedID=? AND memberID=?";
	private static final String GET_ONE_STMT = "SELECT * FROM SNSTranslatedLike WHERE snsTranslatedID=? AND memberID=?";
	private static final String GET_ALL_STMT = "SELECT * FROM SNSTranslatedLike WHERE snsTranslatedID=?";
	
	@Override
	public void insert(SNSTranslatedLikeVO snsTranslatedLikeVO) {
		
		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(INSERT_STMT);

			pstmt.setString(1, snsTranslatedLikeVO.getSnsTranslatedID());
			pstmt.setString(2, snsTranslatedLikeVO.getMemberID());
			
			pstmt.executeUpdate();

		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
	}

	@Override
	public void delete(String snsTranslatedID,String memberID) {
		

		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(DELETE);
			
			pstmt.setString(1, snsTranslatedID);
			pstmt.setString(2, memberID);

			pstmt.executeUpdate();

		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
	}

	@Override
	public SNSTranslatedLikeVO findByPrimaryKey(String snsTranslatedID, String memberID) {
		
		SNSTranslatedLikeVO snsTranslatedLikeVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		try {
			
			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ONE_STMT);
			
			pstmt.setString(1, snsTranslatedID);
			pstmt.setString(2, memberID);
			
			rs = pstmt.executeQuery();
			
			while (rs.next()) {
				snsTranslatedLikeVO = new SNSTranslatedLikeVO();
				snsTranslatedLikeVO.setSnsTranslatedID(rs.getString("snsTranslatedID"));
				snsTranslatedLikeVO.setMemberID(rs.getString("memberID"));
			}
			
			
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured." + se.getMessage());
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			} 
			if (con != null) {
				try {
					con.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
		}
		return snsTranslatedLikeVO;
	}

	@Override
	public List<SNSTranslatedLikeVO> getAll(String snsTranslatedID) {
		List<SNSTranslatedLikeVO> list = new ArrayList<SNSTranslatedLikeVO>();
		SNSTranslatedLikeVO snsTranslatedLikeVO = null;
		
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		try {
			
			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ALL_STMT);
			
			pstmt.setString(1, snsTranslatedID);
			
			rs = pstmt.executeQuery();
			
			while (rs.next()) {
				snsTranslatedLikeVO = new SNSTranslatedLikeVO();
				snsTranslatedLikeVO.setSnsTranslatedID(rs.getString("snsTranslatedID"));
				snsTranslatedLikeVO.setMemberID(rs.getString("memberID"));

				list.add(snsTranslatedLikeVO);
			}
			
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured." + se.getMessage());
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			} 
			if (con != null) {
				try {
					con.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
		}
		return list;
	}
}
