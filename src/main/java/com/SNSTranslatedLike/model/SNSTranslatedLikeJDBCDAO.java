package com.SNSTranslatedLike.model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


public class SNSTranslatedLikeJDBCDAO implements SNSTranslatedLikeDAO_interface{
	String driver = "oracle.jdbc.driver.OracleDriver";
	String url = "jdbc:oracle:thin:@localhost:1521:XE";
	String userid = "CA104G3";
	String passwd = "123456";
	
	private static final String INSERT_STMT = "INSERT INTO SNSTranslatedLike(snsTranslatedID,memberID)VALUES"
			+ "(?,?)";
	private static final String DELETE = "DELETE FROM SNSTranslatedLike WHERE snsTranslatedID=? AND memberID=?";
	private static final String GET_ONE_STMT = "SELECT * FROM SNSTranslatedLike WHERE snsTranslatedID=? AND memberID=?";
	private static final String GET_ALL_STMT = "SELECT * FROM SNSTranslatedLike";
	
	@Override
	public void insert(SNSTranslatedLikeVO snsTranslatedLikeVO) {
		
		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(INSERT_STMT);

			pstmt.setString(1, snsTranslatedLikeVO.getSnsTranslatedID());
			pstmt.setString(2, snsTranslatedLikeVO.getMemberID());
			
			pstmt.executeUpdate();

		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
	}

	@Override
	public void delete(String snsTranslatedID,String memberID) {
		

		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(DELETE);
			
			pstmt.setString(1, snsTranslatedID);
			pstmt.setString(2, memberID);

			pstmt.executeUpdate();

		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
	}

	@Override
	public SNSTranslatedLikeVO findByPrimaryKey(String snsTranslatedID, String memberID) {
		
		SNSTranslatedLikeVO snsTranslatedLikeVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		try {
			
			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(GET_ONE_STMT);
			
			pstmt.setString(1, snsTranslatedID);
			pstmt.setString(2, memberID);
			
			rs = pstmt.executeQuery();
			
			while (rs.next()) {
				snsTranslatedLikeVO = new SNSTranslatedLikeVO();
				snsTranslatedLikeVO.setSnsTranslatedID(rs.getString("snsTranslatedID"));
				snsTranslatedLikeVO.setMemberID(rs.getString("memberID"));
			}
			
			
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver."+ e.getMessage());
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured." + se.getMessage());
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			} 
			if (con != null) {
				try {
					con.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
		}
		return snsTranslatedLikeVO;
	}

	@Override
	public List<SNSTranslatedLikeVO> getAll(String snsTranslatedID) {
		List<SNSTranslatedLikeVO> list = new ArrayList<SNSTranslatedLikeVO>();
		SNSTranslatedLikeVO snsTranslatedLikeVO = null;
		
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		try {
			
			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(GET_ALL_STMT);
			rs = pstmt.executeQuery();
			
			while (rs.next()) {
				snsTranslatedLikeVO = new SNSTranslatedLikeVO();
				snsTranslatedLikeVO.setSnsTranslatedID(rs.getString("snsTranslatedID"));
				snsTranslatedLikeVO.setMemberID(rs.getString("memberID"));

				list.add(snsTranslatedLikeVO);
			}
			
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver."+ e.getMessage());
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured." + se.getMessage());
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			} 
			if (con != null) {
				try {
					con.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
		}
		return list;
	}

}
