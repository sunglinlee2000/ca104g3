package com.SNSTranslatedLike.model;
import java.util.List;

public interface SNSTranslatedLikeDAO_interface {
		public void insert(SNSTranslatedLikeVO snsTranslatedLikeVO);
		public void delete(String snsTranslatedID,String memberID);
		public SNSTranslatedLikeVO findByPrimaryKey(String snsTranslatedID,String memberID);
		public List<SNSTranslatedLikeVO> getAll(String snsTranslatedID);
}
