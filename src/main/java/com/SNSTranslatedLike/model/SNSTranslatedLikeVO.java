package com.SNSTranslatedLike.model;

public class SNSTranslatedLikeVO implements java.io.Serializable{
	private String snsTranslatedID;
	private String memberID;
	
	public String getMemberID() {
		return memberID;
	}
	public void setMemberID(String memberID) {
		this.memberID = memberID;
	}
	public String getSnsTranslatedID() {
		return snsTranslatedID;
	}
	public void setSnsTranslatedID(String snsTranslatedID) {
		this.snsTranslatedID = snsTranslatedID;
	}
	

}
