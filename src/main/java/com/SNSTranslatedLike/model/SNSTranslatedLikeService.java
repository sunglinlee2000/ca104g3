package com.SNSTranslatedLike.model;

import java.util.List;


public class SNSTranslatedLikeService {

	private SNSTranslatedLikeDAO_interface dao;
	
	public SNSTranslatedLikeService() {
		dao = new SNSTranslatedLikeDAO();
	}
	
	
	public SNSTranslatedLikeVO addSNSTranslatedLike(String snsTranslatedID,String memberID) {
		SNSTranslatedLikeVO snsTranslatedLikeVO = new SNSTranslatedLikeVO();
		
		snsTranslatedLikeVO.setSnsTranslatedID(snsTranslatedID);
		snsTranslatedLikeVO.setMemberID(memberID);
		dao.insert(snsTranslatedLikeVO);
		
		return snsTranslatedLikeVO;
	}
	
	
	public void deleteSNSTranslatedLike(String snsTranslatedID,String memberID) {
		dao.delete(snsTranslatedID,memberID);
	}
	
	public SNSTranslatedLikeVO getOneSNSTranslatedLike(String snsTranslatedID,String memberID) {
		return dao.findByPrimaryKey(snsTranslatedID,memberID);
	}
	
	
	public List<SNSTranslatedLikeVO> getAllSNSTranslatedLike(String snsTranslatedID){
		return dao.getAll(snsTranslatedID);
	}


}
