package com.SNSTranslatedLike.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.swing.plaf.synth.SynthSeparatorUI;

import org.json.JSONException;
import org.json.JSONObject;

import com.SNSTranslatedLike.model.SNSTranslatedLikeService;
import com.SNSTranslatedLike.model.SNSTranslatedLikeVO;
import com.member.model.MemberService;
import com.member.model.MemberVO;

public class SNSTranslatedLikeServlet extends HttpServlet{
	private static final long serialVersionUID = 1L;
	public void doGet(HttpServletRequest req,HttpServletResponse res) 
			throws ServletException,IOException{
		doPost(req,res);
	}
	public void doPost(HttpServletRequest req,HttpServletResponse res) 
				throws ServletException,IOException {
		req.setCharacterEncoding("UTF-8");
		res.setContentType("text/html; charset=UTF-8");
		String action = req.getParameter("action");
		
if ("insert".equals(action)) { 
				System.out.println("進入點讚");
				String snsTranslatedID = req.getParameter("snsTranslatedID");
				System.out.println(snsTranslatedID);
				String memberID = req.getParameter("memberID");
				System.out.println(memberID);
				
				
				JSONObject obj = new JSONObject();				

				SNSTranslatedLikeService snsTranslatedLikeSvc = new SNSTranslatedLikeService();
				SNSTranslatedLikeVO snsTranslatedLikeVO = snsTranslatedLikeSvc.getOneSNSTranslatedLike(snsTranslatedID, memberID);
				
				if(snsTranslatedLikeVO==null) {
				
				SNSTranslatedLikeVO snsTranslatedLikeVO1 = new SNSTranslatedLikeVO();
				snsTranslatedLikeVO1.setSnsTranslatedID(snsTranslatedID);
				snsTranslatedLikeVO1.setMemberID(memberID);
				
				snsTranslatedLikeVO1 = snsTranslatedLikeSvc.addSNSTranslatedLike(snsTranslatedID, memberID);
				
				HttpSession session = req.getSession();
				MemberService memberSvc = new MemberService();
				MemberVO memberVO = (MemberVO)memberSvc.getOneMember(memberID);
				session.setAttribute("memberVO", memberVO);
				
				Boolean checked = true;
				
				try {
					obj.accumulate("checked", checked);
				} catch (JSONException e) {
					e.printStackTrace();
				}
				
				res.setContentType("text/plain");
				res.setCharacterEncoding("UTF-8");
				PrintWriter out = res.getWriter();
				out.write(obj.toString());
				out.flush();
				out.close();
				} else {
					System.out.println("已點讚");
				}
		}	

if ("delete".equals(action)) { 
	
			String snsTranslatedID = req.getParameter("snsTranslatedID");
			System.out.println(snsTranslatedID);
			String memberID = req.getParameter("memberID");
			System.out.println(memberID);
			
			
			JSONObject obj = new JSONObject();				
		
			SNSTranslatedLikeService snsTranslatedLikeSvc = new SNSTranslatedLikeService();
			snsTranslatedLikeSvc.deleteSNSTranslatedLike(snsTranslatedID, memberID);
			
			
			Boolean checked = false;
			
			try {
				obj.accumulate("checked", checked);
			} catch (JSONException e) {
				e.printStackTrace();
			}
			
			res.setContentType("text/plain");
			res.setCharacterEncoding("UTF-8");
			PrintWriter out = res.getWriter();
			out.write(obj.toString());
			out.flush();
			out.close();
			} 

if ("checkLike".equals(action)) { 
	
			System.out.println("進入檢查點讚");
			String snsTranslatedID = req.getParameter("snsTranslatedID");
			System.out.println(snsTranslatedID);
			String memberID = req.getParameter("memberID");
			System.out.println(memberID);
			
			
			JSONObject obj = new JSONObject();				
		
			SNSTranslatedLikeService snsTranslatedLikeSvc = new SNSTranslatedLikeService();
			SNSTranslatedLikeVO snsTranslatedLikeVO = snsTranslatedLikeSvc.getOneSNSTranslatedLike(snsTranslatedID, memberID);
			
			if(snsTranslatedLikeVO==null) {
			
				Boolean checked = false;

				try {
					obj.put("checked", checked);
					System.out.println("checked="+checked);
				} catch (JSONException e) {
					e.printStackTrace();
				}
			
			} else {
				
				Boolean checked = true;
				
				try {
					obj.put("checked", checked);
					System.out.println("checked="+checked);
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
			
			res.setContentType("text/plain");
			res.setCharacterEncoding("UTF-8");
			PrintWriter out = res.getWriter();
			out.write(obj.toString());
			out.flush();
			out.close();
		}	

	}
	

}
