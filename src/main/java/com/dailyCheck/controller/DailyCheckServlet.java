package com.dailyCheck.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONException;
import org.json.JSONObject;

import com.board.model.*;
import com.dailyCheck.model.*;
import com.member.model.*;

public class DailyCheckServlet extends HttpServlet{
	private static final long serialVersionUID = 1L;
	public void doGet(HttpServletRequest req,HttpServletResponse res) 
			throws ServletException,IOException{
		doPost(req,res);
	}

	public void doPost(HttpServletRequest req,HttpServletResponse res) 
			throws ServletException,IOException {
		
		req.setCharacterEncoding("UTF-8");
		res.setContentType("text/html; charset=UTF-8");
		String action = req.getParameter("action");
		

        if ("dailyCheck".equals(action)) { 

				String boardID = req.getParameter("boardID");
				String memberID = req.getParameter("memberID");
				Date dailyCheckDate = new Date(System.currentTimeMillis());
				System.out.println("今天日期"+dailyCheckDate);
				DailyCheckService dailyCheckService = new DailyCheckService();
				
				MemberService memberSvc = new MemberService();
				MemberVO memberVO = memberSvc.getCoin(memberID);
				int memberCoin = memberVO.getMemberCoin();
				memberCoin = memberCoin + 10;
				memberSvc.updateMemberCoin(memberCoin, memberID);
				dailyCheckService.addDailyCheck(boardID, memberID, dailyCheckDate);
				
				MemberVO memberVO1 = memberSvc.getOneMember(memberID);
				HttpSession session = req.getSession();
				session.setAttribute("memberVO", memberVO1);
				
				JSONObject obj = new JSONObject();
				res.setContentType("text/plain");
				res.setCharacterEncoding("UTF-8");
				PrintWriter out = res.getWriter();
				out.write(obj.toString());
				out.flush();
				out.close();
		}
		
        
        
        
        if ("dailyCheckAjax".equals(action)) { 
			
    		System.out.println("in");
			String boardID = req.getParameter("boardID");
			String memberID = req.getParameter("memberID");
			Date dailyCheckDate = new Date(System.currentTimeMillis());
			System.out.println("今天日期"+dailyCheckDate);
			Boolean status = true;
			JSONObject obj = new JSONObject();
			
			DailyCheckService dailyCheckService = new DailyCheckService();
			DailyCheckVO dailyCheckVO = dailyCheckService.findDailyCheck(boardID, memberID, dailyCheckDate);

			if(dailyCheckVO == null) {
				status = false;
			}
			try {
				obj.put("status", status);
			} catch(JSONException e) {
				e.printStackTrace();
			}
			res.setContentType("text/plain");
			res.setCharacterEncoding("UTF-8");
			PrintWriter out = res.getWriter();
			out.write(obj.toString());
			out.flush();
			out.close();
        }
	}
}
