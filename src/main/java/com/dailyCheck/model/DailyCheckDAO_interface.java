package com.dailyCheck.model;

import java.sql.Date;
import java.util.List;

public interface DailyCheckDAO_interface {
	public void insert(DailyCheckVO dailyCheckVO);
	public DailyCheckVO findByPrimaryKey(String dailyCheckID);
	public DailyCheckVO findDailyCheck(String boardID,String memberID,Date dailyCheckDate);
	public List<DailyCheckVO> getAll();
}
