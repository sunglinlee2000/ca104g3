package com.dailyCheck.model;
import java.sql.Date;

public class DailyCheckVO implements java.io.Serializable{
	private String dailyCheckID;
	private String boardID;
	private String memberID;
	private Date dailyCheckDate;
	
	public String getDailyCheckID() {
		return dailyCheckID;
	}
	public void setDailyCheckID(String dailyCheckID) {
		this.dailyCheckID = dailyCheckID;
	}
	public String getBoardID() {
		return boardID;
	}
	public void setBoardID(String boardID) {
		this.boardID = boardID;
	}
	public String getMemberID() {
		return memberID;
	}
	public void setMemberID(String memberID) {
		this.memberID = memberID;
	}
	public Date getDailyCheckDate() {
		return dailyCheckDate;
	}
	public void setDailyCheckDate(Date dailyCheckDate) {
		this.dailyCheckDate = dailyCheckDate;
	}
	
}
