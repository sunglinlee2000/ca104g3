package com.dailyCheck.model;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

public class DailyCheckService {
	
	private DailyCheckDAO_interface dao;
	
	public DailyCheckService() {
		dao = new DailyCheckDAO();
	}
	
	public DailyCheckVO addDailyCheck(String boardID,String memberID,Date dailyCheckDate) {
		DailyCheckVO dailyCheckVO = new DailyCheckVO();
		
		dailyCheckVO.setBoardID(boardID);
		dailyCheckVO.setMemberID(memberID);
		dailyCheckVO.setDailyCheckDate(dailyCheckDate);
		dao.insert(dailyCheckVO);
		
		return dailyCheckVO;
	}
	
	
	public DailyCheckVO findByID(String dailyCheckID) {
		return dao.findByPrimaryKey(dailyCheckID);
	}
	
	public DailyCheckVO findDailyCheck(String boardID,String memberID,Date dailyCheckDate) {
		return dao.findDailyCheck(boardID, memberID, dailyCheckDate);
	}
	
	public List<DailyCheckVO> getAll(){
		return dao.getAll();
	}
	
	
}
