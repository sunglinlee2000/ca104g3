package com.dailyCheck.model;

import java.util.List;
import java.util.Set;

public class DailyCheckTest {

	public static void main(String[] args) {
		DailyCheckJDBCDAO dailyCheckJDBCDAO = new DailyCheckJDBCDAO();
//		1.------------------------
//		DailyCheckVO dailyCheckVO = new DailyCheckVO();
//		
//		dailyCheckVO.setBoardID("B000001");
//		dailyCheckVO.setMemberID("M000001");
//		dailyCheckVO.setDailyCheckDate(java.sql.Timestamp.valueOf("2018-10-22 00:00:00"));
//		
//		dailyCheckJDBCDAO.insert(dailyCheckVO);
//		
//		System.out.println("OK");
		
//		2.---------------------
//		DailyCheckVO dailyCheckVO2 = dailyCheckJDBCDAO.findByPrimaryKey("DC000018");
//		System.out.println(dailyCheckVO2.getDailyCheckID());
//		System.out.println(dailyCheckVO2.getBoardID());
//		System.out.println(dailyCheckVO2.getMemberID());
//		System.out.println(dailyCheckVO2.getDailyCheckDate());
//		System.out.println("==================================");
//		System.out.println("OK");
//		3.---------------------
		List<DailyCheckVO> list = dailyCheckJDBCDAO.getAll();
		for(DailyCheckVO dailyCheckVO3 : list) {
			System.out.println(dailyCheckVO3.getDailyCheckID());
			System.out.println(dailyCheckVO3.getBoardID());
			System.out.println(dailyCheckVO3.getMemberID());
			System.out.println(dailyCheckVO3.getDailyCheckDate());
			System.out.println("==================================");
		}
		System.out.println("OK");
	}
}
