package com.dailyCheck.model;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;


public class DailyCheckDAO implements DailyCheckDAO_interface{
	private static DataSource ds = null;
	static {
		try {
			Context ctx = new InitialContext();
			ds = (DataSource) ctx.lookup("java:comp/env/jdbc/CA104G3DB");
		} catch (NamingException e) {
			e.printStackTrace();
		}
	}
	private static final String INSERT_STMT = "INSERT INTO DAILYCHECK"
			+ " VALUES('DC'||LPAD (TO_CHAR(ARTIST_SEQ.NEXTVAL),6,0),?,?,?)";
	private static final String GET_ONE_STMT = "SELECT * FROM dailyCheck WHERE dailyCheckID=?";
	private static final String GET_ALL_STMT = "SELECT * FROM dailyCheck ";
	private static final String GET_DAILYCHECK = "SELECT * FROM dailyCheck WHERE boardID=? AND memberID=? AND dailyCheckDate=?";
	
	
	@Override
	public void insert(DailyCheckVO dailyCheckVO) {
		Connection con = null;
		PreparedStatement pstmt = null;
		
		try {
			
			con = ds.getConnection();
			pstmt = con.prepareStatement(INSERT_STMT);
			
			pstmt.setString(1, dailyCheckVO.getBoardID());
			pstmt.setString(2, dailyCheckVO.getMemberID());
			pstmt.setDate(3, dailyCheckVO.getDailyCheckDate());
			
			pstmt.executeUpdate();			
			
			
		
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured." + se.getMessage());
		
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if(con != null) {
				try {
					con.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
		}
	}

	@Override
	public DailyCheckVO findByPrimaryKey(String dailyCheckID) {
		
		DailyCheckVO dailyCheckVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		try {
			
			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ONE_STMT);
			
			pstmt.setString(1, dailyCheckID);
			
			rs = pstmt.executeQuery();
			
			while (rs.next()) {
				dailyCheckVO = new DailyCheckVO();
				dailyCheckVO.setDailyCheckID(rs.getString("dailyCheckID"));
				dailyCheckVO.setBoardID(rs.getString("boardID"));
				dailyCheckVO.setMemberID(rs.getString("memberID"));
				dailyCheckVO.setDailyCheckDate(rs.getDate("dailyCheckDate"));
				
			}
			
			
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured." + se.getMessage());
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			} 
			if (con != null) {
				try {
					con.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
		}
		return dailyCheckVO;
	}


	@Override
	public List<DailyCheckVO> getAll() {
		List<DailyCheckVO> list = new ArrayList<DailyCheckVO>();
		DailyCheckVO dailyCheckVO = null;
	
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
	
		try {
	
			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ALL_STMT);
			rs = pstmt.executeQuery();
	
			while (rs.next()) {
				dailyCheckVO = new DailyCheckVO();
				dailyCheckVO.setDailyCheckID(rs.getString("dailyCheckID"));
				dailyCheckVO.setBoardID(rs.getString("boardID"));
				dailyCheckVO.setMemberID(rs.getString("memberID"));
				dailyCheckVO.setDailyCheckDate(rs.getDate("dailyCheckDate"));

				list.add(dailyCheckVO); // Store the row in the vector
			}
	
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return list;
	}

	@Override
	public DailyCheckVO findDailyCheck(String boardID, String memberID, Date dailyCheckDate) {
		DailyCheckVO dailyCheckVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		try {
			
			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_DAILYCHECK);
			
			pstmt.setString(1, boardID);
			pstmt.setString(2, memberID);
			pstmt.setDate(3, dailyCheckDate);
			
			rs = pstmt.executeQuery();
			
			while (rs.next()) {
				dailyCheckVO = new DailyCheckVO();
				dailyCheckVO.setDailyCheckID(rs.getString("dailyCheckID"));
				dailyCheckVO.setBoardID(rs.getString("boardID"));
				dailyCheckVO.setMemberID(rs.getString("memberID"));
				dailyCheckVO.setDailyCheckDate(rs.getDate("dailyCheckDate"));
				
			}
			
			
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured." + se.getMessage());
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			} 
			if (con != null) {
				try {
					con.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
		}
		return dailyCheckVO;
	}

}
