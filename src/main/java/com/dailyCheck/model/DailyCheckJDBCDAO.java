package com.dailyCheck.model;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;


public class DailyCheckJDBCDAO implements DailyCheckDAO_interface{
	String driver = "oracle.jdbc.driver.OracleDriver";
	String url = "jdbc:oracle:thin:@localhost:1521:XE";
	String userid = "CA104G3";
	String passwd = "123456";
	
	private static final String INSERT_STMT = "INSERT INTO DAILYCHECK"
			+ " VALUES('DC'||LPAD (TO_CHAR(ARTIST_SEQ.NEXTVAL),6,0),?,?,?)";
	private static final String GET_ONE_STMT = "SELECT * FROM dailyCheck WHERE dailyCheckID=?";
	private static final String GET_ALL_STMT = "SELECT * FROM dailyCheck ";
	
	
	@Override
	public void insert(DailyCheckVO dailyCheckVO) {
		Connection con = null;
		PreparedStatement pstmt = null;
		
		try {
			
			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(INSERT_STMT);
			
			pstmt.setString(1, dailyCheckVO.getBoardID());
			pstmt.setString(2, dailyCheckVO.getMemberID());
			pstmt.setDate(3, dailyCheckVO.getDailyCheckDate());
			
			pstmt.executeUpdate();			
			
			
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver." + e.getMessage());
		
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured." + se.getMessage());
		
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if(con != null) {
				try {
					con.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
		}
	}

	@Override
	public DailyCheckVO findByPrimaryKey(String dailyCheckID) {
		
		DailyCheckVO dailyCheckVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		try {
			
			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(GET_ONE_STMT);
			
			pstmt.setString(1, dailyCheckID);
			
			rs = pstmt.executeQuery();
			
			while (rs.next()) {
				dailyCheckVO = new DailyCheckVO();
				dailyCheckVO.setDailyCheckID(rs.getString("dailyCheckID"));
				dailyCheckVO.setBoardID(rs.getString("boardID"));
				dailyCheckVO.setMemberID(rs.getString("memberID"));
				dailyCheckVO.setDailyCheckDate(rs.getDate("dailyCheckDate"));
				
			}
			
			
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver."+ e.getMessage());
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured." + se.getMessage());
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			} 
			if (con != null) {
				try {
					con.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
		}
		return dailyCheckVO;
	}


	@Override
	public List<DailyCheckVO> getAll() {
		List<DailyCheckVO> list = new ArrayList<DailyCheckVO>();
		DailyCheckVO dailyCheckVO = null;
	
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
	
		try {
	
			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(GET_ALL_STMT);
			rs = pstmt.executeQuery();
	
			while (rs.next()) {
				dailyCheckVO = new DailyCheckVO();
				dailyCheckVO.setDailyCheckID(rs.getString("dailyCheckID"));
				dailyCheckVO.setBoardID(rs.getString("boardID"));
				dailyCheckVO.setMemberID(rs.getString("memberID"));
				dailyCheckVO.setDailyCheckDate(rs.getDate("dailyCheckDate"));

				list.add(dailyCheckVO); // Store the row in the vector
			}
	
			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return list;
	}


	@Override
	public DailyCheckVO findDailyCheck(String boardID, String memberID, Date dailyCheckDate) {
		// TODO Auto-generated method stub
		return null;
	}

}
