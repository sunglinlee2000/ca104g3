package com.artist.controller;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.artist.model.ArtistService;
import com.artist.model.ArtistVO;

public class ArtistServlet extends HttpServlet{
	private static final long serialVersionUID = 1L;
	
	public void doGet(HttpServletRequest req,HttpServletResponse res) 
			throws ServletException,IOException{
		doPost(req,res);
	}
	
	public void doPost(HttpServletRequest req,HttpServletResponse res) 
			throws ServletException,IOException {
		
		req.setCharacterEncoding("UTF-8");
		res.setContentType("text/html; charset=UTF-8");
		String action = req.getParameter("action");
		
		if("getOne_For_Display".equals(action)) {
			
			List<String> errorMsgs = new LinkedList<String>();
			req.setAttribute("errorMsgs", errorMsgs);
			
			try {
				/***************************1.接收請求參數 - 輸入格式的錯誤處理**********************/
				String str = req.getParameter("artistID");
				if (str == null || (str.trim()).length() == 0) {
					errorMsgs.add("請輸入藝人編號");
				}
				// Send the use back to the form, if there were errors
				if (!errorMsgs.isEmpty()) {
					RequestDispatcher failureView = req
							.getRequestDispatcher("/back-end/artist/select_page.jsp");
					failureView.forward(req, res);
					return;//程式中斷
				}
				
				String artistID = null;
				try {
					artistID = new String(str);
				} catch (Exception e) {
					errorMsgs.add("藝人編號格式不正確");
				}
				// Send the use back to the form, if there were errors
				if (!errorMsgs.isEmpty()) {
					RequestDispatcher failureView = req
							.getRequestDispatcher("/back-end/artist/select_page.jsp");
					failureView.forward(req, res);
					return;//程式中斷
				}
				
				/***************************2.開始查詢資料*****************************************/
				ArtistService artistSvc = new ArtistService();
				ArtistVO artistVO = artistSvc.getOneArtist(artistID);
				if (artistVO == null) {
					errorMsgs.add("查無資料");
				}
				// Send the use back to the form, if there were errors
				if (!errorMsgs.isEmpty()) {
					RequestDispatcher failureView = req
							.getRequestDispatcher("/back-end/artist/select_page.jsp");
					failureView.forward(req, res);
					return;//程式中斷
				}
				
				/***************************3.查詢完成,準備轉交(Send the Success view)*************/
				req.setAttribute("artistVO", artistVO); 
				String url = "/back-end/artist/listOneArtist.jsp";
				RequestDispatcher successView = req.getRequestDispatcher(url); // 成功轉交 listOneEmp.jsp
				successView.forward(req, res);

				/***************************其他可能的錯誤處理*************************************/
			} catch (Exception e) {
				errorMsgs.add("無法取得資料:" + e.getMessage());
				RequestDispatcher failureView = req
						.getRequestDispatcher("/back-end/artist/select_page.jsp");
				failureView.forward(req, res);
			}
		}
		
		
		if ("get_Artist_By_Board".equals(action)) { 
			List<String> errorMsgs = new LinkedList<String>();
			req.setAttribute("errorMsgs", errorMsgs);
			
			try {
				/***************************1.接收請求參數****************************************/
				String boardID = new String(req.getParameter("boardID"));
				
				/***************************2.開始查詢資料****************************************/
				ArtistService artistSvc = new ArtistService();
				List<ArtistVO> list = artistSvc.getArtistByBoard(boardID);
								
				/***************************3.查詢完成,準備轉交(Send the Success view)************/
				req.setAttribute("listArtistByBoard", list);         // 資料庫取出的empVO物件,存入req
				
				String url = "/back-end/artist/listArtistByBoard.jsp";
				RequestDispatcher successView = req.getRequestDispatcher(url);// 成功轉交 update_emp_input.jsp
				successView.forward(req, res);

				/***************************其他可能的錯誤處理**********************************/
			} catch (Exception e) {
				errorMsgs.add("無法取得要修改的資料:" + e.getMessage());
				RequestDispatcher failureView = req
						.getRequestDispatcher("/back-end/artist/select_page.jsp");
				failureView.forward(req, res);
			}
		}
		
		
		if ("getOne_For_Update".equals(action)) { // 來自listAllEmp.jsp的請求

			List<String> errorMsgs = new LinkedList<String>();
			// Store this set in the request scope, in case we need to
			// send the ErrorPage view.
			req.setAttribute("errorMsgs", errorMsgs);
			
			try {
				/***************************1.接收請求參數****************************************/
				String artistID = new String(req.getParameter("artistID"));
				
				/***************************2.開始查詢資料****************************************/
				ArtistService artistSvc = new ArtistService();
				ArtistVO artistVO = artistSvc.getOneArtist(artistID);
								
				/***************************3.查詢完成,準備轉交(Send the Success view)************/
				req.setAttribute("artistVO", artistVO);         // 資料庫取出的empVO物件,存入req
				String url = "/back-end/artist/update_artist_input.jsp";
				RequestDispatcher successView = req.getRequestDispatcher(url);// 成功轉交 update_emp_input.jsp
				successView.forward(req, res);

				/***************************其他可能的錯誤處理**********************************/
			} catch (Exception e) {
				errorMsgs.add("無法取得要修改的資料:" + e.getMessage());
				RequestDispatcher failureView = req
						.getRequestDispatcher("/back-end/artist/listAllArtist.jsp");
				failureView.forward(req, res);
			}
		}
		
		
		if ("update".equals(action)) {
			
			List<String> errorMsgs = new LinkedList<String>();
			// Store this set in the request scope, in case we need to
			// send the ErrorPage view.
			req.setAttribute("errorMsgs", errorMsgs);
		
			try {
				/***************************1.接收請求參數 - 輸入格式的錯誤處理**********************/
				String artistID=req.getParameter("artistID");
								
				String boardID = req.getParameter("boardID");				
				String boardIDReg = "^[(B0-9_)]{7,7}$";
				if (boardID == null || boardID.trim().length() == 0) {
					errorMsgs.add("專板編號: 請勿空白");
				} else if(!boardID.trim().matches(boardIDReg)) { //以下練習正則(規)表示式(regular-expression)
					errorMsgs.add("專板編號: 只能是B開頭，總長度7位");
	            }
				
				String artistName = req.getParameter("artistName");
				String artistNameReg = "^[(\u4e00-\u9fa5)(a-zA-Z0-9_)]{2,10}$";
				if (artistName == null || artistName.trim().length() == 0) {
					errorMsgs.add("藝人名稱: 請勿空白");
				} else if(!artistName.trim().matches(artistNameReg)) { //以下練習正則(規)表示式(regular-expression)
					errorMsgs.add("藝人名稱: 只能是中、英文字母、數字和_ , 且長度必需在2到10之間");
	            }
				


				ArtistVO artistVO = new ArtistVO();
				artistVO.setArtistID(artistID);
				artistVO.setBoardID(boardID);
				artistVO.setArtistName(artistName);



				// Send the use back to the form, if there were errors
				if (!errorMsgs.isEmpty()) {
					req.setAttribute("artistVO", artistVO); // 含有輸入格式錯誤的empVO物件,也存入req
					RequestDispatcher failureView = req
							.getRequestDispatcher("/back-end/artist/update_artist_input.jsp");
					failureView.forward(req, res);
					return; //程式中斷
				}
				
				/***************************2.開始修改資料*****************************************/
				ArtistService artistSvc = new ArtistService();
				artistVO = artistSvc.updateArtist(artistID, boardID, artistName);
				/***************************3.修改完成,準備轉交(Send the Success view)*************/
				req.setAttribute("artistVO", artistVO); // 資料庫update成功後,正確的的empVO物件,存入req
				String url = "/back-end/artist/listOneArtist.jsp";
				RequestDispatcher successView = req.getRequestDispatcher(url); // 修改成功後,轉交listOneEmp.jsp
				successView.forward(req, res);
				/***************************其他可能的錯誤處理*************************************/
			} catch (Exception e) {
				errorMsgs.add("修改資料失敗:"+e.getMessage());
				RequestDispatcher failureView = req
						.getRequestDispatcher("/back-end/artist/update_artist_input.jsp");
				failureView.forward(req, res);
			}
		}

        if ("insert".equals(action)) { 
			
			Map<String,String> errorMsgs = new LinkedHashMap<String,String>();
			// Store this set in the request scope, in case we need to
			// send the ErrorPage view.
			req.setAttribute("errorMsgs", errorMsgs);

			try {
				/***********************1.接收請求參數 - 輸入格式的錯誤處理*************************/
				String boardID = req.getParameter("boardID");				
				String boardIDReg = "^[(B0-9_)]{7,7}$";
				if (boardID == null || boardID.trim().length() == 0) {
					errorMsgs.put("boardID","專板編號: 請勿空白");
				} else if(!boardID.trim().matches(boardIDReg)) { //以下練習正則(規)表示式(regular-expression)
					errorMsgs.put("boardID","專板編號: 只能是B開頭，總長度7位");
	            }
				
				String artistName = req.getParameter("artistName");
				String artistNameReg = "^[(\u4e00-\u9fa5)(a-zA-Z0-9_)]{2,10}$";
				if (artistName == null || artistName.trim().length() == 0) {
					errorMsgs.put("artistName","藝人名稱: 請勿空白");
				} else if(!artistName.trim().matches(artistNameReg)) { //以下練習正則(規)表示式(regular-expression)
					errorMsgs.put("artistName","藝人名稱: 只能是中、英文字母、數字和_ , 且長度必需在2到10之間");
	            }
				
				
				ArtistVO artistVO = new ArtistVO();
				artistVO.setBoardID(boardID);
				artistVO.setArtistName(artistName);
				
				// Send the use back to the form, if there were errors
				if (!errorMsgs.isEmpty()) {
					req.setAttribute("artistVO", artistVO); // 含有輸入格式錯誤的empVO物件,也存入req
					RequestDispatcher failureView = req
							.getRequestDispatcher("/back-end/artist/addArtist.jsp");
					failureView.forward(req, res);
					return;
				}
				
				/***************************2.開始新增資料***************************************/
				ArtistService artistSvc = new ArtistService();
				artistVO = artistSvc.addArtist(boardID, artistName);
				/***************************3.新增完成,準備轉交(Send the Success view)***********/
				String url = "/back-end/artist/listAllArtist.jsp";
				RequestDispatcher successView = req.getRequestDispatcher(url); // 新增成功後轉交listAllEmp.jsp
				successView.forward(req, res);				
				/***************************其他可能的錯誤處理**********************************/
			} catch (Exception e) {
				e.printStackTrace(System.err);
				errorMsgs.put("Exception",e.getMessage());
				RequestDispatcher failureView = req
						.getRequestDispatcher("/back-end/artist/addArtist.jsp");
				failureView.forward(req, res);
			}
		}
		
		
		if ("delete".equals(action)) { 

			List<String> errorMsgs = new LinkedList<String>();
			// Store this set in the request scope, in case we need to
			// send the ErrorPage view.
			req.setAttribute("errorMsgs", errorMsgs);
	
			try {
				/***************************1.接收請求參數***************************************/
				String artistID = new String(req.getParameter("artistID"));
				
				/***************************2.開始刪除資料***************************************/
				ArtistService artistSvc = new ArtistService();
				artistSvc.deleteArtist(artistID);
				
				/***************************3.刪除完成,準備轉交(Send the Success view)***********/								
				String url = "/back-end/artist//listAllArtist.jsp";
				RequestDispatcher successView = req.getRequestDispatcher(url);// 刪除成功後,轉交回送出刪除的來源網頁
				successView.forward(req, res);
				
				/***************************其他可能的錯誤處理**********************************/
			} catch (Exception e) {
				errorMsgs.add("刪除資料失敗:"+e.getMessage());
				RequestDispatcher failureView = req
						.getRequestDispatcher("/back-end/artist//listAllArtist.jsp");
				failureView.forward(req, res);
			}
		}
	}
}
