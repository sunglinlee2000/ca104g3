package com.artist.model;
import java.util.List;
import java.util.Set;

public interface ArtistDAO_interface {
		public void insert(ArtistVO artistVO);
		public void update(ArtistVO artistVO);
		public void delete(String artistID);
		public ArtistVO findByPrimaryKey(String artistID);
		public List<ArtistVO> getAll();
		public List<ArtistVO> getArtistByBoardID(String boardID);
}
