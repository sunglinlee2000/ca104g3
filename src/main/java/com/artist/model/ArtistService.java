package com.artist.model;

import java.util.List;

public class ArtistService {

	private ArtistDAO_interface dao;
	
	public ArtistService() {
		dao = new ArtistDAO();
	}
	
	public ArtistVO addArtist(String boardID, String artistName) {
		ArtistVO artistVO = new ArtistVO();
		
		artistVO.setBoardID(boardID);
		artistVO.setArtistName(artistName);
		dao.insert(artistVO);
		
		return artistVO;
	}
	
	public ArtistVO updateArtist(String artistID, String boardID, String artistName) {
		ArtistVO artistVO = new ArtistVO();
		
		artistVO.setArtistID(artistID);
		artistVO.setBoardID(boardID);
		artistVO.setArtistName(artistName);
		dao.update(artistVO);
		
		return artistVO;
	}
	
	public void deleteArtist(String artistID) {
		dao.delete(artistID);
	}
	
	public ArtistVO getOneArtist(String artistID) {
		return dao.findByPrimaryKey(artistID);
	}
	
	public List<ArtistVO> getAll(){
		return dao.getAll();
	}
	
	public List<ArtistVO> getArtistByBoard(String boardID) {
		return dao.getArtistByBoardID(boardID);
	}
}
