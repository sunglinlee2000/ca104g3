package com.SNSAccount.controller;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.SNSAccount.model.SNSAccountService;
import com.SNSAccount.model.SNSAccountVO;

public class SNSAccountServlet extends HttpServlet{
	private static final long serialVersionUID = 1L;
	
	@Override
	public void init() throws ServletException {
		System.setProperty("webdriver.chrome.driver",getServletContext().getRealPath("chromedriver.exe"));
	}
	

	public void doGet(HttpServletRequest req,HttpServletResponse res) 
			throws ServletException,IOException{
		doPost(req,res);
		
	}
	public void doPost(HttpServletRequest req,HttpServletResponse res) 
				throws ServletException,IOException {
		req.setCharacterEncoding("UTF-8");
		res.setContentType("text/html; charset=UTF-8");
		String action = req.getParameter("action");

	if("getOne_For_Display".equals(action)) {
		
		List<String> errorMsgs = new LinkedList<String>();
		req.setAttribute("errorMsgs", errorMsgs);
		
		try {
			/***************************1.接收請求參數 - 輸入格式的錯誤處理**********************/
			String str = req.getParameter("snsAccountID");
			if (str == null || (str.trim()).length() == 0) {
				errorMsgs.add("請輸入藝人社群帳號編號");
			}
			// Send the use back to the form, if there were errors
			if (!errorMsgs.isEmpty()) {
				RequestDispatcher failureView = req
						.getRequestDispatcher("/back-end/SNSAccount/select_page.jsp");
				failureView.forward(req, res);
				return;//程式中斷
			}
			
			String SNSAccountID = null;
			try {
				SNSAccountID = new String(str);
			} catch (Exception e) {
				errorMsgs.add("藝人社群帳號編號格式不正確");
			}
			// Send the use back to the form, if there were errors
			if (!errorMsgs.isEmpty()) {
				RequestDispatcher failureView = req
						.getRequestDispatcher("/back-end/SNSAccount/select_page.jsp");
				failureView.forward(req, res);
				return;//程式中斷
			}
			
			/***************************2.開始查詢資料*****************************************/
			SNSAccountService snsAccountSvc = new SNSAccountService();
			SNSAccountVO snsAccountVO = snsAccountSvc.getSNSAccount(SNSAccountID);
			if (snsAccountVO == null) {
				errorMsgs.add("查無資料");
			}
			// Send the use back to the form, if there were errors
			if (!errorMsgs.isEmpty()) {
				RequestDispatcher failureView = req
						.getRequestDispatcher("/back-end/SNSAccount/select_page.jsp");
				failureView.forward(req, res);
				return;//程式中斷
			}
			/***************************3.查詢完成,準備轉交(Send the Success view)*************/
			req.setAttribute("snsAccountVO", snsAccountVO); // 資料庫取出的empVO物件,存入req
			String url = "/back-end/SNSAccount/listOneSNSAccount.jsp";
			RequestDispatcher successView = req.getRequestDispatcher(url); // 成功轉交 listOneEmp.jsp
			successView.forward(req, res);

			/***************************其他可能的錯誤處理*************************************/
		} catch (Exception e) {
			errorMsgs.add("無法取得資料:" + e.getMessage());
			RequestDispatcher failureView = req
					.getRequestDispatcher("/back-end/SNSAccount/select_page.jsp");
			failureView.forward(req, res);
		}
	}
	
	
	if ("getOne_For_Update".equals(action)) { // 來自listAllEmp.jsp的請求

		List<String> errorMsgs = new LinkedList<String>();
		req.setAttribute("errorMsgs", errorMsgs);
		
		try {
			/***************************1.接收請求參數****************************************/
			String snsAccountID = new String(req.getParameter("snsAccountID"));
			
			/***************************2.開始查詢資料****************************************/
			SNSAccountService snsAccountSvc = new SNSAccountService();
			SNSAccountVO snsAccountVO = snsAccountSvc.getSNSAccount(snsAccountID);
							
			/***************************3.查詢完成,準備轉交(Send the Success view)************/
			req.setAttribute("snsAccountVO", snsAccountVO);         // 資料庫取出的empVO物件,存入req
			String url = "/back-end/SNSAccount/update_SNSAccount_input.jsp";
			RequestDispatcher successView = req.getRequestDispatcher(url);// 成功轉交 update_emp_input.jsp
			successView.forward(req, res);

			/***************************其他可能的錯誤處理**********************************/
		} catch (Exception e) {
			errorMsgs.add("無法取得要修改的資料:" + e.getMessage());
			RequestDispatcher failureView = req
					.getRequestDispatcher("/back-end/SNSAccount/listAllSNSAccount.jsp");
			failureView.forward(req, res);
		}
	}
	
	
	if ("update".equals(action)) {
		
		List<String> errorMsgs = new LinkedList<String>();
		req.setAttribute("errorMsgs", errorMsgs);
		try {
			/***************************1.接收請求參數 - 輸入格式的錯誤處理**********************/
			String snsAccountID=req.getParameter("snsAccountID");
			if (snsAccountID == null || snsAccountID.trim().length() == 0) {
				errorMsgs.add(" 請勿空白");
			} 
			
			String artistID = req.getParameter("artistID");
			if (artistID == null || artistID.trim().length() == 0) {
				errorMsgs.add(" 請勿空白");
			} 
			
			String snsAccount = req.getParameter("snsAccount");
			if (snsAccount == null || snsAccount.trim().length() == 0) {
				errorMsgs.add(" 請勿空白");
			} 
			
			String snsLink = req.getParameter("snsLink");
			if (snsLink == null || snsLink.trim().length() == 0) {
				errorMsgs.add(" 請勿空白");
			} 

			String snsAccountType = req.getParameter("snsAccountType");
			if (snsAccountType == null || snsAccountType.trim().length() == 0) {
				errorMsgs.add(" 請勿空白");
			} 

			SNSAccountVO snsAccountVO = new SNSAccountVO();
			snsAccountVO.setSnsAccountID(snsAccountID);
			snsAccountVO.setArtistID(artistID);
			snsAccountVO.setSnsAccount(snsAccount);
			snsAccountVO.setSnsLink(snsLink);
			snsAccountVO.setSnsAccountType(snsAccountType);
			



			if (!errorMsgs.isEmpty()) {
				req.setAttribute("snsAccountVO", snsAccountVO); // 含有輸入格式錯誤的empVO物件,也存入req
				RequestDispatcher failureView = req
						.getRequestDispatcher("/back-end/SNSAccount/update_SNSAccount_input.jsp");
				failureView.forward(req, res);
				return; //程式中斷
			}
			
			/***************************2.開始修改資料*****************************************/
			SNSAccountService snsAccountSvc = new SNSAccountService();
			snsAccountVO = snsAccountSvc.updateSNSAccount(snsAccountID, artistID, snsAccount, snsLink, snsAccountType);
			/***************************3.修改完成,準備轉交(Send the Success view)*************/
			req.setAttribute("snsAccountVO", snsAccountVO); // 資料庫update成功後,正確的的empVO物件,存入req
			String url = "/back-end/SNSAccount/listAllSNSAccount.jsp";
			RequestDispatcher successView = req.getRequestDispatcher(url); // 修改成功後,轉交listOneEmp.jsp
			successView.forward(req, res);
			/***************************其他可能的錯誤處理*************************************/
		} catch (Exception e) {
			errorMsgs.add("修改資料失敗:"+e.getMessage());
			RequestDispatcher failureView = req
					.getRequestDispatcher("/back-end/SNSAccount/update_SNSAccount_input.jsp");
			failureView.forward(req, res);
		}
	}

    if ("insert".equals(action)) { 
		
		List<String> errorMsgs = new LinkedList<String>();
		req.setAttribute("errorMsgs", errorMsgs);
		try {
			/***********************1.接收請求參數 - 輸入格式的錯誤處理*************************/
			String artistID = req.getParameter("artistID");
			if (artistID == null || artistID.trim().length() == 0) {
				errorMsgs.add(" 請勿空白");
			} 
			
			String snsAccount = req.getParameter("snsAccount");
			if (snsAccount == null || snsAccount.trim().length() == 0) {
				errorMsgs.add(" 請勿空白");
			} 
			
			String snsLink = req.getParameter("snsLink");
			if (snsLink == null || snsLink.trim().length() == 0) {
				errorMsgs.add(" 請勿空白");
			} 

			String snsAccountType = req.getParameter("snsAccountType");
			if (snsAccountType == null || snsAccountType.trim().length() == 0) {
				errorMsgs.add(" 請勿空白");
			} 
			
			

			SNSAccountVO snsAccountVO = new SNSAccountVO();
			snsAccountVO.setArtistID(artistID);
			snsAccountVO.setSnsAccount(snsAccount);
			snsAccountVO.setSnsLink(snsLink);
			snsAccountVO.setSnsAccountType(snsAccountType);

			
			// Send the use back to the form, if there were errors
			if (!errorMsgs.isEmpty()) {
				req.setAttribute("snsAccountVO", snsAccountVO); // 含有輸入格式錯誤的empVO物件,也存入req
				RequestDispatcher failureView = req
						.getRequestDispatcher("/back-end/SNSAccount/addSNSAccount.jsp");
				failureView.forward(req, res);
				return;
			}
			
			/***************************2.開始新增資料***************************************/
			SNSAccountService snsAccountSvc = new SNSAccountService();
			snsAccountVO = snsAccountSvc.addSNSAccount(artistID, snsAccount, snsLink, snsAccountType);
			/***************************3.新增完成,準備轉交(Send the Success view)***********/
			String url = "/back-end/SNSAccount/listAllSNSAccount.jsp";
			RequestDispatcher successView = req.getRequestDispatcher(url); // 新增成功後轉交listAllEmp.jsp
			successView.forward(req, res);				
			/***************************其他可能的錯誤處理**********************************/
		} catch (Exception e) {
			e.printStackTrace(System.err);
			errorMsgs.add(e.getMessage());
			RequestDispatcher failureView = req
					.getRequestDispatcher("/back-end/SNSAccount/addSNSAccount.jsp");
			failureView.forward(req, res);
		}
	}
	
	
	if ("delete".equals(action)) { 

		List<String> errorMsgs = new LinkedList<String>();
		req.setAttribute("errorMsgs", errorMsgs);

		try {
			/***************************1.接收請求參數***************************************/
			String snsAccountID = new String(req.getParameter("snsAccountID"));
			
			/***************************2.開始刪除資料***************************************/
			SNSAccountService snsAccountSvc = new SNSAccountService();
			snsAccountSvc.deleteSNSAccount(snsAccountID);
			
			/***************************3.刪除完成,準備轉交(Send the Success view)***********/								
			String url = "/back-end/SNSAccount/listAllSNSAccount.jsp";
			RequestDispatcher successView = req.getRequestDispatcher(url);// 刪除成功後,轉交回送出刪除的來源網頁
			successView.forward(req, res);
			
			/***************************其他可能的錯誤處理**********************************/
		} catch (Exception e) {
			errorMsgs.add("刪除資料失敗:"+e.getMessage());
			RequestDispatcher failureView = req
					.getRequestDispatcher("/back-end/SNSAccount/listAllSNSAccount.jsp");
			failureView.forward(req, res);
		}
	}
	if("getSNS".equals(action)) {
		SNSAccountService SNSAccountSvc = new SNSAccountService();
		List<SNSAccountVO> list = SNSAccountSvc.getAllSNSAccount();
		String[] link = new String[100];
		for(int i = 0 ; i < list.size();i++) {
			SNSAccountVO snsAccountVO = (SNSAccountVO)list.get(i);
			String snsLink = snsAccountVO.getSnsLink();
			String artistID = snsAccountVO.getArtistID();
			System.out.println(snsLink);
			link[i]=snsLink;
			CrawlIG IG = new CrawlIG(link[i],artistID);
			try {
				System.out.println("執行緒"+i+"已啟動");
				IG.start();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}	

	}	
	}
}
