package com.SNSAccount.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.SNSAccount.model.SNSAccountService;
import com.SNSAccount.model.SNSAccountVO;

public class SNSScheduleServlet extends HttpServlet{

	private static final long serialVersionUID = 1L;

	Timer tm = null;
	
	TimerTask tt = new TimerTask() {

		@Override
		public void run() {
			System.out.println("排程已啟動");
			SNSAccountService SNSAccountSvc = new SNSAccountService();
			List<SNSAccountVO> list = SNSAccountSvc.getAllSNSAccount();
			String[] link = new String[100];
			for(int i = 0 ; i < list.size();i++) {
				SNSAccountVO snsAccountVO = (SNSAccountVO)list.get(i);
				String snsLink = snsAccountVO.getSnsLink();
				String artistID = snsAccountVO.getArtistID();
				System.out.println(snsLink);
				link[i]=snsLink;
				CrawlIG IG = new CrawlIG(link[i],artistID);
				try {
					System.out.println("執行緒"+i+"已啟動");
					IG.start();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}		
		}
		
	};
	
	public void init() throws ServletException{
		System.setProperty("webdriver.chrome.driver",getServletContext().getRealPath("chromedriver.exe"));
		tm = new Timer();
		Calendar firstTime = new GregorianCalendar(2018, Calendar.NOVEMBER, 26 ,0 ,0 ,0);
		Date date = firstTime.getTime();
		Long period = (long) (24*60*60*1000);
		
		tm.scheduleAtFixedRate(tt, date, period);
	}
	
	public void destroy() {
		tm.cancel();
	}
	
	public void doGet(HttpServletRequest req,HttpServletResponse res) throws ServletException,IOException {
		res.setContentType("text/plain");
		PrintWriter out = res.getWriter();
	}
}
