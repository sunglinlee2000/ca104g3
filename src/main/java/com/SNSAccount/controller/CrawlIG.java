package com.SNSAccount.controller;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

import com.SNS.model.SNSService;
import com.SNS.model.SNSVO;


public class CrawlIG extends Thread{
	String snslink ;
	String artistID;
	public CrawlIG(String snsLink,String artistid) {
		this.snslink = snsLink;
		this.artistID = artistid;
	}

	public static void main(String[] args) throws Exception {


			} 
				

	
	public void run() {
		
		WebDriver driver = new ChromeDriver(DesiredCapabilities.chrome());
		JavascriptExecutor js = ((JavascriptExecutor) driver);

		driver.navigate().to(snslink);
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		
		String photoURL = null;
		String content = null;
		String time = null;
		
		SNSVO snsvo = new SNSVO();
		SNSService snsSvc = new SNSService();
		// 抓一開始的頁面
				try {
					Thread.sleep(10000);
				} catch (InterruptedException e1) {
					e1.printStackTrace();
				}
				List<WebElement> rows = driver.findElements(By.xpath("//article[@class='FyNDV']/div[1]/div/*"));
				System.out.println(rows.size());
				WebElement firstRow = rows.get(0);
				WebElement firstPic = firstRow.findElements(By.cssSelector("._9AhH0")).get(0);
				firstPic.click();
				
				try {
					Thread.sleep(500);
				} catch (InterruptedException e1) {
					e1.printStackTrace();
				}
				WebElement data = firstRow.findElements(By.cssSelector(".KL4Bh > img")).get(0);
//				System.out.println(data.getAttribute("alt"));
//				System.out.println(data.getAttribute("src"));
//				System.out.println(driver.findElement(By.cssSelector(".c-Yi7 > time")).getAttribute("title"));
				System.out.println("======================");
				
				for (int i = 1; i <= 5; i++) {
					driver.findElement(By.xpath("//a[contains(@class,'HBoOv')]")).click();
					try {
						Thread.sleep(500);
					} catch (InterruptedException e1) {
						e1.printStackTrace();
					}
					try {
						WebElement movie = driver.findElement(By.xpath("//img[@class='_8jZFn']"));
//						System.out.println(movie.getAttribute("src"));
						photoURL = movie.getAttribute("src");
						WebElement text = driver.findElement(By.xpath("//div[@class='C4VMK']/span"));
						content = text.getText();
//						System.out.println(text.getText());
						time=driver.findElement(By.cssSelector(".c-Yi7 > time")).getAttribute("datetime");
//						System.out.println(driver.findElement(By.cssSelector(".c-Yi7 > time")).getAttribute("datetime"));
						
					} catch (Exception e) {
						try {
							Thread.sleep(500);
						} catch (InterruptedException e1) {
							e1.printStackTrace();
						}
						List<WebElement> wes = driver.findElements(By.xpath("//div[@class='KL4Bh']/img"));
//						System.out.println(i);
						WebElement we = wes.get(wes.size() - 1);
						content = we.getAttribute("alt");
//						System.out.println(we.getAttribute("alt"));
						photoURL = we.getAttribute("src");
//						System.out.println(we.getAttribute("src"));
						time = driver.findElement(By.cssSelector(".c-Yi7 > time")).getAttribute("datetime");
//						System.out.println(driver.findElement(By.cssSelector(".c-Yi7 > time")).getAttribute("datetime"));
					}
					snsvo.setArtistID(artistID);
					snsvo.setSnsPhotoURL(photoURL);
					snsvo.setSnsContent(content);
					snsvo.setTime(time);
					snsSvc.addSNS(snsvo);
					System.out.println("======================");
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}	
				driver.close();
				
	}
	
	
}
