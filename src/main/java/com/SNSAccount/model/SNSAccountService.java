package com.SNSAccount.model;

import java.util.List;


public class SNSAccountService {
	
	private SNSAccountDAO_interface dao;
	
	public SNSAccountService() {
		dao = new SNSAccountDAO();
	}
	
	public SNSAccountVO addSNSAccount(String artistID,String snsAccount,String snsLink,String snsAccountType) {
		SNSAccountVO snsAccountVO = new SNSAccountVO();
		
		snsAccountVO.setArtistID(artistID);
		snsAccountVO.setSnsAccount(snsAccount);
		snsAccountVO.setSnsLink(snsLink);
		snsAccountVO.setSnsAccountType(snsAccountType);
		dao.insert(snsAccountVO);
		
		return snsAccountVO;
	}
	
	public SNSAccountVO updateSNSAccount(String snsAccountID,String artistID,String snsAccount,String snsLink,String snsAccountType) {
		SNSAccountVO snsAccountVO = new SNSAccountVO();
		
		snsAccountVO.setSnsAccountID(snsAccountID);
		snsAccountVO.setArtistID(artistID);
		snsAccountVO.setSnsAccount(snsAccount);
		snsAccountVO.setSnsLink(snsLink);
		snsAccountVO.setSnsAccountType(snsAccountType);
		dao.update(snsAccountVO);
		
		return snsAccountVO;
	}
	
	
	public void deleteSNSAccount(String snsAccountID) {
		dao.delete(snsAccountID);
	}
	
	public SNSAccountVO getSNSAccount(String snsAccountID) {
		return dao.findByPrimaryKey(snsAccountID);
	}
	
	public List<SNSAccountVO> getAllSNSAccount() {
		return dao.getAll();
	}
	
}
