package com.SNSAccount.model;

public class SNSAccountVO implements java.io.Serializable{
	private String snsAccountID;
	private String artistID;
	private String snsAccount;
	private String snsLink;
	private String snsAccountType;
	
	public String getSnsAccountID() {
		return snsAccountID;
	}
	public void setSnsAccountID(String snsAccountID) {
		this.snsAccountID = snsAccountID;
	}
	public String getArtistID() {
		return artistID;
	}
	public void setArtistID(String artistID) {
		this.artistID = artistID;
	}
	public String getSnsAccount() {
		return snsAccount;
	}
	public void setSnsAccount(String snsAccount) {
		this.snsAccount = snsAccount;
	}
	public String getSnsLink() {
		return snsLink;
	}
	public void setSnsLink(String snsLink) {
		this.snsLink = snsLink;
	}
	public String getSnsAccountType() {
		return snsAccountType;
	}
	public void setSnsAccountType(String snsAccountType) {
		this.snsAccountType = snsAccountType;
	}
	
}
