package com.SNSAccount.model;
import java.util.List;

public interface SNSAccountDAO_interface {
		public void insert(SNSAccountVO snsAccountVO);
		public void update(SNSAccountVO snsAccountVO);
		public void delete(String snsAccountID);
		public SNSAccountVO findByPrimaryKey(String snsAccountID);
		public List<SNSAccountVO> getAll();
} 
