package com.SNSAccount.model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


public class SNSAccountJDBCDAO implements SNSAccountDAO_interface{
	String driver = "oracle.jdbc.driver.OracleDriver";
	String url = "jdbc:oracle:thin:@localhost:1521:XE";
	String userid = "CA104G3";
	String passwd = "123456";
	
	private static final String INSERT_STMT = "INSERT INTO SNSAccount (SnsAccountID,artistID,SNSAccount,SnsLink,SnsAccountType) "
			+ "VALUES ('SNSA'||LPAD(TO_CHAR(snsaccount_seq.NEXTVAL),6,0),?,?,?,?)";
	private static final String UPDATE = "UPDATE SNSAccount SET artistID=?, SNSAccount=?, SnsLink=?, SnsAccountType=? WHERE SnsAccountID=?";
	private static final String DELETE = "DELETE FROM SNSAccount WHERE SnsAccountID=?";
	private static final String GET_ONE_STMT = "SELECT * FROM SNSAccount WHERE SnsAccountID=?";
	private static final String GET_ALL_STMT = "SELECT * FROM SNSAccount ORDER BY SnsAccountID";
		
	@Override
	public void insert(SNSAccountVO snsAccountVO) {
		
		Connection con = null;
		PreparedStatement pstmt = null;
		
		try {
			
			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(INSERT_STMT);
			
			pstmt.setString(1,snsAccountVO.getArtistID());
			pstmt.setString(2,snsAccountVO.getSnsAccount());
			pstmt.setString(3,snsAccountVO.getSnsLink());
			pstmt.setString(4,snsAccountVO.getSnsAccountType());
			
			pstmt.executeUpdate();			
			
			
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver." + e.getMessage());
		
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured." + se.getMessage());
		
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if(con != null) {
				try {
					con.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
		}
	}

	@Override
	public void update(SNSAccountVO snsAccountVO) {
		
		Connection con = null;
		PreparedStatement pstmt = null;
		
		try {
			
			Class.forName(driver);
			con =DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(UPDATE);
			
			
			pstmt.setString(1,snsAccountVO.getArtistID());
			pstmt.setString(2,snsAccountVO.getSnsAccount());
			pstmt.setString(3,snsAccountVO.getSnsLink());
			pstmt.setString(4,snsAccountVO.getSnsAccountType());
			pstmt.setString(5,snsAccountVO.getSnsAccountID());
			
			pstmt.executeUpdate();	
			
			
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver." + e.getMessage());
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured." + se.getMessage());
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
	}

	@Override
	public void delete(String snsAccountID) {

		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(DELETE);

			pstmt.setString(1, snsAccountID);

			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
	}

	@Override
	public SNSAccountVO findByPrimaryKey(String snsAccountID) {

		SNSAccountVO snsAccountVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(GET_ONE_STMT);

			pstmt.setString(1, snsAccountID);

			rs = pstmt.executeQuery();

			while (rs.next()) {
				// empVo �]�٬� Domain objects
				snsAccountVO = new SNSAccountVO();
				snsAccountVO.setSnsAccountID(rs.getString("snsAccountID"));
				snsAccountVO.setArtistID(rs.getString("artistID"));
				snsAccountVO.setSnsAccount(rs.getString("snsAccount"));
				snsAccountVO.setSnsLink(rs.getString("snsLink"));
				snsAccountVO.setSnsAccountType(rs.getString("snsAccountType"));

			}

		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return snsAccountVO;
	}

	@Override
	public List<SNSAccountVO> getAll() {
		List<SNSAccountVO> list = new ArrayList<SNSAccountVO>();
		SNSAccountVO snsAccountVO = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(GET_ALL_STMT);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				snsAccountVO = new SNSAccountVO();
				snsAccountVO.setSnsAccountID(rs.getString("snsAccountID"));
				snsAccountVO.setArtistID(rs.getString("artistID"));
				snsAccountVO.setSnsAccount(rs.getString("snsAccount"));
				snsAccountVO.setSnsLink(rs.getString("snsLink"));
				snsAccountVO.setSnsAccountType(rs.getString("snsAccountType"));

				list.add(snsAccountVO); // Store the row in the list
			}

		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return list;
	}

}
