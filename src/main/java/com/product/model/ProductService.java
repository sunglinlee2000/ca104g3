package com.product.model;

import java.util.List;
import java.util.Map;

public class ProductService {

	private ProductDAO_interface dao;
	
	public ProductService() {
		dao=new ProductDAO();
	}
	
	public String addProduct(String memberID,String artistID,byte[] productPhoto,Integer productAmount, Integer productPrice,String productInfo,String productName,String productStatus) {
			
			ProductVO productVO=new ProductVO();
			
			productVO.setMemberID(memberID);
			productVO.setArtistID(artistID);
			productVO.setProductPhoto(productPhoto);
			productVO.setProductAmount(productAmount);
			productVO.setProductPrice(productPrice);
			productVO.setProductInfo(productInfo);
			productVO.setProductName(productName);
			productVO.setProductStatus(productStatus);
			
			return dao.insert(productVO);
	}
	
	public ProductVO updateProduct(String productID,String memberID,String artistID,byte[] productPhoto,Integer productAmount, Integer productPrice,String productInfo,String productName,String productStatus) {
			
			ProductVO productVO=new ProductVO();
			
			productVO.setProductID(productID);
			productVO.setMemberID(memberID);
			productVO.setArtistID(artistID);
			productVO.setProductPhoto(productPhoto);
			productVO.setProductAmount(productAmount);
			productVO.setProductPrice(productPrice);
			productVO.setProductInfo(productInfo);
			productVO.setProductName(productName);
			productVO.setProductStatus(productStatus);
			
			dao.update(productVO);
			
			return productVO;
	}
	public ProductVO updateProduct_noImg(String productID,String memberID,String artistID,Integer productAmount, Integer productPrice,String productInfo,String productName,String productStatus) {
		
		ProductVO productVO=new ProductVO();
		
		productVO.setProductID(productID);
		productVO.setMemberID(memberID);
		productVO.setArtistID(artistID);
		productVO.setProductAmount(productAmount);
		productVO.setProductPrice(productPrice);
		productVO.setProductInfo(productInfo);
		productVO.setProductName(productName);
		productVO.setProductStatus(productStatus);
		
		dao.update_noImg(productVO);
		
		return productVO;
	}
	
	public ProductVO updateProductStatus(String productID,String productStatus) {
		
		ProductVO productVO=new ProductVO();
		
		productVO.setProductID(productID);
		productVO.setProductStatus(productStatus);
		
		dao.update_ProductStatus(productVO);
		
		return productVO;
	}
	
	public ProductVO getOneProduct(String productID) {
		return dao.findByPrimaryKey(productID);
	}
	public List<ProductVO> getAll(String memberID) {
		return dao.getAll(memberID);
	}
	
	public List<ProductVO> getAll(){
		return dao.getAll();
	}
	public List<ProductVO> getAll(Map<String, String[]> map) {
		return dao.getAll(map);
	}
	public List<ProductVO> getAllByProductReportID(){
		return dao.getAllByProductReportID();
	}
	
}
