package com.product.model;
import java.util.*;



public interface ProductDAO_interface {
	public String insert(ProductVO productVO);
    public void update(ProductVO productVO);
    public void update_noImg(ProductVO productVO);
    public void update_ProductStatus(ProductVO productVO);
    public ProductVO findByPrimaryKey(String productID);
    public List<ProductVO> getAll(String memberID);
    public List<ProductVO> getAll();
    public List<ProductVO> getAllByProductReportID();
    
    
    //萬用複合查詢(傳入參數型態Map)(回傳 List)
    public List<ProductVO> getAll(Map<String,String[]> map);
}
