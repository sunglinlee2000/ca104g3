package com.product.model;

import java.util.Arrays;

public class ProductVO implements java.io.Serializable {
	private String  productID;
	private String	memberID;
	private String  artistID;
	private byte[]	productPhoto;
	private Integer	productAmount;
	private Integer productPrice;
	private String	productInfo;
	private String	productName;
	private String 	productStatus;
	private Integer quantity;
	

	public ProductVO() {
		super();
	}

	//購物車使用
	public ProductVO(String productID, String memberID, Integer productPrice, String productName, Integer quantity) {
		super();
		this.productID = productID;
		this.memberID = memberID;
		this.productPrice = productPrice;
		this.productName = productName;
		this.quantity = quantity;
	}


	public String getProductID() {
		return productID;
	}


	public void setProductID(String productID) {
		this.productID = productID;
	}


	public String getMemberID() {
		return memberID;
	}


	public void setMemberID(String memberID) {
		this.memberID = memberID;
	}


	public String getArtistID() {
		return artistID;
	}


	public void setArtistID(String artistID) {
		this.artistID = artistID;
	}


	public byte[] getProductPhoto() {
		return productPhoto;
	}


	public void setProductPhoto(byte[] productPhoto) {
		this.productPhoto = productPhoto;
	}


	public Integer getProductAmount() {
		return productAmount;
	}


	public void setProductAmount(Integer productAmount) {
		this.productAmount = productAmount;
	}


	public Integer getProductPrice() {
		return productPrice;
	}


	public void setProductPrice(Integer productPrice) {
		this.productPrice = productPrice;
	}


	public String getProductInfo() {
		return productInfo;
	}


	public void setProductInfo(String productInfo) {
		this.productInfo = productInfo;
	}


	public String getProductName() {
		return productName;
	}


	public void setProductName(String productName) {
		this.productName = productName;
	}


	public String getProductStatus() {
		return productStatus;
	}


	public void setProductStatus(String productStatus) {
		this.productStatus = productStatus;
	}


	public Integer getQuantity() {
		return quantity;
	}


	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	@Override
	public String toString() {
		return "ProductVO [productID=" + productID + ", memberID=" + memberID + ", artistID=" + artistID
				+ ", productPhoto=" + Arrays.toString(productPhoto) + ", productAmount=" + productAmount
				+ ", productPrice=" + productPrice + ", productInfo=" + productInfo + ", productName=" + productName
				+ ", productStatus=" + productStatus + ", quantity=" + quantity + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((productID == null) ? 0 : productID.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProductVO other = (ProductVO) obj;
		if (productID == null) {
			if (other.productID != null)
				return false;
		} else if (!productID.equals(other.productID))
			return false;
		return true;
	}

	
	
	
	
}
