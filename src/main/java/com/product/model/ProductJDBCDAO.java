package com.product.model;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ProductJDBCDAO implements ProductDAO_interface{
	String driver = "oracle.jdbc.driver.OracleDriver";
	String url = "jdbc:oracle:thin:@localhost:1521:XE";
	String userid = "CA104G3";
	String password = "123456";
	
	private static final String INSERT_STMT = 
			"INSERT INTO product(productid,memberid,artistid,productphoto,productamount,productprice,productinfo,productname,productstatus,quantity) "
			+ "VALUES ('P'||LPAD(to_char(PRODUCT_SEQ.NEXTVAL), 6, '0'),?,?,?,?,?,?,?,?,?)";
//	private static final String UPDATE = 
//			"UPDATE product SET memberid=?, artistid=?, productphoto=?, productamount=?, productprice=?, productinfo=?,productname=?,productstatus=? quantity=? where productid = ?";
	private static final String GET_ONE_STMT = 
			"SELECT * FROM product where productid = ?";
	private static final String GET_ALL_STMT = 
			"SELECT * FROM product order by productid";
	private static final String UPDATE = 
			"UPDATE product SET  productphoto=? where productid = ?";
	@Override
	public String insert(ProductVO productVO) {
		Connection con = null;
		PreparedStatement pstmt = null;
		String next_productID = null; 

		try {
			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, password);
			 String cols[] = {"PRODUCTID"}; 
			 pstmt = con.prepareStatement(INSERT_STMT, cols); 
			
			
			pstmt.setString(1, productVO.getMemberID());
			pstmt.setString(2,productVO.getArtistID());
			pstmt.setBytes(3,productVO.getProductPhoto());
			pstmt.setInt(4, productVO.getProductAmount());
			pstmt.setInt(5, productVO.getProductPrice());
			pstmt.setString(6,productVO.getProductInfo());
			pstmt.setString(7, productVO.getProductName());
			pstmt.setString(8,productVO.getProductStatus());
			
			
			pstmt.executeUpdate();
			
			ResultSet rs = pstmt.getGeneratedKeys(); 
			   if(rs.next()) { 
				   next_productID = rs.getString(1); 
			   } 
			
		
		}catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		}catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return next_productID;
	}

	@Override
	public void update(ProductVO productVO) {
		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, password);
			pstmt = con.prepareStatement(UPDATE);

			
			
			pstmt.setBytes(1,productVO.getProductPhoto());
		
			pstmt.setString(2, productVO.getProductID());
			
			
			
			
//			pstmt.setString(1, productVO.getMemberID());
//			pstmt.setString(2,productVO.getArtistID());
//			pstmt.setBytes(3,productVO.getProductPhoto());
//			pstmt.setInt(4, productVO.getProductAmount());
//			pstmt.setInt(5, productVO.getProductPrice());
//			pstmt.setString(6,productVO.getProductInfo());
//			pstmt.setString(7, productVO.getProductName());
//			pstmt.setString(8,productVO.getProductStatus());
//			pstmt.setString(9, productVO.getProductID());
//			pstmt.setInt(9, productVO.getQuantity());

			pstmt.executeUpdate();

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		
	}

	

	@Override
	public ProductVO findByPrimaryKey(String productID) {
		ProductVO productVO = null;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, password);
			pstmt = con.prepareStatement(GET_ONE_STMT);

			pstmt.setString(1, productID);

			rs = pstmt.executeQuery();

			while (rs.next()) {
				
				productVO = new ProductVO();
				productVO.setProductID(rs.getString("productid"));
				productVO.setMemberID(rs.getString("memberid"));
				productVO.setArtistID(rs.getString("artistid"));
				productVO.setProductPhoto(rs.getBytes("productphoto"));
				productVO.setProductAmount(rs.getInt("productamount"));
				productVO.setProductPrice(rs.getInt("productprice"));
				productVO.setProductInfo(rs.getString("productinfo"));
				productVO.setProductName(rs.getString("productname"));
				productVO.setProductStatus(rs.getString("productstatus"));
				productVO.setQuantity(rs.getInt("quantity"));
			}

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return productVO;
	}

	@Override
	public List<ProductVO> getAll() {
		List<ProductVO> list = new ArrayList<ProductVO>();
		ProductVO productVO = null;

		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, password);
			pstmt = con.prepareStatement(GET_ALL_STMT);
			rs = pstmt.executeQuery();

			while (rs.next()) {
				
				productVO = new ProductVO();
				productVO.setProductID(rs.getString("productid"));
				productVO.setMemberID(rs.getString("memberid"));
				productVO.setArtistID(rs.getString("artistid"));
				productVO.setProductPhoto(rs.getBytes("productphoto"));
				productVO.setProductAmount(rs.getInt("productamount"));
				productVO.setProductPrice(rs.getInt("productprice"));
				productVO.setProductInfo(rs.getString("productinfo"));
				productVO.setProductName(rs.getString("productname"));
				productVO.setProductStatus(rs.getString("productstatus"));
				productVO.setQuantity(rs.getInt("quantity"));
				list.add(productVO); // Store the row in the list
			}

			// Handle any driver errors
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. "
					+ e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. "
					+ se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return list;
	}
	public static void main(String[] args) {
		ProductJDBCDAO pjDAO=new ProductJDBCDAO();
//		1.-----------新增---------------
//		ProductVO pdvo=new ProductVO();
//		pdvo.setMemberID("M000001");
//		pdvo.setArtistID("ART000001");
//		byte[] pic = null;
//		try {
//			pic = getPictureByteArray("WebContent/zin/1.PNG");
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		pdvo.setProductPhoto(pic);
//		pdvo.setProductAmount(10000);
//		pdvo.setProductPrice(100000);
//		pdvo.setProductInfo("fffff");
//		pdvo.setProductName("ffffffff");
//		pdvo.setProductStatus("PRODUCT_ON");
//		pdvo.setQuantity(555);
//		pjDAO.insert(pdvo);
//		System.out.println("success!");
		
//		
//		System.out.println("�s�W���\!");
//		2.-------------�ק�----------------------------
//		ProductVO pdvo2=new ProductVO();
//		pdvo2.setProductID("P00009");
//		pdvo2.setMemberID("100000");
//		pdvo2.setArtistID("100000");
//		byte[] pic2 = null;
//		try {
//			pic2 = getPictureByteArray("WebContent/Product/02.PNG");
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		pdvo2.setProductPhoto(pic2);
//		pdvo2.setProductAmount(10000);
//		pdvo2.setProductPrice(100000);
//		pdvo2.setProductInfo("�s�鵲�̪��F�F");
//		pdvo2.setProductName("�s�鵲��123");
//		pdvo2.setProductStatus("PRODUCT_OFF");
//		
//		pjDAO.update(pdvo2);
//		
//		System.out.println("�ק令�\!");
//		3.----------�R��-------------------------		
//		pjDAO.delete("P00010");
//		System.out.println("�R�����\!");
//		----------�d�߳浧-----------------------
//		ProductVO pdvo3=pjDAO.findByPrimaryKey("P00009");
//		System.out.println(pdvo3.getProductID());
//		System.out.println(pdvo3.getMemberID());
//		System.out.println(pdvo3.getArtistID());
//		System.out.println(pdvo3.getProductPhoto());
//		System.out.println(pdvo3.getProductAmount());
//		System.out.println(pdvo3.getProductPrice());
//		System.out.println(pdvo3.getProductInfo());
//		System.out.println(pdvo3.getProductName());
//		System.out.println(pdvo3.getProductStatus());
//		System.out.println("--------------------------------");
//		System.out.println("�d�߳浧���\!!!!");
//		---------�d�ߥ���--------------------------
//		List<ProductVO> list=pjDAO.getAll();
//		for(ProductVO pdvo4:list) {
//			System.out.println(pdvo4.getProductID());
//			System.out.println(pdvo4.getMemberID());
//			System.out.println(pdvo4.getArtistID());
//			System.out.println(pdvo4.getProductPhoto());
//			System.out.println(pdvo4.getProductAmount());
//			System.out.println(pdvo4.getProductPrice());
//			System.out.println(pdvo4.getProductInfo());
//			System.out.println(pdvo4.getProductName());
//			System.out.println(pdvo4.getProductStatus());	
//			System.out.println("-----------------------------");
//		}
//			System.out.println("�d����OK!!!!");
			ProductVO productVO=new ProductVO();
			for (int i = 1 ; i <= 11 ; i++) {
			    byte[] pic1 = null;
			
			    if(10 <= i) {
			    	productVO.setProductID("P0000"+i);
			    } else {
			    	productVO.setProductID("P00000"+i);
			    }
			    try {
			     pic1 = getPictureByteArray("WebContent/front-end/res/img/mall/zin/"+i+".jpg");
			    
			    } catch(IOException e ) {
			     e.printStackTrace(System.err);
			    }
			    productVO.setProductPhoto(pic1);
			    
			    pjDAO.update(productVO);
			    System.out.println("OK");
			  }
	
	}
	public static byte[] getPictureByteArray(String path) throws IOException {
		File file = new File(path);
		FileInputStream fis = new FileInputStream(file);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		byte[] buffer = new byte[8192];
		int i;
		while ((i = fis.read(buffer)) != -1) {
			baos.write(buffer, 0, i);
		}
		baos.close();
		fis.close();

		return baos.toByteArray();
	}

	@Override
	public List<ProductVO> getAll(Map<String, String[]> map) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<ProductVO> getAll(String memberID) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void update_noImg(ProductVO productVO) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update_ProductStatus(ProductVO productVO) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<ProductVO> getAllByProductReportID() {
		// TODO Auto-generated method stub
		return null;
	}

}
