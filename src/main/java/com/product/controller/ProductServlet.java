package com.product.controller;

import java.io.*;
import java.util.*;

import javax.servlet.*;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.*;

import org.json.JSONException;
import org.json.JSONObject;

import com.product.model.*;

import product.serverEndpoint.*;

@MultipartConfig(fileSizeThreshold = 1024*1024, maxFileSize = 5*1024*1024, maxRequestSize = 5*5*1024*1024)

public class ProductServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	public void doGet(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		doPost(req, res);
	}

	@SuppressWarnings("unused")
	public void doPost(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		req.setCharacterEncoding("UTF-8");
		

		HttpSession session = req.getSession();
		@SuppressWarnings("unchecked")
		
		String action = req.getParameter("action");
		System.out.println(action);
		if ("getOne_For_Display".equals(action)) { 

			
				String productID = req.getParameter("productID");
				
				ProductService productSvc = new ProductService();
				ProductVO productVO = productSvc.getOneProduct(productID);
				
				String productName=productVO.getProductName();
				System.out.println(productName);
				String productInfo=productVO.getProductInfo();
				String productID1=productVO.getProductID();
				Integer productPrice=productVO.getProductPrice();
				JSONObject obj = new JSONObject(); 
				
				try { 
					obj.put("productName", productName); 
					obj.put("productInfo", productInfo); 
					obj.put("productID1", productID1); 
					obj.put("productPrice", productPrice); 
				} catch (JSONException e) { 
					e.printStackTrace(); 
				} 
				res.setContentType("text/plain"); 
				res.setCharacterEncoding("UTF-8"); 
				PrintWriter out = res.getWriter(); 
				out.write(obj.toString()); 
				out.flush(); 
				out.close();
		}
		
		
		if ("getOne_For_Update".equals(action)) { 

			List<String> errorMsgs = new LinkedList<String>();
			// Store this set in the request scope, in case we need to
			// send the ErrorPage view.
			req.setAttribute("errorMsgs", errorMsgs);
			
			String requestURL = req.getParameter("requestURL");
			
			try {
				/***************************1.接收請求參數****************************************/
				
				String productID=new String(req.getParameter("productID").trim());
				
				/***************************2.開始查詢資料****************************************/
				
				ProductService productSvc= new ProductService();
				ProductVO productVO=productSvc.getOneProduct(productID);
								
				/***************************3.查詢完成,準備轉交(Send the Success view)************/
				req.setAttribute("productVO", productVO);         // 資料庫取出的empVO物件,存入req
				String url = "/front-end/product/update_product_input.jsp";
				RequestDispatcher successView = req.getRequestDispatcher(url);// 成功轉交 update_emp_input.jsp
				successView.forward(req, res);
				

				/***************************其他可能的錯誤處理**********************************/
			} catch (Exception e) {
				errorMsgs.add("無法取得要修改的資料:" + e.getMessage());
				RequestDispatcher failureView = req
						.getRequestDispatcher(requestURL);
				failureView.forward(req, res);
			}
		}
		
		
		if ("update".equals(action)) { // 來自listAllProduct.jsp的請求
			List<String> errorMsgs = new LinkedList<String>();
			// Store this set in the request scope, in case we need to
			// send the ErrorPage view.
			req.setAttribute("errorMsgs", errorMsgs);
			String requestURL = req.getParameter("requestURL");
			try {
				
				
				String productID=new String(req.getParameter("productID").trim());
				String memberID = req.getParameter("memberID");
				String enameReg = "^[(a-zA-Z0-9)]{7,7}$";
				if (memberID == null || memberID.trim().length() == 0) {
					errorMsgs.add("會員編號: 請勿空白");
				} else if(!memberID.trim().matches(enameReg)) { //以下練習正則(規)表示式(regular-expression)
					errorMsgs.add("會員編號: 英文字母、數字和, 且長度必需在7");
	            }
				
				String artistID = req.getParameter("artistID").trim();
				if (artistID == null || artistID.trim().length() == 0) {
					errorMsgs.add("藝人編號: 請勿空白");
				} else if(!memberID.trim().matches(enameReg)) { //以下練習正則(規)表示式(regular-expression)
					errorMsgs.add("藝人編號: 英文字母、數字和_ , 且長度必需在7");
	            }

				Integer productAmount = null;
				try {
					productAmount = new Integer(req.getParameter("productAmount").trim());
				} catch (NumberFormatException e) {
					productAmount = 0;
					errorMsgs.add("產品數量請填數字.");
				}
				String productInfo = req.getParameter("productInfo").trim();
				if (productInfo == null || productInfo.trim().length() == 0) {
					errorMsgs.add("產品資訊: 請勿空白");
				}
				Integer productPrice = null;
				try {
					productPrice = new Integer(req.getParameter("productPrice").trim());
				} catch (NumberFormatException e) {
					productPrice = 0;
					errorMsgs.add("商品價格請填數字.");
				}
				
				String productName = req.getParameter("productName").trim();
				if (productName == null || productName.trim().length() == 0) {
					errorMsgs.add("產品資訊: 請勿空白");
				}
				
				
				
			
				Part part = req.getPart("productPhoto");
				long partSize=part.getSize();
				System.out.println(partSize);
				InputStream is = part.getInputStream();
				
				byte[] productPhoto = new byte[is.available()];
				
				is.read(productPhoto);
				
				String productStatus=new String(req.getParameter("productStatus").trim());
				
				ProductVO productVO = new ProductVO();
//				productVO.setProductID(productID);
//				productVO.setMemberID(memberID);
//				productVO.setArtistID(artistID);
//				productVO.setProductPhoto(productPhoto);
//				productVO.setProductAmount(productAmount);
//				productVO.setProductInfo(productInfo);
//				productVO.setProductPrice(productPrice);
//				productVO.setProductName(productName);
//				productVO.setProductStatus(productStatus);
				
				

				// Send the use back to the form, if there were errors
				if (!errorMsgs.isEmpty()) {
					req.setAttribute("productVO", productVO); // 含有輸入格式錯誤的empVO物件,也存入req
					RequestDispatcher failureView = req
							.getRequestDispatcher("/front-end/product/update_product_input.jsp");
					failureView.forward(req, res);
					return; //程式中斷
				}
				
				/***************************2.開始修改資料*****************************************/
				ProductService productSvc = new ProductService();
				if(partSize==0) {
					productVO = productSvc.updateProduct_noImg(productID, memberID, artistID, productAmount, productPrice, productInfo, productName, productStatus);
					System.out.println("1111111");
				}else{
					productVO = productSvc.updateProduct(productID, memberID, artistID, productPhoto, productAmount, productPrice, productInfo, productName, productStatus);
					System.out.println("2222222");
				}
				/***************************3.修改完成,準備轉交(Send the Success view)*************/
				req.setAttribute("productVO", productVO); 
				String url = requestURL;
				RequestDispatcher successView = req.getRequestDispatcher(url); 
				successView.forward(req, res);
				

				/***************************其他可能的錯誤處理*************************************/
			} catch (Exception e) {
				errorMsgs.add("修改資料失敗:"+e.getMessage());
				RequestDispatcher failureView = req
						.getRequestDispatcher("/front-end/product/update_product_input.jsp");
				failureView.forward(req, res);
			}
		}

		 if ("insert".equals(action)) { 
				List<String> errorMsgs = new LinkedList<String>();
				// Store this set in the request scope, in case we need to
				// send the ErrorPage view.
				req.setAttribute("errorMsgs", errorMsgs);

				try {
					
					/***********************1.接收請求參數 - 輸入格式的錯誤處理*************************/
					String productName = req.getParameter("productName").trim();
					
					if (productName == null || productName.trim().length() == 0) {
						errorMsgs.add("產品名稱: 請勿空白");
					} 
					
					String memberID = req.getParameter("memberID").trim();
//					
//					if (memberID == null || memberID.trim().length() == 0) {
//						errorMsgs.add("會員編號勿空白");
//					}
					
					String artistID = req.getParameter("artistID").trim();
					if (artistID.equals("0")) {
						errorMsgs.add("藝人分類: 請選擇");
					} 
						
					Integer productAmount = null;
					try {
						productAmount = new Integer(req.getParameter("productAmount").trim());
					} catch (NumberFormatException e) {
						productAmount = 0;
						errorMsgs.add("數量請填數字.");
					}
					Part part = req.getPart("productPhoto");
					
					InputStream is = part.getInputStream();
					byte[] productPhoto = new byte[is.available()];;
					is.read(productPhoto);
					
					Integer productPrice = null;
					try {
						productPrice = new Integer(req.getParameter("productPrice").trim());
					} catch (NumberFormatException e) {
						productPrice = 0;
						errorMsgs.add("價格請填數字.");
					}
					String productInfo = req.getParameter("productInfo");
					
					if (productInfo == null || productInfo.trim().length() == 0) {
						errorMsgs.add("商品資訊: 請勿空白");
					} 
					String productStatus = req.getParameter("productStatus");
					if (productStatus.equals("0")) {
						errorMsgs.add("狀態: 請選擇");
					} 
					
					
					ProductVO productVO=new ProductVO();
//					productVO.setMemberID(memberID);
					productVO.setProductName(productName);
					productVO.setArtistID(artistID);
					productVO.setProductAmount(productAmount);
					productVO.setProductPrice(productPrice);
					productVO.setProductInfo(productInfo);
					productVO.setProductPhoto(productPhoto);
					productVO.setProductStatus(productStatus);
						

					// Send the use back to the form, if there were errors
					if (!errorMsgs.isEmpty()) {
						req.setAttribute("productVO", productVO); // 含有輸入格式錯誤的empVO物件,也存入req
						RequestDispatcher failureView = req
								.getRequestDispatcher("/front-end/product/product_on.jsp");
						failureView.forward(req, res);
						return;
					}
				
					/***************************2.開始新增資料***************************************/
					ProductService productSvc = new ProductService();
					String productID = productSvc.addProduct(memberID, artistID, productPhoto, productAmount, productPrice, productInfo, productName, productStatus);
					MyProductServer myProductServer=new MyProductServer();
					JSONObject obj = new JSONObject(); 
					try { 
						obj.put("productID", productID);
						obj.put("productName", productName);
						obj.put("productPrice", productPrice);
						obj.put("productInfo", productInfo);
						
					} catch (JSONException e) { 
						e.printStackTrace(); 
					} 
					
					myProductServer.onMessage(null, obj.toString());
					/***************************3.新增完成,準備轉交(Send the Success view)***********/
					String url = "/front-end/product/Seller.jsp";
					res.sendRedirect(req.getContextPath()+url);
					return;
				
					/***************************其他可能的錯誤處理**********************************/
				} catch (Exception e) {
					errorMsgs.add(e.getMessage());
					RequestDispatcher failureView = req
							.getRequestDispatcher("/front-end/product/product_on.jsp");
					failureView.forward(req, res);
				}
			}
		 if ("Mall_ByCompositeQuery".equals(action)) { // 來自Mall.jsp的複合查詢請求
				List<String> errorMsgs = new LinkedList<String>();
				// Store this set in the request scope, in case we need to
				// send the ErrorPage view.
				req.setAttribute("errorMsgs", errorMsgs);
				System.out.println("進入");
				try {
					
					/***************************1.將輸入資料轉為Map**********************************/ 
					//採用Map<String,String[]> getParameterMap()的方法 
					//注意:an immutable java.util.Map 
					//Map<String, String[]> map = req.getParameterMap();
					
					Map<String, String[]> map = (Map<String, String[]>) session.getAttribute("map");
					System.out.println(map);
					if (req.getParameter("whichPage") == null){
						HashMap<String, String[]> map1 = new HashMap<String, String[]>(req.getParameterMap());
						session.setAttribute("map",map1);
						map = map1;
					} 
					
					/***************************2.開始複合查詢***************************************/
					
					ProductService productSvc = new ProductService();
					List<ProductVO> list  = productSvc.getAll(map);
					System.out.println(list.size()+"  ____ ");
					
					
					/***************************3.查詢完成,準備轉交(Send the Success view)************/
					session.setAttribute("Mall_ByCompsiteQuery", list); // 資料庫取出的list物件,存入request
					String url ="/front-end/product/Mall_ByCompositeQuery.jsp";
					RequestDispatcher successView = req.getRequestDispatcher("/front-end/product/Mall_ByCompositeQuery.jsp"); // 成功轉交Mall_ByCompositeQuery.jsp
					
					successView.forward(req, res);
					
					
					
					/***************************其他可能的錯誤處理**********************************/
				} catch (Exception e) {
					errorMsgs.add(" Error _ Product ___  " + e.getMessage());
					RequestDispatcher failureView = req.getRequestDispatcher("/front-end/product/Mall.jsp");
					failureView.forward(req, res);
				}
		}
		 
		 //判斷是否至少成為買家才能進商城 叫他去個人資料那驗證手機
		 if ("isBuyer".equals(action)) { // 來自Mall.jsp的複合查詢請求
				
				System.out.println("進入");
				String memberStatus=req.getParameter("memberStatus");	//抓會員目前狀態
				Boolean status=false;
				JSONObject obj = new JSONObject(); 
				if("一般會員".equals(memberStatus)) {
					 status= true;
				}
				try { 
					obj.put("status", status); 
				} catch (JSONException e) { 
					e.printStackTrace(); 
				} 
				res.setContentType("text/plain"); 
				res.setCharacterEncoding("UTF-8"); 
				PrintWriter out = res.getWriter(); 
				out.write(obj.toString()); 
				out.flush(); 
				out.close(); 				
		}
		//判斷是否至少成為賣方才能進入賣家中心 叫他去個人資料那驗證銀行帳戶
		 if ("isSeller".equals(action)) { 
				
				System.out.println("進入seller");
				String memberStatus=req.getParameter("memberStatus");	//抓會員目前狀態
				Boolean status=false;
				JSONObject obj = new JSONObject(); 
				if("買家".equals(memberStatus)) {
					 status= true;
				}
				try { 
					obj.put("status", status);
					System.out.println(status);
				} catch (JSONException e) { 
					e.printStackTrace(); 
				} 
				res.setContentType("text/plain"); 
				res.setCharacterEncoding("UTF-8"); 
				PrintWriter out = res.getWriter(); 
				out.write(obj.toString()); 
				out.flush(); 
				out.close(); 				
		}
		 //刪除購物車內容
		 if (action.equals("DELETE")) {
			 List<ProductVO> buylist = (Vector<ProductVO>) session.getAttribute("shoppingcart");
				String del = req.getParameter("del");
				int d = Integer.parseInt(del);
				buylist.remove(d);
				
				session.setAttribute("shoppingcart", buylist);
				String requestURL = req.getParameter("requestURL");
				RequestDispatcher successView = req.getRequestDispatcher(requestURL);
				successView.forward(req,res); 
		}
		 //加入購物車
		 if (action.equals("ADD")) {
			 List<ProductVO> buylist = (Vector<ProductVO>) session.getAttribute("shoppingcart");
			 List<String> id = new ArrayList<String>();
			 
			 if(buylist!=null) {
				 for(ProductVO b : buylist) {
					id.add(b.getProductID());
				 }
			 }
			 
			 System.out.println("進入購物車");
			 System.out.println(buylist);
				// 取得後來新增的商品
				ProductVO product = getProduct(req);
				System.out.println("xxxxxxxxxxxxxxxxxxxxxxx" + product);
				JSONObject obj = new JSONObject();

				if (buylist == null) {
					buylist = new Vector<ProductVO>();
					buylist.add(product);
				} 
				else{
					
					if (id.contains(product.getProductID())) {
						ProductVO innerproduct = buylist.get(buylist.indexOf(product));
						innerproduct.setQuantity(innerproduct.getQuantity() + product.getQuantity());
						
					} else {
						buylist.add(product);
					}
					
				}
				session.setAttribute("shoppingcart", buylist);
				res.setContentType("text/plain"); 
				res.setCharacterEncoding("UTF-8"); 
				PrintWriter out = res.getWriter(); 
				out.write(obj.toString()); 
				out.flush(); 
				out.close();
			}
		 //結帳
		 if (action.equals("CHECKOUT")) {
			 List<ProductVO> buylist = (Vector<ProductVO>) session.getAttribute("shoppingcart");
				Integer total = 0;
				for (int i = 0; i < buylist.size(); i++) {
					ProductVO order = buylist.get(i);
					Integer price = order.getProductPrice();
					Integer quantity = order.getQuantity();
					total += (price * quantity);
				}

				String amount = String.valueOf(total);
				session.setAttribute("amount", amount);
				String url = "/front-end/product/Checkout.jsp";
				RequestDispatcher rd = req.getRequestDispatcher(url);
				rd.forward(req, res);
			}
		 
//		if("ADD".equals(action) || "DELETE".equals(action) ||"CHECKOUT".equals(action)) {
//			List<ProductVO> buylist = (Vector<ProductVO>) session.getAttribute("shoppingcart");
//			
//		 if (!action.equals("CHECKOUT")) {
//				// 刪除購物車中的商品
//				if (action.equals("DELETE")) {
//					String del = req.getParameter("del");
//					int d = Integer.parseInt(del);
//					buylist.remove(d);
//				}
//				// 新增商品至購物車中
//				else if (action.equals("ADD")) {
//					
//					// 取得後來新增的商品
//					ProductVO product = getProduct(req);
//					
//
//					if (buylist == null) {
//						buylist = new Vector<ProductVO>();
//						buylist.add(product);
//					} else {
//						if (buylist.contains(product)) {
//							ProductVO innerproduct = buylist.get(buylist.indexOf(product));
//							innerproduct.setQuantity(innerproduct.getQuantity() + product.getQuantity());
//						} else {
//							buylist.add(product);
//						}
//					}
//				}
//
//				//-------------------------------------------------
//				session.setAttribute("shoppingcart", buylist);
//				String requestURL = req.getParameter("requestURL");
//				RequestDispatcher successView = req.getRequestDispatcher(requestURL);
//				successView.forward(req,res); 
//				
//			}
//
//			// 結帳，計算購物車商品價錢總數
//			else if (action.equals("CHECKOUT")) {
//				Integer total = 0;
//				for (int i = 0; i < buylist.size(); i++) {
//					ProductVO order = buylist.get(i);
//					Integer price = order.getProductPrice();
//					Integer quantity = order.getQuantity();
//					total += (price * quantity);
//				}
//
//				String amount = String.valueOf(total);
//				session.setAttribute("amount", amount);
//				String url = "/front-end/product/Checkout.jsp";
//				RequestDispatcher rd = req.getRequestDispatcher(url);
//				rd.forward(req, res);
//			}
//		}
	}

		private ProductVO getProduct(HttpServletRequest req) {
			String productID = req.getParameter("productID");
			String productName = req.getParameter("productName");
			String memberID = req.getParameter("memberID");		
			Integer productPrice = new Integer(req.getParameter("productPrice").trim());
			Integer quantity = new Integer(req.getParameter("quantity").trim());
			
			ProductVO product = new ProductVO();
			
			product.setProductID(productID);
			product.setProductName(productName);
			product.setMemberID(memberID);
			product.setProductPrice(productPrice);
			product.setQuantity(quantity);
			return product;
		}
}

