package com.product.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.product.model.ProductService;
import com.product.model.ProductVO;

public class ProductImgServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		try {
		req.setCharacterEncoding("UTF-8");
		String productID=req.getParameter("productID");
		ProductService productSvc=new ProductService();
		ProductVO productVO=productSvc.getOneProduct(productID);
		
		byte[] photo=null;
		
		ServletOutputStream out= res.getOutputStream();
		photo= productVO.getProductPhoto();
		
		res.setContentType("image/jpeg");
		res.setContentLength(photo.length);
		
		out.write(photo);
		out.flush();
		out.close();
		}catch(Exception e) {
			e.printStackTrace();
		}
		
	}
	
	
}
