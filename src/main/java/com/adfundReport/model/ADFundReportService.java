package com.adfundReport.model;

import java.sql.Timestamp;
import java.util.List;

public class ADFundReportService {
	
	private ADFundReportDAO_interface dao;
	
	public ADFundReportService() {
		dao = new ADFundReportDAO();
	}
	
	public ADFundReportVO addADFundReport(String adfundID, String memberID, String administratorID, String adfundReportContent,
			Timestamp adfundReportDate, String adfundReportStatus, String adfundReportReason) {
		
		ADFundReportVO adfundReportVO = new ADFundReportVO();
		
		adfundReportVO.setAdfundID(adfundID);
		adfundReportVO.setMemberID(memberID);
		adfundReportVO.setAdministratorID(administratorID);
		adfundReportVO.setAdfundReportContent(adfundReportContent);
		adfundReportVO.setAdfundReportDate(adfundReportDate);
		adfundReportVO.setAdfundReportStatus(adfundReportStatus);
		adfundReportVO.setAdfundReportReason(adfundReportReason);
		dao.insert(adfundReportVO);
		
		return adfundReportVO;
	}
	
	public ADFundReportVO updateADFundStatusReport(String adfundReportID, String adfundReportStatus) {
		
		ADFundReportVO adfundReportVO = new ADFundReportVO();
		
		adfundReportVO.setAdfundReportID(adfundReportID);
		adfundReportVO.setAdfundReportStatus(adfundReportStatus);
		
		dao.update(adfundReportVO);
		
		return adfundReportVO;
	}
	
	public ADFundReportVO getOneADFundReport(String adfundReportID) {
		return dao.findByPrimaryKey(adfundReportID);
	}
	
	public List<ADFundReportVO> getAllADFundReport(){
		return dao.getAll();
	}
	
	public Integer getADRepoetCount(){
		return dao.getADRepoetCount();
	}

}
