package com.adfundReport.model;

import java.sql.*;

public class ADFundReportVO implements java.io.Serializable{
	private String adfundReportID;
	private String adfundID;
	private String memberID;
	private String administratorID;
	private String adfundReportContent;
	private Timestamp adfundReportDate;
	private String adfundReportStatus;
	private String adfundReportReason;
	
	public String getAdfundReportID() {
		return adfundReportID;
	}
	public void setAdfundReportID(String adfundReportID) {
		this.adfundReportID = adfundReportID;
	}
	public String getAdfundID() {
		return adfundID;
	}
	public void setAdfundID(String adfundID) {
		this.adfundID = adfundID;
	}
	public String getMemberID() {
		return memberID;
	}
	public void setMemberID(String memberID) {
		this.memberID = memberID;
	}
	public String getAdministratorID() {
		return administratorID;
	}
	public void setAdministratorID(String administratorID) {
		this.administratorID = administratorID;
	}
	public String getAdfundReportContent() {
		return adfundReportContent;
	}
	public void setAdfundReportContent(String adfundReportContent) {
		this.adfundReportContent = adfundReportContent;
	}
	public Timestamp getAdfundReportDate() {
		return adfundReportDate;
	}
	public void setAdfundReportDate(Timestamp adfundReportDate) {
		this.adfundReportDate = adfundReportDate;
	}
	public String getAdfundReportStatus() {
		return adfundReportStatus;
	}
	public void setAdfundReportStatus(String adfundReportStatus) {
		this.adfundReportStatus = adfundReportStatus;
	}
	public String getAdfundReportReason() {
		return adfundReportReason;
	}
	public void setAdfundReportReason(String adfundReportReason) {
		this.adfundReportReason = adfundReportReason;
	}
	
	
	

}
