package com.adfundReport.model;

import java.sql.*;
import java.util.*;

public class ADFundReportJDBCDAO implements ADFundReportDAO_interface {
	String driver = "oracle.jdbc.driver.OracleDriver";
	String url = "jdbc:oracle:thin:@localhost:1521:XE";
	String userid = "CA104G3";
	String passwd = "123456";

	private static final String INSERT_STMT = "INSERT INTO ADFUNDREPORT VALUES "
			+ "('ADR'||(LPAD(TO_CHAR(ADFUNDREPORT_SEQ.NEXTVAL),6,'0')),?,?,?,?,?,?,?)";
	private static final String GET_ALL_STMT = "SELECT * FROM ADFUNDREPORT";
	private static final String GET_ONE_STMT = "SELECT * FROM ADFUNDREPORT WHERE ADFUNDREPORTID = ?";
	private static final String UPDATE = "UPDATE ADFUNDREPORT SET ADFUNDREPORTSTATUS=? WHERE ADFUNDREPORTID = ?";

	@Override
	public void insert(ADFundReportVO adfundRepVO) {

		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(INSERT_STMT);

			pstmt.setString(1, adfundRepVO.getAdfundID());
			pstmt.setString(2, adfundRepVO.getMemberID());
			pstmt.setString(3, adfundRepVO.getAdministratorID());
			pstmt.setString(4, adfundRepVO.getAdfundReportContent());
			pstmt.setTimestamp(5, adfundRepVO.getAdfundReportDate());
			pstmt.setString(6, adfundRepVO.getAdfundReportStatus());
			pstmt.setString(7, adfundRepVO.getAdfundReportReason());

			pstmt.executeUpdate();

		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. " + e.getMessage());

		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. " + se.getMessage());

		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}

			if (con != null) {
				try {
					con.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
		}
	}

	@Override
	public void update(ADFundReportVO adfundRepVO) {

		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(UPDATE);

			pstmt.setString(1, adfundRepVO.getAdfundReportStatus());
			pstmt.setString(2, adfundRepVO.getAdfundReportID());

			pstmt.executeUpdate();

		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. " + e.getMessage());

		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. " + se.getMessage());

		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}

			if (con != null) {
				try {
					con.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
		}
	}

	@Override
	public ADFundReportVO findByPrimaryKey(String adfundreportID) {

		ADFundReportVO adfrVO = new ADFundReportVO();
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(GET_ONE_STMT);

			pstmt.setString(1, adfundreportID);

			rs = pstmt.executeQuery();

			while (rs.next()) {
				adfrVO = new ADFundReportVO();
				adfrVO.setAdfundReportID(rs.getString("adfundreportid"));
				adfrVO.setAdfundID(rs.getString("adfundid"));
				adfrVO.setMemberID(rs.getString("memberid"));
				adfrVO.setAdfundReportContent(rs.getString("adfundreportcontent"));
				adfrVO.setAdfundReportDate(rs.getTimestamp("adfundreportdate"));
				adfrVO.setAdfundReportStatus(rs.getString("adfundreportstatus"));
				adfrVO.setAdfundReportReason(rs.getString("adfundreportreason"));
			}
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Couldn't load database driver. " + e.getMessage());

		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. " + se.getMessage());

		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

		return adfrVO;
	}

	@Override
	public List<ADFundReportVO> getAll() {
		List<ADFundReportVO> adfrlist = new ArrayList<ADFundReportVO>();
		ADFundReportVO adfrVO = new ADFundReportVO();
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			Class.forName(driver);
			con = DriverManager.getConnection(url, userid, passwd);
			pstmt = con.prepareStatement(GET_ALL_STMT);

			rs = pstmt.executeQuery();

			while (rs.next()) {
				adfrVO = new ADFundReportVO();
				adfrVO.setAdfundReportID(rs.getString("adfundreportid"));
				adfrVO.setAdfundID(rs.getString("adfundid"));
				adfrVO.setMemberID(rs.getString("memberid"));
				adfrVO.setAdfundReportContent(rs.getString("adfundreportcontent"));
				adfrVO.setAdfundReportDate(rs.getTimestamp("adfundreportdate"));
				adfrVO.setAdfundReportStatus(rs.getString("adfundreportstatus"));
				adfrVO.setAdfundReportReason(rs.getString("adfundreportreason"));
				adfrlist.add(adfrVO);
			}

		} catch (ClassNotFoundException e) {

			throw new RuntimeException("Couldn't load database driver. " + e.getMessage());
			// Handle any SQL errors
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. " + se.getMessage());
			// Clean up JDBC resources
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

		return adfrlist;
	}

	@Override
	public Integer getADRepoetCount() {
		// TODO Auto-generated method stub
		return null;
	}

}
