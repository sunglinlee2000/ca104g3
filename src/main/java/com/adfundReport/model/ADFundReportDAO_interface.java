package com.adfundReport.model;

import java.util.List;


public interface ADFundReportDAO_interface {
	public void insert(ADFundReportVO adfundVO);
	public void update(ADFundReportVO adfundVO);
	public ADFundReportVO  findByPrimaryKey(String adfundID);
	public List<ADFundReportVO> getAll();
	//計算募資檢舉之狀態未審核的筆數
	public Integer getADRepoetCount();

}
