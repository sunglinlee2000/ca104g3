package com.adfundReport.model;

import java.util.List;

public class ADFundReportJDBCDAOTest {

	public static void main(String[] args) {
		ADFundReportJDBCDAO dao = new ADFundReportJDBCDAO();

		// 新增
//		ADFundReportVO adfrVO1 = new ADFundReportVO();
//		adfrVO1.setAdfundID("AD000001");
//		adfrVO1.setMemberID("M000001");
//		adfrVO1.setAdministratorID("A000001");
//		adfrVO1.setAdfundReportContent("");
//		adfrVO1.setAdfundReportDate(java.sql.Timestamp.valueOf("2018-10-22 18:06:30"));
//		adfrVO1.setAdfundReportStatus("REPORT_UNCHECKED");
//		adfrVO1.setAdfundReportReason("BULLY");
//		dao.insert(adfrVO1);
//		System.out.println("..........");

		// 修改
//		ADFundReportVO adfrVO2 = new ADFundReportVO();
//		adfrVO2.setAdfundReportStatus("REPORT_REJECTED");
//		adfrVO2.setAdfundReportID("ADR000001");
//		dao.update(adfrVO2);
//		System.out.println("..........");

		// 查一筆
//		ADFundReportVO adfrVO3 = dao.findByPrimaryKey("ADR000001");
//		System.out.println(adfrVO3.getAdfundReportID());
//		System.out.println(adfrVO3.getAdfundID());
//		System.out.println(adfrVO3.getMemberID());
//		System.out.println(adfrVO3.getAdfundReportStatus());
//		System.out.println(adfrVO3.getAdfundReportContent());
//		System.out.println(adfrVO3.getAdfundReportDate());
//		System.out.println(adfrVO3.getAdfundReportStatus());
//		System.out.println(adfrVO3.getAdfundReportReason());
//		System.out.println("..........");

		// 查多筆
//		List<ADFundReportVO> adfrlist = dao.getAll();
//		for (ADFundReportVO aADFundRep : adfrlist) {
//			System.out.println(aADFundRep.getAdfundReportID());
//			System.out.println(aADFundRep.getAdfundID());
//			System.out.println(aADFundRep.getMemberID());
//			System.out.println(aADFundRep.getAdfundReportStatus());
//			System.out.println(aADFundRep.getAdfundReportContent());
//			System.out.println(aADFundRep.getAdfundReportDate());
//			System.out.println(aADFundRep.getAdfundReportStatus());
//			System.out.println(aADFundRep.getAdfundReportReason());
//			System.out.println("..........");
//		}

	}
}
