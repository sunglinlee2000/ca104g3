package com.adfundReport.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

public class ADFundReportDAO implements ADFundReportDAO_interface {

	private static DataSource ds = null;
	static {
		try {
			Context ctx = new InitialContext();
			ds = (DataSource) ctx.lookup("java:comp/env/jdbc/CA104G3DB");
		} catch (NamingException e) {
			e.printStackTrace();
		}
	}

	private static final String INSERT_STMT = "INSERT INTO ADFUNDREPORT VALUES "
			+ "('ADR'||(LPAD(TO_CHAR(ADFUNDREPORT_SEQ.NEXTVAL),6,'0')),?,?,?,?,?,?,?)";
	private static final String GET_ALL_STMT = "SELECT * FROM ADFUNDREPORT";
	private static final String GET_ONE_STMT = "SELECT * FROM ADFUNDREPORT WHERE ADFUNDREPORTID = ?";
	private static final String UPDATE = "UPDATE ADFUNDREPORT SET ADFUNDREPORTSTATUS=? WHERE ADFUNDREPORTID = ?";
	private static final String GET_ADREPORT_COUNT = "SELECT COUNT(*) AS COUNT FROM ADFUNDREPORT WHERE adfundReportStatus = 'REPORT_UNCHECKED'";

	@Override
	public void insert(ADFundReportVO adfundRepVO) {

		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(INSERT_STMT);

			pstmt.setString(1, adfundRepVO.getAdfundID());
			pstmt.setString(2, adfundRepVO.getMemberID());
			pstmt.setString(3, adfundRepVO.getAdministratorID());
			pstmt.setString(4, adfundRepVO.getAdfundReportContent());
			pstmt.setTimestamp(5, adfundRepVO.getAdfundReportDate());
			pstmt.setString(6, adfundRepVO.getAdfundReportStatus());
			pstmt.setString(7, adfundRepVO.getAdfundReportReason());

			pstmt.executeUpdate();

		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. " + se.getMessage());

		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}

			if (con != null) {
				try {
					con.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
		}
	}

	@Override
	public void update(ADFundReportVO adfundRepVO) {

		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(UPDATE);

			pstmt.setString(1, adfundRepVO.getAdfundReportStatus());
			pstmt.setString(2, adfundRepVO.getAdfundReportID());

			pstmt.executeUpdate();

		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. " + se.getMessage());

		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}

			if (con != null) {
				try {
					con.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
		}
	}

	@Override
	public ADFundReportVO findByPrimaryKey(String adfundreportid) {

		ADFundReportVO adfrVO = new ADFundReportVO();
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ONE_STMT);

			pstmt.setString(1, adfundreportid);

			rs = pstmt.executeQuery();

			while (rs.next()) {
				adfrVO = new ADFundReportVO();
				adfrVO.setAdfundReportID(rs.getString("adfundreportid"));
				adfrVO.setAdfundID(rs.getString("adfundid"));
				adfrVO.setMemberID(rs.getString("memberid"));
				adfrVO.setAdministratorID(rs.getString("administratorID"));
				adfrVO.setAdfundReportContent(rs.getString("adfundreportcontent"));
				adfrVO.setAdfundReportDate(rs.getTimestamp("adfundreportdate"));
				adfrVO.setAdfundReportStatus(rs.getString("adfundreportstatus"));
				adfrVO.setAdfundReportReason(rs.getString("adfundreportreason"));
			}
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. " + se.getMessage());

		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

		return adfrVO;
	}

	@Override
	public List<ADFundReportVO> getAll() {
		List<ADFundReportVO> adfrlist = new ArrayList<ADFundReportVO>();
		ADFundReportVO adfrVO = new ADFundReportVO();
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ALL_STMT);

			rs = pstmt.executeQuery();

			while (rs.next()) {
				adfrVO = new ADFundReportVO();
				adfrVO.setAdfundReportID(rs.getString("adfundreportid"));
				adfrVO.setAdfundID(rs.getString("adfundid"));
				adfrVO.setMemberID(rs.getString("memberid"));
				adfrVO.setAdministratorID(rs.getString("administratorID"));
				adfrVO.setAdfundReportContent(rs.getString("adfundreportcontent"));
				adfrVO.setAdfundReportDate(rs.getTimestamp("adfundreportdate"));
				adfrVO.setAdfundReportStatus(rs.getString("adfundreportstatus"));
				adfrVO.setAdfundReportReason(rs.getString("adfundreportreason"));
				adfrlist.add(adfrVO);
			}

		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. " + se.getMessage());

		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return adfrlist;
	}

	@Override
	public Integer getADRepoetCount() {
		Integer count = 0;
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ADREPORT_COUNT);

			rs = pstmt.executeQuery();

			while (rs.next()) {
				count= rs.getInt(1);
			}
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. " + se.getMessage());

		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}

		return count;
	}

}
