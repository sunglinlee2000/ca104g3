package com.adfundReport.controller;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import org.json.JSONObject;

import com.adfund.model.ADFundService;
import com.adfund.model.ADFundVO;
import com.adfundReport.model.*;


@MultipartConfig(fileSizeThreshold = 1024*1024, maxFileSize = 5*1024*1024, maxRequestSize = 5*5*1024)
public class ADFundReportServlet extends HttpServlet{
	
	public void doGet(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		doPost(req, res);
	}

	public void doPost(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		System.out.println(1);
		req.setCharacterEncoding("UTF-8");
		String action = req.getParameter("action");
		System.out.println("adrs:" + action);
		
			
		
		if ("getOne_For_Update".equals(action)) { // 來自listAllEmp.jsp的請求            
			
			List<String> errorMsgs = new LinkedList<String>();
			// Store this set in the request scope, in case we need to
			// send the ErrorPage view.
			req.setAttribute("errorMsgs", errorMsgs);
			
			try {
				/***************************1.接收請求參數****************************************/
				String adfundReportID = new String(req.getParameter("adfundReportID"));

				
				/***************************2.開始查詢資料****************************************/
				ADFundReportService adfundReportSvc = new ADFundReportService();
				ADFundReportVO adfundReportVO = adfundReportSvc.getOneADFundReport(adfundReportID);
								
				/***************************3.查詢完成,準備轉交(Send the Success view)************/
				req.setAttribute("adfundReportVO", adfundReportVO);         // 資料庫取出的empVO物件,存入req
				String url = "/back-end/adfund/adfund_back_report_updateStatus.jsp";
				RequestDispatcher successView = req.getRequestDispatcher(url);// 成功轉交 update_emp_input.jsp
				successView.forward(req, res);

				/***************************其他可能的錯誤處理**********************************/
			} catch (Exception e) {
				errorMsgs.add("無法取得要修改的資料:" + e.getMessage());
				RequestDispatcher failureView = req
						.getRequestDispatcher("/back-end/adfund/adfund_back_list.jsp");
				failureView.forward(req, res);
			}
		}
		
		
		if ("update".equals(action)) { // 來自update_emp_input.jsp的請求			
			
			List<String> errorMsgs = new LinkedList<String>();
			// Store this set in the request scope, in case we need to
			// send the ErrorPage view.
			req.setAttribute("errorMsgs", errorMsgs);
			
		
			try {
				/***************************1.接收請求參數 - 輸入格式的錯誤處理**********************/
				String adfundReportID = new String(req.getParameter("adfundReportID").trim());			
				
				String adfundReportStatus = req.getParameter("adfundReportStatus");				
				if (adfundReportStatus == null || adfundReportStatus.trim().length() == 0) {
					errorMsgs.add("請勾選項目");
				} 
				System.out.println(adfundReportStatus);
				
				String adfundID = req.getParameter("adfundID");
				System.out.println(adfundID);
				
				
				
				//如果檢舉狀態為審核通過REPORT_ACCEPTED  那募資狀態要變成審核未通過AD_REJECTED 即募資消失於列表上						
				if("REPORT_ACCEPTED".equals(adfundReportStatus)) {
					System.out.println("???");
					ADFundService adfundSvc = new ADFundService();
					ADFundVO adfundVO = adfundSvc.getOneADFund(adfundID);
					
					
					adfundVO.setAdfundID(adfundID);
					adfundVO.setAdfundStatus("AD_REJECTED");
					
					adfundVO = adfundSvc.updateADFundByAd(adfundID, "AD_REJECTED");
				}				
				
				

				ADFundReportVO adfundReportVO = new ADFundReportVO();
				adfundReportVO.setAdfundReportID(adfundReportID);
				adfundReportVO.setAdfundReportStatus(adfundReportStatus);	

;


				// Send the use back to the form, if there were errors
				if (!errorMsgs.isEmpty()) {
					req.setAttribute("adfundReportVO", adfundReportVO); // 含有輸入格式錯誤的empVO物件,也存入req
					RequestDispatcher failureView = req
							.getRequestDispatcher("/back-end/adfund/adfund_back_report_updateStatus.jsp");
					failureView.forward(req, res);
					return; //程式中斷
				}
				
				/***************************2.開始修改資料*****************************************/
				ADFundReportService adfundReportSvc = new ADFundReportService();
				adfundReportVO = adfundReportSvc.updateADFundStatusReport(adfundReportID, adfundReportStatus);
				
				/***************************3.修改完成,準備轉交(Send the Success view)*************/
				req.setAttribute("adfundReport", adfundReportVO); // 資料庫update成功後,正確的的empVO物件,存入req
				String url = "/back-end/administrator.jsp";
				RequestDispatcher successView = req.getRequestDispatcher(url); // 修改成功後,轉交listOneEmp.jsp
				successView.forward(req, res);				

				/***************************其他可能的錯誤處理*************************************/
			} catch (Exception e) {
				errorMsgs.add("修改資料失敗:"+e.getMessage());
				RequestDispatcher failureView = req
						.getRequestDispatcher("/back-end/adfund/adfund_back_report_updateStatus.jsp");
				failureView.forward(req, res);
			}
		}

		
		
//        if ("insert".equals(action)) { 
//        	
//
//			try {
//				/***********************1.接收請求參數 - 輸入格式的錯誤處理*************************/
//				  
//			
//			    String adfundID = req.getParameter("adfundID");
//				
//				String memberID = req.getParameter("memberID");
//				System.out.println(memberID);
//              
//				Timestamp adfundReportDate = new java.sql.Timestamp(System.currentTimeMillis());
//				
//				String adfundReportReason = req.getParameter("adfundReportReason");
//				
//
//				ADFundReportVO adfundReportVO = new ADFundReportVO();
//				adfundReportVO.setAdfundID(adfundID);
//				adfundReportVO.setMemberID(memberID);
//				adfundReportVO.setAdministratorID("A000001");
//				adfundReportVO.setAdfundReportContent("");
//				adfundReportVO.setAdfundReportDate(adfundReportDate);
//				adfundReportVO.setAdfundReportStatus("REPORT_UNCHECKED");
//				adfundReportVO.setAdfundReportReason(adfundReportReason);
//
//				// Send the use back to the form, if there were errors
//				if (!errorMsgs.isEmpty()) {
//					req.setAttribute("adfundRepoertVO", adfundReportVO); // 含有輸入格式錯誤的empVO物件,也存入req
//					RequestDispatcher failureView = req
//							.getRequestDispatcher("/front-end/adfund/adfund_list.jsp");
//					failureView.forward(req, res);
//					return;
//				}
//				
//				/***************************2.開始新增資料***************************************/
//				ADFundReportService adfundReportSvc = new ADFundReportService();
//				adfundReportVO = adfundReportSvc.addADFundReport(adfundID, memberID, "A000001", "", adfundReportDate, "REPORT_UNCHECKED", adfundReportReason);
//				
//				/***************************3.新增完成,準備轉交(Send the Success view)***********/
//				String url = "/front-end/adfund/adfund_solo.jsp";
//				RequestDispatcher successView = req.getRequestDispatcher(url); // 新增成功後轉交listAllEmp.jsp
//				successView.forward(req, res);				
//				
//				/***************************其他可能的錯誤處理**********************************/
//			} 
//			catch (Exception e) {
//				errorMsgs.add(e.getMessage());
//				RequestDispatcher failureView = req
//						.getRequestDispatcher("/front-end/adfund/adfund_solo.jsp");
//				failureView.forward(req, res);
//			}
//		}
		
		
		//改用ajax寫法
		if ("insert".equals(action)) { 
			
			System.out.println("OK");
			
			String memberID = req.getParameter("memberID");
			String adfundID = req.getParameter("adfundID");
			String adfundReportID = req.getParameter("adfundReportID");
			String adfundReportReason = req.getParameter("adfundReportReason");
			System.out.println("adfundReportReason ="+ adfundReportReason);
			
			Timestamp adfundReportDate = new Timestamp(System.currentTimeMillis());
			
			ADFundReportVO adfundReportVO = new ADFundReportVO();
			adfundReportVO.setAdfundID(adfundID);
			adfundReportVO.setMemberID(memberID);
			adfundReportVO.setAdministratorID("A000001");
			adfundReportVO.setAdfundReportContent("");
			adfundReportVO.setAdfundReportDate(adfundReportDate);
			adfundReportVO.setAdfundReportStatus("REPORT_UNCHECKED");
			adfundReportVO.setAdfundReportReason(adfundReportReason);
			
			ADFundReportService adfundReportSvc = new ADFundReportService();
			adfundReportVO = adfundReportSvc.addADFundReport(adfundID, memberID, "A000001", "", adfundReportDate, "REPORT_UNCHECKED", adfundReportReason);
			
			System.out.println("檢舉新增完成");
			
			
			JSONObject obj = new JSONObject(); 
			
			res.setContentType("text/plain"); 
			res.setCharacterEncoding("UTF-8"); 
			PrintWriter out = res.getWriter(); 
			out.write(obj.toString()); 
			out.flush(); 
			out.close();
			
			
			
		}
	}

}
