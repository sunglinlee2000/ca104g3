package com.gif.model;

import java.util.List;

public class GifService {
	
	private GifDAO_interface dao;
	
	public GifService() {
		dao = new GifDAO();
	}
	
	public GifVO addGif(String memberID, byte[] gifFile, String gifStatus) {
		
		GifVO gifVO = new GifVO();
		
		gifVO.setMemberID(memberID);
        gifVO.setGifFile(gifFile);
        gifVO.setGifStatus(gifStatus);
        dao.insert(gifVO);
        
        return gifVO;
	}
	
public GifVO updateGif(String gifID, String gifStatus) {
		
		GifVO gifVO = new GifVO();
		
		gifVO.setGifID(gifID);
		gifVO.setGifStatus(gifStatus);
        dao.update(gifVO);
        
        return gifVO;
	}
	
	
	public GifVO getOneGif(String gifID) {
		return dao.findByPrimaryKey(gifID);
	}
	
	public List<GifVO> getAllGif(String memberID){
		return dao.getAll(memberID);
	}
	
	public List<GifVO> getAllGif_inUse(String memberID, String gifStatus){
		return dao.getAll_inUse(memberID, gifStatus);
	}

}
