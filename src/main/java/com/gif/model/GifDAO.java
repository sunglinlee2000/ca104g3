package com.gif.model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

public class GifDAO implements GifDAO_interface{
	
	private static DataSource ds = null;
	static {
		try {
			Context ctx = new InitialContext();
			ds = (DataSource) ctx.lookup("java:comp/env/jdbc/CA104G3DB");
		} catch (NamingException e) {
			e.printStackTrace();
		}
	}
	
	private static final String INSERT_STMT = "INSERT INTO GIF VALUES "
			+ "('GIF'||(LPAD(TO_CHAR(GIF_SEQ.NEXTVAL),6,'0')),?,?,?)";
	private static final String GET_ALL_STMT = "SELECT * FROM GIF  WHERE memberID = ? ORDER BY GIFID DESC";
	private static final String GET_ALL_BY_STATUS_STMT = "SELECT * FROM GIF  WHERE memberID = ? "
			+ "and gifStatus = ? ORDER BY GIFID DESC";
	private static final String GET_ONE_STMT = "SELECT * FROM GIF WHERE GIFID = ?";
	private static final String UPDATE = "UPDATE GIF set gifStatus = ? WHERE gifID = ?";
	
	private static final String IN_USE = "GIF_IN_USE";
	private static final String OUT_OF_USE = "GIF_OUT_OF_USE";

	@Override
	public void insert(GifVO gifVO) {
		
		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(INSERT_STMT);

			pstmt.setString(1, gifVO.getMemberID());
			pstmt.setBytes(2, gifVO.getGifFile());
			pstmt.setString(3, gifVO.getGifStatus());

			pstmt.executeUpdate();

		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. " + se.getMessage());

		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}

			if (con != null) {
				try {
					con.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
		}				
	}

	@Override
	public GifVO findByPrimaryKey(String gifID) {
		GifVO gifVO = new GifVO();
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ONE_STMT);
			
			pstmt.setString(1, gifID);
			
			rs = pstmt.executeQuery();
			
			while(rs.next()) {
				gifVO = new GifVO();
				gifVO.setGifID(rs.getString("gifid"));
				gifVO.setMemberID(rs.getString("memberid"));
				gifVO.setGifFile(rs.getBytes("giffile"));
			}
					
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. " + se.getMessage());

		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}
		return gifVO;
	}

	@Override
	public List<GifVO> getAll(String memberID) {
		List<GifVO> giflist = new ArrayList<GifVO>();
		GifVO gifVO = new GifVO();
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			
			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ALL_STMT);
			pstmt.setString(1, memberID);
			rs = pstmt.executeQuery();
			
			while(rs.next()) {
				gifVO = new GifVO();
				gifVO.setGifID(rs.getString("gifid"));
				gifVO.setMemberID(rs.getString("memberid"));
				gifVO.setGifFile(rs.getBytes("giffile"));
				gifVO.setGifStatus(rs.getString("gifStatus"));
				giflist.add(gifVO);
			}
					
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. " + se.getMessage());

		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}	
		return giflist;
	}

	@Override
	public void update(GifVO gifVO) {
		Connection con = null;
		PreparedStatement pstmt = null;

		try {

			con = ds.getConnection();
			pstmt = con.prepareStatement(UPDATE);

			pstmt.setString(1, gifVO.getGifStatus());
			pstmt.setString(2, gifVO.getGifID());

			pstmt.executeUpdate();

		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. " + se.getMessage());

		} finally {
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}

			if (con != null) {
				try {
					con.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
		}				
	}

	@Override
	public List<GifVO> getAll_inUse(String memberID, String gifStatus) {
		List<GifVO> giflist = new ArrayList<GifVO>();
		GifVO gifVO = new GifVO();
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			
			con = ds.getConnection();
			pstmt = con.prepareStatement(GET_ALL_BY_STATUS_STMT);
			pstmt.setString(1, memberID);
			pstmt.setString(2, gifStatus);
			rs = pstmt.executeQuery();
			
			while(rs.next()) {
				gifVO = new GifVO();
				gifVO.setGifID(rs.getString("gifid"));
				gifVO.setMemberID(rs.getString("memberid"));
				gifVO.setGifFile(rs.getBytes("giffile"));
				gifVO.setGifStatus(rs.getString("gifStatus"));
				giflist.add(gifVO);
			}
					
		} catch (SQLException se) {
			throw new RuntimeException("A database error occured. " + se.getMessage());

		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException se) {
					se.printStackTrace(System.err);
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace(System.err);
				}
			}
		}	
		return giflist;
	}


}
