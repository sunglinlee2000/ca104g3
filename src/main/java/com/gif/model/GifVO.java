package com.gif.model;

public class GifVO implements java.io.Serializable{
	private String gifID;
	private String memberID;
	private byte[] gifFile;
	private String gifStatus;
	
	public String getGifID() {
		return gifID;
	}
	public void setGifID(String gifID) {
		this.gifID = gifID;
	}
	public String getMemberID() {
		return memberID;
	}
	public void setMemberID(String memberID) {
		this.memberID = memberID;
	}
	public byte[] getGifFile() {
		return gifFile;
	}
	public void setGifFile(byte[] gifFile) {
		this.gifFile = gifFile;
	}
	public String getGifStatus() {
		return gifStatus;
	}
	public void setGifStatus(String gifStatus) {
		this.gifStatus = gifStatus;
	}
}
