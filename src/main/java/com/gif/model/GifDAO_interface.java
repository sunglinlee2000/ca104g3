package com.gif.model;

import java.util.List;

public interface GifDAO_interface {
    public void insert(GifVO gifVO);
    public void update(GifVO gifVO);
    public GifVO findByPrimaryKey(String gifid); 
    public List<GifVO> getAll(String memberID);
    public List<GifVO> getAll_inUse(String memberID, String gifStatus);

}
