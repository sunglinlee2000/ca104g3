package com.gif.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.gif.model.*;

public class GifImgServlet extends HttpServlet{
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException{
		req.setCharacterEncoding("UTF-8");
		
		String gifID = req.getParameter("gifID");
		String gifFile = req.getParameter("gifFile");
		
		GifService gifService = new GifService();
		GifVO gifVO = gifService.getOneGif(gifID);
		
		byte[] pic = null;
		
		ServletOutputStream out = res.getOutputStream();
		
		if("gifFile".equals(gifFile)) {
			pic = gifVO.getGifFile();
		}
		
		res.setContentType("image/gif");
		res.setContentLength(pic.length);
		
		out.write(pic);
		out.flush();
		out.close();
		
	}
}
