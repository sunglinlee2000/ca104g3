package com.gif.controller;

import java.io.IOException;
import java.io.InputStream;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import com.gif.model.GifService;
import com.gif.model.GifVO;
import com.member.model.MemberVO;

@MultipartConfig(fileSizeThreshold = 1024 * 1024, maxFileSize = 5 * 1024 * 1024, maxRequestSize = 5 * 5 * 1024 * 1024)
public class GifServlet extends HttpServlet {

	public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		doPost(req, res);
	}

	public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {

		req.setCharacterEncoding("UTF-8");
		String action = req.getParameter("action");
		System.out.println("gif=" + action);

		if ("insert".equals(action)) {

			List<String> errorMsgs = new LinkedList<String>();
			// Store this set in the request scope, in case we need to
			// send the ErrorPage view.
			req.setAttribute("errorMsgs", errorMsgs);

			try {

				String memberID = req.getParameter("memberID");
				String gifStatus = req.getParameter("gifStatus");

				Part part1 = req.getPart("gifFile");
				InputStream is1 = part1.getInputStream();
				byte[] gifFile = new byte[is1.available()];
				is1.read(gifFile);

				GifVO gifVO = new GifVO();
				gifVO.setMemberID(memberID);
				gifVO.setGifFile(gifFile);
				gifVO.setGifStatus(gifStatus);

				// Send the use back to the form, if there were errors
				if (!errorMsgs.isEmpty()) {
					req.setAttribute("gifVO", gifVO); // 含有輸入格式錯誤的empVO物件,也存入req
					RequestDispatcher failureView = req.getRequestDispatcher("/front-end/gif_collection/gif_list.jsp");
					failureView.forward(req, res);
					return;
				}

				GifService gifService = new GifService();
				gifVO = gifService.addGif(memberID, gifFile, gifStatus);

				res.sendRedirect(req.getContextPath() + "/front-end/gif_collection/gif_list.jsp");

			} catch (Exception e) {
				errorMsgs.add(e.getMessage());
				RequestDispatcher failureView = req.getRequestDispatcher("/front-end/gif_collection/gif_list.jsp");
				failureView.forward(req, res);
			}

		}

		if ("gif_in_use".equals(action)) {

			List<String> errorMsgs = new LinkedList<String>();
			// Store this set in the request scope, in case we need to
			// send the ErrorPage view.
			req.setAttribute("errorMsgs", errorMsgs);

			try {

				String gifID = req.getParameter("gifID");
				String gifStatus = req.getParameter("gifStatus");

				GifService gifService = new GifService();
				gifService.updateGif(gifID, gifStatus);

			} catch (Exception e) {
				errorMsgs.add(e.getMessage());
				RequestDispatcher failureView = req.getRequestDispatcher("/front-end/gif_collection/gif_list.jsp");
				failureView.forward(req, res);
			}

		}
		
		if ("gif_out_of_use".equals(action)) {

			List<String> errorMsgs = new LinkedList<String>();
			// Store this set in the request scope, in case we need to
			// send the ErrorPage view.
			req.setAttribute("errorMsgs", errorMsgs);

			try {

				String gifID = req.getParameter("gifID");
				String gifStatus = req.getParameter("gifStatus");

				GifService gifService = new GifService();
				gifService.updateGif(gifID, gifStatus);

			} catch (Exception e) {
				errorMsgs.add(e.getMessage());
				RequestDispatcher failureView = req.getRequestDispatcher("/front-end/gif_collection/gif_list.jsp");
				failureView.forward(req, res);
			}

		}
	}

}
