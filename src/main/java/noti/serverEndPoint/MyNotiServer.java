package noti.serverEndPoint;
import java.io.*;
import java.sql.Timestamp;
import java.util.*;

import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;

import org.json.JSONException;
import org.json.JSONObject;

import com.notificationList.model.*;

import javax.websocket.Session;
import javax.websocket.OnOpen;
import javax.websocket.OnMessage;
import javax.websocket.OnError;
import javax.websocket.OnClose;
import javax.websocket.CloseReason;

@ServerEndpoint("/MyNotiServer/{myName}/{myRoom}")
public class MyNotiServer {
	
private static final Set<Session> allSessions = Collections.synchronizedSet(new HashSet<Session>());

    @OnOpen
    public void onOpen(@PathParam("myName") String myName, @PathParam("myRoom") int myRoom, Session userSession) throws IOException{
    	allSessions.add(userSession);
    	System.out.println(userSession.getId() + ": 已連線");
    	
    }
    
    
    @OnMessage
    public void onMessage(Session userSession, String message) {
    	for(Session session : allSessions) {
    		if(session.isOpen()) {
    			try {
                System.out.println(".......");
                
                //接前端送來的東西
    			JSONObject j = new JSONObject(message);
    		    Object startMemberOb = j.get("startMember");  
    		    Object startMemberID = j.get("startMemberID");
    		    Object adfundTitle = j.get("adfundTitle");
    		    Object adfundCoinNow = j.get("adfundCoinNow");
    		    Object adfundCoinCoinUsed = j.get("adfundCoinUsed");
    		    Object adfundID = j.get("adfundID");
    		    Object joinMemberID = j.get("joinMemberID");
    		    Object joinMemberNickName = j.get("joinMemberNickName");
    		    
//    		    System.out.println("adfundCoinCoinUsed = " + adfundCoinCoinUsed);
//    		    System.out.println("adfundCoinNow = " + adfundCoinNow);
    		      			  			
    			
    			//把東西存進資料庫    			
    			Integer sum = Integer.valueOf((String) adfundCoinNow) + Integer.valueOf((String) adfundCoinCoinUsed);
//    			System.out.println("sum" + sum);
    			
    			String messages = joinMemberNickName + "參加了你的【 " + adfundTitle + " 】募資案 已達成" + sum + " 代幣";
    			
    			long nowTime = new Date().getTime();     			
    			Timestamp now = new Timestamp(nowTime);
    			
    			NotificationListService noSvc =new NotificationListService();
				List<NotificationListVO> noList = noSvc.getAllNoList(joinMemberID.toString());
				
				int falseCount = 1;
				
				for(NotificationListVO noList1 : noList) {					
					if("FALSE".equals(noList1.getNotificationclick())){
						falseCount++; 
					}					
				}
				System.out.println("falseCount2=" + falseCount);
    			
    			NotificationListVO noVO =  noSvc.addNoList("NT000001", startMemberID.toString(), messages, now, adfundID.toString(),"FALSE", "/adfundTest/adfund.do?action=getOne_For_Display&adfundID=");
    			
    			System.out.println("新增通知表格OK");
    			
    			
    			//再把東西送回前端
    			message = "{\"joinMemberNickName\":\"" + joinMemberNickName + "\" , \"adfundTitle\":\"" + adfundTitle + "\" , \"sum\":\"" + sum + "\" , \"falseCount\":\"" + falseCount + "\"}";
     //           System.out.println(message);               
    			
    			session.getAsyncRemote().sendText(message);
    			

    			
    			}catch(Exception e){
    			      System.err.println("Error: " + e.getMessage());
    		    }
    		}
    	}
    	System.out.println("Message received: " + message);
    }
    
    
    @OnError
    public void onError(Session userSession, Throwable e) {
//    	e.printStackTrace();
    }
    
    
    @OnClose
    public void onClose(Session userSession, CloseReason reason) {
    	allSessions.remove(userSession);
    	System.out.println(userSession.getId() + ": Disconnected: " + Integer.toString(reason.getCloseCode().getCode()));
    }

}
