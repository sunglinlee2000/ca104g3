--------------------------------------------------------
--  建立表格
--------------------------------------------------------
alter session set deferred_segment_creation=false;

DROP TABLE ORD_DETAILS;
DROP TABLE ORD;
DROP TABLE MEMBER;
DROP TABLE PRODUCT;
DROP TABLE PROD_CATEGORY;

DROP SEQUENCE ord_seq;
DROP SEQUENCE member_seq;
DROP SEQUENCE product_seq;
DROP SEQUENCE prod_category_seq;



--------------------------------------------------------
--  for Table PROD_CATEGORY
--------------------------------------------------------
CREATE TABLE PROD_CATEGORY (
  CATEGORY_NO    NUMBER(10,0)  NOT NULL, 
  CATEGORY_NAME  VARCHAR2(50)  NOT NULL, 
  CATEGORY_DESCR VARCHAR2(600) NOT NULL, 
  CONSTRAINT PROD_CATEGORY_PK PRIMARY KEY (CATEGORY_NO));
	  
CREATE SEQUENCE prod_category_seq
INCREMENT BY 10
START WITH 10
NOMAXVALUE
NOCYCLE
NOCACHE;

Insert into PROD_CATEGORY (CATEGORY_NO,CATEGORY_NAME,CATEGORY_DESCR) values (prod_category_seq.NEXTVAL,'單車','我是腳踏車');
Insert into PROD_CATEGORY (CATEGORY_NO,CATEGORY_NAME,CATEGORY_DESCR) values (prod_category_seq.NEXTVAL,'單車裝備','我是單車裝備');
Insert into PROD_CATEGORY (CATEGORY_NO,CATEGORY_NAME,CATEGORY_DESCR) values (prod_category_seq.NEXTVAL,'騎士裝備','我是騎士裝備');



--------------------------------------------------------
--  for Table PRODUCT
--------------------------------------------------------
CREATE TABLE PRODUCT (
  PRODUCT_NO     NUMBER(10,0)   NOT NULL, 
  CATEGORY_NO    NUMBER(10,0)   NOT NULL, 
  PROD_NAME      VARCHAR2(50)   NOT NULL, 
  PROD_PRICE     NUMBER(10,0)   NOT NULL, 
  PROD_INTRODUCE VARCHAR2(1000) NOT NULL, 
  PROD_STOCK     NUMBER(10,0)   NOT NULL, 
  PROD_STATUS    NUMBER(1,0)    NOT NULL, 
  PROD_PIC       BLOB,
  CONSTRAINT PRODUCT_FK FOREIGN KEY (CATEGORY_NO) REFERENCES PROD_CATEGORY (CATEGORY_NO),
  CONSTRAINT PRODUCT_PK PRIMARY KEY (PRODUCT_NO));
  
CREATE SEQUENCE product_seq
INCREMENT BY 1
START WITH 1001
NOMAXVALUE
NOCYCLE
NOCACHE;

Insert into PRODUCT (PRODUCT_NO,CATEGORY_NO,PROD_NAME,PROD_PRICE,PROD_INTRODUCE,PROD_STOCK,PROD_STATUS) values (product_seq.NEXTVAL,10,'Alias單車',30000,'Alias Sport是結合性能與多功能的完美組合。FACT 10r碳纖維車架，女性專屬Alias車架幾何，妳可以隨時改變設定，作為公路長途訓練使用或進行鐵人三項競賽。',100,1);
Insert into PRODUCT (PRODUCT_NO,CATEGORY_NO,PROD_NAME,PROD_PRICE,PROD_INTRODUCE,PROD_STOCK,PROD_STATUS) values (product_seq.NEXTVAL,10,'Diverge單車',30000,'Diverge Comp是部探險速度機器，獨特的BB高度與前叉角度，讓Diverge在任何地形都能自信、平順的通過。',100,1);
Insert into PRODUCT (PRODUCT_NO,CATEGORY_NO,PROD_NAME,PROD_PRICE,PROD_INTRODUCE,PROD_STOCK,PROD_STATUS) values (product_seq.NEXTVAL,20,'Power Pro座墊',2000,'超輕量化Power Pro座墊，具良好剛性，FACT碳纖維強化底座與耐用的鈦合金座弓，維持舒適坐姿。',100,1);
Insert into PRODUCT (PRODUCT_NO,CATEGORY_NO,PROD_NAME,PROD_PRICE,PROD_INTRODUCE,PROD_STOCK,PROD_STATUS) values (product_seq.NEXTVAL,20,'Roval CLX64輪組',2000,'Roval CLX64輪組與S-Works Turbo輪胎是在我們的Win Tunnel風洞實驗室中整合研發設計，孕育出結合空氣力學與低滾動阻力的最快速的公路競賽輪組/輪胎系統。',100,1);
Insert into PRODUCT (PRODUCT_NO,CATEGORY_NO,PROD_NAME,PROD_PRICE,PROD_INTRODUCE,PROD_STOCK,PROD_STATUS) values (product_seq.NEXTVAL,30,'S-Works車鞋',1000,'頂級S-Works系列重新定義一雙好的車鞋該有什麼效能。我們使用最輕、最佳剛性並符合Body Geometry人體工學設計。提供Pro Peloton選手或追求巔峰的騎士前所未有的舒傳導。',100,1);
Insert into PRODUCT (PRODUCT_NO,CATEGORY_NO,PROD_NAME,PROD_PRICE,PROD_INTRODUCE,PROD_STOCK,PROD_STATUS) values (product_seq.NEXTVAL,30,'後背包',1000,'我們的後背包也許不是所向無敵，但我們相信，它會超越所有『騎士』對後背包的需求！最外部使用較厚的尼龍防水材質，上下班通勤使用也不用擔心',100,1);



--------------------------------------------------------
--  for Table MEMBER
--------------------------------------------------------
CREATE TABLE MEMBER (	
  MEM_NO      VARCHAR2(7)   NOT NULL, 
  MEM_ACC     VARCHAR2(20)  NOT NULL, 
  MEM_PSW     VARCHAR2(20)  NOT NULL, 
  MEM_NAME    VARCHAR2(20)  NOT NULL, 
  MEM_ID      VARCHAR2(10)  NOT NULL UNIQUE, 
  MEM_PHONE   VARCHAR2(15), 
  MEM_ZIP     VARCHAR2(5) , 
  MEM_ADDRESS VARCHAR2(50), 
  CARD_NO     VARCHAR2(20), 
  CARD_YY     VARCHAR2(10), 
  CARD_MM     VARCHAR2(10), 
  CARD_ID     VARCHAR2(3) , 
  CREATE_TIME DATE          NOT NULL, 
  MEM_PHOTO   BLOB, 
  MEM_EMAIL   VARCHAR2(50)  NOT NULL, 
  MEM_STATUS  VARCHAR2(1)   NOT NULL, 
  REGID       VARCHAR2(400),
  CONSTRAINT  MEMBER_PK PRIMARY KEY (MEM_NO));
  
CREATE SEQUENCE member_seq
INCREMENT BY 1
START WITH 1
NOMAXVALUE
NOCYCLE
NOCACHE;

Insert into MEMBER (MEM_NO,MEM_ACC,MEM_PSW,MEM_NAME,MEM_ID,MEM_PHONE,MEM_ZIP,MEM_ADDRESS,CARD_NO,CARD_YY,CARD_MM,CARD_ID,CREATE_TIME,MEM_PHOTO,MEM_EMAIL,MEM_STATUS,REGID) values ('M'||LPAD(to_char(member_seq.NEXTVAL), 6, '0'),'tomcat','111','湯姆貓','A111111111','0988888888','110','台北市-信義區-市府路1號','4019454065577271','2020','6','777',TO_DATE('2015-11-17','YYYY-MM-DD'),null,'ixlogic.wu2@gmail.com','1',null);
Insert into MEMBER (MEM_NO,MEM_ACC,MEM_PSW,MEM_NAME,MEM_ID,MEM_PHONE,MEM_ZIP,MEM_ADDRESS,CARD_NO,CARD_YY,CARD_MM,CARD_ID,CREATE_TIME,MEM_PHOTO,MEM_EMAIL,MEM_STATUS,REGID) values ('M'||LPAD(to_char(member_seq.NEXTVAL), 6, '0'),'peter1','222','吳永志','A123456789','0988888888','110','台北市-信義區-市府路2號','4019454065577272','2020','6','777',TO_DATE('2015-11-17','YYYY-MM-DD'),null,'ixlogic.wu2@gmail.com','1',null);



--------------------------------------------------------
--  for Table ORDER
--------------------------------------------------------
CREATE TABLE ORD (
  ORD_NO      VARCHAR2(15) NOT NULL, 
  MEM_NO      VARCHAR2(7)  NOT NULL, 
  ORD_STATUS  VARCHAR2(1), 
  RECEIVER    VARCHAR2(50), 
  REC_PHONE   VARCHAR2(20), 
  REC_ZIP     VARCHAR2(10), 
  REC_ADDRESS VARCHAR2(200), 
  ORD_TOTAL   NUMBER(10,0), 
  ORD_DATE    DATE,
  CONSTRAINT ORDER_FK FOREIGN KEY (MEM_NO) REFERENCES MEMBER (MEM_NO),
  CONSTRAINT ORDER_PK PRIMARY KEY (ORD_NO));
   
CREATE SEQUENCE ord_seq
INCREMENT BY 1
START WITH 1
NOMAXVALUE
NOCYCLE
NOCACHE;

Insert into ORD (ORD_NO,MEM_NO,ORD_STATUS,RECEIVER,REC_PHONE,REC_ZIP,REC_ADDRESS,ORD_TOTAL,ORD_DATE) values (to_char(sysdate,'yyyymmdd')||'-'||LPAD(to_char(ord_seq.NEXTVAL), 6, '0'),'M000001','1','湯姆貓','0988888888','320','桃園市中壢區福德一路177巷60弄2號',60000,sysdate);



--------------------------------------------------------
--  for Table ORD_DETAILS
--------------------------------------------------------
CREATE TABLE ORD_DETAILS(	
  ORD_NO     VARCHAR2(15) NOT NULL, 
  PRODUCT_NO NUMBER(10,0) NOT NULL, 
  QUANTITY   NUMBER(10,0),
  PROD_PRICE NUMBER(10,0) NOT NULL,
  CONSTRAINT ORD_DETAILS_FK1 FOREIGN KEY (ORD_NO) REFERENCES ORD (ORD_NO),
  CONSTRAINT ORD_DETAILS_FK2 FOREIGN KEY (PRODUCT_NO) REFERENCES PRODUCT (PRODUCT_NO),
  CONSTRAINT ORD_DETAILS_PK PRIMARY KEY(ORD_NO, PRODUCT_NO));
  
Insert into ORD_DETAILS (ORD_NO,PRODUCT_NO,QUANTITY,PROD_PRICE) values (to_char(sysdate,'yyyymmdd')||'-'||LPAD(to_char(ord_seq.CURRVAL), 6, '0'),1001,1,30000);
Insert into ORD_DETAILS (ORD_NO,PRODUCT_NO,QUANTITY,PROD_PRICE) values (to_char(sysdate,'yyyymmdd')||'-'||LPAD(to_char(ord_seq.CURRVAL), 6, '0'),1002,1,30000);
   
commit;